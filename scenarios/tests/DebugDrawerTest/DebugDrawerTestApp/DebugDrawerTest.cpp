/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotAPI::DebugDrawerTestApp
* @author     vahrenkamp
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "DebugDrawerTest.h"
#include <RobotAPI/libraries/core/Pose.h>

namespace armarx
{
    void DebugDrawerTest::onInitComponent()
    {


    }


    void DebugDrawerTest::onConnectComponent()
    {
        ARMARX_INFO << "starting debug drawer test";
        try
        {
            prxDD = getTopic<armarx::DebugDrawerInterfacePrx>("DebugDrawerUpdates");
            std::string robFile = "RobotAPI/robots/Armar3/ArmarIII.xml";
            std::string project = "RobotAPI";
            ARMARX_INFO << "adding robot visu";
            prxDD->setRobotVisu("TestVisu", "TestRobot", robFile, project, DrawStyle::FullModel);

            ARMARX_INFO << "setting robot position";
            Eigen::Matrix4f p;
            p.setIdentity();
            p(0, 3) = 1000.0f;
            p(1, 3) = 1000.0f;
            PosePtr gp(new Pose(p));
            prxDD->updateRobotPose("TestVisu", "TestRobot", gp);

            ARMARX_INFO << "sleeping 10 seconds...";
            usleep(10000000);

            ARMARX_INFO << "setting config of elbow L";
            NameValueMap conf;
            conf["Elbow L"] = 0.5;
            conf["Elbow XY"] = 0.5; // should be ignored!

            prxDD->updateRobotConfig("TestVisu", "TestRobot", conf);

            ARMARX_INFO << "sleeping 10 seconds...";
            usleep(10000000);

            ARMARX_INFO << "adding second robot visu";
            prxDD->setRobotVisu("TestVisu", "TestRobot2", robFile, project, DrawStyle::FullModel);
            ARMARX_INFO << "setting second robot position";
            p(0, 3) = 3000.0f;
            p(1, 3) = 3000.0f;
            PosePtr gp2(new Pose(p));
            prxDD->updateRobotPose("TestVisu", "TestRobot2", gp2);

            ARMARX_INFO << "sleeping 10 seconds...";
            usleep(10000000);

            ARMARX_INFO << "removing second robot visu";
            prxDD->removeRobotVisu("TestVisu", "TestRobot2");

            ARMARX_INFO << "setting robot color...";
            DrawColor c;
            c.r = 0.3;
            c.g = 0.9;
            c.b = 0.3;
            c.a = 1.0f;
            prxDD->updateRobotColor("TestVisu", "TestRobot", c);

            ARMARX_INFO << "sleeping 10 seconds...";
            usleep(10000000);

            ARMARX_INFO << "clearing layer";
            prxDD->clearLayer("TestVisu");

            ARMARX_INFO << "sleeping 10 seconds...";
            usleep(10000000);

            ARMARX_INFO << "adding robot visu";
            prxDD->setRobotVisu("TestVisu", "TestRobot", robFile, project, DrawStyle::FullModel);

            ARMARX_INFO << "setting robot position 6000/1000";
            p(0, 3) = 6000.0f;
            p(1, 3) = 1000.0f;
            PosePtr gp3(new Pose(p));
            prxDD->updateRobotPose("TestVisu", "TestRobot", gp3);

            ARMARX_INFO << "adding second robot visu";
            prxDD->setRobotVisu("TestVisu", "TestRobot2", robFile, project, DrawStyle::FullModel);
            ARMARX_INFO << "setting second robot position 6000/3000";
            p(0, 3) = 6000.0f;
            p(1, 3) = 3000.0f;
            PosePtr gp4(new Pose(p));
            prxDD->updateRobotPose("TestVisu", "TestRobot2", gp4);


        }
        catch (...)
        {
            ARMARX_IMPORTANT << "TEST FAILED";
            return;
        }
        ARMARX_INFO << "FINISHED test";

    }
}
