/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::SkillManager
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/skills/SkillProviderInterface.ice>

module armarx
{
    module skills
    {
        module manager
        {
            module dto
            {
                // Inputs
                struct SkillExecutionRequest
                {
                    string executorName; // Who called the request
                    provider::dto::SkillID skillId; // The id of a skill to request. Note that the skill or provider name can be regexes.
                    aron::data::dto::Dict params; // the parameters for the skill. Can be nullptr if the skill does not require parameters
                };
                struct ProviderInfo
                {
                    string providerName;
                    provider::dti::SkillProviderInterface* provider;
                    provider::dto::SkillDescriptionMap providedSkills;
                };

                // Provider data types
                dictionary<string, provider::dto::SkillDescriptionMap> SkillDescriptionMapMap;
                dictionary<string, provider::dto::SkillStatusUpdateMap> SkillStatusUpdateMapMap;
            }

            module dti
            {
                interface SkillManagerInterface extends callback::dti::SkillProviderCallbackInterface
                {
                    // There is by design no method to get the ProviderInfo. You should only communicate with the manager

                    void addProvider(dto::ProviderInfo providerInfo);
                    void removeProvider(string providerName);

                    dto::SkillDescriptionMapMap getSkillDescriptions();
                    dto::SkillStatusUpdateMapMap getSkillExecutionStatuses();

                    provider::dto::SkillStatusUpdate executeSkill(dto::SkillExecutionRequest skillExecutionInfo);
                    void abortSkill(string providerName, string skillName);

                };
            }
        }
    }
}
