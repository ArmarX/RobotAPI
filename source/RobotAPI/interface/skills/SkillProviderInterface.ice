/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::SkillManager
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/aron.ice>


// We need to prototype the interface
module armarx
{
    module skills
    {
        module callback
        {
            module dti
            {
                interface SkillProviderCallbackInterface;
            }
        }
    }
}


module armarx
{
    module skills
    {
        module provider
        {
            module dto
            {
                sequence<string> StringList;

                // A skill ID. Must be unique within one SkillManager
                struct SkillID
                {
                    string providerName;
                    string skillName;
                };

                // Description of a skill, independant of a provider
                // A skill is nothing but a executable thing, which can be executed on one or more 'robots' (empty means all)
                struct SkillDescription
                {
                    string                        skillName;     // the name of the skill
                    string                        description;   // a human readable description of what the skill does. Used in GUI
                    StringList                    robots;        // the names of the robots that are able to execute that skill
                    long                          timeoutMs;     // in milliseconds, can be set to -1 for infinite
                    aron::type::dto::AronObject   acceptedType;  // the name of the object is irrelevant and only used in GUI. nullptr if not set
                    aron::data::dto::Dict         defaultParams; // the default parameterization used in GUI. nullptr if not set
                };
                dictionary<string, SkillDescription> SkillDescriptionMap;

                // Input to a provider to execute a skill
                struct SkillExecutionRequest
                {
                    string skillName; // the id of the skill
                    string executorName; // the name of the component/lib/skill that called the execution of the skill
                    aron::data::dto::Dict params; // the used parameterization
                    callback::dti::SkillProviderCallbackInterface* callbackInterface; // use nullptr if you do not want to have callbacks
                };

                // The status enum of a skill
                module Execution
                {
                    enum Status
                    {
                        Idle, // a skill can only be idled if it has never been scheduled
                        Scheduled,
                        Running,

                        // terminating values
                        Failed,
                        Succeeded,
                        Aborted
                    };
                }

                // Status updates of a skill
                struct SkillStatusUpdateHeader
                {
                    SkillID                                        skillId; // the id of the skill
                    string                                         executorName; // the name of the component/lib/skill that called the execution of the skill
                    aron::data::dto::Dict                          usedParams; // the used parameterization
                    callback::dti::SkillProviderCallbackInterface* usedCallbackInterface; // the used callback interface. Probably a prx to the manager
                    Execution::Status                              status; // the current status of the skill
                };

                struct SkillStatusUpdate
                {
                    SkillStatusUpdateHeader                        header;
                    aron::data::dto::Dict                          data; // data, attached to the status update. If send via a callback, this data may be used by the callback interface
                };

                dictionary<string, SkillStatusUpdate> SkillStatusUpdateMap;
            }

            module dti
            {
                interface SkillProviderInterface
                {
                    dto::SkillDescription getSkillDescription(string name);
                    dto::SkillDescriptionMap getSkillDescriptions();
                    dto::SkillStatusUpdate getSkillExecutionStatus(string name);
                    dto::SkillStatusUpdateMap getSkillExecutionStatuses();

                    // execute skill will ALWAYS fully execute the skill and wait, until the skill is finished.
                    // Use the _begin() method, if you want to call a skill async
                    // This method returns a status update where the status is ALWAYS one of the terminating values
                    dto::SkillStatusUpdate executeSkill(dto::SkillExecutionRequest executionInfo);

                    // try to kill a skill as soon as possible. When the skill is stopped depends on the implementation.
                    void abortSkill(string skill);
                };
            }
        }
    }
}


module armarx
{
    module skills
    {
        module callback
        {
            module dti
            {
                interface SkillProviderCallbackInterface
                {
                    // used for callbacks from providers to update their skill execution status
                    void updateStatusForSkill(provider::dto::SkillStatusUpdate statusUpdate);
                }
            }
        }
    }
}

