/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::TrajectoryControllerSubUnit
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/core/Trajectory.ice>
#include <RobotAPI/interface/observers/KinematicUnitObserverInterface.ice>
#include <ArmarXCore/interface/serialization/Eigen.ice>
#include <RobotAPI/interface/aron.ice>

module armarx
{
    interface TrajectoryPlayerInterface extends KinematicUnitListener
    {
        bool startTrajectoryPlayer();
        bool pauseTrajectoryPlayer();
        bool stopTrajectoryPlayer();
        bool resetTrajectoryPlayer(bool moveToFrameZeroPose);

        void loadJointTraj(TrajectoryBase jointTraj);
        void loadBasePoseTraj(TrajectoryBase basePoseTraj);

        void setLoopPlayback(bool loop);
        void setIsVelocityControl(bool isVelocity);
        void setIsPreview(bool isPreview);
        bool setJointsInUse(string jointName, bool inUse);
        void enableRobotPoseUnit(bool isRobotPose);

        void setOffset(Eigen::Matrix4f offset);

        double getCurrentTime();
        double getEndTime();
        double getTrajEndTime();

        void setEndTime(double endTime);

        void considerConstraints(bool consider);
    };
};
