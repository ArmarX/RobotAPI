/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_skills::ArmarXObjects::HumanAvoidance
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @author     Fabian Peller
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

// ArmarX
#include <ArmarXCore/interface/serialization/Eigen.ice>


module armarx
{
    module dynamicobstaclemanager
    {
        struct LineSegment
        {
            Eigen::Vector2f lineStart;
            Eigen::Vector2f lineEnd;
        };

        sequence<LineSegment> LineSegments;
    }


    interface DynamicObstacleManagerInterface
    {
        void
        add_decayable_obstacle(Eigen::Vector2f e_origin, float e_rx, float e_ry, float e_yaw);

        void
        add_decayable_line_segment(Eigen::Vector2f line_start, Eigen::Vector2f line_end);

        void
        add_decayable_line_segments(dynamicobstaclemanager::LineSegments lines);

        void
        remove_all_decayable_obstacles();

        void
        directly_update_obstacle(string name, Eigen::Vector2f e_origin, float e_rx, float e_ry, float e_yaw);

        void
        remove_obstacle(string name);

        void wait_unitl_obstacles_are_ready();

        float distanceToObstacle(Eigen::Vector2f agentPosition, float safetyRadius, Eigen::Vector2f goal);
    };
};

