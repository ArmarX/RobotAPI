/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ObstacleAvoidance
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// ArmarX
#include <ArmarXCore/interface/serialization/Eigen.ice>


module armarx
{

    module obstacledetection
	{

		struct Obstacle
		{
			string name;
			double posX = 0;
			double posY = 0;
			double posZ = 0;
			double yaw = 0;
			double axisLengthX = 0;
			double axisLengthY = 0;
			double axisLengthZ = 0;
			double refPosX = 0;
			double refPosY = 0;
			double refPosZ = 0;
			double safetyMarginX = 0;
			double safetyMarginY = 0;
			double safetyMarginZ = 0;
			double curvatureX = 1;
			double curvatureY = 1;
			double curvatureZ = 1;
		};

        sequence<obstacledetection::Obstacle> ObstacleList;

	};


	interface ObstacleDetectionInterface
	{
		void
        updateObstacle(obstacledetection::Obstacle obstacle);

		void
		removeObstacle(string obstacle_name);

        obstacledetection::ObstacleList
		getObstacles();

		void
		updateEnvironment();

	};

};
