/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Adrian Knobloch ( adrian dot knobloch at student dot kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/interface/core/BasicVectorTypes.ice>
#include <ArmarXCore/interface/serialization/Eigen.ice>
//#include <RobotAPI/interface/aron.h>

module armarx
{
    interface CartesianPositionControlInterface
    {
        void setEnabled(string side, bool enabled);
        void setTarget(string side, Eigen::Matrix4f target, bool setOrientation);
        void setFTLimit(string side, float maxForce, float maxTorque);
        void setCurrentFTAsOffset(string side);
        void resetFTOffset(string side);
        int getFTTresholdExceededCounter(string side);
        void emulateFTTresholdExceeded(string side);

    };
};
