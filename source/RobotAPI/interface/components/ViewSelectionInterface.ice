/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotComponents
* @author     David Schiebener (schiebener at kit dot edu)
* @author     Markus Grotz ( markus dot grotz at kit dot edu )
* @copyright  2015 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/core/PoseBase.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>

#include <ArmarXCore/interface/observers/Timestamp.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{

    const int DEFAULT_VIEWTARGET_PRIORITY = 50;

    ["cpp:virtual"]
    class ViewTargetBase
    {
        armarx::FramedPositionBase target;

        TimestampBase validUntil;

        TimestampBase timeAdded;

        float duration;

        int priority;

        string source;
    };

    ["cpp:virtual"]
    class SaliencyMapBase
    {
        string name;

        Ice::FloatSeq map;

        TimestampBase validUntil;

        TimestampBase timeAdded;
    };

    sequence<ViewTargetBase> ViewTargetList;

    interface ViewSelectionInterface
    {
        void addManualViewTarget(armarx::FramedPositionBase target);
        void clearManualViewTargets();
        ViewTargetList getManualViewTargets();

        void activateAutomaticViewSelection();
        void deactivateAutomaticViewSelection();
        bool isEnabledAutomaticViewSelection();

        void updateSaliencyMap(SaliencyMapBase map);
        void removeSaliencyMap(string name);
        ::Ice::StringSeq getSaliencyMapNames();

        void drawSaliencySphere(::Ice::StringSeq names);
        void clearSaliencySphere();
    };



    interface ViewSelectionObserver
    {
        void onActivateAutomaticViewSelection();
        void onDeactivateAutomaticViewSelection();
        void nextViewTarget(long timestamp);
    };

};




