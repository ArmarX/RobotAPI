/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Adrian Knobloch ( adrian dot knobloch at student dot kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/interface/core/BasicVectorTypes.ice>

module armarx
{
    interface FrameTrackingInterface
    {
        void enableTracking(bool enable);
        void setFrame(string frameName);
        ["cpp:const"]
        idempotent
        string getFrame();

        void lookAtFrame(string frameName);
        void lookAtPointInGlobalFrame(Vector3f point);
        bool isLookingAtPointInGlobalFrame(Vector3f point, float max_diff);
        void lookAtPointInRobotFrame(Vector3f point);

        void scanInConfigurationSpace(float yawFrom, float yawTo, ::Ice::FloatSeq pitchValues, float velocity);
        void scanInWorkspace(::armarx::Vector3fSeq points, float maxVelocity, float acceleration);
        void moveJointsTo(float yaw, float pitch);
    };
};
