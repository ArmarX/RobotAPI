/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

module armarx
{
    enum RobotHealthState
    {
        HealthOK, HealthWarning, HealthError
    };

    struct RobotHealthHeartbeatArgs
    {
        int maximumCycleTimeWarningMS = 100;
        int maximumCycleTimeErrorMS = 200;
        string message;
    };

    interface RobotHealthInterface
    {
        void heartbeat(string componentName, RobotHealthHeartbeatArgs args);
        void unregister(string componentName);
    };

    interface AggregatedRobotHealthInterface
    {
        void aggregatedHeartbeat(RobotHealthState overallHealthState);
    };

    interface RobotHealthComponentInterface extends RobotHealthInterface
    {
        string getSummary();
    };
};
