
#pragma once

#include <RobotAPI/interface/units/UnitInterface.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/serialization/Eigen.ice>

/**
*	Messages that can also be found in ROS package 'geometry_msgs'
*	http://docs.ros.org/en/jade/api/geometry_msgs/html/index-msg.html
*/
module armarx
{	
    
    struct FrameHeader
	{
		string parentFrame; // base frame 
		string frame;	    // child frame

		string agent;       // the robot

        long timestampInMicroSeconds = 0;
	}

	struct TransformStamped
	{
		FrameHeader header;
		Eigen::Matrix4f transform; // [mm]
	}

	struct Twist
	{
		// Linear (x,y,z) and angular (roll, pitch, yaw) velocities
		Eigen::Vector3f linear;  //  [mm/s]
		Eigen::Vector3f angular; //  [rad/s]
	}

	struct TwistStamped
	{
		FrameHeader header;
		Twist twist;
	}

} // module armarx