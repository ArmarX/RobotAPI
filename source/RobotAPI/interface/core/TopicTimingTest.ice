#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{
module topic_timing
{
    struct SmallData
    {
        long sentTimestamp = 0;
    };

    sequence <float> FloatSeq;

    struct BigData
    {
        long sentTimestamp = 0;

        Ice::FloatSeq data;
    };

    interface Topic
    {
        void reportSmall(SmallData data);

        void reportBig(BigData data);
    };

};
};
