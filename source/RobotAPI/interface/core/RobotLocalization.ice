/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Fabian Reister (fabian dot reister at kit dot edu)
 * @copyright  2021
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/armem/server/MemoryInterface.ice>
#include <RobotAPI/interface/core/GeometryBase.ice>
#include <RobotAPI/interface/core/PoseBase.ice>

module armarx{
    interface GlobalRobotPoseProvider extends armem::server::MemoryInterface 
    {
        // timestamp in microseconds
        PoseBase getGlobalRobotPose(long timestamp, string robotName);
    }

    interface GlobalRobotPoseLocalizationListener
    {
        void reportGlobalRobotPose(TransformStamped currentPose);
    }

    interface GlobalRobotPoseLocalizationCorrectionListener
    {
        void reportGlobalRobotPoseCorrection(TransformStamped global_T_odom);
    }
    
    interface OdometryListener
    {
        void reportOdometryVelocity(TwistStamped platformVelocity);
        void reportOdometryPose(TransformStamped odometryPose);
    }

} // module armarx
