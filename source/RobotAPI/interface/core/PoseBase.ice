/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI
* @author     Stefan Ulbrich, Nikolaus Vahrenkamp
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/Filters.ice>

module armarx
{
	/**
	* [Vector2Base] defines a 2D vector with x and y.
	*/
    class Vector2Base extends VariantDataClass
    {
        float x = zeroInt;
        float y = zeroInt;
    };
    /**
    * List of [Vector3Base].
    */
    sequence<Vector2Base> Vector2BaseList;

    /**
	* [Vector3Base] defines a 3D vector with x, y, and z.
	*/
    class Vector3Base extends VariantDataClass
    {
        float x = zeroInt;
        float y = zeroInt;
        float z = zeroInt;
    };
	/**
	* List of [Vector3Base].
	*/
    sequence<Vector3Base> Vector3BaseList;

	/**
	* [QuaternionBase] defines a quaternion with
	* @param qw Rotation angle.
	* @param qx x-coordinate of normalized rotation axis.
	* @param qy y-coordinate of normalized rotation axis.
	* @param qz z-coordinate of normalized rotation axis.
	*/
    class QuaternionBase extends VariantDataClass
    {
        float qw = zeroInt;
        float qx = zeroInt;
        float qy = zeroInt;
        float qz = zeroInt;
    };

	/**
	* [PoseBase] defines a pose with position in [Vector3Base] and orientation in [QuaternionBase].
	* @param position Position as 3D vector in x, y, and z [Vector3Base].
	* @param orientation Orientation as quaternion [QuaternionBase].
	*/
    ["cpp:virtual"]
    class PoseBase extends VariantDataClass
    {
        Vector3Base position;
        QuaternionBase orientation;
    };
	/**
	* List of [PoseBase].
	*/
    sequence<PoseBase> PoseBaseList;

};

