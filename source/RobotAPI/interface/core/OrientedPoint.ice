/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI
* @author     Martin Miller
* @copyright  2015 Humanoids Group, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/Filters.ice>

module armarx
{
    /**
    * [PointsWithNormalBase] defines a 3D point with normal.
    */
    class OrientedPointBase extends VariantDataClass
    {
        float px = zeroInt;
        float py = zeroInt;
        float pz = zeroInt;

        float nx = zeroInt;
        float ny = zeroInt;
        float nz = zeroInt;
    };

    /**
    * [FramedOrientedPointBase] defines a OrientedPoint with regard to an agent and a frame within the agent.
    * If agent is empty and frame is set to armarx::GlobalFrame, OrientedPoint is defined in global frame.
    * @param frame Name of frame.
    * @param frame Name of agent.
    */
    ["cpp:virtual"]
    class FramedOrientedPointBase extends OrientedPointBase
    {
        string frame;
        string agent;
    };


};

