/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI
* @author     Mirko Waechter
* @copyright  2016 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{
    sequence<DoubleSeqSeq> DoubleSeqSeqSeq;

    struct LimitlessState
    {
        bool enabled;
        double limitLo;
        double limitHi;
    };

    sequence<LimitlessState> LimitlessStateSeq;

        /**
        * [TrajectoryBase] defines a n-dimension trajectory with n deriviations.
        *
        */
    ["cpp:virtual"]
    class TrajectoryBase extends VariantDataClass
    {
        ["protected"]
        Ice::StringSeq dimensionNames;
        /**
         * first vector dimension is the frame of the trajectory
         * second vector dimension is dimension of trajectory (e.g. which joint).
         * third vector dimension is the derivation of the trajectory (position, velocity, acceleration)
         * data might be incomplete (not all dimension 3 vector might have values)
        */
        ["protected"] DoubleSeqSeqSeq dataVec;
        ["protected"] Ice::DoubleSeq timestamps;

        ["protected"] LimitlessStateSeq limitless;
    };


};

