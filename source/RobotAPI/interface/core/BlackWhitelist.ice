/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemory
* @author     Kai Welke ( welke at kit dot edu), Alexey Kozlov ( kozlov at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <Ice/BuiltinSequences.ice>


module armarx
{

    struct StringListUpdate
    {
        bool clear = false;
        Ice::StringSeq add;
        Ice::StringSeq set;
    };

    struct BlackWhitelistUpdate
    {
        StringListUpdate blacklist;
        StringListUpdate whitelist;
    };


    interface BlackWhitelistInterface
    {
        void updateBlackWhitelist(BlackWhitelistUpdate update);
    }

    interface BlackWhitelistTopic
    {
        void updateBlackWhitelist(BlackWhitelistUpdate update);
    }
};

