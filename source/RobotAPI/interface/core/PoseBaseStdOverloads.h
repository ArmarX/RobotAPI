/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::RobotAPI
 * @author     Raphael Grimm
 * @copyright  2017
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <cmath>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

namespace std
{
    inline bool isfinite(const armarx::Vector3BasePtr& v)
    {
        return v && isfinite(v->x) && isfinite(v->y) & isfinite(v->z);
    }
    inline bool isfinite(const armarx::Vector2BasePtr& v)
    {
        return v && isfinite(v->x) && isfinite(v->y);
    }
    inline bool isfinite(const armarx::QuaternionBasePtr& q)
    {
        return q && isfinite(q->qw) && isfinite(q->qx) && isfinite(q->qy) && isfinite(q->qz);
    }
    inline bool isfinite(const armarx::PoseBasePtr p)
    {
        return p && p->position && isfinite(p->position) && p->orientation && isfinite(p->orientation);
    }
}

