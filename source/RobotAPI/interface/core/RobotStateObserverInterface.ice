/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Stefan Ulbrich (stefan dot ulbrich at kit dot edu)
* @author     Kai Welke (welke at kit dot edu)
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/units/KinematicUnitInterface.ice>

#include <ArmarXCore/interface/observers/ObserverInterface.ice>

module armarx
{
    ["amd"] interface RobotStateObserverInterface extends ObserverInterface
    {
        ["cpp:const"]
        idempotent DatafieldRefBase getPoseDatafield(string nodeName);
    };
};

