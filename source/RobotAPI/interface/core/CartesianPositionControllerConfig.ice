/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI
* @author     Martin Miller
* @copyright  2015 Humanoids Group, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/Filters.ice>

module armarx
{
    /**
    * [CartesianPositionControllerConfigBase] defines the config for CartesianPositionController
    */
    class CartesianPositionControllerConfigBase extends VariantDataClass
    {
        float KpPos = 1;
        float KpOri = 1;
        float maxPosVel = -1;
        float maxOriVel = -1;

        float thresholdOrientationNear = -1;
        float thresholdOrientationReached = -1;
        float thresholdPositionNear = -1;
        float thresholdPositionReached = -1;
    };



};

