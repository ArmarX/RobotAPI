/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI
* @author     Simon Ottenhaus
* @copyright  2020 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

//#include <ArmarXCore/interface/serialization/Eigen.ice>
#include <ArmarXCore/interface/core/BasicVectorTypes.ice>

module armarx
{
    struct CartesianNaturalPositionControllerConfig
    {
        float maxPositionAcceleration       = 500;
        float maxOrientationAcceleration    = 1;
        float maxNullspaceAcceleration      = 2;

        Ice::FloatSeq maxJointVelocity;
        Ice::FloatSeq maxNullspaceVelocity;
        Ice::FloatSeq jointCosts;

        float jointLimitAvoidanceKp         = 0.1;
        float elbowKp                       = 1;
        float nullspaceTargetKp             = 1;

        float thresholdOrientationNear      = 0.1;
        float thresholdOrientationReached   = 0.05;
        float thresholdPositionNear         = 50;
        float thresholdPositionReached      = 5;


        float maxTcpPosVel = 80;
        float maxTcpOriVel = 1;
        float maxElbPosVel = 80;
        float KpOri     = 1;
        float KpPos     = 1;
    };
};


