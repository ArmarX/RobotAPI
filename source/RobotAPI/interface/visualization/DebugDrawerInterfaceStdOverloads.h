/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::RobotAPI
 * @author     Raphael Grimm
 * @copyright  2017
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <cmath>

namespace std
{
    inline bool isfinite(const armarx::DrawColor& c)
    {
        return isfinite(c.r) && isfinite(c.g) && isfinite(c.b) && isfinite(c.a);
    }

    inline bool isfinite(const armarx::DebugDrawerPointCloudElement& e)
    {
        return isfinite(e.x) && isfinite(e.y) && isfinite(e.z);
    }
}
namespace armarx
{
    inline std::ostream& operator<<(std::ostream& o, const DrawColor& c)
    {
        o << "(r= " << c.r << ", g= " << c.g << ", b= " << c.b << ", a= " << c.a << ")";
        return o;
    }
    inline std::ostream& operator<<(std::ostream& o, const DebugDrawerPointCloudElement& e)
    {
        o << "(x = " << e.x << ", y = " << e.y << ", z = " << e.z << ")";
        return o;
    }
}

