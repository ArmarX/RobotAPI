/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Core
 * @author     Nikolaus Vahrenkamp  (vahrenkamp at kit dot edu)
 * @copyright  2016
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/UnitInterface.ice>
#include <RobotAPI/interface/core/PoseBase.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{
    /**
    * Implements an interface to a RobotPoseUnit.
    * This unit is supposed to move a robot around in 6D (@see PlatformUnit is a similar unit, but with the restriction to move the robot in the plane).
    * In particular useful for simulated robots.
    **/
    interface RobotPoseUnitInterface extends SensorActorUnitInterface
    {
        /**
        * moveTo moves the robot to a global pose specified by:
        * @param targetPose Global target pose
        * @param postionalAccuracy Robot stops translating if distance to target position gets lower than this threshhold.
        * @param orientationalAccuracy Robot stops rotating if distance from current to target orientation gets lower than this threshhold.
        **/
        void moveTo(PoseBase targetPose, float positionalAccuracy, float orientationalAccuracy);
        /**
        * move moves the robot with given velocities.
        * @param targetVelocity target velocity defined in gloabl coordinates.
        **/
        void move(PoseBase targetVelocity);
        /**
        * moveRelative moves to a pose defined in robot's local coordinates.
        * @param relativeTarget relative targe in robot's coordinate system
        * @param postionalAccuracy Platform stops translating if distance to target position gets lower than this threshhold.
        * @param orientationalAccuracy Platform stops rotating if distance from current to target orientation gets lower than this threshhold.
        **/
        void moveRelative(PoseBase relativeTarget, float positionalAccuracy, float orientationalAccuracy);
        /**
        * setMaxVelocities allows to specify max velocities in translation and orientation.
        * @param positionalVelocity Max translation velocity.
        * @param orientationalVelocity Max orientational velocity.
        **/
        void setMaxVelocities(float positionalVelocity, float orientationalVelocity);
        /**
        * stopMovement stops the movements of the robot.
        **/
        void stopMovement();
    };
    /**
    * Implements an interface to an RobotPoseUnitListener.
    **/
    interface RobotPoseUnitListener
    {
        /**
        * reportRobotPose reports current robot pose.
        * @param currentRobotPose Global pose of the robot.
        **/
        void reportRobotPose(PoseBase currentRobotPose);
        /**
        * reportNewTargetPose reports a newly set target pose of the robot.
        * @param newRobotPose Global target pose.
        **/
        void reportNewTargetPose(PoseBase newRobotPose);
        /**
        * reportRobotVelocity reports current robot velocities.
        * @param currentRobotvelocity Current velocity on global frame.
        **/
        void reportRobotVelocity(PoseBase currentRobotvelocity);
    };

};

