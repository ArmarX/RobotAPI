/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mmmCapture
 * @author     Julia Starke <julia dot starke at kit dot edu>
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_MMMCAPTURE_CYBER_GLOVE_INTERFACE_SLICE_
#define _ARMARX_MMMCAPTURE_CYBER_GLOVE_INTERFACE_SLICE_

module armarx
{
    struct CyberGloveValues
    {
        string name;
        string time;
        int thumbCMC;
        int thumbMCP;
        int thumbIP;
        int thumbAbd;
        int indexMCP;
        int indexPIP;
        int indexDIP;
        int middleMCP;
        int middlePIP;
        int middleDIP;
        int middleAbd;
        int ringMCP;
        int ringPIP;
        int ringDIP;
        int ringAbd;
        int littleMCP;
        int littlePIP;
        int littleDIP;
        int littleAbd;
        int palmArch;
        int wristFlex;
        int wristAbd;
    };

    interface CyberGloveListenerInterface
    {
        void reportGloveValues(CyberGloveValues gloveValues);
        /*void reportMotorValues(string name, float position1, float pwm1, float position2, float pwm2);*/
    };

};

#endif
