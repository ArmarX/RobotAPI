/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     David Schiebener <schiebener at kit dot edu>
 * @copyright  2014 David Schiebener
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/UnitInterface.ice>
#include <RobotAPI/interface/core/RobotState.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>

module armarx
{
	/**
	* Implements an interface to a HeadIKUnit.
	**/
    interface HeadIKUnitInterface extends armarx::SensorActorUnitInterface
    {
		/**
		* setCycleTime allows to set the cycle time with which the head IK is solved and the head is controlled within a periodic task.
		* @param milliseconds Cycle time in milliseconds.
		**/
        void setCycleTime(int milliseconds);
        /**
		* setHeadTarget allows to set a new target position for the head.
		* @param robotNodeSetName Name of kinematic chain/nodeset with head as TCP.
		* @param targetPosition in x, y, and z.
		**/
        void setHeadTarget(string robotNodeSetName, FramedPositionBase targetPosition);

    };



    interface HeadIKUnitListener
    {
        void reportHeadTargetChanged(NameValueMap targetJointAngles, FramedPositionBase targetPosition);
    };

};

