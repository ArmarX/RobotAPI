/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Lukas Kaul ( lukas dot kaul at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


module armarx
{
    sequence<float> FloatSeq;

    struct MetaWearIMUDataExt {
        FloatSeq orientationQuaternion;
        FloatSeq magnetic;
        FloatSeq gyro;
        FloatSeq acceleration;
    };

    interface MetaWearIMUInterface
    {
        void reportIMUValues(string name, MetaWearIMUDataExt data);
    };
};

