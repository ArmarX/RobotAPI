/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Martin Do ( martin dot do at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/core/PoseBase.ice>
#include <RobotAPI/interface/units/UnitInterface.ice>
#include <ArmarXCore/interface/core/UserException.ice>

module armarx
{
    /**
     * Will be thrown, if a ATINetFT error occurs.
     */
    exception ATINetFTException extends UserException
    {
        /**
         * The ATINetFT error code as defined in the ATINetFTDataStreamSDK.
         * Note that "no error" or "success" is represented with the
         * result code 2. The other result codes are error codes.
         */
        int errorCode;
    };


    interface ATINetFTUnitInterface extends SensorActorUnitInterface
    {

        void startRecording(string customName);

        void stopRecording();

        bool isComponentOnline();
    };
};

