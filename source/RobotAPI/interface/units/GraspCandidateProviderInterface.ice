/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotAPI
* @author     Simon Ottenhaus
* @copyright  2019 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/RequestableService.ice>
#include <RobotAPI/interface/objectpose/object_pose_types.ice>

module armarx
{
    module grasping
    {
        // Grasping will now use ObjectType from armarx::objpose in <RobotAPI/interface/objectpose/object_pose_types.ice>
        /*enum ObjectTypeEnum {
            AnyObject, KnownObject, UnknownObject
        };*/

        enum ApproachType {
            AnyApproach, TopApproach, SideApproach
        };
        enum ApertureType {
            AnyAperture, OpenAperture, PreshapedAperture
        };

        class BoundingBox
        {
            Vector3Base center;
            Vector3Base ha1; // HalfAxis1 : longest half axis
            Vector3Base ha2; // HalfAxis2 : middle half axis
            Vector3Base ha3; // HalfAxis3 : shortest half axis
        };

        class GraspCandidateSourceInfo
        {
            PoseBase referenceObjectPose;
            string referenceObjectName;
            int segmentationLabelID = -1;
            BoundingBox bbox;
        };
        class GraspCandidateExecutionHints
        {
            ApertureType preshape = AnyAperture;
            ApproachType approach = AnyApproach;
            string graspTrajectoryName;
            //string graspTrajectoryPackage;
        };
        class GraspCandidateReachabilityInfo
        {
            bool reachable = false;
            float minimumJointLimitMargin = -1;
            Ice::FloatSeq jointLimitMargins;
            float maxPosError = 0;
            float maxOriError = 0;
        };



        class GraspCandidate
        {
            PoseBase graspPose;
            PoseBase robotPose;
            PoseBase tcpPoseInHandRoot;
            Vector3Base approachVector;

            string sourceFrame;  // frame where graspPose is located
            string targetFrame;  // frame which should be moved to graspPose
            string side;

            float graspSuccessProbability;

            armarx::objpose::ObjectType objectType = AnyObject;
            int groupNr = -1; // used to match candidates that belog together, e.g. from the same object or point cloud segment
            string providerName;

            GraspCandidateSourceInfo sourceInfo; // (optional)
            GraspCandidateExecutionHints executionHints; // (optional)
            GraspCandidateReachabilityInfo reachabilityInfo; // (optional)
        };

        class BimanualGraspCandidate
        {
            PoseBase graspPoseRight;
            PoseBase graspPoseLeft;
            PoseBase robotPose;
            PoseBase tcpPoseInHandRootRight;
            PoseBase tcpPoseInHandRootLeft;

            Vector3Base approachVectorRight;
            Vector3Base approachVectorLeft;

            Vector3Base inwardsVectorRight;
            Vector3Base inwardsVectorLeft;

            string sourceFrame;  // frame where graspPose is located
            string targetFrame;  // frame which should be moved to graspPose

            //float graspSuccessProbability;

            armarx::objpose::ObjectType objectType = AnyObject;
            int groupNr = -1; // used to match candidates that belog together, e.g. from the same object or point cloud segment
            string providerName;

            GraspCandidateSourceInfo sourceInfo; // (optional)
            GraspCandidateExecutionHints executionHintsRight; // (optional)
            GraspCandidateExecutionHints executionHintsLeft; // (optional)
            GraspCandidateReachabilityInfo reachabilityInfoRight; // (optional)
            GraspCandidateReachabilityInfo reachabilityInfoLeft; // (optional)


            string graspName;
        };

        sequence<GraspCandidate> GraspCandidateSeq;
        dictionary<string, GraspCandidate> GraspCandidateDict;
        sequence<BimanualGraspCandidate> BimanualGraspCandidateSeq;


        class ProviderInfo
        {
            armarx::objpose::ObjectType objectType = AnyObject;
            StringVariantBaseMap currentConfig;
        };

        dictionary<string, ProviderInfo> InfoMap;


        interface GraspCandidatesTopicInterface
        {
            void reportProviderInfo(string providerName, ProviderInfo info);
            void reportGraspCandidates(string providerName, GraspCandidateSeq candidates);
            void reportBimanualGraspCandidates(string providerName, BimanualGraspCandidateSeq candidates);

        };

        interface GraspCandidateProviderInterface extends RequestableServiceListenerInterface
        {
        };
    };

};

