/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Mirko Waechter <waechter at kit dot edu>
 * @copyright  2013 Mirko Waechter
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/UnitInterface.ice>
#include <RobotAPI/interface/core/RobotState.ice>
#include <RobotAPI/interface/core/FramedPoseBase.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>

module armarx
{
	/**
	* Implements an interface to an TCPControlUnit. The TCPControlUnit uses differential IK in order to move the TCP a target pose.
	**/
    interface TCPControlUnitInterface extends armarx::SensorActorUnitInterface, armarx::KinematicUnitListener
    {
		/**
		* setCycleTime allows to set the cycle time with which the TCPControlUnit solves the IK and moves the TCP within a periodic task.
		* @param milliseconds Cycle time in milliseconds.
		**/
        void setCycleTime(int milliseconds);
        /**
		* setTCPVelocity allows to set a new target velocities in task space for the TCP.
		* @param robotNodeSetName Name of kinematic chain/nodeset.
		* @param tcpNodeName Name of TCP node within the kinematic chain/nodeset.
		* @param translationVelocity Translational velocity in task space in x, y, and z.
		* @param orientationVelocityRPY Orientational velocity in task space in roll, pitch, yaw.
		**/
        void setTCPVelocity(string robotNodeSetName, string tcpNodeName, FramedDirectionBase translationVelocity, FramedDirectionBase orientationVelocityRPY);

        /**
         * @brief returns if the TCPControlUnit is requested.
         * @return Request status.
         */
        bool isRequested();
    };

	/**
	* [FramedPoseBaseMap] defines a data container which contains nodes, identified by name, and corresponding FramedPoses.
	**/
    dictionary<string, FramedPoseBase> FramedPoseBaseMap;
    /**
	* Implements an interface to an TCPControlUnitListener. 
	**/
    interface TCPControlUnitListener
    {
		/**
		* reportTCPPose reports TCPs and corresponding current FramedPoses.
		* @param tcpPoses Map of TCP node names and corresponding current FramedPoses. 
		**/
        void reportTCPPose(FramedPoseBaseMap tcpPoses);
        /**
		* reportTCPVelocities reports current velocities of TCPs.
		* @param tcpTranslationVelocities Map of TCP node names and corresponding current translational velocities. 
		* @param tcpOrienationVelocities Map of TCP node names and corresponding current orientational velocities. 
		**/
        void reportTCPVelocities(FramedDirectionMap tcpTranslationVelocities, FramedDirectionMap tcpOrienationVelocities);

    };
	/**
	* Implements an interface to an TCPControlUnitObserver. 
	**/
    interface TCPControlUnitObserverInterface extends ObserverInterface, TCPControlUnitListener
    {

    };

};
