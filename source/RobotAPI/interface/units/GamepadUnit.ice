/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Markus Grotz <markus dot grotz at kit dot edu>
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <RobotAPI/interface/units/UnitInterface.ice>
#include <RobotAPI/interface/core/RobotState.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/Matrix.ice>
#include <ArmarXCore/interface/observers/Timestamp.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>



module armarx
{
    struct GamepadData {
        float leftStickX;
        float leftStickY;
        float rightStickX;
        float rightStickY;
        float dPadX;
        float dPadY;
        float leftTrigger;
        float rightTrigger;

        bool leftButton;
        bool rightButton;
        bool backButton;
        bool startButton;
        bool xButton;
        bool yButton;
        bool aButton;
        bool bButton;
        bool theMiddleButton;
        bool leftStickButton;
        bool rightStickButton;

    };
    interface GamepadUnitInterface extends armarx::SensorActorUnitInterface
    {
    };

    interface GamepadUnitListener
    {	
        void reportGamepadState(string device, string name, GamepadData values, TimestampBase timestamp);
    };
	/**
	* Implements an interface to an GamepadUnitObserver.
	**/
    interface GamepadUnitObserverInterface extends ObserverInterface, GamepadUnitListener
    {
    };

};

