/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Core
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @copyright  2013 Manfred Kroehnert
 * @license    http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/UnitInterface.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <RobotAPI/interface/core/RobotLocalization.ice>

#include <RobotAPI/interface/core/GeometryBase.ice>

module armarx
{	
	
    interface LocalizationUnitInterface extends SensorActorUnitInterface
    {
        void reportGlobalRobotPoseCorrection(TransformStamped global_T_odom);
    };

    interface LocalizationUnitListener
    {
       
    };

    interface LocalizationSubUnitInterface extends LocalizationUnitInterface
    {

    };

};
