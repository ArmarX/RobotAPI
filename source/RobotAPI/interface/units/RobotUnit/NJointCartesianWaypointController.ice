/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI
* @author     Raphael Grimm
* @copyright  2019 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/serialization/Eigen.ice>

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/FTSensorValue.ice>
#include <RobotAPI/interface/core/CartesianWaypointControllerConfig.ice>

module armarx
{
    struct NJointCartesianWaypointControllerRuntimeConfig
    {
        CartesianWaypointControllerConfig wpCfg;
        float forceThreshold = -1; // < 0 -> no limit
        bool forceThresholdInRobotRootZ = true;
        bool optimizeNullspaceIfTargetWasReached = false;
        bool skipToClosestWaypoint = true;
    };

    class NJointCartesianWaypointControllerConfig extends NJointControllerConfig
    {
        string rns;
        NJointCartesianWaypointControllerRuntimeConfig runCfg;
        string ftName; //optional
        string referenceFrameName; // optional
    };

    interface NJointCartesianWaypointControllerInterface extends NJointControllerInterface
    {
        idempotent bool hasReachedTarget();
        idempotent bool hasReachedForceLimit();
        idempotent int getCurrentWaypointIndex();

        void setConfig(NJointCartesianWaypointControllerRuntimeConfig cfg);
        void setWaypoints(Eigen::Matrix4fSeq wps);
        void setWaypoint(Eigen::Matrix4f wp);
        void setWaypointAx(Ice::FloatSeq wp);
        void setConfigAndWaypoints(NJointCartesianWaypointControllerRuntimeConfig cfg, Eigen::Matrix4fSeq wps);
        void setConfigAndWaypoint(NJointCartesianWaypointControllerRuntimeConfig cfg, Eigen::Matrix4f wp);
        
        FTSensorValue getFTSensorValue();
        void setCurrentFTAsOffset();
        void stopMovement();

        void setVisualizationRobotGlobalPose(Eigen::Matrix4f p);
        void resetVisualizationRobotGlobalPose();
    };
};
