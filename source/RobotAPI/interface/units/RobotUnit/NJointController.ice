/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXGui/interface/WidgetDescription.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{
    interface NJointControllerInterface;


    module RobotUnitControllerNames
    {
        const string NJointTrajectoryController = "NJointTrajectoryController";
        const string NJointGlobalTCPController = "NJointGlobalTCPController"; /*@@@TODO: move NJointGlobalTCPController to RobotAPI */
        const string NJointTCPController = "NJointTCPController";
        const string NJointCartesianVelocityController = "NJointCartesianVelocityController";
    };


    class NJointControllerConfig {};

    struct NJointControllerDescription
    {
        string instanceName;
        string className;
        NJointControllerInterface* controller;
        StringStringDictionary controlModeAssignment;
        bool deletable;
        bool internal;
    };
    sequence<NJointControllerDescription> NJointControllerDescriptionSeq;

    struct NJointControllerStatus
    {
        string instanceName;
        bool active = false;
        bool requested = false;
        bool error = false;
        long timestampUSec = 0;
    };
    sequence<NJointControllerStatus> NJointControllerStatusSeq;

    struct NJointControllerDescriptionWithStatus
    {
        NJointControllerStatus status;
        NJointControllerDescription description;
    };
    sequence<NJointControllerDescriptionWithStatus> NJointControllerDescriptionWithStatusSeq;

    interface NJointControllerInterface
    {
        ["cpp:const"] idempotent string getClassName();
        ["cpp:const"] idempotent string getInstanceName();
        ["cpp:const"] idempotent StringStringDictionary getControlDeviceUsedControlModeMap();
        ["cpp:const"] idempotent bool isControllerActive();
        ["cpp:const"] idempotent bool isControllerRequested();
        ["cpp:const"] idempotent bool isDeletable();
        ["cpp:const"] idempotent bool hasControllerError();
        ["cpp:const"] idempotent NJointControllerStatus getControllerStatus();
        ["cpp:const"] idempotent NJointControllerDescription getControllerDescription();
        ["cpp:const"] idempotent NJointControllerDescriptionWithStatus getControllerDescriptionWithStatus();
        // This method is not used by anybody and just produces cyclic dependencies
        // If you were able to create/get an NJointController, you know the RobotUnit already.
        //["cpp:const"] idempotent RobotUnitInterface* getRobotUnit();

        void activateController();
        void deactivateController();
        void deleteController() throws LogicError;
        void deactivateAndDeleteController() throws LogicError;

        ["cpp:const"] idempotent WidgetDescription::StringWidgetDictionary getFunctionDescriptions();
        void callDescribedFunction(string fuinctionName, StringVariantBaseMap values) throws InvalidArgumentException;
    };

    dictionary<string, NJointControllerInterface*> StringNJointControllerPrxDictionary;
};

