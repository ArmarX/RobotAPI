/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Sonja Marahrens ( sonja dot marahrens at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/CartesianSelection.ice>

module armarx
{

    class NJointCartesianVelocityControllerWithRampConfig extends NJointControllerConfig
    {
        string nodeSetName = "";
        string tcpName = "";
        CartesianSelectionMode::CartesianSelection mode = CartesianSelectionMode::eAll;
        float maxPositionAcceleration = 0;
        float maxOrientationAcceleration = 0;
        float maxNullspaceAcceleration = 0;
        float KpJointLimitAvoidance = 0;
        float jointLimitAvoidanceScale = 0;
    };

    interface NJointCartesianVelocityControllerWithRampInterface extends NJointControllerInterface
    {
        void setMaxAccelerations(float maxPositionAcceleration, float maxOrientationAcceleration, float maxNullspaceAcceleration);
        void setTargetVelocity(float vx, float vy, float vz, float vrx, float vry, float vrz);
        void immediateHardStop();
        void setJointLimitAvoidanceScale(float scale);
        void setKpJointLimitAvoidance(float KpJointLimitAvoidance);
    };
};
