/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/serialization/Eigen.ice>
#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>

module armarx
{
    class NJointTaskSpaceImpedanceControlConfig extends NJointControllerConfig
    {
        string nodeSetName;

        Eigen::Vector3f desiredPosition;
        Eigen::Quaternionf desiredOrientation;

        Eigen::Vector3f Kpos;
        Eigen::Vector3f Kori;
        Eigen::Vector3f Dpos;
        Eigen::Vector3f Dori;

        Eigen::VectorXf desiredJointPositions;
        Eigen::VectorXf Knull;
        Eigen::VectorXf Dnull;

        float torqueLimit;
    };
    struct NJointTaskSpaceImpedanceControlRuntimeConfig
    {
        Eigen::Vector3f Kpos;
        Eigen::Vector3f Kori;
        Eigen::Vector3f Dpos;
        Eigen::Vector3f Dori;

        Eigen::VectorXf desiredJointPositions;
        Eigen::VectorXf Knull;
        Eigen::VectorXf Dnull;

        float torqueLimit;
    };

    interface NJointTaskSpaceImpedanceControlInterface extends NJointControllerInterface
    {
        void setPosition(Eigen::Vector3f target);
        void setOrientation(Eigen::Quaternionf orientation);
        void setPositionOrientation(Eigen::Vector3f target, Eigen::Quaternionf orientation);
        void setPose(Eigen::Matrix4f mat);
        
        void setImpedanceParameters(string paraName, Ice::FloatSeq vals);
        void setNullspaceConfig(Eigen::VectorXf desiredJointPositions, Eigen::VectorXf Knull, Eigen::VectorXf Dnull);
        void setConfig(NJointTaskSpaceImpedanceControlRuntimeConfig cfg);
    };

};
