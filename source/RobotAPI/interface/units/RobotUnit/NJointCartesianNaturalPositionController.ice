/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI
* @author     Simon Ottenhaus
* @copyright  Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/serialization/Eigen.ice>

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>
#include <RobotAPI/interface/core/FTSensorValue.ice>
#include <RobotAPI/interface/core/CartesianNaturalPositionControllerConfig.ice>
#include <ArmarXCore/interface/core/BasicVectorTypes.ice>

module armarx
{

    class NJointCartesianNaturalPositionControllerConfig extends NJointControllerConfig
    {
        string rns;
        string elbowNode;
        int feedforwardVelocityTTLms = 100;
        CartesianNaturalPositionControllerConfig runCfg;
        string ftName;
        int ftHistorySize = 100;
    };

    interface NJointCartesianNaturalPositionControllerInterface extends NJointControllerInterface
    {
        //idempotent bool hasReachedTarget();
        //idempotent bool hasReachedForceLimit();

        void setConfig(CartesianNaturalPositionControllerConfig cfg);
        void setTarget(Eigen::Matrix4f tcpTarget, Eigen::Vector3f elbowTarget, bool setOrientation);
        void setTargetFeedForward(Eigen::Matrix4f tcpTarget, Eigen::Vector3f elbowTarget, bool setOrientation, Eigen::Vector6f ffVel);
        void setFeedForwardVelocity(Eigen::Vector6f vel);
        void clearFeedForwardVelocity();
        void setNullspaceTarget(Ice::FloatSeq nullspaceTarget);
        void clearNullspaceTarget();
        void setNullspaceControlEnabled(bool enabled);

        FTSensorValue getCurrentFTValue();
        FTSensorValue getAverageFTValue();
        void resetFTOffset();
        void setFTOffset(FTSensorValue offset);
        void setFTLimit(float force, float torque);
        void clearFTLimit();
        void setFakeFTValue(FTSensorValue ftValue);
        void clearFakeFTValue();
        bool isAtForceLimit();

        void stopMovement();

        void setVisualizationRobotGlobalPose(Eigen::Matrix4f p);
        void resetVisualizationRobotGlobalPose();
    };
};
