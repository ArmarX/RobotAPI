/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotAPI
* @author     Simon Ottenhaus
* @copyright  2019 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include <ArmarXCore/interface/observers/ObserverInterface.ice>
#include <RobotAPI/interface/units/GraspCandidateProviderInterface.ice>

module armarx
{
    module grasping
    {
        class CandidateFilterCondition
        {
            string providerName = "*";
            float minimumSuccessProbability = 0;
            armarx::objpose::ObjectType objectType = AnyObject;
            ApertureType preshape = AnyAperture;
            ApproachType approach = AnyApproach;

        };

        sequence<string> StringSeq;
        dictionary<string, int> IntMap;

        interface GraspCandidateObserverInterface extends ObserverInterface, GraspCandidatesTopicInterface
        {
            InfoMap getAvailableProvidersWithInfo();
            StringSeq getAvailableProviderNames();
            ProviderInfo getProviderInfo(string providerName);
            bool hasProvider(string providerName);
            GraspCandidateSeq getAllCandidates();
            GraspCandidateSeq getCandidatesByProvider(string providerName);
            GraspCandidateSeq getCandidatesByProviders(Ice::StringSeq providerNames);
            GraspCandidateSeq getCandidatesByFilter(CandidateFilterCondition filter);
            int getUpdateCounterByProvider(string providerName);
            IntMap getAllUpdateCounters();
            bool setProviderConfig(string providerName, StringVariantBaseMap config);

            // for execution
            void setSelectedCandidates(GraspCandidateSeq candidates);
            GraspCandidateSeq getSelectedCandidates();


            BimanualGraspCandidateSeq getAllBimanualCandidates();
            void setSelectedBimanualCandidates(BimanualGraspCandidateSeq candidates);
            BimanualGraspCandidateSeq getSelectedBimanualCandidates();

            void clearCandidatesByProvider(string providerName);

        };
    };

};

