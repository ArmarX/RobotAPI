/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI
* @author     Mirko Waechter (waechter at kit dot edu)
* @copyright  2015 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/Filters.ice>

module armarx
{
    module checks
    {
        const string magnitudelarger = "magnitudelarger";
        const string magnitudesmaller = "magnitudesmaller";
        const string magnitudeinrange = "magnitudeinrange";
    };

	/**
	* OffsetFilterBase implements a filter which stores the measurement in the first run as an offset.
	* This offset is subtracted from subsequent measurements.
	*/
    ["cpp:virtual"]
    class OffsetFilterBase extends DatafieldFilterBase
    {

    };
	/**
	* MatrixMaxFilterBase implements a filter which returns the maximum coefficient of a matrix.
	*/
    ["cpp:virtual"]
    class MatrixMaxFilterBase extends DatafieldFilterBase
    {

    };
	/**
	* MatrixMinFilterBase implements a filter which returns the minimum coefficient of a matrix.
	*/
    ["cpp:virtual"]
    class MatrixMinFilterBase extends DatafieldFilterBase
    {

    };
	
	/**
	* MatrixMinFilterBase implements a filter which returns the mean value of all coefficients of a matrix.
	*/
    ["cpp:virtual"]
    class MatrixAvgFilterBase extends DatafieldFilterBase
    {

    };
	/**
	* MatrixPercentileFilterBase implements a filter which returns a subset of the smallest coefficients of a matrix.
	* The matrix is transformed int a vector which is sorted in a ascending order. The subset consists of the first entries
	* of the sorted vector. The size of the subset is defined by parameter percentile which ranges from 0.0 to 1.0.
	*/
    ["cpp:virtual"]
    class MatrixPercentileFilterBase extends DatafieldFilterBase
    {
        float percentile;
    };

	/**
	* MatrixPercentilesFilterBase implements a filter which returns a subset of the smallest coefficients of a matrix.
	* The matrix is transformed int a vector which is sorted in a ascending order. The subset consists of the first entries
	* of the sorted vector. The size of the subset is defined by parameter percentiles.
	*/
    ["cpp:virtual"]
    class MatrixPercentilesFilterBase extends DatafieldFilterBase
    {
        int percentiles;
    };
    
	/**
	* MatrixCumulativeFrequencyFilterBase implements a filter which returns a matrix filtered by Cumulative Frequency filter.
	* The entries of the filtered matrix range between the provided min and max values.
	*/
    ["cpp:virtual"]
    class MatrixCumulativeFrequencyFilterBase extends DatafieldFilterBase
    {
        float min;
        float max;
        int bins;
    };

};

