/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotAPI
* @author     Rainer Kartmann
* @copyright  2020 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/core/BasicVectorTypes.ice>
#include <ArmarXCore/interface/core/time.ice>

#include <RobotAPI/interface/core/PoseBase.ice>
#include <RobotAPI/interface/core/NameValueMap.ice>
#include <RobotAPI/interface/ArmarXObjects/ArmarXObjectsTypes.ice>


module armarx
{
    // A struct's name cannot cannot differ only in capitalization from its immediately enclosing module name.
    module objpose
    {
        // ObjectTypeEnum is now renamed to ObjectType
        enum ObjectType
        {
            AnyObject, KnownObject, UnknownObject
        };



        class AABB
        {
            Vector3Base center;
            Vector3Base extents;
        };

        /**
         * @brief A 3D box.
         * Box is a a class to allow it being null.
         */
        class Box
        {
            Vector3Base position;
            QuaternionBase orientation;
            Vector3Base extents;
        };


        module data
        {
            /**
             * @brief A "Gaussian" distribution on the pose manifold.
             *
             * @see `armarx::objpose::PoseManifoldGaussian`
             */
            class PoseManifoldGaussian  // class => optional
            {
                PoseBase mean;
                Ice::FloatSeq covariance6x6;
            };


            /// An object pose provided by an ObjectPoseProvider.
            struct ProvidedObjectPose
            {
                /// The object ID, i.e. dataset, class name and instance name.
                armarx::data::ObjectID objectID;

                /// Name of the providing component.
                string providerName;
                /// Known or unknown object.
                ObjectType objectType = AnyObject;
                /// Whether object is static. Static objects don't decay.
                bool isStatic = false;

                /// Pose in `objectPoseFrame`.
                PoseBase objectPose;
                /// The frame in which the object was localized.
                string objectPoseFrame;
                /// Name of the robot if `objectPoseFrame` is not the global frame.
                string robotName;

                /// Object pose with uncertainty.
                PoseManifoldGaussian objectPoseGaussian;

                /// The object's joint values if it is articulated.
                NameValueMap objectJointValues;

                /// Confidence in [0, 1] (1 = full, 0 = none).
                float confidence = 0;
                /// Source timestamp.
                armarx::core::time::dto::DateTime timestamp;

                /// [Optional] Object bounding box in object's local coordinate frame.
                Box localOOBB;
            };
            sequence<ProvidedObjectPose> ProvidedObjectPoseSeq;


            class ObjectAttachmentInfo;

            /// An object pose as stored by the ObjectPoseStorage.
            struct ObjectPose
            {
                /// The object ID, i.e. dataset, class name and instance name.
                armarx::data::ObjectID objectID;

                /// Name of the providing component.
                string providerName;
                /// Known or unknown object.
                ObjectType objectType = AnyObject;
                /// Whether object is static. Static objects don't decay.
                bool isStatic = false;

                PoseBase objectPoseRobot;
                PoseManifoldGaussian objectPoseRobotGaussian;

                PoseBase objectPoseGlobal;
                PoseManifoldGaussian objectPoseGlobalGaussian;

                PoseBase objectPoseOriginal;
                string objectPoseOriginalFrame;
                PoseManifoldGaussian objectPoseOriginalGaussian;

                /// The object's joint values if it is articulated.
                NameValueMap objectJointValues;


                StringFloatDictionary robotConfig;
                PoseBase robotPose;
                string robotName;

                ObjectAttachmentInfo attachment;

                /// Confidence in [0, 1] (1 = full, 0 = none).
                float confidence = 0;
                /// Source timestamp.
                armarx::core::time::dto::DateTime timestamp;

                /// Object bounding box in object's local coordinate frame. May be null.
                Box localOOBB;
            };
            sequence<ObjectPose> ObjectPoseSeq;


            class ObjectAttachmentInfo
            {
                string frameName;
                string agentName;

                PoseBase poseInFrame;
            };
        }

    };
};

