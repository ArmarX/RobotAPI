/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotAPI
* @author     Rainer Kartmann
* @copyright  2020 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/observers/ObserverInterface.ice>

#include <RobotAPI/interface/armem/server/MemoryInterface.ice>

#include <RobotAPI/interface/objectpose/object_pose_types.ice>
#include <RobotAPI/interface/objectpose/ObjectPoseProvider.ice>


module armarx
{
    module objpose
    {

        module observer
        {
            struct RequestObjectsInput
            {
                /// The provider to request. If empty, any suitable provider per object ID is chosen.
                string provider;
                provider::RequestObjectsInput request;
            };
            struct ObjectRequestResult
            {
                /// Name of the provider who was requested.
                /// If empty, no suitable provider was found.
                string providerName;
                provider::ObjectRequestResult result;
            };
            dictionary<armarx::data::ObjectID, ObjectRequestResult> ObjectRequestResultMap;
            struct RequestObjectsOutput
            {
                /// The results per objectID.
                ObjectRequestResultMap results;
            };
        };

        struct AttachObjectToRobotNodeInput
        {
            /// The object ID.
            armarx::data::ObjectID objectID;
            /// Optional. If empty, all providers will be searched.
            string providerName;

            /// The frame (robot node) to attach to.
            string frameName;
            /// The agent's name.
            string agentName;

            /**
             * If given, specifies the object's pose in the frame.
             * If not given, the current object's pose is used.
             */
            PoseBase poseInFrame;
        };
        struct AttachObjectToRobotNodeOutput
        {
            bool success = false;
            data::ObjectAttachmentInfo attachment;
        };

        struct DetachObjectFromRobotNodeInput
        {
            string providerName;
            armarx::data::ObjectID objectID;

            /**
             * @brief If true, the object will stay at the position before
             * detaching until it is provided again.
             */
            bool commitAttachedPose = true;
        };
        struct DetachObjectFromRobotNodeOutput
        {
            /// Whether the object was attached before.
            bool wasAttached = false;
        };

        struct DetachAllObjectsFromRobotNodesInput
        {
            /**
             * @brief If true, the objects will stay at the position before
             * detaching until they are provided again.
             */
            bool commitAttachedPose = true;
        }
        struct DetachAllObjectsFromRobotNodesOutput
        {
            /// Number of objects that have been detached.
            int numDetached = 0;
        };

        struct AgentFrames
        {
            string agent;
            Ice::StringSeq frames;
        };
        sequence<AgentFrames> AgentFramesSeq;


        enum HeadMovementAction
        {
            /// Head movement is about to start.
            HeadMovementAction_Starting,
            /// Head movement is stopping.
            HeadMovementAction_Stopping
        };
        struct SignalHeadMovementInput
        {
            HeadMovementAction action;

            /**
             * If starting, for how long (after this call) new poses shall be discarded
             * due to head movement.
             * When set to -1 (default), the configured value in the component is used.
             *
             * If stopping, for how long (after this call) new poses shall be discarded
             * due to remaining head movement.
             * When set to -1 (default), resume processing updates immediately.
             */
            long discardUpdatesIntervalMilliSeconds = -1;
        };
        struct SignalHeadMovementOutput
        {
            /// The time until new updates will be discarded.
            long discardUpdatesUntilMilliSeconds = -1;
        };

        struct ObjectPosePredictionRequest
        {
            armarx::data::ObjectID objectID;
            armarx::core::time::dto::DateTime timestamp;
            armarx::core::time::dto::Duration timeWindow;
            armem::prediction::data::PredictionSettings settings;
        };
        sequence<ObjectPosePredictionRequest> ObjectPosePredictionRequestSeq;
        struct ObjectPosePredictionResult
        {
            bool success = false;
            string errorMessage;

            data::ObjectPose prediction;
        };
        sequence<ObjectPosePredictionResult> ObjectPosePredictionResultSeq;

        interface ObjectPoseStorageInterface extends
                ObjectPoseTopic
        {
            // Object poses

            data::ObjectPoseSeq getObjectPoses();
            data::ObjectPoseSeq getObjectPosesByProvider(string providerName);

            // Provider information

            bool hasProvider(string providerName);
            Ice::StringSeq getAvailableProviderNames();
            ProviderInfoMap getAvailableProvidersInfo();
            ProviderInfo getProviderInfo(string providerName);

            // Requesting

            observer::RequestObjectsOutput requestObjects(observer::RequestObjectsInput input);

            // Attaching

            /// Attach an object to a robot node.
            AttachObjectToRobotNodeOutput attachObjectToRobotNode(AttachObjectToRobotNodeInput input);
            /// Detach an attached object from a robot node.
            DetachObjectFromRobotNodeOutput detachObjectFromRobotNode(DetachObjectFromRobotNodeInput input);
            /// Detach all objects from robot nodes.
            DetachAllObjectsFromRobotNodesOutput detachAllObjectsFromRobotNodes(DetachAllObjectsFromRobotNodesInput input);

            AgentFramesSeq getAttachableFrames();


            /*
             * Notifying about head/camera movement.
             * While the head is moving (and a short time afterwards), pose updates are discarded
             * as they can be bad due to camera movement.
             */

            SignalHeadMovementOutput signalHeadMovement(SignalHeadMovementInput input);

            // Predicting

            // Request predictions for the poses of the given objects at the given timestamps.
            ObjectPosePredictionResultSeq
            predictObjectPoses(ObjectPosePredictionRequestSeq requests);

            // Get the prediction engines available for use with this storage interface.
            armem::prediction::data::PredictionEngineSeq getAvailableObjectPoseEngines();

        };
    };

};

