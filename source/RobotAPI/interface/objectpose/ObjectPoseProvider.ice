/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotAPI
* @author     Rainer Kartmann
* @copyright  2020 Humanoids Group, H2T, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>

#include <RobotAPI/interface/objectpose/object_pose_types.ice>


module armarx
{
    // A struct's name cannot cannot differ only in capitalization from its immediately enclosing module name.
    module objpose
    {
        interface ObjectPoseProvider;
        struct ProviderInfo
        {
            ObjectPoseProvider* proxy;
            ObjectType objectType = AnyObject;
            armarx::data::ObjectIDSeq supportedObjects;
        };
        dictionary<string, ProviderInfo> ProviderInfoMap;


        module provider
        {
            struct RequestObjectsInput
            {
                /// Object IDs.
                armarx::data::ObjectIDSeq objectIDs;
                /// For how long to request localization. Negative for no timeout.
                long relativeTimeoutMS;
            };

            struct ObjectRequestResult
            {
                bool success;
            };
            dictionary<armarx::data::ObjectID, ObjectRequestResult> ObjectRequestResultMap;
            struct RequestObjectsOutput
            {
                ObjectRequestResultMap results;
            };
        };
        interface ObjectPoseProvider
        {
            /// Get the provider info.
            ProviderInfo getProviderInfo();
            /// Request to track the specified objects for some time.
            provider::RequestObjectsOutput requestObjects(provider::RequestObjectsInput input);
        };

        interface ObjectPoseTopic
        {
            /// Signal that a new provider is now available.
            void reportProviderAvailable(string providerName, ProviderInfo info);

            /**
             * @brief Report new object poses.
             *
             * This will overwrite all object poses previously provided
             * with the same provider name.
             */
            void reportObjectPoses(string providerName, data::ProvidedObjectPoseSeq candidates);
        };
    };

};

