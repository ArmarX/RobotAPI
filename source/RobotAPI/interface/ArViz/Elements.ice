#pragma once

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/BasicVectorTypes.ice>


module armarx
{
module viz
{
module data
{
    struct GlobalPose
    {
        float x = 0.0f;
        float y = 0.0f;
        float z = 0.0f;
        float qw = 1.0f;
        float qx = 0.0f;
        float qy = 0.0f;
        float qz = 0.0f;
    };

    struct Color
    {
        byte a = 255;
        byte r = 100;
        byte g = 100;
        byte b = 100;
    };

    module InteractionEnableFlags
    {
        const int NONE           = 0;

        // Make an object selectable
        const int SELECT         = 1;
        // Enable the context menu for an object
        const int CONTEXT_MENU   = 2;

        // Enable translation along the three axes
        const int TRANSLATION_X  = 4;
        const int TRANSLATION_Y  = 8;
        const int TRANSLATION_Z  = 16;
        const int TRANSLATION_LOCAL = 32;

        // Enable rotation along the three axes
        const int ROTATION_X     = 64;
        const int ROTATION_Y     = 128;
        const int ROTATION_Z     = 256;
        const int ROTATION_LOCAL = 512;

        // Enable scaling along the three axes
        const int SCALING_X       = 1024;
        const int SCALING_Y       = 2048;
        const int SCALING_Z       = 4096;
        const int SCALING_LOCAL   = 8192;

        // Hide the original object during the transformation
        const int TRANSFORM_HIDE = 16384;
    };

    struct InteractionDescription
    {
        int enableFlags = 0;
        Ice::StringSeq contextMenuOptions;
    };

    module InteractionFeedbackType
    {
        const int NONE = 0;

        const int SELECT  = 1;
        const int DESELECT = 2;

        const int CONTEXT_MENU_CHOSEN = 3;

        const int TRANSFORM = 4;

        // Flag to indicate state of the transformation
        const int TRANSFORM_BEGIN_FLAG  = 16;
        const int TRANSFORM_DURING_FLAG = 32;
        const int TRANSFORM_END_FLAG    = 64;
    };

    struct InteractionFeedback
    {
        // The type of interaction that happened (in the lower 4 bits)
        // The higher bits are used for flags specifying more details of the interaction
        int type = 0;

        // The element with which the interaction took place
        string component;
        string layer;
        string element;
        // The revision in which the interaction took place
        long revision = 0;

        // Chosen context menu entry is only relevant for type == CONTEXT_MENU_CHOSEN
        int chosenContextMenuEntry = 0;

        // Applied transformation is only relevant for type == TRANSFORM
        GlobalPose transformation;
        Vector3f scale;
    };

    sequence<InteractionFeedback> InteractionFeedbackSeq;

    module ElementFlags
    {
        const int NONE = 0;
        const int OVERRIDE_MATERIAL = 1;
        const int HIDDEN = 2;
    };

    class Element
    {
        string id;
        InteractionDescription interaction;

        GlobalPose pose;
        Vector3f scale;
        Color color;
        int flags = 0;
    };

    class ElementPose extends Element
    {
    };

    class ElementLine extends Element
    {
        Vector3f from;
        Vector3f to;

        float lineWidth = 10.0f;
    };

    class ElementBox extends Element
    {
        Vector3f size;
    };

    class ElementSphere extends Element
    {
        float radius = 10.0f;
    };

    class ElementEllipsoid extends Element
    {
        Vector3f axisLengths;
        Vector3f curvature;
    };

    class ElementCylinder extends Element
    {
        float height = 10.0f;
        float radius = 10.0f;
    };

    class ElementCylindroid extends Element
    {
        Vector2f axisLengths;
        Vector2f curvature;
        float height;
    };

    class ElementText extends Element
    {
        string text;
    };

    class ElementPolygon extends Element
    {
        Vector3fSeq points;

        Color lineColor;
        float lineWidth = 0.0f;
    };

    class ElementPath extends Element
    {
        Vector3fSeq points;

        float lineWidth = 10.0f;
    };

    class ElementArrow extends Element
    {
        float length = 100.0f;
        float width = 10.0f;
    };

    class ElementArrowCircle extends Element
    {
        float radius = 100.0f;
        float completion = 1.0f;
        float width = 10.0f;
    };

    struct ColoredPoint
    {
        float x;
        float y;
        float z;
        Color color;
    };

    sequence<ColoredPoint> ColoredPointList;

    class ElementPointCloud extends Element
    {
        Ice::ByteSeq points;
        float transparency = 0.0f;
        float pointSizeInPixels = 1.0f;
    };

    sequence<Color> ColorSeq;

    struct Face
    {
        int v0 = 0;
        int v1 = 0;
        int v2 = 0;
        int c0 = 0;
        int c1 = 0;
        int c2 = 0;
    };

    sequence<Face> FaceSeq;

    class ElementMesh extends Element
    {
        Vector3fSeq vertices;
        ColorSeq colors;
        FaceSeq faces;
    };

    module ModelDrawStyle
    {
        const int ORIGINAL  = 0;
        const int COLLISION = 1;
        const int OVERRIDE_COLOR = 2;
    };

    class ElementRobot extends Element
    {
        string project;
        string filename;
        int drawStyle = ModelDrawStyle::ORIGINAL;

        StringFloatDictionary jointValues;
    };

    class ElementObject extends Element
    {
        string project;
        string filename;
        int drawStyle = ModelDrawStyle::ORIGINAL;
    };

};
};
};
