#pragma once

#include <RobotAPI/interface/ArViz/Elements.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{
module viz
{
module data
{

sequence <Element> ElementSeq;

enum LayerAction
{
    // Create a new layer or update an existing layer
    Layer_CREATE_OR_UPDATE,

    // Delete an existing layer
    Layer_DELETE,
};

struct LayerUpdate
{
    string component;
    string name;
    LayerAction action = Layer_CREATE_OR_UPDATE;

    // Elements are only needed for Layer_CREATE_OR_UPDATE
    ElementSeq elements;
};

sequence <LayerUpdate> LayerUpdateSeq;

struct TimestampedLayerUpdate
{
    LayerUpdate update;
    long revision = 0;
    long timestampInMicroseconds = 0;
};

struct LayerUpdates
{
    LayerUpdateSeq updates;
    long revision = 0;
};

struct CommitInput
{
    LayerUpdateSeq updates;

    string interactionComponent;
    Ice::StringSeq interactionLayers;
};

struct CommitResult
{
    // The revision number corresponding to the applied commit
    long revision = 0;

    // Interactions for the requested layers (see Layer_RECEIVE_INTERACTIONS)
    InteractionFeedbackSeq interactions;
};

struct RecordingHeader
{
    string prefix;
    string comment;
    long firstTimestampInMicroSeconds = 0;
    long lastTimestampInMicroSeconds = 0;
    long firstRevision = 0;
    long lastRevision = 0;
};

struct RecordingBatchHeader
{
    long index = 0;
    long firstTimestampInMicroSeconds = 0;
    long lastTimestampInMicroSeconds = 0;
    long firstRevision = 0;
    long lastRevision = 0;
};

sequence<RecordingBatchHeader> RecordingBatchHeaderSeq;

struct Recording
{
    string id;
    string comment;
    long firstTimestampInMicroSeconds = 0;
    long lastTimestampInMicroSeconds = 0;
    long firstRevision = 0;
    long lastRevision = 0;
    RecordingBatchHeaderSeq batchHeaders;
};

sequence<TimestampedLayerUpdate> TimestampedLayerUpdateSeq;

struct RecordingBatch
{
    RecordingBatchHeader header;
    TimestampedLayerUpdateSeq initialState; // Keyframe with complete state
    TimestampedLayerUpdateSeq updates; // Incremental updates on keyframe
};

sequence<Recording> RecordingSeq;

};

interface StorageInterface
{
    data::CommitResult commitAndReceiveInteractions(data::CommitInput input);

    data::LayerUpdates pullUpdatesSince(long revision);

    data::LayerUpdates pullUpdatesSinceAndSendInteractions(
                long revision, data::InteractionFeedbackSeq interactions);

    string startRecording(string prefix);

    void stopRecording();

    data::RecordingSeq getAllRecordings();

    data::RecordingBatch getRecordingBatch(string recordingID, long batchIndex);
};

interface Topic
{
    // FIXME: The old interface used this topic.
    //        But this call has been superceeded by commitAndReceiveInteractions
    void updateLayers(data::LayerUpdateSeq updates);
};

interface StorageAndTopicInterface extends StorageInterface, Topic
{
};


};
};
