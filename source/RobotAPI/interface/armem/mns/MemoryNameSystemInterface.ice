#pragma once

#include <RobotAPI/interface/armem/server/ActionsInterface.ice>
#include <RobotAPI/interface/armem/server/PredictingMemoryInterface.ice>
#include <RobotAPI/interface/armem/server/ReadingMemoryInterface.ice>
#include <RobotAPI/interface/armem/server/WritingMemoryInterface.ice>


module armarx
{
    module armem
    {
        module mns
        {
            module dto
            {
                /**
                 * A memory server can implement the reading and/or writing
                 * memory interface. They should be handled individually.
                 * Additionally, it can implement prediction or actions interfaces,
                 * which is currently the case for all working memories.
                 */
                struct MemoryServerInterfaces
                {
                    server::ReadingMemoryInterface* reading;
                    server::WritingMemoryInterface* writing;
                    server::PredictingMemoryInterface* prediction;
                    server::actions::ActionsInterface* actions;
                };
                dictionary<string, MemoryServerInterfaces> MemoryServerInterfacesMap;


                /**
                 * @brief Register a memory server.
                 */
                struct RegisterServerInput
                {
                    string name;
                    MemoryServerInterfaces server;

                    bool existOk = true;
                };
                struct RegisterServerResult
                {
                    bool success;
                    string errorMessage;
                };


                /**
                 * @brief Remove a memory server.
                 */
                struct RemoveServerInput
                {
                    string name;
                    bool notExistOk = true;
                };
                struct RemoveServerResult
                {
                    bool success;
                    string errorMessage;
                };

                /**
                 * @brief Resolve a memory name.
                 */
                struct ResolveServerInput
                {
                    string name;
                };
                struct ResolveServerResult
                {
                    bool success;
                    string errorMessage;

                    MemoryServerInterfaces server;
                };


                /**
                 * @brief Get all registered entries.
                 */
                struct GetAllRegisteredServersResult
                {
                    bool success;
                    string errorMessage;

                    MemoryServerInterfacesMap servers;
                };


                /**
                 * @brief Wait until a server is registered.
                 */
                struct WaitForServerInput
                {
                    string name;
                };
                struct WaitForServerResult
                {
                    bool success;
                    string errorMessage;

                    MemoryServerInterfaces server;
                };
            };


            interface MemoryNameSystemInterface
            {
                dto::RegisterServerResult registerServer(dto::RegisterServerInput input);
                dto::RemoveServerResult removeServer(dto::RemoveServerInput input);

                dto::GetAllRegisteredServersResult getAllRegisteredServers();

                dto::ResolveServerResult resolveServer(dto::ResolveServerInput input);

                ["amd"]  // Asynchronous Method Dispatch
                dto::WaitForServerResult waitForServer(dto::WaitForServerInput input);
            };
        }

    };
};
