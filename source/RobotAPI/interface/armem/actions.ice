#pragma once

#include <RobotAPI/interface/armem/memory.ice>

module armarx
{
    module armem
    {
        module actions
        {
            module data
            {

                class MenuEntry
                {
                    /// Item in the created action path.
                    string id;
                    /// Human-readable text displayed to the user.
                    string text;
                };
                sequence<MenuEntry> MenuEntrySeq;

                /**
                 * Clickable / executable action.
                 */
                class Action extends MenuEntry
                {
                };

                /**
                 * Expandable sub-menu, but cannot be executed itself.
                 */
                class SubMenu extends MenuEntry
                {
                    MenuEntrySeq entries;
                };


                class Menu
                {
                    MenuEntrySeq entries;
                };

                sequence<string> ActionPath;

                struct GetActionsInput
                {
                    armem::data::MemoryID id;
                };
                sequence<GetActionsInput> GetActionsInputSeq;

                struct GetActionsOutput
                {
                    Menu menu;
                };
                sequence<GetActionsOutput> GetActionsOutputSeq;

                struct ExecuteActionInput
                {
                    armem::data::MemoryID id;
                    ActionPath actionPath;
                };
                sequence<ExecuteActionInput> ExecuteActionInputSeq;

                struct ExecuteActionOutput
                {
                    bool success = false;
                    string errorMessage;
                };
                sequence<ExecuteActionOutput> ExecuteActionOutputSeq;
            }
        }
    }
}
