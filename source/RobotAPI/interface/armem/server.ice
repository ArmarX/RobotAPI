#pragma once

#include <RobotAPI/interface/armem/server/MemoryInterface.ice>
#include <RobotAPI/interface/armem/server/ReadingMemoryInterface.ice>
#include <RobotAPI/interface/armem/server/WritingMemoryInterface.ice>
#include <RobotAPI/interface/armem/server/PredictingMemoryInterface.ice>
