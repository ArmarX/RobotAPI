#pragma once

#include <RobotAPI/interface/armem/server/ReplayingMemoryInterface.ice>
#include <RobotAPI/interface/armem/server/RecordingMemoryInterface.ice>

#include <RobotAPI/interface/armem/server/ReadingMemoryInterface.ice>
#include <RobotAPI/interface/armem/server/WritingMemoryInterface.ice>
#include <RobotAPI/interface/armem/server/PredictingMemoryInterface.ice>

#include <RobotAPI/interface/armem/server/ActionsInterface.ice>

#include <RobotAPI/interface/armem/client/MemoryListenerInterface.ice>


module armarx
{
    module armem
    {
        module server
        {
            interface LongTermMemoryInterface extends ReplayingMemoryInterface, RecordingMemoryInterface
            {
            };

            interface WorkingMemoryInterface extends ReadingMemoryInterface, WritingMemoryInterface
            {
            };

            interface MemoryInterface extends
                    WorkingMemoryInterface,
                    LongTermMemoryInterface,
                    PredictingMemoryInterface,
                    actions::ActionsInterface,
                    client::MemoryListenerInterface
            {
            };
        };
    };
};
