#pragma once

#include <RobotAPI/interface/armem/actions.ice>

module armarx
{
    module armem
    {
        module server
        {
            module actions
            {
                interface ActionsInterface
                {
                    actions::data::GetActionsOutputSeq getActions(actions::data::GetActionsInputSeq inputs);
                    actions::data::ExecuteActionOutputSeq executeActions(actions::data::ExecuteActionInputSeq inputs);
                };
            }
        }
    }
}
