#pragma once

#include <RobotAPI/interface/armem/query.ice>
#include <RobotAPI/interface/armem/structure.ice>


module armarx
{
    module armem
    {
        module server
        {
            module dto
            {

            }

            interface ReadingMemoryInterface
            {
                armem::query::data::Result query(armem::query::data::Input input);

                armem::structure::data::GetServerStructureResult getServerStructure();
            };
        };
    };
};
