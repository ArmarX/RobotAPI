#pragma once

#include <RobotAPI/interface/armem/memory.ice>

module armarx
{
    module armem
    {        
        module server
        {
            module dto
            {
                module RecordingMode
                {
                    enum RecordingModeEnum
                    {
                        ALL,
                        NO_BUFFER,
                        FREQUENCY
                    };
                }

                struct DirectlyStoreInput
                {
                    armarx::armem::data::Memory memory;
                };

                struct DirectlyStoreResult
                {
                    bool success = false;
                    long timeStartedMicroSeconds;
                    long timeFinishedMicroSeconds;

                    string errorMessage;
                };

                struct RecordStatus
                {
                    int totalSnapshots;
                    int savedSnapshots;
                };

                struct RecordingModeConfiguration
                {
                    RecordingMode::RecordingModeEnum mode;
                    float frequency;
                };
                dictionary <armarx::armem::data::MemoryID, RecordingModeConfiguration> Configuration;

                struct StartRecordResult {
                    bool success;
                    string errorMessage;
                };

                struct StopRecordResult {
                    bool success;
                    string errorMessage;
                };

                struct RecordStatusResult {
                    bool success;
                    string errorMessage;
                    RecordStatus status;
                };

                struct StartRecordInput {
                    armarx::core::time::dto::DateTime executionTime;
                    string recordingID;
                    armarx::core::time::dto::DateTime startTime;
                    Configuration configuration;
                };
            }

            interface RecordingMemoryInterface
            {
                // Storing
                dto::DirectlyStoreResult directlyStore(dto::DirectlyStoreInput directlStoreInput);

                // Recording
                dto::StartRecordResult startRecord(dto::StartRecordInput startRecordInput);
                dto::StopRecordResult stopRecord();
                dto::RecordStatusResult getRecordStatus();
            };
        };
    };
};
