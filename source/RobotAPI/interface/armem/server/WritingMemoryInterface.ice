#pragma once


#include <RobotAPI/interface/armem/commit.ice>
#include <RobotAPI/interface/armem/memory.ice>


module armarx
{
    module armem
    {
        module server
        {
            interface WritingMemoryInterface
            {
                /// Register multiple core or provider segments.
                data::AddSegmentsResult addSegments(data::AddSegmentsInput input);
                // void removeSegments();

                /// Commit data to the memory.
                data::CommitResult commit(data::Commit commit);
            };
        };
    };
};
