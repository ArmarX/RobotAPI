#pragma once

#include <RobotAPI/interface/armem/prediction.ice>
#include <RobotAPI/interface/armem/query.ice>

module armarx
{
  module armem
  {
    module server
    {
        interface PredictingMemoryInterface
        {
            /* Request multiple predictions from the memory.
             * The timestamp to requst a prediction for is encoded in the MemoryIDs.
             */
            prediction::data::PredictionResultSeq
            predict(prediction::data::PredictionRequestSeq requests);

            /* Get a map from memory segments to the prediction engines they support.
             * Best used with the container_map operations to easily retrieve
             * the available engine selection for fully evaluated MemoryIDs.
             */
            prediction::data::EngineSupportMap getAvailableEngines();
        }
    };
  };
};
