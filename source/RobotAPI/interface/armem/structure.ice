#pragma once

#include <RobotAPI/interface/armem/memory.ice>

module armarx
{
    module armem
    {
        module structure
        {
            module data
            {
                struct GetServerStructureResult {
                    bool success;
                    string errorMessage;
                    armarx::armem::data::Memory serverStructure; //structure of the Server without data (all Segments...)
                };
            };
        };
    };
};
