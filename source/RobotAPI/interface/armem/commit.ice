#pragma once


#include <RobotAPI/interface/armem/memory.ice>
#include <RobotAPI/interface/aron.ice>


module armarx
{
    module armem
    {
        module data
        {
            struct AddSegmentInput
            {
                string coreSegmentName;
                string providerSegmentName;

                bool clearWhenExists = false;
            };
            sequence<AddSegmentInput> AddSegmentsInput;

            struct AddSegmentResult
            {
                bool success = false;
                string segmentID;

                string errorMessage;
            };
            sequence<AddSegmentResult> AddSegmentsResult;


            struct EntityUpdate
            {
                armem::data::MemoryID entityID;
                aron::data::dto::AronDictSeq instancesData;
                armarx::core::time::dto::DateTime timeCreated;

                float confidence = 1.0;
                armarx::core::time::dto::DateTime timeSent;
            };
            sequence<EntityUpdate> EntityUpdateList;

            struct EntityUpdateResult
            {
                bool success = false;

                armem::data::MemoryID snapshotID;
                armarx::core::time::dto::DateTime timeArrived;

                string errorMessage;
            };
            sequence<EntityUpdateResult> EntityUpdateResultList;

            struct Commit
            {
                EntityUpdateList updates;
            };
            struct CommitResult
            {
                EntityUpdateResultList results;
            };
        };
    };
};
