#pragma once

#include <RobotAPI/interface/units/KinematicUnitInterface.ice>
#include <RobotAPI/interface/units/PlatformUnitInterface.ice>

module armarx
{
    module armem
    {
        module robot_state
        {
            interface LegacyRobotStateMemoryAdapterInterface extends armarx::KinematicUnitListener, armarx::PlatformUnitListener
            {
            }
        }
    }
}
