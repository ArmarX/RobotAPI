#pragma once

#include <ArmarXCore/interface/core/time.ice>

#include <RobotAPI/interface/aron.ice>


module armarx
{
    module armem
    {
        module data
        {
            struct MemoryID
            {
                string memoryName = "";
                string coreSegmentName = "";
                string providerSegmentName = "";
                string entityName = "";
                armarx::core::time::dto::DateTime timestamp;
                int instanceIndex = -1;
            }

            sequence<MemoryID> MemoryIDs;

            module detail
            {
                class MemoryItem
                {
                    MemoryID id;
                };
                class TypedMemoryContainer extends MemoryItem
                {
                    aron::type::dto::GenericType aronType;
                };
            }

            /// Ice Twin of `armarx::armem::EntityInstanceMetadata`.
            class EntityInstanceMetadata
            {
                armarx::core::time::dto::DateTime timeCreated;
                armarx::core::time::dto::DateTime timeSent;
                armarx::core::time::dto::DateTime timeArrived;

                float confidence = 1.0;
            };
            /// Ice Twin of `armarx::armem::EntityInstance`.
            class EntityInstance extends detail::MemoryItem
            {
                aron::data::dto::Dict data;

                EntityInstanceMetadata metadata;
            };
            sequence<EntityInstance> EntityInstanceSeq;


            /// Ice Twin of `armarx::armem::EntitySnapshot`.
            class EntitySnapshot extends detail::MemoryItem
            {
                EntityInstanceSeq instances;
            };
            dictionary<armarx::core::time::dto::DateTime, EntitySnapshot> EntityHistory;


            /// Ice Twin of `armarx::armem::Entity`.
            class Entity extends detail::MemoryItem
            {
                EntityHistory history;
            };
            dictionary<string, Entity> EntityDict;


            /// Ice Twin of `armarx::armem::ProviderSegment`.
            class ProviderSegment extends detail::TypedMemoryContainer
            {
                EntityDict entities;
            };
            dictionary<string, ProviderSegment> ProviderSegmentDict;


            /// Ice Twin of `armarx::armem::CoreSegment`.
            class CoreSegment extends detail::TypedMemoryContainer
            {
                ProviderSegmentDict providerSegments;
            };
            dictionary<string, CoreSegment> CoreSegmentDict;


            /// Ice Twin of `armarx::armem::Memory`.
            class Memory extends detail::MemoryItem
            {
                CoreSegmentDict coreSegments;
            };
            dictionary<string, Memory> MemoryDict;

        }

    };
};
