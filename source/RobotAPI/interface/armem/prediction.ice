#pragma once

#include <RobotAPI/interface/armem/memory.ice>
#include <RobotAPI/interface/aron.ice>

module armarx
{
    module armem
    {
        module prediction
        {
            module data
            {
                // Holds properties of a prediction engine identified by its engineID.
                struct PredictionEngine
                {
                    string engineID;
                }
                sequence<PredictionEngine> PredictionEngineSeq;
                dictionary<armem::data::MemoryID, PredictionEngineSeq> EngineSupportMap;

                // Settings to use for a given prediction (e.g. which engine to use)
                struct PredictionSettings
                {
                    string predictionEngineID = "";
                };

                // A single request for a prediction from a memory.
                // The timestamp to request the prediction for is in the MemoryID.
                struct PredictionRequest
                {
                    armem::data::MemoryID snapshotID;
                    PredictionSettings settings;
                }
                sequence<PredictionRequest> PredictionRequestSeq;

                // The result of a single prediction.
                // If successful, it contains the predicted data.
                struct PredictionResult
                {
                    bool success = false;
                    string errorMessage;

                    armem::data::MemoryID snapshotID;
                    aron::data::dto::Dict prediction;
                }
                sequence<PredictionResult> PredictionResultSeq;
            };
        };
    };
};
