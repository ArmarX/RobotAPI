#pragma once

#include <RobotAPI/interface/armem/memory.ice>


module armarx
{
    module armem
    {
        module query
        {
            module data
            {
                module QueryTarget
                {
                    enum QueryTargetEnum
                    {
                        WM,
                        WM_LTM
                    };
                }

                /// Which entity snapshots to get from an entity?
                class EntityQuery
                {
                };
                sequence<EntityQuery> EntityQuerySeq;

                module entity
                {
                    /// Get all snapshots.
                    class All extends EntityQuery
                    {
                    };
                    /// Get a single snapshot (default for latest).
                    class Single extends EntityQuery
                    {
                        armarx::core::time::dto::DateTime timestamp;  // -1 for latest
                    };
                    /// Get snapshots based on a time range (default for all).
                    class TimeRange extends EntityQuery
                    {
                        armarx::core::time::dto::DateTime minTimestamp;  // -1 for oldest
                        armarx::core::time::dto::DateTime maxTimestamp;  // -1 for latest
                    };
                    /**
                     * @brief Get snapshots based on an index range (default for all).
                     *
                     * The returned range of snapshots can be smaller than the requested
                     * range, if less snapshots exist.
                     *
                     * Uses python negative index semantics:
                     * * -1 = last item
                     * * -2 = item before last item, ...
                     */
                    class IndexRange extends EntityQuery
                    {
                        long first = 0;  ///< First index to get.
                        long last = -1;  ///< Last index to get (inclusive).
                    };


                    /**
                     * @brief Get snapshot(s) around timestamp.
                     *
                     * If there is a snapshot which exactly matches the query timestamp,
                     * the behavior is as for the @see Single query. 
                     * 
                     * However, if there is no matching timestamp, the snapshots before 
                     * and after the query timestamp will be returned.
                     * 
                     * If #eps is positive, the snapshots must be within the time
                     * range [timestamp - eps, timestamp + eps]. 
                     * Consequently, the query result can be empty.
                     *
                     * Hint: 
                     *   This query can be quite useful when interpolating snapshots
                     *   is possible.
                     */
                    class TimeApprox extends EntityQuery
                    {
                        armarx::core::time::dto::DateTime timestamp;
                        armarx::core::time::dto::Duration eps;
                    };

                    /**
                     * @brief Get the last snapshot before or at the timestamp.
                     * 
                     * This query is kind of similar to latest() but with a reference timestamp.
                     * 
                     */
                    class BeforeOrAtTime extends EntityQuery
                    {
                        armarx::core::time::dto::DateTime timestamp;
                    }

                    /**
                     * @brief Get the last snapshot before the timestamp or a sequence of 
                     *           n last elements before the timestamp depending on #maxEntries
                     * 
                     * Depending on #maxEntries, the behavior is as follows:
                     *  - #maxEntries == 1 => last element before timestamp
                     *  - #maxEntries > 1  => n last elements before timestamp
                     *  - #maxEntries < 0  => all elements before timestamp
                     */
                    class BeforeTime extends EntityQuery
                    {
                        armarx::core::time::dto::DateTime timestamp;
                        long maxEntries = 1;
                    }

                }


                /// Which entities to get from a provider segment?
                class ProviderSegmentQuery
                {
                    EntityQuerySeq entityQueries;
                };
                sequence<ProviderSegmentQuery> ProviderSegmentQuerySeq;
                module provider
                {
                    class All extends ProviderSegmentQuery
                    {
                    };
                    class Single extends ProviderSegmentQuery
                    {
                        string entityName;
                    };
                    class Regex extends ProviderSegmentQuery
                    {
                        string entityNameRegex;
                    };
                }


                /// Which provider segments to get from a core segment?
                class CoreSegmentQuery
                {
                    ProviderSegmentQuerySeq providerSegmentQueries;
                };
                sequence<CoreSegmentQuery> CoreSegmentQuerySeq;

                module core
                {
                    class All extends CoreSegmentQuery
                    {
                    };
                    class Single extends CoreSegmentQuery
                    {
                        string providerSegmentName;
                    };
                    class Regex extends CoreSegmentQuery
                    {
                        string providerSegmentNameRegex;
                    };
                }


                /// Which core segments to get from a memory?
                class MemoryQuery
                {
                    CoreSegmentQuerySeq coreSegmentQueries;
                    QueryTarget::QueryTargetEnum target = QueryTarget::QueryTargetEnum::WM;
                };
                sequence<MemoryQuery> MemoryQuerySeq;
                dictionary<string, MemoryQuerySeq> MemoryQueriesDict;

                module memory
                {
                    class All extends MemoryQuery
                    {
                    };
                    class Single extends MemoryQuery
                    {
                        string coreSegmentName;
                    };
                    class Regex extends MemoryQuery
                    {
                        string coreSegmentNameRegex;
                    };
                }


                /// Which memories to get from the distributed memory system?
                class DistributedMemoryQuery
                {
                    /// Dict of memory name to
                    MemoryQueriesDict memoryQueries;
                };

                struct Input
                {
                    MemoryQuerySeq memoryQueries;

                    /// If false, no AronData is transferred.
                    bool withData = true;
                };
                struct Result
                {
                    bool success = false;
                    string errorMessage;

                    armem::data::Memory memory;
                };
            };
        };
    };
};
