#pragma once


#include <RobotAPI/interface/armem/memory.ice>


module armarx
{
    module armem
    {
        module client
        {
            interface MemoryListenerInterface
            {
                void memoryUpdated(armem::data::MemoryIDs updatedIDs);
            };
        };
    };
};
