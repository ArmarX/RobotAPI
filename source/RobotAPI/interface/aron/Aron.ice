#pragma once

/**
 * Changelog:
 * - Update 1.1.0: Allow templates in aron and anyobjects
 * - Update 1.1.1: Switch to armarx type. Also remove position, orientation and pose types as they are nothing else that matrices or quaternions
 **/

#include <ArmarXCore/interface/core/BasicTypes.ice>

#define ARON_MAJOR "1"
#define ARON_MINOR "1"
#define ARON_PATCH "2"
#define ARON_VERSION "1.1.2"

module armarx
{
    module aron
    {

        const string VERSION = ARON_VERSION;


        /*************************
         * Aron Types ************
         ************************/
        module type
        {
            enum Maybe
            {
                NONE,
                OPTIONAL,
                RAW_PTR,
                UNIQUE_PTR,
                SHARED_PTR
            };

            module ndarray
            {
                enum ElementType
                {
                    UINT8,
                    INT8,
                    UINT16,
                    INT16,
                    UINT32,
                    INT32,
                    FLOAT32,
                    FLOAT64
                };
            };

            module image
            {
                enum PixelType
                {
                    RGB24,
                    DEPTH32
                };
            };

            module pointcloud
            {
                enum VoxelType
                {
                    POINT_XYZ,
                    POINT_XYZI,
                    POINT_XYZL,
                    POINT_XYZRGB,
                    POINT_XYZRGBL,
                    POINT_XYZRGBA,
                    POINT_XYZHSV
                };
            };

            module matrix
            {
                enum ElementType
                {
                    INT16,
                    INT32,
                    INT64,
                    FLOAT32,
                    FLOAT64
                };
            };

            module quaternion
            {
                enum ElementType
                {
                    FLOAT32,
                    FLOAT64
                };
            };

            module dto
            {
                class GenericType {
                    string VERSION = ARON_VERSION;
                    Maybe maybe = Maybe::NONE;
                }
                sequence<GenericType> GenericTypeSeq;
                dictionary<string, GenericType> GenericTypeDict;

                /* ***** Container types ***** */
                class List extends GenericType { GenericType acceptedType; }
                class Tuple extends GenericType { GenericTypeSeq elementTypes; }
                class Pair extends GenericType { GenericType acceptedType1; GenericType acceptedType2; }
                class AronObject extends GenericType { AronObject parent; Ice::StringSeq templates; string objectName; Ice::StringSeq templateInstantiations; GenericTypeDict elementTypes; }
                class Dict extends GenericType { GenericType acceptedType; }

                /* ***** Complex Types (serialize to ndarray) ***** */
                class NDArray extends GenericType { int ndimensions; ndarray::ElementType elementType; }
                class Matrix extends GenericType { int rows; int cols; matrix::ElementType elementType; }
                class Quaternion extends GenericType { quaternion::ElementType elementType; }
                class Image extends GenericType { image::PixelType pixelType; }
                class PointCloud extends GenericType { pointcloud::VoxelType voxelType; }

                /* ***** Enum types ***** */
                class IntEnum extends GenericType { string enumName; StringIntDict acceptedValues; }
                //class FloatEnum extends GenericType { string enumName; StringFloatDict acceptedValues; }
                //class StringEnum extends GenericType { string enumName; StringStringDict acceptedValues; }

                /* ***** Any Types ***** */
                class AnyObject extends GenericType { };

                /* ***** Primitive Types ***** */
                class AronInt extends GenericType { };
                class AronLong extends GenericType { };
                class AronDouble extends GenericType { };
                class AronFloat extends GenericType { };
                class AronString extends GenericType { };
                class AronBool extends GenericType { };
            };
        };


        /*************************
         * Aron Data *************
         ************************/
        module data
        {
            module dto
            {
                class GenericData {
                    string VERSION = ARON_VERSION;
                };
                sequence<GenericData> GenericDataSeq;
                dictionary<string, GenericData> GenericDataDict;

                /* ***** Container Data ***** */
                class List extends GenericData { GenericDataSeq elements; };
                class Dict extends GenericData { GenericDataDict elements; };

                /* ***** Complex Data ***** */
                // The NDArray contains more or less the same information as an AronType, but there is no other way to do it
                // Especially, note the difference between the type's typeName (e.g. "GRAY_SCALE_IMAGE" => language dependent) and the data's type ("0")
                // Further, note the difference between the type's dimensions (e.g. 128x128) and the data's dimensions (128x128x3 for RGB)
                class NDArray extends GenericData { Ice::IntSeq shape; string type; Ice::ByteSeq data; }


                /* ***** Primitive Data ***** */
                class AronInt extends GenericData { int value; };
                class AronLong extends GenericData { long value; };
                class AronDouble extends GenericData { double value; };
                class AronFloat extends GenericData { float value; };
                class AronString extends GenericData { string value; };
                class AronBool extends GenericData { bool value; };

                // useful for memory ice_conversions
                sequence<Dict> AronDictSeq;
            };
        };
    };
};
