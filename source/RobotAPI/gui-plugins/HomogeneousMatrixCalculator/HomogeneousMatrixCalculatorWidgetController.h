/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::HomogeneousMatrixCalculatorWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/gui-plugins/HomogeneousMatrixCalculator/ui_HomogeneousMatrixCalculatorWidget.h>

#include <VirtualRobot/MathTools.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

namespace armarx
{
    /**
    \page RobotAPI-GuiPlugins-HomogeneousMatrixCalculator HomogeneousMatrixCalculator
    \brief The HomogeneousMatrixCalculator allows visualizing ...

    \image html HomogeneousMatrixCalculator.png
    The user can

    API Documentation \ref HomogeneousMatrixCalculatorWidgetController

    \see HomogeneousMatrixCalculatorGuiPlugin
    */

    /**
     * \class HomogeneousMatrixCalculatorWidgetController
     * \brief HomogeneousMatrixCalculatorWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        HomogeneousMatrixCalculatorWidgetController:
        public ArmarXComponentWidgetControllerTemplate<HomogeneousMatrixCalculatorWidgetController>
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit HomogeneousMatrixCalculatorWidgetController();

        /**
         * Controller destructor
         */
        ~HomogeneousMatrixCalculatorWidgetController() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Util.HomogeneousMatrixCalculator";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

        Eigen::Matrix4d getMatrix();

        static QIcon GetWidgetIcon()
        {
            return QIcon("://icons/htmx_calc_icon.svg");
        }
        static QIcon GetWidgetCategoryIcon()
        {
            return QIcon("://icons/tools.svg");
        }

    public slots:
        /* QT slot declarations */

    signals:
        /* QT signal declarations */

    private slots:
        void changed4f();

        void changedrpy();

        void recalcInv();
        void recalcProd();
        void recalcInvAndProd();


    private:
        /**
         * Widget Form
         */
        Ui::HomogeneousMatrixCalculatorWidget widget;
    };
}

