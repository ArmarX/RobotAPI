/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::LaserScannerPluginWidgetController
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/gui-plugins/LaserScannerPlugin/ui_LaserScannerPluginWidget.h>
#include <RobotAPI/interface/units/LaserScannerUnit.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <mutex>

namespace armarx
{
    /**
    \page RobotAPI-GuiPlugins-LaserScannerPlugin LaserScannerPlugin
    \brief The LaserScannerPlugin allows visualizing the raw sensor data captured from multiple laser scanners.

    API Documentation \ref LaserScannerPluginWidgetController

    \see LaserScannerPluginGuiPlugin
    */

    /**
     * \class LaserScannerPluginWidgetController
     * \brief LaserScannerPluginWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        LaserScannerPluginWidgetController:
        public ArmarXComponentWidgetControllerTemplate<LaserScannerPluginWidgetController>,
        public armarx::LaserScannerUnitListener
    {
        Q_OBJECT

    public:
        /**
         * Controller Constructor
         */
        explicit LaserScannerPluginWidgetController();

        /**
         * Controller destructor
         */
        ~LaserScannerPluginWidgetController() override;

        /**
         * @see ArmarXWidgetController::loadSettings()
         */
        void loadSettings(QSettings* settings) override;

        /**
         * @see ArmarXWidgetController::saveSettings()
         */
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "RobotControl.LaserScannerGUI";
        }

        /**
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

        QPointer<QDialog> getConfigDialog(QWidget* parent) override;

        void configured() override;

        void reportSensorValues(const std::string& device, const std::string& name,
                                const LaserScan& scan, const TimestampBasePtr& timestamp,
                                const Ice::Current& c) override;
    public slots:
        void onNewSensorValuesReported();
        void onDeviceSelected(int index);

    signals:
        void newSensorValuesReported();

    private:
        /**
         * Widget Form
         */
        Ui::LaserScannerPluginWidget widget;
        QPointer<SimpleConfigDialog> dialog;

        std::string laserScannerUnitName;
        LaserScannerUnitInterfacePrx laserScannerUnit;
        LaserScannerInfoSeq laserScanners;

        std::mutex scanMutex;
        std::unordered_map<std::string, LaserScan> scans;
        std::unordered_map<std::string, std::deque<int>> numberOfRingsHistory;


        std::unique_ptr<QGraphicsScene> scene;
    };
}

