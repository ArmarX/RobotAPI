/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::ObjectPoseGuiWidgetController
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/gui-plugins/ObjectPoseGui/ui_ObjectPoseGuiWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <RobotAPI/interface/objectpose/ObjectPoseStorageInterface.h>


namespace armarx
{
    /**
    @page RobotAPI-GuiPlugins-ObjectPoseGui ObjectPoseGui
    @brief The ObjectPoseGui allows visualizing ...

    @image html ObjectPoseGui.png
    The user can

    API Documentation @ref ObjectPoseGuiWidgetController

    @see ObjectPoseGuiGuiPlugin
    */

    /**
     * @class ObjectPoseGuiWidgetController
     * @brief ObjectPoseGuiWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        ObjectPoseGuiWidgetController :
        public armarx::ArmarXComponentWidgetControllerTemplate < ObjectPoseGuiWidgetController >
    {
        Q_OBJECT

    public:
        /// Controller Constructor
        explicit ObjectPoseGuiWidgetController();

        /// Controller destructor
        virtual ~ObjectPoseGuiWidgetController() override;

        /// Returns the Widget name displayed in the ArmarXGui to create an instance of this class.
        static QString GetWidgetName();


        /// @see ArmarXWidgetController::loadSettings()
        void loadSettings(QSettings* settings) override;
        /// @see ArmarXWidgetController::saveSettings()
        void saveSettings(QSettings* settings) override;


        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;


        /// @see armarx::Component::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::Component::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::Component::onConnectComponent()
        void onDisconnectComponent() override;


    public slots:
        /* QT slot declarations */

        /// Update the currently opened tab.
        void updateTab();
        void updateObjectsTab();
        void updateRequestTab();

        void prepareObjectContextMenu(const QPoint& pos);
        void attachObjectToRobotNode(QString providerName, QString objectID,
                                     const std::string& agentName, const std::string& frameName);
        void detachObjectFromRobotNode(QString providerName, QString objectID);

        void requestSelectedObjects();


    signals:
        /* QT signal declarations */

    private:

        /// Widget Form
        Ui::ObjectPoseGuiWidget widget;

        QPointer<SimpleConfigDialog> configDialog;

        std::string ObjectPoseStorageName;
        armarx::objpose::ObjectPoseStorageInterfacePrx ObjectPoseStorage;

        objpose::AgentFramesSeq attachableFrames;

    };
}


