armarx_set_target("DebugDrawerGuiPluginGuiPlugin")
armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")

set(SOURCES DebugDrawerGuiPluginGuiPlugin.cpp DebugDrawerGuiPluginWidgetController.cpp)
set(HEADERS DebugDrawerGuiPluginGuiPlugin.h   DebugDrawerGuiPluginWidgetController.h)

set(GUI_MOC_HDRS ${HEADERS})
set(GUI_UIS DebugDrawerGuiPluginWidget.ui)

# Add more libraries you depend on here, e.g. ${QT_LIBRARIES}.
set(COMPONENT_LIBS 
    DebugDrawer 
    RobotAPIInterfaces
    SimpleConfigDialog
)

if(ArmarXGui_FOUND)
    armarx_gui_library(DebugDrawerGuiPluginGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
