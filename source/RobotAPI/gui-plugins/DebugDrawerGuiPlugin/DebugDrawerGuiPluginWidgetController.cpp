/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::DebugDrawerGuiPluginWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2019
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>

#include <Ice/UUID.h>

#include <VirtualRobot/MathTools.h>

#include <ArmarXCore/util/CPPUtility/trace.h>

#include "DebugDrawerGuiPluginWidgetController.h"

namespace armarx
{
    DebugDrawerGuiPluginWidgetController::DebugDrawerGuiPluginWidgetController():
        _layerName{Ice::generateUUID()},
        _debugDrawer{_layerName}
    {
        ARMARX_TRACE;
        _ui.setupUi(getWidget());
        connect(_ui.pushButtonLayerClear, SIGNAL(clicked()), this, SLOT(on_pushButtonLayerClear_clicked()));
        connect(_ui.pushButtonPoseDelete, SIGNAL(clicked()), this, SLOT(on_pushButtonPoseDelete_clicked()));
        connect(_ui.pushButtonArrowDelete, SIGNAL(clicked()), this, SLOT(on_pushButtonArrowDelete_clicked()));

        connect(_ui.doubleSpinBoxPoseTX, SIGNAL(valueChanged(double)), this, SLOT(updatePose()));
        connect(_ui.doubleSpinBoxPoseTY, SIGNAL(valueChanged(double)), this, SLOT(updatePose()));
        connect(_ui.doubleSpinBoxPoseTZ, SIGNAL(valueChanged(double)), this, SLOT(updatePose()));
        connect(_ui.doubleSpinBoxPoseRX, SIGNAL(valueChanged(double)), this, SLOT(updatePose()));
        connect(_ui.doubleSpinBoxPoseRY, SIGNAL(valueChanged(double)), this, SLOT(updatePose()));
        connect(_ui.doubleSpinBoxPoseRZ, SIGNAL(valueChanged(double)), this, SLOT(updatePose()));
        connect(_ui.doubleSpinBoxPoseScale, SIGNAL(valueChanged(double)), this, SLOT(updatePose()));

        connect(_ui.doubleSpinBoxArrowFX, SIGNAL(valueChanged(double)), this, SLOT(updateArrow()));
        connect(_ui.doubleSpinBoxArrowFY, SIGNAL(valueChanged(double)), this, SLOT(updateArrow()));
        connect(_ui.doubleSpinBoxArrowFZ, SIGNAL(valueChanged(double)), this, SLOT(updateArrow()));
        connect(_ui.doubleSpinBoxArrowTX, SIGNAL(valueChanged(double)), this, SLOT(updateArrow()));
        connect(_ui.doubleSpinBoxArrowTY, SIGNAL(valueChanged(double)), this, SLOT(updateArrow()));
        connect(_ui.doubleSpinBoxArrowTZ, SIGNAL(valueChanged(double)), this, SLOT(updateArrow()));
        connect(_ui.doubleSpinBoxArrowClrR, SIGNAL(valueChanged(double)), this, SLOT(updateArrow()));
        connect(_ui.doubleSpinBoxArrowClrG, SIGNAL(valueChanged(double)), this, SLOT(updateArrow()));
        connect(_ui.doubleSpinBoxArrowClrB, SIGNAL(valueChanged(double)), this, SLOT(updateArrow()));
        connect(_ui.doubleSpinBoxArrowClrA, SIGNAL(valueChanged(double)), this, SLOT(updateArrow()));
        connect(_ui.doubleSpinBoxArrowWidth, SIGNAL(valueChanged(double)), this, SLOT(updateArrow()));
    }


    DebugDrawerGuiPluginWidgetController::~DebugDrawerGuiPluginWidgetController()
    {

    }


    void DebugDrawerGuiPluginWidgetController::loadSettings(QSettings* settings)
    {
    }

    void DebugDrawerGuiPluginWidgetController::saveSettings(QSettings* settings)
    {
    }


    void DebugDrawerGuiPluginWidgetController::onInitComponent()
    {
        ARMARX_TRACE;
        offeringTopic(_debugDrawerTopicName);
    }


    void DebugDrawerGuiPluginWidgetController::onConnectComponent()
    {
        ARMARX_TRACE;
        _debugDrawer.setDebugDrawer(getTopic<DebugDrawerInterfacePrx>(_debugDrawerTopicName));
    }

    void DebugDrawerGuiPluginWidgetController::onDisconnectComponent()
    {
        _debugDrawer.clearLayer();
    }

    void DebugDrawerGuiPluginWidgetController::on_pushButtonArrowDelete_clicked()
    {
        const auto name = _ui.lineEditArrowName->text().toStdString();
        ARMARX_INFO << "Delete arrow " << name;
        _debugDrawer.getDebugDrawer()->removePoseVisu(_layerName, name);
    }

    void DebugDrawerGuiPluginWidgetController::on_pushButtonPoseDelete_clicked()
    {
        const auto name = _ui.lineEditPoseName->text().toStdString();
        ARMARX_INFO << "Delete pose " << name;
        _debugDrawer.getDebugDrawer()->removePoseVisu(_layerName, name);
    }

    void DebugDrawerGuiPluginWidgetController::on_pushButtonLayerClear_clicked()
    {
        _debugDrawer.clearLayer();
    }

    void DebugDrawerGuiPluginWidgetController::updatePose()
    {
        const auto name = _ui.lineEditPoseName->text().toStdString();
        ARMARX_INFO << "Updated pose " << name;

        const float deg2rad = M_PI / 360;

        _debugDrawer.drawPose(
            name,
            VirtualRobot::MathTools::posrpy2eigen4f(
                _ui.doubleSpinBoxPoseTX->value(),
                _ui.doubleSpinBoxPoseTY->value(),
                _ui.doubleSpinBoxPoseTZ->value(),
                _ui.doubleSpinBoxPoseRX->value() * deg2rad,
                _ui.doubleSpinBoxPoseRY->value() * deg2rad,
                _ui.doubleSpinBoxPoseRZ->value() * deg2rad
            ),
            _ui.doubleSpinBoxPoseScale->value());
    }

    void DebugDrawerGuiPluginWidgetController::updateArrow()
    {
        const Eigen::Vector3f from
        {
            static_cast<float>(_ui.doubleSpinBoxArrowFX->value()),
            static_cast<float>(_ui.doubleSpinBoxArrowFY->value()),
            static_cast<float>(_ui.doubleSpinBoxArrowFZ->value())
        };

        const Eigen::Vector3f to
        {
            static_cast<float>(_ui.doubleSpinBoxArrowTX->value()),
            static_cast<float>(_ui.doubleSpinBoxArrowTY->value()),
            static_cast<float>(_ui.doubleSpinBoxArrowTZ->value())
        };

        const Eigen::Vector3f dir = to - from;

        const float len =  dir.norm();

        const auto name = _ui.lineEditArrowName->text().toStdString();
        ARMARX_INFO << "Updated arrow " << name;

        _debugDrawer.drawArrow(
            name,
            from,
            dir.normalized(),
        {
            static_cast<float>(_ui.doubleSpinBoxArrowClrR->value()),
            static_cast<float>(_ui.doubleSpinBoxArrowClrG->value()),
            static_cast<float>(_ui.doubleSpinBoxArrowClrB->value()),
            static_cast<float>(_ui.doubleSpinBoxArrowClrA->value())
        },
        len,
        static_cast<float>(_ui.doubleSpinBoxArrowWidth->value()));
    }
}



