/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::RobotUnitPluginWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/gui-plugins/RobotUnitPlugin/ui_RobotUnitPluginWidget.h>

#include <mutex>
#include <map>
#include <string>
#include <chrono>
#include <fstream>

#include <QWidget>
#include <QPushButton>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QPointer>
#include <QToolBar>
#include <QGroupBox>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/libraries/RobotAPIVariantWidget/RobotAPIVariantWidget.h>
#include <RobotAPI/libraries/RobotUnitDataStreamingReceiver/RobotUnitDataStreamingReceiver.h>

#include "../QWidgets/NJointControllerClassesWidget.h"
#include "../QWidgets/NJointControllersWidget.h"
#include "../QWidgets/ControlDevicesWidget.h"
#include "../QWidgets/SensorDevicesWidget.h"

namespace armarx
{
    /**
    \page RobotAPI-GuiPlugins-RobotUnitPlugin RobotUnitPlugin
    \brief The RobotUnitPlugin allows visualizing ...

    \image html RobotUnitPlugin.png
    The user can

    API Documentation \ref RobotUnitPluginWidgetController

    \see RobotUnitPluginGuiPlugin
    */

    /**
     * \class RobotUnitPluginWidgetController
     * \brief RobotUnitPluginWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT RobotUnitPluginWidgetController:
        public ArmarXComponentWidgetControllerTemplate<RobotUnitPluginWidgetController>,
        public RobotUnitListener
    {
        Q_OBJECT
    public:
        /// @brief Controller Constructor
        explicit RobotUnitPluginWidgetController();

        /// @see ArmarXWidgetController::loadSettings()
        void loadSettings(QSettings* settings) override;

        /// @see ArmarXWidgetController::saveSettings()
        void saveSettings(QSettings* settings) override;

        /// @brief Returns the Widget name displayed in the ArmarXGui to create an instance of this class.
        static QString GetWidgetName()
        {
            return "RobotControl.RobotUnitGUI";
        }

        /// @see armarx::Component::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::Component::onConnectComponent()
        void onConnectComponent() override;

        void onDisconnectComponent() override;
        void onExitComponent() override;
        /**
         * @param parent The dialog's parent.
         * @return The plugin's config dialog.
         */
        QPointer<QDialog> getConfigDialog(QWidget* parent) override;

        /// @brief Callback called when the config dialog is closed.
        void configured() override;

        static QIcon GetWidgetIcon()
        {
            return QIcon(":/robotunitplugin/icons/robot_unit_icon.svg");
        }
        static QIcon GetWidgetCategoryIcon()
        {
            return QIcon(":/robotunitplugin/icons/robot_unit_icon.svg");
        }
        QPointer<QWidget> getCustomTitlebarWidget(QWidget* parent) override;

        // RobotUnitListener interface
    public:
        void nJointControllerStatusChanged(const NJointControllerStatusSeq& status, const Ice::Current&) override;
        void controlDeviceStatusChanged(const ControlDeviceStatusSeq& status, const Ice::Current&) override;
        void sensorDeviceStatusChanged(const SensorDeviceStatusSeq& status, const Ice::Current&) override;
        void nJointControllerClassAdded(const std::string& name, const Ice::Current&) override;
        void nJointControllerCreated(const std::string& name, const Ice::Current&) override;
        void nJointControllerDeleted(const std::string& name, const Ice::Current&) override;

    private slots:
        void refreshNJointControllersClicked();
        void refreshNJointControllerClassesClicked();
        void refreshControlDevicesClicked();
        void refreshSensorDevicesClicked();
        void refreshLogging();

        void startOnConnectTimer();
        void stopOnConnectTimer();

        //logging
        void on_lineEditLoggingFilter_textChanged(const QString& arg1);
        void on_pushButtonLoggingStop_clicked();
        void on_pushButtonLoggingStart_clicked();
        void on_treeWidgetLoggingNames_itemChanged(QTreeWidgetItem* item, int column);
        void on_pushButtonLoggingMark1_clicked();
        void on_pushButtonLoggingMark2_clicked();
        void on_pushButtonLoggingMark3_clicked();

    private:
        void timerEvent(QTimerEvent*) override;
        void updateToolBarActionCheckedState();

    private:
        /// @brief Widget Form
        Ui::RobotUnitPluginWidget widget;
        /// @brief The plugin's config dialog.
        QPointer<SimpleConfigDialog> dialog;

        std::string robotUnitProxyName;
        std::recursive_mutex  robotUnitPrxMutex;
        RobotUnitInterfacePrx robotUnitPrx;
        std::string listenerTopicName;

        NJointControllerClassesWidget* nJointControllerClasses;
        NJointControllersWidget* nJointControllers;
        ControlDevicesWidget* controlDevices;
        SensorDevicesWidget* sensorDevices;

        QPointer<QToolBar> customToolbar;

        QPointer<QAction> showCDevs;
        QPointer<QAction> showSDevs;
        QPointer<QAction> showNJoint;
        QPointer<QAction> showNJointClasses;
        QPointer<QAction> showLogging;

        struct LoggingData
        {
            QTreeWidgetItem*                  top;
            std::vector<QTreeWidgetItem*>     allItems;
            SimpleRemoteReferenceCounterBasePtr     handle;
            RobotUnitDataStreamingReceiverPtr streamingHandler;
            std::ofstream                     logStream;
            static void updateCheckStateUpward(QTreeWidgetItem* item, bool recurseParents);
            static void updateCheckStateDownward(QTreeWidgetItem* item, Qt::CheckState state, bool recurseChildren);
            void localLogging();
        };
        LoggingData loggingData;

        int timerId = 0;
        std::atomic_bool timerLastIterationRuWasRunning = false;
    };
}

