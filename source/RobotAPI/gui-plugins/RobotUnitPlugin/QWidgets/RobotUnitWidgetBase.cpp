/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::RobotUnitPlugin
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "RobotUnitWidgetBase.h"
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <thread>

namespace armarx
{
    RobotUnitWidgetBase::RobotUnitWidgetBase(QString name, QWidget* parent) :
        QWidget(parent)
    {
        setObjectName(name);
    }

    RobotUnitWidgetBase::~RobotUnitWidgetBase()
    {
        while (_resetThread.joinable() && ! _resetThread.try_join_for(boost::chrono::milliseconds{100}))
        {
            ARMARX_INFO << "Joining reset thread";
        }
    }

    void RobotUnitWidgetBase::updateContent()
    {
        if (isResetting)
        {
            return;
        }
        std::unique_lock<std::recursive_timed_mutex> guard {mutex};
        resetCount = 0;
        getResettigLabel().setText("Resetting... " + QString::number(resetCount));
        getStackedWidget().setCurrentIndex(0);
        if (!robotUnit)
        {
            return;
        }
        doContentUpdate();
    }

    void RobotUnitWidgetBase::reset(RobotUnitInterfacePrx ru)
    {
        isResetting = true;
        gotResetData = false;
        std::unique_lock<std::recursive_timed_mutex> guard {mutex};
        robotUnit = ru;
        QMetaObject::invokeMethod(this, "doReset", Qt::QueuedConnection);
    }

    void RobotUnitWidgetBase::setVisible(bool visible)
    {
        doMetaCall = visible;
        if (visible)
        {
            QMetaObject::invokeMethod(this, "updateContent", Qt::QueuedConnection);
        }
        QWidget::setVisible(visible);
    }

    void RobotUnitWidgetBase::doReset()
    {
        std::unique_lock<std::recursive_timed_mutex> guard {mutex};
        clearAll();
        getTreeWidget().clear();
        addFilter();
        if (robotUnit)
        {
            if (!_resetThread.joinable() || _resetThread.try_join_for(boost::chrono::milliseconds{10}))
            {
                resetTimerId = startTimer(0);
                getStackedWidget().setCurrentIndex(1);
                _resetThread = boost::thread
                {
                    [&]
                    {
                        ARMARX_INFO << "Resetting of widget '" << objectName().toStdString() << "' started";
                        try
                        {
                            getResetData();
                        }
                        catch (...)
                        {
                            ARMARX_WARNING << "Resetting of widget '" << objectName().toStdString() << "' failed: "
                                           << armarx::GetHandledExceptionString();
                        }
                        gotResetData = true;
                        if (doMetaCall)
                        {
                            QMetaObject::invokeMethod(this, "updateContent", Qt::QueuedConnection);
                        }
                        ARMARX_INFO << "Resetting of widget '" << objectName().toStdString() << "' done!";
                    }
                };
            }
        }
        else
        {
            gotResetData = true;
            if (doMetaCall)
            {
                QMetaObject::invokeMethod(this, "updateContent", Qt::QueuedConnection);
            }
        }
    }

    void RobotUnitWidgetBase::timerEvent(QTimerEvent*)
    {
        if (isResetting && gotResetData)
        {
            getResettigLabel().setText("Resetting... " + QString::number(++resetCount));
            isResetting = ! addOneFromResetData();
            if (!isResetting)
            {
                killTimer(resetTimerId);
                resetTimerId = 0;
                if (doMetaCall)
                {
                    QMetaObject::invokeMethod(this, "updateContent", Qt::QueuedConnection);
                }
            }
        }
    }
}
