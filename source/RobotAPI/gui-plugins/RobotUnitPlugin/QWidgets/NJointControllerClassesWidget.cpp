/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::RobotUnitPlugin
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <filesystem>

#include "NJointControllerClassesWidget.h"
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/util/StringHelpers.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <QGridLayout>
#include <QDir>
#include <QSortFilterProxyModel>

namespace armarx
{
    NJointControllerClassesWidget::NJointControllerClassesWidget(QWidget* parent) :
        RobotUnitWidgetTemplateBase("NJointControllerClassesWidget", parent)
    {
        connect(ui->pushButtonLoadLib, SIGNAL(released()), this, SLOT(loadLibClicked()));
        connect(ui->comboBoxPackage, SIGNAL(currentIndexChanged(QString)), this, SLOT(packageEditChanged()));
        connect(ui->comboBoxPackage, SIGNAL(editTextChanged(QString)), this, SLOT(packageEditChanged()));
        ui->comboBoxPackage->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        ui->comboBoxPackage->setFixedWidth(300);

        //get package hints
        {
            using namespace std::filesystem;
            std::string homeDir = QDir::homePath().toStdString();
            path p = path {homeDir} / ".cmake" / "packages";
            if (is_directory(p))
            {
                for (const path& entry : directory_iterator(p))
                {
                    const std::string pkg = entry.filename().string();
                    if (CMakePackageFinder {pkg, "", true} .packageFound())
                    {
                        ui->comboBoxPackage->addItem(QString::fromStdString(pkg));
                    }
                }
                // for sorting you need the following 4 lines
                QSortFilterProxyModel* proxy = new QSortFilterProxyModel(ui->comboBoxPackage);
                proxy->setSourceModel(ui->comboBoxPackage->model());
                // combo's current model must be reparented,
                // otherwise QComboBox::setModel() will delete it
                ui->comboBoxPackage->model()->setParent(proxy);
                ui->comboBoxPackage->setModel(proxy);
                ui->comboBoxPackage->model()->sort(0);
            }
        }

        ui->comboBoxPackage->setEditText("");
        packageEditChanged();

        ui->treeWidget->setColumnCount(2);

        QTreeWidgetItem* head = ui->treeWidget->headerItem();
        head->setText(0, "Class");
        head->setText(1, "");
        head->setToolTip(0, "Controller class name");
        ui->treeWidget->header()->setResizeMode(0, QHeaderView::Fixed);
        ui->treeWidget->header()->setResizeMode(1, QHeaderView::Fixed);

    }

    NJointControllerClassesWidget::~NJointControllerClassesWidget()
    {
        delete ui;
    }

    void NJointControllerClassesWidget::nJointControllerClassAdded(std::string name)
    {
        RobotUnitInterfacePrx ru;
        std::unique_lock<std::recursive_timed_mutex> guard {mutex};
        if (!robotUnit)
        {
            return;
        }
        ru = robotUnit;
        guard.unlock();
        auto data = ru->getNJointControllerClassDescription(name);
        guard.lock();
        nJointControllerClassDescriptions[data.className] = std::move(data);
        if (doMetaCall)
        {
            QMetaObject::invokeMethod(this, "updateContent", Qt::QueuedConnection);
        }
    }

    void NJointControllerClassesWidget::updateDefaultNameOnControllerCreated(QString createdName, bool force)
    {
        const auto oldName = getDefaultName();
        if (oldName == createdName || force)
        {
            ++defaultControllerName;
            const auto newName = getDefaultName();
            for (auto& pair : entries)
            {
                pair.second->updateDefaultName(oldName, newName);
            }
        }
    }

    QString NJointControllerClassesWidget::getDefaultName() const
    {
        return QString::number(defaultControllerName);
    }

    void NJointControllerClassesWidget::loadSettings(QSettings* settings)
    {
        ui->lineEditLibrary->setText(settings->value("classLoadLineEditLib", "").toString());
        ui->comboBoxPackage->lineEdit()->setText(settings->value("classLoadComboBoxPkg", "").toString());
        ui->comboBoxLibrary->lineEdit()->setText(settings->value("classLoadComboBoxLib", "").toString());
    }

    void NJointControllerClassesWidget::saveSettings(QSettings* settings)
    {
        settings->setValue("classLoadLineEditLib", ui->lineEditLibrary->text());
        settings->setValue("classLoadComboBoxPkg", ui->comboBoxPackage->currentText());
        settings->setValue("classLoadComboBoxLib", ui->comboBoxLibrary->currentText());
    }

    void NJointControllerClassesWidget::clearAll()
    {
        entries.clear();
        nJointControllerClassDescriptions.clear();
    }

    void NJointControllerClassesWidget::doContentUpdate()
    {
        for (const auto& pair : nJointControllerClassDescriptions)
        {
            add(pair.second);
        }
        nJointControllerClassDescriptions.clear();
    }

    void NJointControllerClassesWidget::getResetData()
    {
        auto temp = robotUnit->getNJointControllerClassDescriptions();
        {
            std::unique_lock<std::recursive_timed_mutex> guard {mutex};
            for (NJointControllerClassDescription& ds : temp)
            {
                nJointControllerClassDescriptions[ds.className] = std::move(ds);
            }
        }
    }

    bool NJointControllerClassesWidget::addOneFromResetData()
    {
        if (nJointControllerClassDescriptions.empty())
        {
            return true;
        }
        add(nJointControllerClassDescriptions.begin()->second);
        nJointControllerClassDescriptions.erase(nJointControllerClassDescriptions.begin());
        return false;
    }

    void NJointControllerClassesWidget::filterUpdated()
    {
        for (auto& entry : entries)
        {
            NJointControllerClassesWidgetEntry* item = entry.second;
            if (! filterNameActive->isChecked() && !filterRemoteCreationActive->isChecked())
            {
                item->setVisible(true);
                return;
            }
            const bool combineOr = (filterCombination->currentText() == "Or");
            bool showName = !combineOr; //init to neutral element
            bool showRemote = !combineOr;//init to neutral element

            if (filterNameActive->isChecked())
            {
                showName = (item->matchName(filterName->text()) != filterNameInverted->isChecked());
            }
            if (filterRemoteCreationActive->isChecked())
            {
                if (item->hasRemoteCreation())
                {
                    showRemote = (filterRemoteCreation->currentText() != "Without");
                }
                else
                {
                    showRemote = (filterRemoteCreation->currentText() != "With");
                }
            }
            if (combineOr)
            {
                item->setVisible(showName || showRemote);
            }
            else
            {
                item->setVisible(showName && showRemote);
            }
        }
    }

    void NJointControllerClassesWidget::add(const  NJointControllerClassDescription& desc)
    {
        std::unique_lock<std::recursive_timed_mutex> guard {mutex};
        if (entries.count(desc.className))
        {
            return;
        }
        entries[desc.className] = new NJointControllerClassesWidgetEntry(*this, *(ui->treeWidget), desc, robotUnit);
    }

    void NJointControllerClassesWidget::addFilter()
    {
        QTreeWidgetItem* filterHeader = new QTreeWidgetItem;
        ui->treeWidget->addTopLevelItem(filterHeader);
        filterHeader->setText(0, "Filter");

        QTreeWidgetItem* filter = new QTreeWidgetItem;
        filterHeader->addChild(filter);
        filter->setFirstColumnSpanned(true);
        QWidget* w = new QWidget;
        QGridLayout* l = new QGridLayout;
        w->setLayout(l);
        l->setContentsMargins(0, 0, 0, 0);
        l->addItem(new QSpacerItem {0, 0, QSizePolicy::MinimumExpanding}, 0, 3);
        l->addWidget(new QLabel {"Combine filters with"}, 0, 0, 1, 1);
        filterCombination = new QComboBox;
        filterCombination->addItem("Or");
        filterCombination->addItem("And");
        l->addWidget(filterCombination, 0, 1, 1, 1);

        filterNameActive = new QCheckBox;
        filterNameActive->setText("Filter by name");
        l->addWidget(filterNameActive, 1, 0, 1, 1);

        filterName = new QLineEdit;
        l->addWidget(filterName, 1, 1, 1, 1);

        filterNameInverted = new QCheckBox;
        filterNameInverted->setText("Invert filter");
        l->addWidget(filterNameInverted, 1, 2, 1, 1);
        connect(filterNameActive, SIGNAL(toggled(bool)), filterName, SLOT(setEnabled(bool)));
        connect(filterNameActive, SIGNAL(toggled(bool)), filterNameInverted, SLOT(setEnabled(bool)));
        filterName->setEnabled(false);
        filterNameInverted->setEnabled(false);

        filterRemoteCreationActive = new QCheckBox;
        filterRemoteCreationActive->setText("Filter by remote creation capabilities");
        l->addWidget(filterRemoteCreationActive, 2, 0, 1, 1);
        filterRemoteCreation = new QComboBox;
        filterRemoteCreation->addItem("Both");
        filterRemoteCreation->addItem("With");
        filterRemoteCreation->addItem("Without");
        l->addWidget(filterRemoteCreation, 2, 1, 1, 1);
        connect(filterRemoteCreationActive, SIGNAL(toggled(bool)), filterRemoteCreation, SLOT(setEnabled(bool)));
        filterRemoteCreationActive->setEnabled(false);

        connect(filterCombination, SIGNAL(currentIndexChanged(QString)), this, SLOT(filterUpdated()));
        connect(filterNameActive, SIGNAL(clicked(bool)), this, SLOT(filterUpdated()));
        connect(filterRemoteCreationActive, SIGNAL(clicked(bool)), this, SLOT(filterUpdated()));
        connect(filterNameInverted, SIGNAL(clicked(bool)), this, SLOT(filterUpdated()));
        connect(filterName, SIGNAL(textChanged(QString)), this, SLOT(filterUpdated()));
        connect(filterRemoteCreation, SIGNAL(currentIndexChanged(int)), this, SLOT(filterUpdated()));

        filterNameActive->setChecked(false);
        filterRemoteCreationActive->setChecked(false);

        ui->treeWidget->setItemWidget(filter, 0, w);
    }

    NJointControllerClassesWidgetEntry::NJointControllerClassesWidgetEntry(
        NJointControllerClassesWidget& parent,
        QTreeWidget& treeWidget,
        const NJointControllerClassDescription& desc,
        RobotUnitInterfacePrx robotUnit
    ) :
        QObject {&parent},
        className {desc.className},
        classNameQStr {QString::fromStdString(className)},
        robotUnit {robotUnit},
        parent {&parent}
    {
        header = new QTreeWidgetItem {{QString::fromStdString(desc.className)}};
        treeWidget.addTopLevelItem(header);
        treeWidget.resizeColumnToContents(0);
        QWidget* headerW = new QWidget;
        headerW->setLayout(new QHBoxLayout);
        headerW->layout()->setContentsMargins(0, 0, 0, 0);
        headerW->layout()->addItem(new QSpacerItem {0, 0, QSizePolicy::MinimumExpanding});
        if (desc.configDescription)
        {
            //add textfiel + button
            {
                nameEdit = new QLineEdit;
                nameEdit->setText(parent.getDefaultName());
                nameEdit->setToolTip("The instance name for the created controller instance");
                headerW->layout()->addWidget(new QLabel {"Controller name"});
                headerW->layout()->addWidget(nameEdit);
                QPushButton* button = new QPushButton {"Create"};
                headerW->layout()->addWidget(button);
                button->setToolTip("Create a new controller instance of this class");
                connect(button, &QPushButton::clicked, this, &NJointControllerClassesWidgetEntry::createCtrl);
                connect(nameEdit, &QLineEdit::returnPressed, this, &NJointControllerClassesWidgetEntry::createCtrl);
            }
            //descr
            {
                QTreeWidgetItem* child = new QTreeWidgetItem;
                header->addChild(child);
                child->setFirstColumnSpanned(true);
                QWidget* compressWid = new QWidget;
                QHBoxLayout* compressLay = new QHBoxLayout;
                compressWid->setLayout(compressLay);
                compressLay->setContentsMargins(0, 0, 0, 0);
                creator = WidgetDescription::makeDescribedWidget(desc.configDescription);
                creator->layout()->setContentsMargins(0, 0, 0, 0);
                compressLay->addWidget(creator);
                compressLay->addItem(new QSpacerItem {0, 0, QSizePolicy::MinimumExpanding});
                treeWidget.setItemWidget(child, 0, compressWid);
            }
        }
        else
        {
            //dummy to force the same height for all lines
            QLineEdit* dummy = new QLineEdit;
            dummy->setFixedWidth(0);
            dummy->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Minimum);
            headerW->layout()->addWidget(dummy);

            headerW->layout()->addWidget(new QLabel {"No remote creation allowed."});
        }
        treeWidget.setItemWidget(header, 1, headerW);
    }

    bool NJointControllerClassesWidgetEntry::matchName(const QString& name)
    {
        return classNameQStr.contains(name, Qt::CaseInsensitive);
    }

    bool NJointControllerClassesWidgetEntry::hasRemoteCreation()
    {
        return creator;
    }

    void NJointControllerClassesWidgetEntry::updateDefaultName(const QString& oldName, const QString& newName)
    {
        if (nameEdit && nameEdit->text() == oldName)
        {
            nameEdit->setText(newName);
        }
    }

    void NJointControllerClassesWidgetEntry::setVisible(bool vis)
    {
        header->setHidden(!vis);
    }

    void NJointControllerClassesWidgetEntry::createCtrl()
    {
        const auto instanceName = nameEdit->text().toStdString();
        const auto variants = creator->getVariants();
        if (variants.empty())
        {
            ARMARX_INFO << "creating " << instanceName << " of class " << className << " with no parameters\n";
        }
        else
        {
            std::stringstream ss;
            ss  << "creating " << instanceName << " of class " << className << " with parameters:\n";
            for (const auto& pair : variants)
            {
                if (pair.second)
                {

                    if (pair.second->data)
                    {
                        ss << "    '" << pair.first << "' of type " << pair.second->data->ice_id() << "\n";
                    }
                    else
                    {
                        ss << "    '" << pair.first << "' nullptr data \n";
                    }
                }
                else
                {
                    ss << "    '" << pair.first << "' nullptr\n";
                }
            }
            ARMARX_INFO << ss.str();
        }
        robotUnit->createNJointControllerFromVariantConfig(className, instanceName, variants);
        parent->updateDefaultNameOnControllerCreated(nameEdit->text());
    }

    void NJointControllerClassesWidget::packageEditChanged()
    {
        auto package = ui->comboBoxPackage->currentText();
        if (package.isEmpty())
        {
            selectLibMode = SelectLibsMode::LineEdit;
            ui->lineEditLibrary->setVisible(true);
            ui->comboBoxLibrary->setVisible(false);
            ui->pushButtonLoadLib->setEnabled(true);
            ui->labelPackageFound->setPixmap(QPixmap(":/icons/Blank.svg").scaled(16, 16));
            ui->labelPackageFound->setToolTip("");
        }
        else
        {
            libShortNameToFileName.clear();
            ui->comboBoxLibrary->clear();

            selectLibMode = SelectLibsMode::ComboBox;
            ui->lineEditLibrary->setVisible(false);
            ui->comboBoxLibrary->setVisible(true);
            CMakePackageFinder pFinder(package.toStdString());
            if (pFinder.packageFound())
            {
                ui->pushButtonLoadLib->setEnabled(true);
                ui->labelPackageFound->setPixmap(QPixmap(":/icons/user-online.svg").scaled(16, 16));
                ui->labelPackageFound->setToolTip("Found Package");
                int libidx = -1;
                for (const std::string& lib : Split(pFinder.getLibs(), ",; ", true))
                {
                    if (lib.empty())
                    {
                        return;
                    }
                    const auto libPrefix = lib.find("lib");
                    const auto libSubstrStart = libPrefix == lib.npos ? 0 : libPrefix + 3;
                    const auto libSuffix = lib.find(".");
                    const auto libSubstrEnd = libSuffix - libSubstrStart;
                    std::string shortName = lib.substr(libSubstrStart, libSubstrEnd);
                    libShortNameToFileName[shortName] = lib;
                    ui->comboBoxLibrary->addItem(QString::fromStdString(shortName));
                    if (libidx == -1 && (lib.find("Controller") != lib.npos || lib.find("controller") != lib.npos))
                    {
                        libidx = libShortNameToFileName.size() - 1 ;
                    }
                    ui->comboBoxLibrary->setCurrentIndex(libidx);
                }
            }
            else
            {
                ui->pushButtonLoadLib->setEnabled(false);
                ui->labelPackageFound->setPixmap(QPixmap(":/icons/dialog-cancel-5.svg").scaled(16, 16));
                ui->labelPackageFound->setToolTip("Cannot find Package");
            }
        }
    }

    void NJointControllerClassesWidget::loadLibClicked()
    {
        if (!robotUnit)
        {
            return;
        }

        switch (selectLibMode)
        {
            case SelectLibsMode::LineEdit:
            {
                auto lib = ui->lineEditLibrary->text().toStdString();
                if (lib.empty())
                {
                    return;
                }
                ARMARX_INFO << "requesting to load lib " << lib
                            << " -> " << robotUnit->loadLibFromPath(lib);
            }
            break;

            case SelectLibsMode::ComboBox:
            {
                auto package = ui->comboBoxPackage->currentText().toStdString();
                auto lib = ui->comboBoxLibrary->currentText().toStdString();
                if (lib.empty())
                {
                    return;
                }
                if (libShortNameToFileName.count(lib))
                {
                    lib = libShortNameToFileName.at(lib);
                }
                ARMARX_INFO << "requesting to load lib " << lib << " from package " << package
                            << " -> " << robotUnit->loadLibFromPackage(package, lib);
            }
            break;
            default:
                ARMARX_WARNING << "Load lib  for given SelectLibsMode nyi";
                break;
        }
    }

}
