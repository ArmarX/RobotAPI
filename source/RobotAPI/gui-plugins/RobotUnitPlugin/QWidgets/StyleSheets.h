/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::RobotUnitPlugin
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QString>
#include <QColor>

namespace armarx
{
    inline QString checkboxStyleSheet()
    {
        return
            "QCheckBox::indicator {\n"                              \
            "    width: 10px;\n"                                    \
            "    height: 10px;\n"                                   \
            "}\n"                                                   \
            "QCheckBox::indicator:checked\n"                        \
            "{\n"                                                   \
            "    image: url(:/icons/TriangleBlackDown.svg);\n"      \
            "}\n"                                                   \
            "QCheckBox::indicator:unchecked\n"                      \
            "{\n"                                                   \
            "    image: url(:/icons/TriangleBlackRight.svg);\n"     \
            "}\n"                                                   \
            "QCheckBox::indicator:checked:hover\n"                  \
            "{\n"                                                   \
            "    image: url(:/icons/TriangleBlackDown.svg);\n"      \
            "}\n"                                                   \
            "QCheckBox::indicator:unchecked:hover\n"                \
            "{\n"                                                   \
            "    image: url(:/icons/TriangleBlackRight.svg);\n"     \
            "}\n"                                                   \
            "QCheckBox::indicator:checked:pressed\n"                \
            "{\n"                                                   \
            "    image: url(:/icons/TriangleBlackDown.svg);\n"      \
            "}\n"                                                   \
            "QCheckBox::indicator:unchecked:pressed\n"              \
            "{\n"                                                   \
            "    image: url(:/icons/TriangleBlackRight.svg);\n"     \
            "}\n"                                                   \
            "QCheckBox::indicator:checked:disabled\n"               \
            "{\n"                                                   \
            "    image: url(:/icons/TriangleGrayDown.svg);\n"       \
            "}\n"                                                   \
            "QCheckBox::indicator:unchecked:disabled\n"             \
            "{\n"                                                   \
            "    image: url(:/icons/TriangleGrayRight.svg);\n"      \
            "}";
    }


    inline QColor green()
    {
        return {0, 255, 0};
    }
    inline QColor red()
    {
        return {240, 128, 128};
    }
    inline QColor orange()
    {
        return {255, 193, 37};
    }
    inline QColor transparent()
    {
        return {0, 0, 0, 0};
    }
}

