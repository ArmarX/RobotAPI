/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::RobotUnitPlugin
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ControlDevicesWidget.h"

#include "StyleSheets.h"

#include <QCheckBox>

namespace armarx
{
    ControlDevicesWidget::ControlDevicesWidget(QWidget* parent) :
        RobotUnitWidgetTemplateBase("ControlDevicesWidget", parent)
    {
        ui->treeWidget->setColumnCount(8);
        QTreeWidgetItem* head = ui->treeWidget->headerItem();
        head->setText(idxName,   "Name");
        head->setText(idxTags,   "Tags");
        head->setText(idxMode,   "Mode");
        head->setText(idxHWMode, "HardwareMode");
        head->setText(idxAct,    "Act");
        head->setText(idxReq,    "Req");
        head->setText(idxType,   "Target Type");
        head->setText(idxVal,    "Target Value");

        head->setToolTip(idxName,   "The control device's name (green: ok, red: missmatch between requested and active mode, orange: active/requested device not found in the displayed list)");
        head->setToolTip(idxTags,   "The control device's tags");
        head->setToolTip(idxMode,   "The control device's control mode");
        head->setToolTip(idxHWMode, "The control device's hardware control mode");
        head->setToolTip(idxAct,    "Whether the control mode is active (green: ok, red: missmatch between requested and active mode)");
        head->setToolTip(idxReq,    "Whether the control mode is requested (green: ok, red: missmatch between requested and active mode)");
        head->setToolTip(idxType,   "The control mode's target type");
        head->setToolTip(idxVal,    "The control mode's target value");

        ui->treeWidget->setColumnWidth(idxName,   150);
        ui->treeWidget->setColumnWidth(idxTags,   100);
        ui->treeWidget->setColumnWidth(idxMode,   225);
        ui->treeWidget->setColumnWidth(idxHWMode, 110);
        ui->treeWidget->setColumnWidth(idxAct,    40);
        ui->treeWidget->setColumnWidth(idxReq,    40);
        ui->treeWidget->setColumnWidth(idxType,   350);
        ui->treeWidget->header()->setResizeMode(idxAct, QHeaderView::Fixed);
        ui->treeWidget->header()->setResizeMode(idxReq, QHeaderView::Fixed);
    }

    ControlDevicesWidget::~ControlDevicesWidget()
    {
        delete ui;
    }

    void ControlDevicesWidget::controlDeviceStatusChanged(const ControlDeviceStatusSeq& allStatus)
    {
        std::unique_lock<std::recursive_timed_mutex> guard {mutex, std::defer_lock};
        if (!guard.try_lock_for(std::chrono::microseconds(100)))
        {
            return;
        }
        for (const auto& status : allStatus)
        {
            if (statusUpdates[status.deviceName].timestampUSec < status.timestampUSec)
            {
                statusUpdates[status.deviceName] = status;
                if (doMetaCall)
                {
                    QMetaObject::invokeMethod(this, "updateContent", Qt::QueuedConnection);
                }
            }
        }
    }

    void ControlDevicesWidget::clearAll()
    {
        entries.clear();
        statusUpdates.clear();
        resetData.clear();
    }

    void ControlDevicesWidget::doContentUpdate()
    {
        for (const auto& pair : statusUpdates)
        {
            update(pair.second);
        }
        statusUpdates.clear();
    }

    void ControlDevicesWidget::getResetData()
    {
        auto temp = robotUnit->getControlDeviceDescriptions();
        {
            std::unique_lock<std::recursive_timed_mutex> guard {mutex};
            resetData = std::move(temp);
        }
    }

    bool ControlDevicesWidget::addOneFromResetData()
    {
        if (resetData.empty())
        {
            return true;
        }
        add(resetData.back());
        resetData.pop_back();
        return false;
    }

    void ControlDevicesWidget::add(const ControlDeviceDescription& desc)
    {
        std::unique_lock<std::recursive_timed_mutex> guard {mutex};
        if (entries.count(desc.deviceName))
        {
            return;
        }
        entries[desc.deviceName] = new ControlDevicesWidgetEntry(*this, *(ui->treeWidget), desc);
    }

    void ControlDevicesWidget::update(const ControlDeviceStatus& status)
    {
        std::unique_lock<std::recursive_timed_mutex> guard {mutex};
        if (!robotUnit || ! robotUnit->isRunning())
        {
            return;
        }
        if (!entries.count(status.deviceName))
        {
            add(robotUnit->getControlDeviceDescription(status.deviceName));
        }
        entries.at(status.deviceName)->update(status);
    }

    ControlDevicesWidgetEntry::ControlDevicesWidgetEntry(ControlDevicesWidget& parent, QTreeWidget& treeWidget, const ControlDeviceDescription& desc)
        : QObject {&parent}
    {
        header = new QTreeWidgetItem;
        treeWidget.addTopLevelItem(header);
        header->setText(ControlDevicesWidget::idxName, QString::fromStdString(desc.deviceName));
        //tags
        {
            QCheckBox* boxTags = new QCheckBox{QString::number(desc.tags.size()) + " Tags"};
            treeWidget.setItemWidget(header, ControlDevicesWidget::idxTags, boxTags);
            boxTags->setStyleSheet(checkboxStyleSheet());
            boxTags->setChecked(false);
            connect(boxTags, SIGNAL(clicked(bool)), this, SLOT(hideTagList(bool)));
            for (const auto& tag : desc.tags)
            {
                QTreeWidgetItem* child = new QTreeWidgetItem {{QString::fromStdString(tag)}};
                treeWidget.addTopLevelItem(child);
                tags.emplace_back(child);
            }
        }
        for (const auto& nameToType : desc.contolModeToTargetType)
        {
            ControllerEntry& subEntry = subEntries[nameToType.first];
            subEntry.child = new QTreeWidgetItem;
            header->addChild(subEntry.child);
            subEntry.child->setText(ControlDevicesWidget::idxMode, QString::fromStdString(nameToType.first));
            subEntry.child->setText(ControlDevicesWidget::idxHWMode, QString::fromStdString(nameToType.second.hardwareControlMode));
            subEntry.child->setText(ControlDevicesWidget::idxAct, " ");
            subEntry.child->setText(ControlDevicesWidget::idxReq, " ");
            subEntry.child->setText(ControlDevicesWidget::idxType, QString::fromStdString(nameToType.second.targetType));
            subEntry.value = new VariantWidget;
            treeWidget.setItemWidget(subEntry.child, ControlDevicesWidget::idxVal, subEntry.value);
        }
    }

    void ControlDevicesWidgetEntry::update(const ControlDeviceStatus& status)
    {
        auto colorNew = (status.activeControlMode == status.requestedControlMode) ? green() : red();
        bool newDevsNotFound = false;
        if (activeMode != status.activeControlMode)
        {
            if (subEntries.count(activeMode))
            {
                subEntries.at(activeMode).child->setText(ControlDevicesWidget::idxAct, " ");
                subEntries.at(activeMode).child->setBackgroundColor(ControlDevicesWidget::idxAct, transparent());
            }
            activeMode = status.activeControlMode;
            if (subEntries.count(activeMode))
            {
                newDevsNotFound = true;
                subEntries.at(activeMode).child->setText(ControlDevicesWidget::idxAct, "X");
            }
        }
        if (requestedMode != status.requestedControlMode)
        {
            if (subEntries.count(requestedMode))
            {
                subEntries.at(requestedMode).child->setText(ControlDevicesWidget::idxReq, " ");
                subEntries.at(requestedMode).child->setBackgroundColor(ControlDevicesWidget::idxReq, transparent());
            }
            requestedMode = status.requestedControlMode;
            if (subEntries.count(requestedMode))
            {
                newDevsNotFound = true;
                subEntries.at(requestedMode).child->setText(ControlDevicesWidget::idxReq, "X");
            }
        }
        //color
        {
            //set the color here, since active may change without requested changen (or the other way) -> above there is no update
            if (subEntries.count(requestedMode))
            {
                subEntries.at(requestedMode).child->setBackgroundColor(ControlDevicesWidget::idxReq, colorNew);
            }
            if (subEntries.count(activeMode))
            {
                subEntries.at(activeMode).child->setBackgroundColor(ControlDevicesWidget::idxAct, colorNew);
            }
            header->setBackgroundColor(ControlDevicesWidget::idxName, newDevsNotFound ? orange() : colorNew);
        }
        for (const auto& pair : status.controlTargetValues)
        {
            if (subEntries.count(pair.first))
            {
                subEntries.at(pair.first).value->update(pair.second);
                subEntries.at(pair.first).value->layout()->setContentsMargins(0, 0, 0, 0);
            }
        }
    }

    bool ControlDevicesWidgetEntry::matchName(const QString& name)
    {
        return header->text(ControlDevicesWidget::idxName).contains(name, Qt::CaseInsensitive);
    }

    bool ControlDevicesWidgetEntry::matchTag(const QString& tag)
    {
        for (const auto& tagit : tags)
        {
            if (tagit->text(ControlDevicesWidget::idxTags).contains(tag, Qt::CaseInsensitive))
            {
                return true;
            }
        }
        return false;
    }

    std::set<QTreeWidgetItem*> ControlDevicesWidgetEntry::matchMode(const QString& mode)
    {
        std::set<QTreeWidgetItem*> hits;
        for (const auto& pair : subEntries)
        {
            if (pair.second.child->text(ControlDevicesWidget::idxMode).contains(mode, Qt::CaseInsensitive))
            {
                hits.emplace(pair.second.child);
            }
        }
        return hits;
    }

    std::set<QTreeWidgetItem*> ControlDevicesWidgetEntry::isActiveState(const QString& state)
    {
        std::set<QTreeWidgetItem*> hits;
        for (const auto& pair : subEntries)
        {
            if (pair.second.child->text(ControlDevicesWidget::idxAct).contains(state, Qt::CaseInsensitive))
            {
                hits.emplace(pair.second.child);
            }
        }
        return hits;
    }

    std::set<QTreeWidgetItem*> ControlDevicesWidgetEntry::isRequestedState(const QString& state)
    {
        std::set<QTreeWidgetItem*> hits;
        for (const auto& pair : subEntries)
        {
            if (pair.second.child->text(ControlDevicesWidget::idxReq).contains(state, Qt::CaseInsensitive))
            {
                hits.emplace(pair.second.child);
            }
        }
        return hits;
    }

    std::set<QTreeWidgetItem*> ControlDevicesWidgetEntry::matchTargetType(const QString& type)
    {
        std::set<QTreeWidgetItem*> hits;
        for (const auto& pair : subEntries)
        {
            if (pair.second.child->text(ControlDevicesWidget::idxType).contains(type, Qt::CaseInsensitive))
            {
                hits.emplace(pair.second.child);
            }
        }
        return hits;
    }

    void ControlDevicesWidgetEntry::setVisible(bool vis)
    {
        if (!vis)
        {
            hideTagList(true);
        }
        header->setHidden(!vis);
    }

    void ControlDevicesWidgetEntry::setChildVis(bool vis, std::set<QTreeWidgetItem*> children)
    {
        for (const auto& pair : subEntries)
        {
            if (children.count(pair.second.child))
            {
                pair.second.child->setHidden(!vis);
            }
            else
            {
                pair.second.child->setHidden(vis);
            }
        }
    }

    void ControlDevicesWidgetEntry::hideTagList(bool hide)
    {
        for (QTreeWidgetItem* tag : tags)
        {
            tag->setHidden(hide);
        }
    }
}
