/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::RobotUnitPlugin
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <atomic>

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QPushButton>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QCheckBox>
#include <QComboBox>

#include <ArmarXGui/libraries/WidgetDescription/WidgetDescription.h>
#include <ArmarXGui/libraries/VariantWidget/VariantWidget.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

#include "RobotUnitWidgetBase.h"
#include <RobotAPI/gui-plugins/RobotUnitPlugin/ui_NJointControllerClassesWidget.h>

namespace armarx
{

    class NJointControllerClassesWidgetEntry;

    class NJointControllerClassesWidget : public RobotUnitWidgetTemplateBase<Ui::NJointControllerClassesWidget>
    {
        Q_OBJECT

    public:
        explicit NJointControllerClassesWidget(QWidget* parent = 0);
        ~NJointControllerClassesWidget() override;

        virtual void nJointControllerClassAdded(std::string name);

        virtual void updateDefaultNameOnControllerCreated(QString createdName, bool force = false);
        QString getDefaultName() const;

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
    protected:
        void clearAll() override;
        void doContentUpdate() override;
        void getResetData() override;
        bool addOneFromResetData() override;

    private slots:
        void filterUpdated();
        void packageEditChanged();
        void loadLibClicked();
    private:
        void add(const NJointControllerClassDescription& desc);
        void addFilter() override;

        std::map<std::string, NJointControllerClassesWidgetEntry*> entries;
        std::map<std::string, NJointControllerClassDescription> nJointControllerClassDescriptions;

        QComboBox* filterCombination;

        QLineEdit* filterName;
        QCheckBox* filterNameActive;
        QCheckBox* filterNameInverted;

        QComboBox* filterRemoteCreation;
        QCheckBox* filterRemoteCreationActive;

        //selecting libs
        enum class SelectLibsMode
        {
            LineEdit,
            ComboBox
        };
        SelectLibsMode selectLibMode;
        std::map<std::string, std::string> libShortNameToFileName;

        int defaultControllerName {0};
    };

    //helper
    class NJointControllerClassesWidgetEntry : public QObject
    {
        Q_OBJECT
    public:
        NJointControllerClassesWidgetEntry(
            NJointControllerClassesWidget& parent,
            QTreeWidget& treeWidget,
            const  NJointControllerClassDescription& desc,
            RobotUnitInterfacePrx robotUnit
        );

        bool matchName(const QString& name);
        bool hasRemoteCreation();

        void updateDefaultName(const QString& oldName, const QString& newName);

    public slots:
        void setVisible(bool vis);

    private slots:
        void createCtrl();

    public:
        std::string className;
        QString classNameQStr;
        RobotUnitInterfacePrx robotUnit;
        QTreeWidgetItem* header {nullptr};
        QLineEdit* nameEdit {nullptr};
        WidgetDescription::DescribedWidgetBase* creator {nullptr};
        NJointControllerClassesWidget* parent;
    };
}
