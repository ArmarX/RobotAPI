/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::RobotUnitPlugin
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2017
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <atomic>

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QPushButton>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QTreeWidgetItem>

#include <ArmarXGui/libraries/WidgetDescription/WidgetDescription.h>
#include <ArmarXGui/libraries/VariantWidget/VariantWidget.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

#include "RobotUnitWidgetBase.h"
#include <RobotAPI/gui-plugins/RobotUnitPlugin/ui_SensorDevicesWidget.h>

namespace armarx
{
    class SensorDevicesWidgetEntry;

    class SensorDevicesWidget : public RobotUnitWidgetTemplateBase<Ui::SensorDevicesWidget>
    {
    public:
        explicit SensorDevicesWidget(QWidget* parent = 0);
        ~SensorDevicesWidget() override;
        void sensorDeviceStatusChanged(const SensorDeviceStatusSeq& allStatus);

    protected:
        void clearAll() override;
        void doContentUpdate() override;
        void getResetData() override;
        bool addOneFromResetData() override;
    private:
        void add(const SensorDeviceDescription& desc);

        std::map<std::string, SensorDevicesWidgetEntry*> entries;
        std::map<std::string, SensorDeviceStatus> statusUpdates;
        SensorDeviceDescriptionSeq resetData;

    public:
        static constexpr int idxName    = 0;
        static constexpr int idxTags    = 1;
        static constexpr int idxValType = 2;
        static constexpr int idxValue   = 3;
    };

    class SensorDevicesWidgetEntry : QObject
    {

        Q_OBJECT
    public:
        SensorDevicesWidgetEntry(
            SensorDevicesWidget& parent,
            QTreeWidget& treeWidget,
            const SensorDeviceDescription& desc
        );

        void update(const SensorDeviceStatus& status);

        bool matchName(const QString& name);
        bool matchTag(const QString& tag);
        bool matchValueType(const QString& type);

    public slots:
        void setVisible(bool vis);

    private slots:
        void hideTagList(bool hide);

    private:
        QTreeWidgetItem* header;
        VariantWidget* value;
        std::vector<QTreeWidgetItem*> tags;
    };
}

