/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "MatrixDatafieldDisplayWidget.h"

#include <QPainter>
#include <pthread.h>
#include <iostream>

#include <RobotAPI/libraries/core/observerfilters/MatrixFilters.h>
#include <RobotAPI/libraries/core/observerfilters/OffsetFilter.h>

namespace armarx
{
    void MatrixDatafieldDisplayWidget::updateRequested()
    {
        mtx.lock();
        this->data = matrixDatafieldOffsetFiltered->getDataField()->get<MatrixFloat>()->toEigen();
        this->percentiles = percentilesDatafield->getDataField()->get<MatrixFloat>()->toVector();
        mtx.unlock();
        update();
    }

    MatrixDatafieldDisplayWidget::MatrixDatafieldDisplayWidget(DatafieldRefBasePtr matrixDatafield, ObserverInterfacePrx observer, QWidget* parent) :
        QWidget(parent)
    {
        this->matrixDatafield = DatafieldRefPtr::dynamicCast(matrixDatafield);
        this->observer = observer;
        this->min = 0;
        this->max = 1;
        this->data = MatrixXf(1, 1);
        this->data(0, 0) = 0;
        enableOffsetFilter(false);
        QColor c[] = {QColor::fromHsv(0, 0, 0), QColor::fromHsv(240, 255, 255), QColor::fromHsv(270, 255, 255), QColor::fromHsv(300, 255, 255),
                      QColor::fromHsv(0, 255, 255), QColor::fromHsv(30, 255, 255), QColor::fromHsv(60, 255, 255)
                     };
        this->colors = std::valarray<QColor>(c, sizeof c / sizeof c[0]);

        //connect(this, SIGNAL(updateData(MatrixXf)), SLOT(setData(MatrixXf)), Qt::QueuedConnection);
        connect(this, SIGNAL(doUpdate()), SLOT(updateRequested()), Qt::QueuedConnection);
    }

    MatrixDatafieldDisplayWidget::~MatrixDatafieldDisplayWidget()
    {
        //delete ui;
    }

    void MatrixDatafieldDisplayWidget::paintEvent(QPaintEvent*)
    {
        mtx.lock();

        int paddingBottom = 40;
        QPainter painter(this);
        painter.fillRect(rect(), QColor::fromRgb(0, 0, 0));
        int matrixHeight = (height() - paddingBottom) * 8 / 10;
        int percentilesHeight = (height() - paddingBottom) - matrixHeight;
        drawMatrix(QRect(0, 0, width(), matrixHeight), painter);
        drawPercentiles(QRect(0, matrixHeight, width() - 1, percentilesHeight), painter);

        painter.setPen(QColor(Qt::GlobalColor::gray));
        painter.setFont(QFont("Arial", 12));
        painter.drawText(rect(), Qt::AlignBottom | Qt::AlignRight, infoOverlay);

        mtx.unlock();
    }

    void MatrixDatafieldDisplayWidget::enableOffsetFilter(bool enabled)
    {
        if (enabled)
        {
            this->matrixDatafieldOffsetFiltered = DatafieldRefPtr::dynamicCast(observer->createFilteredDatafield(DatafieldFilterBasePtr(new filters::OffsetFilter()), matrixDatafield));
        }
        else
        {
            this->matrixDatafieldOffsetFiltered = this->matrixDatafield;
        }

        this->percentilesDatafield = DatafieldRefPtr::dynamicCast(observer->createFilteredDatafield(DatafieldFilterBasePtr(new filters::MatrixPercentilesFilter(50)), matrixDatafieldOffsetFiltered));
    }

    QColor MatrixDatafieldDisplayWidget::getColor(float value, float min, float max)
    {
        value = (value - min) / (max - min) * (colors.size() - 1);

        if (value < 0)
        {
            return colors[0];
        }

        if (value >= colors.size() - 1)
        {
            return colors[colors.size() - 1];
        }

        int i = (int)value;
        float f2 = value - i;
        float f1 = 1 - f2;
        QColor c1 = colors[i];
        QColor c2 = colors[i + 1];
        return QColor((int)(c1.red() * f1 + c2.red() * f2), (int)(c1.green() * f1 + c2.green() * f2), (int)(c1.blue() * f1 + c2.blue() * f2));
    }

    void MatrixDatafieldDisplayWidget::drawMatrix(const QRect& target, QPainter& painter)
    {
        int pixelSize = std::min(target.width() / data.cols(), target.height() / data.rows());
        int dx = (target.width() - pixelSize * data.cols()) / 2;
        int dy = (target.height() - pixelSize * data.rows()) / 2;
        painter.setFont(QFont("Arial", 8));
        painter.setPen(QColor(Qt::GlobalColor::gray));

        for (int x = 0; x < data.cols(); x++)
        {
            for (int y = 0; y < data.rows(); y++)
            {
                QRect target = QRect(dx + x * pixelSize, dy + y * pixelSize, pixelSize, pixelSize);
                painter.fillRect(target, getColor(data(y, x), min, max));
                painter.drawText(target, Qt::AlignCenter, QString::number(data(y, x)));
            }
        }
    }

    void MatrixDatafieldDisplayWidget::drawPercentiles(const QRect& target, QPainter& painter)
    {
        painter.setPen(QColor(Qt::GlobalColor::gray));
        painter.drawRect(target);
        painter.setPen(QColor(Qt::GlobalColor::red));

        for (int i = 0; i < (int)percentiles.size() - 1; i++)
        {
            int x1 = i * target.width() / (percentiles.size() - 1);
            int x2 = (i + 1) * target.width() / (percentiles.size() - 1);
            float y1 = (percentiles.at(i) - min) / (max - min) * target.height();
            float y2 = (percentiles.at(i + 1) - min) / (max - min) * target.height();
            painter.drawLine(target.left() + x1, target.bottom() - (int)y1, target.left() + x2, target.bottom() - (int)y2);
        }
    }
}
