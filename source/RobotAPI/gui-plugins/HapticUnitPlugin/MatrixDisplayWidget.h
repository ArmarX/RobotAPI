/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <QWidget>
#include <QMutex>
#include <Eigen/Dense>
#include <valarray>

using Eigen::MatrixXf;

namespace Ui
{
    class MatrixDisplayWidget;
}

namespace armarx
{
    class MatrixDisplayWidget : public QWidget
    {
        Q_OBJECT

    signals:
        void doUpdate();

    public slots:
        void setData(MatrixXf data)
        {
            mtx.lock();
            this->data = data;
            mtx.unlock();
            //this->update();
        }

    public:
        explicit MatrixDisplayWidget(QWidget* parent = 0);
        ~MatrixDisplayWidget() override;
        void paintEvent(QPaintEvent*) override;



        void setRange(float min, float max)
        {
            this->min = min;
            this->max = max;
        }
        QColor getColor(float value, float min, float max);

        void invokeUpdate()
        {
            emit doUpdate();
        }
        void setInfoOverlay(QString infoOverlay)
        {
            mtx.lock();
            this->infoOverlay = infoOverlay;
            mtx.unlock();
        }

    private:
        Ui::MatrixDisplayWidget* ui;
        MatrixXf data;
        float min, max;
        std::valarray<QColor> colors;
        QMutex mtx;
        QString infoOverlay;
    };
}

