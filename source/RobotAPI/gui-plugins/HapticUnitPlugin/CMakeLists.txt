armarx_set_target("HapticUnitUnitGuiPlugin")

set(SOURCES
    HapticUnitGuiPlugin.cpp
    HapticUnitConfigDialog.cpp
    MatrixDisplayWidget.cpp
    MatrixDatafieldDisplayWidget.cpp
)

set(HEADERS
    HapticUnitGuiPlugin.h
    HapticUnitConfigDialog.h
    MatrixDisplayWidget.h
    MatrixDatafieldDisplayWidget.h
)

set(GUI_MOC_HDRS
    HapticUnitGuiPlugin.h
    HapticUnitConfigDialog.h
    MatrixDisplayWidget.h
    MatrixDatafieldDisplayWidget.h
)

set(GUI_UIS
    HapticUnitGuiPlugin.ui
    HapticUnitConfigDialog.ui
    MatrixDisplayWidget.ui
)

set(COMPONENT_LIBS RobotAPIUnits ArmarXCoreInterfaces ArmarXCoreEigen3Variants)

if (ArmarXGui_FOUND)
	armarx_gui_library(HapticUnitGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}" )
endif()
