/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Component::ObjectExaminerGuiPlugin
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @copyright  2012
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#pragma once

/* ArmarX headers */
#include <RobotAPI/gui-plugins/HapticUnitPlugin/ui_HapticUnitGuiPlugin.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXCore/observers/Observer.h>

/* Qt headers */
#include <QMainWindow>
#include <QtCore/QTimer>
#include <RobotAPI/interface/units/WeissHapticUnit.h>

#include <string>
#include <QLayout>
#include "MatrixDatafieldDisplayWidget.h"


namespace armarx
{
    class HapticUnitConfigDialog;

    class HapticUnitGuiPlugin :
        public ArmarXGuiPlugin
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
    public:
        HapticUnitGuiPlugin();
        QString getPluginName() override
        {
            return "HapticUnitGuiPlugin";
        }
    };

    /*!
      \page RobotAPI-GuiPlugins-HapticUnitPlugin HapticUnitPlugin
      \brief With this widget the HapticUnit can be controlled.
      */
    class HapticUnitWidget :
        public ArmarXComponentWidgetControllerTemplate<HapticUnitWidget>
    {
        Q_OBJECT
    public:
        HapticUnitWidget();
        ~HapticUnitWidget() override
        {}

        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;
        void onDisconnectComponent() override;

        // inherited of ArmarXWidget
        static QString GetWidgetName()
        {
            return "RobotControl.HapticUnitGUI";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon("://icons/haptic-hand.png");
        }

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        void configured() override;

    public slots:
        void updateData();
        void onContextMenu(QPoint pos);

    signals:
        void doUpdateDisplayWidgets();

    private slots:
        void updateDisplayWidgets();
        void onCheckBoxOffsetFilterStateChanged(int state);

    protected:
        void connectSlots();

        Ui::HapticUnitGuiPlugin ui;

    private:

        void createMatrixWidgets();

    private:

        std::string hapticObserverProxyName;
        std::string hapticUnitProxyName;
        ObserverInterfacePrx hapticObserverProxy;
        WeissHapticUnitInterfacePrx weissHapticUnit;

        QPointer<QWidget> __widget;
        QPointer<HapticUnitConfigDialog> dialog;

        QTimer* updateTimer;

        std::map<std::string, MatrixDatafieldDisplayWidget*> matrixDisplays;
        std::map<MatrixDatafieldDisplayWidget*, std::string> channelNameReverseMap;
        std::map<MatrixDatafieldDisplayWidget*, std::string> deviceNameReverseMap;

    };
    //using HapticUnitGuiPluginPtr = std::shared_ptr<HapticUnitWidget>;
}

