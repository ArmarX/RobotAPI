/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::ArVizWidgetController
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/gui-plugins/ArViz/ui_ArVizWidget.h>

#include <RobotAPI/components/ArViz/Coin/Visualizer.h>
#include <RobotAPI/interface/ArViz/Component.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include "LayerInfoTree.h"


namespace armarx
{
    enum class ArVizWidgetMode
    {
        NotConnected,
        Live,
        Recording,
        ReplayingManual,
        ReplayingTimed,
    };

    struct ArVizWidgetBatchCallback;


    /**
    \page RobotAPI-GuiPlugins-ArViz ArViz
    \brief The ArViz allows visualizing ...

    \image html ArViz.png
    The user can

    API Documentation \ref ArVizWidgetController

    \see ArVizGuiPlugin
    */

    /**
     * \class ArVizWidgetController
     * \brief ArVizWidgetController brief one line description
     *
     * Detailed description
     */
    struct ARMARXCOMPONENT_IMPORT_EXPORT
        ArVizWidgetController
        : armarx::ArmarXComponentWidgetControllerTemplate < ArVizWidgetController >
    {
        Q_OBJECT

    public:

        /// Controller Constructor
        explicit ArVizWidgetController();

        /// Controller destructor
        virtual ~ArVizWidgetController() override;

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;

        SoNode* getScene() override;

        /// Returns the Widget name displayed in the ArmarXGui to create an instance of this class.
        static QString GetWidgetName()
        {
            return "Visualization.ArViz";
        }

        /// @see armarx::Component::onInitComponent()
        void onInitComponent() override;
        void onExitComponent() override;

        void onConnectComponent() override;
        void onDisconnectComponent() override;

        void onGetBatchAsync(viz::data::RecordingBatch const& batch);


    public slots:
        /* QT slot declarations */

    signals:
        /* QT signal declarations */

        void connectGui();
        void disconnectGui();


    private:

        void onConnectGui();
        void onDisconnectGui();

        void layersChanged(std::vector<viz::CoinLayerID> const& layers);
        void layerTreeChanged(QTreeWidgetItem* item, int column);
        void updateSelectedLayer(QTreeWidgetItem* current, QTreeWidgetItem* previous);

        void onCollapseAll(bool);
        void onExpandAll(bool);

        void onHideAll(bool);
        void onShowAll(bool);
        void onHideFiltered(bool);
        void onShowFiltered(bool);

        void onFilterTextChanged(QString const& filter);

        void showAllLayers(bool visible);
        void showFilteredLayers(bool visible);

        void onDeselectElement();
        void onContextMenuClicked();
        void onInteractiveElementSelected(QListWidgetItem* item);

        void onUpdate();
        void onTimingObserverUpdate();


        // Record & Replay

        void onStartRecording();
        void onStopRecording();

        void onRefreshRecordings();
        void onRecordingSelectionChanged(QListWidgetItem* current, QListWidgetItem* previous);

        void onReplaySpinChanged(int newValue);
        void onReplaySliderChanged(int newValue);

        void selectRecording(viz::data::Recording const& recording);

        void onReplayStart(bool);
        void onReplayStop(bool);
        long replayToRevision(long revision);
        long getRevisionForTimestamp(long timestamp);
        void onReplayTimedStart(bool checked);
        void onReplayTimerTick();
        void exportToVRML();

        void changeMode(ArVizWidgetMode newMode);
        void enableWidgetAccordingToMode();


    private:

        Ui::ArVizWidget widget;

        QPointer<SimpleConfigDialog> configDialog;

        QTimer* updateTimer;
        viz::CoinVisualizer_UpdateTiming lastTiming;
        StringVariantBaseMap timingMap;

        QTimer* replayTimer;
        long replayCurrentTimestamp = 0;
        long lastReplayRealTime = 0;

        std::string storageName;
        armarx::viz::StorageInterfacePrx storage;
        std::string debugObserverName;
        DebugObserverInterfacePrx debugObserver;

        armarx::viz::CoinVisualizer visualizer;
        armarx::LayerInfoTree layerInfoTree;

        viz::data::RecordingSeq allRecordings;

        // Replay control
        viz::data::Recording currentRecording;
        long currentRevision = -1;
        long currentTimestamp = -1;
        bool currentRecordingSelected = false;

        // Recording batch cache
        struct TimestampedRecordingBatch
        {
            IceUtil::Time lastUsed;
            viz::data::RecordingBatch data;
        };

        viz::data::RecordingBatch const& getRecordingBatch(long index);
        void limitRecordingBatchCacheSize();

        std::size_t recordingBatchCacheMaxSize = 5;
        std::mutex recordingBatchCacheMutex;
        std::atomic<long> recordingWaitingForBatchIndex = -1;
        std::map<long, TimestampedRecordingBatch> recordingBatchCache;

        ArVizWidgetMode mode = ArVizWidgetMode::NotConnected;

        IceUtil::Handle<ArVizWidgetBatchCallback> callbackData;
        armarx::viz::Callback_StorageInterface_getRecordingBatchPtr callback;
        Ice::AsyncResultPtr callbackResult;

        std::vector<double> timings;

    public:
        static QIcon GetWidgetIcon()
        {
            return QIcon(":icons/ArViz.png");
        }
    };
}


