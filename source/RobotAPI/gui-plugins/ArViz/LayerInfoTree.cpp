#include "LayerInfoTree.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SimoxUtility/algorithm/string.h>

#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QLineEdit>
#include <QPushButton>
#include <QSpinBox>

#include <RobotAPI/components/ArViz/Introspection/ElementJsonSerializers.h>
#include <RobotAPI/components/ArViz/Introspection/json_base.h>
#include <RobotAPI/components/ArViz/Introspection/json_elements.h>
#include <RobotAPI/components/ArViz/Introspection/json_layer.h>


namespace armarx
{
    LayerInfoTree::LayerInfoTree()
    {
    }

    void LayerInfoTree::setWidget(QTreeWidget* widget)
    {
        if (!widgetConnections.empty())
        {
            // Disconnect connections to prior widget.
            for (const auto& conn : widgetConnections)
            {
                disconnect(conn);
            }
            widgetConnections.clear();
        }

        this->widget = widget;

        // Update layer element properties when expanded.
        widgetConnections.append(connect(widget, &QTreeWidget::itemExpanded, [this](QTreeWidgetItem * item)
        {
            if (item->parent() || this->layers == nullptr)
            {
                return;
            }

            std::string id = item->text(0).toStdString();
            auto* layer = this->layers->findLayer(this->layerID);
            if (layer != nullptr)
            {
                viz::CoinLayerElement const* element = layer->findElement(id);
                if (element != nullptr)
                {
                    this->updateLayerElementProperties(item, element->data);
                }
            }
        }));

        widget->setColumnCount(2);
        widget->setHeaderLabels({"Property", "Value"});
        widget->setHeaderHidden(false);
    }

    void LayerInfoTree::registerVisualizerCallbacks(viz::CoinVisualizer& visualizer)
    {
        visualizer.layerUpdatedCallbacks.emplace_back(
            [this](const viz::CoinLayerID & layerID, const viz::CoinLayer & layer)
        {
            if (layerID == this->layerID)
            {
                this->update();
            }
        });
    }

    void LayerInfoTree::setMaxElementCountDefault(int max)
    {
        this->maxElementCountDefault = max;
    }

    void LayerInfoTree::setEnabled(bool value)
    {
        if (this->enabled != value)
        {
            this->enabled = value;
            if (value)
            {
                update();
            }
            else
            {
                clearLayerElements();
            }
        }
    }

    void LayerInfoTree::setSelectedLayer(viz::CoinLayerID id, viz::CoinLayerMap* layers)
    {
        this->layerID = id;
        this->layers = layers;

        showMoreItem = nullptr;
        maxElementCount = maxElementCountDefault;

        widget->clear();

        update();
    }


    void LayerInfoTree::update()
    {
        if (enabled && layers)
        {
            auto* layer = layers->findLayer(layerID);
            if (layer != nullptr)
            {
                updateLayerElements(layer->elements);
            }
        }
    }


    void LayerInfoTree::clearLayerElements()
    {
        if (widget)
        {
            widget->clear();
            showMoreItem = nullptr;
        }
    }


    void LayerInfoTree::updateLayerElements(const std::vector<viz::CoinLayerElement>& elements)
    {
        if (showMoreItem)
        {
            // We always delete the item up-front. If we still need it, it will be added again.
            // This way, we don't have to check for the showMoreItem while we update the entries.
            int index = widget->indexOfTopLevelItem(showMoreItem);
            ARMARX_CHECK_NONNEGATIVE(index) << "Invariant: If showMoreItem != nullptr, then the widget contains it.";
            delete widget->takeTopLevelItem(index);
            showMoreItem = nullptr;
        }
        ARMARX_CHECK_IS_NULL(showMoreItem);

        int currentIndex = 0;
        for (viz::CoinLayerElement const& element : elements)
        {
            std::string const& name = element.data->id;

            if (maxElementCount >= 0 && currentIndex >= maxElementCount)
            {
                // Stop adding more.
                addShowMoreItem();
                break;
            }

            if (currentIndex >= widget->topLevelItemCount())
            {
                // Add elements to the end of the list.
                QTreeWidgetItem* item = insertLayerElementItem(currentIndex, name, element.data);
                updateLayerElementProperties(item, element.data);
                ++currentIndex;
                continue;
            }

            // Get current item.
            QTreeWidgetItem* currentItem = widget->topLevelItem(currentIndex);

            // Compare current item to name.

            while (currentItem != nullptr && name.c_str() > currentItem->text(0))
            {
                // Delete items before current item.
                delete widget->takeTopLevelItem(currentIndex);
                currentItem = widget->topLevelItem(currentIndex);
            }

            if (currentItem != nullptr && name.c_str() < currentItem->text(0))
            {
                // Insert new item before child.
                currentItem = insertLayerElementItem(currentIndex, name, element.data);
                updateLayerElementProperties(currentItem, element.data);
                ++currentIndex;
                continue;
            }

            if (currentItem != nullptr && name.c_str() == currentItem->text(0))
            {
                // Already existing.
                updateLayerElementItem(currentItem, element.data);
                if (currentItem->isExpanded())
                {
                    updateLayerElementProperties(currentItem, element.data);
                }
                ++currentIndex;
                continue;
            }
        }
    }


    QTreeWidgetItem* LayerInfoTree::insertLayerElementItem(
        int index, const std::string& name, const viz::data::ElementPtr& element)
    {
        QTreeWidgetItem* item = new QTreeWidgetItem(QStringList { name.c_str(), getTypeName(element).c_str() });
        // To be used when we can hide specific elements.
        // item->setCheckState(0, Qt::CheckState::Unchecked);
        widget->insertTopLevelItem(index, item);
        return item;
    }

    void LayerInfoTree::updateLayerElementItem(QTreeWidgetItem* item, const viz::data::ElementPtr& element)
    {
        // Update type name.
        item->setText(1, getTypeName(element).c_str());
    }


    void LayerInfoTree::updateLayerElementProperties(QTreeWidgetItem* item, const viz::data::ElementPtr& element)
    {
        bool recursive = true;
        updateJsonChildren(item, serializeElement(element), recursive);
    }


    std::string LayerInfoTree::getTypeName(const viz::data::ElementPtr& element) const
    {
        const std::string typeName = simox::meta::get_type_name(*element);
        return viz::json::ElementJsonSerializers::isTypeRegistered(typeName)
               ? typeName
               : "<unknown type '" + typeName + "'>";
    }


    nlohmann::json LayerInfoTree::serializeElement(const viz::data::ElementPtr& element) const
    {
        try
        {
            return nlohmann::json(element);
        }
        catch (const viz::error::NoSerializerForType&)
        {
            return nlohmann::json::object();
        }
    }


    void LayerInfoTree::updateJsonChildren(QTreeWidgetItem* parent, const nlohmann::json& json, bool recursive)
    {
        if (json.is_array())
        {
            nlohmann::json jmeta = nlohmann::json::object();

            int iItem = 0;
            for (auto j : json)
            {
                const std::string key = std::to_string(iItem);

                if (iItem >= parent->childCount())
                {
                    QTreeWidgetItem* item = addJsonChild(parent, key);
                    updateJsonItem(item, key, j, jmeta, recursive);
                }
                else // iItem < parent->childCount()
                {
                    QTreeWidgetItem* currentItem = parent->child(iItem);
                    updateJsonItem(currentItem, key, j, jmeta, recursive);
                }
                ++iItem;
            }
            while (parent->childCount() > int(json.size()))
            {
                parent->removeChild(parent->child(parent->childCount() - 1));
            }
        }
        else if (json.is_object())
        {
            namespace meta = viz::json::meta;
            const nlohmann::json& jmeta = json.count(meta::KEY) > 0 ? json.at(meta::KEY)
                                          : nlohmann::json::object();

            int iItem = 0;
            for (auto it : json.items())
            {
                // Hide meta entries.
                if (isMetaKey(it.key()))
                {
                    continue;
                }

                // Check meta info.
                if (jmeta.count(it.key()) > 0 && jmeta.at(it.key()) == meta::HIDE)
                {
                    continue;
                }

                if (iItem >= parent->childCount())
                {
                    QTreeWidgetItem* item = addJsonChild(parent, it.key());
                    updateJsonItem(item, it.key(), it.value(), jmeta, recursive);
                    ++iItem;
                    continue;
                }

                QTreeWidgetItem* currentItem = parent->child(iItem);

                while (it.key().c_str() > currentItem->text(0))
                {
                    // Delete items before current item.
                    parent->removeChild(currentItem);
                    currentItem = parent->child(iItem);
                }

                if (it.key().c_str() < currentItem->text(0))
                {
                    // Insert new item before child.
                    currentItem = addJsonChild(parent, it.key());
                    updateJsonItem(currentItem, it.key(), it.value(), jmeta, recursive);
                    ++iItem;
                    continue;
                }

                if (it.key().c_str() == currentItem->text(0))
                {
                    // Already existing.
                    updateJsonItem(currentItem, it.key(), it.value(), jmeta, recursive);
                    ++iItem;
                    continue;
                }
            }
        }
    }


    QTreeWidgetItem* LayerInfoTree::addJsonChild(QTreeWidgetItem* parent, const std::string& key)
    {
        QTreeWidgetItem* child = new QTreeWidgetItem(QStringList { key.c_str(), "" });
        // To be used when we can actually change the values (enabled = use value from GUI).
        // child->setCheckState(0, Qt::CheckState::Unchecked);
        parent->addChild(child);

        return child;
    }


    void LayerInfoTree::updateJsonItem(
        QTreeWidgetItem* item, const std::string& key, const nlohmann::json& value,
        const nlohmann::json& parentMeta,
        bool recursive)
    {
        const int columnKey = 0;

        // Update key.
        item->setText(columnKey, key.c_str());
        // Update value.
        updateJsonItemValue(item, key, value, parentMeta);

        // Recursion.
        if (recursive)
        {
            updateJsonChildren(item, value, recursive);
        }
    }


    void LayerInfoTree::updateJsonItemValue(
        QTreeWidgetItem* item, const std::string& key, const nlohmann::json& value,
        const nlohmann::json& parentMeta)
    {
        const int columnValue = 1;

        if (parentMeta.count(key) > 0)
        {
            const nlohmann::json& metaValue = parentMeta.at(key);
            if (metaValue.is_string())
            {
                const std::string metaString = metaValue.get<std::string>();
                if (metaString == viz::json::meta::READ_ONLY)
                {
                    item->setText(columnValue, getValuePreview(value).toString());
                    // Hide check box.
                    item->setData(0, Qt::CheckStateRole, QVariant());
                    return;
                }
            }
        }

        // No handled meta info.
        if (QWidget* w = widget->itemWidget(item, columnValue))
        {
            updateValueWidget(value, w);
        }
        else if (QWidget* w = getValueWidget(item, value); w)
        {
            widget->setItemWidget(item, columnValue, w);
        }
        else
        {
            // No widget.
            item->setText(columnValue, getValuePreview(value).toString());
        }
    }


    QVariant LayerInfoTree::getValuePreview(const nlohmann::json& json) const
    {
        std::stringstream ss;

        switch (json.type())
        {
            case nlohmann::json::value_t::null:
                return QVariant("null");

            case nlohmann::json::value_t::object:
            {
                std::vector<std::string> keys;
                for (auto it : json.items())
                {
                    if (!isMetaKey(it.key()))
                    {
                        keys.push_back(it.key());
                    }
                }
                ss << "object [" << simox::alg::join(keys, ", ") << "]";
                return QVariant(ss.str().c_str());
            }

            case nlohmann::json::value_t::array:
                ss << "array of size " << json.size();
                return QVariant(ss.str().c_str());

            case nlohmann::json::value_t::string:
                return QVariant(json.get<std::string>().c_str());
            case nlohmann::json::value_t::boolean:
                return QVariant(json.get<bool>());
            case nlohmann::json::value_t::number_integer:
                return QVariant(json.get<int>());
            case nlohmann::json::value_t::number_unsigned:
                return QVariant(json.get<unsigned int>());
            case nlohmann::json::value_t::number_float:
                return QVariant(json.get<float>());

            default:
                return QVariant("n/a");
        }
    }


    QWidget* LayerInfoTree::getValueWidget(QTreeWidgetItem* item, const nlohmann::json& json) const
    {
        auto setChecked = [item]()
        {
            item->setCheckState(0, Qt::CheckState::Checked);
        };

        switch (json.type())
        {
            case nlohmann::json::value_t::string:
            {

                QLineEdit* w = new QLineEdit(json.get<std::string>().c_str());
                connect(w, &QLineEdit::editingFinished, this, setChecked);
                return w;
            }
            case nlohmann::json::value_t::boolean:
            {
                QCheckBox* w = new QCheckBox("Enabled");
                w->setChecked(json.get<bool>());
                connect(w, &QCheckBox::stateChanged, this, setChecked);
                return w;
            }
            case nlohmann::json::value_t::number_integer:
            case nlohmann::json::value_t::number_unsigned:
            {
                QSpinBox* w = new QSpinBox();
                w->setMinimum(std::numeric_limits<int>::lowest());
                w->setMaximum(std::numeric_limits<int>::max());
                w->setValue(json.get<int>());
                connect(w, qOverload<int>(&QSpinBox::valueChanged), this, setChecked);
                return w;
            }
            case nlohmann::json::value_t::number_float:
            {
                QDoubleSpinBox* w = new QDoubleSpinBox();
                w->setMinimum(std::numeric_limits<double>::lowest());
                w->setMaximum(std::numeric_limits<double>::max());
                w->setValue(json.get<double>());
                connect(w, qOverload<double>(&QDoubleSpinBox::valueChanged), this, setChecked);
                return w;
            }
            case nlohmann::json::value_t::null:
            case nlohmann::json::value_t::object:
            case nlohmann::json::value_t::array:
                return nullptr;

            default:
                return nullptr;
        }
    }


    void LayerInfoTree::updateValueWidget(const nlohmann::json& json, QWidget* widget) const
    {
        switch (json.type())
        {
            case nlohmann::json::value_t::string:
            {
                QLineEdit* w = dynamic_cast<QLineEdit*>(widget);
                ARMARX_CHECK_NOT_NULL(w);
                w->setText(json.get<std::string>().c_str());
            }
            break;
            case nlohmann::json::value_t::boolean:
            {
                QCheckBox* w = dynamic_cast<QCheckBox*>(widget);
                ARMARX_CHECK_NOT_NULL(w);
                w->setChecked(json.get<bool>());
            }
            break;
            case nlohmann::json::value_t::number_integer:
            case nlohmann::json::value_t::number_unsigned:
            {
                QSpinBox* w = dynamic_cast<QSpinBox*>(widget);
                ARMARX_CHECK_NOT_NULL(w);
                w->setValue(json.get<int>());
            }
            break;
            case nlohmann::json::value_t::number_float:
            {
                QDoubleSpinBox* w = dynamic_cast<QDoubleSpinBox*>(widget);
                ARMARX_CHECK_NOT_NULL(w);
                w->setValue(json.get<double>());
            }
            break;
            case nlohmann::json::value_t::null:
            case nlohmann::json::value_t::object:
            case nlohmann::json::value_t::array:
            default:
                break;
        }
    }


    bool LayerInfoTree::isMetaKey(const std::string& key)
    {
        return key.substr(0, 2) == "__" && key.substr(key.size() - 2) == "__";
    }

    void LayerInfoTree::addShowMoreItem()
    {
        ARMARX_CHECK_NULL(showMoreItem) << "There should not be a showMoreItem when this function is called.";

        auto* layer = this->layers->findLayer(this->layerID);
        if (layer == nullptr)
        {
            ARMARX_WARNING << "No layer with ID '" << this->layerID.first
                           << "/" << this->layerID.second << "' found";
            return;
        }

        std::stringstream ss;
        ss << "Showing " << widget->topLevelItemCount() << " of " << layer->elements.size() << " elements.";

        // Add a new item.
        QTreeWidgetItem* item = new QTreeWidgetItem(QStringList { "", ss.str().c_str() });
        widget->addTopLevelItem(item);

        QPushButton* button = new QPushButton("Show more");
        widget->setItemWidget(item, 0, button);

        connect(button, &QPushButton::pressed, [this, layer]()
        {
            maxElementCount += maxElementCountDefault;
            this->updateLayerElements(layer->elements);
        });

        this->showMoreItem = item;
    }

}
