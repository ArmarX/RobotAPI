/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>

#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/ManagedIceObject.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

namespace Ui
{
    class HandUnitConfigDialog;
}

namespace armarx
{
    class HandUnitConfigDialog :
        public QDialog,
        virtual public ManagedIceObject
    {
        Q_OBJECT

    public:
        explicit HandUnitConfigDialog(QWidget* parent = 0);
        ~HandUnitConfigDialog() override;

    protected:
        // ManagedIceObject interface
        std::string getDefaultName() const override
        {
            return "HandUnitConfigDialog" + uuid;
        }
        void onInitComponent() override;
        void onConnectComponent() override;

    private:
        Ui::HandUnitConfigDialog* ui;

        IceProxyFinderBase* proxyFinderLeftHand;
        IceProxyFinderBase* proxyFinderRightHand;
        std::string uuid;

        friend class HandUnitWidget;

    };
}

