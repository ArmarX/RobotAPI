/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::SkillManagerMonitorWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <stack>
#include <vector>
#include <thread>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <RobotAPI/interface/skills/SkillMemoryInterface.h>

#include <RobotAPI/gui-plugins/SkillManagerPlugin/ui_SkillManagerMonitorWidget.h>

#include "aronTreeWidget/AronTreeWidgetController.h"

#include <RobotAPI/libraries/aron/core/type/variant/All.h>
#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/type/visitor/variant/VariantVisitor.h>

#include <QTimer>

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT
        SkillManagerMonitorWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < SkillManagerMonitorWidgetController >
    {
        Q_OBJECT

    public:

        /// Controller Constructor
        explicit SkillManagerMonitorWidgetController();
        /// Controller destructor
        virtual ~SkillManagerMonitorWidgetController();

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Skills.Manager";
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;

    private slots:
        void skillSelectionChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous);

        void stopSkill();
        void executeSkill();

        void updateTimerFrequency();
        void refreshSkills();

        void copyCurrentConfig();
        void pasteCurrentConfig();
        void resetCurrentConfig();

        void onTreeWidgetItemDoubleClicked(QTreeWidgetItem * item, int column);

    private:
        aron::data::DictPtr getConfigAsAron() const;

    private:
        /**
         * Widget Form
         */
        Ui::SkillManagerMonitorWidget  widget;
        QPointer<SimpleConfigDialog>   dialog;

        std::string observerName = "SkillManager";
        skills::manager::dti::SkillManagerInterfacePrx manager = nullptr;

        struct SelectedSkill
        {
            std::string providerName;
            std::string skillName;
        };

        // Data taken from observer (snapshot of it)
        skills::manager::dto::SkillDescriptionMapMap skills = {};

        // User Input
        SelectedSkill selectedSkill;

        // Helper to get the treeWidgetItem easily
        QTreeWidgetItem* skillsArgumentsTreeWidgetItem = nullptr;
        AronTreeWidgetControllerPtr aronTreeWidgetController = nullptr;

        // others
        std::atomic_bool connected = false;
        QTimer* refreshSkillsResultTimer;

        // skillExecutions
        std::vector<std::thread> executions;
    };
}


