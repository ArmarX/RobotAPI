#include "AronTreeWidgetController.h"

#include "visitors/AronTreeWidgetConverter.h"
#include "visitors/AronTreeWidgetSetter.h"

namespace armarx
{
    AronTreeWidgetController::AronTreeWidgetController(QTreeWidget* tree, QTreeWidgetItem* parent, const aron::type::ObjectPtr& type, const aron::data::DictPtr& data):
        parent(parent),
        tree(tree),
        type(type)
    {
        if (type) // if there is a type set, we create a tree widget from the typp
        {
            AronTreeWidgetCreatorVisitor v;
            aron::type::visit(v, type);

            if (v.createdQWidgetItem)
            {
                parent->addChild(v.createdQWidgetItem);
            }

            if (data) // check if there is a default argument set. Prefill the GUI with it
            {
                setFromAron(data);
            }
        }
        else if(data) // there is no type but a default configuration. Prefill the GUI with the default arguments
        {
            // create type from data, ...
        }
        else
        {
            new QTreeWidgetItem(parent, {QString::fromStdString("No args")});
        }
    }

    aron::data::DictPtr AronTreeWidgetController::convertToAron() const
    {
        if (parent)
        {
            AronTreeWidgetConverterVisitor v(parent, 0);
            aron::type::visit(v, type);

            auto aron_args = aron::data::Dict::DynamicCastAndCheck(v.createdAron);
            return aron_args;
        }
        return nullptr;
    }

    void AronTreeWidgetController::setFromAron(const aron::data::DictPtr& data)
    {
        if (parent)
        {
            AronTreeWidgetSetterVisitor v(parent, 0);
            aron::data::visit(v, data);
        }
    }
}
