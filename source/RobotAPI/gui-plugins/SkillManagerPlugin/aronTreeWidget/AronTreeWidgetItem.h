#pragma once

#include <stack>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include "Data.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <RobotAPI/libraries/aron/core/type/variant/Variant.h>

#include <QTreeWidget>

namespace armarx
{
    class AronTreeWidgetItem : public QObject, public QTreeWidgetItem
    {
        Q_OBJECT
    public:
        AronTreeWidgetItem(const AronTreeWidgetItem& other) :
            QObject(),
            QTreeWidgetItem(other)
        {
            aronType = other.aronType;
        }

        using QTreeWidgetItem::QTreeWidgetItem;

        AronTreeWidgetItem* copy(); // differs from clone!!!!

        static AronTreeWidgetItem* DynamicCast(QTreeWidgetItem*);

        static AronTreeWidgetItem* DynamicCastAndCheck(QTreeWidgetItem*);

        aron::type::VariantPtr aronType;

    };
}
