#include "AronTreeWidgetItem.h"

#include <RobotAPI/libraries/aron/core/type/variant/All.h>

namespace armarx
{
    AronTreeWidgetItem* AronTreeWidgetItem::DynamicCast(QTreeWidgetItem* i)
    {
        return dynamic_cast<AronTreeWidgetItem*>(i);
    }

    AronTreeWidgetItem* AronTreeWidgetItem::copy()
    {
        AronTreeWidgetItem* ret = new AronTreeWidgetItem(*this);
        return ret;
    }

    AronTreeWidgetItem* AronTreeWidgetItem::DynamicCastAndCheck(QTreeWidgetItem* i)
    {
        if (!i)
        {
            return nullptr;
        }
        auto c = DynamicCast(i);
        ARMARX_CHECK_NOT_NULL(c);
        return c;
    }
}
