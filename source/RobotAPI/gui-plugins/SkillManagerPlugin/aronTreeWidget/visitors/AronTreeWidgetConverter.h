/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::SkillManagerMonitorWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/libraries/aron/core/type/variant/All.h>
#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/type/visitor/variant/VariantVisitor.h>

// forward declarations of qt
class QTreeWidgetItem;


namespace armarx
{
    // Conversion from TreeView to aron data
    class AronTreeWidgetConverterVisitor :
            public armarx::aron::type::ConstVariantVisitor
    {
    public:
        QTreeWidgetItem* parentItem;
        int index;
        aron::data::VariantPtr createdAron = nullptr;

        AronTreeWidgetConverterVisitor() = delete;
        AronTreeWidgetConverterVisitor(QTreeWidgetItem* i, int x) :
            parentItem(i), index(x)
        {}

        void visitAronVariant(const aron::type::ObjectPtr&) final;
        void visitAronVariant(const aron::type::DictPtr&) final;
        void visitAronVariant(const aron::type::PairPtr&) final;
        void visitAronVariant(const aron::type::TuplePtr&) final;
        void visitAronVariant(const aron::type::ListPtr&) final;
        void visitAronVariant(const aron::type::NDArrayPtr&) final;
        void visitAronVariant(const aron::type::MatrixPtr&) final;
        void visitAronVariant(const aron::type::QuaternionPtr&) final;
        void visitAronVariant(const aron::type::ImagePtr&) final;
        void visitAronVariant(const aron::type::PointCloudPtr&) final;
        void visitAronVariant(const aron::type::IntEnumPtr&) final;
        void visitAronVariant(const aron::type::IntPtr&) final;
        void visitAronVariant(const aron::type::LongPtr&) final;
        void visitAronVariant(const aron::type::FloatPtr&) final;
        void visitAronVariant(const aron::type::DoublePtr&) final;
        void visitAronVariant(const aron::type::BoolPtr&) final;
        void visitAronVariant(const aron::type::StringPtr&) final;
        void visitUnknown(Input&) final;
    };
}


