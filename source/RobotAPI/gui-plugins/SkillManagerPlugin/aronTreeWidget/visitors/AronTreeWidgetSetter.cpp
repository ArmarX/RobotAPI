/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::SkillManagerMonitorWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2020
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>

#include "AronTreeWidgetSetter.h"

//visitors
namespace armarx
{
    bool AronTreeWidgetSetterVisitor::checkTreeWidgetItemForSimilarName(const std::string& name) const
    {
        QTreeWidgetItem* el = parentItem->child(index);
        std::string n = el->text(0).toStdString();
        if (name != n)
        {
            ARMARX_WARNING_S << "Could not set a tree widget value for the element with key '" << name << "' because it is different from the expected name '" << n << "'.";
            return false;
        }
        return true;
    }

    void AronTreeWidgetSetterVisitor::visitAronVariant(const aron::data::DictPtr& i)
    {
        if (i->getPath().size() == 0 || checkTreeWidgetItemForSimilarName(i->getPath().getLastElement())) // either it is the root or it has a name
        {
            QTreeWidgetItem* el = parentItem->child(index);

            unsigned int x = 0;
            for (const auto& [key, value] : i->getElements())
            {
                AronTreeWidgetSetterVisitor v(el, x++);
                aron::data::visit(v, value);
            }
            return;
        }
    }

    void AronTreeWidgetSetterVisitor::visitAronVariant(const aron::data::ListPtr& i)
    {
        if (checkTreeWidgetItemForSimilarName(i->getPath().getLastElement()))
        {
            QTreeWidgetItem* el = parentItem->child(index);

            unsigned int x = 0;
            for (const auto& value : i->getElements())
            {
                AronTreeWidgetSetterVisitor v(el, x++);
                aron::data::visit(v, value);
            }
        }
    }

    void AronTreeWidgetSetterVisitor::visitAronVariant(const aron::data::NDArrayPtr& i)
    {

    }

    void AronTreeWidgetSetterVisitor::visitAronVariant(const aron::data::IntPtr& i)
    {
        if (checkTreeWidgetItemForSimilarName(i->getPath().getLastElement()))
        {
            QTreeWidgetItem* el = parentItem->child(index);
            el->setText(1, QString::fromStdString(std::to_string(i->getValue())));
        }
    }

    void AronTreeWidgetSetterVisitor::visitAronVariant(const aron::data::LongPtr& i)
    {
        if (checkTreeWidgetItemForSimilarName(i->getPath().getLastElement()))
        {
            QTreeWidgetItem* el = parentItem->child(index);
            el->setText(1, QString::fromStdString(std::to_string(i->getValue())));
        }
    }

    void AronTreeWidgetSetterVisitor::visitAronVariant(const aron::data::FloatPtr& i)
    {
        if (checkTreeWidgetItemForSimilarName(i->getPath().getLastElement()))
        {
            QTreeWidgetItem* el = parentItem->child(index);
            el->setText(1, QString::fromStdString(std::to_string(i->getValue())));
        }
    }

    void AronTreeWidgetSetterVisitor::visitAronVariant(const aron::data::DoublePtr& i)
    {
        if (checkTreeWidgetItemForSimilarName(i->getPath().getLastElement()))
        {
            QTreeWidgetItem* el = parentItem->child(index);
            el->setText(1, QString::fromStdString(std::to_string(i->getValue())));
        }
    }

    void AronTreeWidgetSetterVisitor::visitAronVariant(const aron::data::BoolPtr& i)
    {
        if (checkTreeWidgetItemForSimilarName(i->getPath().getLastElement()))
        {
            QTreeWidgetItem* el = parentItem->child(index);
            el->setText(1, QString::fromStdString(std::to_string(i->getValue())));
        }
    }

    void AronTreeWidgetSetterVisitor::visitAronVariant(const aron::data::StringPtr& i)
    {
        if (checkTreeWidgetItemForSimilarName(i->getPath().getLastElement()))
        {
            QTreeWidgetItem* el = parentItem->child(index);
            el->setText(1, QString::fromStdString(i->getValue()));
        }
    }

    void AronTreeWidgetSetterVisitor::visitUnknown(Input&)
    {
        ARMARX_WARNING_S << "Received an unknown type when trying to set a skill argument type from an aron data object.";
    }
}

