/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::SkillManagerMonitorWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <stack>

#include "../AronTreeWidgetItem.h"

#include <RobotAPI/libraries/aron/core/type/variant/All.h>
#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>

namespace armarx
{
    // Conversion from TreeView to aron data
    class AronTreeWidgetSetterVisitor :
            public armarx::aron::data::ConstVariantVisitor
    {
    public:
        QTreeWidgetItem* parentItem;
        int index;
        AronTreeWidgetItem* qWidgetItem;

        AronTreeWidgetSetterVisitor() = delete;
        AronTreeWidgetSetterVisitor(QTreeWidgetItem* i, int x) :
            parentItem(i), index(x)
        {}

        virtual void visitAronVariant(const aron::data::DictPtr&) final;
        virtual void visitAronVariant(const aron::data::ListPtr&) final;
        virtual void visitAronVariant(const aron::data::NDArrayPtr&) final;
        virtual void visitAronVariant(const aron::data::IntPtr&) final;
        virtual void visitAronVariant(const aron::data::LongPtr&) final;
        virtual void visitAronVariant(const aron::data::FloatPtr&) final;
        virtual void visitAronVariant(const aron::data::DoublePtr&) final;
        virtual void visitAronVariant(const aron::data::BoolPtr&) final;
        virtual void visitAronVariant(const aron::data::StringPtr&) final;
        void visitUnknown(Input&) final;

    private:
        bool checkTreeWidgetItemForSimilarName(const std::string& name) const;
    };
}


