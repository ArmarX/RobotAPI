/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::SkillManagerMonitorWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2020
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>

#include "AronTreeWidgetModalCreator.h"

#include <SimoxUtility/algorithm/string.h>

// modals
#include "../modal/text/AronTreeWidgetTextInputModalController.h"
#include "../modal/dict/AronTreeWidgetDictInputModalController.h"

// qt
#include <QTreeWidget>

//visitors
namespace armarx
{

    void AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::ObjectPtr& i)
    {
        // should not happen, right?
    }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::DictPtr& i)
    {
        createdModal = std::make_shared<AronTreeWidgetDictInputModalController>(label, item, parent);
    }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::PairPtr& i)
    {
    }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::TuplePtr& i)
    { }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::ListPtr& i)
    { }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::NDArrayPtr& i)
    { }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::MatrixPtr& i)
    { }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::QuaternionPtr& i)
    { }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::ImagePtr& i)
    { }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::PointCloudPtr& i)
    { }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::IntEnumPtr& i)
    { }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::IntPtr& i)
    {
        createdModal = std::make_shared<AronTreeWidgetTextInputModalController>(label, item, parent);
    }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::LongPtr& i)
    {
        createdModal = std::make_shared<AronTreeWidgetTextInputModalController>(label, item, parent);
    }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::FloatPtr& i)
    {
        createdModal = std::make_shared<AronTreeWidgetTextInputModalController>(label, item, parent);
    }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::DoublePtr& i)
    {
        createdModal = std::make_shared<AronTreeWidgetTextInputModalController>(label, item, parent);
    }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::BoolPtr& i)
    {
        createdModal = std::make_shared<AronTreeWidgetTextInputModalController>(label, item, parent);
    }

    void
    AronTreeWidgetModalCreatorVisitor::visitAronVariant(const aron::type::StringPtr& i)
    {
        createdModal = std::make_shared<AronTreeWidgetTextInputModalController>(label, item, parent);
    }

    void AronTreeWidgetModalCreatorVisitor::visitUnknown(Input&)
    {
        ARMARX_WARNING_S << "Received an unknown type when trying to create a tree view widget modal for a skill argument type.";
    }
}

