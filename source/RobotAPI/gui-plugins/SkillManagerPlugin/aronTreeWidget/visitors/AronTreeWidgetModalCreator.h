/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::SkillManagerMonitorWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "../modal/AronTreeWidgetModal.h"

#include <RobotAPI/libraries/aron/core/type/variant/All.h>
#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/type/visitor/variant/VariantVisitor.h>

namespace armarx
{
    // Convert aron type to tree widget
    class AronTreeWidgetModalCreatorVisitor :
            public armarx::aron::type::ConstVariantVisitor
    {
    public:
        std::string label;
        AronTreeWidgetItem* item;
        QTreeWidget* parent;
        AronTreeWidgetModalControllerPtr createdModal;

        AronTreeWidgetModalCreatorVisitor() = delete;
        AronTreeWidgetModalCreatorVisitor(const std::string& label, AronTreeWidgetItem* item, QTreeWidget* parent) :
            label(label),
            item(item),
            parent(parent)
        {}

        void visitAronVariant(const aron::type::ObjectPtr&) final;
        void visitAronVariant(const aron::type::DictPtr& i) final;
        void visitAronVariant(const aron::type::PairPtr& i) final;
        void visitAronVariant(const aron::type::TuplePtr& i) final;
        void visitAronVariant(const aron::type::ListPtr& i) final;
        void visitAronVariant(const aron::type::NDArrayPtr& i) final;
        void visitAronVariant(const aron::type::MatrixPtr& i) final;
        void visitAronVariant(const aron::type::QuaternionPtr& i) final;
        void visitAronVariant(const aron::type::ImagePtr& i) final;
        void visitAronVariant(const aron::type::PointCloudPtr& i) final;
        void visitAronVariant(const aron::type::IntEnumPtr& i) final;
        void visitAronVariant(const aron::type::IntPtr& i) final;
        void visitAronVariant(const aron::type::LongPtr& i) final;
        void visitAronVariant(const aron::type::FloatPtr& i) final;
        void visitAronVariant(const aron::type::DoublePtr& i) final;
        void visitAronVariant(const aron::type::BoolPtr& i) final;
        void visitAronVariant(const aron::type::StringPtr& i) final;
        void visitUnknown(Input&) final;
    };
}


