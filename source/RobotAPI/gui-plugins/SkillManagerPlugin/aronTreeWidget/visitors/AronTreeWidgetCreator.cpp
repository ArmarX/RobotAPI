/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::SkillManagerMonitorWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2020
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>

// base class
#include "AronTreeWidgetCreator.h"

// data
#include "../AronTreeWidgetItem.h"
#include "../Data.h"

namespace armarx
{
    void
    AronTreeWidgetCreatorVisitor::createSimpleTreeViewWidget(Input& i, const std::string& defaul)
    {
        ARMARX_CHECK_NOT_NULL(i);

        auto key = i->getPath().getLastElement();
        createdQWidgetItem = new AronTreeWidgetItem();
        createdQWidgetItem->aronType = i;
        createdQWidgetItem->setText(0, QString::fromStdString(key));
        createdQWidgetItem->setText(1, QString::fromStdString(defaul));
        createdQWidgetItem->setText(2, QString::fromStdString(i->getShortName()));
        createdQWidgetItem->setText(3, QString::fromStdString(aron_tree_widget::constantes::ITEM_EMPTY_MESSAGE) /*QString::fromStdString(i->getDefaultFromString())*/);
        createdQWidgetItem->setFlags(createdQWidgetItem->flags() | Qt::ItemIsEditable);
    }

    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::ObjectPtr& i)
    {
        ARMARX_CHECK_NOT_NULL(i);

        auto key = i->getObjectName();
        if (i->getPath().hasElement())
        {
            key = i->getPath().getLastElement();
        }

        createdQWidgetItem = new AronTreeWidgetItem();
        createdQWidgetItem->setText(0, QString::fromStdString(key));

        for (const auto& [key, value] : i->getMemberTypes())
        {
            AronTreeWidgetCreatorVisitor v;
            aron::type::visit(v, value);

            if (v.createdQWidgetItem)
            {
                createdQWidgetItem->addChild(v.createdQWidgetItem);
            }
        }
    }

    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::DictPtr& i)
    { createSimpleTreeViewWidget(i, ""); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::PairPtr& i)
    { createSimpleTreeViewWidget(i, ""); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::TuplePtr& i)
    { createSimpleTreeViewWidget(i, ""); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::ListPtr& i)
    { createSimpleTreeViewWidget(i, ""); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::NDArrayPtr& i)
    { createSimpleTreeViewWidget(i, ""); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::MatrixPtr& i)
    { createSimpleTreeViewWidget(i, ""); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::QuaternionPtr& i)
    { createSimpleTreeViewWidget(i, ""); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::ImagePtr& i)
    { createSimpleTreeViewWidget(i, ""); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::PointCloudPtr& i)
    { createSimpleTreeViewWidget(i, ""); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::IntEnumPtr& i)
    { createSimpleTreeViewWidget(i, ""); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::IntPtr& i)
    { createSimpleTreeViewWidget(i, "0"); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::LongPtr& i)
    { createSimpleTreeViewWidget(i, "0"); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::FloatPtr& i)
    { createSimpleTreeViewWidget(i, "0.0"); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::DoublePtr& i)
    { createSimpleTreeViewWidget(i, "0.0"); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::BoolPtr& i)
    { createSimpleTreeViewWidget(i, "false"); }
    void
    AronTreeWidgetCreatorVisitor::visitAronVariant(const aron::type::StringPtr& i)
    { createSimpleTreeViewWidget(i, ""); }

    void AronTreeWidgetCreatorVisitor::visitUnknown(Input&)
    {
        ARMARX_WARNING_S << "Received an unknown type when trying to create a tree view widget for a skill argument type.";
    }
}

