/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::SkillManagerMonitorWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2020
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>

// base class
#include "AronTreeWidgetConverter.h"

// armarx
#include <ArmarXCore/core/logging/Logging.h>

// qt
#include <QTreeWidgetItem>

namespace armarx
{
    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::ObjectPtr& i)
    {
        auto createdAronDict = std::make_shared<aron::data::Dict>(i->getPath());
        createdAron = createdAronDict;
        QTreeWidgetItem* el = parentItem->child(index);

        unsigned int x = 0;
        for (const auto& [key, value] : i->getMemberTypes())
        {
            AronTreeWidgetConverterVisitor v(el, x++);
            aron::type::visit(v, value);

            createdAronDict->addElement(key, v.createdAron);
        }
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::DictPtr& i)
    {
        auto createdAronDict = std::make_shared<aron::data::Dict>(i->getPath());
        createdAron = createdAronDict;
        QTreeWidgetItem* el = parentItem->child(index);

        for (int x = 0; x < el->childCount(); ++x)
        {
            auto it = el->child(x);
            AronTreeWidgetConverterVisitor v(el, x);
            aron::type::visit(v, i->getAcceptedType());

            if (v.createdAron)
            {
                createdAronDict->addElement(it->text(0).toStdString(), v.createdAron);
            }
        }
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::PairPtr& i)
    {
        // TODO
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::TuplePtr& i)
    {
        // TODO
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::ListPtr& i)
    {
        // TODO
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::NDArrayPtr& i)
    {
        // TODO
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::MatrixPtr& i)
    {
        // TODO
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::QuaternionPtr& i)
    {
        // TODO
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::ImagePtr& i)
    {
        // TODO
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::PointCloudPtr& i)
    {
        // TODO
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::IntEnumPtr& i)
    {
        // TODO
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::IntPtr& i)
    {
        auto createdAronInt = std::make_shared<aron::data::Int>(i->getPath());
        createdAron = createdAronInt;
        QTreeWidgetItem* el = parentItem->child(index);

        std::string str = el->text(1).toStdString();
        if (str.empty())
        {
            createdAronInt->setValue(0);
            return;
        }

        int val = std::stoi(str);
        createdAronInt->setValue(val);
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::LongPtr& i)
    {
        auto createdAronLong = std::make_shared<aron::data::Long>(i->getPath());
        createdAron = createdAronLong;
        QTreeWidgetItem* el = parentItem->child(index);

        std::string str = el->text(1).toStdString();
        if (str.empty())
        {
            str = el->text(3).toStdString();
        }

        createdAronLong->fromString(str);
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::FloatPtr& i)
    {
        auto createdAronFloat = std::make_shared<aron::data::Float>(i->getPath());
        createdAron = createdAronFloat;
        QTreeWidgetItem* el = parentItem->child(index);

        std::string str = el->text(1).toStdString();
        if (str.empty())
        {
            str = el->text(3).toStdString();
        }

        createdAronFloat->fromString(str);
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::DoublePtr& i)
    {
        auto createdAronDouble = std::make_shared<aron::data::Double>(i->getPath());
        createdAron = createdAronDouble;
        QTreeWidgetItem* el = parentItem->child(index);

        std::string str = el->text(1).toStdString();
        if (str.empty())
        {
            str = el->text(3).toStdString();
        }

        createdAronDouble->fromString(str);
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::BoolPtr& i)
    {
        auto createdAronBool = std::make_shared<aron::data::Bool>(i->getPath());
        createdAron = createdAronBool;
        QTreeWidgetItem* el = parentItem->child(index);

        std::string str = el->text(1).toStdString();
        if (str.empty())
        {
            str = el->text(3).toStdString();
        }

        createdAronBool->fromString(str);
    }

    void
    AronTreeWidgetConverterVisitor::visitAronVariant(const aron::type::StringPtr& i)
    {
        auto createdAronString = std::make_shared<aron::data::String>(i->getPath());
        createdAron = createdAronString;
        QTreeWidgetItem* el = parentItem->child(index);

        std::string str = el->text(1).toStdString();
        createdAronString->fromString(str);
    }

    void AronTreeWidgetConverterVisitor::visitUnknown(Input&)
    {
        ARMARX_WARNING_S << "Received an unknown type when trying to convert a skill argument type to an aron data object.";
    }
}

