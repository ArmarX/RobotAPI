#include "AronTreeWidgetTextInputModalController.h"

namespace armarx
{
    AronTreeWidgetTextInputModalController::AronTreeWidgetTextInputModalController(const std::string& label, AronTreeWidgetItem* item, QTreeWidget* parent) :
        AronTreeWidgetModal(label, item, parent)
    {
        widget.setupUi(this);

        // set header
        widget.groupBoxInput->setTitle(QString::fromStdString(label));
        reset();

        // connect signals
        connect(widget.pushButtonReset, &QPushButton::clicked,
                  this, &AronTreeWidgetTextInputModalController::reset);
        connect(widget.pushButtonSubmit, &QPushButton::clicked,
                  this, &AronTreeWidgetTextInputModalController::submit);
    }

    void AronTreeWidgetTextInputModalController::submit()
    {
        item->setText(aron_tree_widget::constantes::TREE_WIDGET_ITEM_VALUE, widget.textEditInput->toPlainText());

        AronTreeWidgetModal::submit();
    }

    void AronTreeWidgetTextInputModalController::reset()
    {
        AronTreeWidgetModal::reset();

        // reset to initial value
        widget.textEditInput->setPlainText(init.text(aron_tree_widget::constantes::TREE_WIDGET_ITEM_VALUE));
    }
}
