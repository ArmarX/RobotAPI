#pragma once

#include "../AronTreeWidgetModal.h"

#include <RobotAPI/gui-plugins/SkillManagerPlugin/aronTreeWidget/modal/text/ui_AronTreeWidgetTextInputModal.h>

#include <QDialog>

namespace armarx
{
    class AronTreeWidgetTextInputModalController :
        public AronTreeWidgetModal
    {

    public:

        AronTreeWidgetTextInputModalController(const std::string& label, AronTreeWidgetItem* item, QTreeWidget* parent);

    private slots:

        void submit() final;
        void reset() final;

    private:
        Ui::AronTreeWidgetTextInputModalWidget widget;
    };
}
