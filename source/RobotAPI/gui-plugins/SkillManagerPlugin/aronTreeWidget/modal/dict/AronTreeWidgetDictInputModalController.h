#pragma once

#include <stack>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include "../AronTreeWidgetModal.h"

#include <RobotAPI/gui-plugins/SkillManagerPlugin/aronTreeWidget/modal/dict/ui_AronTreeWidgetDictInputModal.h>

#include <QDialog>

namespace armarx
{
    class AronTreeWidgetDictInputModalController :
        public AronTreeWidgetModal
    {
    public:

        AronTreeWidgetDictInputModalController(const std::string& label, AronTreeWidgetItem* item, QTreeWidget* parent);

    private slots:

        void submit() final;
        void reset() final;

        void addEmptyElement();

    private:
        std::vector<AronTreeWidgetItem*> addedItems;
        Ui::AronTreeWidgetDictInputModalWidget widget;
    };
}
