#pragma once

#include "../AronTreeWidgetModal.h"

#include <RobotAPI/gui-plugins/SkillManagerPlugin/aronTreeWidget/modal/float_double/ui_AronTreeWidgetFloatInputModal.h>

#include <QDialog>

namespace armarx
{
    class AronTreeWidgetFloatInputModalController :
        public AronTreeWidgetModal
    {

    public:

        AronTreeWidgetFloatInputModalController(const std::string& label, AronTreeWidgetItem* item, QTreeWidget* parent);

    private slots:

        void submit() final;
        void reset() final;

    private:
        Ui::AronTreeWidgetFloatInputModalWidget widget;
    };
}
