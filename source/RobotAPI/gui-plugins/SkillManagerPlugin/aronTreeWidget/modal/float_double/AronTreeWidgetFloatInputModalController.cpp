#include "AronTreeWidgetFloatInputModalController.h"

namespace armarx
{
    AronTreeWidgetFloatInputModalController::AronTreeWidgetFloatInputModalController(const std::string& label, AronTreeWidgetItem* item, QTreeWidget* parent) :
        AronTreeWidgetModal(label, item, parent)
    {
        widget.setupUi(this);

        // TODO
    }

    void AronTreeWidgetFloatInputModalController::submit()
    {
        item->setText(aron_tree_widget::constantes::TREE_WIDGET_ITEM_VALUE, widget.textEditInput->toPlainText());

        AronTreeWidgetModal::submit();
    }

    void AronTreeWidgetFloatInputModalController::reset()
    {
        AronTreeWidgetModal::reset();

        // reset to initial value
        widget.textEditInput->setPlainText(init.text(aron_tree_widget::constantes::TREE_WIDGET_ITEM_VALUE));
    }
}
