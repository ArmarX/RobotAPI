#include "AronTreeWidgetBoolInputModalController.h"

namespace armarx
{
    AronTreeWidgetBoolInputModalController::AronTreeWidgetBoolInputModalController(const std::string& label, AronTreeWidgetItem* item, QTreeWidget* parent) :
        AronTreeWidgetModal(label, item, parent)
    {
        widget.setupUi(this);

        // TODO
    }

    void AronTreeWidgetBoolInputModalController::submit()
    {
        item->setText(aron_tree_widget::constantes::TREE_WIDGET_ITEM_VALUE, widget.textEditInput->toPlainText());

        AronTreeWidgetModal::submit();
    }

    void AronTreeWidgetBoolInputModalController::reset()
    {
        AronTreeWidgetModal::reset();

        // reset to initial value
        widget.textEditInput->setPlainText(init.text(aron_tree_widget::constantes::TREE_WIDGET_ITEM_VALUE));
    }
}
