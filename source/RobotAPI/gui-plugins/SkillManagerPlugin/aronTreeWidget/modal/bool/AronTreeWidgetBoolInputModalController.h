#pragma once

#include "../AronTreeWidgetModal.h"

#include <RobotAPI/gui-plugins/SkillManagerPlugin/aronTreeWidget/modal/bool/ui_AronTreeWidgetBoolInputModal.h>

#include <QDialog>

namespace armarx
{
    class AronTreeWidgetBoolInputModalController :
        public AronTreeWidgetModal
    {

    public:

        AronTreeWidgetBoolInputModalController(const std::string& label, AronTreeWidgetItem* item, QTreeWidget* parent);

    private slots:

        void submit() final;
        void reset() final;

    private:
        Ui::AronTreeWidgetBoolInputModalWidget widget;
    };
}
