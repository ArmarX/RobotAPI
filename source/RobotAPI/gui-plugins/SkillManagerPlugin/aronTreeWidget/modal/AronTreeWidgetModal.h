#pragma once

#include <stack>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include "../AronTreeWidgetItem.h"
#include "../Data.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <QDialog>
#include <QTreeWidget>

namespace armarx
{
    class AronTreeWidgetModal  :
            public QDialog
    {
        Q_OBJECT

    public:
        AronTreeWidgetModal(const std::string& label, AronTreeWidgetItem* item, QTreeWidget* parent);

    protected slots:
        virtual void reset()
        {
            item->aronType = init.aronType;
            item->setText(aron_tree_widget::constantes::TREE_WIDGET_ITEM_NAME, init.text(aron_tree_widget::constantes::TREE_WIDGET_ITEM_NAME));
            item->setText(aron_tree_widget::constantes::TREE_WIDGET_ITEM_VALUE, init.text(aron_tree_widget::constantes::TREE_WIDGET_ITEM_VALUE));
            item->setText(aron_tree_widget::constantes::TREE_WIDGET_ITEM_TYPE, init.text(aron_tree_widget::constantes::TREE_WIDGET_ITEM_TYPE));
            for (int i = 0; i < item->childCount(); ++i)
            {
                item->removeChild(item->child(i));
            }
            for (int i = 0; i < init.childCount(); ++i)
            {
                item->addChild(init.child(i)->clone());
            }
        }
        virtual void submit()
        {
            accept();
        }

    protected:

        AronTreeWidgetItem init;
        AronTreeWidgetItem* item;

    private:
        std::string label;
        QTreeWidget* parent;
    };

    using AronTreeWidgetModalControllerPtr = std::shared_ptr<AronTreeWidgetModal>;
}
