#include "AronTreeWidgetIntInputModalController.h"

namespace armarx
{
    AronTreeWidgetIntInputModalController::AronTreeWidgetIntInputModalController(const std::string& label, AronTreeWidgetItem* item, QTreeWidget* parent) :
        AronTreeWidgetModal(label, item, parent)
    {
        widget.setupUi(this);

        // TODO
    }

    void AronTreeWidgetIntInputModalController::submit()
    {
        item->setText(aron_tree_widget::constantes::TREE_WIDGET_ITEM_VALUE, widget.textEditInput->toPlainText());

        AronTreeWidgetModal::submit();
    }

    void AronTreeWidgetIntInputModalController::reset()
    {
        AronTreeWidgetModal::reset();

        // reset to initial value
        widget.textEditInput->setPlainText(init.text(aron_tree_widget::constantes::TREE_WIDGET_ITEM_VALUE));
    }
}
