#pragma once

#include "../AronTreeWidgetModal.h"

#include <RobotAPI/gui-plugins/SkillManagerPlugin/aronTreeWidget/modal/int_long/ui_AronTreeWidgetIntInputModal.h>

#include <QDialog>

namespace armarx
{
    class AronTreeWidgetIntInputModalController :
        public AronTreeWidgetModal
    {

    public:

        AronTreeWidgetIntInputModalController(const std::string& label, AronTreeWidgetItem* item, QTreeWidget* parent);

    private slots:

        void submit() final;
        void reset() final;

    private:
        Ui::AronTreeWidgetIntInputModalWidget widget;
    };
}
