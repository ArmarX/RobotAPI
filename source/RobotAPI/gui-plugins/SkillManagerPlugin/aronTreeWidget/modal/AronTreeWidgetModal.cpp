#include "AronTreeWidgetModal.h"

namespace armarx
{
    AronTreeWidgetModal::AronTreeWidgetModal(const std::string& label, AronTreeWidgetItem* item, QTreeWidget* parent) :
        QDialog(parent),
        item(item),
        label(label),
        parent(parent)
    {
        init.aronType = item->aronType;
        init.setText(aron_tree_widget::constantes::TREE_WIDGET_ITEM_NAME, item->text(aron_tree_widget::constantes::TREE_WIDGET_ITEM_NAME));
        init.setText(aron_tree_widget::constantes::TREE_WIDGET_ITEM_VALUE, item->text(aron_tree_widget::constantes::TREE_WIDGET_ITEM_VALUE));
        init.setText(aron_tree_widget::constantes::TREE_WIDGET_ITEM_TYPE, item->text(aron_tree_widget::constantes::TREE_WIDGET_ITEM_TYPE));
        for (int i = 0; i < item->childCount(); ++i)
        {
            init.addChild(item->child(i)->clone());
        }
    }
}
