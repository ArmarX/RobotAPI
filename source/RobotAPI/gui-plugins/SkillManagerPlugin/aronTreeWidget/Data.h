#pragma once

#include <string>

namespace armarx::aron_tree_widget::constantes
{
    const int TREE_WIDGET_ITEM_NAME = 0;
    const int TREE_WIDGET_ITEM_VALUE = 1;
    const int TREE_WIDGET_ITEM_TYPE = 2;

    const std::string ITEM_EMPTY_MESSAGE = "(double click to edit)";
    const std::string NEW_ITEM_DEFAULT_MESSAGE = "(Please set via main GUI (not in modal))";
}
