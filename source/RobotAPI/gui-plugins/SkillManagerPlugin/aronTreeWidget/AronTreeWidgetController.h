#pragma once

#include <stack>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include "Data.h"

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <QTreeWidget>
#include "visitors/AronTreeWidgetCreator.h"
#include "AronTreeWidgetItem.h"

namespace armarx
{
    class AronTreeWidgetController
    {

    public:
        AronTreeWidgetController(QTreeWidget* tree, QTreeWidgetItem* parent, const aron::type::ObjectPtr& type, const aron::data::DictPtr& data = nullptr);

        aron::data::DictPtr convertToAron() const;
        void setFromAron(const aron::data::DictPtr&);

    private:
        QTreeWidgetItem* parent;
        QTreeWidget* tree;

        aron::type::ObjectPtr type;
    };

    using AronTreeWidgetControllerPtr = std::shared_ptr<AronTreeWidgetController>;
}
