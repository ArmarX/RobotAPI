/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::SkillManagerMonitorWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2020
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>

#include "SkillManagerMonitorWidgetController.h"

#include "aronTreeWidget/visitors/AronTreeWidgetCreator.h"
#include "aronTreeWidget/visitors/AronTreeWidgetConverter.h"
#include "aronTreeWidget/visitors/AronTreeWidgetModalCreator.h"

#include <RobotAPI/libraries/skills/provider/Skill.h>

// modals
#include "aronTreeWidget/modal/text/AronTreeWidgetTextInputModalController.h"

// debug
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

#include <QDoubleSpinBox>
#include <QClipboard>

#include "aronTreeWidget/Data.h"

//config
namespace armarx
{
    QPointer<QDialog> SkillManagerMonitorWidgetController::getConfigDialog(QWidget* parent)
    {
        if (!dialog)
        {
            dialog = new SimpleConfigDialog(parent);
            dialog->addProxyFinder<skills::manager::dti::SkillManagerInterfacePrx>("SkillManager", "", "Skill*");
        }
        return qobject_cast<SimpleConfigDialog*>(dialog);
    }
    void SkillManagerMonitorWidgetController::configured()
    {
        observerName = dialog->getProxyName("SkillManager");
    }
    void SkillManagerMonitorWidgetController::loadSettings(QSettings* settings)
    {
        observerName = settings->value("SkillManager", "SkillManager").toString().toStdString();
    }
    void SkillManagerMonitorWidgetController::saveSettings(QSettings* settings)
    {
        settings->setValue("SkillManager", QString::fromStdString(observerName));
    }
}

// Others
namespace armarx
{
    SkillManagerMonitorWidgetController::SkillManagerMonitorWidgetController()
    {
        widget.setupUi(getWidget());

        widget.doubleSpinBoxUpdateFreq->setValue(5.0);
        widget.doubleSpinBoxUpdateFreq->setMinimum(0);
        widget.doubleSpinBoxUpdateFreq->setMaximum(100);
        widget.doubleSpinBoxUpdateFreq->setSingleStep(0.5);
        widget.doubleSpinBoxUpdateFreq->setSuffix(" Hz");

        refreshSkillsResultTimer = new QTimer(this);
        refreshSkillsResultTimer->setInterval(1000 / 5);  // Keep this stable.
        refreshSkillsResultTimer->start();

        connect(widget.doubleSpinBoxUpdateFreq, &QDoubleSpinBox::editingFinished,
                this, &SkillManagerMonitorWidgetController::updateTimerFrequency);
        connect(refreshSkillsResultTimer, &QTimer::timeout,
                this, &SkillManagerMonitorWidgetController::refreshSkills);

        connect(widget.pushButtonCopy, &QPushButton::clicked,
                this, &SkillManagerMonitorWidgetController::copyCurrentConfig);
        connect(widget.pushButtonPaste, &QPushButton::clicked,
                this, &SkillManagerMonitorWidgetController::pasteCurrentConfig);

        connect(widget.pushButtonExecuteSkill, &QPushButton::clicked,
                this, &SkillManagerMonitorWidgetController::executeSkill);
        connect(widget.pushButtonStopSkill, &QPushButton::clicked,
                this, &SkillManagerMonitorWidgetController::stopSkill);

        connect(widget.treeWidgetSkills, &QTreeWidget::currentItemChanged,
                this, &SkillManagerMonitorWidgetController::skillSelectionChanged);

        connect(widget.treeWidgetSkillDetails, &QTreeWidget::itemDoubleClicked,
                this, &SkillManagerMonitorWidgetController::onTreeWidgetItemDoubleClicked);
    }

    SkillManagerMonitorWidgetController::~SkillManagerMonitorWidgetController()
    {

    }

    void SkillManagerMonitorWidgetController::onInitComponent()
    {
        usingProxy(observerName);
    }


    void SkillManagerMonitorWidgetController::onConnectComponent()
    {
        getProxy(manager, observerName);
        widget.groupBoxSkills->setTitle(QString::fromStdString(observerName));
        widget.treeWidgetSkillDetails->setEditTriggers(QAbstractItemView::EditTrigger::NoEditTriggers);
        widget.treeWidgetSkillDetails->setColumnHidden(3, true);

        connected = true;
    }

    void SkillManagerMonitorWidgetController::onDisconnectComponent()
    {
        connected = false;

        // reset all
        skills.clear();
        widget.treeWidgetSkills->clear();
        widget.treeWidgetSkillDetails->clear();
        skillsArgumentsTreeWidgetItem = nullptr;
        selectedSkill.providerName = "";
        selectedSkill.skillName = "";
    }

    void SkillManagerMonitorWidgetController::updateTimerFrequency()
    {
        int f = static_cast<int>(std::round(1000 / widget.doubleSpinBoxUpdateFreq->value()));
        refreshSkillsResultTimer->setInterval(f);
    }

    void SkillManagerMonitorWidgetController::refreshSkills()
    {
        static std::map<skills::provider::dto::Execution::Status, std::string> ExecutionStatus2String = {
            {skills::provider::dto::Execution::Status::Aborted, "Aborted"},
            {skills::provider::dto::Execution::Status::Failed, "Failed"},
            {skills::provider::dto::Execution::Status::Idle, "Not yet started"},
            {skills::provider::dto::Execution::Status::Running, "Running"},
            {skills::provider::dto::Execution::Status::Scheduled, "Scheduled"},
            {skills::provider::dto::Execution::Status::Succeeded, "Succeeded"}
        };

        if (!connected)
            return;

        /* CHECK OWN SKILLS LIST */
        // remove non-existing ones
        auto managerSkills = manager->getSkillDescriptions();
        std::vector<std::string> removedProviders;
        for (auto it = skills.begin(); it != skills.end();)
        {
            // TODO: iterate over skills, not just over providers!
            std::string providerName = it->first;
            if (managerSkills.find(providerName) == managerSkills.end())
            {
                removedProviders.push_back(providerName);
                it = skills.erase(it);
            }
            else
            {
                it++;
            }
        }

        // add new ones
        std::vector<std::string> newProviders;
        for (const auto& [providerName, providerSkills] : managerSkills)
        {
            if (skills.find(providerName) == skills.end())
            {
                skills.insert(std::make_pair(providerName, providerSkills));
                newProviders.push_back(providerName);
            }
        }

        /* CHECK TREE VIEW */
        // remove providers from tree
        int i = 0;
        while (i < widget.treeWidgetSkills->topLevelItemCount())
        {
            QTreeWidgetItem* item = widget.treeWidgetSkills->topLevelItem(i);
            if (std::find(removedProviders.begin(), removedProviders.end(), item->text(0).toStdString()) != removedProviders.end())
            {
                delete widget.treeWidgetSkills->takeTopLevelItem(i);
            }
            else
            {
                ++i;
            }
        }

        // add new providers
        for (const auto& [providerName, providerSkills] : skills)
        {
            if (auto it = std::find(newProviders.begin(), newProviders.end(), providerName); it != newProviders.end())
            {
                auto item = new QTreeWidgetItem(widget.treeWidgetSkills);
                item->setText(0, QString::fromStdString(providerName));
                for (const auto& [name, sk] : providerSkills)
                {
                    auto itsk = new QTreeWidgetItem(item);
                    item->addChild(itsk);
                    itsk->setText(0, QString::fromStdString(name));
                }
            }
        }

        // update status and active skills window
        std::map<skills::SkillID, std::string> activeSkillsAndPrefixes;
        auto managerStatuses = manager->getSkillExecutionStatuses();
        for (int i = 0;  i < widget.treeWidgetSkills->topLevelItemCount(); ++i)
        {
            try
            {
                QTreeWidgetItem* item = widget.treeWidgetSkills->topLevelItem(i);
                auto providerName = item->text(0).toStdString();

                auto allStatusesForProvider = managerStatuses.at(providerName);

                for (int j = 0; j < item->childCount(); ++j)
                {
                    QTreeWidgetItem* skillItem = item->child(j);
                    skills::SkillID currentSkillId(providerName, skillItem->text(0).toStdString());

                    auto statusForSkill = allStatusesForProvider.at(currentSkillId.skillName);
                    skillItem->setText(2, QString::fromStdString(ExecutionStatus2String.at(statusForSkill.header.status)));

                    if (not statusForSkill.header.executorName.empty()) // it means that the skill was called by someone
                    {
                        activeSkillsAndPrefixes.insert({currentSkillId, statusForSkill.header.executorName});
                    }
                }
            }
            catch (const std::exception& e)
            {
                // Perhaps the skill provider died after the check at the beginning of this method
                continue;
            }
        }

        // finally update the view of active skills
        widget.listWidgetActiveSkills->clear();
        for (const auto& [id, prefix] : activeSkillsAndPrefixes)
        {
            auto prefixedStr = id.toString(prefix);
            bool longest = true;
            for (const auto& [id2, prefix2] : activeSkillsAndPrefixes) // check if there is a deeper skill currently executing
            {
                auto prefixedStr2 = id.toString(prefix2);
                if (prefixedStr == prefixedStr2)
                {
                    continue;
                }

                if (simox::alg::starts_with(prefixedStr2, prefixedStr))
                {
                    longest = false;
                    break;
                }
            }

            if (longest)
            {
                widget.listWidgetActiveSkills->addItem(QString::fromStdString(id.toString() + ": " + id.toString(prefix)));
            }
        }
    }

    void SkillManagerMonitorWidgetController::executeSkill()
    {
        if (selectedSkill.providerName.empty() or selectedSkill.skillName.empty())
        {
            return;
        }

        const auto& skillDescriptions = skills.at(selectedSkill.providerName);
        if (!skillDescriptions.count(selectedSkill.skillName))
        {
            return;
        }

        auto data = getConfigAsAron();

        char hostname[HOST_NAME_MAX];

        gethostname(hostname, HOST_NAME_MAX);

        skills::manager::dto::SkillExecutionRequest exInfo;
        exInfo.executorName = "Skills.Manager GUI (hostname: " + std::string(hostname) + ")";
        exInfo.skillId = {selectedSkill.providerName, selectedSkill.skillName};
        exInfo.params = aron::data::Dict::ToAronDictDTO(data);

        ARMARX_IMPORTANT << "Executing skill from GUI: " << selectedSkill.providerName << "/" << selectedSkill.skillName << ". The data was: " << data;
        // Note that we execute the skill in a seperate thread so that the GUI thread does not freeze.
        manager->begin_executeSkill(exInfo);
    }

    void SkillManagerMonitorWidgetController::stopSkill()
    {
        if (selectedSkill.providerName.empty() or selectedSkill.skillName.empty())
        {
            return;
        }

        const auto& skillDescriptions = skills.at(selectedSkill.providerName);
        if (!skillDescriptions.count(selectedSkill.skillName))
        {
            return;
        }

        ARMARX_INFO << "Stopping skill from GUI: " << selectedSkill.providerName << "/" << selectedSkill.skillName;
        manager->abortSkill(selectedSkill.providerName, selectedSkill.skillName);
    }

    void SkillManagerMonitorWidgetController::skillSelectionChanged(QTreeWidgetItem* current, QTreeWidgetItem*)
    {
        widget.groupBoxSkillDetails->setEnabled(false);

        if (!current)
        {
            // gui has died?
            return;
        }

        if (!current->parent())
        {
            // no parent available. Should not happen
            return;
        }

        SelectedSkill newSelectedSkill;

        // setup selected skill
        newSelectedSkill.providerName = current->parent()->text(0).toStdString();
        newSelectedSkill.skillName = current->text(0).toStdString();

        // setup groupBox
        widget.groupBoxSkillDetails->setTitle(QString::fromStdString(newSelectedSkill.providerName + "/" + newSelectedSkill.skillName));
        widget.groupBoxSkillDetails->setEnabled(true);

        if (newSelectedSkill.providerName == selectedSkill.providerName and newSelectedSkill.skillName == selectedSkill.skillName)
        {
            return;
        }

        selectedSkill = newSelectedSkill;

        // setup table view
        widget.treeWidgetSkillDetails->clear();
        aronTreeWidgetController = nullptr;
        skillsArgumentsTreeWidgetItem = nullptr;

        auto skillDesc = skills.at(selectedSkill.providerName).at(selectedSkill.skillName);

        {
            auto it = new QTreeWidgetItem(widget.treeWidgetSkillDetails,
                                          {QString::fromStdString("Name"), QString::fromStdString(skillDesc.skillName)});
            widget.treeWidgetSkillDetails->addTopLevelItem(it);
        }

        {
            auto it = new QTreeWidgetItem(widget.treeWidgetSkillDetails,
                                          {QString::fromStdString("Robot"), QString::fromStdString(simox::alg::join(skillDesc.robots, ", "))});
            widget.treeWidgetSkillDetails->addTopLevelItem(it);
        }

        {
            auto it = new QTreeWidgetItem(widget.treeWidgetSkillDetails,
                                          {QString::fromStdString("Description"), QString::fromStdString(skillDesc.description)});
            widget.treeWidgetSkillDetails->addTopLevelItem(it);
        }

        {
            auto it = new QTreeWidgetItem(widget.treeWidgetSkillDetails,
                                          {QString::fromStdString("Timeout"), QString::fromStdString(std::to_string(skillDesc.timeoutMs)) + " ms"});
            widget.treeWidgetSkillDetails->addTopLevelItem(it);
        }

        skillsArgumentsTreeWidgetItem = new QTreeWidgetItem(widget.treeWidgetSkillDetails, {QString::fromStdString("Arguments")});
        auto aron_args = aron::type::Object::FromAronObjectDTO(skillDesc.acceptedType);
        auto default_args = aron::data::Dict::FromAronDictDTO(skillDesc.defaultParams);

        aronTreeWidgetController = std::make_shared<AronTreeWidgetController>(widget.treeWidgetSkillDetails, skillsArgumentsTreeWidgetItem, aron_args, default_args);
    }

    aron::data::DictPtr SkillManagerMonitorWidgetController::getConfigAsAron() const
    {
        // create argument aron (if there is an accepted type set)
        if (aronTreeWidgetController)
        {
            return aronTreeWidgetController->convertToAron();
        }
        return nullptr;
    }

    void SkillManagerMonitorWidgetController::copyCurrentConfig()
    {
        auto data = getConfigAsAron();
        if (!data)
        {
            return;
        }

        auto json = aron::converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(data);
        QClipboard* clipboard = QApplication::clipboard();
        clipboard->setText(QString::fromStdString(json.dump(2)));
    }

    void SkillManagerMonitorWidgetController::pasteCurrentConfig()
    {
        QClipboard* clipboard = QApplication::clipboard();
        std::string s = clipboard->text().toStdString();
        nlohmann::json json = nlohmann::json::parse(s);
        auto data = aron::converter::AronNlohmannJSONConverter::ConvertFromNlohmannJSONObject(json);

        if (!aronTreeWidgetController)
        {
            return;
        }

        aronTreeWidgetController->setFromAron(data);
    }

    void SkillManagerMonitorWidgetController::resetCurrentConfig()
    {
        // TODO
    }

    void SkillManagerMonitorWidgetController::onTreeWidgetItemDoubleClicked(QTreeWidgetItem* item, int column)
    {
        if (!item)
        {
            return;
        }

        if (column == 1)
        {
            if (item->flags() & Qt::ItemIsEditable) // we use the flag to indicate whether the item is editable or not
            {
                // we assume its aron item
                AronTreeWidgetItem* aItem = AronTreeWidgetItem::DynamicCastAndCheck(item);
                std::string name = aItem->text(aron_tree_widget::constantes::TREE_WIDGET_ITEM_NAME).toStdString();
                std::string type = aItem->text(aron_tree_widget::constantes::TREE_WIDGET_ITEM_TYPE).toStdString();

                // why visitor?!?!?
                AronTreeWidgetModalCreatorVisitor v(name, aItem, widget.treeWidgetSkillDetails);
                aron::type::visit(v, aItem->aronType);
                auto modal = v.createdModal;
                modal->exec();
            }
        }
    }
}

