/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "KinematicUnitConfigDialog.h"
#include <RobotAPI/gui-plugins/KinematicUnitPlugin/ui_KinematicUnitConfigDialog.h>

#include <QTimer>
#include <QPushButton>
#include <QMessageBox>


// ArmarX
#include <RobotAPI/interface/units/KinematicUnitInterface.h>

#include <IceUtil/UUID.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

#include <RobotAPI/interface/core/RobotState.h>

using namespace armarx;

KinematicUnitConfigDialog::KinematicUnitConfigDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::KinematicUnitConfigDialog),
    uuid(IceUtil::generateUUID())
{
    ui->setupUi(this);

    connect(this->ui->buttonBox, SIGNAL(accepted()), this, SLOT(verifyConfig()));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setDefault(false);
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setAutoDefault(false);
    proxyFinder = new IceProxyFinder<KinematicUnitInterfacePrx>(this);
    proxyFinder->setSearchMask("*KinematicUnit|KinematicUnit*");
    ui->gridLayout->addWidget(proxyFinder, 0, 1, 1, 2);

    connect(proxyFinder->getProxyNameComboBox(), SIGNAL(currentIndexChanged(int)), this, SLOT(selectionChanged(int)));
    connect(proxyFinder->getProxyNameComboBox(), SIGNAL(editTextChanged(QString)), this, SLOT(proxyNameChanged(QString)));
}

KinematicUnitConfigDialog::~KinematicUnitConfigDialog()
{
    delete ui;
}

void KinematicUnitConfigDialog::onInitComponent()
{
    proxyFinder->setIceManager(getIceManager());
}

void KinematicUnitConfigDialog::onConnectComponent()
{

}

void KinematicUnitConfigDialog::onExitComponent()
{
    QObject::disconnect();

}

void KinematicUnitConfigDialog::verifyConfig()
{
    if (proxyFinder->getSelectedProxyName().trimmed().length() == 0)
    {
        QMessageBox::critical(this, "Invalid Configuration", "The proxy name must not be empty");
    }
    else
    {
        this->accept();
    }
}

void KinematicUnitConfigDialog::updateSubconfig(std::string kinematicUnitName)
{

    try
    {
        ARMARX_INFO << "Connecting to KinematicUnitProxy " << kinematicUnitName;

        KinematicUnitInterfacePrx kinematicUnitInterfacePrx = getProxy<KinematicUnitInterfacePrx>(kinematicUnitName);
        std::string topicName = kinematicUnitInterfacePrx->getReportTopicName();
        std::string robotNodeSetName = kinematicUnitInterfacePrx->getRobotNodeSetName();
        std::string rfile = kinematicUnitInterfacePrx->getRobotFilename();
        ui->labelTopic->setText(QString(topicName.c_str()));
        ui->labelRobotModel->setText(QString(rfile.c_str()));
        ui->labelRNS->setText(QString(robotNodeSetName.c_str()));
    }
    catch (...)
    {
        ARMARX_INFO << "Could not connect to KinematicUnitProxy " << kinematicUnitName;
    }
}

void KinematicUnitConfigDialog::selectionChanged(int nr)
{
    ARMARX_LOG << "Selected entry:" << nr;
    ui->labelTopic->setText("");
    ui->labelRobotModel->setText("");
    ui->labelRNS->setText("");
    if (nr < 0)
    {
        return;
    }
    std::string kinematicUnitName = proxyFinder->getSelectedProxyName().toStdString();

    updateSubconfig(kinematicUnitName);

}

void KinematicUnitConfigDialog::proxyNameChanged(QString kinematicUnitName)
{
    ui->labelTopic->setText("");
    ui->labelRobotModel->setText("");
    ui->labelRNS->setText("");
    updateSubconfig(kinematicUnitName.toStdString());
}



