/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QDialog>
#include <QFileDialog>

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/IceProxyFinder.h>

namespace Ui
{
    class KinematicUnitConfigDialog;
}

namespace armarx
{
    class KinematicUnitConfigDialog :
        public QDialog,
        virtual public ManagedIceObject
    {
        Q_OBJECT

    public:
        explicit KinematicUnitConfigDialog(QWidget* parent = 0);
        ~KinematicUnitConfigDialog() override;

        // inherited from ManagedIceObject
        std::string getDefaultName() const override
        {
            return "KinematicUnitConfigDialog" + uuid;
        }
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        void updateKinematicUnitList();
    signals:

    public slots:
        //void modelFileSelected();
        void verifyConfig();

        void selectionChanged(int nr);
        void proxyNameChanged(QString);
    protected slots:
        void updateSubconfig(std::string kinematicUnitName);

    private:

        IceProxyFinderBase* proxyFinder;
        Ui::KinematicUnitConfigDialog* ui;
        std::string uuid;
        friend class KinematicUnitWidgetController;
    };
}

