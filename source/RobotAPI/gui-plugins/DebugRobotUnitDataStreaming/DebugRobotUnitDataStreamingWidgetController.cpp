/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::DebugRobotUnitDataStreamingWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2021
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DebugRobotUnitDataStreamingWidgetController.h"

#include <string>

namespace armarx
{

    void DebugRobotUnitDataStreamingWidgetController::loadSettings(QSettings* settings)
    {
        getRobotUnitComponentPlugin().setRobotUnitName(
            settings->value("ru", "Armar6Unit").toString().toStdString());
    }

    void DebugRobotUnitDataStreamingWidgetController::saveSettings(QSettings* settings)
    {
        settings->setValue("ru", QString::fromStdString(getRobotUnitComponentPlugin().getRobotUnitName()));
    }

    QPointer<QDialog> DebugRobotUnitDataStreamingWidgetController::getConfigDialog(QWidget* parent)
    {
        if (!dialog)
        {
            dialog = new SimpleConfigDialog(parent);
            dialog->addProxyFinder<RobotUnitInterfacePrx>("ru", "Robot Unit", "*Unit");
        }
        return qobject_cast<SimpleConfigDialog*>(dialog);
    }

    void DebugRobotUnitDataStreamingWidgetController::configured()
    {
        getRobotUnitComponentPlugin().setRobotUnitName(dialog->get("ru"));
    }

    DebugRobotUnitDataStreamingWidgetController::DebugRobotUnitDataStreamingWidgetController()
    {
        widget.setupUi(getWidget());
        startTimer(10);
    }

    void DebugRobotUnitDataStreamingWidgetController::onDisconnectComponent()
    {
        std::lock_guard f{mutex};
        rec.clear();
    }

    void DebugRobotUnitDataStreamingWidgetController::timerEvent(QTimerEvent* event)
    {
        const std::size_t n = widget.spinBoxNumberOfStreams->value();
        if (!getRobotUnit())
        {
            return;
        }
        if (rec.size() > n)
        {
            rec.resize(n);
        }
        std::lock_guard f{mutex};
        RobotUnitDataStreaming::Config cfg;
        if (widget.checkBoxStreamSens->isChecked())
        {
            cfg.loggingNames.emplace_back("sens");
        }
        if (widget.checkBoxStreamCtrl->isChecked())
        {
            cfg.loggingNames.emplace_back("ctrl");
        }
        while (rec.size() < n)
        {
            rec.emplace_back(getRobotUnitComponentPlugin()
                             .startDataSatreming(cfg));
            ARMARX_INFO << rec.back()->getDataDescriptionString();
        }
        for (auto& r : rec)
        {
            r->getDataBuffer().clear();
        }
    }
}
