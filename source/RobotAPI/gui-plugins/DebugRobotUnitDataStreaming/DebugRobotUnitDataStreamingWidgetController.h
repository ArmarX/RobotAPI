/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::DebugRobotUnitDataStreamingWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <RobotAPI/gui-plugins/DebugRobotUnitDataStreaming/ui_DebugRobotUnitDataStreamingWidget.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>

namespace armarx
{
    /**
    \page RobotAPI-GuiPlugins-DebugRobotUnitDataStreaming DebugRobotUnitDataStreaming
    \brief The DebugRobotUnitDataStreaming allows visualizing ...

    \image html DebugRobotUnitDataStreaming.png
    The user can

    API Documentation \ref DebugRobotUnitDataStreamingWidgetController

    \see DebugRobotUnitDataStreamingGuiPlugin
    */


    /**
     * \class DebugRobotUnitDataStreamingGuiPlugin
     * \ingroup ArmarXGuiPlugins
     * \brief DebugRobotUnitDataStreamingGuiPlugin brief description
     *
     * Detailed description
     */

    /**
     * \class DebugRobotUnitDataStreamingWidgetController
     * \brief DebugRobotUnitDataStreamingWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        DebugRobotUnitDataStreamingWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < DebugRobotUnitDataStreamingWidgetController >,
        public virtual RobotUnitComponentPluginUser
    {
        Q_OBJECT
    public:
        explicit DebugRobotUnitDataStreamingWidgetController();

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Debugging.DebugRobotUnitDataStreaming";
        }

        void onInitComponent()       override {}
        void onConnectComponent()    override {}
        void onDisconnectComponent() override;
        void onExitComponent()       override {}

    protected:
        void timerEvent(QTimerEvent* event) override;

    private:
        std::mutex                                     mutex;
        Ui::DebugRobotUnitDataStreamingWidget          widget;
        std::vector<RobotUnitDataStreamingReceiverPtr> rec;
        QPointer<SimpleConfigDialog>                   dialog;
    };
}


