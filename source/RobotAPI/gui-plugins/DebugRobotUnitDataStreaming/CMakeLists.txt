set(LIB_NAME "DebugRobotUnitDataStreamingGuiPlugin")
armarx_set_target("${LIB_NAME}")

armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")

# do not rename this variable, it is used in armarx_gui_library()...
set(SOURCES DebugRobotUnitDataStreamingWidgetController.cpp)
set(HEADERS DebugRobotUnitDataStreamingWidgetController.h)
set(GUI_UIS DebugRobotUnitDataStreamingWidget.ui)

set(COMPONENT_LIBS
    RobotAPIComponentPlugins
    SimpleConfigDialog)

if(ArmarXGui_FOUND)
    armarx_gui_plugin("${LIB_NAME}" "${SOURCES}" "" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
