/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* \package    ArmarX::Component::ObjectExaminerGuiPlugin
* \author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* \copyright  2012
* \license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#pragma once

/* ArmarX headers */
#include <RobotAPI/gui-plugins/PlatformUnitPlugin/ui_PlatformUnitGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/widgets/JoystickControlWidget.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <RobotAPI/interface/units/PlatformUnitInterface.h>

/* Qt headers */
#include <QMainWindow>
#include <QTimer>

#include <string>


namespace armarx
{

    class KeyboardPlatformHookWidget : public QWidget
    {
        Q_OBJECT
    public:
        KeyboardPlatformHookWidget(QWidget* parent = NULL):  QWidget(parent)
        {
            setFocusPolicy(Qt::ClickFocus);
        }

    signals:
        void commandKeyPressed(int key);
        void commandKeyReleased(int key);
        // QWidget interface
    protected:
        void keyPressEvent(QKeyEvent* event) override;

        void keyReleaseEvent(QKeyEvent* event) override;
    };

    class PlatformUnitConfigDialog;

    /**
      \class PlatformUnitGuiPlugin
      \brief This plugin provides a widget with which the PlatformUnit can be controlled.
      \see PlatformUnitWidget
      */
    class PlatformUnitGuiPlugin :
        public ArmarXGuiPlugin
    {
        Q_OBJECT
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
    public:
        PlatformUnitGuiPlugin();
        QString getPluginName() override
        {
            return "PlatformUnitGuiPlugin";
        }
    };

    /*!
      \page RobotAPI-GuiPlugins-PlatformUnitPlugin PlatformUnitPlugin
      \brief With this widget the PlatformUnit can be controlled.

      \image html PlatformUnitGUI.png "The plugin's ui." width=300px
            -# The current position and rotation, fields to enter a new target and a button to set the platform in motion.
            -# A joystick like control widget to move the platform. The platform does not rotate to move in a direction. Up moves the platform forward.
            -# A joystick like control widget to rotate the platform.
            -# Be careful to set a maximum velocity before using the joysticks.

       When you add the widget to the Gui, you need to specify the following parameters:

       Parameter Name   | Example Value     | Required?     | Description
       :----------------  | :-------------:   | :-------------- |:--------------------
       PlatformUnit - Proxy     | PlatformUnit   | Yes | The name of the platform unit.
       Platform | Platform | Yes | The name of the platform.
      */
    class PlatformUnitWidget :
        public ArmarXComponentWidgetControllerTemplate<PlatformUnitWidget>,
        public PlatformUnitListener
    {
        Q_OBJECT
    public:
        PlatformUnitWidget();
        ~PlatformUnitWidget() override
        {}

        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        // slice interface implementation
        void reportPlatformPose(PlatformPose const& currentPose, const Ice::Current& c = Ice::emptyCurrent) override;
        void reportNewTargetPose(::Ice::Float newPlatformPositionX, ::Ice::Float newPlatformPositionY, ::Ice::Float newPlatformRotation, const Ice::Current& c = Ice::emptyCurrent) override;
        void reportPlatformVelocity(::Ice::Float currentPlatformVelocityX, ::Ice::Float currentPlatformVelocityY, ::Ice::Float currentPlatformVelocityRotation, const Ice::Current& c = Ice::emptyCurrent) override;
        void reportPlatformOdometryPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current&) override;

        // inherited of ArmarXWidget
        static QString GetWidgetName()
        {
            return "RobotControl.PlatformUnitGUI";
        }
        static QIcon GetWidgetIcon()
        {
            return QIcon("://icons/retro_joystick2.svg");
        }

        QPointer<QDialog> getConfigDialog(QWidget* parent = 0) override;
        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        void configured() override;

    public slots:

        void moveTo();

        void setNewPlatformPoseLabels(float x, float y, float alpha);
        void setNewTargetPoseLabels(float x, float y, float alpha);

        void startControlTimer();
        void stopControlTimer();

    protected:
        void connectSlots();

        Ui::PlatformUnitGuiPlugin ui;

    private slots:
        /**
          \brief Checks the joystick contol widgets speedCtrl and rotaCtrl and performs a move if necessary.
            Activated when ctrlEvaluationTimer times out.
         */
        void controlTimerTick();

        /**
              \brief Stops the platform
              */
        void stopPlatform();
        void controlPlatformWithKeyboard(int key);
        void stopPlatformWithKeyboard(int key);
        void keyboardVelocityControl();

    private:
        std::string platformUnitProxyName;
        std::string platformName;

        PlatformUnitInterfacePrx platformUnitProxy;

        QPointer<QWidget> __widget;
        QPointer<PlatformUnitConfigDialog> dialog;

        /**
         * \brief A Joystick control for the platform speed.
         * (currently the velocity control is nyi is the simulator so this is emulated by
         * adding a delta to the target positions)
         */
        QPointer<JoystickControlWidget> speedCtrl;

        /**
         * \brief A Joystick control for the platform rotation.
         */
        QPointer<JoystickControlWidget> rotaCtrl;

        /**
         * \brief A timer to evaluate the speedCtrl and rotaCtrl
         */
        QTimer ctrlEvaluationTimer;
        QTimer stopPlatformTimer;
        QTimer keyboardVelocityTimer;
        /**
         * \brief Holds the last reported platform rotation. (required to emulate the speed control)
         */
        ::Ice::Float platformRotation;

        /**
         * \brief Whether the platform is moved with speedCtrl.
         */
        bool platformMoves;

        /**
         * \brief Whether the platform is moved with speedCtrl
         */
        ::Ice::Float platformRotationAtMoveStart;

        /**
         * \brief The tick rate (in ms) for the ctrlEvaluationTimer.
         */
        static const int CONTROL_TICK_RATE = 50;

        QSet<int> pressedKeys;

        float currentKeyboardVelocityX = 0;
        float currentKeyboardVelocityY = 0;
        float currentKeyboardVelocityAlpha = 0;
        float acceleration = 0.2;
        float deceleration = 0.8;

        // ArmarXWidgetController interface
    public:
        QPointer<QWidget> getWidget() override;

    };
    using PlatformUnitGuiPluginPtr = std::shared_ptr<PlatformUnitWidget>;
}

