/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "PlatformUnitConfigDialog.h"
#include <RobotAPI/gui-plugins/PlatformUnitPlugin/ui_PlatformUnitConfigDialog.h>

#include <IceUtil/UUID.h>


armarx::PlatformUnitConfigDialog::PlatformUnitConfigDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::PlatformUnitConfigDialog),
    uuid(IceUtil::generateUUID())
{
    ui->setupUi(this);
    finder = new IceProxyFinder<PlatformUnitInterfacePrx>(this);
    finder->setSearchMask("*Unit");
    ui->gridLayout->addWidget(finder, 0, 1);
}

armarx::PlatformUnitConfigDialog::~PlatformUnitConfigDialog()
{
    delete ui;
}



void armarx::PlatformUnitConfigDialog::onInitComponent()
{
    finder->setIceManager(getIceManager());
}

void armarx::PlatformUnitConfigDialog::onConnectComponent()
{
}

std::string armarx::PlatformUnitConfigDialog::getDefaultName() const
{
    return "PlatformUnitConfigDialog" + uuid;
}
