/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ARCHES::gui-plugins::BoxToGraspCandidatesWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/SpinBoxToVector.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/SpinBoxToPose.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/interface/observers/GraspCandidateObserverInterface.h>

#include <RobotAPI/gui-plugins/BoxToGraspCandidates/ui_BoxToGraspCandidatesWidget.h>

namespace armarx
{
    /**
    \page RobotAPI-GuiPlugins-BoxToGraspCandidates BoxToGraspCandidates
    \brief The BoxToGraspCandidates allows visualizing ...

    \image html BoxToGraspCandidates.png
    The user can

    API Documentation \ref BoxToGraspCandidatesWidgetController

    \see BoxToGraspCandidatesGuiPlugin
    */

    /**
     * \class BoxToGraspCandidatesWidgetController
     * \brief BoxToGraspCandidatesWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        BoxToGraspCandidatesWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < BoxToGraspCandidatesWidgetController >,
        public virtual ArVizComponentPluginUser,
        public virtual RobotStateComponentPluginUser
    {
        Q_OBJECT

    public:
        explicit BoxToGraspCandidatesWidgetController();
        virtual ~BoxToGraspCandidatesWidgetController() = default;

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

        static QString GetWidgetName()
        {
            return "Grasping.BoxToGraspCandidates";
        }

        void onInitComponent() override {}
        void onConnectComponent() override;

        QPointer<QDialog> getConfigDialog(QWidget* parent);
        void configured() override;
    public slots:
        void update();

    private:
        std::mutex                              _mutex;
        QPointer<SimpleConfigDialog>            _dialog;
        Ui::BoxToGraspCandidatesWidget          _ui;

        SpinBoxToPose<QDoubleSpinBox>           _box_pose;
        SpinBoxToVector<QDoubleSpinBox, 3>      _box_extend;
        SpinBoxToVector<QDoubleSpinBox, 3>      _tcp_shift_l;
        SpinBoxToVector<QDoubleSpinBox, 3>      _tcp_shift_r;
        SpinBoxToVector<QDoubleSpinBox, 3>      _transversal_l;
        SpinBoxToVector<QDoubleSpinBox, 3>      _transversal_r;
        SpinBoxToVector<QDoubleSpinBox, 3>      _up_l;
        SpinBoxToVector<QDoubleSpinBox, 3>      _up_r;

        VirtualRobot::RobotPtr _robot;
        std::string                                        _gc_topic_name;
        armarx::grasping::GraspCandidatesTopicInterfacePrx _gc_topic;
    };
}


