/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::ArVizDrawerGuiWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <functional>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/gui-plugins/ArVizDrawerGui/ui_ArVizDrawerGuiWidget.h>

namespace armarx
{
    class ElementWidgetBase;

    /**
    \page RobotAPI-GuiPlugins-ArVizDrawerGui ArVizDrawerGui
    \brief The ArVizDrawerGui allows visualizing ...

    \image html ArVizDrawerGui.png
    The user can

    API Documentation \ref ArVizDrawerGuiWidgetController

    \see ArVizDrawerGuiGuiPlugin
    */

    /**
     * \class ArVizDrawerGuiWidgetController
     * \brief ArVizDrawerGuiWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        ArVizDrawerGuiWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < ArVizDrawerGuiWidgetController >,
        public virtual ArVizComponentPluginUser
    {
        Q_OBJECT
    public:
        explicit ArVizDrawerGuiWidgetController();
        virtual ~ArVizDrawerGuiWidgetController() = default;

        void loadSettings(QSettings* settings) override {}
        void saveSettings(QSettings* settings) override {}

        static QString GetWidgetName()
        {
            return "Visualization.ArVizDrawer";
        }

        void onInitComponent() override {}
        void onConnectComponent() override;
        void onDisconnectComponent() override;

    protected:
        void timerEvent(QTimerEvent* event) override;

    private slots:
        void on_pushButtonElementAdd_clicked();

    private:
        Ui::ArVizDrawerGuiWidget                                   _ui;
        std::atomic_bool                                           _connected{false};
        std::set<ElementWidgetBase*>                               _elements;
        std::map<std::string, std::function<ElementWidgetBase*()>> _factory;
    };
}


