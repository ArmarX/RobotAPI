/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::ArVizDrawerGuiWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2020
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>

#include "ArVizDrawerGuiWidgetController.h"
#include "Elements/ElementWidgetBase.h"
#include "Elements/RobotWidget.h"
#include "Elements/PoseWidget.h"

namespace armarx
{
    ArVizDrawerGuiWidgetController::ArVizDrawerGuiWidgetController()
    {
        _ui.setupUi(getWidget());
        connect(_ui.pushButtonElementAdd, &QPushButton::clicked, this,
                &ArVizDrawerGuiWidgetController::on_pushButtonElementAdd_clicked);
        //setup factories
        {
            _factory["Robot"] = [] { return new RobotWidget; };
            _factory["Pose" ] = [] { return new PoseWidget ; };

            for (const auto& [key, _] : _factory)
            {
                _ui.comboBoxElement->addItem(QString::fromStdString(key));
            }
        }
        startTimer(20);
    }

    void ArVizDrawerGuiWidgetController::onConnectComponent()
    {
        _connected = true;
    }
    void ArVizDrawerGuiWidgetController::onDisconnectComponent()
    {
        _connected = false;
    }

    void ArVizDrawerGuiWidgetController::on_pushButtonElementAdd_clicked()
    {
        const auto name = _ui.comboBoxElement->currentText().toStdString();
        auto* ptr = _factory.at(name)();
        _ui.verticalLayoutElements->addWidget(ptr);
        _elements.emplace(ptr);
    }

    void ArVizDrawerGuiWidgetController::timerEvent(QTimerEvent*)
    {
        if (!_connected)
        {
            return;
        }
        auto layer = getArvizClient().layer("elements");
        for (auto* elem : _elements)
        {
            if (elem->toDelete())
            {
                _ui.verticalLayoutElements->removeWidget(elem);
                elem->deleteLater();
                _elements.erase(elem);
            }
            else
            {
                elem->addTo(layer);
            }
        }
        getArvizClient().commit({layer});
    }
}

