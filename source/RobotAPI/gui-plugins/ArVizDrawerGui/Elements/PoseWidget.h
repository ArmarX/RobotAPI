#pragma once

#include "ElementWidgetBase.h"

#include <RobotAPI/gui-plugins/ArVizDrawerGui/ui_PoseWidget.h>

namespace armarx
{
    class PoseWidget : public ElementWidgetBaseTemplate<Ui::PoseWidget>
    {
    public:
        void addTo(viz::Layer& layer) const override;
    };
}
