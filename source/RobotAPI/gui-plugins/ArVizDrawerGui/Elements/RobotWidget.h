#pragma once

#include "ElementWidgetBase.h"

#include <RobotAPI/gui-plugins/ArVizDrawerGui/ui_RobotWidget.h>

namespace armarx
{
    class RobotWidget : public ElementWidgetBaseTemplate<Ui::RobotWidget>
    {
    public:
        void addTo(viz::Layer& layer) const override;
    };
}
