#pragma once

#include <QWidget>

#include <RobotAPI/components/ArViz/Client/Layer.h>
#include <RobotAPI/components/ArViz/Client/Elements.h>

namespace armarx
{

    class ElementWidgetBase : public QWidget
    {
    public:
        ~ElementWidgetBase() override = default;
        virtual void addTo(viz::Layer& layer) const = 0;
        virtual bool toDelete() const = 0;
    };

    template<class UiT>
    class ElementWidgetBaseTemplate : public ElementWidgetBase
    {
    public:
        ElementWidgetBaseTemplate()
        {
            _ui.setupUi(this);
            _ui.pushButtonDelete->setCheckable(true);
            _ui.pushButtonDelete->setChecked(false);
        }
        bool toDelete() const override
        {
            return _ui.pushButtonDelete->isChecked();
        }
        UiT _ui;
    };
}
