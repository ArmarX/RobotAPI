#include "PoseWidget.h"

namespace armarx
{
    void PoseWidget::addTo(viz::Layer& layer) const
    {
        const auto adr = reinterpret_cast<std::intptr_t>(this);
        layer.add(viz::Pose("Pose_" + std::to_string(adr))
                  .position(_ui.doubleSpinBoxTX->value(),
                            _ui.doubleSpinBoxTY->value(),
                            _ui.doubleSpinBoxTZ->value())
                  .orientation(_ui.doubleSpinBoxRX->value(),
                               _ui.doubleSpinBoxRY->value(),
                               _ui.doubleSpinBoxRZ->value())
                  .scale(_ui.doubleSpinBoxScale->value()));
    }
}
