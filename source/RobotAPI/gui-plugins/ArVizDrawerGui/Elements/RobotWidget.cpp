#include "RobotWidget.h"

namespace armarx
{
    void RobotWidget::addTo(viz::Layer& layer) const
    {
        const auto adr = reinterpret_cast<std::intptr_t>(this);
        auto r = viz::Robot("Robot_" + std::to_string(adr))
                 .file(_ui.lineEditProject->text().toStdString(),
                       _ui.comboBoxFile->currentText().toStdString())
                 .position(_ui.doubleSpinBoxTX->value(),
                           _ui.doubleSpinBoxTY->value(),
                           _ui.doubleSpinBoxTZ->value())
                 .orientation(_ui.doubleSpinBoxRX->value(),
                              _ui.doubleSpinBoxRY->value(),
                              _ui.doubleSpinBoxRZ->value());
        if (_ui.checkBoxColor->isChecked())
        {
            r.overrideColor(viz::Color{static_cast<float>(_ui.doubleSpinBoxR->value()),
                                       static_cast<float>(_ui.doubleSpinBoxG->value()),
                                       static_cast<float>(_ui.doubleSpinBoxB->value()),
                                       static_cast<float>(_ui.doubleSpinBoxA->value())});
        }
        layer.add(r);
    }
}
