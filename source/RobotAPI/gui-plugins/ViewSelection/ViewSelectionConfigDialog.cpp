#include "ViewSelectionConfigDialog.h"
#include <RobotAPI/gui-plugins/ViewSelection/ui_ViewSelectionConfigDialog.h>

#include <RobotAPI/interface/components/ViewSelectionInterface.h>

#include <QPushButton>
#include <QMessageBox>
#include <IceUtil/UUID.h>

using namespace armarx;

ViewSelectionConfigDialog::ViewSelectionConfigDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::ViewSelectionConfigDialog),
    uuid(IceUtil::generateUUID())
{
    ui->setupUi(this);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setDefault(true);
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(verifyConfig()));

    viewSelectionProxyFinder = new IceProxyFinder<ViewSelectionInterfacePrx>(this);
    viewSelectionProxyFinder->setSearchMask("*ViewSelection");
    //ui->verticalLayout->addWidget(viewSelectionProxyFinder);
}

ViewSelectionConfigDialog::~ViewSelectionConfigDialog()
{
    delete ui;
}


void ViewSelectionConfigDialog::onInitComponent()
{
}

void ViewSelectionConfigDialog::onConnectComponent()
{
    viewSelectionProxyFinder->setIceManager(getIceManager());
}

void ViewSelectionConfigDialog::onExitComponent()
{
    QObject::disconnect();
}

std::string ViewSelectionConfigDialog::getDefaultName() const
{
    return "ViewSelectionConfigDialog" + uuid;
}

void ViewSelectionConfigDialog::verifyConfig()
{

    if (!viewSelectionProxyFinder->getSelectedProxyName().trimmed().length())
    {
        QMessageBox::critical(this, "Invalid Configuration", "The proxy names must not be empty");
        return;
    }

    this->accept();
}
