/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::GuiHealthClientWidgetController
 * \author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * \date       2018
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GuiHealthClientWidgetController.h"

#include <QLabel>
#include <string>
#include <QPushButton>

namespace armarx
{
    GuiHealthClientWidgetController::GuiHealthClientWidgetController()
    {
        widget.setupUi(getWidget());
        //ARMARX_IMPORTANT << "ctor start";

        healthTimer = new QTimer(this);
        healthTimer->setInterval(10);
        connect(healthTimer, SIGNAL(timeout()), this, SLOT(healthTimerClb()));
        connect(widget.pushButtonToggleOwnHeartbeats, SIGNAL(clicked()), this, SLOT(toggleSendOwnHeartbeats()));
        connect(this, SIGNAL(invokeConnectComponentQt()), this, SLOT(onConnectComponentQt()), Qt::QueuedConnection);
        connect(this, SIGNAL(invokeDisconnectComponentQt()), this, SLOT(onDisconnectComponentQt()), Qt::QueuedConnection);

        updateSummaryTimer = new QTimer(this);
        updateSummaryTimer->setInterval(100);
        connect(updateSummaryTimer, SIGNAL(timeout()), this, SLOT(updateSummaryTimerClb()));

        widget.labelState->setText("idle.");
        widget.pushButtonToggleOwnHeartbeats->setText("send own heartbeats");

        //ARMARX_IMPORTANT << "ctor end";
    }


    GuiHealthClientWidgetController::~GuiHealthClientWidgetController()
    {

    }


    void GuiHealthClientWidgetController::loadSettings(QSettings* settings)
    {

    }

    void GuiHealthClientWidgetController::saveSettings(QSettings* settings)
    {

    }


    void GuiHealthClientWidgetController::onInitComponent()
    {
        //ARMARX_IMPORTANT << "onInitComponent";
        usingProxy("RobotHealth");
        offeringTopic("RobotHealthTopic");
    }

    void GuiHealthClientWidgetController::healthTimerClb()
    {
        RobotHealthHeartbeatArgs rhha;
        rhha.maximumCycleTimeWarningMS = 250;
        rhha.maximumCycleTimeErrorMS = 500;
        robotHealthTopicPrx->heartbeat(getName(), RobotHealthHeartbeatArgs());
    }
    void GuiHealthClientWidgetController::updateSummaryTimerClb()
    {
        //ARMARX_IMPORTANT << "updateSummaryTimerClb";
        if (robotHealthComponentPrx)
        {
            //ARMARX_IMPORTANT << "has robotHealthComponentPrx";
            try
            {
                //ARMARX_IMPORTANT << "before set text";
                widget.labelHealthState->setText(QString::fromUtf8(robotHealthComponentPrx->getSummary().c_str()));
                //ARMARX_IMPORTANT << "after set text";
            }
            catch (...)
            {}
        }
    }

    void GuiHealthClientWidgetController::toggleSendOwnHeartbeats()
    {
        if (ownHeartbeatsActive)
        {
            ownHeartbeatsActive = false;
            healthTimer->stop();
            robotHealthTopicPrx->unregister(getName());
            widget.labelState->setText("idle.");
            //widget.pushButtonToggleOwnHeartbeats->setDisabled(true);
            widget.pushButtonToggleOwnHeartbeats->setText("send own heartbeats");
        }
        else
        {
            ownHeartbeatsActive = true;
            healthTimer->start();
            widget.labelState->setText("sending heartbeats...");
            widget.pushButtonToggleOwnHeartbeats->setText("unregister self");
        }

    }


    void GuiHealthClientWidgetController::onConnectComponent()
    {
        //ARMARX_IMPORTANT << "onConnectComponent";
        robotHealthTopicPrx = getTopic<RobotHealthInterfacePrx>("RobotHealthTopic");
        robotHealthComponentPrx = getProxy<RobotHealthComponentInterfacePrx>("RobotHealth");
        invokeConnectComponentQt();
    }
    void GuiHealthClientWidgetController::onConnectComponentQt()
    {
        //healthTimer->start();
        //ARMARX_IMPORTANT << "updateSummaryTimer->start";
        updateSummaryTimer->start();
    }


    void GuiHealthClientWidgetController::onDisconnectComponent()
    {
        invokeDisconnectComponentQt();
    }
    void GuiHealthClientWidgetController::onDisconnectComponentQt()
    {
        healthTimer->stop();
        updateSummaryTimer->stop();
    }
}
