armarx_set_target("DebugDrawerViewerGuiPlugin")

set(SOURCES DebugDrawerViewerGuiPlugin.cpp DebugDrawerViewerWidgetController.cpp)
set(HEADERS DebugDrawerViewerGuiPlugin.h   DebugDrawerViewerWidgetController.h)
set(GUI_MOC_HDRS DebugDrawerViewerGuiPlugin.h DebugDrawerViewerWidgetController.h)
set(GUI_UIS DebugDrawerViewerWidget.ui)

set(COMPONENT_LIBS DebugDrawer)

if(ArmarXGui_FOUND)
	armarx_gui_library(DebugDrawerViewerGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}")
endif()
