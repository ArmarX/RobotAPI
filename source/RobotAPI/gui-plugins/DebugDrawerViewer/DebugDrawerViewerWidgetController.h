/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::DebugDrawerViewerWidgetController
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/gui-plugins/DebugDrawerViewer/ui_DebugDrawerViewerWidget.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>


namespace armarx
{
    /**
    \page RobotAPI-GuiPlugins-DebugDrawerViewer DebugDrawerViewer
    \brief The DebugDrawerViewer listens on the DebugDrawer topic and visualizes all incoming data.
    This GUI only displays the DebugDrawer content and nothing else.
    The content is shown in the 3D Viewer Widget.

    API Documentation: \ref DebugDrawerViewerWidgetController

    \see DebugDrawerViewerGuiPlugin
    */

    /**
     * \class DebugDrawerViewerWidgetController
     * \brief DebugDrawerViewerWidgetController
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        DebugDrawerViewerWidgetController :
        public ArmarXComponentWidgetControllerTemplate<DebugDrawerViewerWidgetController>
    {
        Q_OBJECT

    public:

        /// Controller Constructor
        explicit DebugDrawerViewerWidgetController();

        /// Controller destructor
        virtual ~DebugDrawerViewerWidgetController() override = default;

        /// @see ArmarXWidgetController::loadSettings()
        void loadSettings(QSettings* settings) override;

        /// @see ArmarXWidgetController::saveSettings()
        void saveSettings(QSettings* settings) override;

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Visualization.DebugDrawerViewer";
        }

        /// @see armarx::Component::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::Component::onConnectComponent()
        void onConnectComponent() override;

        void onExitComponent() override;


    public slots:
        /* QT slot declarations */

    signals:
        /* QT signal declarations */


    private slots:

        /// Clear all layers.
        void on_btnClearAll_clicked();

        /// Clear the selected layer.
        void on_btnClearLayer_clicked();

        /// Fetch the layer information and update the combo box items.
        void updateComboClearLayer();


    private:

        /// Make an item text for a layer entry in the clear layer combo box.
        static QString makeLayerItemText(const LayerInformation& layer);


    private:

        /// Widget Form
        Ui::DebugDrawerViewerWidget widget;

        /// The debug drawer.
        armarx::DebugDrawerComponentPtr debugDrawer;

        SoSeparator* rootVisu;

        // ArmarXWidgetController interface
    public:
        SoNode* getScene() override;

        // ArmarXWidgetController interface
    public:
        static QIcon GetWidgetIcon()
        {
            return QIcon(":icons/Outline-3D-DebugDrawer.png");
        }
    };
}

