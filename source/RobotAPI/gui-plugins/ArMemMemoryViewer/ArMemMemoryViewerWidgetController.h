/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::gui-plugins::ArMemMemoryViewerWidgetController
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <memory>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <RobotAPI/libraries/armem_gui/lifecycle.h>
#include <RobotAPI/libraries/armem_gui/MemoryViewer.h>

#include <RobotAPI/gui-plugins/ArMemMemoryViewer/ui_ArMemMemoryViewerWidget.h>


namespace armarx
{
    /**
     * @page RobotAPI-GuiPlugins-ArMemMemoryViewer ArMemMemoryViewer
     * @brief The ArMemMemoryViewer allows visualizing ...
     * @image html ArMemMemoryViewer.png
     * The user can
     *
     * API Documentation @ref ArMemMemoryViewerWidgetController
     *
     * @see ArMemMemoryViewerGuiPlugin
     */

    /**
     * @class ArMemMemoryViewerGuiPlugin
     * @ingroup ArmarXGuiPlugins
     * @brief ArMemMemoryViewerGuiPlugin brief description
     *
     * Detailed description
     */

    /**
     * @class ArMemMemoryViewerWidgetController
     * @brief ArMemMemoryViewerWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        ArMemMemoryViewerWidgetController :
        public armarx::ArmarXComponentWidgetControllerTemplate < ArMemMemoryViewerWidgetController >
    {
        Q_OBJECT
        using This = ArMemMemoryViewerWidgetController;
        using MemoryViewer = armarx::armem::gui::MemoryViewer;

    public:

        static QString GetWidgetName();
        static QIcon GetWidgetIcon();

        explicit ArMemMemoryViewerWidgetController();
        virtual ~ArMemMemoryViewerWidgetController() override;


        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;

        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;


        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;


    public slots:

    signals:


    private slots:

    signals:


    private:

        /// Widget Form
        Ui::ArMemMemoryViewerWidget widget;
        armarx::gui::LifecycleServer lifecycleServer;

        QPointer<SimpleConfigDialog> configDialog;

        std::unique_ptr<MemoryViewer> viewer;

    };
}


