/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    RobotAPI::gui-plugins::ArMemMemoryViewerWidgetController
 * \author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * \date       2020
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArMemMemoryViewerWidgetController.h"

#include <RobotAPI/libraries/armem_gui/gui_utils.h>

#include <string>


namespace armarx
{

    QString ArMemMemoryViewerWidgetController::GetWidgetName()
    {
        return "ArMem.MemoryViewer";
    }


    QIcon ArMemMemoryViewerWidgetController::GetWidgetIcon()
    {
        return QIcon(":icons/memory-128.png");
    }


    ArMemMemoryViewerWidgetController::ArMemMemoryViewerWidgetController()
    {
        widget.setupUi(getWidget());

        viewer = std::make_unique<MemoryViewer>(

                     widget.updateWidgetLayout,

                     widget.memoryGroupBox,
                     widget.treesLayout,

                     widget.instanceGroupBox,
                     widget.treesLayout,

                     widget.diskControlWidgetLayout,

                     widget.statusLabel
                 );
        viewer->setLogTag("ArMem Memory Viewer");

        armarx::gui::useSplitter(widget.treesLayout);

        armarx::gui::connectLifecycle(&lifecycleServer, viewer.get());
    }

    ArMemMemoryViewerWidgetController::~ArMemMemoryViewerWidgetController()
    {
    }


    void ArMemMemoryViewerWidgetController::onInitComponent()
    {
        emit lifecycleServer.initialized(this);
    }

    void ArMemMemoryViewerWidgetController::onConnectComponent()
    {
        emit lifecycleServer.connected(this);
    }

    void ArMemMemoryViewerWidgetController::onDisconnectComponent()
    {
        emit lifecycleServer.disconnected(this);
    }


    void ArMemMemoryViewerWidgetController::loadSettings(QSettings* settings)
    {
        viewer->loadSettings(settings);
    }
    void ArMemMemoryViewerWidgetController::saveSettings(QSettings* settings)
    {
        viewer->saveSettings(settings);
    }

    QPointer<QDialog> ArMemMemoryViewerWidgetController::getConfigDialog(QWidget* parent)
    {
        if (!configDialog)
        {
            configDialog = new SimpleConfigDialog(parent);
            viewer->writeConfigDialog(configDialog.data());
        }
        return qobject_cast<QDialog*>(configDialog);
    }

    void ArMemMemoryViewerWidgetController::configured()
    {
        if (configDialog)
        {
            viewer->readConfigDialog(configDialog.data());
        }
    }
}
