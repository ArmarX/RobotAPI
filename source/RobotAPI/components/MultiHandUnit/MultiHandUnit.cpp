/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::MultiHandUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MultiHandUnit.h"


namespace armarx
{
    MultiHandUnitPropertyDefinitions::MultiHandUnitPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("HandUnitNameCSV", "", "");
    }


    std::string MultiHandUnit::getDefaultName() const
    {
        return "MultiHandUnit";
    }


    void MultiHandUnit::onInitComponent()
    {
        const auto units = getPropertyAsCSV<std::string>("HandUnitNameCSV");
        if (units.empty())
        {
            ARMARX_ERROR << "PROPERTY HandUnitNameCSV IS EMPTY";
        }
        _handInfos.reserve(units.size());
        for (const auto& name : units)
        {
            _handInfos.emplace_back();
            _handInfos.back().proxyName = name;
            usingProxy(name);
        }
    }

    void MultiHandUnit::onConnectComponent()
    {
        for (auto& info : _handInfos)
        {
            getProxy(info.proxy, info.proxyName);
            info.handName = info.proxy->getHandName();
            _hands[info.handName] = info.proxy;
        }
    }

    void MultiHandUnit::onDisconnectComponent()
    {
        _hands.clear();
    }

    void MultiHandUnit::onExitComponent()
    {

    }

    armarx::PropertyDefinitionsPtr MultiHandUnit::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new MultiHandUnitPropertyDefinitions(
                getConfigIdentifier()));
    }

    HandInfoSeq MultiHandUnit::getHandInfos(const Ice::Current&)
    {
        return _handInfos;
    }

    void MultiHandUnit::setJointValues(const std::string& handName, const NameValueMap& jointValues, const Ice::Current&)
    {
        _hands.at(handName)->setJointAngles(jointValues);
    }

    NameValueMap MultiHandUnit::getJointValues(const std::string& handName, const Ice::Current&)
    {
        return _hands.at(handName)->getCurrentJointValues();
    }
}
