/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::MultiHandUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/units/MultiHandUnitInterface.h>


namespace armarx
{
    /**
     * @class MultiHandUnitPropertyDefinitions
     * @brief
     */
    class MultiHandUnitPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        MultiHandUnitPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-MultiHandUnit MultiHandUnit
     * @ingroup RobotAPI-Components
     * A description of the component MultiHandUnit.
     *
     * @class MultiHandUnit
     * @ingroup Component-MultiHandUnit
     * @brief Brief description of class MultiHandUnit.
     *
     * Detailed description of class MultiHandUnit.
     */
    class MultiHandUnit :
        virtual public armarx::Component,
        virtual public MultiHandUnitInterface
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        virtual void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        virtual void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        virtual void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        virtual void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        HandInfoSeq getHandInfos(const Ice::Current& = Ice::emptyCurrent) override;
        void setJointValues(const std::string& handName, const NameValueMap& jointValues, const Ice::Current& = Ice::emptyCurrent) override;
        NameValueMap getJointValues(const std::string& handName, const Ice::Current& = Ice::emptyCurrent) override;
    private:
        HandInfoSeq _handInfos;
        std::map<std::string, HandUnitInterfacePrx> _hands;
    };
}
