

#include "SkillProviderExample.h"

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/aron/core/type/variant/primitive/String.h>
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

#include <RobotAPI/components/skills/SkillProviderExample/aron/HelloWorldAcceptedType.aron.generated.h>

namespace armarx::skills::provider
{
    HelloWorldSkill::HelloWorldSkill() :
        Skill(GetSkillDescription())
    {}

    SkillDescription HelloWorldSkill::GetSkillDescription()
    {
        armarx::skills::Example::HelloWorldAcceptedType default_params;
        default_params.some_float = 5;
        default_params.some_int = 42;
        default_params.some_text = "YOLO";

        return SkillDescription{
            "HelloWorld",
            "This skill logs a message on ARMARX_IMPORTANT",
            {},
            armarx::core::time::Duration::MilliSeconds(1000),
            armarx::skills::Example::HelloWorldAcceptedType::ToAronType(),
            default_params.toAron()
        };
    }

    Skill::MainResult HelloWorldSkill::main(const MainInput& in)
    {
        ARMARX_IMPORTANT << "Hi, from the Hello World Skill.\n" <<
                                "I received the following data: \n" <<
                                aron::converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(in.params).dump(2) << "\n" <<
                                "(executed at: " << IceUtil::Time::now() << ")";
        return {TerminatedSkillStatus::Succeeded, nullptr};
    }

    ChainingSkill::ChainingSkill() :
        Skill(GetSkillDescription())
    {}

    SkillDescription ChainingSkill::GetSkillDescription()
    {
        return SkillDescription{
            "ChainingSkill",
            "This skill calls the HelloWorld skill three times.",
            {},
            armarx::core::time::Duration::MilliSeconds(3000),
            nullptr
        };
    }

    Skill::MainResult ChainingSkill::main(const MainInput& in)
    {
        armarx::skills::Example::HelloWorldAcceptedType exec1;
        armarx::skills::Example::HelloWorldAcceptedType exec2;
        armarx::skills::Example::HelloWorldAcceptedType exec3;

        exec1.some_text = "Hello from the ChainingSkill 1";
        exec2.some_text = "Hello from the ChainingSkill 2";
        exec3.some_text = "Hello from the ChainingSkill 3";

        SkillProxy skillExecPrx(manager, {"SkillProviderExample", "HelloWorld"});

        skillExecPrx.executeFullSkill(getSkillId().toString(), exec1.toAron());
        skillExecPrx.executeFullSkill(getSkillId().toString(), exec2.toAron());
        skillExecPrx.executeFullSkill(getSkillId().toString(), exec3.toAron());

        return {TerminatedSkillStatus::Succeeded, nullptr};
    }

    TimeoutSkill::TimeoutSkill() :
        PeriodicSkill(GetSkillDescription(), armarx::core::time::Frequency::Hertz(5))
    {}

    SkillDescription TimeoutSkill::GetSkillDescription()
    {
        return SkillDescription{
            "Timeout",
            "This fails with timeout reached",
            {},
            armarx::core::time::Duration::MilliSeconds(1000),
            nullptr
        };
    }

    PeriodicSkill::StepResult TimeoutSkill::step(const MainInput& in)
    {
        // do heavy work
        std::this_thread::sleep_for(std::chrono::milliseconds(200));

        return {ActiveOrTerminatedSkillStatus::Running, nullptr};
    }

    CallbackSkill::CallbackSkill() :
        Skill(GetSkillDescription())
    {}

    SkillDescription CallbackSkill::GetSkillDescription()
    {
        return SkillDescription{
            "ShowMeCallbacks",
            "This skill does shows callbacks",
            {},
            armarx::core::time::Duration::MilliSeconds(1000),
            nullptr
        };
    }

    Skill::MainResult CallbackSkill::main(const MainInput& in)
    {
        ARMARX_IMPORTANT << "Logging three updates via the callback";
        auto up1 = std::make_shared<aron::data::Dict>();
        up1->addElement("updateInfo", std::make_shared<aron::data::String>("Update 1"));
        in.callback(up1);

        auto up2 = std::make_shared<aron::data::Dict>();
        up2->addElement("updateInfo", std::make_shared<aron::data::String>("Update 2"));
        in.callback(up2);

        auto up3 = std::make_shared<aron::data::Dict>();
        up3->addElement("updateInfo", std::make_shared<aron::data::String>("Update 3"));
        in.callback(up3);

        return {TerminatedSkillStatus::Succeeded, nullptr};
    }


    SkillProviderExample::SkillProviderExample() :
        SkillProviderComponentPluginUser()
    {}

    armarx::PropertyDefinitionsPtr SkillProviderExample::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());
        return defs;
    }

    std::string SkillProviderExample::getDefaultName() const
    {
        return "SkillProviderExample";
    }

    void SkillProviderExample::onInitComponent()
    {
        // Add example skill
        addSkill(std::make_unique<HelloWorldSkill>());

        // Add another lambda example skill
        {
            skills::SkillDescription fooDesc;
            fooDesc.acceptedType = nullptr; // accept everything
            fooDesc.description = "This skill does exactly nothing.";
            fooDesc.skillName = "Foo";
            fooDesc.timeout = armarx::core::time::Duration::MilliSeconds(1000);
            addSkill([](const std::string& clientId, const aron::data::DictPtr&){
                std::cout << "Hello from Foo. The skill was called from " << clientId << "." << std::endl;
                return TerminatedSkillStatus::Succeeded;
            }, fooDesc);
        }

        // Add another example skill
        addSkill(std::make_unique<CallbackSkill>());

        // Add timeout skill
        addSkill(std::make_unique<TimeoutSkill>());

        // chaining
        addSkill(std::make_unique<ChainingSkill>());
    }

    void SkillProviderExample::onConnectComponent()
    {

    }

    void SkillProviderExample::onDisconnectComponent()
    {

    }

    void SkillProviderExample::onExitComponent()
    {

    }
}
