
/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


// ArmarX
#include <ArmarXCore/core/Component.h>

// RobotAPI
#include <RobotAPI/libraries/skills/provider/SkillProxy.h>
#include <RobotAPI/libraries/skills/provider/SkillProviderComponentPlugin.h>

namespace armarx::skills::provider
{
    // Skills:
    class HelloWorldSkill : public Skill
    {
    public:
        HelloWorldSkill();

        static SkillDescription GetSkillDescription();

    private:
        Skill::MainResult main(const MainInput& in) final;
    };

    class ChainingSkill : public Skill
    {
    public:
        ChainingSkill();

        static SkillDescription GetSkillDescription();

    private:
        Skill::MainResult main(const MainInput& in) final;
    };


    class TimeoutSkill : public PeriodicSkill
    {
    public:
        TimeoutSkill();

        static SkillDescription GetSkillDescription();

    private:
        PeriodicSkill::StepResult step(const MainInput& in) final;
    };


    class CallbackSkill : public Skill
    {
    public:
        CallbackSkill();

        static SkillDescription GetSkillDescription();

    private:
        Skill::MainResult main(const MainInput& in) final;
    };

    /**
     * @defgroup Component-ExampleClient ExampleClient
     * @ingroup RobotAPI-Components
     * A description of the component ExampleClient.
     *
     * @class ExampleClient
     * @ingroup Component-ExampleClient
     * @brief Brief description of class ExampleClient.
     *
     * Detailed description of class ExampleClient.
     */
    class SkillProviderExample :
            virtual public armarx::Component,
            virtual public SkillProviderComponentPluginUser
    {
    public:
        SkillProviderExample();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

    private:
    };
}
