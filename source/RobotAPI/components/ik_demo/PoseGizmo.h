#pragma once

#include <RobotAPI/components/ArViz/Client/Elements.h>
#include <RobotAPI/components/ArViz/Client/Layer.h>
#include <RobotAPI/components/ArViz/Client/Client.h>


namespace armarx::viz
{

    class PoseGizmo
    {
    public:

        PoseGizmo();

        void setLayer(const viz::Layer& layer);

        void update();
        void updateDuringTransform();

        bool handleInteraction(const viz::InteractionFeedback& interaction,
                               viz::StagedCommit* stage);

        Eigen::Matrix4f getCurrent() const;


    public:

        Eigen::Matrix4f initial = Eigen::Matrix4f::Identity();
        Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();

        viz::Layer layer;

        viz::Box box { "box" };
        viz::Pose pose { "pose" };

    };

}
