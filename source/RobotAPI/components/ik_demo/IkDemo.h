#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <ArmarXCore/core/application/properties/forward_declarations.h>

#include <RobotAPI/components/ArViz/Client/Client.h>


namespace armar6::skills::components::armar6_ik_demo
{

    struct Manipulator;


    class IkDemo
    {
    public:

        struct Params
        {
            Params();

            std::string robotFile;
            std::string robotNodeSetNamesStr;
            std::vector<std::string> robotNodeSetNames() const;
        };
        struct Remote
        {
            std::optional<armarx::viz::Client> arviz;
        };
        struct Robot
        {
            VirtualRobot::RobotPtr robot;

            armarx::viz::Layer layer;
            armarx::viz::Robot visu { "robot" };
        };


    public:

        IkDemo();
        ~IkDemo();

        void defineProperties(armarx::PropertyDefinitionContainer& defs);

        void connect(Remote&& remote);

        void start();
        void update();

        void runIk();


    public:

        Remote remote;
        Params params;
        Robot robot;

        armarx::viz::StagedCommit stage;

        std::vector<std::unique_ptr<Manipulator>> manipulators;

    };

}
