#include "PoseGizmo.h"


namespace viz = armarx::viz;


namespace armarx::viz
{

    PoseGizmo::PoseGizmo()
    {
        box.enable(viz::interaction().selection().transform().hideDuringTransform());
    }

    void PoseGizmo::setLayer(const Layer& layer_)
    {
        this->layer = layer_;
        this->layer.add(box);
        this->layer.add(pose);
    }

    void PoseGizmo::update()
    {
        const Eigen::Matrix4f current = getCurrent();
        box.pose(current);
        pose.pose(current);
    }

    void PoseGizmo::updateDuringTransform()
    {
        const Eigen::Matrix4f current = getCurrent();
        box.pose(initial);
        pose.pose(current);
    }

    bool
    PoseGizmo::handleInteraction(
            const InteractionFeedback& interaction,
            StagedCommit* stage)
    {
        if (interaction.element() != box.data_->id)
        {
            return false;
        }

        switch (interaction.type())
        {
        case viz::InteractionFeedbackType::Select:
        {
            // Nothing to do.
        } break;

        case viz::InteractionFeedbackType::Transform:
        {
            // Update state of tcp object
            transform = interaction.transformation();

            if (interaction.isTransformDuring())
            {
                updateDuringTransform();
            }

            stage->add(layer);
            return true;
        } break;

        case viz::InteractionFeedbackType::Deselect:
        {
            // If an object is deselected, we apply the transformation

            initial = getCurrent();
            transform.setIdentity();

            update();
            stage->add(layer);

            return true;
        } break;

        case viz::InteractionFeedbackType::ContextMenuChosen:
        {
        }

        default:
        {
            // Ignore other interaction types
        } break;

        }

        return false;
    }


    Eigen::Matrix4f PoseGizmo::getCurrent() const
    {
        return transform * initial;
    }

}


