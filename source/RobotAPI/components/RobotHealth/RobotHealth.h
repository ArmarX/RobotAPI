/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotHealth
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/components/RobotHealthInterface.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/interface/speech/SpeechInterface.h>

#include <ArmarXGui/interface/RemoteGuiInterface.h>

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/interface/components/EmergencyStopInterface.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

#include <atomic>
#include <mutex>

namespace armarx
{
    /**
     * @class RobotHealthPropertyDefinitions
     * @brief
     */
    class RobotHealthPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        RobotHealthPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("EmergencyStopTopicName", "EmergencyStop", "The name of the topic over which changes of the emergencyStopState are sent.");
            defineOptionalProperty<std::string>("RobotHealthTopicName", "RobotHealthTopic", "Name of the RobotHealth topic");
            defineOptionalProperty<bool>("ReportErrorsWithSpeech", true, "");
            defineOptionalProperty<std::string>("TextToSpeechTopicName", "TextToSpeech", "Name of the TextToSpeech topic");
            defineOptionalProperty<int>("MaximumCycleTimeWarnMS", 50, "Default value of the maximum cycle time for warnings");
            defineOptionalProperty<int>("MaximumCycleTimeErrMS", 100, "Default value of the maximum cycle time for error");
            defineOptionalProperty<std::string>("AggregatedRobotHealthTopicName", "AggregatedRobotHealthTopic", "Name of the AggregatedRobotHealthTopic");
            defineOptionalProperty<std::string>("RequiredComponents", "", "Comma separated list of required components");

            defineOptionalProperty<int>("SpeechMinimumReportInterval", 60, "Time that has to pass between reported messages in seconds.");

            //defineOptionalProperty<std::string>("RemoteGuiName", "RemoteGuiProvider", "Name of the remote GUI provider");
            //defineOptionalProperty<std::string>("RobotUnitName", "Armar6Unit", "Name of the RobotUnit");
            //defineOptionalProperty<bool>("RobotUnitRequired", true, "Wait for RobotUnit");

            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-RobotHealth RobotHealth
     * @ingroup RobotAPI-Components
     * A description of the component RobotHealth.
     *
     * @class RobotHealth
     * @ingroup Component-RobotHealth
     * @brief Brief description of class RobotHealth.
     *
     * Detailed description of class RobotHealth.
     */
    class RobotHealth :
        virtual public RobotHealthComponentInterface,
        virtual public armarx::Component,
        virtual public armarx::DebugObserverComponentPluginUser
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "RobotHealth";
        }

        struct Entry
        {
            Entry(std::string name, long maximumCycleTimeWarn, long maximumCycleTimeErr)
                : name(name), maximumCycleTimeWarn(maximumCycleTimeWarn), maximumCycleTimeErr(maximumCycleTimeErr), history(100, -1)
            {}
            Entry(Entry&&) = default;
            Entry& operator=(Entry&&) = default;

            std::string name;
            long maximumCycleTimeWarn;
            long maximumCycleTimeErr;
            long lastUpdate = 0;
            long lastDelta = 0;
            RobotHealthState state = HealthOK;
            std::string message = "";
            bool isRunning = false;
            bool required = false;
            bool enabled = true;
            std::mutex messageMutex;
            std::vector<long> history;
            size_t historyIndex = 0;
        };

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void monitorHealthTaskClb();
        Entry& findOrCreateEntry(const std::string& name);

        void reportDebugObserver();

        std::mutex mutex;
        std::deque<Entry> entries;
        std::atomic_size_t validEntries {0};
        PeriodicTask<RobotHealth>::pointer_type monitorHealthTask;
        int defaultMaximumCycleTimeWarn;
        int defaultMaximumCycleTimeErr;
        EmergencyStopListenerPrx emergencyStopTopicPrx;
        //RobotUnitInterfacePrx robotUnitPrx;
        //bool robotUnitRequired;
        RemoteGuiInterfacePrx remoteGuiPrx;
        AggregatedRobotHealthInterfacePrx aggregatedRobotHealthTopicPrx;
        TextListenerInterfacePrx textToSpeechTopic;
        bool reportErrorsWithSpeech;
        int speechMinimumReportInterval;
        IceUtil::Time lastSpeechOutput;

        // RobotHealthInterface interface
    public:
        void heartbeat(const std::string& componentName, const RobotHealthHeartbeatArgs& args, const Ice::Current&) override;
        void unregister(const std::string& componentName, const Ice::Current&) override;
        std::string getSummary(const Ice::Current&) override;

    };
}
