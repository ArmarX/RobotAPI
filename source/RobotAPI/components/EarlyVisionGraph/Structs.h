// *****************************************************************
// Filename:    Structs.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        12.06.2007
// *****************************************************************

#pragma once

#include <vector>
#include <Eigen/Core>

// *****************************************************************
// structs
// *****************************************************************

// sphere coordinates with the following convention:
// phi: rotation from positive z-axis (pointing up) to the vector (p-o) (around the rotated y-axis)
// theta: rotation from the positive x-axis to the projection of the point in xy plane (around z-axis)
struct TSphereCoord
{
    float fDistance;
    float fPhi;     // zenith
    float fTheta;   // azimuth
};

// transformation from one sphere to another
// both spheres wiht same radius
// psi: orientation of the view = rotation around the rotated (by spherical coords) x-axis
struct TSphereTransform
{
    TSphereCoord    fPosition;
    float           fPsi;       // orientation
};

// transformation from one sphere to another
// both spheres wiht same radius
// fAlpha: rotation around the z axis in counterclockwise direction considering the positive z-axis
// fBeta: rotation around the rotated y axis in counterclockwise direction considering the positive y-axis
struct TTransform
{
    float fAlpha;
    float fBeta;
    Eigen::Vector3d betaAxis;
    float fPsi;
    Eigen::Vector3d psiAxis;
};

// *****************************************************************
// structs
// *****************************************************************
struct TPathElement
{
    TSphereCoord    Position;
    int             nNodeIndex;
    int             nHits;
    float           fBestSimilarity;
};

using TPath = std::vector<TPathElement>;

struct THypothesis
{
    int             nID;
    TPath           Path;
    // path look-a-like rating
    float           fRating;
    // visual similarity of last comparison
    float           fSimilarity;
    // overall visual similarity
    float           fAccSimilarity;
    // overall rating
    float           fOverallRating;
    TTransform      Transform;
    bool            bValidTransform;
};



