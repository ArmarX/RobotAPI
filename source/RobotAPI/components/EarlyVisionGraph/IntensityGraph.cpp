// *****************************************************************
// Filename:    SensoryEgoSphere.cpp
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        21.07.2008
// *****************************************************************

// *****************************************************************
// includes
// *****************************************************************
#include "IntensityGraph.h"
//#include "Base/Math/DistributePoints.h"
//#include "MathTools.h"
//#include "Base/DataStructures/Graph/GraphProcessor.h"

#include <cfloat>
#include <fstream>

// *****************************************************************
// implementation of CIntensityNode
// *****************************************************************
CIntensityNode::CIntensityNode()
{
    setIntensity(0.0f);
}

CIntensityGraph::CIntensityGraph(const CIntensityGraph& prototype)

    = default;

CIntensityNode::CIntensityNode(TSphereCoord coord)
    : CSGNode(coord)
{
    setIntensity(0.0f);
}

CIntensityNode::~CIntensityNode()
    = default;

void CIntensityNode::setIntensity(float fIntensity)
{
    m_fIntensity = fIntensity;
}

float CIntensityNode::getIntensity()
{
    return m_fIntensity;
}

// *****************************************************************
// implementation of CIntensityGraph
// *****************************************************************
// *****************************************************************
// construction / destruction
// *****************************************************************
CIntensityGraph::CIntensityGraph()
{
    reset();
}

//CIntensityGraph::CIntensityGraph(int nNumberNodes)
//{
//  // generate points on sphere
//  points P = generatePoints(nNumberNodes,1000);

//  for(int i = 0 ; i < int(P.V.size()) ; i++)
//  {
//      // project point to polar coordinates
//      Vec3d point;
//      Math3d::SetVec(point,P.V.at(i).array()[0],P.V.at(i).array()[1],P.V.at(i).array()[2]);
//      TSphereCoord point_on_sphere;
//      MathTools::convert(point, point_on_sphere);

//      CIntensityNode* pNode = new CIntensityNode(point_on_sphere);

//      addNode(pNode);
//  }

//  // initialize
//  reset();
//}

CIntensityGraph::CIntensityGraph(std::string sFilename)
{
    printf("Reading intensity graph from: %s\n", sFilename.c_str());

    std::ifstream infile;
    infile.open(sFilename.c_str());

    read(infile);

    //printf("Read number nodes: %d\n", getNodes()->size());
    //printf("Read number edges: %d\n", getEdges()->size());
}

CIntensityGraph::~CIntensityGraph()
    = default;

CIntensityGraph& CIntensityGraph::operator= (CIntensityGraph const& rhs)
{
    *((CSphericalGraph*) this) = (const CSphericalGraph) rhs;
    return *this;
}

// *****************************************************************
// setting
// *****************************************************************
void CIntensityGraph::reset()
{
    set(0.0f);
}

void CIntensityGraph::set(float fIntensity)
{
    TNodeList* pNodes = getNodes();

    for (auto& pNode : *pNodes)
    {
        ((CIntensityNode*) pNode)->setIntensity(fIntensity);
    }
}


// *****************************************************************
// file io
// *****************************************************************
bool CIntensityGraph::read(std::istream& infile)
{
    try
    {
        int nNumberNodes;

        // read number of nodes
        infile >> nNumberNodes;

        // read all nodes
        for (int n = 0 ; n < nNumberNodes ; n++)
        {
            readNode(infile);
        }

        // read number of edges
        int nNumberEdges;
        infile >> nNumberEdges;

        for (int e = 0 ; e < nNumberEdges ; e++)
        {
            readEdge(infile);
        }

    }
    catch (std::istream::failure&)
    {
        printf("ERROR: failed to write FeatureGraph to file\n");
        return false;
    }

    return true;
}

bool CIntensityGraph::readNode(std::istream& infile)
{
    CIntensityNode* pNewNode = getNewNode();

    int nIndex;
    TSphereCoord pos;
    float fIntensity;

    infile >> nIndex;
    infile >> pos.fPhi;
    infile >> pos.fTheta;
    infile >> fIntensity;

    pNewNode->setPosition(pos);
    pNewNode->setIntensity(fIntensity);

    int nTest = addNode(pNewNode);

    if (nTest != nIndex)
    {
        printf("Error input file inconsistent: %d %d\n", nTest, nIndex);
        return false;
    }

    return true;
}

bool CIntensityGraph::readEdge(std::istream& infile)
{
    int nIndex1, nIndex2, nLeftFace, nRightFace;

    infile >> nIndex1;
    infile >> nIndex2;

    infile >> nLeftFace;
    infile >> nRightFace;

    addEdge(nIndex1, nIndex2, nLeftFace, nRightFace);
    return true;
}


bool CIntensityGraph::write(std::ostream& outfile)
{
    try
    {
        int nNumberNodes = int(m_Nodes.size());
        // write number of nodes
        outfile << nNumberNodes << std::endl;

        // write all nodes
        for (int n = 0 ; n < nNumberNodes ; n++)
        {
            writeNode(outfile, n);
        }

        // write number of edges
        int nNumberEdges = int(m_Edges.size());
        outfile << nNumberEdges << std::endl;

        for (int e = 0 ; e < nNumberEdges ; e++)
        {
            writeEdge(outfile, e);
        }

    }
    catch (std::ostream::failure&)
    {
        printf("ERROR: failed to write FeatureGraph to file\n");
        return false;
    }

    return true;
}

bool CIntensityGraph::writeNode(std::ostream& outfile, int n)
{
    CIntensityNode* pCurrentNode = (CIntensityNode*) m_Nodes.at(n);

    outfile << pCurrentNode->getIndex() << std::endl;
    outfile << pCurrentNode->getPosition().fPhi << std::endl;
    outfile << pCurrentNode->getPosition().fTheta << std::endl;
    outfile << pCurrentNode->getIntensity() << std::endl;

    outfile << std::endl;
    return true;
}

bool CIntensityGraph::writeEdge(std::ostream& outfile, int e)
{
    CSGEdge* pCurrentEdge = m_Edges.at(e);
    outfile << pCurrentEdge->nIndex1 << " ";
    outfile << pCurrentEdge->nIndex2 << " ";

    outfile << pCurrentEdge->nLeftFace << " ";
    outfile << pCurrentEdge->nRightFace << " ";

    outfile << std::endl;
    return true;
}

void CIntensityGraph::graphToVec(std::vector<float>& vec)
{
    TNodeList* nodes = this->getNodes();

    for (auto& i : *nodes)
    {
        CIntensityNode* node = (CIntensityNode*) i;
        vec.push_back(node->getIntensity());
    }
}
