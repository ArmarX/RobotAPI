// *****************************************************************
// Filename:    MathTools.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        12.06.2007
// *****************************************************************

#pragma once

// *****************************************************************
// defines
// *****************************************************************
#include <math.h>
#ifndef M_PI
#define M_PI 3.141592653589793
#endif

// *****************************************************************
// includes
// *****************************************************************
#include "Structs.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

// *****************************************************************
// class MathTools
// *****************************************************************
class MathTools
{
public:
    // transformation of coordinates and paths
    static TTransform determinePathTransformation(TPath* p1, TPath* p2);
    static TPath* transformPath(TTransform transform, TPath* pPath);
    static TPath* inverseTransformPath(TTransform transform, TPath* pPath);

    static TTransform determineTransformation(TSphereCoord start1, TSphereCoord start2, TSphereCoord end1, TSphereCoord end2);
    static TSphereCoord transform(TSphereCoord sc, TTransform transform);
    static TSphereCoord inverseTransform(TSphereCoord sc, TTransform transform);

    static void determineTransformationAlphaBeta(TSphereCoord sc1, TSphereCoord sc2, float& fAlpha, float& fBeta, Eigen::Vector3d& betaAxis);
    static TSphereCoord transformAlphaBeta(TSphereCoord sc, float fAlpha, float fBeta, Eigen::Vector3d betaAxis);
    static TSphereCoord inverseTransformAlphaBeta(TSphereCoord sc, float fAlpha, float fBeta, Eigen::Vector3d betaAxis);

    // calculations on sphere coordimates
    static float CalculateCrossProductFromDifference(TSphereCoord p1, TSphereCoord p2, TSphereCoord p3);
    static float getDistanceOnArc(TSphereCoord p1, TSphereCoord p2);
    static float getDistance(TSphereCoord p1, TSphereCoord p2, float fRadius);

    // extended 2d math
    static bool IntersectLines(Eigen::Vector2d p1, Eigen::Vector2d m1, Eigen::Vector2d p2, Eigen::Vector2d m2, Eigen::Vector2d& res);

    // extended 3d math
    static Eigen::Vector3d dropAPerpendicular(Eigen::Vector3d point, Eigen::Vector3d linepoint, Eigen::Vector3d linevector);
    static float AngleAndDirection(Eigen::Vector3d vector1, Eigen::Vector3d vector2, Eigen::Vector3d normal);

    // coorrdinate conversion
    static void convert(TSphereCoord in, Eigen::Vector3d& out);
    static void convert(TSphereCoord in, Eigen::Vector3f& out);
    static void convert(Eigen::Vector3d in, TSphereCoord& out);
    static void convert(Eigen::Vector3f in, TSphereCoord& out);
    static void convertWithDistance(TSphereCoord in, Eigen::Vector3d& out);

    static double Angle(Eigen::Vector3d vec1, Eigen::Vector3d vec2);


};

