// *****************************************************************
// Filename:    GraphLookupTable.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        10.10.2008
// *****************************************************************

#pragma once

// *****************************************************************
// includes
// *****************************************************************
#include "SphericalGraph.h"
#include <list>

// *****************************************************************
// decleration of CGraphLookupTable
// *****************************************************************
class CGraphLookupTable
{
public:
    // construction / destruction
    CGraphLookupTable(int nMaxZenithBins, int nMaxAzimuthBins);
    ~CGraphLookupTable();

    // build table
    void buildLookupTable(CSphericalGraph* pGraph);

    // operation
    int getClosestNode(Eigen::Vector3d position);
    int getClosestNode(TSphereCoord position);
    int getClosestNode(Eigen::Vector3d position, bool& bExact);
    int getClosestNode(TSphereCoord position, bool& bExact);

private:
    void buildMemory();
    void cleanMemory();
    void addEntry(TSphereCoord position, int nIndex);
    void reset();

    int getNumberZenithBins();
    int getNumberAzimuthBins(float fZenith);
    int getAzimuthBinIndex(float fAzimuth, float fZenith);
    int getZenithBinIndex(float fZenith);

    void getBinMinMaxValues(int nZenithBinIndex, int nAzimuthBinIndex, float& fMinZenith, float& fMaxZenith, float& fMinAzimuth, float& fMaxAzimuth);

    int         m_nMaxZenithBins;
    int         m_nMaxAzimuthBins;

    std::list<int>**     m_ppTable;

    CSphericalGraph*    m_pGraph;
};


