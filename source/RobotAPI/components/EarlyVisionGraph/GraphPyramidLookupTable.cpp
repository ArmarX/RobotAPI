// *****************************************************************
// Filename:    GraphLookupTable.cpp
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        10.10.2008
// *****************************************************************

// *****************************************************************
// includes
// *****************************************************************
#include <cstdlib>
#include "GraphPyramidLookupTable.h"
#include "MathTools.h"

// *****************************************************************
// implementation of CGraphLookupTable
// *****************************************************************
// construction / destruction
CGraphPyramidLookupTable::CGraphPyramidLookupTable(int nMaxZenithBins, int nMaxAzimuthBins)
{
    m_nMaxZenithBins = nMaxZenithBins;
    m_nMaxAzimuthBins = nMaxAzimuthBins;

    m_nSubDivision = 2;
    m_nLevels = 0;

    CGraphLookupTable* pTable;

    while (nMaxZenithBins != 0)
    {
        pTable = new CGraphLookupTable(nMaxZenithBins, nMaxAzimuthBins);

        m_Tables.push_back(pTable);

        //      printf("Level %d: zenithbins: %d, azimuithbins: %d\n", m_nLevels, nMaxZenithBins, nMaxAzimuthBins);

        m_nLevels++;
        nMaxZenithBins /= m_nSubDivision;
        nMaxAzimuthBins /= m_nSubDivision;
    }

}

CGraphPyramidLookupTable::~CGraphPyramidLookupTable()
{
    std::list<CGraphLookupTable*>::iterator iter = m_Tables.begin();

    while (iter != m_Tables.end())
    {
        delete *iter;

        iter++;
    }
}

void CGraphPyramidLookupTable::buildLookupTable(CSphericalGraph* pGraph)
{
    std::list<CGraphLookupTable*>::iterator iter = m_Tables.begin();

    while (iter != m_Tables.end())
    {
        (*iter)->buildLookupTable(pGraph);

        iter++;
    }
}

int CGraphPyramidLookupTable::getClosestNode(Eigen::Vector3d position)
{
    TSphereCoord coords;
    MathTools::convert(position, coords);

    return getClosestNode(coords);
}

int CGraphPyramidLookupTable::getClosestNode(TSphereCoord position)
{
    bool bFinished = false;
    std::list<CGraphLookupTable*>::iterator iter = m_Tables.begin();
    int nIndex = -1;

    int L = 0;

    while (iter != m_Tables.end())
    {
        nIndex = (*iter)->getClosestNode(position, bFinished);
        //      printf("Result in level %d: index %d, finished: %d\n",L,nIndex,bFinished);

        if (bFinished)
        {
            break;
        }

        L++;
        iter++;
    }

    if (!bFinished)
    {
        printf("CGraphPyramidLookupTable:: no closest node found\n");
        exit(1);
    }

    return nIndex;
}

