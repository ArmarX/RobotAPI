// *****************************************************************
// Filename:    GraphPyramidLookupTable.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        10.10.2008
// *****************************************************************

#pragma once

// *****************************************************************
// includes
// *****************************************************************
#include "SphericalGraph.h"
#include "GraphLookupTable.h"
#include <list>

// *****************************************************************
// decleration of CGraphPyramidLookupTable
// *****************************************************************
class CGraphPyramidLookupTable
{
public:
    // construction / destruction
    CGraphPyramidLookupTable(int nMaxZenithBins, int nMaxAzimuthBins);
    ~CGraphPyramidLookupTable();

    // build table
    void buildLookupTable(CSphericalGraph* pGraph);

    // operation
    int getClosestNode(Eigen::Vector3d position);
    int getClosestNode(TSphereCoord position);

private:
    std::list<CGraphLookupTable*> m_Tables;
    int         m_nSubDivision;
    int         m_nLevels;

    int         m_nMaxZenithBins;
    int         m_nMaxAzimuthBins;
};


