// *****************************************************************
// Filename:    AspectGraph.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        12.06.2007
// *****************************************************************

#pragma once

// *****************************************************************
// includes
// *****************************************************************
// stl includes
#include <vector>
#include <istream>
#include <ostream>

// local includes
#include "Structs.h"

// *****************************************************************
// defines
// *****************************************************************
#define MAX_NODES 100000

// *****************************************************************
// nodes and edges
// *****************************************************************
class CSGEdge
{
public:
    // indices of nodes
    int             nIndex1;
    int             nIndex2;

    // indices of faces (needed in triangulation
    int             nLeftFace;
    int             nRightFace;
};

class CSGNode
{
public:
    CSGNode() {}
    CSGNode(TSphereCoord position)
    {
        m_Position = position;
    }
    virtual ~CSGNode() {}

    TSphereCoord getPosition()
    {
        return m_Position;
    }
    void setPosition(TSphereCoord position)
    {
        m_Position = position;
    }

    int getIndex()
    {
        return m_nIndex;
    }
    void setIndex(int nIndex)
    {
        m_nIndex = nIndex;
    }

    virtual CSGNode* clone()
    {
        CSGNode* copy = new CSGNode(m_Position);
        copy->setIndex(m_nIndex);
        return copy;
    }

protected:
    TSphereCoord    m_Position;
    int             m_nIndex;

};

// *****************************************************************
// types
// *****************************************************************
using TEdgeList = std::vector<CSGEdge*>;
using TNodeList = std::vector<CSGNode*>;

// *****************************************************************
// definition of class CSphericalGraph
// *****************************************************************
class CSphericalGraph
{
public:
    ///////////////////////////////
    // construction / destruction
    ///////////////////////////////
    CSphericalGraph();
    CSphericalGraph(const CSphericalGraph& prototype);

    virtual ~CSphericalGraph();
    virtual CSGNode* getNewNode()
    {
        return new CSGNode();
    }

    ///////////////////////////////
    // aspect graph manipulation
    ///////////////////////////////
    // clear graph
    void clear();

    // add nodes
    int addNode(CSGNode* pNode);

    // add edges
    int addEdge(int nIndex1, int nIndex2);
    int addEdge(int nIndex1, int nIndex2, int nLeftFace, int nRightFace);

    // node adjacency (usually only required by addEdge)
    void addNodeAdjacency(int nNode, int nAdjacency);

    ///////////////////////////////
    // member access
    ///////////////////////////////
    TEdgeList*          getEdges();
    TNodeList*          getNodes();

    CSGEdge*            getEdge(int nEdgeIndex);
    CSGNode*            getNode(int nIndex);
    std::vector<int>*   getNodeAdjacency(int nIndex);

    ///////////////////////////////
    // file io
    ///////////////////////////////
    virtual bool read(std::istream& infile);
    virtual bool readNode(std::istream& infile);
    virtual bool readEdge(std::istream& infile);

    virtual bool write(std::ostream& outfile);
    virtual bool writeNode(std::ostream& outfile, int n);
    virtual bool writeEdge(std::ostream& outfile, int e);



    ///////////////////////////////
    // operators
    ///////////////////////////////
    CSphericalGraph& operator= (CSphericalGraph const& rhs);

protected:
    TEdgeList           m_Edges;
    TNodeList           m_Nodes;
    std::vector<int>    m_NodeAdjacency[MAX_NODES];

};

