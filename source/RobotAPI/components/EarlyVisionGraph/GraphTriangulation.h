// *****************************************************************
// Filename:    AspectGraphProcessor.h
// Copyright:   Kai Welke, Chair Prof. Dillmann (IAIM),
//              Institute for Computer Science and Engineering (CSE),
//              University of Karlsruhe. All rights reserved.
// Author:      Kai Welke
// Date:        12.06.2007
// *****************************************************************

#pragma once

// *****************************************************************
// forward declarations
// *****************************************************************

// *****************************************************************
// includes
// *****************************************************************
#include "SphericalGraph.h"
#include "Base/DataStructures/Graph/Convexhull/C3DNode.h"
#include "Base/DataStructures/Graph/Convexhull/HalfSpace.h"
#include "Base/DataStructures/Graph/Convexhull/CHGiftWrap.h"
#include <float.h>

// *****************************************************************
// namespaces
// *****************************************************************

// *****************************************************************
// namespace GraphTriangulation
// *****************************************************************
class GraphTriangulation
{
public:
    static void delaunayTriangulationQuadratic(CSphericalGraph* pGraph);
    // triangulate with O(n^2) spherical
    static void triangulationQuadraticSpherical(CSphericalGraph* pGraph);
    // triangulate with O(n^4)
    static void triangulationQuartic(CSphericalGraph* pGraph, float fThreshold = FLT_MAX);
    // triangulate with O(n^2)
    static void triangulationQuadratic(CSphericalGraph* pGraph);

private:
    static void updateLeftFace(CSphericalGraph* pGraph, int nEdgeIndex, int nIndex1, int nIndex2, int nFace);
    static void completeFacet(CSphericalGraph* pGraph, int nEdgeIndex, int nFaces);

    static CSphericalGraph* doubleNodes(CSphericalGraph* pSourceGraph);

};

