/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ObjectMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "RobotAPI/libraries/armem/client/plugins/ReaderWriterPlugin.h"
#include "RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h"
#include <RobotAPI/libraries/armem_objects/server/class/Segment.h>
#include <RobotAPI/libraries/armem_objects/server/instance/SegmentAdapter.h>
#include <RobotAPI/libraries/armem_objects/server/attachments/Segment.h>

#include <RobotAPI/libraries/armem/server/plugins/ReadWritePluginUser.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>

#include <RobotAPI/interface/armem/server/ObjectMemoryInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <memory>


#define ICE_CURRENT_ARG const Ice::Current& = Ice::emptyCurrent


namespace armarx::armem::server::obj
{

    /**
     * @defgroup Component-ObjectMemory ObjectMemory
     * @ingroup RobotAPI-Components
     * A description of the component ObjectMemory.
     *
     * @class ObjectMemory
     * @ingroup Component-ObjectMemory
     * @brief Brief description of class ObjectMemory.
     *
     * Detailed description of class ObjectMemory.
     */
    class ObjectMemory :
        virtual public Component

        , virtual public armarx::armem::server::ObjectMemoryInterface
        , virtual public armarx::armem::server::ReadWritePluginUser
        , virtual public armarx::armem::server::obj::instance::SegmentAdapter
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        , virtual public armarx::ArVizComponentPluginUser
    {
    public:

        static const std::string defaultMemoryName;


    public:

        ObjectMemory();
        virtual ~ObjectMemory() override;


        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    public:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;

        void onDisconnectComponent() override;
        void onExitComponent() override;

        // Without this, ObjectMemory draws the original
        // methods from ObjectMemoryInterface and complains
        // that an overload is being hidden.
        using ReadWritePluginUser::executeActions;
        using ReadWritePluginUser::getActions;
        using ReadWritePluginUser::getAvailableEngines;
        using ReadWritePluginUser::predict;

        // Actions
        armem::actions::GetActionsOutputSeq
        getActions(const armem::actions::GetActionsInputSeq& inputs) override;
        armem::actions::ExecuteActionOutputSeq
        executeActions(const armem::actions::ExecuteActionInputSeq& inputs) override;

        // Predictions
        armem::prediction::data::PredictionResultSeq
        predict(const armem::prediction::data::PredictionRequestSeq& requests) override;

        // Remote GUI
        void createRemoteGuiTab();
        void RemoteGui_update() override;


    private:

        DebugObserverInterfacePrx debugObserver;
        KinematicUnitObserverInterfacePrx kinematicUnitObserver;

        double predictionTimeWindow = 2;

        clazz::Segment classSegment;

        attachments::Segment attachmentSegment;

        // associations::Segment associationsSegment;

        struct RemoteGuiTab : armarx::RemoteGui::Client::Tab
        {
            instance::SegmentAdapter::RemoteGui instance;
            clazz::Segment::RemoteGui clazz;
        };
        std::unique_ptr<RemoteGuiTab> tab;

        armem::client::plugins::ReaderWriterPlugin<robot_state::VirtualRobotReader>* virtualRobotReaderPlugin;
    };

}

#undef ICE_CURRENT_ARG
