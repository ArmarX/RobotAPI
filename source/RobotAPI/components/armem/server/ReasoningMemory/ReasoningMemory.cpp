/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ExampleMemory
 * @author     Markus Grotz ( markus dot grotz at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ReasoningMemory.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/error.h>


namespace armarx::armem::server
{
    armarx::PropertyDefinitionsPtr ReasoningMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        setMemoryName("Reasoning");
        return defs;
    }


    ReasoningMemory::ReasoningMemory() :
        anticipationSegment(iceAdapter())
    {

    }

    std::string ReasoningMemory::getDefaultName() const
    {
        return "ReasoningMemory";
    }

    void ReasoningMemory::onInitComponent()
    {
	
	{
        wm::CoreSegment& cs = workingMemory().addCoreSegment("Anticipation", armarx::reasoning::arondto::Anticipation::ToAronType());
		cs.setMaxHistorySize(20);
	}


    }

    void ReasoningMemory::onConnectComponent()
    {
    }

    void ReasoningMemory::onDisconnectComponent()
    {
    }

    void ReasoningMemory::onExitComponent()
    {
    }
}
