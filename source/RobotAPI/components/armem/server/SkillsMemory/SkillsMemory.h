/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::SkillsMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// Base Class
#include <ArmarXCore/core/Component.h>

// ArmarX
#include <ArmarXCore/interface/core/Profiler.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>

#include <RobotAPI/interface/skills/SkillMemoryInterface.h>

#include <RobotAPI/libraries/skills/manager/SkillManagerComponentPlugin.h>
#include <RobotAPI/libraries/armem_skills/server/StatechartListenerComponentPlugin.h>
#include <RobotAPI/libraries/armem/server/plugins/ReadWritePluginUser.h>

#include <RobotAPI/libraries/armem_skills/aron/Statechart.aron.generated.h>
#include <RobotAPI/libraries/armem_skills/server/segment/StatechartListenerSegment.h>
#include <RobotAPI/libraries/armem_skills/server/segment/ExecutableSkillLibrarySegment.h>
#include <RobotAPI/libraries/armem_skills/server/segment/SkillExecutionRequestSegment.h>
#include <RobotAPI/libraries/armem_skills/server/segment/SkillEventSegment.h>

namespace armarx
{
    /**
     * @defgroup Component-SkillsMemory SkillsMemory
     * @ingroup RobotAPI-Components
     * A description of the component SkillsMemory.
     *
     * @class SkillsMemory
     * @ingroup Component-SkillsMemory
     * @brief Brief description of class SkillsMemory.
     *
     * Detailed description of class SkillsMemory.
     */
    class SkillsMemory :
        virtual public armarx::Component,
        virtual public skills::dti::SkillMemoryInterface,

        virtual public armem::server::ReadWritePluginUser,
        virtual public StatechartListenerComponentPluginUser,
        virtual public SkillManagerComponentPluginUser
    {
    public:

        SkillsMemory();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        // Override StatechartListener
        void reportStatechartTransitionWithParameters(const ProfilerStatechartTransitionWithParameters&, const Ice::Current&) override;
        void reportStatechartTransitionWithParametersList(const ProfilerStatechartTransitionWithParametersList&, const Ice::Current&) override;

        // Override SkillManager to add memory functions
        void addProvider(const skills::manager::dto::ProviderInfo& info, const Ice::Current &current) override;
        void removeProvider(const std::string&, const Ice::Current &current) override;
        skills::provider::dto::SkillStatusUpdate executeSkill(const skills::manager::dto::SkillExecutionRequest& info, const Ice::Current &current) override;
        void updateStatusForSkill(const skills::provider::dto::SkillStatusUpdate& statusUpdate, const Ice::Current &current) override;

        // WritingInterface interface
        armem::data::CommitResult commit(const armem::data::Commit& commit, const Ice::Current&) override;

    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        static constexpr const char* MemoryName = "Skill";

        struct Properties
        {
        };
        Properties p;

        skills::segment::StatechartListenerProviderSegment statechartListenerProviderSegment;
        skills::segment::ExecutableSkillLibraryCoreSegment executableSkillCoreSegment;
        skills::segment::SkillEventCoreSegment skillEventCoreSegment;
        skills::segment::SkillExecutionRequestCoreSegment skillExecutionRequestCoreSegment;
    };
}
