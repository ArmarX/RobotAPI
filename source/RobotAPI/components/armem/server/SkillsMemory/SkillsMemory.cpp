/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::SkillsMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SkillsMemory.h"

#include <SimoxUtility/algorithm/string.h>

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem_skills/aron_conversions.h>
#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>

namespace armarx
{

    SkillsMemory::SkillsMemory():
        ReadWritePluginUser(),
        StatechartListenerComponentPluginUser(),
        SkillManagerComponentPluginUser(),
        statechartListenerProviderSegment(iceAdapter()),
        executableSkillCoreSegment(iceAdapter()),
        skillEventCoreSegment(iceAdapter()),
        skillExecutionRequestCoreSegment(iceAdapter())
    {
    }


    armarx::PropertyDefinitionsPtr SkillsMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        const std::string prefix = "mem.";
        statechartListenerProviderSegment.defineProperties(defs, prefix + "statechartlistener.");
        executableSkillCoreSegment.defineProperties(defs, prefix + "executableskill.");
        skillExecutionRequestCoreSegment.defineProperties(defs, prefix + "executionrequest.");

        setMemoryName(MemoryName);
        return defs;
    }


    std::string SkillsMemory::getDefaultName() const
    {
        return "SkillMemory";
    }


    void SkillsMemory::onInitComponent()
    {
        statechartListenerProviderSegment.init();
        executableSkillCoreSegment.init();
        skillExecutionRequestCoreSegment.init();
        skillEventCoreSegment.init();
    }


    void SkillsMemory::onConnectComponent()
    {
    }


    void SkillsMemory::onDisconnectComponent()
    {

    }


    void SkillsMemory::onExitComponent()
    {
    }


    armem::data::CommitResult SkillsMemory::commit(const armem::data::Commit& commit, const Ice::Current& current)
    {
        // This function is overloaded to check for skill executions
        // First pass arg to parent func to ensure that data is written into memory and that clients are notified
        armem::data::CommitResult result = ReadWritePluginUser::commit(commit);

        skills::callback::dti::SkillProviderCallbackInterfacePrx myPrx;
        getProxy(myPrx, -1);

        for (const auto& up : commit.updates)
        {
            if (up.entityID.coreSegmentName == skills::segment::SkillExecutionRequestCoreSegment::CoreSegmentName)
            {
                for (const auto& instance : up.instancesData)
                {
                    ARMARX_CHECK_NOT_NULL(instance);

                    skills::manager::dto::SkillExecutionRequest exInfo = skillExecutionRequestCoreSegment.convertCommit(instance);
                    SkillManagerComponentPluginUser::executeSkill(exInfo, current);
                }
            }
        }

        return result;
    }

    void SkillsMemory::addProvider(const skills::manager::dto::ProviderInfo& info, const Ice::Current &current)
    {
        SkillManagerComponentPluginUser::addProvider(info, current);

        // log skills to memory
        executableSkillCoreSegment.addSkillProvider(info);
    }

    void SkillsMemory::removeProvider(const std::string& skillProviderName, const Ice::Current &current)
    {
        executableSkillCoreSegment.removeSkillProvider(skillProviderName);

        // remove skills from memory
        SkillManagerComponentPluginUser::removeProvider(skillProviderName, current);
    }

    skills::provider::dto::SkillStatusUpdate SkillsMemory::executeSkill(const skills::manager::dto::SkillExecutionRequest& info, const Ice::Current &current)
    {
        skills::manager::dto::SkillExecutionRequest requestCopy = info;
        if (requestCopy.skillId.providerName == "*")
        {
            // sanitize the provider name if set to 'any'
            requestCopy.skillId.providerName = getFirstProviderNameThatHasSkill(requestCopy.skillId.skillName);
        }

        skillExecutionRequestCoreSegment.addSkillExecutionRequest(requestCopy);
        return SkillManagerComponentPluginUser::executeSkill(requestCopy, current);
    }

    void SkillsMemory::updateStatusForSkill(const skills::provider::dto::SkillStatusUpdate& update, const Ice::Current &current)
    {
        skillEventCoreSegment.addSkillUpdateEvent(update);
    }



    /*
     * Statechart stuff
     */
    void SkillsMemory::reportStatechartTransitionWithParameters(const ProfilerStatechartTransitionWithParameters& x, const Ice::Current&)
    {
        statechartListenerProviderSegment.reportStatechartTransitionWithParameters(x);
    }

    void SkillsMemory::reportStatechartTransitionWithParametersList(const ProfilerStatechartTransitionWithParametersList& x, const Ice::Current&)
    {
        statechartListenerProviderSegment.reportStatechartTransitionWithParametersList(x);
    }
}
