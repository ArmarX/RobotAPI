/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ExampleMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MotionMemory.h"


namespace armarx
{
    armarx::PropertyDefinitionsPtr MotionMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        setMemoryName("Motion");

        const std::string prefix = "mem.";
        mdbMotions.defineProperties(defs, prefix + "mdbmotions.");
        motionPrimitive.defineProperties(defs, prefix + "trajs.");
        return defs;
    }


    MotionMemory::MotionMemory() :
        mdbMotions(iceAdapter()),
        motionPrimitive(iceAdapter())
    {
    }


    std::string MotionMemory::getDefaultName() const
    {
        return "MotionMemory";
    }


    void MotionMemory::onInitComponent()
    {
        mdbMotions.init();
        motionPrimitive.init();
    }


    void MotionMemory::onConnectComponent()
    {
        mdbMotions.onConnect();
        motionPrimitive.onConnect();
    }


    void MotionMemory::onDisconnectComponent()
    {
    }


    void MotionMemory::onExitComponent()
    {
    }

}
