/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ExampleMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SubjectMemory.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/armem/core/error.h>


namespace armarx
{
    armarx::PropertyDefinitionsPtr SubjectMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        const std::string prefix = "mem.";
        defs->optional(memoryName, prefix + "MemoryName", "Name of this memory (server).");
        mdbMotions.defineProperties(defs, prefix + "mdb.");
        return defs;
    }

    SubjectMemory::SubjectMemory() :
        mdbMotions(armem::server::ComponentPluginUser::iceMemory, armem::server::ComponentPluginUser::workingMemoryMutex)
    {

    }


    std::string SubjectMemory::getDefaultName() const
    {
        return "MotionMemory";
    }

    void SubjectMemory::onInitComponent()
    {
        workingMemory.name() = memoryName;
        longtermMemory.name() = memoryName;
    }

    void SubjectMemory::onConnectComponent()
    {
    }

    void SubjectMemory::onDisconnectComponent()
    {
    }

    void SubjectMemory::onExitComponent()
    {
    }
}
