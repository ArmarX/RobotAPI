/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ExampleMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <RobotAPI/libraries/armem/server/plugins/ReadWritePluginUser.h>

#include <RobotAPI/libraries/armem_subjects/server/MotionDatabase/Segment.h>

namespace armarx
{
    /**
     * @defgroup Component-ExampleMemory ExampleMemory
     * @ingroup RobotAPI-Components
     * A description of the component ExampleMemory.
     *
     * @class ExampleMemory
     * @ingroup Component-ExampleMemory
     * @brief Brief description of class ExampleMemory.
     *
     * Detailed description of class ExampleMemory.
     */
    class SubjectMemory :
        virtual public armarx::Component
        , virtual public armem::server::ReadWritePluginUser
    {
    public:

        SubjectMemory();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


    private:
        std::string memoryName = "Subject";

        armem::server::subjects::mdb::Segment mdbSubjects;

    };
}
