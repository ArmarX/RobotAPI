/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ExampleMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExampleMemory.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>

#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/Prediction.h>
#include <RobotAPI/libraries/armem/core/ice_conversions.h>
#include <RobotAPI/libraries/armem/server/MemoryRemoteGui.h>

#include <RobotAPI/components/armem/server/ExampleMemory/aron/ExampleData.aron.generated.h>


namespace armarx
{
    armarx::PropertyDefinitionsPtr ExampleMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        defs->topic(debugObserver);

        setMemoryName("Example");

        p.core._defaultSegmentsStr = simox::alg::join(p.core.defaultCoreSegments, ", ");
        defs->optional(p.core._defaultSegmentsStr, "core.DefaultSegments",
                       "Core segments to add on start up (just as example).");
        defs->optional(p.core.addOnUsage, "core.AddOnUsage",
                       "If enabled, core segments are added when required by a new provider segment."
                       "This will usually be off for most memory servers.");

        defs->optional(p.enableRemoteGui, "p.enableRemoteGui",
                       "If true, the memory cotent is shown in the remote gui."
                       "Can be very slow for high-frequency updates!");
        return defs;
    }


    std::string ExampleMemory::getDefaultName() const
    {
        return "ExampleMemory";
    }


    void ExampleMemory::onInitComponent()
    {
        // Usually, the memory server will specify a number of core segments with a specific aron type.
        workingMemory().addCoreSegment("ExampleData", armem::example::ExampleData::ToAronType());
        workingMemory().addCoreSegment("LinkedData", armem::example::LinkedData::ToAronType());

        // We support the "Latest" prediction engine for the entire memory.
        workingMemory().addPredictor(
                    armem::PredictionEngine{.engineID = "Latest"},
                    [this](const armem::PredictionRequest& request)
                    { return this->predictLatest(request); });

        // For illustration purposes, we add more segments (without types).
        bool trim = true;
        p.core.defaultCoreSegments = simox::alg::split(p.core._defaultSegmentsStr, ",", trim);
        p.core._defaultSegmentsStr.clear();

        for (const std::string& name : p.core.defaultCoreSegments)
        {
            auto& c = workingMemory().addCoreSegment(name);
            c.setMaxHistorySize(100);
        }
    }

    void ExampleMemory::onConnectComponent()
    {
        if (p.enableRemoteGui)
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
    }

    void ExampleMemory::onDisconnectComponent()
    {
    }

    void ExampleMemory::onExitComponent()
    {
    }



    // WRITING

    armem::data::AddSegmentsResult ExampleMemory::addSegments(const armem::data::AddSegmentsInput& input, const Ice::Current&)
    {
        // This function is overloaded to trigger the remote gui rebuild.
        armem::data::AddSegmentsResult result = ReadWritePluginUser::addSegments(input, p.core.addOnUsage);
        tab.rebuild = true;
        return result;
    }


    armem::data::CommitResult ExampleMemory::commit(const armem::data::Commit& commit, const Ice::Current&)
    {
        // This function is overloaded to trigger the remote gui rebuild.
        armem::data::CommitResult result = ReadWritePluginUser::commit(commit);
        tab.rebuild = true;
        return result;
    }


    // READING

    // Inherited from Plugin



    // ACTIONS
    armem::actions::GetActionsOutputSeq
    ExampleMemory::getActions(
            const armem::actions::GetActionsInputSeq& input)
    {
        using namespace armem::actions;
        Action greeting{"hi", "Say hello to " + input[0].id.entityName};
        Action failure{"fail", "Fail dramatically"};
        Action nothing{"null", "Do nothing, but deeply nested"};

        SubMenu one{"one", "One", { nothing } };
        SubMenu two{"two", "Two", { one } };
        SubMenu three{"three", "Three", { two } };
        SubMenu four{"four", "Four", { three } };

        Menu menu {
                greeting,
                failure,
                four,
                SubMenu{"mut", "Mutate", {
                    Action{"copy", "Copy latest instance"}
                }}
        };
        
        return {{ menu.toIce() }};
    }

    armem::actions::ExecuteActionOutputSeq ExampleMemory::executeActions(
            const armem::actions::ExecuteActionInputSeq& input)
    {
        using namespace armem::actions;

        ExecuteActionOutputSeq output;
        for (const auto& [id, path] : input)
        {
            const armem::MemoryID memoryID = armarx::fromIce<armem::MemoryID>(id);
            if (path == ActionPath{"hi"})
            {
                ARMARX_INFO << "Hello, " << memoryID.str() << "!";
                output.emplace_back(true, "");
            }
            else if (path == ActionPath{"fail"})
            {
                ARMARX_WARNING << "Alas, I am gravely wounded!";
                output.emplace_back(false, "Why would you do that to him?");
            }
            else if (not path.empty() and path.front() == "four" and path.back() == "null")
            {
                // Do nothing.
                ARMARX_INFO << "Nested action (path: " << path << ")";
                output.emplace_back(true, "");
            }
            else if (path == ActionPath{"mut", "copy"})
            {
                auto* instance = workingMemory().findLatestInstance(memoryID);
                if (instance != nullptr)
                {
                    armem::EntityUpdate update;
                    armem::MemoryID newID = memoryID.getCoreSegmentID()
                        .withProviderSegmentName(memoryID.providerSegmentName)
                        .withEntityName(memoryID.entityName);
                    update.entityID = newID;
                    update.timeCreated = armem::Time::Now();
                    update.instancesData = { instance->data() };

                    armem::Commit newCommit;
                    newCommit.add(update);
                    this->commit(armarx::toIce<armem::data::Commit>(newCommit));

                    tab.rebuild = true;
                    ARMARX_INFO << "Duplicated " << memoryID;
                    output.emplace_back(true, "");
                }
                else
                {
                    output.emplace_back(false, "Couldn't duplicate " + memoryID.str());
                }
            }
        }

        return output;
    }

    // PREDICTING
    armem::PredictionResult
    ExampleMemory::predictLatest(const armem::PredictionRequest& request)
    {
        armem::PredictionResult result;
        auto memID = request.snapshotID;
        result.snapshotID = memID;

        armem::client::QueryBuilder builder;
        builder.latestEntitySnapshot(memID);
        auto queryResult =
            query(armarx::toIce<armem::query::data::Input>(builder.buildQueryInput()));
        if (queryResult.success)
        {
            auto readMemory = fromIce<armem::wm::Memory>(queryResult.memory);
            auto* latest = readMemory.findLatestSnapshot(memID);
            if (latest != nullptr)
            {
                auto instance = memID.hasInstanceIndex()
                                    ? latest->getInstance(memID)
                                    : latest->getInstance(latest->getInstanceIndices().at(0));
                result.success = true;
                result.prediction = instance.data();
            }
            else
            {
                result.success = false;
                result.errorMessage =
                    "Could not find entity referenced by MemoryID '" + memID.str() + "'.";
            }
        }
        else
        {
            result.success = false;
            result.errorMessage =
                "Could not find entity referenced by MemoryID '" + memID.str() + "'.";
        }

        return result;
    }

    // REMOTE GUI

    void ExampleMemory::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        {
            // Core segments are locked by MemoryRemoteGui.
            tab.memoryGroup = armem::server::MemoryRemoteGui().makeGroupBox(workingMemory());
        }

        VBoxLayout root = {tab.memoryGroup, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void ExampleMemory::RemoteGui_update()
    {
        if (tab.rebuild.exchange(false))
        {
            createRemoteGuiTab();
        }
    }

}
