
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore ExampleMemory)
 
armarx_add_test(ExampleMemoryTest ExampleMemoryTest.cpp "${LIBS}")
