/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ExampleMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXObjects::ExampleMemory

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include "../ExampleMemory.h"

#include <RobotAPI/components/armem/server/ExampleMemory/aron/ExampleData.aron.generated.h>
#include <RobotAPI/libraries/armem/core.h>

#include <iostream>


using armarx::armem::example::ExampleData;
namespace armem = armarx::armem;


BOOST_AUTO_TEST_CASE(test_ExampleData_type)
{
    armarx::aron::type::ObjectPtr type = ExampleData::ToAronType();

    BOOST_CHECK_EQUAL(type->childrenSize(), 21);

    armem::wm::Memory memory;
    armem::wm::CoreSegment& core = memory.addCoreSegment("ExampleData", type);
    armem::wm::ProviderSegment& prov = core.addProviderSegment("Provider");

    BOOST_CHECK_EQUAL(core.aronType(), prov.aronType());
}
