
#include "GraspMemory.h"

#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/server/MemoryRemoteGui.h>
#include <RobotAPI/libraries/armem/core/ice_conversions.h>

#include <RobotAPI/libraries/GraspingUtility/aron/GraspCandidate.aron.generated.h>
#include <RobotAPI/libraries/GraspingUtility/GraspCandidateVisu.h>
#include <RobotAPI/libraries/GraspingUtility/aron_conversions.h>
#include <RobotAPI/libraries/armem_grasping/aron/KnownGraspCandidate.aron.generated.h>


namespace armarx::armem::server::grasp
{
    armarx::PropertyDefinitionsPtr GraspMemory::createPropertyDefinitions()
    {
        setMemoryName("Grasp");

        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        defs->topic(debugObserver);

        defs->optional(enableRemoteGui, "remoteGui.enable", "Enable/Disable Remote GUI");
        defs->optional(gui.trackNewEntities, "EnableTrackingOfNewEntities", "Enable/Disable the automatic visual tracking of newly commited Entities");
        return defs;
    }


    std::string GraspMemory::getDefaultName() const
    {
        return "GraspMemory";
    }

    GraspMemory::GraspMemory() :
        knownGraspProviderSegment(iceAdapter())
    {
    }

    void GraspMemory::onInitComponent()
    {
        workingMemory().addCoreSegment("GraspCandidate",
                                     armarx::grasping::arondto::GraspCandidate::ToAronType());
        workingMemory().addCoreSegment("BimanualGraspCandidate",
                                     armarx::grasping::arondto::BimanualGraspCandidate::ToAronType());
        workingMemory().addCoreSegment("KnownGraspCandidate",
                                     armarx::armem::grasping::arondto::KnownGraspInfo::ToAronType());

        knownGraspProviderSegment.init();
    }

    void GraspMemory::onConnectComponent()
    {
        if (enableRemoteGui)
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
    }

    void GraspMemory::onDisconnectComponent()
    {
    }

    void GraspMemory::onExitComponent()
    {
    }



    // WRITING

    armem::data::CommitResult GraspMemory::commit(const armem::data::Commit& commit, const Ice::Current&)
    {
        std::vector<armem::MemoryID> trackedEntityIds;
        {
            std::unique_lock lock(gui.visualizationMutex);
            trackedEntityIds = gui.trackedEntityIds;
        }

        if(! trackedEntityIds.empty())
        {
            for (auto &update : commit.updates)
            {
                armem::MemoryID entityID = armarx::fromIce<armem::MemoryID>(update.entityID);
                entityID.clearTimestamp();
                if(std::find(trackedEntityIds.begin(), trackedEntityIds.end(), entityID) != trackedEntityIds.end())
                {
                    workingMemory().getEntity(entityID).getLatestSnapshot().forEachInstance([this](const auto & instance)
                    {
                        removeInstanceFromVisu(instance.id());

                    });
                }

            }
        }

        if(gui.trackNewEntities)
        {
            for (auto &update : commit.updates)
            {
                armem::MemoryID entityID = armarx::fromIce<armem::MemoryID>(update.entityID);
                entityID.clearTimestamp();
                if(!workingMemory().findEntity(entityID))
                {
                    std::unique_lock lock(gui.visualizationMutex);
                    gui.trackedEntityIds.emplace_back(entityID);
                    trackedEntityIds.emplace_back(entityID);
                }
            }
        }

        // This function is overloaded to trigger the remote gui rebuild.
        armem::data::CommitResult result = ReadWritePluginUser::commit(commit);
        gui.tab.rebuild = true;


        if (! trackedEntityIds.empty())
        {
            for (auto &update : commit.updates)
            {
                armem::MemoryID entityID = armarx::fromIce<armem::MemoryID>(update.entityID);
                entityID.clearTimestamp();
                if(std::find(trackedEntityIds.begin(), trackedEntityIds.end(), entityID) != trackedEntityIds.end())
                {
                    workingMemory().getEntity(entityID).getLatestSnapshot().forEachInstance([this](const auto & instance)
                    {
                        addInstanceToVisu(instance.id());

                    });
                }

            }
        }
        return result;
    }

    // READING

    // Inherited from Plugin

    armem::actions::data::GetActionsOutputSeq
    GraspMemory::getActions(
            const armem::actions::data::GetActionsInputSeq &inputs)
    {
        using namespace armem::actions;

        GetActionsOutputSeq outputs;
        for (const auto& input : inputs)
        {
            auto memoryID = armarx::fromIce<armem::MemoryID>(input.id);
            if (armem::contains(armem::MemoryID("Grasp"), memoryID) and not memoryID.hasGap())
            {
                if(armem::contains(armem::MemoryID("Grasp", "BimanualGraspCandidate"), memoryID)
                        || armem::contains(armem::MemoryID("Grasp", "KnownGraspCandidate"), memoryID))
                {
                    continue;  // Not supported, ignore.
                }

                std::vector<MenuEntry> actions;

                if ( !memoryID.hasTimestamp() && !memoryID.hasInstanceIndex())
                {
                    if(memoryID.hasEntityName())
                    {
                        if(std::find(gui.trackedEntityIds.begin(), gui.trackedEntityIds.end(), memoryID) == gui.trackedEntityIds.end())
                        {
                            actions.push_back(Action{"track","Track this entity in arviz"});
                        }
                        else
                        {
                            actions.push_back(Action{"untrack","Stop tracking this entity in arviz"});
                        }

                    }
                    else
                    {
                        actions.push_back(Action{"track","Track all underlying entities in arviz"});
                        actions.push_back(Action{"untrack","Stop tracking all underlying entities in arviz"});
                    }
                }

                actions.push_back(Action{"vis", "Visualize all contained grasp candidates"});
                actions.push_back(Action{"rem", "Remove all contained grasp candidates from visualization"});
                actions.push_back(SubMenu{"high", "Highlight all contain grasp candidates", {
                                              Action{"pink", "in pink"},
                                              Action{"red", "in red"},
                                              Action{"blue", "in blue"},
                                              Action{"yellow", "in yellow"},
                                              Action{"purple", "in purple"}
                                          }});
                actions.push_back(Action{"reset", "Reset highlight layer"});

                Menu menu{actions};
                outputs.push_back({ menu.toIce() });

            }
        }

        return outputs;
    }

    armem::actions::data::ExecuteActionOutputSeq
    GraspMemory::executeActions(
            const armem::actions::data::ExecuteActionInputSeq &inputs)
    {
        using namespace armem::actions;
        ExecuteActionOutputSeq outputs;

        for (const auto& input : inputs)
        {
            auto memoryID = armarx::fromIce<armem::MemoryID>(input.id);
            if (input.actionPath == ActionPath{"vis"} || input.actionPath == ActionPath{"rem"})
            {
                if (armem::contains(armem::MemoryID("Grasp"), memoryID) and not memoryID.hasGap())
                {
                    {
                        if(armem::contains(armem::MemoryID("Grasp", "BimanualGraspCandidate"), memoryID)
                                || armem::contains(armem::MemoryID("Grasp", "KnownGraspCandidate"), memoryID))
                        {
                            std::stringstream sstream;
                            sstream << "Currently visualization for CoreSegment " << memoryID.coreSegmentName << " is not yet supported";
                            outputs.emplace_back(false, sstream.str());
                        }
                        else
                        {
                            if (memoryID.hasInstanceIndex())
                            {
                                if (input.actionPath == ActionPath{"vis"})
                                {
                                    addInstanceToVisu(memoryID);
                                }
                                else if (input.actionPath == ActionPath{"rem"})
                                {
                                    removeInstanceFromVisu(memoryID);
                                }

                            }
                            else if (memoryID.hasTimestamp())
                            {
                                workingMemory().getSnapshot(memoryID).forEachInstance([this, &input](const auto & instance)
                                {
                                    if (input.actionPath == ActionPath{"vis"})
                                    {
                                        addInstanceToVisu(instance.id());
                                    }
                                    else if (input.actionPath == ActionPath{"rem"})
                                    {
                                        removeInstanceFromVisu(instance.id());
                                    }
                                });
                            }
                            else if (memoryID.hasEntityName())
                            {
                                workingMemory().getEntity(memoryID).forEachInstance([this, &input](const auto & instance)
                                {
                                    if (input.actionPath == ActionPath{"vis"})
                                    {
                                        addInstanceToVisu(instance.id());
                                    }
                                    else if (input.actionPath == ActionPath{"rem"})
                                    {
                                        removeInstanceFromVisu(instance.id());
                                    }
                                });
                            }
                            else if (memoryID.hasProviderSegmentName())
                            {
                                workingMemory().getProviderSegment(memoryID).forEachInstance([this, &input](const auto & instance)
                                {
                                    if (input.actionPath == ActionPath{"vis"})
                                    {
                                        addInstanceToVisu(instance.id());
                                    }
                                    else if (input.actionPath == ActionPath{"rem"})
                                    {
                                        removeInstanceFromVisu(instance.id());
                                    }
                                });
                            }
                            else
                            {
                                if (input.actionPath == ActionPath{"rem"})
                                {
                                    std::unique_lock lock(gui.visualizationMutex);
                                    gui.visibleInstanceIds.clear();
                                    // mark all layers for deletion
                                    for (std::string & layer : gui.activeLayers)
                                    {
                                        arviz.commitDeleteLayer(layer + "_reachable");
                                        arviz.commitDeleteLayer(layer + "_unreachable");
                                    }
                                    gui.activeLayers.clear();
                                }
                                else if (input.actionPath == ActionPath{"vis"})
                                {
                                    //currently only visualization for CoreSegment GraspCandidate available
                                    workingMemory().getCoreSegment("GraspCandidate").forEachInstance([this](const auto & instance)
                                    {
                                        addInstanceToVisu(instance.id());
                                    });
                                }
                            }

                        visualizeGraspCandidates();
                        outputs.emplace_back(true, "");
                        }
                    }
                }
                else
                {
                    std::stringstream sstream;
                    sstream << "MemoryID " << memoryID
                            << " does not refer to a valid Grasp Memory Part.";
                    outputs.emplace_back(false, sstream.str());
                }
            }
            else if(input.actionPath == ActionPath{"track"} || input.actionPath == ActionPath{"untrack"} )
            {
                std::vector<armem::MemoryID> entityIDs;
                if(memoryID.hasEntityName())
                {
                    entityIDs.emplace_back(memoryID);
                }
                else if(memoryID.hasProviderSegmentName()){
                    workingMemory().getProviderSegment(memoryID).forEachEntity([&entityIDs](const auto & entity)
                    {
                        entityIDs.emplace_back(entity.id());
                    });
                }
                else
                {
                    //currently only visualization for CoreSegment GraspCandidate available
                    workingMemory().getCoreSegment("GraspCandidate").forEachEntity([&entityIDs](const auto & entity)
                    {
                        entityIDs.emplace_back(entity.id());
                    });
                }
                for(auto & entityID : entityIDs)
                {
                    if(input.actionPath == ActionPath{"track"})
                    {
                        //visualize latest snapshot of entity and refresh if necessary
                        workingMemory().getEntity(entityID).getLatestSnapshot().forEachInstance([this](const auto & instance)
                        {
                            addInstanceToVisu(instance.id());
                        });
                        gui.trackedEntityIds.push_back(entityID);
                        ARMARX_INFO << "starting to track " << entityID;
                        outputs.emplace_back(true, "");
                    }
                    else if(input.actionPath == ActionPath{"untrack"})
                    {
                        auto pos = std::find(gui.trackedEntityIds.begin(), gui.trackedEntityIds.end(), entityID);
                        if(pos != gui.trackedEntityIds.end())
                        {
                            gui.trackedEntityIds.erase(pos);
                            ARMARX_INFO << "Stop tracking of " << entityID;
                        }
                        outputs.emplace_back(true, "");
                    }
                }
            }
            else if(input.actionPath.front() == "high")
            {
                addToHighlightLayer(memoryID, input.actionPath.back());
            }
            else if(input.actionPath == ActionPath{"reset"})
            {
                arviz.commit(arviz.layer("HighlightedGrasps"));
            }
            else
            {
                std::stringstream sstream;
                sstream << "Action path " << input.actionPath << " is not defined.";
                outputs.emplace_back(false, sstream.str());
            }
        }
        return outputs;
    }



    // REMOTE GUI

    void GraspMemory::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;
        GridLayout root;
        {
            gui.tab.selectAll.setLabel("Select All");
            gui.tab.deselectAll.setLabel("Deselect All");
            gui.tab.showUnlimitedInstances.setValue(gui.unlimitedInstances);
            gui.tab.maxInstances.setRange(0,1000);
            gui.tab.maxInstances.setValue(gui.maxInstances);
            int row = 0;

            // bimanual candidates are not available for visualization for now
//            {
//                root.add(Label("CoreSegment"), Pos{row, 0});
//                row++;
//                std::vector<std::string> options = workingMemory().getCoreSegmentNames();
//                if (options.empty())
//                {
//                    options.push_back("<None>");
//                }
//                gui.tab.selectCoreSegment.setOptions(options);
//                if (gui.coreSegIndex >= 0 && gui.coreSegIndex < static_cast<int>(options.size()))
//                {
//                    gui.tab.selectCoreSegment.setIndex(gui.coreSegIndex);
//                    gui.coreSegment = options[gui.coreSegIndex];
//                }
//                root.add(gui.tab.selectCoreSegment, Pos{row,0});
//                row++;
//            }
            if(workingMemory().hasCoreSegment(gui.coreSegment))
            {
                {
                    root.add(Label("Provider"), Pos{row, 0});
                    row++;
                    std::vector<std::string> options = workingMemory().getCoreSegment(gui.coreSegment).getProviderSegmentNames();
                    if (options.empty())
                    {
                        options.push_back("<None>");
                    }
                    else
                    {
                        options.insert(options.begin(), "<All>");
                    }

                    gui.tab.selectProvider.setOptions(options);

                    if (gui.providerIndex >= 0 && gui.providerIndex < static_cast<int>(options.size()))
                    {
                        gui.tab.selectProvider.setIndex(gui.providerIndex);
                        gui.provider = options[gui.providerIndex];
                    }
                    root.add(gui.tab.selectProvider, Pos{row, 0});
                    row++;
                }

                if(gui.provider == "<All>"
                        || workingMemory().getCoreSegment(gui.coreSegment).hasProviderSegment(gui.provider))
                {
                    std::vector<MemoryID> providers;
                    {
                        root.add(Label("Entity"), Pos{row,0});
                        row++;
                        std::vector<std::string> options;

                        if (gui.provider == "<All>")
                        {
                            workingMemory().getCoreSegment(gui.coreSegment).forEachProviderSegment([&providers](const auto & provider)
                            {
                                providers.push_back(provider.id());
                            });
                        }
                        else
                        {
                            providers.push_back(MemoryID(workingMemory().name(), gui.coreSegment, gui.provider));

                        }
                        for (MemoryID & provider : providers)
                        {
                            for(std::string & entity : workingMemory().getProviderSegment(provider).getEntityNames())
                            {
                                options.push_back(entity);
                            }
                        }
                        if (options.empty())
                        {
                            options.push_back("<None>");
                        }
                        else
                        {
                            options.insert(options.begin(), "<All>");
                        }
                        gui.tab.selectEntity.setOptions(options);
                        if (gui.entityIndex >= 0 && gui.entityIndex < static_cast<int>(options.size()))
                        {
                            gui.tab.selectEntity.setIndex(gui.entityIndex);
                            gui.entity = options[gui.entityIndex];
                        }
                        root.add(gui.tab.selectEntity, Pos{row, 0});
                        row++;
                    }

                    if( gui.entity == "<All>"
                            || (gui.provider == "<All>" && workingMemory().getCoreSegment(gui.coreSegment).hasEntity(gui.entity))
                            || workingMemory().getCoreSegment(gui.coreSegment).getProviderSegment(gui.provider).hasEntity(gui.entity))
                    {
                        std::vector<MemoryID> entities;
                        {
                            root.add(Label("Snapshot"), Pos{row, 0});
                            row++;
                            std::vector<std::string> options;
                            for (MemoryID & provider : providers)
                            {
                                if (gui.entity == "<All>")
                                {
                                    workingMemory().getProviderSegment(provider).forEachEntity([&entities](auto & entity)
                                    {
                                        entities.push_back(entity.id());
                                    });
                                }
                                else
                                {
                                    if (workingMemory().getProviderSegment(provider).hasEntity(gui.entity))
                                    {
                                        entities.push_back(MemoryID(workingMemory().name(), gui.coreSegment, provider.providerSegmentName, gui.entity));
                                    }
                                }
                            }

                            for (MemoryID & entity : entities)
                            {
                                workingMemory().getEntity(entity).forEachSnapshot([this, &options](const auto & snapshot)
                                {
                                    std::string time = armem::toDateTimeMilliSeconds(snapshot.time());
                                    gui.timeMap[time] = snapshot.time();
                                    options.push_back(time);
                                });
                            }

                            if (options.empty())
                            {
                                options.push_back("<None>");
                            }
                            else
                            {
                                options.insert(options.begin(), "<All>");
                            }
                            gui.tab.selectSnapshot.setOptions(options);
                            if (gui.snapshotIndex >= 0 && gui.snapshotIndex < static_cast<int>(options.size()))
                            {
                                gui.tab.selectSnapshot.setIndex(gui.snapshotIndex);
                                if(options[gui.snapshotIndex] != "<None>")
                                {
                                    gui.snapshot = options[gui.snapshotIndex];
                                }
                            }
                            root.add(gui.tab.selectSnapshot, Pos{row, 0});
                            row++;


                        }
                        bool comboExists;
                        if (gui.snapshot == "<All>")
                        {
                            comboExists = true;
                        }
                        else
                        {
                            for (MemoryID & entity : entities)
                            {
                                if (workingMemory().getEntity(entity).hasSnapshot(gui.timeMap.at(gui.snapshot)))
                                {
                                    comboExists = true;
                                }
                            }
                        }
                        if(comboExists)
                        {
                            root.add(Label("Instances"), Pos{row,0});
                            row++;
                            root.add(Label("Show all Instances"), Pos{row, 0});
                            root.add(gui.tab.showUnlimitedInstances, Pos{row, 1});
                            row++;
                            root.add(Label("Limit for shown Instances"), Pos{row, 0});
                            root.add(gui.tab.maxInstances, Pos{row, 1});
                            row++;
                            root.add(gui.tab.selectAll, Pos{row, 0});
                            row++;
                            root.add(gui.tab.deselectAll, Pos{row, 0});
                            row++;
                            gui.tab.checkInstances.clear();
                            std::vector<MemoryID> snapshots;
                            for (MemoryID & entity : entities)
                            {
                                if(gui.snapshot == "<All>")
                                {
                                    workingMemory().getEntity(entity).forEachSnapshot([&snapshots](auto & snapshot)
                                    {
                                        snapshots.push_back(snapshot.id());
                                    });
                                }
                                else
                                {
                                    if (workingMemory().getEntity(entity).hasSnapshot(gui.timeMap.at(gui.snapshot)))
                                    {
                                        snapshots.push_back(workingMemory().getEntity(entity).getSnapshot(gui.timeMap.at(gui.snapshot)).id());
                                    }
                                }
                            }
                            int instances = 0;
                            for (MemoryID & snapshot : snapshots)
                            {
                                workingMemory().getSnapshot(snapshot).forEachInstance([this, &row, &root, &instances](const auto & instance)
                                {
                                    if (gui.unlimitedInstances || instances < gui.maxInstances)
                                    {
                                        std::unique_lock lock(gui.visualizationMutex);
                                        root.add(Label(instance.id().str()) , Pos{row, 0});
                                        gui.tab.checkInstances[instance.id().str()] = CheckBox();
                                        if(std::find(gui.visibleInstanceIds.begin(), gui.visibleInstanceIds.end(), instance.id()) != gui.visibleInstanceIds.end())
                                        {
                                           gui.tab.checkInstances.at(instance.id().str()).setValue(true);
                                        }
                                        root.add(gui.tab.checkInstances[instance.id().str()], Pos{row, 1});
                                        row++;
                                        instances++;
                                    }
                                });
                            }
                        }
                        else
                        {
                            root.add(Label("Instances"), Pos{row,0});
                            row++;
                            root.add(Label("Show all Instances"), Pos{row, 0});
                            root.add(gui.tab.showUnlimitedInstances, Pos{row, 1});
                            row++;
                            root.add(Label("Limit for shown Instances"), Pos{row, 0});
                            root.add(gui.tab.maxInstances, Pos{row, 1});
                            row++;
                            root.add(gui.tab.selectAll, Pos{row, 0});
                            row++;
                            root.add(gui.tab.deselectAll, Pos{row, 0});
                            row++;
                            gui.tab.checkInstances.clear();
                        }
                    }
                    else
                    {
                        gui.tab.checkInstances.clear();

                        std::vector<std::string> options = {"<None>"};

                        gui.tab.selectSnapshot.setOptions(options);

                        root.add(Label("Snapshot"), Pos{row, 0});
                        row++;
                        root.add(gui.tab.selectSnapshot, Pos{row, 0});
                        row++;
                        root.add(Label("Show all Instances"), Pos{row, 0});
                        root.add(gui.tab.showUnlimitedInstances, Pos{row, 1});
                        row++;
                        root.add(Label("Limit for shown Instances"), Pos{row, 0});
                        root.add(gui.tab.maxInstances, Pos{row, 1});
                        row++;
                        root.add(gui.tab.selectAll, Pos{row, 0});
                        row++;
                        root.add(gui.tab.deselectAll, Pos{row, 0});
                        row++;
                    }
                }
                else
                {
                    gui.tab.checkInstances.clear();

                    std::vector<std::string> options = {"<None>"};

                    gui.tab.selectEntity.setOptions(options);

                    root.add(Label("Entity"), Pos{row,0});
                    row++;
                    root.add(gui.tab.selectEntity, Pos{row, 0});
                    row++;

                    gui.tab.selectSnapshot.setOptions(options);

                    root.add(Label("Snapshot"), Pos{row, 0});
                    row++;
                    root.add(gui.tab.selectSnapshot, Pos{row, 0});
                    row++;
                    root.add(Label("Show all Instances"), Pos{row, 0});
                    root.add(gui.tab.showUnlimitedInstances, Pos{row, 1});
                    row++;
                    root.add(Label("Limit for shown Instances"), Pos{row, 0});
                    root.add(gui.tab.maxInstances, Pos{row, 1});
                    row++;
                    root.add(gui.tab.selectAll, Pos{row, 0});
                    row++;
                    root.add(gui.tab.deselectAll, Pos{row, 0});
                    row++;
                }
            }
            else {
                gui.tab.checkInstances.clear();

                std::vector<std::string> options = {"<None>"};
                gui.tab.selectProvider.setOptions(options);

                root.add(Label("Provider"), Pos{row, 0});
                row++;
                root.add(gui.tab.selectProvider, Pos{row, 0});
                row++;

                gui.tab.selectEntity.setOptions(options);

                root.add(Label("Entity"), Pos{row,0});
                row++;
                root.add(gui.tab.selectEntity, Pos{row, 0});
                row++;

                gui.tab.selectSnapshot.setOptions(options);

                root.add(Label("Snapshot"), Pos{row, 0});
                row++;
                root.add(gui.tab.selectSnapshot, Pos{row, 0});
                row++;
                root.add(Label("Show all Instances"), Pos{row, 0});
                root.add(gui.tab.showUnlimitedInstances, Pos{row, 1});
                row++;
                root.add(Label("Limit for shown Instances"), Pos{row, 0});
                root.add(gui.tab.maxInstances, Pos{row, 1});
                row++;
                root.add(gui.tab.selectAll, Pos{row, 0});
                row++;
                root.add(gui.tab.deselectAll, Pos{row, 0});
                row++;

            }
        }


        RemoteGui_createTab(getName(), root, &gui.tab);
        gui.tab.rebuild = false;

    }


    void armarx::armem::server::grasp::GraspMemory::visualizeGraspCandidates()
    {
        std::unique_lock lock(gui.visualizationMutex);
        armarx::grasping::GraspCandidateVisu visu;
        std::map<std::string, viz::Layer> reachableLayers;
        std::map<std::string, viz::Layer> unreachableLayers;

        {

            for (auto & element : gui.visibleInstanceIds)
            {
                std::string entityName = element.entityName;

                if (reachableLayers.find(entityName) == reachableLayers.end())
                {
                    reachableLayers[entityName] = arviz.layer(entityName + "_reachable");
                    unreachableLayers[entityName] = arviz.layer(entityName + "_unreachable");
                    gui.activeLayers.push_back(entityName);
                }

                armarx::grasping::GraspCandidate candidate;

                armarx::grasping::arondto::GraspCandidate aronTransform;

                aronTransform.fromAron(workingMemory().getInstance(element).data());

                fromAron(aronTransform, candidate);

                visu.visualize(element.str(), candidate, reachableLayers.at(entityName), unreachableLayers.at(entityName));

            }
        }

        std::vector<viz::Layer> layers;
        for(auto & [entityName, layer] : reachableLayers)
        {
            layers.push_back(layer);
            layers.push_back(unreachableLayers.at(entityName));
        }
        arviz.commit(layers);
    }

    void armarx::armem::server::grasp::GraspMemory::addInstanceToVisu(const armarx::armem::MemoryID &instance)
    {
        std::unique_lock lock(gui.visualizationMutex);
        auto position = std::find(gui.visibleInstanceIds.begin(), gui.visibleInstanceIds.end(), instance);

        if(position == gui.visibleInstanceIds.end())
        {
            gui.visibleInstanceIds.push_back(instance);
        }

    }


    void armarx::armem::server::grasp::GraspMemory::removeInstanceFromVisu(const armarx::armem::MemoryID &instance)
    {
        std::unique_lock lock(gui.visualizationMutex);
        auto position = std::find(gui.visibleInstanceIds.begin(), gui.visibleInstanceIds.end(), instance);

        if (position != gui.visibleInstanceIds.end())
        {
            gui.visibleInstanceIds.erase(position);
        }
        //check if this was the last instance of this entity and if so, mark layer for deletion

        std::string entityName = instance.entityName;

        if (std::none_of(gui.visibleInstanceIds.begin(), gui.visibleInstanceIds.end(), [&entityName](const armem::MemoryID& id)
        { return id.entityName == entityName; }))
        {
            arviz.commitDeleteLayer(entityName + "_reachable");
            arviz.commitDeleteLayer(entityName + "_unreachable");
        }

        auto layerPos = std::find(gui.activeLayers.begin(), gui.activeLayers.end(), entityName);

        if (layerPos != gui.activeLayers.end())
        {
            gui.activeLayers.erase(layerPos);
        }
    }

    void GraspMemory::addToHighlightLayer(const MemoryID &memoryID, const std::string color)
    {
        viz::Color handColor;

        if (color == "pink")
        {
            handColor = viz::Color::pink();
        }
        else if (color == "red")
        {
            handColor = viz::Color::red();
        }
        else if (color == "blue")
        {
            handColor = viz::Color::blue();
        }
        else if (color == "yellow")
        {
            handColor = viz::Color::yellow();
        }
        else if (color == "purple")
        {
            handColor = viz::Color::purple();
        }

        viz::Layer highlightLayer = arviz.layer(("HighlightedGrasps"));
        armarx::grasping::GraspCandidateVisu visu;

        std::vector<armem::MemoryID> instances;

        if (memoryID.hasInstanceIndex())
        {
            instances.push_back(memoryID);
        }
        else if (memoryID.hasTimestamp())
        {
            workingMemory().getSnapshot(memoryID).forEachInstance([&instances](const auto & instance)
            {
                instances.push_back(instance.id());
            });
        }
        else if (memoryID.hasEntityName())
        {
            workingMemory().getEntity(memoryID).forEachInstance([&instances](const auto & instance)
            {
                instances.push_back(instance.id());
            });
        }
        else if (memoryID.hasProviderSegmentName())
        {
            workingMemory().getProviderSegment(memoryID).forEachInstance([&instances](const auto & instance)
            {
                instances.push_back(instance.id());
            });
        }
        else
        {
                //currently only visualization for CoreSegment GraspCandidate available
                workingMemory().getCoreSegment("GraspCandidate").forEachInstance([&instances](const auto & instance)
                {
                    instances.push_back(instance.id());
                });
        }

        armarx::grasping::GraspCandidate candidate;

        armarx::grasping::arondto::GraspCandidate aronTransform;

        for (armem::MemoryID &instance : instances)
        {
            aronTransform.fromAron(workingMemory().getInstance(instance).data());

            fromAron(aronTransform, candidate);


            viz::Robot hand = visu.visualize(instance.str(), candidate);
            hand.color(handColor);
            highlightLayer.add(hand);
        }

        arviz.commit(highlightLayer);

    }

    void GraspMemory::RemoteGui_update()
    {
//        if (gui.tab.selectCoreSegment.hasValueChanged())
//        {
//            gui.coreSegment = gui.tab.selectCoreSegment.getValue();
//            gui.coreSegIndex = gui.tab.selectCoreSegment.getIndex();
//            gui.providerIndex = 0;
//            gui.entityIndex = 0;
//            gui.snapshotIndex = 0;
//            gui.tab.rebuild = true;
//        }

        if (gui.tab.selectProvider.hasValueChanged())
        {
            gui.provider = gui.tab.selectProvider.getValue();
            gui.providerIndex = gui.tab.selectProvider.getIndex();
            gui.entityIndex = 0;
            gui.snapshotIndex = 0;
            gui.tab.rebuild = true;
        }

        if (gui.tab.selectEntity.hasValueChanged())
        {
            gui.entity = gui.tab.selectEntity.getValue();
            gui.entityIndex = gui.tab.selectEntity.getIndex();
            gui.snapshotIndex = 0;
            gui.tab.rebuild = true;
        }

        if (gui.tab.selectSnapshot.hasValueChanged())
        {
            gui.snapshot = gui.tab.selectSnapshot.getValue();
            gui.snapshotIndex = gui.tab.selectSnapshot.getIndex();
            gui.tab.rebuild = true;
        }

        if(gui.tab.showUnlimitedInstances.hasValueChanged())
        {
            gui.unlimitedInstances = gui.tab.showUnlimitedInstances.getValue();
            gui.tab.rebuild = true;
        }

        if(gui.tab.maxInstances.hasValueChanged())
        {
            gui.maxInstances = gui.tab.maxInstances.getValue();
            gui.tab.rebuild = true;
        }

        if(gui.tab.selectAll.wasClicked())
        {
            for(auto & element : gui.tab.checkInstances)
            {
                element.second.setValue(true);
            }
        }

        if(gui.tab.deselectAll.wasClicked())
        {
            for(auto & element : gui.tab.checkInstances)
            {
                element.second.setValue(false);
            }
        }

        for(auto & element : gui.tab.checkInstances)
        {
            if(element.second.hasValueChanged())
            {
                if (element.second.getValue())
                {
                    addInstanceToVisu(armem::MemoryID::fromString(element.first));
                }
                else
                {
                    removeInstanceFromVisu(armem::MemoryID::fromString(element.first));
                }

            }
        }

        visualizeGraspCandidates();

        if (gui.tab.rebuild)
        {
            ARMARX_INFO << "Rebuilding remote gui tab";
            createRemoteGuiTab();
        }
    }

}
