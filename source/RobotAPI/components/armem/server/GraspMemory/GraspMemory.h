#pragma once


#include <memory>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/armem/server/plugins/ReadWritePluginUser.h>

#include <RobotAPI/libraries/armem_grasping/server/KnownGraspProviderSegment.h>

namespace armarx::armem::server::grasp
{
    /**
     * @defgroup Component-GraspMemory GraspMemory
     * @ingroup RobotAPI-Components
     * A description of the component GraspMemory.
     *
     * @class GraspMemory
     * @ingroup Component-GraspMemory
     * @brief Brief description of class GraspMemory.
     *
     * Detailed description of class GraspMemory.
     */
    class GraspMemory :
        virtual public armarx::Component
        , virtual public armem::server::ReadWritePluginUser
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        , virtual public armarx::ArVizComponentPluginUser
    {
    public:

        GraspMemory();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    public:
        armem::data::CommitResult commit(const armem::data::Commit& commit, const Ice::Current&) override;


    public:
        armem::actions::GetActionsOutputSeq getActions(const armem::actions::GetActionsInputSeq& inputs) override;
        armem::actions::ExecuteActionOutputSeq executeActions(const armem::actions::ExecuteActionInputSeq& inputs) override;

        void createRemoteGuiTab();
        void RemoteGui_update() override;


    protected:

        PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


    private:

        DebugObserverInterfacePrx debugObserver;

        // segments
        armem::grasping::segment::KnownGraspProviderSegment knownGraspProviderSegment;

        struct RemoteGuiTab : RemoteGui::Client::Tab
        {
            std::atomic_bool rebuild = false;

            // RemoteGui::Client::ComboBox selectCoreSegment;
            RemoteGui::Client::ComboBox selectProvider;
            RemoteGui::Client::ComboBox selectEntity;
            RemoteGui::Client::ComboBox selectSnapshot;
            RemoteGui::Client::CheckBox showUnlimitedInstances;
            RemoteGui::Client::IntSpinBox maxInstances;
            RemoteGui::Client::Button selectAll;
            RemoteGui::Client::Button deselectAll;
            std::map<std::string, RemoteGui::Client::CheckBox> checkInstances;
        };


        struct GuiInfo
        {
            // int coreSegIndex = 0;
            int providerIndex = 0;
            int entityIndex = 0;
            int snapshotIndex = 0;

            int maxInstances = 10;
            bool unlimitedInstances = false;

            RemoteGuiTab tab;

            std::string coreSegment = "GraspCandidate";
            std::string provider = "";
            std::string entity = "";
            std::string snapshot = "";

            std::map<std::string, Time> timeMap;

            std::mutex visualizationMutex;
            std::vector<armem::MemoryID> visibleInstanceIds;
            std::vector<std::string> activeLayers;
            std::vector<armem::MemoryID> trackedEntityIds;
            bool trackNewEntities{true};
        };
        GuiInfo gui;

        bool enableRemoteGui{true};

        void visualizeGraspCandidates();
        void addInstanceToVisu(const armem::MemoryID& instance);
        void removeInstanceFromVisu(const armem::MemoryID& instance);
        void addToHighlightLayer(const armem::MemoryID& memoryID, const std::string color);
    };
}
