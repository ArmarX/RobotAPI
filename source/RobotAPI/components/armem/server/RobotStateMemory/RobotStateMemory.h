/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotSensorMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <queue>

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotUnitDataStreamingReceiver/RobotUnitDataStreamingReceiver.h>
#include <RobotAPI/libraries/armem/server/plugins/ReadWritePluginUser.h>

#include <RobotAPI/libraries/armem_robot_state/server/description/Segment.h>
#include <RobotAPI/libraries/armem_robot_state/server/localization/Segment.h>
#include <RobotAPI/libraries/armem_robot_state/server/common/Visu.h>
#include <RobotAPI/libraries/armem_robot_state/server/proprioception/Segment.h>
#include <RobotAPI/libraries/armem_robot_state/server/proprioception/RobotStateWriter.h>
#include <RobotAPI/libraries/armem_robot_state/server/proprioception/RobotUnitData.h>
#include <RobotAPI/libraries/armem_robot_state/server/proprioception/RobotUnitReader.h>

#include <RobotAPI/interface/core/RobotLocalization.h>


namespace armarx::plugins
{
    class DebugObserverComponentPlugin;
    class RobotUnitComponentPlugin;
}
namespace armarx::armem::server::robot_state
{

    /**
     * @defgroup Component-RobotSensorMemory RobotSensorMemory
     * @ingroup RobotAPI-Components
     * A description of the component RobotSensorMemory.
     *
     * @class RobotSensorMemory
     * @ingroup Component-RobotSensorMemory
     * @brief Brief description of class RobotSensorMemory.
     *
     * Detailed description of class RobotSensorMemory.
     */
    class RobotStateMemory :
        virtual public armarx::Component,
        virtual public armem::server::ReadWritePluginUser,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armarx::GlobalRobotPoseProvider
    {
    public:

        RobotStateMemory();
        virtual ~RobotStateMemory() override;


        std::string getDefaultName() const override;


        // GlobalRobotPoseProvider interface
        armarx::PoseBasePtr getGlobalRobotPose(Ice::Long timestamp, const std::string& robotName, const ::Ice::Current&) override;


    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


    private:

        void startRobotUnitStream();
        void stopRobotUnitStream();


    private:

        armarx::plugins::DebugObserverComponentPlugin* debugObserver = nullptr;


        // Core segments
        // - description
        description::Segment descriptionSegment;

        // - proprioception
        proprioception::Segment proprioceptionSegment;
        armem::data::MemoryID robotUnitProviderID;

        // - localization
        localization::Segment localizationSegment;

        // Joint visu for all segments => robot pose and configuration
        Visu commonVisu;


        struct RobotUnit
        {
            // params
            static constexpr float ROBOT_UNIT_MAXIMUM_FREQUENCY = 100;
            static constexpr const char* sensorValuePrefix = "RobotUnit.";

            float pollFrequency = 50;

            armarx::plugins::RobotUnitComponentPlugin* plugin = nullptr;
            proprioception::RobotUnitReader reader;
            proprioception::RobotStateWriter writer;

            // queue
            std::queue<proprioception::RobotUnitData> dataQueue;
            std::mutex dataMutex;

            bool waitForRobotUnit = false;
        };
        RobotUnit robotUnit;
    };

}  // namespace armarx::armem::server::robot_state
