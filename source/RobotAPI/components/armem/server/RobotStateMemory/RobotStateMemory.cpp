/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotSensorMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotStateMemory.h"
#include "RobotAPI/libraries/armem/core/forward_declarations.h"

#include <RobotAPI/interface/core/PoseBase.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/armem/core/Prediction.h>
#include <RobotAPI/libraries/armem_robot_state/server/proprioception/aron_conversions.h>
#include <RobotAPI/libraries/armem_robot_state/server/common/Visu.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <SimoxUtility/algorithm/get_map_keys_values.h>
#include <SimoxUtility/algorithm/contains.h>
#include <SimoxUtility/algorithm/string.h>


namespace armarx::armem::server::robot_state
{

    RobotStateMemory::RobotStateMemory() :
        descriptionSegment(iceAdapter()),
        proprioceptionSegment(iceAdapter()),
        localizationSegment(iceAdapter()),
        commonVisu(descriptionSegment, proprioceptionSegment, localizationSegment)
    {
        addPlugin(debugObserver);
        ARMARX_CHECK_NOT_NULL(debugObserver);

        addPlugin(robotUnit.plugin);
        ARMARX_CHECK_NOT_NULL(robotUnit.plugin);
        robotUnit.plugin->setRobotUnitAsOptionalDependency(true);
    }


    RobotStateMemory::~RobotStateMemory()
    {
    }


    armarx::PropertyDefinitionsPtr RobotStateMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        const std::string robotUnitPrefix = robotUnit.sensorValuePrefix;


        defs->optional(robotUnit.waitForRobotUnit, "WaitForRobotUnit", "Add the robot unit as dependency to the component. This memory requires a running RobotUnit, therefore we should add it as explicit dependency.");

        defs->optional(robotUnit.reader.properties.sensorPrefix, robotUnitPrefix + "SensorValuePrefix",
                       "Prefix of all sensor values.");
        defs->optional(robotUnit.pollFrequency, robotUnitPrefix + "UpdateFrequency",
                       "The frequency to store values in Hz. All other values get discarded. "
                       "Minimum is 1, max is " + std::to_string(robotUnit.ROBOT_UNIT_MAXIMUM_FREQUENCY) + ".")
        .setMin(1).setMax(robotUnit.ROBOT_UNIT_MAXIMUM_FREQUENCY);


        const std::string prefix = "mem.";

        setMemoryName("RobotState");

        descriptionSegment.defineProperties(defs, prefix + "desc.");
        proprioceptionSegment.defineProperties(defs, prefix + "prop.");
        localizationSegment.defineProperties(defs, prefix + "loc.");
        commonVisu.defineProperties(defs, prefix + "visu.");

        return defs;
    }


    std::string RobotStateMemory::getDefaultName() const
    {
        return "RobotStateMemory";
    }


    void RobotStateMemory::onInitComponent()
    {
        descriptionSegment.init();
        proprioceptionSegment.init();
        localizationSegment.init();
        commonVisu.init();

        robotUnit.pollFrequency = std::clamp(robotUnit.pollFrequency, 0.f, robotUnit.ROBOT_UNIT_MAXIMUM_FREQUENCY);

        std::vector<std::string> includePaths;
        std::vector<std::string> packages = armarx::Application::GetProjectDependencies();
        packages.push_back(Application::GetProjectName());

        for (const std::string& projectName : packages)
        {
            if (projectName.empty())
            {
                continue;
            }

            CMakePackageFinder project(projectName);
            std::string pathsString = project.getIncludePaths();
            std::vector<std::string> projectIncludePaths = simox::alg::split(pathsString, ";,");
            includePaths.insert(includePaths.end(), projectIncludePaths.begin(), projectIncludePaths.end());
        }

        if (robotUnit.waitForRobotUnit)
        {
            usingProxy(robotUnit.plugin->getRobotUnitName());
        }
    }


    void RobotStateMemory::onConnectComponent()
    {
        ARMARX_CHECK_NOT_NULL(debugObserver->getDebugObserver());

        // 1. General setup
        localizationSegment.onConnect();
        commonVisu.connect(getArvizClient(), debugObserver->getDebugObserver());

        // 2. Check for RobotUnit. If RobotUnit is enabled. Otherwise we do not wait for a streaming service and do not setup the segments
        if (robotUnit.plugin->hasRobotUnitName())
        {
            robotUnit.plugin->waitUntilRobotUnitIsRunning();
            RobotUnitInterfacePrx robotUnitPrx = robotUnit.plugin->getRobotUnit();

            robotUnit.reader.setTag(getName());
            robotUnit.writer.setTag(getName());

            descriptionSegment.onConnect(robotUnitPrx);
            proprioceptionSegment.onConnect(robotUnitPrx);

            robotUnit.reader.connect(*robotUnit.plugin, *debugObserver,
                                     proprioceptionSegment.getRobotUnitProviderID().providerSegmentName);
            robotUnit.writer.connect(*debugObserver);
            robotUnit.writer.properties.robotUnitProviderID = proprioceptionSegment.getRobotUnitProviderID();

            robotUnit.reader.task = new SimpleRunningTask<>([this]()
            {
                robotUnit.reader.run(
                    robotUnit.pollFrequency, robotUnit.dataQueue, robotUnit.dataMutex
                );
            }, "Robot Unit Reader");
            robotUnit.writer.task = new SimpleRunningTask<>([this]()
            {
                robotUnit.writer.run(
                    robotUnit.pollFrequency, robotUnit.dataQueue, robotUnit.dataMutex,
                    iceAdapter(), localizationSegment
                );
            }, "Robot State Writer");

            startRobotUnitStream();
        }
    }


    void RobotStateMemory::onDisconnectComponent()
    {
        stopRobotUnitStream();
    }


    void RobotStateMemory::onExitComponent()
    {
        stopRobotUnitStream();
    }


    armarx::PoseBasePtr RobotStateMemory::getGlobalRobotPose(Ice::Long timestamp_us, const std::string& robotName, const ::Ice::Current& /*unused*/)
    {
        armem::Time timestamp { armem::Duration::MicroSeconds(timestamp_us) };
        ARMARX_DEBUG << "Getting robot pose of robot " << robotName << " at timestamp " << timestamp << ".";

        auto poseMap = localizationSegment.getRobotGlobalPoses(timestamp);
        
        bool robotNameFound = simox::alg::contains_key(poseMap, robotName);
        ARMARX_CHECK(robotNameFound)
            << "Robot with name " << robotName << " does not exist at or before timestamp " << timestamp << ".\n"
            << "Available robots are: " << simox::alg::get_keys(poseMap);

        return { new Pose(poseMap[robotName].matrix()) };
    }

    /*************************************************************/
    // RobotUnit Streaming functions
    /*************************************************************/


    void RobotStateMemory::startRobotUnitStream()
    {
        ARMARX_CHECK(robotUnit.plugin->robotUnitIsRunning());

        if (robotUnit.reader.task->isRunning() || robotUnit.writer.task->isRunning())
        {
            if (robotUnit.reader.task->isRunning() && robotUnit.writer.task->isRunning())
            {
                return;
            }
            ARMARX_WARNING << "Found inconsistency in running tasks. Restarting all!";
            stopRobotUnitStream();
        }
        {
            std::stringstream ss;
            ss << "Getting sensor values for:" << std::endl;
            for (const auto& [name, dataEntry] : robotUnit.reader.description.entries)
            {
                const std::string type = RobotUnitDataStreaming::DataEntryNames.to_name(dataEntry.type);
                ss << "\t" << name << " (type: '" << type << "')" << std::endl;
            }
            ARMARX_INFO << ss.str();
        }

        robotUnit.reader.task->start();
        robotUnit.writer.task->start();
    }


    void RobotStateMemory::stopRobotUnitStream()
    {
        robotUnit.reader.task->stop();
        robotUnit.writer.task->stop();
    }

}
