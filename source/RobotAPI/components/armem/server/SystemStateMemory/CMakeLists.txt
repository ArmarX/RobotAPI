armarx_component_set_name("SystemStateMemory")

set(COMPONENT_LIBS
    ArmarXCore ArmarXCoreInterfaces  # for DebugObserverInterface
    ArmarXGuiComponentPlugins
    RobotAPICore RobotAPIInterfaces armem armem_system_state
)

set(SOURCES
    SystemStateMemory.cpp
)
set(HEADERS
    SystemStateMemory.h
)

armarx_add_component("${SOURCES}" "${HEADERS}")


#generate the application
armarx_generate_and_add_component_executable()



