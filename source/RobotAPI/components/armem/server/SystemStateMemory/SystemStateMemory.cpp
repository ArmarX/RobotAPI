/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ExampleMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SystemStateMemory.h"

namespace armarx
{
    armarx::PropertyDefinitionsPtr SystemStateMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        setMemoryName("SystemState");

        const std::string prefix = "mem.";
        ramUsageCoreSegment.defineProperties(defs, prefix + "ram.");
        cpuUsageCoreSegment.defineProperties(defs, prefix + "cpu.");
        return defs;
    }

    SystemStateMemory::SystemStateMemory() :
        ramUsageCoreSegment(iceAdapter()),
        cpuUsageCoreSegment(iceAdapter())
    {
    }

    std::string SystemStateMemory::getDefaultName() const
    {
        return "SystemStateMemory";
    }


    void SystemStateMemory::onInitComponent()
    {
        ramUsageCoreSegment.init();
        cpuUsageCoreSegment.init();
    }


    void SystemStateMemory::onConnectComponent()
    {
    }


    void SystemStateMemory::onDisconnectComponent()
    {
    }


    void SystemStateMemory::onExitComponent()
    {
    }
}
