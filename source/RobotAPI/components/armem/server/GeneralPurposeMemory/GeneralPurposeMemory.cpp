/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::GeneralPurposeMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "GeneralPurposeMemory.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/server/MemoryRemoteGui.h>


namespace armarx
{
    GeneralPurposeMemory::GeneralPurposeMemory()
    {
    }

    armarx::PropertyDefinitionsPtr GeneralPurposeMemory::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        workingMemory().name() = "GeneralPurpose";

        return defs;
    }


    std::string GeneralPurposeMemory::getDefaultName() const
    {
        return "GeneralPurposeMemory";
    }


    void GeneralPurposeMemory::onInitComponent()
    {
    }


    void GeneralPurposeMemory::onConnectComponent()
    {
    }


    void GeneralPurposeMemory::onDisconnectComponent()
    {
    }


    void GeneralPurposeMemory::onExitComponent()
    {
    }



    armem::data::AddSegmentsResult GeneralPurposeMemory::addSegments(const armem::data::AddSegmentsInput& input, const Ice::Current&)
    {
        // Allowing adding core segments.
        armem::data::AddSegmentsResult result = ReadWritePluginUser::addSegments(input, addCoreSegmentOnUsage);
        return result;
    }

}
