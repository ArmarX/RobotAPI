/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::GeneralPurposeMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <RobotAPI/libraries/armem/server/plugins/ReadWritePluginUser.h>


namespace armarx
{
    /**
     * @defgroup Component-GeneralPurposeMemory GeneralPurposeMemory
     * @ingroup RobotAPI-Components
     * A description of the component GeneralPurposeMemory.
     *
     * @class GeneralPurposeMemory
     * @ingroup Component-GeneralPurposeMemory
     * @brief Brief description of class GeneralPurposeMemory.
     *
     * Detailed description of class GeneralPurposeMemory.
     */
    class GeneralPurposeMemory :
        virtual public armarx::Component
        , virtual public armem::server::ReadWritePluginUser
    {
    public:

        GeneralPurposeMemory();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    public:

        armem::data::AddSegmentsResult
        addSegments(const armem::data::AddSegmentsInput& input, const Ice::Current&) override;


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


    private:

        bool addCoreSegmentOnUsage = true;

    };
}
