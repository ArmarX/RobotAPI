/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::MemoryNameSystem
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MemoryNameSystem.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/armem/core/error.h>


namespace armarx::armem
{

    armarx::PropertyDefinitionsPtr MemoryNameSystem::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        return defs;
    }


    std::string MemoryNameSystem::getDefaultName() const
    {
        return "MemoryNameSystem";
    }



    void MemoryNameSystem::onInitComponent()
    {
    }


    void MemoryNameSystem::onConnectComponent()
    {
        createRemoteGuiTab();
        RemoteGui_startRunningTask();
    }


    void MemoryNameSystem::onDisconnectComponent()
    {
    }


    void MemoryNameSystem::onExitComponent()
    {
    }


    mns::dto::RegisterServerResult MemoryNameSystem::registerServer(
        const mns::dto::RegisterServerInput& input, const Ice::Current& c)
    {
        mns::dto::RegisterServerResult result = PluginUser::registerServer(input, c);
        tab.rebuild = true;
        return result;
    }


    mns::dto::RemoveServerResult MemoryNameSystem::removeServer(
        const mns::dto::RemoveServerInput& input, const Ice::Current& c)
    {
        mns::dto::RemoveServerResult result = PluginUser::removeServer(input, c);
        tab.rebuild = true;
        return result;
    }



    // REMOTE GUI

    void MemoryNameSystem::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        std::scoped_lock lock(mnsMutex);
        GridLayout grid = mns().RemoteGui_buildInfoGrid();

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void MemoryNameSystem::RemoteGui_update()
    {
        if (tab.rebuild.exchange(false))
        {
            createRemoteGuiTab();
        }
    }

}
