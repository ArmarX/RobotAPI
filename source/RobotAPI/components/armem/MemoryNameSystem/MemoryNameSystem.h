/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::MemoryNameSystem
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem/mns/plugins/PluginUser.h>
#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <ArmarXCore/core/Component.h>


namespace armarx::armem
{
    /**
     * @defgroup Component-MemoryNameSystem MemoryNameSystem
     * @ingroup RobotAPI-Components
     * A description of the component MemoryNameSystem.
     *
     * @class MemoryNameSystem
     * @ingroup Component-MemoryNameSystem
     * @brief Brief description of class MemoryNameSystem.
     *
     * Detailed description of class MemoryNameSystem.
     */
    class MemoryNameSystem :
        virtual public armarx::Component
        , virtual public armem::mns::plugins::PluginUser
        , virtual public LightweightRemoteGuiComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // mns::MemoryNameSystemInterface interface
    public:
        mns::dto::RegisterServerResult registerServer(const mns::dto::RegisterServerInput& input, const Ice::Current&) override;
        mns::dto::RemoveServerResult removeServer(const mns::dto::RemoveServerInput& input, const Ice::Current&) override;
        // Others are inherited from ComponentPluginUser


        // LightweightRemoteGuiComponentPluginUser interface
    public:

        void createRemoteGuiTab();
        void RemoteGui_update() override;


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


    private:

        struct Properties
        {
        };
        Properties properties;


        struct RemoteGuiTab : RemoteGui::Client::Tab
        {
            std::atomic_bool rebuild = false;
        };
        RemoteGuiTab tab;

    };
}
