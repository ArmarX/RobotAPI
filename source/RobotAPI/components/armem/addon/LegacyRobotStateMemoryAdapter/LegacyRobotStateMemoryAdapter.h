/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::MemoryNameSystem
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <mutex>

// Base Classes
#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/armem/addon/LegacyRobotStateMemoryAdapterInterface.h>

// ArmarX
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>

#include <RobotAPI/libraries/armem_robot_state/client/common/constants.h>
#include <RobotAPI/libraries/core/FramedPose.h>

namespace armarx::armem
{
    class LegacyRobotStateMemoryAdapter :
        virtual public armarx::Component,
        virtual public robot_state::LegacyRobotStateMemoryAdapterInterface
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

        void reportControlModeChanged(const NameControlModeMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointAngles(const NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointVelocities(const NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointTorques(const NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointAccelerations(const NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointCurrents(const NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointMotorTemperatures(const NameValueMap&, Ice::Long, bool, const Ice::Current&) override;
        void reportJointStatuses(const NameStatusMap&, Ice::Long, bool, const Ice::Current&) override;

        void reportPlatformPose(const PlatformPose &, const Ice::Current &) override;
        void reportNewTargetPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current &) override;
        void reportPlatformVelocity(Ice::Float, Ice::Float, Ice::Float, const Ice::Current &) override;
        void reportPlatformOdometryPose(Ice::Float, Ice::Float, Ice::Float, const Ice::Current &) override;

    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

    private:
        void commitArmar3RobotDescription();

        void updateTimestamps(long dataGeneratedUs);
        void checkUpdateAndSendToMemory();

    private:
        // Update to send to the memory
        struct Update
        {
            std::map<std::string, float> jointAngles = {};
            std::map<std::string, float> jointVelocities = {};
            std::map<std::string, float> jointTorques = {};
            std::map<std::string, float> jointAccelerations = {};
            std::map<std::string, float> jointCurrents = {};
            armarx::PlatformPose platformPose = {};
            std::tuple<float, float, float> platformVelocity = {0, 0, 0};
            std::tuple<float, float, float> platformOdometryPose = {0, 0, 0};
        };

        // Update
        bool updateChanged = true;
        double _timestampUpdateFirstModifiedInUs;
        Update update;

        // Component properties
        struct Properties
        {
            std::string memoryNameSystemName = "MemoryNameSystem";
            std::string robotStateMemoryName = "RobotState";
            int frequency = 100;
        };
        Properties properties;

        // RunningTask
        armarx::PeriodicTask<LegacyRobotStateMemoryAdapter>::pointer_type runningTask;

        // Memory
        MemoryID propEntityID = MemoryID(armarx::armem::robot_state::constants::memoryName, armarx::armem::robot_state::constants::proprioceptionCoreSegment, "Armar3", "Armar3");
        MemoryID locEntityID = MemoryID(armarx::armem::robot_state::constants::memoryName, armarx::armem::robot_state::constants::localizationCoreSegment, "Armar3", (OdometryFrame + "," + armarx::armem::robot_state::constants::robotRootNodeName));

        armem::client::Writer memoryWriter;

        mutable std::mutex updateMutex;
    };
}
