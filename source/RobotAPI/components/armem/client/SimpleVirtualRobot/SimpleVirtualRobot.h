
#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

// #include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/armem/client/plugins/ReaderWriterPlugin.h>
#include <RobotAPI/libraries/armem/client/plugins.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotWriter.h>


namespace armarx::simple_virtual_robot
{

    /**
     * @defgroup Component-SimpleVirtualRobot SimpleVirtualRobot
     * @ingroup RobotAPI-Components
     * Simply loads a Simox robot model and commits it to the memory.
     *
     * @class SimpleVirtualRobot
     * @ingroup Component-SimpleVirtualRobot
     * @brief Brief description of class SimpleVirtualRobot.
     *
     * Detailed description of class SimpleVirtualRobot.
     */
    class SimpleVirtualRobot :
        virtual public armarx::Component,
        virtual public armarx::armem::client::ComponentPluginUser
    {
    public:
        SimpleVirtualRobot();

        std::string getDefaultName() const override;

    protected:
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        void run();



    private:

        struct Properties
        {
            bool oneShot = true;
            float updateFrequency{10.F};

            struct Robot
            {
                std::string name;
                std::string package;
                std::string path;

                std::string jointValues; // json-style map<std::string, float>
            };
            Robot robot;
        };

        VirtualRobot::RobotPtr loadRobot(const Properties::Robot& p) const;


    private:
        Properties properties;


        VirtualRobot::RobotPtr robot;

        armarx::SimpleRunningTask<>::pointer_type task;

        armem::client::plugins::ReaderWriterPlugin<armem::robot_state::VirtualRobotWriter>*
        virtualRobotWriterPlugin = nullptr;


    };

}  // namespace armarx::simple_virtual_robot
