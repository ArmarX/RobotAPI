
#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/util/tasks.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include "RobotAPI/libraries/armem/client/plugins/ReaderWriterPlugin.h"
#include "RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotWriter.h"
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>
#include <RobotAPI/interface/armem/server/MemoryInterface.h>
#include <RobotAPI/libraries/armem/client/plugins.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>

namespace armarx::virtual_robot_writer_example
{

    /**
     * @defgroup Component-VirtualRobotWriterExample VirtualRobotWriterExample
     * @ingroup RobotAPI-Components
     * A description of the component VirtualRobotWriterExample.
     *
     * @class VirtualRobotWriterExample
     * @ingroup Component-VirtualRobotWriterExample
     * @brief Brief description of class VirtualRobotWriterExample.
     *
     * Detailed description of class VirtualRobotWriterExample.
     */
    class VirtualRobotWriterExample :
        virtual public armarx::Component,
        virtual public armarx::armem::client::ComponentPluginUser
    {
    public:
        VirtualRobotWriterExample();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

    protected:
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        void run();

    private:
        VirtualRobot::RobotPtr loadRobot() const;
        VirtualRobot::RobotPtr robot;

        /// Reference timestamp for object movement
        armem::Time start;

        armarx::PeriodicTask<VirtualRobotWriterExample>::pointer_type task;

        armem::client::plugins::ReaderWriterPlugin<armem::robot_state::VirtualRobotWriter>* virtualRobotWriterPlugin = nullptr;

        struct Properties
        {
            float updateFrequency{10.F};

            struct
            {
                std::string package;
                std::string path;
            } robot;
        } p;
    };

}  // namespace armarx::virtual_robot_writer_example
