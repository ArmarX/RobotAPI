/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotStatePredictionClientExample
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Impl.h"

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/Metronome.h>

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/query.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem_robot_state/aron/Proprioception.aron.generated.h>
#include <RobotAPI/libraries/armem_robot_state/aron/Transform.aron.generated.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/constants.h>
#include <RobotAPI/libraries/armem_robot_state/common/localization/TransformHelper.h>


namespace simox::alg
{
    template <class... Args>
    std::vector<Args...>
    concatenate(const std::vector<Args...>& lhs, const std::vector<Args...>& rhs)
    {
        std::vector<Args...> conc = lhs;
        std::copy(rhs.begin(), rhs.end(), std::back_inserter(conc));
        return conc;
    }


    template <class KeyT, class ValueT>
    std::map<KeyT, ValueT>
    map_from_key_value_pairs(const std::vector<KeyT>& lhs, const std::vector<ValueT>& rhs)
    {
        const size_t size = std::min(lhs.size(), rhs.size());

        std::map<KeyT, ValueT> map;
        for (size_t i = 0; i < size; ++i)
        {
            map.emplace(lhs[i], rhs[i]);
        }
        return map;
    }


    template <class KeyT, class ValueT>
    std::vector<ValueT>
    multi_at(const std::map<KeyT, ValueT>& map,
             const std::vector<KeyT>& keys,
             bool skipMissing = false)
    {
        std::vector<ValueT> values;
        values.reserve(keys.size());

        for (const KeyT& key : keys)
        {
            if (skipMissing)
            {
                if (auto it = map.find(key); it != map.end())
                {
                    values.push_back(it->second);
                }
            }
            else
            {
                // Throw an exception if missing.
                values.push_back(map.at(key));
            }
        }

        return values;
    }

    template <class... Args>
    std::vector<Args...>
    slice(const std::vector<Args...>& vector,
          size_t start = 0,
          std::optional<size_t> end = std::nullopt)
    {
        std::vector<Args...> result;
        auto beginIt = vector.begin() + start;
        auto endIt = end ? vector.begin() + *end : vector.end();
        std::copy(beginIt, endIt, std::back_inserter(result));
        return result;
    }

} // namespace simox::alg

namespace armarx::robot_state_prediction_client_example
{

    Impl::Impl(armem::client::MemoryNameSystem& memoryNameSystem)
    {
        client.remote.robotReader.emplace(memoryNameSystem);
    }


    Impl::~Impl() = default;


    void
    Impl::defineProperties(IceUtil::Handle<PropertyDefinitionContainer>& defs,
                           const std::string& prefix)
    {
        defs->optional(properties.robotName, prefix + "robotName", "Name of the robot.");

        defs->optional(properties.predictAheadSeconds,
                       prefix + "predictAheadSeconds",
                       "How far into the future to predict [s].");

        client.remote.robotReader->registerPropertyDefinitions(defs);
    }


    void
    Impl::connect(armem::client::MemoryNameSystem& mns, viz::Client arviz)
    {
        try
        {
            client.remote.reader =
                mns.useReader(armem::MemoryID(armem::robot_state::constants::memoryName));
            ARMARX_IMPORTANT << "got reader";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_WARNING << e.what();
        }

        client.remote.robotReader->connect();

        this->remote.arviz = arviz;
    }


    void
    Impl::start()
    {
        task = new armarx::SimpleRunningTask<>([this]() { this->run(); });
        task->start();
    }


    void
    Impl::stop()
    {
        task->stop();
    }


    void
    Impl::run()
    {
        Metronome metronome(Frequency::Hertz(properties.updateFrequencyHz));

        while (task and not task->isStopped())
        {
            metronome.waitForNextTick();

            runOnce();
        }
    }


    void
    Impl::runOnce()
    {
        ARMARX_CHECK(client.remote.reader);

        const DateTime now = Clock::Now();
        const DateTime predictedTime =
            now + Duration::SecondsDouble(properties.predictAheadSeconds);

        if (not robotViz)
        {
            auto desc = client.remote.robotReader->queryDescription(properties.robotName, now);
            if (desc.has_value())
            {
                PackagePath& pp = desc->xml;
                robotViz = viz::Robot(properties.robotName)
                               .file(pp.serialize().package, pp.serialize().path)
                               .overrideColor(simox::Color::cyan(255, 64));
            }
            else
            {
                ARMARX_INFO << "Failed to get robot description.";
            }
        }
        if (not robotViz)
        {
            return;
        }

        // Which entities to predict?

        const std::vector<armem::MemoryID> locEntityIDs = this->localizationEntityIDs.has_value()
                                                              ? this->localizationEntityIDs.value()
                                                              : client.queryLocalizationEntityIDs();
        const std::vector<armem::MemoryID> propEntityIDs =
            this->propioceptionEntityIDs.has_value() ? this->propioceptionEntityIDs.value()
                                                     : client.queryProprioceptionEntityIDs();

        // Predict.

        auto prediction = client.predictWholeBody(
            locEntityIDs, propEntityIDs, predictedTime, properties.robotName, "Linear");

        // Gather results.

        if (prediction.globalPose.has_value())
        {
            // ARMARX_INFO << "Predicted global pose: \n" << globalPose->matrix();
            robotViz->pose(prediction.globalPose->matrix());

            if (not localizationEntityIDs)
            {
                // Store entity IDs for successful lookup.
                this->localizationEntityIDs = locEntityIDs;
            }
        }
        if (prediction.jointPositions.has_value())
        {
            robotViz->joints(prediction.jointPositions.value());

            if (not propioceptionEntityIDs)
            {
                this->propioceptionEntityIDs = propEntityIDs;
            }
        }

        // Visualize.
        {
            viz::Layer layer = remote.arviz.layer(properties.robotName + " Prediction");
            layer.add(robotViz.value());
            remote.arviz.commit(layer);
        }
    }


} // namespace armarx::robot_state_prediction_client_example
