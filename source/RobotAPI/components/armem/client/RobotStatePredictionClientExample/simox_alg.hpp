/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotStatePredictionClientExample
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <vector>

namespace simox::alg
{
    template <class... Args>
    std::vector<Args...>
    concatenate(const std::vector<Args...>& lhs, const std::vector<Args...>& rhs)
    {
        std::vector<Args...> conc = lhs;
        std::copy(rhs.begin(), rhs.end(), std::back_inserter(conc));
        return conc;
    }


    template <class KeyT, class ValueT>
    std::map<KeyT, ValueT>
    map_from_key_value_pairs(const std::vector<KeyT>& lhs, const std::vector<ValueT>& rhs)
    {
        const size_t size = std::min(lhs.size(), rhs.size());

        std::map<KeyT, ValueT> map;
        for (size_t i = 0; i < size; ++i)
        {
            map.emplace(lhs[i], rhs[i]);
        }
        return map;
    }


    template <class KeyT, class ValueT>
    std::vector<ValueT>
    multi_at(const std::map<KeyT, ValueT>& map,
             const std::vector<KeyT>& keys,
             bool skipMissing = false)
    {
        std::vector<ValueT> values;
        values.reserve(keys.size());

        for (const KeyT& key : keys)
        {
            if (skipMissing)
            {
                if (auto it = map.find(key); it != map.end())
                {
                    values.push_back(it->second);
                }
            }
            else
            {
                // Throw an exception if missing.
                values.push_back(map.at(key));
            }
        }

        return values;
    }

    template <class... Args>
    std::vector<Args...>
    slice(const std::vector<Args...>& vector,
          size_t start = 0,
          std::optional<size_t> end = std::nullopt)
    {
        std::vector<Args...> result;
        auto beginIt = vector.begin() + start;
        auto endIt = end ? vector.begin() + *end : vector.end();
        std::copy(beginIt, endIt, std::back_inserter(result));
        return result;
    }

} // namespace simox::alg
