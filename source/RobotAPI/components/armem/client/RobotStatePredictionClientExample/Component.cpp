/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotStatePredictionClientExample
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "RobotStatePredictionClientExample.h"
#include "Impl.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::robot_state_prediction_client_example
{

    Component::Component() :
        pimpl(std::make_unique<Impl>(memoryNameSystem()))
    {
    }


    RobotStatePredictionClientExample::~RobotStatePredictionClientExample() = default;


    std::string Component::getDefaultName() const
    {
        return "RobotStatePredictionClientExample";
    }


    armarx::PropertyDefinitionsPtr Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        ARMARX_CHECK_NOT_NULL(pimpl);
        pimpl->defineProperties(defs, "p.");

        return defs;
    }


    void Component::onInitComponent()
    {
    }


    void Component::onConnectComponent()
    {
        pimpl->connect(memoryNameSystem(), arviz);
        pimpl->start();

        createRemoteGuiTab();
        RemoteGui_startRunningTask();
    }


    void Component::onDisconnectComponent()
    {
        pimpl->stop();
    }


    void Component::onExitComponent()
    {
    }


    void Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        VBoxLayout root = {VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }

    void Component::RemoteGui_update()
    {
    }

}
