/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotStatePredictionClientExample
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <memory>

#include <ArmarXCore/core/Component.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>

// For some reason, the generated main requires Impl to be complete ...
#include "Impl.h"


namespace armarx::robot_state_prediction_client_example
{
    class Impl;


    /**
     * @defgroup Component-ExampleClient ExampleClient
     * @ingroup RobotAPI-Components
     *
     * An example for an ArMem Memory Client.
     *
     * @class ExampleClient
     * @ingroup Component-ExampleClient
     * @brief Brief description of class ExampleClient.
     *
     * Connects to the example memory, and commits and queries example data.
     */
    class Component :
        virtual public armarx::Component
        , virtual public armarx::LightweightRemoteGuiComponentPluginUser
        , virtual public armarx::ArVizComponentPluginUser
        , virtual public armarx::armem::ClientPluginUser
    {
    public:
        using Impl = robot_state_prediction_client_example::Impl;

        Component();
        virtual ~Component();


        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // LightweightRemoteGuiComponentPluginUser interface
    public:
        void createRemoteGuiTab();
        void RemoteGui_update() override;


    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


    private:

        std::unique_ptr<Impl> pimpl = nullptr;

        struct RemoteGuiTab : RemoteGui::Client::Tab
        {
        };
        RemoteGuiTab tab;

    };
}
