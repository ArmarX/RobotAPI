/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotStatePredictionClientExample
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <optional>
#include <vector>

#include <Eigen/Geometry>

#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/core/forward_declarations.h>
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>


namespace armarx::armem::robot_state
{

    class RobotStatePredictionClient
    {
    public:
        RobotStatePredictionClient();


        struct WholeBodyPrediction
        {
            std::optional<Eigen::Affine3f> globalPose;
            std::optional<std::map<std::string, float>> jointPositions;
        };
        WholeBodyPrediction
        predictWholeBody(const std::vector<armem::MemoryID>& localizationEntityIDs,
                         const std::vector<armem::MemoryID>& proprioceptionEntityIDs,
                         armem::Time predictedTime,
                         const std::string& robotName,
                         const std::string& engineID = "Linear");

        std::optional<Eigen::Affine3f>
        predictGlobalPose(const std::vector<armem::MemoryID>& entityIDs,
                          armem::Time predictedTime,
                          const std::string& engineID = "Linear");

        std::optional<std::map<std::string, float>>
        predictJointPositions(const std::vector<armem::MemoryID>& entityIDs,
                              armem::Time predictedTime,
                              const std::string& robotName,
                              const std::string& engineID = "Linear");


        std::vector<armem::PredictionResult> predict(const std::vector<armem::MemoryID>& entityIDs,
                                                     armem::Time predictedTime,
                                                     const std::string& engineID = "Linear");
        std::vector<armem::PredictionResult>
        predict(const std::vector<armem::PredictionRequest>& requests);


        std::vector<armem::MemoryID> queryLocalizationEntityIDs();
        std::vector<armem::MemoryID> queryProprioceptionEntityIDs();

        std::vector<armem::PredictionRequest>
        makePredictionRequests(const std::vector<armem::MemoryID>& entityIDs,
                               armem::Time predictedTime,
                               const std::string& engineID = "Linear");

        std::optional<Eigen::Affine3f>
        lookupGlobalPose(const std::vector<armem::PredictionResult>& localizationPredictionResults,
                         const std::vector<armem::MemoryID>& localizationEntityIDs,
                         armem::Time predictedTime);

        std::optional<std::map<std::string, float>> lookupJointPositions(
            const std::vector<armem::PredictionResult>& proprioceptionPredictionResults,
            const std::string& robotName);


    private:
        std::vector<armem::MemoryID> _queryEntityIDs(const std::string& coreSegmentName);

    public:
        struct Remote
        {
            armem::client::Reader reader;
            std::optional<armem::robot_state::VirtualRobotReader> robotReader;
        };
        Remote remote;
    };

} // namespace armarx::armem::robot_state
