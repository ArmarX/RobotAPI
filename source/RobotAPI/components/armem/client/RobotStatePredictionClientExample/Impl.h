/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotStatePredictionClientExample
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <vector>

#include <ArmarXCore/util/tasks.h>

#include <RobotAPI/libraries/armem/core/forward_declarations.h>
#include <RobotAPI/libraries/armem/client/forward_declarations.h>
#include <RobotAPI/libraries/armem/client/Reader.h>

#include <RobotAPI/components/ArViz/Client/Client.h>

#include "RobotStatePredictionClient.h"


namespace armarx::robot_state_prediction_client_example
{

    class Impl
    {
    public:

        Impl(armem::client::MemoryNameSystem& memoryNameSystem);
        ~Impl();


        void defineProperties(IceUtil::Handle<armarx::PropertyDefinitionContainer>& defs, const std::string& prefix);
        void connect(armem::client::MemoryNameSystem& mns, viz::Client arviz);

        void start();
        void stop();

        void run();
        void runOnce();


    public:

        struct Properties
        {
            float updateFrequencyHz = 10;

            std::string robotName = "Armar6";
            float predictAheadSeconds = 1.0;
        };
        Properties properties;

        struct Remote
        {
            viz::Client arviz;
        };
        Remote remote;

        armarx::SimpleRunningTask<>::pointer_type task;


        armem::robot_state::RobotStatePredictionClient client;

        std::optional<std::vector<armem::MemoryID>> localizationEntityIDs;
        std::optional<std::vector<armem::MemoryID>> propioceptionEntityIDs;
        std::optional<viz::Robot> robotViz;

    };
}
