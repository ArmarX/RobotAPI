#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/util/tasks.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/libraries/GraspingUtility/GraspCandidateReader.h>
#include <RobotAPI/libraries/GraspingUtility/GraspCandidateWriter.h>

#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>

#pragma once


namespace armarx
{


    class GraspProviderExamplePropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        GraspProviderExamplePropertyDefinitions(std::string prefix);
    };


    class GraspProviderExample :
        virtual public armarx::Component,
        virtual public armarx::armem::ClientPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;
        GraspProviderExample();


    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        void run();

        grasping::GraspCandidate makeDummyGraspCandidate();
        grasping::BimanualGraspCandidate makeDummyBimanualGraspCandidate();


    private:

        Eigen::Matrix4f const identityMatrix = Eigen::Matrix4f::Identity();
        Eigen::Vector3f const zeroVector = Eigen::Vector3f();

        armarx::RunningTask<GraspProviderExample>::pointer_type task;

        armarx::DebugObserverInterfacePrx debugObserver;

        std::string memoryName = "Grasp";
        armarx::armem::GraspCandidateWriter writer;
        armarx::armem::GraspCandidateReader reader;
    };
}
