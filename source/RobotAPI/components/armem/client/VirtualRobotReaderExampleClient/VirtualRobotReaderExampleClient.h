
/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <RobotAPI/libraries/armem/client/plugins.h>

#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>


namespace armarx::robot_state
{

    /**
     * @defgroup Component-VirtualRobotReaderExampleClient VirtualRobotReaderExampleClient
     * @ingroup RobotAPI-Components
     * A description of the component VirtualRobotReaderExampleClient.
     *
     * @class VirtualRobotReaderExampleClient
     * @ingroup Component-VirtualRobotReaderExampleClient
     * @brief Brief description of class VirtualRobotReaderExampleClient.
     *
     * Detailed description of class VirtualRobotReaderExampleClient.
     */
    class VirtualRobotReaderExampleClient :
        virtual public armarx::Component,
        // Use the memory client plugin.
        virtual public armarx::armem::client::ComponentPluginUser
    {
    public:
        VirtualRobotReaderExampleClient();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


    private:

        void run();


    private:

        struct Properties
        {
            std::string robotName{"Armar6"};
            float updateFrequency{10.F};
        };
        Properties properties;

        // The running task which is started in onConnectComponent().
        armarx::SimpleRunningTask<>::pointer_type task;

        // The reader used to get the robot state.
        armem::robot_state::VirtualRobotReader virtualRobotReader;

        // The Simox robot model.
        VirtualRobot::RobotPtr robot = nullptr;

        // For publishing timing information.
        armarx::DebugObserverInterfacePrx debugObserver;

    };

}  // namespace armarx::robot_state
