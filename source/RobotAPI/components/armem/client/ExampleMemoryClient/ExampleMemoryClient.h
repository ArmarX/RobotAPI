/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ExampleClient
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <RobotAPI/libraries/armem/client/plugins/ListeningPluginUser.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/util/tasks.h>


namespace armarx
{

    /**
     * @defgroup Component-ExampleClient ExampleClient
     * @ingroup RobotAPI-Components
     *
     * An example for an ArMem Memory Client.
     *
     * @class ExampleClient
     * @ingroup Component-ExampleClient
     * @brief Brief description of class ExampleClient.
     *
     * Connects to the example memory, and commits and queries example data.
     */
    class ExampleMemoryClient :
        virtual public armarx::Component,
        virtual public armarx::armem::ListeningClientPluginUser,
        virtual public armarx::LightweightRemoteGuiComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // LightweightRemoteGuiComponentPluginUser interface
    public:
        void createRemoteGuiTab();
        void RemoteGui_update() override;


    protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        void run();


    private:

        // Callback for updates on `example_entity`.
        void processExampleEntityUpdate(const armem::MemoryID& id, const std::vector<armem::MemoryID>& snapshotIDs);

        // Examples
        void waitForMemory();
        armem::MemoryID addProviderSegment();

        armem::MemoryID commitSingleSnapshot(const armem::MemoryID& exampleEntityID);
        void commitMultipleSnapshots(const armem::MemoryID& exampleEntityID, int num = 3);

        void queryLatestSnapshot(const armem::MemoryID& exampleEntityID);
        void queryExactSnapshot(const armem::MemoryID& snapshotID);

        void commitExampleData();
        void queryExampleData();

        void commitExamplesWithIDs();
        void commitExamplesWithLinks();

        void commitExampleImages();
        void commitExamplesWithUntypedData();

        void queryPredictionEngines();


    private:

        struct Properties
        {
            std::string usedMemoryName = "Example";
            float commitFrequency = 10;
        };
        Properties p;

        armem::client::Reader memoryReader;
        armem::client::Writer memoryWriter;

        armem::MemoryID exampleProviderID;
        armem::MemoryID exampleEntityID;

        armem::MemoryID exampleDataProviderID;
        armem::MemoryID theAnswerSnapshotID;
        armem::MemoryID linkedDataProviderID;
        armem::MemoryID yetMoreDataSnapshotID;


        armem::Time runStarted;
        unsigned int imageCounter = 0;

        armarx::RunningTask<ExampleMemoryClient>::pointer_type task;

        armarx::DebugObserverInterfacePrx debugObserver;

        struct RemoteGuiTab : RemoteGui::Client::Tab
        {
            std::atomic_bool rebuild = false;

            std::optional<armem::wm::Memory> queryResult;
            RemoteGui::Client::GroupBox queryResultGroup;
        };
        RemoteGuiTab tab;
    };
}
