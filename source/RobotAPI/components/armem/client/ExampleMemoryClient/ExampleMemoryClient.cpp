/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ExampleMemoryClient
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExampleMemoryClient.h"

#include <RobotAPI/components/armem/server/ExampleMemory/aron/ExampleData.aron.generated.h>

#include <random>
#include <algorithm>

#include <opencv2/opencv.hpp>

#include <SimoxUtility/color/cmaps.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/CycleUtil.h>

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/server/MemoryRemoteGui.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/client/query/query_fns.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/aron_conversions.h>
#include <RobotAPI/libraries/armem/core/operations.h>
#include <RobotAPI/libraries/armem/core/wm/ice_conversions.h>

#include <RobotAPI/libraries/aron/core/data/variant/All.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/CycleUtil.h>

#include <SimoxUtility/color/cmaps.h>
#include <SimoxUtility/math/pose/pose.h>

#include <Eigen/Geometry>

#include <random>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include <RobotAPI/libraries/aron/converter/opencv/OpenCVConverter.h>
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

namespace armarx
{

    armarx::PropertyDefinitionsPtr ExampleMemoryClient::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs = new ComponentPropertyDefinitions(getConfigIdentifier());

        defs->topic(debugObserver);

        defs->optional(p.usedMemoryName, "mem.UsedMemoryName", "Name of the memory to use.");
        defs->optional(p.commitFrequency, "ex.CommitFrequency", "Frequency in which example data is commited. (max = 50Hz)");

        return defs;
    }


    std::string ExampleMemoryClient::getDefaultName() const
    {
        return "ExampleMemoryClient";
    }


    void ExampleMemoryClient::onInitComponent()
    {
    }


    void ExampleMemoryClient::onConnectComponent()
    {
        p.commitFrequency = std::min(p.commitFrequency, 50.f);

        createRemoteGuiTab();
        RemoteGui_startRunningTask();

        // Wait for the memory to become available and add it as dependency.
        ARMARX_IMPORTANT << "Waiting for memory '" << p.usedMemoryName << "' ...";
        try
        {
            memoryReader = memoryNameSystem().useReader(p.usedMemoryName);
            memoryWriter = memoryNameSystem().useWriter(p.usedMemoryName);
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }

        // Add a provider segment to commit to.
        exampleProviderID = addProviderSegment();
        // Construct the entity ID.
        exampleEntityID = exampleProviderID.withEntityName("example_entity");


        // Subscribe to example_entity updates
        // Using a lambda:
        memoryNameSystem().subscribe(
            exampleEntityID,
            [&](const armem::MemoryID & exampleEntityID, const std::vector<armem::MemoryID>& snapshotIDs)
        {
            ARMARX_INFO << "Entity " << exampleEntityID << " was updated by " << snapshotIDs.size() << " snapshots.";
        });
        // Using a member function:
        memoryNameSystem().subscribe(exampleEntityID, this, &ExampleMemoryClient::processExampleEntityUpdate);


        task = new RunningTask<ExampleMemoryClient>(this, &ExampleMemoryClient::run);
        task->start();
    }


    void ExampleMemoryClient::onDisconnectComponent()
    {
        task->stop();
    }


    void ExampleMemoryClient::onExitComponent()
    {
    }


    void ExampleMemoryClient::run()
    {
        ARMARX_IMPORTANT << "Running example.";
        runStarted = armem::Time::Now();

        armem::MemoryID snapshotID = commitSingleSnapshot(exampleEntityID);
        if (true)
        {
            commitMultipleSnapshots(exampleEntityID, 3);
        }
        if (true)
        {
            queryLatestSnapshot(snapshotID.getEntityID());
        }
        if (true)
        {
            queryExactSnapshot(snapshotID);
        }
        if (true)
        {
            commitExampleData();
            queryExampleData();
        }
        if (true)
        {
            commitExamplesWithIDs();
        }
        if (true)
        {
            commitExamplesWithLinks();
        }
        if (true)
        {
            //commitExampleImages();
        }
        if (true)
        {
            commitExamplesWithUntypedData();
        }
        if (true)
        {
            queryPredictionEngines();
        }

        CycleUtil c(static_cast<int>(1000 / p.commitFrequency));
        while (!task->isStopped())
        {
            commitSingleSnapshot(exampleEntityID);

            c.waitForCycleDuration();
        }
    }


    armem::MemoryID ExampleMemoryClient::addProviderSegment()
    {
        armem::data::AddSegmentInput input;
        input.coreSegmentName = "ExampleModality";
        input.providerSegmentName = "FancyMethodModality";

        ARMARX_IMPORTANT << input;
        armem::data::AddSegmentResult result = memoryWriter.addSegment(input);
        ARMARX_INFO << result;

        return armem::MemoryID(result.segmentID);
    }


    armem::MemoryID ExampleMemoryClient::commitSingleSnapshot(const armem::MemoryID& entityID)
    {
        std::default_random_engine gen(std::random_device{}());
        std::uniform_int_distribution<int> distrib(-20, 20);

        // Prepare the update with some empty instances.
        armem::EntityUpdate update;
        update.entityID = entityID;
        update.timeCreated = armem::Time::Now();

        double diff = (update.timeCreated - runStarted).toMilliSecondsDouble() / 1000;

        auto dict1 = std::make_shared<aron::data::Dict>();
        auto dict2 = std::make_shared<aron::data::Dict>();

        auto sin = std::make_shared<aron::data::Float>(std::sin(diff));
        auto cos = std::make_shared<aron::data::Float>(std::cos(diff));

        auto sqrt = std::make_shared<aron::data::Double>(std::sqrt(diff));
        auto lin = std::make_shared<aron::data::Long>(static_cast<long>(diff * 1000));
        auto rand = std::make_shared<aron::data::Int>(distrib(gen));

        dict1->addElement("sin", sin);
        dict1->addElement("cos", cos);

        dict2->addElement("sqrt", sqrt);
        dict2->addElement("lin", lin);
        dict2->addElement("rand", rand);

        update.instancesData =
        {
            dict1,
            dict2
        };

        ARMARX_IMPORTANT << "Committing " << update;
        armem::EntityUpdateResult updateResult = memoryWriter.commit(update);
        ARMARX_INFO << updateResult;
        if (!updateResult.success)
        {
            ARMARX_ERROR << updateResult.errorMessage;
        }

        return updateResult.snapshotID;
    }


    void ExampleMemoryClient::commitMultipleSnapshots(const armem::MemoryID& entityID, int num)
    {
        // Commit a number of updates with different timestamps and number of instances.
        armem::Commit commit;
        for (int i = 0; i < num; ++i)
        {
            armem::EntityUpdate& update = commit.add();
            update.entityID = entityID;
            update.timeCreated = armem::Time::Now() + armem::Duration::Seconds(i);
            for (int j = 0; j < i; ++j)
            {
                update.instancesData.push_back(std::make_shared<aron::data::Dict>());
            }
        }
        ARMARX_IMPORTANT << "Committing " << commit;
        armem::CommitResult commitResult = memoryWriter.commit(commit);
        ARMARX_INFO << commitResult;
        if (!commitResult.allSuccess())
        {
            ARMARX_ERROR << commitResult.allErrorMessages();
        }
    }


    void ExampleMemoryClient::queryLatestSnapshot(const armem::MemoryID& entityID)
    {
        ARMARX_IMPORTANT
                << "Querying latest snapshot: "
                << "\n- entityID:     \t'" << entityID << "'"
                ;

        armem::client::query::Builder builder;
        builder
        .coreSegments().withID(entityID)
        .providerSegments().withID(entityID)
        .entities().withID(entityID)
        .snapshots().latest();

        armem::client::QueryResult qResult = memoryReader.query(builder.buildQueryInput());
        ARMARX_INFO << qResult;
        if (qResult.success)
        {
            ARMARX_IMPORTANT << "Getting entity via ID";

            armem::wm::Memory& memory = qResult.memory;
            ARMARX_CHECK_GREATER_EQUAL(memory.size(), 1);

            const armem::wm::Entity* entity = memory.findEntity(entityID);
            ARMARX_CHECK_NOT_NULL(entity)
                    << "Entity " << entityID << " was not found in " << armem::print(memory);
            ARMARX_CHECK_GREATER_EQUAL(entity->size(), 1);

            const armem::wm::EntitySnapshot& snapshot = entity->getLatestSnapshot();
            ARMARX_CHECK_GREATER_EQUAL(snapshot.size(), 1);

            ARMARX_INFO << "Result: "
                        << "\n- entity:       \t" << entity->name()
                        << "\n- snapshot:     \t" << snapshot.time()
                        << "\n- #instances:   \t" << snapshot.size()
                        ;

            // Show memory contents in remote gui.
            tab.queryResult = std::move(memory);
            tab.rebuild = true;
        }
        else
        {
            ARMARX_ERROR << qResult.errorMessage;
        }
    }


    void ExampleMemoryClient::queryExactSnapshot(const armem::MemoryID& snapshotID)
    {
        ARMARX_IMPORTANT
                << "Querying exact snapshot: "
                << "\n- snapshotID:     \t'" << snapshotID << "'"
                ;

        namespace qf = armem::client::query_fns;
        armem::client::query::Builder qb;
        qb.singleEntitySnapshot(snapshotID);

        armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());
        ARMARX_INFO << qResult;

        if (qResult.success)
        {
            armem::wm::Memory memory = std::move(qResult.memory);
            const armem::wm::EntitySnapshot& entitySnapshot = memory.getEntity(snapshotID).getLatestSnapshot();

            ARMARX_INFO << "Result snapshot: "
                        << "\n- time:        \t" << entitySnapshot.time()
                        << "\n- # instances: \t" << entitySnapshot.size()
                        ;
        }
        else
        {
            ARMARX_ERROR << qResult.errorMessage;
        }
    }


    void ExampleMemoryClient::commitExampleData()
    {
        ARMARX_IMPORTANT << "Adding segment " << "ExampleData" << "/" << getName();

        auto addSegmentResult = memoryWriter.addSegment("ExampleData", getName());
        if (!addSegmentResult.success)
        {
            ARMARX_ERROR << addSegmentResult.errorMessage;
            return;
        }
        exampleDataProviderID = armem::MemoryID(addSegmentResult.segmentID);

        addSegmentResult = memoryWriter.addSegment("LinkedData", getName());
        if (!addSegmentResult.success)
        {
            ARMARX_ERROR << addSegmentResult.errorMessage;
            return;
        }
        linkedDataProviderID = armem::MemoryID(addSegmentResult.segmentID);

        const armem::Time time = armem::Time::Now();
        armem::Commit commit;

        //commit to default
        {
            armem::EntityUpdate& update = commit.add();
            update.entityID = exampleDataProviderID.withEntityName("default");
            update.timeCreated = time;

            armem::example::ExampleData data;
            toAron(data.memoryID, armem::MemoryID());
            toAron(data.memoryLink.memoryID, armem::MemoryID());
            ARMARX_CHECK_NOT_NULL(data.toAron());
            update.instancesData = { data.toAron() };
        }


        //commit to the answer
        {
            armem::EntityUpdate& update = commit.add();
            update.entityID = exampleDataProviderID.withEntityName("the answer");
            update.timeCreated = time;

            armem::example::ExampleData data;
            data.the_bool = true;
            data.the_double = std::sin(time.toDurationSinceEpoch().toSecondsDouble());
            data.the_float = 21.5;
            data.the_int = 42;
            data.the_long = 424242;
            data.the_string = "fourty two";
            data.the_float_list = { 21, 42, 84 };
            data.the_int_list = { 21, 42, 84 };
            data.the_string_list = simox::alg::multi_to_string(data.the_int_list);
            data.the_object_list.emplace_back();

            data.the_float_dict =
            {
                { "one", 1.0 },
                { "two", 2.0 },
                { "three", 3.0 },
            };
            data.the_int_dict =
            {
                { "one", 1 },
                { "two", 2 },
                { "three", 3 },
            };

            data.the_position = { 42, 24, 4224 };
            data.the_orientation = Eigen::AngleAxisf(1.57f, Eigen::Vector3f(1, 1, 1).normalized());
            data.the_pose = simox::math::pose(data.the_position, data.the_orientation);

            data.the_3x1_vector = { 24, 42, 2442 };
            data.the_4x4_matrix = 42 * Eigen::Matrix4f::Identity();

            toAron(data.memoryID, armem::MemoryID()); // ////1/1
            toAron(data.memoryLink.memoryID, armem::MemoryID());

            simox::ColorMap cmap = simox::color::cmaps::plasma();
            {
                cv::Mat& image = data.the_rgb24_image;
                image.create(10, 20, image.type());
                cmap.set_vlimits(0, float(image.cols + image.rows));
                using Pixel = cv::Point3_<uint8_t>;
                image.forEach<Pixel>([&cmap](Pixel& pixel, const int index[]) -> void
                {
                    simox::Color color = cmap(float(index[0] + index[1]));
                    pixel.x = color.r;
                    pixel.y = color.g;
                    pixel.z = color.b;
                });

                //cv::Mat out;
                //cv::cvtColor(image, out, /*CV_RGB2BGR*/);
                cv::imwrite("/tmp/the_rgb24_image.png", image); // out
            }
            {
                cv::Mat& image = data.the_depth32_image;
                image.create(20, 10, image.type());
                image.forEach<float>([&image](float& pixel, const int index[]) -> void
                {
                    pixel = 100 * float(index[0] + index[1]) / float(image.rows + image.cols);
                });

                cmap.set_vlimits(0, 100);
                cv::Mat rgb(image.rows, image.cols, CV_8UC3);
                using Pixel = cv::Point3_<uint8_t>;
                rgb.forEach<Pixel>([&image, &cmap](Pixel& pixel, const int index[]) -> void
                {
                    simox::Color color = cmap(image.at<float>(index));
                    pixel.x = color.r;
                    pixel.y = color.g;
                    pixel.z = color.b;
                });

                //cv::Mat out;
                //cv::cvtColor(rgb, out, CV_RGB2BGR);
                cv::imwrite("/tmp/the_depth32_image.png", image); // out
            }

            ARMARX_CHECK_NOT_NULL(data.toAron());
            update.instancesData = { data.toAron() };
        }

        // commit to linked data
        {
            armem::EntityUpdate& update = commit.add();
            update.entityID = linkedDataProviderID.withEntityName("yet_more_data");
            update.timeCreated = time;

            armem::example::LinkedData data;
            data.yet_another_int = 42;
            data.yet_another_string = "Hi! I'm from another core segment!";
            data.yet_another_object.element_int = 8349;
            data.yet_another_object.element_float = -1e3;
            data.yet_another_object.element_string = "I'm a nested object in some linked data.";
            ARMARX_CHECK_NOT_NULL(data.toAron());
            update.instancesData = { data.toAron() };
        }

        armem::CommitResult commitResult = memoryWriter.commit(commit);
        if (commitResult.allSuccess())
        {
            theAnswerSnapshotID = commitResult.results.at(1).snapshotID;
            yetMoreDataSnapshotID = commitResult.results.at(2).snapshotID;
        }
        else
        {
            ARMARX_WARNING << commitResult.allErrorMessages();
        }
    }


    void ExampleMemoryClient::queryExampleData()
    {
        // Query all entities from provider.
        armem::client::query::Builder qb;
        qb
        .coreSegments().withID(exampleProviderID)
        .providerSegments().withID(exampleProviderID)
        .entities().all()
        .snapshots().all();

        armem::client::QueryResult result = memoryReader.query(qb.buildQueryInput());
        if (result.success)
        {
            tab.queryResult = std::move(result.memory);
            tab.rebuild = true;
        }
        else
        {
            ARMARX_ERROR << result.errorMessage;
        }
    }


    void ExampleMemoryClient::commitExamplesWithIDs()
    {
        ARMARX_IMPORTANT << "Committing multiple entity updates with links ...";
        const armem::Time time = armem::Time::Now();

        armem::Commit commit;
        {
            armem::EntityUpdate& update = commit.add();
            update.entityID = exampleDataProviderID.withEntityName("id to the_answer");
            update.timeCreated = time;

            armem::example::ExampleData data;
            armem::toAron(data.memoryID, theAnswerSnapshotID);
            armem::toAron(data.memoryLink.memoryID, armem::MemoryID());

            update.instancesData = { data.toAron() };
        }
        {
            armem::EntityUpdate& update = commit.add();
            update.entityID = exampleDataProviderID.withEntityName("id to self");
            update.timeCreated = time;

            armem::example::ExampleData data;
            armem::toAron(data.memoryID, update.entityID.withTimestamp(time));
            armem::toAron(data.memoryLink.memoryID, armem::MemoryID());

            update.instancesData = { data.toAron() };
        }

        {
            armem::EntityUpdate& update = commit.add();
            update.entityID = exampleDataProviderID.withEntityName("id to previous snapshot");
            update.timeCreated = time - armem::Duration::Seconds(1);  // 1 sec in the past

            armem::example::ExampleData data;
            armem::toAron(data.memoryID, armem::MemoryID());  // First entry - invalid link
            armem::toAron(data.memoryLink.memoryID, armem::MemoryID());

            update.instancesData = { data.toAron() };
        }
        {
            armem::EntityUpdate& update = commit.add();
            update.entityID = exampleDataProviderID.withEntityName("id to previous snapshot");
            update.timeCreated = time;

            armem::example::ExampleData data;
            armem::toAron(data.memoryID, update.entityID.withTimestamp(time - armem::Duration::Seconds(1)));
            armem::toAron(data.memoryLink.memoryID, armem::MemoryID());

            update.instancesData = { data.toAron() };
        }

        ARMARX_CHECK_EQUAL(commit.updates.size(), 4);
        armem::CommitResult commitResult = memoryWriter.commit(commit);

        if (!commitResult.allSuccess() || commitResult.results.size() != commit.updates.size())
        {
            ARMARX_WARNING << commitResult.allErrorMessages();
        }


        // Resolve memory IDs via memory name system (works for IDs from different servers).
        ARMARX_IMPORTANT << "Resolving multiple memory IDs via Memory Name System:";
        {
            std::vector<armem::MemoryID> ids;
            for (armem::EntityUpdateResult& result : commitResult.results)
            {
                ids.push_back(result.snapshotID);
            }
            ARMARX_CHECK_EQUAL(ids.size(), commit.updates.size());

            std::map<armem::MemoryID, armem::wm::EntityInstance> instances =
                    memoryNameSystem().resolveEntityInstances(ids);
            ARMARX_CHECK_EQUAL(instances.size(), commit.updates.size());

            std::stringstream ss;
            for (const auto& [id, instance]: instances)
            {
                ss << "- Snapshot " << id << " "
                   << "\n--> Instance" << instance.id()
                   << " (# keys in data: " << instance.data()->childrenSize() << ")"
                   << "\n";
            }
            ARMARX_INFO << ss.str();
        }
    }


    void ExampleMemoryClient::commitExamplesWithLinks()
    {
        ARMARX_IMPORTANT << "Committing an entity update with a link...";

        const armem::Time time = armem::Time::Now();

        armem::Commit commit;
        {
            armem::EntityUpdate& update = commit.add();
            update.entityID = exampleDataProviderID.withEntityName("link to yet_more_data");
            update.timeCreated = time;

            armem::example::ExampleData data;
            armem::toAron(data.memoryID, armem::MemoryID());
            armem::toAron(data.memoryLink.memoryID, yetMoreDataSnapshotID);

            update.instancesData = { data.toAron() };
        }

        ARMARX_CHECK_EQUAL(commit.updates.size(), 1);
        armem::CommitResult commitResult = memoryWriter.commit(commit);

        if (!commitResult.allSuccess() || commitResult.results.size() != commit.updates.size())
        {
            ARMARX_WARNING << commitResult.allErrorMessages();
        }


        // Resolve memory IDs via memory name system (works for IDs from different servers).
        ARMARX_IMPORTANT << "Resolving multiple memory IDs via Memory Name System:";
        {
            std::vector<armem::MemoryID> ids;
            for (armem::EntityUpdateResult& result : commitResult.results)
            {
                ids.push_back(result.snapshotID);
            }
            ARMARX_CHECK_EQUAL(ids.size(), commit.updates.size());

            std::map<armem::MemoryID, armem::wm::EntityInstance> instances =
                    memoryNameSystem().resolveEntityInstances(ids);
            ARMARX_CHECK_EQUAL(instances.size(), commit.updates.size());

            std::stringstream ss;
            for (const auto& [id, instance]: instances)
            {
                ss << "- Snapshot " << id << " "
                   << "\n--> Instance" << instance.id()
                   << " (# keys in data: " << instance.data()->childrenSize() << ")"
                   << "\n";
            }
            ARMARX_INFO << ss.str();
        }
    }

    void ExampleMemoryClient::commitExampleImages()
    {
        const armem::Time time = armem::Time::Now();

        armem::Commit commit;
        {
            armem::EntityUpdate& update = commit.add();
            update.entityID = exampleDataProviderID.withEntityName("some_new_fancy_entity_id");
            update.timeCreated = time;

            auto currentFolder = std::filesystem::current_path();
            auto opencv_img = cv::imread((currentFolder / "images" / (std::to_string(imageCounter + 1) + ".jpg")).string());
            imageCounter++;
            imageCounter %= 10;

            auto data = std::make_shared<aron::data::Dict>();
            data->addElement("opencv_image", aron::converter::AronOpenCVConverter::ConvertFromMat(opencv_img));

            update.instancesData = { data };
        }
    }

    void ExampleMemoryClient::commitExamplesWithUntypedData()
    {
        const armem::Time time = armem::Time::Now();

        armem::Commit commit;
        {
            armem::EntityUpdate& update = commit.add();
            update.entityID = exampleDataProviderID.withEntityName("unexpected_data");
            update.timeCreated = time;

            armem::example::ExampleData data;
            toAron(data.memoryID, armem::MemoryID()); // ////1/1
            toAron(data.memoryLink.memoryID, armem::MemoryID());

            aron::data::DictPtr aron = data.toAron();
            aron->addElement("unexpectedString", std::make_shared<aron::data::String>("unexpected value"));
            aron->addElement("unexpectedDict", std::make_shared<aron::data::Dict>(
                                 std::map<std::string, aron::data::VariantPtr>{
                                    { "key43", std::make_shared<aron::data::Int>(43) },
                                    { "keyABC", std::make_shared<aron::data::String>("ABC") },
                                 }
                                 ));
            update.instancesData = { aron };
        }

        armem::CommitResult commitResult = memoryWriter.commit(commit);
        if (!commitResult.allSuccess())
        {
            ARMARX_WARNING << commitResult.allErrorMessages();
        }
    }


    void ExampleMemoryClient::queryPredictionEngines()
    {
        const std::map<armem::MemoryID, std::vector<armem::PredictionEngine>> predictionEngines =
                memoryReader.getAvailablePredictionEngines();

        std::stringstream ss;
        ss << "Prediction engines available in the server:" << std::endl;
        for (const auto& [id, engines] : predictionEngines)
        {
            ss << " - " << id << ": ";
            for (const armem::PredictionEngine& engine : engines)
            {
                ss << engine.engineID << ", ";
            }
            ss << std::endl;
        }

        ARMARX_INFO << ss.str();
    }


    void ExampleMemoryClient::processExampleEntityUpdate(
        const armem::MemoryID& subscriptionID, const std::vector<armem::MemoryID>& snapshotIDs)
    {
        std::stringstream ss;
        ss << "example_entity got updated: " << subscriptionID << "\n";
        ss << "Updated snapshots: \n";
        for (const auto& id : snapshotIDs)
        {
            ss << "- " << id << "\n";
        }
        ARMARX_IMPORTANT << ss.str();
        // Fetch new data of example_entity and do something with it.
    }


    void ExampleMemoryClient::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        if (tab.queryResult)
        {
        }

        VBoxLayout root = {tab.queryResultGroup, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }

    void ExampleMemoryClient::RemoteGui_update()
    {
        if (tab.rebuild.exchange(false))
        {
            createRemoteGuiTab();
        }
    }

}
