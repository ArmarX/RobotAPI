
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore KITProstheticHandUnit)
 
armarx_add_test(KITProstheticHandUnitTest KITProstheticHandUnitTest.cpp "${LIBS}")