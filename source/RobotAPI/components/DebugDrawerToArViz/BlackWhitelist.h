#pragma once

#include <set>
#include <string>
#include <ostream>


namespace armarx
{

    /**
     * @brief A combination of blacklist and whitelist.
     *
     * An element is included if
     * (1) it is not in the blacklist, and
     * (2) the whitelist is empty or it contains the element.
     */
    template <typename Key>
    class BlackWhitelist
    {
    public:

        /// Construct an empty blacklist and whitelist.
        BlackWhitelist() = default;


        /**
         * An element is included if
         * (1) it is not in the blacklist, and
         * (2) the whitelist is empty or it contains the element.
         */
        bool isIncluded(const Key& element) const
        {
            return !isExcluded(element);
        }

        /**
         * An element is excluded if
         * (1) it is in the blacklist, or
         * (2) it is not in the non-empty whitelist
         */
        bool isExcluded(const Key& element) const
        {
            return black.count(element) > 0 || (!white.empty() && white.count(element) == 0);
        }


        /// Elements in this list are always excluded.
        std::set<Key> black;
        /// If not empty, only these elements are included.
        std::set<Key> white;

        template <class K>
        friend std::ostream& operator<< (std::ostream& os, const BlackWhitelist<K>& bw);
    };


    template <class K>
    std::ostream& operator<< (std::ostream& os, const BlackWhitelist<K>& bw)
    {
        os << "Blacklist (" << bw.black.size() << "): ";
        for (const auto& e : bw.black)
        {
            os << "\n\t" << e;
        }
        os << "\n";

        os << "Whitelist (" << bw.white.size() << "):";
        for (const auto& e : bw.white)
        {
            os << "\n\t" << e;
        }
        return os;
    }


    using StringBlackWhitelist = BlackWhitelist<std::string>;

}

