#include "BlackWhitelistUpdate.h"


void armarx::updateStringList(std::set<std::string>& list, const StringListUpdate& update)
{
    if (update.clear)
    {
        list.clear();
    }
    else if (!update.add.empty())
    {
        list.insert(update.add.begin(), update.add.end());
    }
    else if (!update.set.empty())
    {
        list = { update.set.begin(), update.set.end() };
    }
}
