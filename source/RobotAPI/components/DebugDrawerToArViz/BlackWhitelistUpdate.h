#pragma once

#include "BlackWhitelist.h"

#include <RobotAPI/interface/core/BlackWhitelist.h>


namespace armarx
{

    void updateStringList(std::set<std::string>& list, const StringListUpdate& update);


    inline
    void updateBlackWhitelist(StringBlackWhitelist& bw, const armarx::BlackWhitelistUpdate& update)
    {
        updateStringList(bw.black, update.blacklist);
        updateStringList(bw.white, update.whitelist);
    }

}

