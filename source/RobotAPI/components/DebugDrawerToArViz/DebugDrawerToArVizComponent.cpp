/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::DebugDrawerToArViz
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DebugDrawerToArVizComponent.h"

#include "BlackWhitelistUpdate.h"


namespace armarx
{
    DebugDrawerToArVizPropertyDefinitions::DebugDrawerToArVizPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("DebugDrawerTopicName", "DebugDrawerUpdates",
                                            "Name of the topic the DebugDrawer listens to.");


        defineOptionalProperty<std::string>(
            "LayerBlackWhitelistTopic", "DebugDrawerToArVizLayerBlackWhitelistUpdates",
            "The layer where updates to the layer black-whitelist are published.");
        defineOptionalProperty<std::vector<std::string>>("LayerWhitelist", {},
                "If not empty, layers are shown (comma separated list).")
                .map("[empty whitelist]", {});
        defineOptionalProperty<std::vector<std::string>>("LayerBlacklist", {},
                "These layers will never be shown (comma separated list).")
                .map("[empty blacklist]", {});
    }


    std::string DebugDrawerToArVizComponent::getDefaultName() const
    {
        return "DebugDrawerToArViz";
    }


    void DebugDrawerToArVizComponent::onInitComponent()
    {
        {
            BlackWhitelistUpdate update;
            getProperty(update.whitelist.set, "LayerWhitelist");
            getProperty(update.blacklist.set, "LayerBlacklist");
            armarx::updateBlackWhitelist(DebugDrawerToArViz::layerBlackWhitelist, update);
            ARMARX_VERBOSE << "Layer black-white-list: \n" << DebugDrawerToArViz::layerBlackWhitelist;
        }

        usingTopicFromProperty("DebugDrawerTopicName");
        usingTopicFromProperty("LayerBlackWhitelistTopic");
    }


    void DebugDrawerToArVizComponent::onConnectComponent()
    {
        DebugDrawerToArViz::setArViz(ArVizComponentPluginUser::arviz);
    }


    void DebugDrawerToArVizComponent::onDisconnectComponent()
    {
    }


    void DebugDrawerToArVizComponent::onExitComponent()
    {
    }


    armarx::PropertyDefinitionsPtr DebugDrawerToArVizComponent::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new DebugDrawerToArVizPropertyDefinitions(getConfigIdentifier()));
    }
}
