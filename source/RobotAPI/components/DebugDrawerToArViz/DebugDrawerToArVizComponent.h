/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::DebugDrawerToArViz
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/visualization/DebugDrawerToArViz.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include "DebugDrawerToArViz.h"


namespace armarx
{
    /**
     * @class DebugDrawerToArVizPropertyDefinitions
     * @brief Property definitions of `DebugDrawerToArViz`.
     */
    class DebugDrawerToArVizPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DebugDrawerToArVizPropertyDefinitions(std::string prefix);
    };


    /**
     * @defgroup Component-DebugDrawerToArViz DebugDrawerToArViz
     * @ingroup RobotAPI-Components
     * A description of the component DebugDrawerToArViz.
     *
     * @class DebugDrawerToArViz
     * @ingroup Component-DebugDrawerToArViz
     * @brief Brief description of class DebugDrawerToArViz.
     *
     * Detailed description of class DebugDrawerToArViz.
     */
    class DebugDrawerToArVizComponent :
        virtual public armarx::Component,
        virtual public armarx::DebugDrawerToArViz,  // Implements armarx::DebugDrawerToArvizInterface
        virtual public armarx::ArVizComponentPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    };
}
