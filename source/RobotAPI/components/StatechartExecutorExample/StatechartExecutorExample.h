/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXGui::ArmarXObjects::StatechartExecutorExample
 * @author     Stefan Reither ( stefan dot reither at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>

#include <ArmarXCore/interface/components/SimpleStatechartExecutorInterface.h>


namespace armarx
{
    /**
     * @class StatechartExecutorExamplePropertyDefinitions
     * @brief
     */
    class StatechartExecutorExamplePropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        StatechartExecutorExamplePropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-StatechartExecutorExample StatechartExecutorExample
     * @ingroup ArmarXGui-Components
     * A description of the component StatechartExecutorExample.
     *
     * @class StatechartExecutorExample
     * @ingroup Component-StatechartExecutorExample
     * @brief Brief description of class StatechartExecutorExample.
     *
     * Detailed description of class StatechartExecutorExample.
     */
    class StatechartExecutorExample :
        virtual public armarx::Component
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        virtual void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        virtual void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        virtual void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        virtual void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:

        SimpleStatechartExecutorInterfacePrx _statechartExecutor;

        void setupRemoteGuiWidget();
        RemoteGuiInterfacePrx _remoteGuiPrx;
        RemoteGui::TabProxy _remoteGuiTab;
        std::string _tabName;
        void runRemoteGui();
        RunningTask<StatechartExecutorExample>::pointer_type _remoteGuiTask;

    };
}
