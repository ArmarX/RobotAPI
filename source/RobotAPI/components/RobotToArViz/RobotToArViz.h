/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotToArViz
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>


namespace armarx
{
    /**
     * @class RobotToArVizPropertyDefinitions
     * @brief Property definitions of `RobotToArViz`.
     */
    class RobotToArVizPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        RobotToArVizPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-RobotToArViz RobotToArViz
     * @ingroup RobotAPI-Components
     *
     * Visualizes a robot via ArViz.
     *
     * @class RobotToArViz
     * @ingroup Component-RobotToArViz
     * @brief Brief description of class RobotToArViz.
     *
     * Detailed description of class RobotToArViz.
     */
    class RobotToArViz :
        virtual public armarx::Component,
        virtual public armarx::ArVizComponentPluginUser,
        virtual public armarx::RobotStateComponentPluginUser,
        virtual public armarx::LightweightRemoteGuiComponentPluginUser
    {
        using RobotState = RobotStateComponentPluginUser;

    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


        void createRemoteGuiTab();
        void RemoteGui_update() override;


    private:

        void updateRobot();
        static bool trySetFilePathFromDataDir(viz::Robot& robotViz, const std::string& absolutePath);


    private:

        float updateFrequency = 100;
        SimplePeriodicTask<>::pointer_type task;

        std::string robotName = "robot";

        VirtualRobot::RobotPtr robot;

        viz::Robot robotViz { "" };


        // Remote Gui

        struct Tab : RemoteGui::Client::Tab
        {
            RemoteGui::Client::CheckBox showRobotNodeFrames;
            RemoteGui::Client::CheckBox useCollisionModel;
            // Todo: add spin box for scale
        };
        Tab tab;

        struct Gui
        {
            bool showRobotNodeFrames = false;
            bool useCollisionModel = false;
        };
        std::mutex guiMutex;
        Gui gui;

    };
}
