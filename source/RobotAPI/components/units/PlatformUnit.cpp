/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PlatformUnit.h"

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <SimoxUtility/math/convert/mat4f_to_rpy.h>


namespace armarx
{
    PropertyDefinitionsPtr PlatformUnit::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = new PlatformUnitPropertyDefinitions(getConfigIdentifier());

        def->topic(odometryPrx);
        def->topic(globalPosePrx);
        // def->topic(listenerPrx, listenerChannelName, "");

        def->component(robotStateComponent);

        return def;
    }


    void PlatformUnit::onInitComponent()
    {
        std::string platformName = getProperty<std::string>("PlatformName").getValue();

        listenerChannelName = platformName + "State";
        offeringTopic(listenerChannelName);

        this->onInitPlatformUnit();
    }


    void PlatformUnit::onConnectComponent()
    {
                listenerPrx = getTopic<PlatformUnitListenerPrx>(listenerChannelName);

        this->onStartPlatformUnit();
    }


    void PlatformUnit::onDisconnectComponent()
    {
        this->onStopPlatformUnit();
    }


    void PlatformUnit::onExitComponent()
    {
        this->onExitPlatformUnit();
    }


    void PlatformUnit::moveTo(Ice::Float targetPlatformPositionX, Ice::Float targetPlatformPositionY, Ice::Float targetPlatformRotation, Ice::Float positionalAccuracy, Ice::Float orientationalAccuracy, const Ice::Current& c)
    {
    }

    PlatformPose toPlatformPose(const TransformStamped& transformStamped)
    {
        const float yaw = simox::math::mat4f_to_rpy(transformStamped.transform).z();
        const Eigen::Affine3f pose(transformStamped.transform);

        PlatformPose platformPose;
        platformPose.x = pose.translation().x();
        platformPose.y = pose.translation().y();
        platformPose.rotationAroundZ = yaw;
        platformPose.timestampInMicroSeconds = transformStamped.header.timestampInMicroSeconds;

        return platformPose;
    }

} // namespace armarx
