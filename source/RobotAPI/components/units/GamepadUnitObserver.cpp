/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "GamepadUnitObserver.h"


#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>
#include <ArmarXCore/observers/checks/ConditionCheckUpdated.h>

#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/observers/variant/TimestampVariant.h>

namespace armarx
{
    void GamepadUnitObserver::onInitObserver()
    {
        usingTopic(getProperty<std::string>("GamepadTopicName").getValue());

        offerConditionCheck("updated", new ConditionCheckUpdated());
        offerConditionCheck("larger", new ConditionCheckLarger());
        offerConditionCheck("equals", new ConditionCheckEquals());
        offerConditionCheck("smaller", new ConditionCheckSmaller());

        offeringTopic(getProperty<std::string>("DebugDrawerTopic").getValue());
    }



    void GamepadUnitObserver::onConnectObserver()
    {
        debugDrawerPrx = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopic").getValue());
    }


    void GamepadUnitObserver::onExitObserver()
    {
        debugDrawerPrx->removePoseVisu("IMU", "orientation");
        debugDrawerPrx->removeLineVisu("IMU", "acceleration");
    }


    PropertyDefinitionsPtr GamepadUnitObserver::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new GamepadUnitObserverPropertyDefinitions(getConfigIdentifier()));
    }

    void GamepadUnitObserver::reportGamepadState(const std::string& device, const std::string& name, const GamepadData& data, const TimestampBasePtr& timestamp, const Ice::Current& c)
    {
        std::unique_lock lock(dataMutex);

        TimestampVariantPtr timestampPtr = TimestampVariantPtr::dynamicCast(timestamp);

        if (!existsChannel(device))
        {
            offerChannel(device, "Gamepad data");
        }

        //ARMARX_IMPORTANT << deactivateSpam(1) << "observed " << device << " with name " << name;

        //axis
        offerOrUpdateDataField(device, "leftStickX", Variant(data.leftStickX), "X value of the left analog stick");
        offerOrUpdateDataField(device, "leftStickY", Variant(data.leftStickY), "Y value of the left analog stick");
        offerOrUpdateDataField(device, "rightStickX", Variant(data.rightStickX), "X value of the right analog stick");
        offerOrUpdateDataField(device, "rightStickY", Variant(data.rightStickY), "Y value of the right analog stick");
        offerOrUpdateDataField(device, "dPadX", Variant(data.dPadX), "X value of the digital pad");
        offerOrUpdateDataField(device, "dPadY", Variant(data.dPadY), "y value of the digital pad");
        offerOrUpdateDataField(device, "leftTrigger", Variant(data.leftTrigger), "value of the left analog trigger");
        offerOrUpdateDataField(device, "rightTrigger", Variant(data.rightTrigger), "value of the right analog trigger");
        //buttons
        offerOrUpdateDataField(device, "aButton", Variant(data.aButton), "A button pressed");
        offerOrUpdateDataField(device, "backButton", Variant(data.backButton), "Back button pressed");
        offerOrUpdateDataField(device, "bButton", Variant(data.bButton), "B button pressed");
        offerOrUpdateDataField(device, "leftButton", Variant(data.leftButton), "Left shoulder button pressed");
        offerOrUpdateDataField(device, "leftStickButton", Variant(data.leftStickButton), "Left stick button pressed");
        offerOrUpdateDataField(device, "rightButton", Variant(data.rightButton), "Right shoulder button pressed");
        offerOrUpdateDataField(device, "rightStickButton", Variant(data.rightStickButton), "Right stick button pressed");
        offerOrUpdateDataField(device, "startButton", Variant(data.startButton), "Start button pressed");
        offerOrUpdateDataField(device, "theMiddleButton", Variant(data.theMiddleButton), "The middle button pressed");
        offerOrUpdateDataField(device, "xButton", Variant(data.xButton), "X button pressed");
        offerOrUpdateDataField(device, "yButton", Variant(data.yButton), "Y button pressed");

        updateChannel(device);
    }
}

