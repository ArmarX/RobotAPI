/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     David Schiebener <schiebener at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/OrientedTactileSensorUnit.h>
#include <ArmarXCore/observers/Observer.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/Pose.h>

#include <mutex>

namespace armarx
{
    /**
     * \class OrientedTactileSensorUnitObserverPropertyDefinitions
     * \brief
     */
    class OrientedTactileSensorUnitObserverPropertyDefinitions:
        public ObserverPropertyDefinitions
    {
    public:
        OrientedTactileSensorUnitObserverPropertyDefinitions(std::string prefix):
            ObserverPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("SensorTopicName", "OrientedTactileSensorValues", "Name of the Sensor Topic.");
            defineOptionalProperty<std::string>("DebugDrawerTopic", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
        }
    };


    /**
     * \class OrientedTactileSensorUnitObserver
     * \ingroup RobotAPI-SensorActorUnits-observers
     * \brief Observer monitoring @IMU sensor values
     *
     * The OrientedTactileSensorUnitObserver monitors @IMU sensor values published by OrientedTactileSensorUnit-implementations and offers condition checks on these values.
     */
    class OrientedTactileSensorUnitObserver :
        virtual public Observer,
        virtual public OrientedTactileSensorUnitObserverInterface
    {
    public:
        OrientedTactileSensorUnitObserver() {}

        std::string getDefaultName() const override
        {
            return "OrientedTactileSensorUnitObserver";
        }
        void onInitObserver() override;
        void onConnectObserver() override;
        void onExitObserver() override;

        void reportSensorValues(int id, float pressure, float qw, float qx, float qy, float qz, float pressureRate, float rotationRate, float accelerationRate, float accelx, float accely, float accelz, const TimestampBasePtr& timestamp, const Ice::Current&) override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        std::mutex dataMutex;
        DebugDrawerInterfacePrx debugDrawerPrx;
    };
}

