/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2013-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "HandUnitSimulation.h"

#include <Eigen/Geometry>

#include <cmath>

#include <VirtualRobot/EndEffector/EndEffector.h>
#include <VirtualRobot/RobotConfig.h>

namespace armarx
{
    void HandUnitSimulation::onInitHandUnit()
    {
        kinematicUnitName = getProperty<std::string>("KinematicUnitName").getValue();
        usingProxy(kinematicUnitName);
    }

    void HandUnitSimulation::onStartHandUnit()
    {
        kinematicUnitPrx = getProxy<KinematicUnitInterfacePrx>(kinematicUnitName);
        if (!kinematicUnitPrx)
        {
            ARMARX_ERROR << "Failed to obtain kinematic unit proxy";
        }
    }

    void HandUnitSimulation::onExitHandUnit()
    {
    }

    PropertyDefinitionsPtr HandUnitSimulation::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new HandUnitSimulationPropertyDefinitions(getConfigIdentifier()));
    }

    void armarx::HandUnitSimulation::setShape(const std::string& shapeName, const Ice::Current&)
    {
        std::string myShapeName = shapeName;
        ARMARX_INFO << "Setting shape " << myShapeName;

        if (!eef)
        {
            ARMARX_WARNING << "No EEF";
            return;
        }


        if (!eef->hasPreshape(myShapeName))
        {
            ARMARX_INFO << "Shape with name " << myShapeName << " not known in eef " << eef->getName() << ". Looking for partial match";

            bool foundMatch = false;

            for (std::string name : eef->getPreshapes())
            {
                if (name.find(myShapeName) != std::string::npos)
                {
                    foundMatch = true;
                    myShapeName = name;
                    ARMARX_INFO << "Using matching shape: " << name;
                    break;
                }
            }

            if (!foundMatch)
            {
                ARMARX_WARNING << "No match found for " << myShapeName << " in eef " << eef->getName() << " available shapes: " << eef->getPreshapes();
                return;
            }
        }

        VirtualRobot::RobotConfigPtr config = eef->getPreshape(myShapeName);
        std::map < std::string, float > jointAngles = config->getRobotNodeJointValueMap();

        NameControlModeMap controlModes;

        for (std::pair<std::string, float> pair : jointAngles)
        {
            controlModes.insert(std::make_pair(pair.first, ePositionControl));
        }

        kinematicUnitPrx->switchControlMode(controlModes);
        kinematicUnitPrx->setJointAngles(jointAngles);
    }

    void armarx::HandUnitSimulation::setJointAngles(const NameValueMap& jointAngles, const Ice::Current&)
    {
        NameControlModeMap controlModes;

        for (std::pair<std::string, float> pair : jointAngles)
        {
            controlModes.insert(std::make_pair(pair.first, ePositionControl));
        }

        kinematicUnitPrx->switchControlMode(controlModes);
        kinematicUnitPrx->setJointAngles(jointAngles);
    }
}
