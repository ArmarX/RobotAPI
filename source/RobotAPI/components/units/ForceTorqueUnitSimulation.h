/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "ForceTorqueUnit.h"

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <IceUtil/Time.h>

#include <string>

namespace armarx
{
    /**
     * \class ForceTorqueUnitSimulationPropertyDefinitions
     * \brief
     */
    class ForceTorqueUnitSimulationPropertyDefinitions :
        public ForceTorqueUnitPropertyDefinitions
    {
    public:
        ForceTorqueUnitSimulationPropertyDefinitions(std::string prefix) :
            ForceTorqueUnitPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("SensorNames", "simulated sensor name. seperated by comma");
            defineRequiredProperty<std::string>("AgentName", "name of the robot agent");
            defineOptionalProperty<int>("IntervalMs", 50, "The time in milliseconds between two calls to the simulation method.");
        }
    };

    /**
     * \class ForceTorqueUnitSimulation
     * \brief Simulates a set of Force/Torque sensors.
     * \ingroup RobotAPI-SensorActorUnits-simulation
     *
     * The unit is given a list of sensor names as a property. It then publishes force/torque values under these names.
     * The published values will always be zero.
     */
    class ForceTorqueUnitSimulation :
        virtual public ForceTorqueUnit
    {
    public:
        std::string getDefaultName() const override
        {
            return "ForceTorqueUnitSimulation";
        }

        void onInitForceTorqueUnit() override;
        void onStartForceTorqueUnit() override;
        void onExitForceTorqueUnit() override;

        void simulationFunction();

        /**
         * \warning Not implemented yet
         */
        void setOffset(const FramedDirectionBasePtr& forceOffsets, const FramedDirectionBasePtr& torqueOffsets, const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * \warning Not implemented yet
         */
        void setToNull(const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * \see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        std::map<std::string, armarx::FramedDirectionPtr> forces;
        std::map<std::string, armarx::FramedDirectionPtr> torques;
        Ice::StringSeq sensorNamesList;
        PeriodicTask<ForceTorqueUnitSimulation>::pointer_type simulationTask;
    };
}

