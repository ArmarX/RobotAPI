/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     David Schiebener <schiebener at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/MetaWearIMU.h>
#include <ArmarXCore/observers/Observer.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/Pose.h>

#include <mutex>

namespace armarx
{
    /**
     * \class MetaWearIMUObserverPropertyDefinitions
     * \brief
     */
    class MetaWearIMUObserverPropertyDefinitions:
        public ObserverPropertyDefinitions
    {
    public:
        MetaWearIMUObserverPropertyDefinitions(std::string prefix):
            ObserverPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("MetaWearTopicName", "MetaWearData", "Name of the MetaWear topic");
            defineOptionalProperty<std::string>("DebugDrawerTopic", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
        }
    };



    class MetaWearIMUObserver :
        virtual public Observer,
        virtual public MetaWearIMUObserverInterface
    {
    public:
        MetaWearIMUObserver() {}

        std::string getDefaultName() const override
        {
            return "MetaWearIMUObserver";
        }
        void onInitObserver() override;
        void onConnectObserver() override;
        void onExitObserver() override;

        void reportIMUValues(const std::string& name, const MetaWearIMUData& data, const TimestampBasePtr& timestamp, const Ice::Current&) override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        std::mutex dataMutex;
        DebugDrawerInterfacePrx debugDrawerPrx;

        void offerVector3(const std::string& channelName, const std::string& dfName, const std::vector<float>& data);
        void offerQuaternion(const std::string& channelName, const std::string& dfName, const std::vector<float>& data);

    };
}

