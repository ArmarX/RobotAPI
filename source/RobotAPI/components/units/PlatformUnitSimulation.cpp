/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Christian Boege (boege at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PlatformUnitSimulation.h"
#include <RobotAPI/components/units/PlatformUnit.h>

#include <RobotAPI/interface/core/GeometryBase.h>
#include <cmath>

#include <Eigen/Geometry>

#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/interface/units/PlatformUnitInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <VirtualRobot/MathTools.h>

namespace armarx
{
    PropertyDefinitionsPtr PlatformUnitSimulation::createPropertyDefinitions()
    {
        auto def = PlatformUnit::createPropertyDefinitions();

        def->defineOptionalProperty<int>("IntervalMs", 10, "The time in milliseconds between two calls to the simulation method.");
        def->defineOptionalProperty<float>("InitialRotation", 0, "Initial rotation of the platform.");
        def->defineOptionalProperty<float>("InitialPosition.x", 0, "Initial x position of the platform.");
        def->defineOptionalProperty<float>("InitialPosition.y", 0, "Initial y position of the platform.");
        def->defineOptionalProperty<std::string>("ReferenceFrame", "Platform", "Reference frame in which the platform position is reported.");
        def->defineOptionalProperty<float>("LinearVelocity", 1000.0, "Linear velocity of the platform (default: 1000 mm/sec).");
        def->defineOptionalProperty<float>("MaxLinearAcceleration", 1000.0, "Linear acceleration of the platform (default: 1000 mm/sec).");
        def->defineOptionalProperty<float>("Kp_Position", 5.0, "P value of the PID position controller");
        def->defineOptionalProperty<float>("Kp_Velocity", 5.0, "P value of the PID velocity controller");
        def->defineOptionalProperty<float>("AngularVelocity", 1.5, "Angular velocity of the platform (default: 1.5 rad/sec).");

        def->component(robotStateComponent);

        return def;
    }

    void PlatformUnitSimulation::onInitPlatformUnit()
    {
        platformMode = eUndefined;
        referenceFrame = getProperty<std::string>("ReferenceFrame").getValue();
        targetPositionX = currentPositionX = getProperty<float>("InitialPosition.x").getValue();
        targetPositionY = currentPositionY = getProperty<float>("InitialPosition.y").getValue();
        targetRotation = 0.0;
        targetRotation = currentRotation = getProperty<float>("InitialRotation").getValue();

        maxLinearVelocity = getProperty<float>("LinearVelocity").getValue();
        maxAngularVelocity = getProperty<float>("AngularVelocity").getValue();

        velPID.reset(new MultiDimPIDController(getProperty<float>("Kp_Velocity").getValue(), 0, 0, getProperty<float>("MaxLinearAcceleration").getValue()));
        posPID.reset(new MultiDimPIDController(getProperty<float>("Kp_Position").getValue(), 0, 0, maxLinearVelocity, getProperty<float>("MaxLinearAcceleration").getValue()));


        positionalAccuracy = 0.01;

        intervalMs = getProperty<int>("IntervalMs").getValue();
        ARMARX_VERBOSE << "Starting platform unit simulation with " << intervalMs << " ms interval";
        simulationTask = new PeriodicTask<PlatformUnitSimulation>(this, &PlatformUnitSimulation::simulationFunction, intervalMs, false, "PlatformUnitSimulation");

    }

    Eigen::Matrix4f PlatformUnitSimulation::currentPlatformPose() const
    {
        return VirtualRobot::MathTools::posrpy2eigen4f(currentPositionX, currentPositionY, 0, 0, 0, currentRotation);
    }

    void PlatformUnitSimulation::onStartPlatformUnit()
    {
        agentName = robotStateComponent->getRobotName();
        robotRootFrame = robotStateComponent->getSynchronizedRobot()->getRootNode()->getName();

        TransformStamped currentPose;
        currentPose.header.parentFrame = GlobalFrame;
        currentPose.header.frame = robotRootFrame;
        currentPose.header.agent = agentName;
        currentPose.header.timestampInMicroSeconds = TimeUtil::GetTime().toMicroSeconds();
        currentPose.transform = currentPlatformPose();

        globalPosePrx->reportGlobalRobotPose(currentPose);

        // legacy
        listenerPrx->reportPlatformPose(toPlatformPose(currentPose));
        simulationTask->start();
    }

    void PlatformUnitSimulation::onStopPlatformUnit()
    {
        if (simulationTask)
        {
            simulationTask->stop();
        }
    }


    void PlatformUnitSimulation::onExitPlatformUnit()
    {
        if (simulationTask)
        {
            simulationTask->stop();
        }
    }

    void PlatformUnitSimulation::simulationFunction()
    {
        // the time it took until this method was called again
        auto now = TimeUtil::GetTime();
        double timeDeltaInSeconds = (now - lastExecutionTime).toSecondsDouble();
        lastExecutionTime = now;
        std::vector<float> vel(3, 0.0f);
        {
            std::unique_lock lock(currentPoseMutex);
            switch (platformMode)
            {
                case ePositionControl:
                {
                    posPID->update(timeDeltaInSeconds,
                                   Eigen::Vector2f(currentPositionX, currentPositionY),
                                   Eigen::Vector2f(targetPositionX, targetPositionY));
                    float newXTickMotion = posPID->getControlValue()[0] * timeDeltaInSeconds;
                    currentPositionX += newXTickMotion;
                    currentPositionY += posPID->getControlValue()[1] * timeDeltaInSeconds;
                    vel[0] = posPID->getControlValue()[0];
                    vel[1] = posPID->getControlValue()[1];

                    float diff = fabs(targetRotation - currentRotation);

                    if (diff > orientationalAccuracy)
                    {
                        int sign = copysignf(1.0f, (targetRotation - currentRotation));
                        currentRotation += sign * std::min<float>(maxAngularVelocity * timeDeltaInSeconds, diff);

                        // stay between +/- M_2_PI
                        if (currentRotation > 2 * M_PI)
                        {
                            currentRotation -= 2 * M_PI;
                        }

                        if (currentRotation < -2 * M_PI)
                        {
                            currentRotation += 2 * M_PI;
                        }

                        vel[2] = angularVelocity;

                    }
                }
                break;
                case eVelocityControl:
                {
                    Eigen::Vector2f targetVel(linearVelocityX, linearVelocityY);
                    Eigen::Rotation2Df rot(currentRotation);
                    targetVel = rot * targetVel;
                    velPID->update(timeDeltaInSeconds, currentTranslationVelocity, targetVel);
                    currentTranslationVelocity = timeDeltaInSeconds * velPID->getControlValue();
                    currentPositionX += timeDeltaInSeconds * currentTranslationVelocity[0];
                    currentPositionY += timeDeltaInSeconds * currentTranslationVelocity[1];


                    currentRotation += angularVelocity * timeDeltaInSeconds;

                    // stay between +/- M_2_PI
                    if (currentRotation > 2 * M_PI)
                    {
                        currentRotation -= 2 * M_PI;
                    }

                    if (currentRotation < -2 * M_PI)
                    {
                        currentRotation += 2 * M_PI;
                    }
                    vel[0] = currentTranslationVelocity[0];
                    vel[1] = currentTranslationVelocity[1];
                    vel[2] = angularVelocity;
                }
                break;
                default:
                    break;
            }
        }

        // odom velocity is in local robot frame
        FrameHeader odomVelocityHeader;
        odomVelocityHeader.parentFrame = robotRootFrame;
        odomVelocityHeader.frame = robotRootFrame;
        odomVelocityHeader.agent = agentName;
        odomVelocityHeader.timestampInMicroSeconds = TimeUtil::GetTime().toMicroSeconds();;

        // odom pose is in odom frame
        FrameHeader odomPoseHeader;
        odomPoseHeader.parentFrame = OdometryFrame;
        odomPoseHeader.frame = robotRootFrame;
        odomPoseHeader.agent = agentName;
        odomPoseHeader.timestampInMicroSeconds = TimeUtil::GetTime().toMicroSeconds();;

        TransformStamped platformPose;
        platformPose.header = odomPoseHeader;
        platformPose.transform = currentPlatformPose();

        TwistStamped platformVelocity;
        platformVelocity.header = odomVelocityHeader;
        platformVelocity.twist.linear << vel[0], vel[1], 0;
        platformVelocity.twist.angular << 0, 0, vel[2];

        odometryPrx->reportOdometryPose(platformPose);
        odometryPrx->reportOdometryVelocity(platformVelocity);

        // legacy
        listenerPrx->reportPlatformPose(toPlatformPose(platformPose));

        // legacy
        const auto& velo = platformVelocity.twist;
        listenerPrx->reportPlatformVelocity(velo.linear.x(), velo.linear.y(), velo.angular.z());
    }

    void PlatformUnitSimulation::moveTo(Ice::Float targetPlatformPositionX, Ice::Float targetPlatformPositionY, Ice::Float targetPlatformRotation, Ice::Float positionalAccuracy, Ice::Float orientationalAccuracy, const Ice::Current& c)
    {
        ARMARX_INFO << "new target pose : " << targetPlatformPositionX << ", " << targetPlatformPositionY << " with angle " << targetPlatformRotation;
        {
            std::unique_lock lock(currentPoseMutex);
            platformMode = ePositionControl;
            targetPositionX = targetPlatformPositionX;
            targetPositionY = targetPlatformPositionY;
            targetRotation = targetPlatformRotation;
            this->positionalAccuracy = positionalAccuracy;
            this->orientationalAccuracy = orientationalAccuracy;
        }

        FrameHeader header;
        header.timestampInMicroSeconds = TimeUtil::GetTime().toMicroSeconds();
        header.parentFrame = GlobalFrame;
        header.frame = robotRootFrame;
        header.agent = agentName;

        TransformStamped targetPose;
        targetPose.header = header;
        targetPose.transform = VirtualRobot::MathTools::posrpy2eigen4f(targetPositionX, targetPositionY, 0, 0, 0, targetRotation);

        const auto tp = toPlatformPose(targetPose);
        listenerPrx->reportNewTargetPose(tp.x, tp.y, tp.rotationAroundZ);
    }

    void armarx::PlatformUnitSimulation::move(float targetPlatformVelocityX, float targetPlatformVelocityY, float targetPlatformVelocityRotation, const Ice::Current& c)
    {
        ARMARX_INFO << deactivateSpam(1) << "New velocity: " << targetPlatformVelocityX << ", " << targetPlatformVelocityY << " with angular velocity: " << targetPlatformVelocityRotation;
        std::unique_lock lock(currentPoseMutex);
        platformMode = eVelocityControl;
        linearVelocityX = std::min(maxLinearVelocity, targetPlatformVelocityX);
        linearVelocityY = std::min(maxLinearVelocity, targetPlatformVelocityY);
        angularVelocity = std::min(maxAngularVelocity, targetPlatformVelocityRotation);

    }

    void PlatformUnitSimulation::moveRelative(float targetPlatformOffsetX, float targetPlatformOffsetY, float targetPlatformOffsetRotation, float positionalAccuracy, float orientationalAccuracy, const Ice::Current& c)
    {
        float targetPositionX, targetPositionY, targetRotation;
        {
            std::unique_lock lock(currentPoseMutex);
            targetPositionX = currentPositionX + targetPlatformOffsetX;
            targetPositionY = currentPositionY + targetPlatformOffsetY;
            targetRotation = currentRotation + targetPlatformOffsetRotation;
        }
        moveTo(targetPositionX, targetPositionY, targetRotation, positionalAccuracy, orientationalAccuracy);
    }

    void PlatformUnitSimulation::setMaxVelocities(float positionalVelocity, float orientaionalVelocity, const Ice::Current& c)
    {
        std::unique_lock lock(currentPoseMutex);
        maxLinearVelocity = positionalVelocity;
        maxAngularVelocity = orientaionalVelocity;
    }

    void PlatformUnitSimulation::stopPlatform(const Ice::Current& c)
    {
        move(0, 0, 0);
    }
}
