/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/observers/Observer.h>
#include <RobotAPI/interface/observers/SpeechObserverInterface.h>
#include <mutex>

namespace armarx
{
    class SpeechObserverPropertyDefinitions:
        public ObserverPropertyDefinitions
    {
    public:
        SpeechObserverPropertyDefinitions(std::string prefix):
            ObserverPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("TextToSpeechTopicName", "TextToSpeech", "Name of the TextToSpeechTopic");
            defineOptionalProperty<std::string>("TextToSpeechStateTopicName", "TextToSpeechState", "Name of the TextToSpeechStateTopic");
        }
    };
    class SpeechObserver;
    class SpeechListenerImpl : public TextListenerInterface
    {
    public:
        SpeechListenerImpl(SpeechObserver* obs);
    protected:
        SpeechObserver* obs;
        std::mutex dataMutex;
        // TextListenerInterface interface
    public:
        void reportText(const std::string&, const Ice::Current&) override;
        void reportTextWithParams(const std::string&, const Ice::StringSeq&, const Ice::Current&) override;
    };

    class SpeechObserver :
        virtual public Observer,
        virtual public SpeechObserverInterface
    {
        friend class SpeechListenerImpl;
    public:
        SpeechObserver();

        std::string getDefaultName() const override
        {
            return "SpeechObserver";
        }

        PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return PropertyDefinitionsPtr(new SpeechObserverPropertyDefinitions(getConfigIdentifier()));
        }

        void onInitObserver() override;
        void onConnectObserver() override;
        virtual void reportState(armarx::TextToSpeechStateType state, const Ice::Current& = Ice::emptyCurrent) override;
        virtual void reportText(const std::string& text, const Ice::Current& = Ice::emptyCurrent) override;
        virtual void reportTextWithParams(const std::string& text, const Ice::StringSeq& params, const Ice::Current& = Ice::emptyCurrent) override;

        static std::string SpeechStateToString(TextToSpeechStateType state);
    private:
        std::mutex dataMutex;
        int reportTextCounter = 0;
        int reportStateCounter = 0;
    };
}
