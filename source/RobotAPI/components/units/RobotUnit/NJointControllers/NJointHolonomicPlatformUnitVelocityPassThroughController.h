/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::NJointHolonomicPlatformUnitVelocityPassThroughController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/Robot.h>

#include "NJointControllerWithTripleBuffer.h"
#include "../ControlTargets/ControlTargetHolonomicPlatformVelocity.h"
#include "../util.h"

namespace armarx
{

    TYPEDEF_PTRS_HANDLE(NJointHolonomicPlatformUnitVelocityPassThroughControllerConfig);
    class NJointHolonomicPlatformUnitVelocityPassThroughControllerConfig: virtual public NJointControllerConfig
    {
    public:
        std::string platformName;
        float initialVelocityX;
        float initialVelocityY;
        float initialVelocityRotation;
    };

    TYPEDEF_PTRS_HANDLE(NJointHolonomicPlatformUnitVelocityPassThroughController);
    class NJointHolonomicPlatformUnitVelocityPassThroughControllerControlData
    {
    public:
        float velocityX = 0;
        float velocityY = 0;
        float velocityRotation = 0;
        IceUtil::Time commandTimestamp;
    };

    TYPEDEF_PTRS_HANDLE(NJointHolonomicPlatformUnitVelocityPassThroughController);
    /**
     * @brief The NJointHolonomicPlatformUnitVelocityPassThroughController class
     * @ingroup Library-RobotUnit-NJointControllers
     */
    class NJointHolonomicPlatformUnitVelocityPassThroughController:
        virtual public NJointControllerWithTripleBuffer<NJointHolonomicPlatformUnitVelocityPassThroughControllerControlData>
    {
    public:
        using ConfigPtrT = NJointHolonomicPlatformUnitVelocityPassThroughControllerConfigPtr;

        NJointHolonomicPlatformUnitVelocityPassThroughController(
            RobotUnit* prov,
            NJointHolonomicPlatformUnitVelocityPassThroughControllerConfigPtr config,
            const VirtualRobot::RobotPtr&);

        void rtRun(const IceUtil::Time&, const IceUtil::Time&) override;

        //for the platform unit
        void setVelocites(float velocityX, float velocityY, float velocityRotation);

        //ice interface
        std::string getClassName(const Ice::Current& = Ice::emptyCurrent) const override
        {
            return "NJointHolonomicPlatformUnitVelocityPassThroughController";
        }
        IceUtil::Time getMaxCommandDelay() const;
        void setMaxCommandDelay(const IceUtil::Time& value);

    protected:
        IceUtil::Time maxCommandDelay;

        ControlTargetHolonomicPlatformVelocity* target;
        NJointHolonomicPlatformUnitVelocityPassThroughControllerControlData initialSettings;
    };
}
