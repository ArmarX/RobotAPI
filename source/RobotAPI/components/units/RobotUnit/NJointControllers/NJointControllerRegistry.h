/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::NJointController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "NJointControllerBase.h"

#include <ArmarXCore/util/CPPUtility/TemplateMetaProgramming.h>
#include <ArmarXCore/core/util/OnScopeExit.h>
#include <ArmarXCore/core/util/Registrar.h>

namespace armarx::RobotUnitModule
{
    class ControllerManagement;
}

namespace armarx
{
    RobotUnit* getRobotUnit(RobotUnitModule::ControllerManagement* cmgr);

    class NJointControllerRegistryEntry
    {
    private:
        friend class RobotUnitModule::ControllerManagement;
        virtual NJointControllerBasePtr create(RobotUnitModule::ControllerManagement* cmngr,
                                               const NJointControllerConfigPtr&,
                                               const VirtualRobot::RobotPtr&,
                                               bool deletable,
                                               bool internal,
                                               const std::string& instanceName) const = 0;
        virtual WidgetDescription::WidgetPtr
        GenerateConfigDescription(const VirtualRobot::RobotPtr&,
                                  const std::map<std::string, ConstControlDevicePtr>&,
                                  const std::map<std::string, ConstSensorDevicePtr>&) const = 0;
        virtual NJointControllerConfigPtr
        GenerateConfigFromVariants(const StringVariantBaseMap&) const = 0;
        virtual bool hasRemoteConfiguration() const = 0;

    protected:
        static thread_local bool ConstructorIsRunning_;

    public:
        virtual ~NJointControllerRegistryEntry() = default;
        static bool
        ConstructorIsRunning()
        {
            return ConstructorIsRunning_;
        }
    };
    using NJointControllerRegistry = Registrar<std::unique_ptr<NJointControllerRegistryEntry>>;

    template <class ControllerType>
    struct NJointControllerRegistration;

    namespace detail
    {
        ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK(
            hasGenerateConfigDescription,
            GenerateConfigDescription,
            NJointControllerBase::GenerateConfigDescriptionFunctionSignature);
        ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK(
            hasGenerateConfigFromVariants,
            GenerateConfigFromVariants,
            NJointControllerBase::GenerateConfigFromVariantsFunctionSignature<typename T::ConfigPtrT>);

        template <class NJointControllerT>
        class NJointControllerRegistryEntryHelper : public NJointControllerRegistryEntry
        {
            static_assert(hasGenerateConfigDescription<NJointControllerT>::value ==
                              hasGenerateConfigFromVariants<NJointControllerT>::value,
                          "Either overload both GenerateConfigDescription and "
                          "GenerateConfigFromVariants, or none!");
            static constexpr bool hasRemoteConfiguration_ =
                hasGenerateConfigDescription<NJointControllerT>::value;

            NJointControllerBasePtr
            create(RobotUnitModule::ControllerManagement* cmngr,
                   const NJointControllerConfigPtr& config,
                   const VirtualRobot::RobotPtr& rob,
                   bool deletable,
                   bool internal,
                   const std::string& instanceName) const final override
            {
                ARMARX_CHECK_EXPRESSION(cmngr) << "ControllerManagement module is NULL!";

                using ConfigPtrT = typename NJointControllerT::ConfigPtrT;
                ConfigPtrT cfg = ConfigPtrT::dynamicCast(config);
                ARMARX_CHECK_EXPRESSION(cfg)
                    << "The configuration is of the wrong type! it has to be an instance of: "
                    << GetTypeString<ConfigPtrT>();

                ARMARX_CHECK_EXPRESSION(!ConstructorIsRunning())
                    << "Two NJointControllers are created at the same time";
                NJointControllerBasePtr ptr;
                {
                    ConstructorIsRunning_ = true;
                    ARMARX_ON_SCOPE_EXIT
                    {
                        ConstructorIsRunning_ = false;
                    };
                    ptr = new NJointControllerT(getRobotUnit(cmngr), cfg, rob);
                }
                ptr->deletable = deletable;
                ptr->internal = internal;
                ptr->rtClassName_ = ptr->getClassName(Ice::emptyCurrent);
                ptr->instanceName_ = instanceName;
                return ptr;
            }

            WidgetDescription::WidgetPtr
            GenerateConfigDescription(
                const VirtualRobot::RobotPtr& robot,
                const std::map<std::string, ConstControlDevicePtr>& controlDevices,
                const std::map<std::string, ConstSensorDevicePtr>& sensorDevices) const final override
            {
                if constexpr (hasRemoteConfiguration_)
                {
                    try
                    {
                        return NJointControllerT::GenerateConfigDescription(
                            robot, controlDevices, sensorDevices);
                    }
                    catch (Ice::UserException& e)
                    {
                        ARMARX_ERROR << "Exception calling '" << GetTypeString<NJointControllerT>()
                                     << "::GenerateConfigDescription'"
                                     << "\n---- file = " << e.ice_file()
                                     << "\n---- line = " << e.ice_line()
                                     << "\n---- id   = " << e.ice_id() << "\n---- what:\n"
                                     << e.what() << "\n---- stacktrace:\n"
                                     << e.ice_stackTrace();
                        throw;
                    }
                    catch (std::exception& e)
                    {
                        ARMARX_ERROR << "Exception calling '" << GetTypeString<NJointControllerT>()
                                     << "::GenerateConfigDescription'"
                                     << "\n---- what:\n"
                                     << e.what();
                        throw;
                    }
                    catch (...)
                    {
                        ARMARX_ERROR << "Exception calling '" << GetTypeString<NJointControllerT>()
                                     << "::GenerateConfigDescription'";
                        throw;
                    }
                }
                else
                {
                    ARMARX_CHECK_EXPRESSION(!"This function should never be called");
                }
            }

            NJointControllerConfigPtr
            GenerateConfigFromVariants(const StringVariantBaseMap& variants) const final override
            {
                if constexpr (hasRemoteConfiguration_)
                {
                    try
                    {
                        return NJointControllerT::GenerateConfigFromVariants(variants);
                    }
                    catch (Ice::UserException& e)
                    {
                        ARMARX_ERROR << "Exception calling '" << GetTypeString<NJointControllerT>()
                                     << "::GenerateConfigFromVariants'"
                                     << "\n---- file = " << e.ice_file()
                                     << "\n---- line = " << e.ice_line()
                                     << "\n---- id   = " << e.ice_id() << "\n---- what:\n"
                                     << e.what() << "\n---- stacktrace:\n"
                                     << e.ice_stackTrace();
                        throw;
                    }
                    catch (std::exception& e)
                    {
                        ARMARX_ERROR << "Exception calling '" << GetTypeString<NJointControllerT>()
                                     << "::GenerateConfigFromVariants'"
                                     << "\n---- what:\n"
                                     << e.what();
                        throw;
                    }
                    catch (...)
                    {
                        ARMARX_ERROR << "Exception calling '" << GetTypeString<NJointControllerT>()
                                     << "::GenerateConfigFromVariants'";
                        throw;
                    }
                }
                else
                {
                    ARMARX_CHECK_EXPRESSION(!"This function should never be called");
                }
            }

            bool
            hasRemoteConfiguration() const final override
            {
                return hasRemoteConfiguration_;
            }
        };
    } // namespace detail

    using NJointControllerRegistry = Registrar<std::unique_ptr<NJointControllerRegistryEntry>>;
    template <class ControllerType>
    struct NJointControllerRegistration
    {
        NJointControllerRegistration(const std::string& name)
        {
            NJointControllerRegistry::registerElement(
                name,
                std::unique_ptr<NJointControllerRegistryEntry>(
                    new detail::NJointControllerRegistryEntryHelper<ControllerType>));
        }
    };
} // namespace armarx

#define ARMARX_ASSERT_NJOINTCONTROLLER_HAS_CONSTRUCTION_GUI(T)                                     \
    static_assert(                                                                                 \
        ::armarx::detail::hasGenerateConfigDescription<T>::value,                                  \
        #T " does not offer a construction gui (missing: static GenerateConfigDescription)");      \
    static_assert(                                                                                 \
        ::armarx::detail::hasGenerateConfigFromVariants<T>::value,                                 \
        #T " does not offer a construction gui (missing: static GenerateConfigFromVariants)")


