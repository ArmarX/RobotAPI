/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Sonja Marahrens( sonja.marahrens at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include "NJointControllerWithTripleBuffer.h"
#include <VirtualRobot/Robot.h>
#include "../ControlTargets/ControlTarget1DoFActuator.h"
#include "../SensorValues/SensorValue1DoFActuator.h"
#include <VirtualRobot/IK/DifferentialIK.h>
#include <RobotAPI/libraries/core/CartesianVelocityControllerWithRamp.h>
#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityControllerWithRamp.h>

namespace armarx
{

    TYPEDEF_PTRS_HANDLE(NJointCartesianVelocityControllerWithRamp);


    class NJointCartesianVelocityControllerWithRampControlData
    {
    public:
        float xVel = 0;
        float yVel = 0;
        float zVel = 0;
        float rollVel = 0;
        float pitchVel = 0;
        float yawVel = 0;
    };

    class LayoutBuilder
    {
        WidgetDescription::VBoxLayoutPtr vlayout = new WidgetDescription::VBoxLayout;
        WidgetDescription::HBoxLayoutPtr hlayout;
    public:
        LayoutBuilder()
        {
            newLine();
        }

        void newLine()
        {
            using namespace WidgetDescription;
            hlayout = new HBoxLayout;
            vlayout->children.emplace_back(hlayout);
        }

        void addSlider(const std::string& label, float min, float max, float value)
        {
            using namespace WidgetDescription;
            hlayout->children.emplace_back(new Label(false, label));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = label;
            slider->min = min;
            slider->defaultValue = value;
            slider->max = max;
            hlayout->children.emplace_back(slider);
        }

        WidgetDescription::VBoxLayoutPtr getLayout() const
        {
            return vlayout;
        }

    };



    /**
     * @brief The NJointCartesianVelocityControllerWithRamp class
     * @ingroup Library-RobotUnit-NJointControllers
     */
    class NJointCartesianVelocityControllerWithRamp :
        public NJointControllerWithTripleBuffer<NJointCartesianVelocityControllerWithRampControlData>,
        public NJointCartesianVelocityControllerWithRampInterface
    {
    public:
        using ConfigPtrT = NJointCartesianVelocityControllerWithRampConfigPtr;
        NJointCartesianVelocityControllerWithRamp(RobotUnit* robotUnit, const NJointCartesianVelocityControllerWithRampConfigPtr& config, const VirtualRobot::RobotPtr&);

        // NJointControllerInterface interface
        std::string getClassName(const Ice::Current&) const override;

        // NJointController interface
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);
        static NJointCartesianVelocityControllerWithRampConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&) override;
        WidgetDescription::VBoxLayoutPtr createTargetLayout() const;
        WidgetDescription::VBoxLayoutPtr createParameterLayout() const;

        static VirtualRobot::IKSolver::CartesianSelection ModeFromString(const std::string mode);
        static CartesianSelectionMode::CartesianSelection IceModeFromString(const std::string mode);
        static VirtualRobot::IKSolver::CartesianSelection ModeFromIce(const CartesianSelectionMode::CartesianSelection mode);

    protected:
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
    private:
        std::vector<ControlTarget1DoFActuatorVelocity*> targets;
        //std::vector<const SensorValue1DoFActuatorPosition*> sensors;
        std::vector<const float*> velocitySensors;

        std::string nodeSetName;
        float jointLimitAvoidanceScale;
        float KpJointLimitAvoidance;
        VirtualRobot::IKSolver::CartesianSelection mode = VirtualRobot::IKSolver::All;

        CartesianVelocityControllerWithRampPtr controller;


        // NJointCartesianVelocityControllerWithRampInterface interface
    public:
        void setMaxAccelerations(float maxPositionAcceleration, float maxOrientationAcceleration, float maxNullspaceAcceleration, const Ice::Current& = Ice::emptyCurrent) override;
        void setTargetVelocity(float vx, float vy, float vz, float vrx, float vry, float vrz, const Ice::Current&) override;
        void setJointLimitAvoidanceScale(float jointLimitAvoidanceScale, const Ice::Current&) override;
        void setKpJointLimitAvoidance(float KpJointLimitAvoidance, const Ice::Current&) override;
        void immediateHardStop(const Ice::Current&) override;
        const std::string& getNodeSetName() const;
    };

} // namespace armarx

