/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::NJointKinematicUnitPassThroughController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "NJointKinematicUnitPassThroughController.h"

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointControllerRegistry.h>

namespace armarx
{
    NJointControllerRegistration<NJointKinematicUnitPassThroughController> registrationControllerNJointKinematicUnitPassThroughController("NJointKinematicUnitPassThroughController");

    NJointKinematicUnitPassThroughController::NJointKinematicUnitPassThroughController(
        RobotUnit* prov,
        const NJointKinematicUnitPassThroughControllerConfigPtr& cfg,
        const VirtualRobot::RobotPtr&)
    {
        const SensorValueBase* sv = useSensorValue(cfg->deviceName);

        ControlTargetBase* ct = useControlTarget(cfg->deviceName, cfg->controlMode);
        //get sensor
        if (ct->isA<ControlTarget1DoFActuatorPosition>())
        {
            ARMARX_CHECK_EXPRESSION(sv->asA<SensorValue1DoFActuatorPosition>());
            sensor._float = &(sv->asA<SensorValue1DoFActuatorPosition>()->position);
            ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorPosition>());
            target._float = &(ct->asA<ControlTarget1DoFActuatorPosition>()->position);
            sensorToControlOnActivateFactor = 1;
        }
        else if (ct->isA<ControlTarget1DoFActuatorVelocity>())
        {
            ARMARX_CHECK_EXPRESSION(sv->asA<SensorValue1DoFActuatorVelocity>());
            sensor._float = &(sv->asA<SensorValue1DoFActuatorVelocity>()->velocity);
            ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorVelocity>());
            target._float = &(ct->asA<ControlTarget1DoFActuatorVelocity>()->velocity);
            sensorToControlOnActivateFactor = 1;
            resetZeroThreshold = 0.1f; // TODO: way to big value?!
        }
        else if (ct->isA<ControlTarget1DoFActuatorTorque>())
        {
            ARMARX_CHECK_EXPRESSION(sv->asA<SensorValue1DoFActuatorTorque>());
            sensor._float = &(sv->asA<SensorValue1DoFActuatorTorque>()->torque);
            ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorTorque>());
            target._float = &(ct->asA<ControlTarget1DoFActuatorTorque>()->torque);
            sensorToControlOnActivateFactor = 0;
        }
        else if (ct->isA<ControlTarget1DoFActuatorPWM>())
        {
            ARMARX_CHECK_EXPRESSION(sv->asA<SensorValue1DoFMotorPWM>());
            sensor._int16 = &(sv->asA<SensorValue1DoFMotorPWM>()->motorPWM);
            ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorPWM>());
            target._int16 = &(ct->asA<ControlTarget1DoFActuatorPWM>()->pwm);
            sensorToControlOnActivateFactor = 0;
        }
        else
        {
            throw InvalidArgumentException {"Unsupported control mode: " + cfg->controlMode};
        }
    }
}
