/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::NJointKinematicUnitPassThroughController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <atomic>

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "NJointControllerBase.h"
#include "../SensorValues/SensorValue1DoFActuator.h"

#include "../ControlTargets/ControlTarget1DoFActuator.h"

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(NJointKinematicUnitPassThroughControllerConfig);
    class NJointKinematicUnitPassThroughControllerConfig : virtual public NJointControllerConfig
    {
    public:
        std::string deviceName;
        std::string controlMode;
    };


    TYPEDEF_PTRS_HANDLE(NJointKinematicUnitPassThroughController);
    /**
     * @brief The NJointKinematicUnitPassThroughController class
     * @ingroup Library-RobotUnit-NJointControllers
     */
    class NJointKinematicUnitPassThroughController:
        virtual public NJointController
    {
    public:
        using ConfigPtrT = NJointKinematicUnitPassThroughControllerConfigPtr;

        inline NJointKinematicUnitPassThroughController(
            RobotUnit* prov,
            const NJointKinematicUnitPassThroughControllerConfigPtr& cfg,
            const VirtualRobot::RobotPtr&);

        inline void rtRun(const IceUtil::Time&, const IceUtil::Time&) override
        {
            target.setValue(control);
        }
        inline void rtPreActivateController() override
        {

        }

        void reset()
        {
            control = (sensor.getValue()) * sensorToControlOnActivateFactor;
            if (std::abs(control) < resetZeroThreshold)
            {
                control = 0;
            }
        }

        //ice interface
        inline std::string getClassName(const Ice::Current& = Ice::emptyCurrent) const override
        {
            return "NJointKinematicUnitPassThroughController";
        }

        void set(float val)
        {
            control = val;
        }

    protected:
        std::atomic<float> control {0};

        template<bool ConstPtr>
        struct ptr_wrapper
        {
            float getValue() const
            {
                if (_float)
                {
                    return *_float;
                }
                return *_int16;
            }

            template < bool constPtr = ConstPtr, std::enable_if_t < !constPtr, int > = 0 >
            void setValue(float val)
            {
                if (_float)
                {
                    *_float = val;
                }
                else
                {
                    *_int16 = static_cast<std::int16_t>(val);
                }
            }
            template<class T>
            using maybe_const_ptr = std::conditional_t<ConstPtr, const T*, T*>;
            maybe_const_ptr<float>        _float{nullptr};
            maybe_const_ptr<std::int16_t> _int16{nullptr};
        };

        ptr_wrapper<false> target;
        ptr_wrapper<true> sensor;
        float sensorToControlOnActivateFactor {0};
        float resetZeroThreshold {0};
    };
}
