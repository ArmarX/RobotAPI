/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointControllerWithTripleBuffer.h>

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/interface/units/RobotUnit/NJointCartesianVelocityController.h>
#include <RobotAPI/libraries/core/CartesianVelocityController.h>

#include <VirtualRobot/IK/DifferentialIK.h>
#include <VirtualRobot/VirtualRobot.h>

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(NJointCartesianVelocityController);

    TYPEDEF_PTRS_HANDLE(NJointCartesianVelocityControllerControlData);
    class NJointCartesianVelocityControllerControlData
    {
    public:
        float xVel = 0;
        float yVel = 0;
        float zVel = 0;
        float rollVel = 0;
        float pitchVel = 0;
        float yawVel = 0;
        std::vector<float> nullspaceJointVelocities;
        float avoidJointLimitsKp = 0;
        std::vector<float> torqueKp;
        std::vector<float> torqueKd;

        VirtualRobot::IKSolver::CartesianSelection mode = VirtualRobot::IKSolver::All;
    };

    class SimplePID
    {
    public:
        float Kp = 0, Kd = 0;
        float lastError = 0;
        float update(float dt, float error);
    };


    /**
     * @brief The NJointCartesianVelocityController class
     * @ingroup Library-RobotUnit-NJointControllers
     */
    class NJointCartesianVelocityController :
        public NJointControllerWithTripleBuffer<NJointCartesianVelocityControllerControlData>,
        public NJointCartesianVelocityControllerInterface
    {
    public:
        using ConfigPtrT = NJointCartesianVelocityControllerConfigPtr;
        NJointCartesianVelocityController(RobotUnit* prov, const NJointCartesianVelocityControllerConfigPtr& config, const VirtualRobot::RobotPtr&);

        // NJointControllerInterface interface
        std::string getClassName(const Ice::Current&) const override;
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&) override;

        // NJointController interface
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);

        static NJointCartesianVelocityControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
        NJointCartesianVelocityController(
            RobotUnitPtr prov,
            NJointCartesianVelocityControllerConfigPtr config,
            const VirtualRobot::RobotPtr& r);

        // for TCPControlUnit
        void setVelocities(float xVel, float yVel, float zVel, float rollVel, float pitchVel, float yawVel, VirtualRobot::IKSolver::CartesianSelection mode);
        void setAvoidJointLimitsKp(float kp);
        std::string getNodeSetName() const;
        static ::armarx::WidgetDescription::WidgetSeq createSliders();
        WidgetDescription::HBoxLayoutPtr createJointSlidersLayout(float min, float max, float defaultValue) const;

        static VirtualRobot::IKSolver::CartesianSelection ModeFromString(const std::string mode);
        static NJointCartesianVelocityControllerMode::CartesianSelection IceModeFromString(const std::string mode);
        static VirtualRobot::IKSolver::CartesianSelection ModeFromIce(const NJointCartesianVelocityControllerMode::CartesianSelection mode);
    protected:
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
    private:
        std::vector<ControlTarget1DoFActuatorVelocity*> targets;
        //std::vector<const SensorValue1DoFActuatorPosition*> sensors;
        std::vector<const SensorValue1DoFActuatorTorque*> torqueSensors;
        std::vector<const SensorValue1DoFGravityTorque*> gravityTorqueSensors;
        std::vector<SimplePID> torquePIDs;

        CartesianVelocityControllerPtr tcpController;

        std::string nodeSetName;


        // NJointCartesianVelocityControllerInterface interface
    public:
        void setControllerTarget(float x, float y, float z, float roll, float pitch, float yaw, float avoidJointLimitsKp, NJointCartesianVelocityControllerMode::CartesianSelection mode, const Ice::Current&) override;
        void setTorqueKp(const StringFloatDictionary& torqueKp, const Ice::Current&) override;
        void setNullspaceJointVelocities(const StringFloatDictionary& nullspaceJointVelocities, const Ice::Current&) override;
    };

} // namespace armarx

