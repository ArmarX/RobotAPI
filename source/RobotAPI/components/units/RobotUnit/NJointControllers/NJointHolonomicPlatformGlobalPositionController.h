/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Markus Grotz (markus.grotz at kit dot edu)
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include <atomic>

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "NJointControllerWithTripleBuffer.h"
#include "../SensorValues/SensorValueHolonomicPlatform.h"

#include "../ControlTargets/ControlTarget1DoFActuator.h"
#include "RobotAPI/components/units/RobotUnit/Devices/GlobalRobotPoseSensorDevice.h"
#include <RobotAPI/libraries/core/PIDController.h>

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetHolonomicPlatformVelocity.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include <RobotAPI/interface/units/PlatformUnitInterface.h>

namespace armarx
{

    TYPEDEF_PTRS_HANDLE(NJointHolonomicPlatformGlobalPositionControllerConfig);
    class NJointHolonomicPlatformGlobalPositionControllerConfig : virtual public NJointControllerConfig
    {
    public:
        std::string platformName;
        float p = 1.0f;
        float i = 0.0f;
        float d = 0.0f;
        float maxVelocity = 400;
        float maxAcceleration = 300;

        float p_rot = 0.8f;
        float i_rot = 0.0f;
        float d_rot = 0.0f;

        float maxRotationVelocity = 1.0;
        float maxRotationAcceleration = 0.5;

    };

    struct NJointHolonomicPlatformGlobalPositionControllerTarget
    {
        Eigen::Vector2f target; // x,y
        float targetOrientation;
        float translationAccuracy = 0.0f;
        float rotationAccuracy = 0.0f;
        bool newTargetSet = false;

        // Eigen::Vector2f startPosition;
        // float startOrientation;

        // Eigen::Vector2f globalPosition;
        // float globalOrientation;

        IceUtil::Time lastUpdate = IceUtil::Time::seconds(0);
    };

    TYPEDEF_PTRS_HANDLE(NJointHolonomicPlatformGlobalPositionController);

    /**
     * @brief The NJointHolonomicPlatformGlobalPositionController class
     * @ingroup Library-RobotUnit-NJointControllers
     */
    class NJointHolonomicPlatformGlobalPositionController:
        virtual public NJointControllerWithTripleBuffer<NJointHolonomicPlatformGlobalPositionControllerTarget>
    {
    public:
        using ConfigPtrT = NJointHolonomicPlatformGlobalPositionControllerConfigPtr;

        inline NJointHolonomicPlatformGlobalPositionController(
            RobotUnit* robotUnit,
            const NJointHolonomicPlatformGlobalPositionControllerConfigPtr& cfg,
            const VirtualRobot::RobotPtr&);

        inline virtual void rtRun(const IceUtil::Time&, const IceUtil::Time& timeSinceLastIteration) override;
        inline virtual void rtPreActivateController() override;

        //ice interface
        inline virtual std::string getClassName(const Ice::Current& = Ice::emptyCurrent) const override
        {
            return "NJointHolonomicPlatformGlobalPositionController";
        }

        void setTarget(float x, float y, float yaw, float translationAccuracy, float rotationAccuracy);

    protected:
        const GlobalRobotLocalizationSensorDevice::SensorValueType* sv;
        MultiDimPIDController pid;
        PIDController  opid;

        ControlTargetHolonomicPlatformVelocity* target;

        bool isTargetSet = false;

    };
} // namespace armarx
