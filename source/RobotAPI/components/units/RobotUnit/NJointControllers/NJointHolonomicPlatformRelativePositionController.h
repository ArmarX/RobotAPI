/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once


#include <atomic>

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "NJointControllerWithTripleBuffer.h"
#include "../SensorValues/SensorValueHolonomicPlatform.h"

#include "../ControlTargets/ControlTarget1DoFActuator.h"
#include <RobotAPI/libraries/core/PIDController.h>

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetHolonomicPlatformVelocity.h>

#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
namespace armarx
{

    TYPEDEF_PTRS_HANDLE(NJointHolonomicPlatformRelativePositionControllerConfig);
    class NJointHolonomicPlatformRelativePositionControllerConfig : virtual public NJointControllerConfig
    {
    public:
        std::string platformName;
        float p = 1.0f;
        float i = 0.0f;
        float d = 0.0f;
        float maxVelocity = 300;
        float maxAcceleration = 500;
        float maxRotationVelocity = 0.5;
        float maxRotationAcceleration = 0.5;
        //        float rad2MMFactor = 50.0f;
    };

    struct NJointHolonomicPlatformRelativePositionControllerTarget
    {
        Eigen::Vector2f target; // x,y
        float targetOrientation;
        float translationAccuracy = 0.0f;
        float rotationAccuracy = 0.0f;
        bool newTargetSet = false;
    };

    TYPEDEF_PTRS_HANDLE(NJointHolonomicPlatformRelativePositionController);

    /**
     * @brief The NJointHolonomicPlatformRelativePositionController class
     * @ingroup Library-RobotUnit-NJointControllers
     */
    class NJointHolonomicPlatformRelativePositionController:
        virtual public NJointControllerWithTripleBuffer<NJointHolonomicPlatformRelativePositionControllerTarget>
    {
    public:
        using ConfigPtrT = NJointHolonomicPlatformRelativePositionControllerConfigPtr;

        inline NJointHolonomicPlatformRelativePositionController(
            RobotUnit* robotUnit,
            const NJointHolonomicPlatformRelativePositionControllerConfigPtr& cfg,
            const VirtualRobot::RobotPtr&);

        inline virtual void rtRun(const IceUtil::Time&, const IceUtil::Time& timeSinceLastIteration) override;
        inline virtual void rtPreActivateController() override;

        //ice interface
        inline virtual std::string getClassName(const Ice::Current& = Ice::emptyCurrent) const override
        {
            return "NJointHolonomicPlatformRelativePositionController";
        }

        void setTarget(float x, float y, float yaw, float translationAccuracy, float rotationAccuracy);


    protected:
        const SensorValueHolonomicPlatform* sv;
        MultiDimPIDController pid;
        PositionThroughVelocityControllerWithAccelerationBoundsAndPeriodicPosition oriCtrl;
        ControlTargetHolonomicPlatformVelocity* target;
        Eigen::Vector2f startPosition, currentPosition;
        float startOrientation;
        float currentOrientation;
        //        float rad2MMFactor;
    };
} // namespace armarx


