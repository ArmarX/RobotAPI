/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI::Test
* @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
* @date       2017
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE RobotAPI::BasicControllers::Test
#define ARMARX_BOOST_TEST
#define CREATE_LOGFILES
#include <random>
#include <iostream>

#include <boost/algorithm/clamp.hpp>

#include <ArmarXCore/core/util/algorithm.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/Test.h>
#include "../BasicControllers.h"
using namespace armarx;
//params for random tests
const std::size_t tries = 10;
const std::size_t ticks = 20000; // each tick is 0.75 to 1.25 ms
//helpers for logging
#ifdef CREATE_LOGFILES
#define LOG_CONTROLLER_DATA_WRITE_TO(name) change_logging_file(name)
#define LOG_CONTROLLER_DATA(...) f << __VA_ARGS__ << "\n"

#include <boost/filesystem.hpp>
#include <fstream>

static const boost::filesystem::path fpath
{
    "controller_logfiles/"
};
static std::ofstream f;
static bool isSetup = false;

void change_logging_file(const std::string& name)
{
    if (f.is_open())
    {
        f.close();
    }
    if (!isSetup)
    {
        boost::filesystem::create_directories(fpath);
        //create the python evaluation file
        boost::filesystem::path tmppath(fpath / "eval.py");
        f.open(tmppath.string());
        // *INDENT-OFF*
        f << R"raw_str_delim(
          def consume_file(fname, save=True, show=True):
#get data
          with open(fname, 'r') as f:
          data = [ x.split(' ') for x in f.read().split('\n') if x != '']
#plot
          from mpl_toolkits.axes_grid1 import host_subplot
          import mpl_toolkits.axisartist as AA
          import matplotlib.pyplot as plt

          pos = host_subplot(111, axes_class=AA.Axes)
          plt.subplots_adjust(right=0.75)
          vel = pos.twinx()
          acc = pos.twinx()


          pos.axhline(0, color='black')
          vel.axhline(0, color='black')
          acc.axhline(0, color='black')

          offset = 60
          acc.axis['right'] = acc.get_grid_helper().new_fixed_axis(loc='right',
          axes=acc,
          offset=(offset, 0))

          c={pos:'red', vel:'blue', acc:'green'}
          b={pos:0    , vel:0     , acc:0      }

          pos.set_xlabel('Time [s]')
          pos.set_ylabel('Position')
          vel.set_ylabel('Velocity')
          acc.set_ylabel('Acceleration')

          pos.axis['left'].label.set_color(c[pos])
          vel.axis['right'].label.set_color(c[vel])
          acc.axis['right'].label.set_color(c[acc])


          def get(name,factor=1):
          if name in data[0]:
          return [factor*float(x[data[0].index(name)]) for x in data[1:]]

          times=get('time')

          def add(plt,name,factor=1, clr=None, style='-'):
          d=get(name,factor)
          if d is None or [0]*len(d) == d:
          return
          if clr is None:
          clr=c[plt]
          plt.plot(times, d, style,color=clr)
          b[plt] = max([b[plt]]+[abs(x) for x in d])
          plt.set_ylim(-b[plt]*1.1, b[plt]*1.1)

          add(pos,'curpos',clr='red')
          add(pos,'targpos',clr='red', style='-.')
          add(pos,'posHi',clr='darkred', style='--')
          add(pos,'posLo',clr='darkred', style='--')
          add(pos,'posHiHard',clr='darkred', style='--')
          add(pos,'posLoHard',clr='darkred', style='--')
          add(pos,'brakingDist',clr='orange', style=':')
          add(pos,'posAfterBraking',clr='magenta', style=':')

          add(vel,'curvel',clr='teal')
          add(vel,'targvel',clr='teal', style='-.')
          add(vel,'maxv',clr='blue', style='--')
          add(vel,'maxv',factor=-1,clr='blue', style='--')

          add(acc,'curacc',clr='green')
          add(acc,'acc',clr='darkgreen', style='--')
          add(acc,'dec',clr='darkgreen', style='--')

          plt.draw()
          if save: plt.savefig(fname+'.png')
          if show: plt.show()
          if not show: plt.close()


          import sys
          import os
          def handle_path(p, show=True):
          if '.' in p:
          return
          if os.path.isfile(p):
          consume_file(p,show=show)
          if os.path.isdir(p):
          show=False
          for subdir, dirs, files in os.walk(p):
          for f in files:
          handle_path(subdir+f,show=show)

          if len(sys.argv) >= 2:
          handle_path(sys.argv[1])
          else:
          handle_path('./')
          )raw_str_delim";
        // *INDENT-ON*
        isSetup = true;
        f.close();
    }
    boost::filesystem::path tmppath(fpath / name);
    f.open(tmppath.string());
    std::cout << "now writing to: " << boost::filesystem::absolute(tmppath).string() << "\n";
}

#else
#define LOG_CONTROLLER_DATA_WRITE_TO(name) do{}while(0)
#define LOG_CONTROLLER_DATA(...) do{}while(0)
#endif


unsigned int getSeed()
{
    static const auto seed = std::random_device {}();
    std::cout << "seed = " << seed << std::endl;
    return seed;
}

static std::mt19937 gen {getSeed()};

struct Simulation
{
    float time = 0;
    float dt = 0;
    float maxDt = 0.001;

    float curpos = 0;
    float oldpos = 0;
    float targpos = 0;
    float posHiHard = M_PI;
    float posLoHard = -M_PI;
    float posHi = M_PI;
    float posLo = -M_PI;

    float curvel = 0;
    float oldvel = 0;
    float targvel = 0;
    float maxvel = 2;

    float curacc = 0;
    float oldacc = 0;
    float acc = 0;
    float dec = 0;

    float brakingDist = 0;
    float posAfterBraking = 0;


    std::uniform_real_distribution<float> vDist;
    std::uniform_real_distribution<float> aDist;
    std::uniform_real_distribution<float> pDist;
    std::uniform_real_distribution<float> tDist;

    void reset()
    {
        time = 0;

        curpos = 0;
        oldpos = 0;
        targpos = 0;

        curvel = 0;
        oldvel = 0;
        targvel = 0;

        curacc = 0;
        oldacc = 0;
        acc = 0;
        dec = 0;

        brakingDist = 0;
        posAfterBraking = 0;

        vDist = std::uniform_real_distribution<float> { -maxvel, maxvel};
        aDist = std::uniform_real_distribution<float> {maxvel, maxvel * 4   };
        tDist = std::uniform_real_distribution<float> {maxDt * 0.75f, maxDt * 1.25f};
        pDist = std::uniform_real_distribution<float> {posLoHard, posHiHard};
    }

    //updating
    template<class FNC>
    void tick(FNC& callee)
    {
        dt = tDist(gen);
        callee();
        log();
    }

    void updateVel(float newvel)
    {
        BOOST_CHECK(std::isfinite(newvel));
        //save old
        oldvel = curvel;
        oldpos = curpos;
        oldacc = curacc;

        //update
        curvel = newvel;
        curacc = std::abs(curvel - oldvel) / dt;
        curpos += curvel * dt;
        time += dt;
        brakingDist = brakingDistance(curvel, dec);
        posAfterBraking = curpos + brakingDist;
    }

    //checks
    void checkVel()
    {
        //check v
        BOOST_CHECK_LE(curvel, maxvel * 1.01);
        BOOST_CHECK_LE(-maxvel * 1.01, curvel);
    }

    void checkPos()
    {
        BOOST_CHECK_LE(curpos, posHiHard);
        BOOST_CHECK_LE(posLoHard, curpos);
    }

    void checkAcc()
    {
        if (sign(curvel) != sign(oldvel))
        {
            //            std::cout<< "Time["<<time<<"] velocity changed sign (from/to):" <<  sign(oldvel) << " / " << sign(curvel)<<"\n";
            return;
        }
        if (std::abs(oldvel) > std::abs(curvel))
        {
            // we dont care about high deceleration
            //            //we decellerated
            //            if (!(curacc < dec * 1.01))
            //            {
            //                ARMARX_INFO << "Time[" << time << "] violated deceleration bound! vold " << oldvel << " / vnew " << curvel << " / dv " << std::abs(oldvel - curvel) << " / dt " << dt << " / dec " << dec
            //                          << " / (targetv " << targvel << ")\n";
            //            }
            //            BOOST_CHECK_LE(curacc, dec * 1.01);
        }
        else
        {
            //we accellerated
            if (!(curacc < acc * 1.01))
            {
                ARMARX_INFO << "Time[" << time << "] violated deceleration bound! vold " << oldvel << " / vnew " << curvel << " / dv " << std::abs(oldvel - curvel) << " / dt " << dt << " / acc " << acc
                            << " / (targetv " << targvel << ")\n";
            }
            BOOST_CHECK_LE(curacc, acc * 1.01);
        }
    }

    //logging
    void writeHeader(const std::string& name)
    {
        LOG_CONTROLLER_DATA_WRITE_TO(name);
        LOG_CONTROLLER_DATA("time curpos targpos posHiHard posLoHard posHi posLo curvel targvel maxvel curacc acc dec brakingDist posAfterBraking");
        reset();
    }
    void log()
    {
        //output a neg val for dec and a pos val for acc
        float outputacc;
        if (sign(curvel) != sign(oldvel))
        {
            // cant determine the current acceleration correctly (since we have an acceleration and deceleration phase)
            outputacc = 0;
        }
        else
        {
            outputacc = curacc;
            if (std::abs(oldvel) > std::abs(curvel))
            {
                //we decelerated -> negative sign
                outputacc *= -1;
            }
        }
        LOG_CONTROLLER_DATA(time << " " <<
                            curpos << " " << targpos << " " << posHiHard << " " << posLoHard << " " << posHi << " " << posLo << " " <<
                            curvel << " " << targvel << " " << maxvel << " " <<
                            outputacc << " " << acc << " " << -dec << " " <<
                            brakingDist << " " << posAfterBraking);
    }
};



BOOST_AUTO_TEST_CASE(positionThroughVelocityControlWithAccelerationBoundsTest)
{
    std::cout << "starting positionThroughVelocityControlWithAccelerationBounds \n";
    Simulation s;
    s.posHi = M_PI;
    s.posLo = -M_PI;

    float p = 0.3;
    //    const float pControlPosErrorLimit = 0.01;
    //    const float pControlVelLimit = 0.01;

    auto testTick = [&]
    {
        PositionThroughVelocityControllerWithAccelerationBoundsAndPeriodicPosition ctrl;
        ctrl.dt = s.dt;
        ctrl.maxDt = s.maxDt;
        ctrl.currentV = s.curvel;
        ctrl.maxV = s.maxvel;
        ctrl.acceleration = s.acc;
        ctrl.deceleration = s.dec;
        ctrl.currentPosition = s.curpos;
        ctrl.targetPosition = s.targpos;
        //        ctrl.pControlPosErrorLimit = pControlPosErrorLimit;
        //        ctrl.pControlVelLimit = pControlVelLimit;
        ctrl.p = p;
        ctrl.positionPeriodHi = s.posHi;
        ctrl.positionPeriodLo = s.posLo;
        BOOST_CHECK(ctrl.validParameters());
        s.updateVel(ctrl.run());
        //        ARMARX_INFO << deactivateSpam(0.01) << VAROUT(s.curpos);
        //s.updateVel(positionThroughVelocityControlWithAccelerationBounds(
        //s.dt, s.maxDt,
        //s.curvel, s.maxvel,
        //s.acc, s.dec,
        //s.curpos, s.targpos,
        //pControlPosErrorLimit, pControlVelLimit, p
        //));
        s.checkVel();
        s.checkAcc();
    };

    std::cout << "random tests\n";
    for (std::size_t try_ = 0; try_ < tries; ++ try_)
    {
        s.writeHeader("posViaVelCtrl+acc_random_" + to_string(try_));
        s.maxvel = std::abs(s.vDist(gen)) + 1;
        s.curvel = armarx::math::MathUtils::LimitTo(s.vDist(gen), s.maxvel);
        s.curpos = s.pDist(gen);
        s.targpos = s.pDist(gen);
        s.acc = s.aDist(gen);
        s.dec = s.aDist(gen);
        //s.acc: 6.08172 s.dec: 7.98634 s.maxvel: 1.73312
        //s.curpos: -1.90146 TargetPos: 1.2378s.acc: 5.93058 s.dec: 3.06262 s.maxvel: 2.94756
        ARMARX_INFO << VAROUT(s.curpos) << "TargetPos: " << s.targpos << VAROUT(s.acc) << VAROUT(s.dec) << VAROUT(s.maxvel) << VAROUT(s.curvel);
        //        p = ((std::min(s.acc,s.dec)*s.maxDt*0.75-pControlVelLimit)/pControlPosErrorLimit) * 0.99f; // sometimes <0
        s.log();
        for (std::size_t tick = 0; tick < ticks; ++tick)
        {
            s.tick(testTick);
        }
        float distance = std::abs(angleDistance(s.curpos, s.targpos));
        ARMARX_INFO << "Final pos: " << s.curpos << " final distance: " << distance;
        BOOST_CHECK_LE(distance, 0.01); // allow error of 0.5729577951308232 deg
    }
    std::cout << "bounds tests\n";
    std::cout << "TODO\n";
    ///TODO

    std::cout << "done positionThroughVelocityControlWithAccelerationBounds \n";
}

BOOST_AUTO_TEST_CASE(timeEstimationTest)
{
    float val = std::nanf(to_string((1 << 16) - 1).c_str());
    BOOST_CHECK(!(val > 0));
    for (int i = 0; i < 10; i++)
    {

        ARMARX_INFO << "\nTrial " << i;
        PositionThroughVelocityControllerWithAccelerationBounds c;
        c.acceleration = 1;
        c.deceleration = 2;
        c.currentPosition = 0;
        c.currentV = 0.5;
        c.dt = 0.001;
        c.maxDt = 0.002;
        c.maxV = 1;
        c.p = 1;
        c.targetPosition = 2;
        float est = c.estimateTime();
        ARMARX_INFO << "estimated time " << est;
        float newDt = est + (float)(rand() % 100) / 10;
        findVelocityAndAccelerationForTimeAndDistance(c.targetPosition - c.currentPosition, c.currentV, c.maxV, c.deceleration,
                trapeze(c.currentV, c.acceleration, c.maxV, c.deceleration, 0, c.targetPosition - c.currentPosition),
                newDt, c.maxV, c.acceleration, c.deceleration);

        //    c.maxV = 0.583;
        //    c.acceleration = 0.16666;
        //    c.deceleration = 1.1666;
        ARMARX_INFO << VAROUT(c.maxV) << VAROUT(c.acceleration) << VAROUT(c.deceleration);
        est = c.estimateTime();
        ARMARX_INFO << "desired time:  " << newDt << " estimated time " << est;
        BOOST_CHECK_CLOSE(est, newDt, 1);
    }

}

