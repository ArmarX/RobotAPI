/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI::Test
* @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
* @date       2017
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE RobotAPI::BasicControllers::Test
#define ARMARX_BOOST_TEST
#define CREATE_LOGFILES
#include <random>
#include <iostream>
#include <filesystem>
#include "../util/CtrlUtil.h"
#include <boost/algorithm/clamp.hpp>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/util/algorithm.h>
#include <RobotAPI/Test.h>
#include "../BasicControllers.h"
using namespace armarx;
//params for random tests
const std::size_t tries = 1;
const std::size_t ticks = 5000; // each tick is 0.75 to 1.25 ms
//helpers for logging
#ifdef CREATE_LOGFILES
#define LOG_CONTROLLER_DATA_WRITE_TO(name) change_logging_file(name)
#define LOG_CONTROLLER_DATA(...) f << __VA_ARGS__ << "\n"

#include <boost/filesystem.hpp>
#include <fstream>

static const std::filesystem::path fpath
{
    "controller_logfiles/"
};
static std::ofstream f;
static bool isSetup = false;

void change_logging_file(const std::string& name)
{
    if (f.is_open())
    {
        f.close();
    }
    if (!isSetup)
    {
        std::filesystem::create_directories(fpath);
        //create the python evaluation file
        std::filesystem::path tmppath(fpath / "eval.py");
        f.open(tmppath.string());
#include "eval_script.inc"
        isSetup = true;
        f.close();
    }
    std::filesystem::path tmppath(fpath / (name + ".log"));
    f.open(tmppath.string());
    std::cout << "now writing to: " << std::filesystem::absolute(tmppath).string() << "\n";
}

#else
#define LOG_CONTROLLER_DATA_WRITE_TO(name) do{}while(0)
#define LOG_CONTROLLER_DATA(...) do{}while(0)
#endif


unsigned int getSeed()
{
    static const auto seed = std::random_device {}();
    std::cout << "seed = " << seed << std::endl;
    return seed;
}

static std::mt19937 gen {getSeed()};

struct Simulation
{
    double time = 0;
    double dt = 0;
    double maxDt = 0.01;

    double curpos = 0;
    double oldpos = 0;
    double targpos = 0;
    double posHiHard = M_PI;
    double posLoHard = -M_PI;
    double posHi = M_PI;
    double posLo = -M_PI;

    double curvel = 0;
    double oldvel = 0;
    double targvel = 0;
    double maxvel = 10;

    double curacc = 0;
    double oldacc = 0;
    double acc = 0;
    double dec = 0;

    double jerk = 0;

    double brakingDist = 0;
    double posAfterBraking = 0;


    std::uniform_real_distribution<double> vDist;
    std::uniform_real_distribution<double> aDist;
    std::uniform_real_distribution<double> pDist;
    std::uniform_real_distribution<double> tDist;

    void reset()
    {
        time = 0;

        curpos = 0;
        oldpos = 0;
        targpos = 0;

        curvel = 0;
        oldvel = 0;
        targvel = 0;

        curacc = 0;
        oldacc = 0;
        acc = 0;
        dec = 0;
        jerk = 0;

        brakingDist = 0;
        posAfterBraking = 0;

        vDist = std::uniform_real_distribution<double> { -maxvel, maxvel};
        aDist = std::uniform_real_distribution<double> {maxvel / 4, maxvel * 4   };
        tDist = std::uniform_real_distribution<double> {maxDt * 0.75f, maxDt * 1.25f};
        pDist = std::uniform_real_distribution<double> {posLoHard, posHiHard};
    }

    //updating
    template<class FNC>
    void tick(FNC& callee)
    {
        dt = maxDt;//tDist(gen);
        callee();
        log();
    }

    void updateVel(double newvel)
    {
        BOOST_CHECK(std::isfinite(newvel));
        //save old
        oldvel = curvel;
        oldpos = curpos;
        oldacc = curacc;

        //update
        curvel = newvel;
        curacc = (curvel - oldvel) / dt;
        jerk = (curacc -  oldacc) / dt;
        curpos += ctrlutil::s(dt, 0, curvel, curacc, 0/*math::MathUtils::Sign(curacc-oldacc) * jerk*/);
        time += dt;
        brakingDist = brakingDistance(curvel, dec);
        posAfterBraking = curpos + brakingDist;
    }

    void updatePos(double newPos)
    {
        BOOST_CHECK(std::isfinite(newPos));
        //save old
        oldvel = curvel;
        oldpos = curpos;
        oldacc = curacc;

        //update
        curvel = (newPos - oldpos) / dt;
        curacc = (curvel - oldvel) / dt;
        jerk = (curacc -  oldacc) / dt;
        curpos = newPos;
        time += dt;
        brakingDist = brakingDistance(curvel, dec);
        posAfterBraking = curpos + brakingDist;
    }

    //checks
    void checkVel()
    {
        //check v
        BOOST_CHECK_LE(curvel, maxvel * 1.01);
        BOOST_CHECK_LE(-maxvel * 1.01, curvel);
    }

    void checkPos()
    {
        BOOST_CHECK_LE(curpos, posHiHard);
        BOOST_CHECK_LE(posLoHard, curpos);
    }

    void checkAcc()
    {
        if (sign(curvel) != sign(oldvel))
        {
            //            std::cout<< "Time["<<time<<"] velocity changed sign (from/to):" <<  sign(oldvel) << " / " << sign(curvel)<<"\n";
            return;
        }
        if (std::abs(oldvel) > std::abs(curvel))
        {
            // deceleration check with PID controller for motion-finish does not work -> check disabled
            //we decelerated
            if (!(curacc < dec * 1.01))
            {
                std::cout << "Time[" << time << "] violated deceleration bound! vold " << oldvel << " / vnew " << curvel << " / dv " << std::abs(oldvel - curvel) << " / dt " << dt << " / dec " << dec
                          << " / (targetv " << targvel << ")\n";
            }
            BOOST_CHECK_LE(curacc, dec * 1.01);
        }
        else
        {
            //we accellerated
            if (!(curacc < acc * 1.01))
            {
                std::cout << "Time[" << time << "] violated deceleration bound! vold " << oldvel << " / vnew " << curvel << " / dv " << std::abs(oldvel - curvel) << " / dt " << dt << " / acc " << acc
                          << " / (targetv " << targvel << ")\n";
            }
            BOOST_CHECK_LE(curacc, acc * 1.01);
        }
    }

    //logging
    void writeHeader(const std::string& name)
    {
        LOG_CONTROLLER_DATA_WRITE_TO(name);
        LOG_CONTROLLER_DATA("time curpos targpos posHiHard posLoHard posHi posLo curvel targvel maxv curacc acc dec brakingDist posAfterBraking");
        reset();
    }
    void log()
    {
        //        //output a neg val for dec and a pos val for acc
        //        double outputacc;
        //        if (sign(curvel) != sign(oldvel))
        //        {
        //            // cant determine the current acceleration correctly (since we have an acceleration and deceleration phase)
        //            outputacc = 0;
        //        }
        //        else
        //        {
        //            outputacc = curacc;
        //            if (std::abs(oldvel) > std::abs(curvel))
        //            {
        //                //we decelerated -> negative sign
        //                outputacc *= -1;
        //            }
        //        }
        LOG_CONTROLLER_DATA(time << " " <<
                            curpos << " " << targpos << " " << posHiHard << " " << posLoHard << " " << posHi << " " << posLo << " " <<
                            curvel << " " << targvel << " " << maxvel << " " <<
                            curacc << " " << acc << " " << -dec << " " <<
                            brakingDist << " " << posAfterBraking);
    }
};

BOOST_AUTO_TEST_CASE(VelocityControllerWithRampedAccelerationAndPositionBoundsTest)
{

    std::cout << "starting VelocityControllerWithRampedAccelerationTest \n";
    Simulation s;
    //    s.posHi = 0;
    //    s.posLo = 0;

    auto distPosLo = std::uniform_real_distribution<double> { -M_PI, -M_PI_4};
    auto distPosHi = std::uniform_real_distribution<double> { M_PI_4, M_PI};
    s.posLo = distPosLo(gen);
    s.posHi = distPosHi(gen);
    //    double p = 20.5;
    VelocityControllerWithRampedAccelerationAndPositionBounds ctrl;

    auto testTick = [&]
    {
        ctrl.dt = s.dt;
        ctrl.maxDt = s.maxDt;
        //        ctrl.currentV = s.curvel;
        //        ctrl.currentAcc = s.curacc;
        ctrl.maxV = s.maxvel;
        ctrl.targetV = s.targvel;
        ctrl.positionLimitHiSoft = s.posHi;
        ctrl.positionLimitLoSoft = s.posLo;
        ctrl.currentPosition = s.curpos;
        ctrl.deceleration = 40;

        //        ctrl.accuracy = 0.00001;
        //        ctrl.pControlPosErrorLimit = pControlPosErrorLimit;
        //        ctrl.pControlVelLimit = pControlVelLimit;
        //        ctrl.p = p;//ctrl.calculateProportionalGain();
        //        ctrl.usePIDAtEnd = false;
        //        ARMARX_INFO << VAROUT(ctrl.p);
        BOOST_CHECK(ctrl.validParameters());
        auto r = ctrl.run();
        //        ARMARX_INFO << "State: " << (int)ctrl.calcState() << " " << VAROUT(r.acceleration) << VAROUT(r.velocity) << VAROUT(r.jerk);
        s.updateVel(r.velocity);
        ctrl.currentV = r.velocity;
        ctrl.currentAcc = r.acceleration;
        //s.updateVel(positionThroughVelocityControlWithAccelerationBounds(
        //s.dt, s.maxDt,
        //s.curvel, s.maxvel,
        //s.acc, s.dec,
        //s.curpos, s.targpos,
        //pControlPosErrorLimit, pControlVelLimit, p
        //));
        //        s.checkVel();
        //        s.checkAcc();
    };

    std::cout << "random tests\n";
    for (std::size_t try_ = 0; try_ < tries; ++ try_)
    {
        ctrl = VelocityControllerWithRampedAccelerationAndPositionBounds();
        s.writeHeader("RampedAccPositionLimitedVelCtrl+acc_random_" + to_string(try_));
        s.maxvel = 5;//std::abs(s.vDist(gen)) + 1;
        s.curvel = 5;//armarx::math::MathUtils::LimitTo(s.vDist(gen), s.maxvel);
        ctrl.currentV = s.curvel;
        s.curpos = s.pDist(gen);
        ctrl.currentAcc = s.pDist(gen);
        s.curacc = ctrl.currentAcc;
        ARMARX_INFO << VAROUT(s.dt) << VAROUT(s.curpos) << VAROUT(s.targvel) << " " << VAROUT(s.acc) << VAROUT(s.dec) << VAROUT(s.maxvel) << VAROUT(s.curvel) << VAROUT(s.jerk);
        //        p = ((std::min(s.acc,s.dec)*s.maxDt*0.75-pControlVelLimit)/pControlPosErrorLimit) * 0.99f; // sometimes <0
        ctrl.jerk = 100;//(random() % 100) + 10;
        s.log();
        double stopTime = -1;
        for (std::size_t tick = 0; tick < ticks; ++tick)
        {
            //            std::cout << std::endl;
            //            s.targpos = tick > 900 ? 3 : 5;

            s.tick(testTick);
            ARMARX_INFO  << /*deactivateSpam(0.01) <<*/ VAROUT(s.dt * tick) << VAROUT(s.curpos) << VAROUT(s.targvel) << VAROUT(s.curacc) << VAROUT(s.maxvel) << VAROUT(s.curvel) << VAROUT(s.jerk);
            if (std::abs(s.curvel - s.targvel) < 0.001 && stopTime < 0)
            {
                stopTime = s.dt * (tick + 20); // let it run for some more ticks to make sure the velocity is stable
            }
            if (stopTime > 0 && s.dt * tick > stopTime)
            {
                break;
            }
        }
        BOOST_CHECK_LE(std::abs(s.curvel - s.targvel), 0.001); // allow error of 0.5729577951308232 deg
    }
    std::cout << "bounds tests\n";
    std::cout << "TODO\n";
    ///TODO

    std::cout << "done positionThroughVelocityControlWithAccelerationBounds \n";
}

BOOST_AUTO_TEST_CASE(VelocityControllerWithRampedAccelerationTest)
{
    return;
    std::cout << "starting VelocityControllerWithRampedAccelerationTest \n";
    Simulation s;
    s.posHi = 0;
    s.posLo = 0;


    //    double p = 20.5;
    VelocityControllerWithRampedAcceleration ctrl;

    auto testTick = [&]
    {
        ctrl.dt = s.dt;
        ctrl.maxDt = s.maxDt;
        //        ctrl.currentV = s.curvel;
        //        ctrl.currentAcc = s.curacc;
        ctrl.maxV = s.maxvel;
        ctrl.targetV = s.targvel;
        //        ctrl.accuracy = 0.00001;
        //        ctrl.pControlPosErrorLimit = pControlPosErrorLimit;
        //        ctrl.pControlVelLimit = pControlVelLimit;
        //        ctrl.p = p;//ctrl.calculateProportionalGain();
        //        ctrl.usePIDAtEnd = false;
        //        ARMARX_INFO << VAROUT(ctrl.p);
        BOOST_CHECK(ctrl.validParameters());
        auto r = ctrl.run();
        //        ARMARX_INFO << "State: " << (int)ctrl.calcState() << " " << VAROUT(r.acceleration) << VAROUT(r.velocity) << VAROUT(r.jerk);
        s.updateVel(r.velocity);
        ctrl.currentV = r.velocity;
        ctrl.currentAcc = r.acceleration;
        //s.updateVel(positionThroughVelocityControlWithAccelerationBounds(
        //s.dt, s.maxDt,
        //s.curvel, s.maxvel,
        //s.acc, s.dec,
        //s.curpos, s.targpos,
        //pControlPosErrorLimit, pControlVelLimit, p
        //));
        //        s.checkVel();
        //        s.checkAcc();
    };

    std::cout << "random tests\n";
    for (std::size_t try_ = 0; try_ < tries; ++ try_)
    {
        ctrl = VelocityControllerWithRampedAcceleration();
        s.writeHeader("RampedAccVelCtrl+acc_random_" + to_string(try_));
        s.maxvel = std::abs(s.vDist(gen)) + 1;
        s.curvel = armarx::math::MathUtils::LimitTo(s.vDist(gen), s.maxvel);
        ctrl.currentV = s.curvel;
        s.curpos = 1;//s.pDist(gen);
        ctrl.currentAcc = s.pDist(gen);
        ARMARX_INFO << VAROUT(s.dt) << VAROUT(s.curpos) << VAROUT(s.targvel) << " " << VAROUT(s.acc) << VAROUT(s.dec) << VAROUT(s.maxvel) << VAROUT(s.curvel) << VAROUT(s.jerk);
        //        p = ((std::min(s.acc,s.dec)*s.maxDt*0.75-pControlVelLimit)/pControlPosErrorLimit) * 0.99f; // sometimes <0
        ctrl.jerk = (random() % 100) + 10;
        s.log();
        double stopTime = -1;
        for (std::size_t tick = 0; tick < ticks; ++tick)
        {
            //            std::cout << std::endl;
            //            s.targpos = tick > 900 ? 3 : 5;

            s.tick(testTick);
            //            ARMARX_INFO  << /*deactivateSpam(0.01) <<*/ VAROUT(s.dt * tick) << VAROUT(s.curpos) << VAROUT(s.targvel) << VAROUT(s.curacc) << VAROUT(s.maxvel) << VAROUT(s.curvel) << VAROUT(s.jerk);
            if (std::abs(s.curvel - s.targvel) < 0.001 && stopTime < 0)
            {
                stopTime = s.dt * (tick + 20); // let it run for some more ticks to make sure the velocity is stable
            }
            if (stopTime > 0 && s.dt * tick > stopTime)
            {
                break;
            }
        }
        BOOST_CHECK_LE(std::abs(s.curvel - s.targvel), 0.001); // allow error of 0.5729577951308232 deg
    }
    std::cout << "bounds tests\n";
    std::cout << "TODO\n";
    ///TODO

    std::cout << "done positionThroughVelocityControlWithAccelerationBounds \n";
}


//BOOST_AUTO_TEST_CASE(velocityControlWithAccelerationBoundsTest)
//{
//    std::cout << "starting velocityControlWithAccelerationBoundsTest \n";
//    Simulation s;
//    s.posHi = 0;
//    s.posLo = 0;
//    s.posHiHard = 0;
//    s.posLoHard = 0;

//    double directSetVLimit = 0.005;


//    auto testTick = [&]
//    {
//        VelocityControllerWithAccelerationBounds ctrl;
//        ctrl.dt = s.dt;
//        ctrl.maxDt = s.maxDt;
//        ctrl.currentV = s.curvel;
//        ctrl.targetV = s.targvel;
//        ctrl.maxV = s.maxvel;
//        ctrl.acceleration = s.acc;
//        ctrl.deceleration = s.dec;
//        ctrl.directSetVLimit = directSetVLimit;
//        BOOST_CHECK(ctrl.validParameters());
//        //    s.updateVel(velocityControlWithAccelerationBounds(s.dt, s.maxDt, s.curvel, s.targvel, s.maxvel, s.acc, s.dec, directSetVLimit));
//        s.updateVel(ctrl.run());
//        s.checkVel();
//        s.checkAcc();
//    };

//    std::cout << "random tests\n";
//    for (std::size_t try_ = 0; try_ < tries; ++ try_)
//    {
//        s.writeHeader("velCtrl+acc_random_" + to_string(try_));
//        s.curvel = s.vDist(gen);
//        s.targvel = s.vDist(gen);
//        s.acc = s.aDist(gen);
//        s.dec = s.aDist(gen);
//        directSetVLimit = (std::min(s.acc, s.dec) * s.maxDt * 0.75f / 2.f) * 0.99f; // vlimit = accmin*dtmin/2

//        s.log();
//        for (std::size_t tick = 0; tick < ticks; ++tick)
//        {
//            s.tick(testTick);
//        }
//        BOOST_CHECK(std::abs(s.curvel - s.targvel) < 0.001);
//    }
//    std::cout << "bounds tests\n";
//    std::cout << "TODO\n";
//    ///TODO

//    std::cout << "done velocityControlWithAccelerationBoundsTest \n";
//}

//BOOST_AUTO_TEST_CASE(velocityControlWithAccelerationAndPositionBoundsTest)
//{
//    std::cout << "starting velocityControlWithAccelerationAndPositionBoundsTest \n";
//    Simulation s;

//    //    const double positionSoftLimitViolationVelocity = 0.1;
//    double directSetVLimit = 0.005;
//    s.posLo = s.posLoHard * 0.99;
//    s.posHi = s.posHiHard * 0.99;

//    auto testTick = [&]
//    {
//        VelocityControllerWithAccelerationAndPositionBounds ctrl;
//        ctrl.dt = s.dt;
//        ctrl.maxDt = s.maxDt;
//        ctrl.currentV = s.curvel;
//        ctrl.targetV = s.targvel;
//        ctrl.maxV = s.maxvel;
//        ctrl.acceleration = s.acc;
//        ctrl.deceleration = s.dec;
//        ctrl.directSetVLimit = directSetVLimit;
//        ctrl.currentPosition = s.curpos;
//        ctrl.positionLimitLoSoft = s.posLo;
//        ctrl.positionLimitHiSoft = s.posHi;
//        BOOST_CHECK(ctrl.validParameters());
//        s.updateVel(ctrl.run());
//        //s.updateVel(velocityControlWithAccelerationAndPositionBounds(
//        //s.dt, s.maxDt,
//        //s.curvel, s.targvel, s.maxvel,
//        //s.acc, s.dec,
//        //directSetVLimit,
//        //s.curpos,
//        //s.posLo, s.posHi,
//        ////            positionSoftLimitViolationVelocity,
//        //s.posLoHard, s.posHiHard
//        //));
//        s.checkPos();
//        s.checkVel();
//    };

//    std::cout << "random tests\n";
//    for (std::size_t try_ = 0; try_ < tries; ++ try_)
//    {
//        s.writeHeader("velCtrl+acc+pos_random_" + to_string(try_));
//        s.curvel = s.vDist(gen);
//        s.curpos = s.pDist(gen);
//        s.targvel = s.vDist(gen);
//        s.acc = s.aDist(gen);
//        s.dec = s.aDist(gen);
//        directSetVLimit = (std::min(s.acc, s.dec) * s.maxDt * 0.75f / 2.f) * 0.99f; // vlimit = accmin*dtmin/2
//        s.log();
//        for (std::size_t tick = 0; tick < ticks; ++tick)
//        {
//            s.tick(testTick);
//        }
//    }
//    std::cout << "bounds tests\n";
//    std::cout << "TODO\n";
//    ///TODO

//    std::cout << "done velocityControlWithAccelerationAndPositionBoundsTest \n";
//}

//BOOST_AUTO_TEST_CASE(positionThroughVelocityControlWithConstantJerkTest)
//{
//    return;

//    std::cout << "starting positionThroughVelocityControlWithConstantJerk \n";
//    Simulation s;
//    s.posHi = 0;
//    s.posLo = 0;


//    double p = 20.5;
//    PositionThroughVelocityControllerWithAccelerationRamps ctrl;

//    auto testTick = [&]
//    {
//        ctrl.dt = s.dt;
//        ctrl.maxDt = s.maxDt;
//        //        ctrl.currentV = s.curvel;
//        //        ctrl.currentAcc = s.curacc;
//        ctrl.maxV = s.maxvel;
//        ctrl.acceleration = s.acc;
//        ctrl.deceleration = s.dec;
//        ctrl.currentPosition = s.curpos;
//        ctrl.setTargetPosition(s.targpos);
//        ctrl.accuracy = 0.00001;
//        ctrl.jerk = 100;
//        //        ctrl.pControlPosErrorLimit = pControlPosErrorLimit;
//        //        ctrl.pControlVelLimit = pControlVelLimit;
//        ctrl.p = p;//ctrl.calculateProportionalGain();
//        ctrl.usePIDAtEnd = false;
//        //        ARMARX_INFO << VAROUT(ctrl.p);
//        BOOST_CHECK(ctrl.validParameters());
//        auto r = ctrl.run();
//        //        ARMARX_INFO << "State: " << (int)ctrl.calcState() << " " << VAROUT(r.acceleration) << VAROUT(r.velocity) << VAROUT(r.jerk);
//        s.updateVel(r.velocity);
//        ctrl.currentV = r.velocity;
//        ctrl.currentAcc = r.acceleration;
//        //s.updateVel(positionThroughVelocityControlWithAccelerationBounds(
//        //s.dt, s.maxDt,
//        //s.curvel, s.maxvel,
//        //s.acc, s.dec,
//        //s.curpos, s.targpos,
//        //pControlPosErrorLimit, pControlVelLimit, p
//        //));
//        s.checkVel();
//        s.checkAcc();
//    };

//    std::cout << "random tests\n";
//    for (std::size_t try_ = 0; try_ < tries; ++ try_)
//    {
//        ctrl = PositionThroughVelocityControllerWithAccelerationRamps();
//        s.writeHeader("posViaVelCtrl+acc_random_" + to_string(try_));
//        s.maxvel = 1.49;//std::abs(s.vDist(gen)) + 1;
//        s.curvel = -1.49;//armarx::math::MathUtils::LimitTo(s.vDist(gen), s.maxvel);
//        ctrl.currentV = s.curvel;
//        s.curpos = 1;//s.pDist(gen);
//        //        s.targpos = 5;//s.pDist(gen);
//        s.acc = 10;//s.aDist(gen);
//        s.dec = 20;//s.aDist(gen);
////        ARMARX_INFO << VAROUT(s.dt) << VAROUT(s.curpos) << "TargetPos: " << s.targpos << " " << VAROUT(s.acc) << VAROUT(s.dec) << VAROUT(s.maxvel) << VAROUT(s.curvel) << VAROUT(s.jerk);
////                p = ((std::min(s.acc,s.dec)*s.maxDt*0.75-pControlVelLimit)/pControlPosErrorLimit) * 0.99f; // sometimes <0
//        s.log();
//        for (std::size_t tick = 0; tick < ticks; ++tick)
//        {
////            std::cout << std::endl;
//            s.targpos = tick > 300 ? 3 : 5;

//            s.tick(testTick);
////            ARMARX_INFO  << /*deactivateSpam(0.01) <<*/ VAROUT(s.dt) << VAROUT(s.curpos) << "TargetPos: " << s.targpos << " " << VAROUT(s.curacc) << VAROUT(s.maxvel) << VAROUT(s.curvel) << VAROUT(s.jerk);
//            if (std::abs(s.curpos - s.targpos) < 0.00001 /*|| s.curpos > s.targpos*/)
//            {
//                break;
//            }
//        }
//        BOOST_CHECK_LE(std::abs(s.curpos - s.targpos), 0.00001); // allow error of 0.5729577951308232 deg
//    }
//    std::cout << "bounds tests\n";
//    std::cout << "TODO\n";
//    ///TODO

//    std::cout << "done positionThroughVelocityControlWithAccelerationBounds \n";
//}

BOOST_AUTO_TEST_CASE(MinJerkPositionControllerTest)
{

    std::cout << "starting MinJerkPositionControllerTest \n";
    Simulation s;
    s.posHi = 0;
    s.posLo = 0;


    //    double p = 20.5;
    MinJerkPositionController ctrl;

    auto testTick = [&]
    {
        ctrl.dt = s.dt;
        ctrl.maxDt = s.maxDt;
        //        ctrl.currentV = s.curvel;
        //        ctrl.currentAcc = s.curacc;
        ctrl.maxV = s.maxvel;
        ctrl.desiredDeceleration = s.dec;
        ctrl.currentPosition = s.curpos;
        ctrl.setTargetPosition(s.targpos);
        //        ctrl.accuracy = 0.00001;
        ctrl.desiredJerk = 100;
        //        ctrl.pControlPosErrorLimit = pControlPosErrorLimit;
        //        ctrl.pControlVelLimit = pControlVelLimit;
        //        ctrl.p = p;//ctrl.calculateProportionalGain();
        //        ctrl.usePIDAtEnd = false;
        //        ARMARX_INFO << VAROUT(ctrl.p);
        //        BOOST_CHECK(ctrl.validParameters());
        auto r = ctrl.run();
        //        ARMARX_INFO << "State: " << (int)ctrl.calcState() << " " << VAROUT(r.acceleration) << VAROUT(r.velocity) << VAROUT(r.jerk);
        //        s.updateVel(r.velocity);
        s.updatePos(r.position);
        ctrl.currentV = r.velocity;
        ctrl.currentAcc = r.acceleration;
        //s.updateVel(positionThroughVelocityControlWithAccelerationBounds(
        //s.dt, s.maxDt,
        //s.curvel, s.maxvel,
        //s.acc, s.dec,
        //s.curpos, s.targpos,
        //pControlPosErrorLimit, pControlVelLimit, p
        //));
        //        s.checkVel();
        //        s.checkAcc();
    };

    std::cout << "random tests\n";
    for (std::size_t try_ = 0; try_ < tries; ++ try_)
    {
        ctrl = MinJerkPositionController();
        s.writeHeader("minJerkPosCtrl+acc_random_" + to_string(try_));
        s.maxvel = std::abs(s.vDist(gen)) + 1;
        s.curvel = armarx::math::MathUtils::LimitTo(s.vDist(gen), s.maxvel);
        s.curacc = s.vDist(gen);
        s.dec = s.aDist(gen);
        ctrl.currentAcc = s.curacc;
        ctrl.currentV = s.curvel;
        s.curpos = 1;//s.pDist(gen);
        ARMARX_INFO << VAROUT(s.dt) << VAROUT(s.curpos) << "TargetPos: " << s.targpos << " " << VAROUT(s.acc) << VAROUT(s.dec) << VAROUT(s.maxvel) << VAROUT(s.curvel) << VAROUT(s.jerk);
        //        p = ((std::min(s.acc,s.dec)*s.maxDt*0.75-pControlVelLimit)/pControlPosErrorLimit) * 0.99f; // sometimes <0
        s.log();
        double stopTime = -1;
        for (std::size_t tick = 0; tick < ticks; ++tick)
        {
            //            std::cout << std::endl;
            s.targpos = tick > 900 ? 3 : 5;

            s.tick(testTick);
            //            ARMARX_INFO  << /*deactivateSpam(0.01) <<*/ VAROUT(s.dt * tick) << VAROUT(s.curpos) << "TargetPos: " << s.targpos << " " << VAROUT(s.curacc) << VAROUT(s.maxvel) << VAROUT(s.curvel) << VAROUT(s.jerk);
            if (std::abs(s.curpos - s.targpos) < 0.01 && stopTime < 0)
            {
                stopTime = s.dt * (tick + 20); // let it run for some more ticks to make sure the velocity is stable
                ARMARX_INFO << VAROUT(s.time) << VAROUT(stopTime);
            }
            if (stopTime > 0 && s.dt * tick > stopTime)
            {
                break;
            }
        }
        BOOST_CHECK_LE(std::abs(s.curpos - s.targpos), 0.01); // allow error of 0.5729577951308232 deg
    }
    std::cout << "bounds tests\n";
    std::cout << "TODO\n";
    ///TODO

    std::cout << "done positionThroughVelocityControlWithAccelerationBounds \n";
}



//BOOST_AUTO_TEST_CASE(positionThroughVelocityControlWithAccelerationBoundsTest)
//{
//    return;
//    std::cout << "starting positionThroughVelocityControlWithAccelerationBounds \n";
//    Simulation s;
//    s.posHi = 0;
//    s.posLo = 0;


//    double p = 0.5;

//    auto testTick = [&]
//    {
//        PositionThroughVelocityControllerWithAccelerationBounds ctrl;
//        ctrl.dt = s.dt;
//        ctrl.maxDt = s.maxDt;
//        ctrl.currentV = s.curvel;
//        //        ctrl.currentAcc = s.curacc;
//        ctrl.maxV = s.maxvel;
//        ctrl.acceleration = s.acc;
//        ctrl.deceleration = s.dec;
//        ctrl.currentPosition = s.curpos;
//        ctrl.targetPosition = s.targpos;
////        ctrl.accuracy = 0.0;
//        //        ctrl.jerk = 100;
//        //        ctrl.pControlPosErrorLimit = pControlPosErrorLimit;
//        //        ctrl.pControlVelLimit = pControlVelLimit;
//        ctrl.pid->Kp = p;
//        BOOST_CHECK(ctrl.validParameters());
//        auto result = ctrl.run();
//        s.updateVel(result);
//        //s.updateVel(positionThroughVelocityControlWithAccelerationBounds(
//        //s.dt, s.maxDt,
//        //s.curvel, s.maxvel,
//        //s.acc, s.dec,
//        //s.curpos, s.targpos,
//        //pControlPosErrorLimit, pControlVelLimit, p
//        //));
//        s.checkVel();
//        s.checkAcc();
//    };

//    std::cout << "random tests\n";
//    for (std::size_t try_ = 0; try_ < tries; ++ try_)
//    {
//        s.writeHeader("posViaVelCtrl+acc_random_" + to_string(try_));
//        s.maxvel = std::abs(s.vDist(gen)) + 1;
//        s.curvel = //armarx::math::MathUtils::LimitTo(s.vDist(gen), s.maxvel);
//        s.curpos = s.pDist(gen);
//        s.targpos = s.pDist(gen);
//        s.acc = s.aDist(gen);
//        s.dec = s.aDist(gen);
//        ARMARX_INFO << VAROUT(s.dt) << VAROUT(s.curpos) << "TargetPos: " << s.targpos << VAROUT(s.acc) << VAROUT(s.dec) << VAROUT(s.maxvel) << VAROUT(s.curvel);
//        //        p = ((std::min(s.acc,s.dec)*s.maxDt*0.75-pControlVelLimit)/pControlPosErrorLimit) * 0.99f; // sometimes <0
//        s.log();
//        for (std::size_t tick = 0; tick < ticks; ++tick)
//        {
//            s.tick(testTick);
////            ARMARX_INFO  << /*deactivateSpam(0.01) <<*/ VAROUT(s.dt) << VAROUT(s.curpos) << "TargetPos: " << s.targpos << VAROUT(s.curacc) << VAROUT(s.maxvel) << VAROUT(s.curvel);
//            if (std::abs(s.curpos - s.targpos) < 0.01)
//            {
//                break;
//            }
//        }
//        BOOST_CHECK_LE(std::abs(s.curpos - s.targpos), 0.01); // allow error of 0.5729577951308232 deg
//    }
//    std::cout << "bounds tests\n";
//    std::cout << "TODO\n";
//    ///TODO

//    std::cout << "done positionThroughVelocityControlWithAccelerationBounds \n";
//}

//BOOST_AUTO_TEST_CASE(positionThroughVelocityControlWithAccelerationAndPositionBoundsTest)
//{
//    std::cout << "starting positionThroughVelocityControlWithAccelerationAndPositionBounds \n";
//    Simulation s;
//    s.posHi = 0;
//    s.posLo = 0;

//    double p = 0.5;


//    auto testTick = [&]
//    {
//        PositionThroughVelocityControllerWithAccelerationAndPositionBounds ctrl;
//        ctrl.dt = s.dt;
//        ctrl.maxDt = s.maxDt;
//        ctrl.currentV = s.curvel;
//        ctrl.maxV = s.maxvel;
//        ctrl.acceleration = s.acc;
//        ctrl.deceleration = s.dec;
//        ctrl.currentPosition = s.curpos;
//        ctrl.targetPosition = s.targpos;
//        ctrl.pid->Kp = p;
//        ctrl.positionLimitLo = s.posLoHard;
//        ctrl.positionLimitHi = s.posHiHard;
//        BOOST_REQUIRE(ctrl.validParameters());
//        s.updateVel(ctrl.run());
//        //s.updateVel(positionThroughVelocityControlWithAccelerationAndPositionBounds(
//        //s.dt, s.maxDt,
//        //s.curvel, s.maxvel,
//        //s.acc, s.dec,
//        //s.curpos, s.targpos,
//        //pControlPosErrorLimit, pControlVelLimit, p,
//        //s.posLoHard, s.posHiHard));
//        s.checkPos();
//    };

//    std::cout << "random tests\n";
//    for (std::size_t try_ = 0; try_ < tries; ++ try_)
//    {
//        s.writeHeader("posViaVelCtrl+acc+pos_random_" + to_string(try_));
//        s.curvel = s.vDist(gen);
//        s.curpos = s.pDist(gen);
//        s.targpos = s.pDist(gen);
//        s.acc = s.aDist(gen);
//        s.dec = s.aDist(gen);
//        //        p = ((std::min(s.acc,s.dec)*s.maxDt*0.75-pControlVelLimit)/pControlPosErrorLimit) * 0.99f; // sometimes <0
//        s.log();
//        for (std::size_t tick = 0; tick < ticks; ++tick)
//        {
//            s.tick(testTick);
//        }
//        BOOST_CHECK_LE(std::abs(s.curpos - s.targpos), 0.05);
//    }
//    std::cout << "bounds tests\n";
//    std::cout << "TODO\n";
//    ///TODO

//    std::cout << "done positionThroughVelocityControlWithAccelerationAndPositionBounds \n";
//}

////BOOST_AUTO_TEST_CASE(positionThroughVelocityControlWithAccelerationBoundsAndPeriodicPositionTest)
////{
////    std::cout << "starting positionThroughVelocityControlWithAccelerationBoundsAndPeriodicPosition \n";
////    Simulation s;
////    s.posHi = 0;
////    s.posLo = 0;
////    //    double p = 0.05;
////    //    const double pControlPosErrorLimit = 0.02;
////    //    const double pControlVelLimit = 0.02;
////    double p = 0.1;
////    const double pControlPosErrorLimit = 0.01;
////    const double pControlVelLimit = 0.01;
////    double direction;

////    auto testTick = [&]
////    {
////        PositionThroughVelocityControllerWithAccelerationAndPositionBounds ctrl;
////        ctrl.dt = s.dt;
////        ctrl.maxDt = s.maxDt;
////        ctrl.currentV = s.curvel;
////        ctrl.maxV = s.maxvel;
////        ctrl.acceleration = s.acc;
////        ctrl.deceleration = s.dec;
////        ctrl.currentPosition = s.curpos;
////        ctrl.targetPosition = s.targpos;
////        ctrl.pControlPosErrorLimit = pControlPosErrorLimit;
////        ctrl.pControlVelLimit = pControlVelLimit;
////        ctrl.p = p;
////        ctrl.positionLimitLo = s.posLoHard;
////        ctrl.positionLimitHi = s.posHiHard;
////        BOOST_CHECK(ctrl.validParameters());
////        s.updateVel(ctrl.run());
////        //s.updateVel(positionThroughVelocityControlWithAccelerationBoundsAndPeriodicPosition(
////        //s.dt, s.maxDt,
////        //s.curvel, s.maxvel,
////        //s.acc, s.dec,
////        //s.curpos, s.targpos,
////        //pControlPosErrorLimit, pControlVelLimit, p,
////        //direction,
////        //s.posLoHard, s.posHiHard));
////        s.checkVel();
////        s.checkAcc();
////        s.curpos = periodicClamp(s.curpos, s.posLoHard, s.posHiHard);
////    };

////    auto rngTestRuns = [&](int d)
////    {
////        std::cout << "random tests (dir=" << to_string(d) << "\n";
////        for (std::size_t try_ = 0; try_ < tries; ++ try_)
////        {
////            s.writeHeader("posViaVelCtrl+acc+periodicPos+dir" + to_string(d) + "_random_" + to_string(try_));
////            direction = d;
////            s.curvel = s.vDist(gen);
////            s.curpos = periodicClamp(s.pDist(gen), s.posLoHard, s.posHiHard);
////            s.targpos = periodicClamp(s.pDist(gen), s.posLoHard, s.posHiHard);
////            s.acc = s.aDist(gen);
////            s.dec = s.aDist(gen);
////            s.log();
////            for (std::size_t tick = 0; tick < ticks; ++tick)
////            {
////                s.tick(testTick);
////            }
////            BOOST_CHECK_LE(std::abs(s.curpos - s.targpos), 0.01);
////            BOOST_CHECK_LE(std::abs(s.curvel), 0.1);
////        }

////    };
////    rngTestRuns(0);

////    std::cout << "bounds tests\n";
////    std::cout << "TODO\n";
////    ///TODO

////    std::cout << "done positionThroughVelocityControlWithAccelerationBoundsAndPeriodicPosition \n";
////}

