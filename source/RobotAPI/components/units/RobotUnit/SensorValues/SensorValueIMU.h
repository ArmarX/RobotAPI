/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "SensorValueBase.h"

#include <ArmarXCore/core/util/algorithm.h>
#include <RobotAPI/libraries/core/Pose.h>

namespace armarx
{
    class SensorValueIMU :
        virtual public SensorValueBase
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION
        Eigen::Vector3f angularVelocity = Eigen::Vector3f::Zero();
        Eigen::Vector3f linearAcceleration = Eigen::Vector3f::Zero();
        Eigen::Quaternionf orientation = Eigen::Quaternionf::Identity();

        static SensorValueInfo<SensorValueIMU> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValueIMU> svi;
            svi.addMemberVariable(&SensorValueIMU::angularVelocity, "angularVelocity");
            svi.addMemberVariable(&SensorValueIMU::linearAcceleration, "linearAcceleration");
            svi.addMemberVariable(&SensorValueIMU::orientation, "orientation");
            return svi;
        }
    };
}
