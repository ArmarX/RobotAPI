/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "SensorValueBase.h"

#include <chrono>

#include <ArmarXCore/observers/variant/TimestampVariant.h>

namespace armarx
{
    class SensorValueRTThreadTimings : public SensorValueBase
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

        IceUtil::Time rtSwitchControllerSetupDuration;
        IceUtil::Time rtSwitchControllerSetupRoundTripTime;

        IceUtil::Time rtRunNJointControllersDuration;
        IceUtil::Time rtRunNJointControllersRoundTripTime;

        IceUtil::Time rtHandleInvalidTargetsDuration;
        IceUtil::Time rtHandleInvalidTargetsRoundTripTime;

        IceUtil::Time rtRunJointControllersDuration;
        IceUtil::Time rtRunJointControllersRoundTripTime;

        IceUtil::Time rtBusSendReceiveDuration;
        IceUtil::Time rtBusSendReceiveRoundTripTime;

        IceUtil::Time rtReadSensorDeviceValuesDuration;
        IceUtil::Time rtReadSensorDeviceValuesRoundTripTime;

        IceUtil::Time rtUpdateSensorAndControlBufferDuration;
        IceUtil::Time rtUpdateSensorAndControlBufferRoundTripTime;

        IceUtil::Time rtResetAllTargetsDuration;
        IceUtil::Time rtResetAllTargetsRoundTripTime;

        IceUtil::Time rtLoopDuration;
        IceUtil::Time rtLoopRoundTripTime;

        static SensorValueInfo<SensorValueRTThreadTimings> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValueRTThreadTimings> svi;
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtSwitchControllerSetupDuration,             "rtSwitchControllerSetup.Duration");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtSwitchControllerSetupRoundTripTime,        "rtSwitchControllerSetup.RoundTripTime");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtRunNJointControllersDuration,              "rtRunNJointControllers.Duration");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtRunNJointControllersRoundTripTime,         "rtRunNJointControllers.RoundTripTime");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtHandleInvalidTargetsDuration,              "rtHandleInvalidTargets.Duration");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtHandleInvalidTargetsRoundTripTime,         "rtHandleInvalidTargets.RoundTripTime");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtRunJointControllersDuration,               "rtRunJointControllers.Duration");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtRunJointControllersRoundTripTime,          "rtRunJointControllers.RoundTripTime");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtBusSendReceiveDuration,                    "rtBusSendReceive.Duration");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtBusSendReceiveRoundTripTime,               "rtBusSendReceive.RoundTripTime");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtReadSensorDeviceValuesDuration,            "rtReadSensorDeviceValues.Duration");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtReadSensorDeviceValuesRoundTripTime,       "rtReadSensorDeviceValues.RoundTripTime");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtUpdateSensorAndControlBufferDuration,      "rtUpdateSensorAndControlBuffer.Duration");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtUpdateSensorAndControlBufferRoundTripTime, "rtUpdateSensorAndControlBuffer.RoundTripTime");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtResetAllTargetsDuration,                   "rtResetAllTargets.Duration");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtResetAllTargetsRoundTripTime,              "rtResetAllTargets.RoundTripTime");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtLoopDuration,                              "rtLoop.Duration");
            svi.addMemberVariable(&SensorValueRTThreadTimings::rtLoopRoundTripTime,                         "rtLoop.RoundTripTime");
            return svi;
        }
    };
}
