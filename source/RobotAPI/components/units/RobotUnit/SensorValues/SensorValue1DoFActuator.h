/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "SensorValueBase.h"

#include <RobotAPI/interface/units/KinematicUnitInterface.h>

namespace armarx
{

#define make_SensorValue1DoFActuator(type, name, varname)       \
    class name : virtual public SensorValueBase                 \
    {                                                           \
    public:                                                     \
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION    \
        type varname = 0.0f;                                   \
        static SensorValueInfo<name> GetClassMemberInfo()       \
        {                                                       \
            SensorValueInfo<name> svi;                          \
            svi.addMemberVariable(&name::varname, #varname);    \
            return svi;                                         \
        }                                                       \
    }

    make_SensorValue1DoFActuator(float, SensorValue1DoFActuatorPosition, position);
    make_SensorValue1DoFActuator(float, SensorValue1DoFActuatorVelocity, velocity);
    make_SensorValue1DoFActuator(float, SensorValue1DoFActuatorFilteredVelocity, filteredvelocity);
    make_SensorValue1DoFActuator(float, SensorValue1DoFActuatorAcceleration, acceleration);
    make_SensorValue1DoFActuator(float, SensorValue1DoFActuatorTorque, torque);
    make_SensorValue1DoFActuator(float, SensorValue1DoFGravityTorque, gravityTorque);
    make_SensorValue1DoFActuator(float, SensorValue1DoFActuatorCurrent, motorCurrent);
    make_SensorValue1DoFActuator(float, SensorValue1DoFActuatorMotorTemperature, motorTemperature);
    make_SensorValue1DoFActuator(std::int16_t, SensorValue1DoFMotorPWM, motorPWM);

#undef make_SensorValue1DoFActuator

    class SensorValue1DoFInverseDynamicsTorque :
        virtual public SensorValue1DoFGravityTorque
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION
        float inverseDynamicsTorque = 0.0f;
        float inertiaTorque = 0.0f;
        static SensorValueInfo<SensorValue1DoFInverseDynamicsTorque> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValue1DoFInverseDynamicsTorque> svi;
            svi.addBaseClass<SensorValue1DoFGravityTorque>();
            svi.addMemberVariable(&SensorValue1DoFInverseDynamicsTorque::inverseDynamicsTorque, "inverseDynamicsTorque");
            svi.addMemberVariable(&SensorValue1DoFInverseDynamicsTorque::inertiaTorque, "inertiaTorque");
            return svi;
        }
    };


    class SensorValue1DoFActuator :
        virtual public SensorValue1DoFActuatorPosition,
        virtual public SensorValue1DoFActuatorVelocity,
        virtual public SensorValue1DoFActuatorAcceleration,
        virtual public SensorValue1DoFActuatorTorque,
        virtual public SensorValue1DoFInverseDynamicsTorque
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION
        static SensorValueInfo<SensorValue1DoFActuator> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValue1DoFActuator> svi;
            svi.addBaseClass<SensorValue1DoFActuatorPosition>();
            svi.addBaseClass<SensorValue1DoFActuatorVelocity>();
            svi.addBaseClass<SensorValue1DoFActuatorAcceleration>();
            svi.addBaseClass<SensorValue1DoFActuatorTorque>();
            svi.addBaseClass<SensorValue1DoFInverseDynamicsTorque>();
            return svi;
        }
    };

    class SensorValue1DoFActuatorStatus : virtual public SensorValueBase
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION
        armarx::JointStatus status;
        static SensorValueInfo<SensorValue1DoFActuatorStatus> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValue1DoFActuatorStatus> svi;
            svi.addMemberVariable(&SensorValue1DoFActuatorStatus::status, "JointStatus")
            .setFieldNames({"JointStatusError", "JointStatusOperation", "JointStatusEnabled", "JointStatusEmergencyStop"});
            return svi;
        }
    };

    class SensorValue1DoFRealActuator :
        virtual public SensorValue1DoFActuator,
        virtual public SensorValue1DoFActuatorCurrent,
        virtual public SensorValue1DoFActuatorMotorTemperature
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION
        static SensorValueInfo<SensorValue1DoFRealActuator> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValue1DoFRealActuator> svi;
            svi.addBaseClass<SensorValue1DoFActuator>();
            svi.addBaseClass<SensorValue1DoFActuatorCurrent>();
            svi.addBaseClass<SensorValue1DoFActuatorMotorTemperature>();
            return svi;
        }
    };

    class SensorValue1DoFRealActuatorWithStatus :
        virtual public SensorValue1DoFRealActuator,
        virtual public SensorValue1DoFActuatorStatus
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION
        static SensorValueInfo<SensorValue1DoFRealActuatorWithStatus> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValue1DoFRealActuatorWithStatus> svi;
            svi.addBaseClass<SensorValue1DoFRealActuator>();
            svi.addBaseClass<SensorValue1DoFActuatorStatus>();
            return svi;
        }
    };
}
