/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <typeinfo>
#include <memory>
#include <string>
#include <map>

#include <ArmarXCore/util/CPPUtility/TemplateMetaProgramming.h>

#include <RobotAPI/components/units/RobotUnit/util/introspection/ClassMemberInfo.h>
#include <RobotAPI/components/units/RobotUnit/util/HeterogenousContinuousContainerMacros.h>

namespace armarx
{
    /**
     * @ingroup Library-RobotUnit
     * @brief The SensorValueBase class
     */
    class SensorValueBase
    {
        template<class...Ts>
        struct IsAHelper
        {
            // only called then sizeof...(Ts) == 0
            static_assert(sizeof...(Ts) == 0, "called overload for empty pack with params");
            static bool IsA(const SensorValueBase* sv)
            {
                return true;
            }
        };
        template<class T, class...Ts>
        struct IsAHelper<T, Ts...>
        {
            static bool IsA(const SensorValueBase* sv)
            {
                return dynamic_cast<const T*>(sv) && IsAHelper<Ts...>::IsA(sv);
            }
        };

    public:
        template<class DerivedClass>
        using SensorValueInfo = introspection::ClassMemberInfo<SensorValueBase, DerivedClass>;

        virtual ~SensorValueBase() = default;

        virtual std::string getSensorValueType(bool withoutNamespaceSpecifier) const = 0;

        template<class...Ts>
        bool isA() const
        {
            return IsAHelper<Ts...>::IsA(this);
        }

        template<class T>
        const T* asA() const
        {
            return dynamic_cast<const T*>(this);
        }

        template<class T>
        T* asA()
        {
            return dynamic_cast<T*>(this);
        }

        //logging functions
        /// @brief used to send the data to the DebugObserverTopic and to other Components (e.g. GUI widgets)
        virtual std::map<std::string, VariantBasePtr> toVariants(const IceUtil::Time& timestamp) const = 0;

        virtual std::size_t getNumberOfDataFields() const = 0;
        virtual std::vector<std::string> getDataFieldNames() const = 0;
        virtual void getDataFieldAs(std::size_t i, bool&        out) const = 0;
        virtual void getDataFieldAs(std::size_t i, Ice::Byte&   out) const = 0;
        virtual void getDataFieldAs(std::size_t i, Ice::Short&  out) const = 0;
        virtual void getDataFieldAs(std::size_t i, Ice::Int&    out) const = 0;
        virtual void getDataFieldAs(std::size_t i, Ice::Long&   out) const = 0;
        virtual void getDataFieldAs(std::size_t i, Ice::Float&  out) const = 0;
        virtual void getDataFieldAs(std::size_t i, Ice::Double& out) const = 0;
        virtual void getDataFieldAs(std::size_t i, std::string& out) const = 0;
        template<class T>
        T getDataFieldAs(std::size_t i) const
        {
            ARMARX_TRACE;
            T t;
            this->getDataFieldAs(i, t);
            return t;
        }
        virtual const std::type_info& getDataFieldType(std::size_t i) const = 0;

        //management functions
        template<class T, class = typename std::enable_if<std::is_base_of<SensorValueBase, T>::value>::type>
        void _copyTo(std::unique_ptr<T>& target) const
        {
            _copyTo(target.get());
        }

        ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK(
            SensorValueHasGetClassMemberInfo,
            GetClassMemberInfo, SensorValueInfo<T>(*)(void));

        ARMARX_PLACEMENT_CONSTRUCTION_HELPER_BASE(SensorValueBase)
    };
}

#define DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION                                        \
    ARMARX_PLACEMENT_CONSTRUCTION_HELPER                                                            \
    using SensorValueBase = ::armarx::SensorValueBase;                                              \
    using VariantBasePtr = ::armarx::VariantBasePtr;                                                \
    std::string getSensorValueType(bool withoutNamespaceSpecifier = false) const override           \
    {                                                                                               \
        return armarx::GetTypeString(*this, withoutNamespaceSpecifier);                             \
    }                                                                                               \
    void _check_for_static_GetClassMemberInfo_overload()                                            \
    {                                                                                               \
        static_assert(SensorValueHasGetClassMemberInfo<std::decay<decltype(*this)>::type>::value,   \
                      "This class has to implement GetClassMemberInfo() returning "                 \
                      "an instance of SensorValueInfo<THIS_CLASS_TYPE>");                           \
    }                                                                                               \
    std::map<std::string, VariantBasePtr> toVariants(const IceUtil::Time& timestamp) const override \
    {                                                                                               \
        return SensorValueInfo<std::decay<decltype(*this)>::type>::ToVariants(timestamp,this);      \
    }                                                                                               \
    std::size_t getNumberOfDataFields() const override                                              \
    {                                                                                               \
        return SensorValueInfo<std::decay<decltype(*this)>::type>::GetNumberOfDataFields();         \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, bool&        out) const override                            \
    {                                                                                               \
        SensorValueInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);          \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, Ice::Byte&   out) const override                            \
    {                                                                                               \
        SensorValueInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);          \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, Ice::Short&  out) const override                            \
    {                                                                                               \
        SensorValueInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);          \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, Ice::Int&    out) const override                            \
    {                                                                                               \
        SensorValueInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);          \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, Ice::Long&   out) const override                            \
    {                                                                                               \
        SensorValueInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);          \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, Ice::Float&  out) const override                            \
    {                                                                                               \
        SensorValueInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);          \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, Ice::Double& out) const override                            \
    {                                                                                               \
        SensorValueInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);          \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, std::string& out) const override                            \
    {                                                                                               \
        SensorValueInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);          \
    }                                                                                               \
    const std::type_info& getDataFieldType(std::size_t i) const override                            \
    {                                                                                               \
        return SensorValueInfo<std::decay<decltype(*this)>::type>::GetDataFieldType(i);             \
    }                                                                                               \
    std::vector<std::string> getDataFieldNames() const override                                     \
    {                                                                                               \
        return SensorValueInfo<std::decay<decltype(*this)>::type>::GetDataFieldNames();             \
    }

namespace armarx
{
    class SensorValueDummy : public SensorValueBase
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION
        static SensorValueInfo<SensorValueDummy> GetClassMemberInfo()
        {
            return SensorValueInfo<SensorValueDummy> {};
        }
    };
}
