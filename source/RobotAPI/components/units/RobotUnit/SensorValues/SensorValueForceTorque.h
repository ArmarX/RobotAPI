/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "Eigen/Core"
#include "SensorValueBase.h"

#include <RobotAPI/libraries/core/Pose.h>

namespace armarx
{
    class SensorValueForceTorque : public SensorValueBase
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION
        Eigen::Vector3f torque;
        Eigen::Vector3f force;
        Eigen::Vector3f gravityCompensatedTorque;
        Eigen::Vector3f gravityCompensatedForce;
        static SensorValueInfo<SensorValueForceTorque> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValueForceTorque> svi;
            svi.addMemberVariable(&SensorValueForceTorque::torque, "torque").setFieldNames({"tx", "ty", "tz"});
            svi.addMemberVariable(&SensorValueForceTorque::force, "force").setFieldNames({"fx", "fy", "fz"});
            svi.addMemberVariable(&SensorValueForceTorque::gravityCompensatedTorque, "gravityCompensatedTorque")
            .setFieldNames({"gravCompTx", "gravCompTy", "gravCompTz"});
            svi.addMemberVariable(&SensorValueForceTorque::gravityCompensatedForce, "gravityCompensatedForce")
            .setFieldNames({"gravCompFx", "gravCompFy", "gravCompFz"});
            return svi;
        }
    };
}
