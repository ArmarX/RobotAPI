#pragma once

#include <ArmarXCore/observers/Observer.h>

#include "util.h"

namespace armarx::RobotUnitModule
{
    class Publisher;
}

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(RobotUnitObserver);

    class RobotUnitObserver : public Observer
    {
    public:
        const std::string additionalChannel = "Additional";
        const std::string timingChannel = "TimingInformation";
        const std::string controlDevicesChannel = "ControlDevices";
        const std::string sensorDevicesChannel = "SensorDevices";

        RobotUnitObserver() = default;

        void offerOrUpdateDataFieldsFlatCopy_async(const std::string& channelName, StringVariantBaseMap&& valueMap);
        // Observer interface
    protected:
        void onInitObserver()    override {}
        void onConnectObserver() override;

        friend class RobotUnitModule::Publisher;

        // ManagedIceObject interface
    protected:
        std::string getDefaultName() const override
        {
            return "RobotUnitObserver";
        }
    };
}
