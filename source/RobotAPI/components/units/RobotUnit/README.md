# RobotUnit
The RobotUnit can be used for real-time control.
The central principle is that all controllers are executed synchronously.
The controllers are arranged in a 2-layer architecture.
For each actuator of the robot there are one or more so-called "joint controllers".
These can realize e.g. position control, velocity control, torque control, etc.
The joint controllers are responsible for translating the input variables into control variables for the individual joints, e.g. to calculate a target current from a position specification.
NJointControllers are arranged above the JointControllers. These accept more complex targets (e.g. a Cartesian target position for the TCP).
Each NJointController uses one or more JointControllers.
