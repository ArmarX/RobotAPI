/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ControlTargetBase
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/RobotUnit/util/introspection/ClassMemberInfo.h>
#include <RobotAPI/components/units/RobotUnit//util/HeterogenousContinuousContainerMacros.h>
#include <RobotAPI/components/units/RobotUnit/ControlModes.h>

#include <IceUtil/Time.h>
#include <Ice/Handle.h>

#include <memory>
#include <string>
#include <map>


namespace armarx
{
    typedef ::IceInternal::Handle<class VariantBase> VariantBasePtr;

    /**
    * @class ControlTargetBase
    * @ingroup Library-RobotUnit
    * @brief Brief description of class JointControlTargetBase.
    *
    * Detailed description of class ControlTargetBase.
    */
    class ControlTargetBase
    {
    public:
        ////use this class as parameter to functions that only should be called by control devices
        class ControlDeviceAccessToken
        {
            friend class ControlDevice;
            ControlDeviceAccessToken() = default;
        };

        template<class DerivedClass>
        using ControlTargetInfo = introspection::ClassMemberInfo<ControlTargetBase, DerivedClass>;

        virtual ~ControlTargetBase() = default;

        virtual std::string getControlTargetType(bool withoutNamespaceSpecifier = false) const = 0;
        virtual const std::string& getControlMode() const = 0;
        virtual void reset() = 0;
        virtual bool isValid() const = 0;

        template<class T>
        bool isA() const
        {
            return asA<T>();
        }

        template<class T>
        const T* asA() const
        {
            return dynamic_cast<const T*>(this);
        }

        template<class T>
        T* asA()
        {
            return dynamic_cast<T*>(this);
        }

        //logging functions
        /// @brief used to send the data to the DebugObserverTopic and to other Components (e.g. GUI widgets)
        virtual std::map<std::string, VariantBasePtr>   toVariants(const IceUtil::Time& timestamp) const = 0;

        virtual std::size_t getNumberOfDataFields() const = 0;
        virtual std::vector<std::string> getDataFieldNames() const = 0;
        virtual void getDataFieldAs(std::size_t i, bool&        out) const = 0;
        virtual void getDataFieldAs(std::size_t i, Ice::Byte&   out) const = 0;
        virtual void getDataFieldAs(std::size_t i, Ice::Short&  out) const = 0;
        virtual void getDataFieldAs(std::size_t i, Ice::Int&    out) const = 0;
        virtual void getDataFieldAs(std::size_t i, Ice::Long&   out) const = 0;
        virtual void getDataFieldAs(std::size_t i, Ice::Float&  out) const = 0;
        virtual void getDataFieldAs(std::size_t i, Ice::Double& out) const = 0;
        virtual void getDataFieldAs(std::size_t i, std::string& out) const = 0;
        virtual const std::type_info& getDataFieldType(std::size_t i) const = 0;

        //management functions
        template<class T, class = typename std::enable_if<std::is_base_of<ControlTargetBase, T>::value>::type>
        void _copyTo(std::unique_ptr<T>& target) const
        {
            _copyTo(target.get());
        }

        ARMARX_META_MAKE_HAS_MEMBER_FNC_CHECK(
            ControlTargetHasGetClassMemberInfo,
            GetClassMemberInfo, ControlTargetInfo<T>(*)(void));

        ARMARX_PLACEMENT_CONSTRUCTION_HELPER_BASE(ControlTargetBase)
    };

#define DETAIL_ControlTargetBase_DEFAULT_METHOD_IMPLEMENTATION                                      \
    ARMARX_PLACEMENT_CONSTRUCTION_HELPER                                                            \
    using ControlTargetBase = ::armarx::ControlTargetBase;                                          \
    using VariantBasePtr = ::armarx::VariantBasePtr;                                                \
    std::string getControlTargetType(bool withoutNamespaceSpecifier = false) const override         \
    {                                                                                               \
        return armarx::GetTypeString(*this, withoutNamespaceSpecifier);                             \
    }                                                                                               \
    void _check_for_static_GetClassMemberInfo_overload()                                            \
    {                                                                                               \
        static_assert(ControlTargetHasGetClassMemberInfo<std::decay<decltype(*this)>::type>::value, \
                      "This class has to implement GetClassMemberInfo() returning "                 \
                      "an instance of ControlTargetInfo<THIS_CLASS_TYPE>");                         \
    }                                                                                               \
    std::map<std::string, VariantBasePtr> toVariants(const IceUtil::Time& timestamp) const override \
    {                                                                                               \
        return ControlTargetInfo<std::decay<decltype(*this)>::type>::ToVariants(timestamp,this);    \
    }                                                                                               \
    std::size_t getNumberOfDataFields() const override                                              \
    {                                                                                               \
        return ControlTargetInfo<std::decay<decltype(*this)>::type>::GetNumberOfDataFields();       \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, bool&        out) const override                            \
    {                                                                                               \
        ControlTargetInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);        \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, Ice::Byte&   out) const override                            \
    {                                                                                               \
        ControlTargetInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);        \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, Ice::Short&  out) const override                            \
    {                                                                                               \
        ControlTargetInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);        \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, Ice::Int&    out) const override                            \
    {                                                                                               \
        ControlTargetInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);        \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, Ice::Long&   out) const override                            \
    {                                                                                               \
        ControlTargetInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);        \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, Ice::Float&  out) const override                            \
    {                                                                                               \
        ControlTargetInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);        \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, Ice::Double& out) const override                            \
    {                                                                                               \
        ControlTargetInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);        \
    }                                                                                               \
    void getDataFieldAs (std::size_t i, std::string& out) const override                            \
    {                                                                                               \
        ControlTargetInfo<std::decay<decltype(*this)>::type>::GetDataFieldAs (this, i, out);        \
    }                                                                                               \
    const std::type_info& getDataFieldType(std::size_t i) const override                            \
    {                                                                                               \
        return ControlTargetInfo<std::decay<decltype(*this)>::type>::GetDataFieldType(i);           \
    }                                                                                               \
    std::vector<std::string> getDataFieldNames() const override                                     \
    {                                                                                               \
        return ControlTargetInfo<std::decay<decltype(*this)>::type>::GetDataFieldNames();           \
    }

#define make_DummyControlTarget(Suffix,ControlMode)                     \
    class DummyControlTarget##Suffix : public ControlTargetBase         \
    {                                                                   \
    public:                                                             \
        virtual const std::string& getControlMode() const override      \
        {                                                               \
            return ControlMode;                                         \
        }                                                               \
        virtual void reset() override {}                                \
        virtual bool isValid() const override                           \
        {                                                               \
            return true;                                                \
        }                                                               \
        DETAIL_ControlTargetBase_DEFAULT_METHOD_IMPLEMENTATION          \
        static ControlTargetInfo<DummyControlTarget##Suffix>            \
        GetClassMemberInfo()                                            \
        {                                                               \
            return ControlTargetInfo<DummyControlTarget##Suffix> {};    \
        }                                                               \
    }
    make_DummyControlTarget(EmergencyStop, ControlModes::EmergencyStop);
    make_DummyControlTarget(StopMovement, ControlModes::StopMovement);
#undef make_DummyControlTarget
}
