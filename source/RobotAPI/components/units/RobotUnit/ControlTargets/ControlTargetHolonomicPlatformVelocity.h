/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ControlTargetHolonomicPlatformVelocity
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "ControlTargetBase.h"

#include <RobotAPI/components/units/RobotUnit/Constants.h> // for ControllerConstants::ValueNotSetNaN

namespace armarx
{
    /**
    * @class ControlTargetHolonomicPlatformVelocity
    * @ingroup Library-RobotUnit
    * @brief Brief description of class ControlTargetHolonomicPlatformVelocity.
    *
    * Detailed description of class ControlTargetHolonomicPlatformVelocity.
    */
    class ControlTargetHolonomicPlatformVelocity: public ControlTargetBase
    {
    public:
        float velocityX = ControllerConstants::ValueNotSetNaN;
        float velocityY = ControllerConstants::ValueNotSetNaN;
        float velocityRotation = ControllerConstants::ValueNotSetNaN;
        const std::string& getControlMode() const override
        {
            return ControlModes::HolonomicPlatformVelocity;
        }
        void reset() override
        {
            velocityX = ControllerConstants::ValueNotSetNaN;
            velocityY = ControllerConstants::ValueNotSetNaN;
            velocityRotation = ControllerConstants::ValueNotSetNaN;
        }
        bool isValid() const override
        {
            return std::isfinite(velocityX) && std::isfinite(velocityY) && std::isfinite(velocityRotation);
        }

        DETAIL_ControlTargetBase_DEFAULT_METHOD_IMPLEMENTATION
        static ControlTargetInfo<ControlTargetHolonomicPlatformVelocity> GetClassMemberInfo()
        {
            ControlTargetInfo<ControlTargetHolonomicPlatformVelocity> cti;
            cti.addMemberVariable(&ControlTargetHolonomicPlatformVelocity::velocityX, "velocityX");
            cti.addMemberVariable(&ControlTargetHolonomicPlatformVelocity::velocityY, "velocityY");
            cti.addMemberVariable(&ControlTargetHolonomicPlatformVelocity::velocityRotation, "velocityRotation");
            return cti;
        }
    };
}

