/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "../util.h"

namespace armarx
{
    class DeviceBase
    {
    public:
        /// @brief Create a Device with the given name
        DeviceBase(const std::string& name) : deviceName{name}
        {
        }
        virtual ~DeviceBase() = default;
        /// @return The Device's name
        const std::string& getDeviceName() const;
        /// @return The Device's name
        const char* rtGetDeviceName() const;
        //tags
        /// @return Thes set of tags for this Device
        const std::set<std::string>& getTags() const;
        /// @return Whether the device has the given tag
        bool hasTag(const std::string& tag) const;

    protected:
        /// @brief adds the given tag to the Device
        void addDeviceTag(const std::string& tag);

    private:
        std::set<std::string> tags;
        const std::string deviceName;
    };
} // namespace armarx

//inline functions
namespace armarx
{
    inline bool
    DeviceBase::hasTag(const std::string& tag) const
    {
        return getTags().count(tag);
    }

    inline void
    DeviceBase::addDeviceTag(const std::string& tag)
    {
        tags.emplace(tag);
    }

    inline const std::string&
    DeviceBase::getDeviceName() const
    {
        return deviceName;
    }

    inline const char*
    DeviceBase::rtGetDeviceName() const
    {
        return deviceName.c_str();
    }

    inline const std::set<std::string>&
    DeviceBase::getTags() const
    {
        return tags;
    }
} // namespace armarx
