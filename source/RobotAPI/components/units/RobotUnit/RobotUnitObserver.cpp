#include "RobotUnitObserver.h"

#include <ArmarXCore/observers/exceptions/user/InvalidDatafieldException.h>

namespace armarx
{
    void RobotUnitObserver::offerOrUpdateDataFieldsFlatCopy_async(const std::string& channelName, StringVariantBaseMap&& valueMap)
    {
        addWorkerJob("RobotUnitObserver::offerOrUpdateDataFieldsFlatCopy_async", [this, channelName, vmap = std::move(valueMap)]
        {
            offerOrUpdateDataFieldsFlatCopy(channelName, vmap);
        });
    }

    void RobotUnitObserver::onConnectObserver()
    {
        offerChannel(sensorDevicesChannel, "Devices that provide sensor data");
        offerChannel(controlDevicesChannel, "Devices that are controlled by JointControllers");
        offerChannel(timingChannel, "Channel for timings of the different phases of the robot unit");
        offerChannel(additionalChannel, "Additional custom datafields");
    }
}
