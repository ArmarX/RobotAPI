/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "RobotUnitModules/RobotUnitModules.h"

// This include is not necessary but a lot of other files rely on it being included here
// Can be removed if other files include what they use
#include "NJointControllers/NJointControllerBase.h"

/**
* @defgroup Library-RobotUnit RobotUnit
* @ingroup RobotAPI
* A description of the library RobotUnit.
*/

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(RobotUnit);

    /**
     * @ingroup Library-RobotUnit
     * @brief
     */
    class RobotUnitPropertyDefinitions:
        public RobotUnitModule::UnitsPropertyDefinitions,
        public RobotUnitModule::DevicesPropertyDefinitions,
        public RobotUnitModule::LoggingPropertyDefinitions,
        public RobotUnitModule::RobotDataPropertyDefinitions,
        public RobotUnitModule::PublisherPropertyDefinitions,
        public RobotUnitModule::ManagementPropertyDefinitions,
        public RobotUnitModule::ControlThreadPropertyDefinitions,
        public RobotUnitModule::SelfCollisionCheckerPropertyDefinitions
    {
    public:
        RobotUnitPropertyDefinitions(std::string prefix):
            UnitsPropertyDefinitions(prefix),
            DevicesPropertyDefinitions(prefix),
            LoggingPropertyDefinitions(prefix),
            RobotDataPropertyDefinitions(prefix),
            PublisherPropertyDefinitions(prefix),
            ManagementPropertyDefinitions(prefix),
            ControlThreadPropertyDefinitions(prefix),
            SelfCollisionCheckerPropertyDefinitions(prefix)
        {}
    };
}

namespace armarx
{
    /**
     * @ingroup Library-RobotUnit
     * @brief The RobotUnit class manages a robot and its controllers.
     *
     * The \ref RobotUnit manages \ref NJointControllerBase and \ref JointController for a robot.
     * Controllers are executed in a control thread.
     *
     * \section Controllers
     *
     * \subsection RobotUnitCtrlsJoint Joint Controller
     *
     * \subsection RobotUnitCtrlsNJoint Multi Joint Controller
     *
     * \subsection RobotUnitControlModes Control Modes
     * \subsubsection RobotUnitCtrlModesJointCtrl Control Modes of JointControlers
     * \subsubsection RobotUnitCtrlModesHWCtrl Hardware Control Modes of JointControlers
     * \subsubsection RobotUnitCtrlModeGroups Device Groups of Control Modes
     *
     * \section Units
     * Units are created on an as needed basis.
     * (e.g. If there is a platform, a \ref PlatformSubUnit is created)
     *
     * Available units are
     *
     *  - \ref KinematicSubUnit
     *  - \ref PlatformSubUnit
     *  - \ref ForceTorqueSubUnit
     *
     * \section Publishing
     * \section RtLogging
     * Since the control thread (Section \ref softrt) should adhere soft rt properties,
     * no function executed in it is allowed to block.
     * Hence logging of messages via armarx logging streams (e.g. \ref ARMARX_INFO) is forbidden.
     *
     * There are non-blocking macros to log messages in the control thread:
     *
     *  - \ref ARMARX_RT_LOGF_DEBUG
     *  - \ref ARMARX_RT_LOGF_VERBOSE
     *  - \ref ARMARX_RT_LOGF_INFO
     *  - \ref ARMARX_RT_LOGF_IMPORTANT
     *  - \ref ARMARX_RT_LOGF_WARN
     *  - \ref ARMARX_RT_LOGF_ERROR
     *  - \ref ARMARX_RT_LOGF_FATAL
     *
     * \warning These functions must only be called in the control thread after calling armarx::RobotUnit::finishControlThreadInitialization
     *
     * These macros work similar to printf and write their content to the corresponding
     * armarx logging stream.
     *
     * The stream returns an object of the type \ref armarx::detail::RtMessageLogEntryBase.
     * It is possible to deactivate spam on these streams by calling
     * \ref armarx::detail::RtMessageLogEntryBase::deactivateSpam .
     *
     * E.g.
     * \code
            // prints every 1.5 seconds the message "foo 42 bar 24" to ARMARX_IMPORTANT
            ARMARX_RT_LOGF_IMPORTANT("foo %d bar %d\n", 42, 24).deactivateSpam(1.5);
     * \endcode
     * \note It is not possible to print out strings this would need memory allocation.
     *
     * If an error occours, it is possible to print all recent messages (even the deactivated messages)
     * to a file by calling the ice interface function \ref RobotUnitInterface::writeRecentIterationsToFile.
     * The property "RTLoggingKeepIterationsForMs" decides how long all logging messages are keept (default 60 seconds).
     * The \ref RobotUnitGui has a button to call this function.
     *
     * \section Threads
     * This section explains the running threads and their responsibilities.
     * \subsection ctrlthrd Control Thread
     * This thread should be able to adhere soft rt if PREEMPT_RT is used.
     *
     * Its tasks are:
     *
     *  - Communication with the Robot
     *  - Execution of \ref NJointControllerBase and \ref JointController
     *  - Transferring data (\ref SensorValues, \ref ControlTargets) to other threads
     *    (publishing, units, logging)
     *
     * \subsubsection softrt Soft rt
     *
     * To adhere soft rt, the following actions are forbidden (an example and the reason are in parentheses):
     *
     *  - Blocking operations (e.g. lock on a mutex) (can block the thread for a long time)
     *  - Writing data to an armarx stream (e.g. \ref ARMARX_INFO << "foo") (is blocking)
     *  - Allocating / freeing memory on the heap (e.g. std::vector<int>(2,2) ) (is blocking)
     *  - Writing data to a stream (e.g. std::cout << "bar") (may allocate memory)
     *  - Making remote calls (e.g. via ICE) (is blocking)
     *
     * \subsection pubthrd Publish Thread
     *
     * Its tasks are:
     *
     *  - Update Units
     *  - Call publishing hooks of \ref NJointControllerBase
     *  - Publish data to \ref RobotUnitListener
     *
     * \subsection logthrd RtLogging Thread
     * If this thread is deactivated, no Rt Logging is possible and
     * no messages from the control Thread are printed.
     *
     * Its tasks are:
     *
     *  - Log SensorValues and ControlTargets to files
     *  - Print messages from the Control Thread
     *
     * \subsection icethrd ICE Threads
     *
     *  - Receive ICE calls
     */
    class RobotUnit :
        virtual public RobotUnitInterface,
        virtual public RobotUnitModule::Units,
        virtual public RobotUnitModule::Logging,
        virtual public RobotUnitModule::Devices,
        virtual public RobotUnitModule::Publisher,
        virtual public RobotUnitModule::RobotData,
        virtual public RobotUnitModule::Management,
        virtual public RobotUnitModule::ControlThread,
        virtual public RobotUnitModule::SelfCollisionChecker,
        virtual public RobotUnitModule::ControllerManagement,
        virtual public RobotUnitModule::ControlThreadDataBuffer
    {
    public:
        ~RobotUnit();

        static RobotUnit& Instance()
        {
            return ModuleBase::Instance<RobotUnit>();
        }
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return PropertyDefinitionsPtr(new RobotUnitPropertyDefinitions(getConfigIdentifier()));
        }
    };
}
