/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <string>
#include <set>

#include <VirtualRobot/Robot.h>

#include <ArmarXGui/interface/WidgetDescription.h>
#include <ArmarXGui/libraries/DefaultWidgetDescriptions/DefaultWidgetDescriptions.h>

namespace armarx::WidgetDescription
{
    StringComboBoxPtr makeStringSelectionComboBox(
        std::string name,
        std::vector<std::string> options);

    StringComboBoxPtr makeStringSelectionComboBox(
        std::string name,
        std::vector<std::string> options,
        const std::set<std::string>& preferredSet);

    inline StringComboBoxPtr makeStringSelectionComboBox(
        std::string name,
        std::vector<std::string> options,
        const std::initializer_list<std::string>& preferredSet)
    {
        return makeStringSelectionComboBox(std::move(name), std::move(options), std::set<std::string> {preferredSet});
    }

    StringComboBoxPtr makeStringSelectionComboBox(
        std::string name,
        std::vector<std::string> options,
        const std::string& mostPreferred);

    StringComboBoxPtr makeStringSelectionComboBox(
        std::string name,
        std::vector<std::string> options,
        const std::set<std::string>& preferredSet,
        const std::string& mostPreferred);

    inline StringComboBoxPtr makeStringSelectionComboBox(
        std::string name,
        std::vector<std::string> options,
        const std::string& mostPreferred,
        const std::set<std::string>& preferredSet)
    {
        return makeStringSelectionComboBox(std::move(name), std::move(options), preferredSet, mostPreferred);
    }

    inline StringComboBoxPtr makeRNSComboBox(
        const VirtualRobot::RobotPtr& robot,
        std::string name = "RobotNodeSet",
        const std::set<std::string>& preferredSet = {},
        const std::string& mostPreferred = "")
    {
        return makeStringSelectionComboBox(std::move(name), robot->getRobotNodeSetNames(), preferredSet, mostPreferred);
    }

    inline StringComboBoxPtr makeRobotNodeComboBox(
        const VirtualRobot::RobotPtr& robot,
        std::string name = "RobotNode",
        const std::set<std::string>& preferredSet = {},
        const std::string& mostPreferred = "")
    {
        return makeStringSelectionComboBox(std::move(name), robot->getRobotNodeNames(), preferredSet, mostPreferred);
    }
}
