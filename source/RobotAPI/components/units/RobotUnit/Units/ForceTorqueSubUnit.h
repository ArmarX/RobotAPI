/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ForceTorqueSubUnit
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "RobotUnitSubUnit.h"

#include <RobotAPI/components/units/ForceTorqueUnit.h>

#include "../SensorValues/SensorValueForceTorque.h"

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(ForceTorqueSubUnit);
    class ForceTorqueSubUnit:
        virtual public RobotUnitSubUnit,
        virtual public ForceTorqueUnit
    {
    public:
        void update(const SensorAndControl& sc, const JointAndNJointControllers& c) override;

        // ForceTorqueUnitInterface interface
        void setOffset(const FramedDirectionBasePtr&, const FramedDirectionBasePtr&,  const Ice::Current& = Ice::emptyCurrent) override;
        void setToNull(const Ice::Current& = Ice::emptyCurrent) override;

        // ForceTorqueUnit interface
        void onInitForceTorqueUnit()  override;
        void onStartForceTorqueUnit() override {}
        void onExitForceTorqueUnit()  override {}

        struct DeviceData
        {
            std::string deviceName;
            std::size_t sensorIndex;
            std::string frame;
        };

        std::vector<DeviceData> devs;
    private:
        std::string agentName;
    };
}
