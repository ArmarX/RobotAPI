/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::InertialMeasurementSubUnit
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "RobotUnitSubUnit.h"

#include <ArmarXCore/observers/variant/TimestampVariant.h>

#include <RobotAPI/components/units/InertialMeasurementUnit.h>

#include "../SensorValues/SensorValueIMU.h"

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(InertialMeasurementSubUnit);
    class InertialMeasurementSubUnit:
        virtual public RobotUnitSubUnit,
        virtual public InertialMeasurementUnit
    {
    public:
        void update(const SensorAndControl& sc, const JointAndNJointControllers& c) override;

        // InertialMeasurementUnit interface
    protected:
        void onInitIMU()  override {}
        void onStartIMU() override;
        void onExitIMU()  override {}
    public:
        std::map<std::string, std::size_t> devs;
    };
}
