/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::GlobalPoseSubUnit
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <Eigen/Core>

#include <VirtualRobot/MathTools.h>

#include <RobotAPI/components/units/SensorActorUnit.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/interface/units/LocalizationUnitInterface.h>

#include "RobotUnitSubUnit.h"

namespace armarx
{

    class LocalizationUnit :
        virtual public LocalizationUnitInterface,
        virtual public SensorActorUnit
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "LocalizationUnit";
        }

        void onInitComponent() override {}
        void onConnectComponent() override {}
    };



    class GlobalRobotPoseCorrectionSensorDevice;

    TYPEDEF_PTRS_HANDLE(LocalizationSubUnit);
    class LocalizationSubUnit:
        virtual public RobotUnitSubUnit,
        virtual public LocalizationUnit,
        virtual public LocalizationSubUnitInterface
    {
    public:
        LocalizationSubUnit() = default;
        ~LocalizationSubUnit() override;

        void update(const SensorAndControl& sc, const JointAndNJointControllers& c) override;

        void reportGlobalRobotPoseCorrection(const TransformStamped& global_T_odom, const Ice::Current& = Ice::emptyCurrent) override;

        /**
         * This device partially holds the information about the robot's global pose.
         * It is the transformation from the `global` to the `odometry` frame which can be 
         * obtained by SLAM. In conjunction with the relative pose (pose within the `odometry` frame),
         * the global pose can be obtained.
         */
        GlobalRobotPoseCorrectionSensorDevice* globalPositionCorrectionSensorDevice;

    private:

        std::string agentName;
        std::string robotRootFrame;


    };
}
