/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ForceTorqueSubUnit
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ForceTorqueSubUnit.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/core/FramedPose.h>

void armarx::ForceTorqueSubUnit::update(const armarx::SensorAndControl& sc, const JointAndNJointControllers&)
{
    if (!getProxy())
    {
        //this unit is not initialized yet
        ARMARX_IMPORTANT << deactivateSpam(1) << "not initialized yet";
        return;
    }
    if (!listenerPrx)
    {
        ARMARX_IMPORTANT << deactivateSpam(1) << "listener is not set";
        return;
    }
    for (DeviceData& dev : devs)
    {
        const SensorValueBase* sensorValBase = sc.sensors.at(dev.sensorIndex).get();
        ARMARX_CHECK_EXPRESSION(sensorValBase->isA<SensorValueForceTorque>());
        const SensorValueForceTorque& sensorVal = *sensorValBase->asA<SensorValueForceTorque>();
        ARMARX_CHECK_EXPRESSION(listenerPrx);
        listenerPrx->reportSensorValues(
            dev.deviceName,
            new armarx::FramedDirection(sensorVal.force, dev.frame, agentName),
            new armarx::FramedDirection(sensorVal.torque, dev.frame, agentName)
        );
    }
}

void armarx::ForceTorqueSubUnit::setOffset(const armarx::FramedDirectionBasePtr&, const armarx::FramedDirectionBasePtr&, const Ice::Current&)
{
    ARMARX_WARNING << "NYI";
}

void armarx::ForceTorqueSubUnit::setToNull(const Ice::Current&)
{
    ARMARX_WARNING << "NYI";
}

void armarx::ForceTorqueSubUnit::onInitForceTorqueUnit()
{
    agentName = getProperty<std::string>("AgentName").getValue();
}
