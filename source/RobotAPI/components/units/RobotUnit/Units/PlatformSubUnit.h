/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::PlatformSubUnit
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <Eigen/Core>

#include <VirtualRobot/MathTools.h>

#include <RobotAPI/components/units/PlatformUnit.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointHolonomicPlatformRelativePositionController.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointHolonomicPlatformGlobalPositionController.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/interface/core/RobotState.h>

#include "RobotUnitSubUnit.h"
#include "../NJointControllers/NJointHolonomicPlatformUnitVelocityPassThroughController.h"
#include "../SensorValues/SensorValueHolonomicPlatform.h"

namespace armarx
{
    class GlobalRobotPoseCorrectionSensorDevice;

    TYPEDEF_PTRS_HANDLE(PlatformSubUnit);
    class PlatformSubUnit:
        virtual public RobotUnitSubUnit,
        virtual public PlatformUnit,
        virtual public PlatformSubUnitInterface
    {
    public:
        void update(const SensorAndControl& sc, const JointAndNJointControllers& c) override;

        // PlatformUnitInterface interface
        void move(Ice::Float vx, Ice::Float vy, Ice::Float vr,  const Ice::Current& = Ice::emptyCurrent) override;
        void moveTo(Ice::Float rx, Ice::Float ry, Ice::Float rr, Ice::Float lac, Ice::Float rac,  const Ice::Current& = Ice::emptyCurrent) override;
        void moveRelative(Ice::Float rx, Ice::Float ry, Ice::Float rr, Ice::Float lac, Ice::Float rac,  const Ice::Current& = Ice::emptyCurrent) override;
        void setMaxVelocities(Ice::Float mxVLin, Ice::Float mxVAng,  const Ice::Current& = Ice::emptyCurrent) override;


        // PlatformUnit interface
        void onInitPlatformUnit()  override
        {
            usingTopic("PlatformState");
        }
        void onStartPlatformUnit() override
        {
            agentName = robotStateComponent->getRobotName();
            robotRootFrame = robotStateComponent->getSynchronizedRobot()->getRootNode()->getName();
        }
        void onExitPlatformUnit()  override {}
        void stopPlatform(const Ice::Current& c = Ice::emptyCurrent) override;

        NJointHolonomicPlatformUnitVelocityPassThroughControllerPtr pt;
        NJointHolonomicPlatformRelativePositionControllerPtr relativePosCtrl;
        NJointHolonomicPlatformGlobalPositionControllerPtr globalPosCtrl;

        std::size_t platformSensorIndex;

    protected:

        mutable std::mutex dataMutex;

        Ice::Float maxVLin = std::numeric_limits<Ice::Float>::infinity();
        Ice::Float maxVAng = std::numeric_limits<Ice::Float>::infinity();


    private:

        std::string agentName;
        std::string robotRootFrame;


    };
}
