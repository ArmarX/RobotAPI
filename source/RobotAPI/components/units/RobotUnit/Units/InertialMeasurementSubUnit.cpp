/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::InertialMeasurementSubUnit
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "InertialMeasurementSubUnit.h"
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

void armarx::InertialMeasurementSubUnit::onStartIMU()
{
    ARMARX_IMPORTANT << "Devices with IMUs: " << devs;
}


void armarx::InertialMeasurementSubUnit::update(const armarx::SensorAndControl& sc, const JointAndNJointControllers&)
{
    if (!getProxy())
    {
        //this unit is not initialized yet
        return;
    }
    if (!IMUTopicPrx)
    {
        ARMARX_IMPORTANT << deactivateSpam(1) << "listener is not set";
        return;
    }
    TimestampVariantPtr t = new TimestampVariant(sc.sensorValuesTimestamp);
    InertialMeasurementUnitListenerPrx batchPrx = IMUTopicPrx->ice_batchOneway();
    for (auto nam2idx : devs)
    {
        const auto devidx = nam2idx.second;
        const auto& dev = nam2idx.first;
        const SensorValueBase* sv = sc.sensors.at(devidx).get();
        ARMARX_CHECK_EXPRESSION(sv->isA<SensorValueIMU>());
        const SensorValueIMU* s = sv->asA<SensorValueIMU>();
        IMUData data;
        data.acceleration = Ice::FloatSeq(s->linearAcceleration.data(), s->linearAcceleration.data() + s->linearAcceleration.rows() * s->linearAcceleration.cols());
        data.gyroscopeRotation = Ice::FloatSeq(s->angularVelocity.data(), s->angularVelocity.data() + s->angularVelocity.rows() * s->angularVelocity.cols());;
        data.orientationQuaternion = {s->orientation.w(), s->orientation.x(), s->orientation.y(), s->orientation.z()};
        auto frame = dev;
        batchPrx->reportSensorValues(dev, frame, data, t);
    }
    batchPrx->ice_flushBatchRequests();
}

