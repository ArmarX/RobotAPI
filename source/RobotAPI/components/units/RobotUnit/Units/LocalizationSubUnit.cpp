/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::LocalizationSubUnit
 * @author     Raphael ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "LocalizationSubUnit.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <SimoxUtility/math/convert/mat4f_to_rpy.h>

#include <RobotAPI/components/units/RobotUnit/Devices/GlobalRobotPoseSensorDevice.h>
#include <RobotAPI/interface/core/GeometryBase.h>
#include <RobotAPI/interface/core/PoseBase.h>
#include <RobotAPI/libraries/core/FramedPose.h>


namespace armarx
{
    LocalizationSubUnit::~LocalizationSubUnit() = default;


    void LocalizationSubUnit::update(const armarx::SensorAndControl& sc, const JointAndNJointControllers&)
    {
        if (!getProxy())
        {
            //this unit is not initialized yet
            ARMARX_IMPORTANT << deactivateSpam(1) << "not initialized yet - skipping this update";
            return;
        }
    }


    void LocalizationSubUnit::reportGlobalRobotPoseCorrection(const TransformStamped& global_T_odom, const Ice::Current&)
    {
        ARMARX_CHECK_EXPRESSION(globalPositionCorrectionSensorDevice);
        globalPositionCorrectionSensorDevice->updateGlobalPositionCorrection(global_T_odom.transform);
    }
}
