/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <string>

namespace armarx::ControlModes
{
    //'normal' actor modes
    static const std::string Position1DoF = "ControlMode_Position1DoF";
    static const std::string Torque1DoF   = "ControlMode_Torque1DoF";
    static const std::string ZeroTorque1DoF   = "ControlMode_ZeroTorque1DoF";
    static const std::string Velocity1DoF = "ControlMode_Velocity1DoF";
    static const std::string Current1DoF = "ControlMode_Current1DoF";
    static const std::string PWM1DoF = "ControlMode_PWM1DoF";

    // 'extended' actor modes
    static const std::string VelocityTorque = "ControlMode_VelocityTorque";
    static const std::string ActiveImpedance = "ControlMode_ActiveImpedance";

    static const std::string VelocityWithPwmLimit1DoF = "ControlMode_VelocityWithPwmLimit1DoF";
    static const std::string PositionWithPwmLimit1DoF = "ControlMode_PositionWithPwmLimit1DoF";

    //modes for holonomic platforms
    static const std::string HolonomicPlatformVelocity = "ControlMode_HolonomicPlatformVelocity";
    static const std::string HolonomicPlatformRelativePosition = "ControlMode_HolonomicPlatformRelativePosition";

    //special sentinel modes
    static const std::string EmergencyStop = "ControlMode_EmergencyStop";
    static const std::string StopMovement = "ControlMode_StopMovement";
}

namespace armarx::HardwareControlModes
{
    //'normal' actor modes
    static const std::string Position1DoF = "Position1DoF";
    static const std::string Velocity1DoF = "Velocity1DoF";
    static const std::string Current1DoF = "Current1DoF";

    //modes for holonomic platforms
    static const std::string HolonomicPlatformVelocity = "HolonomicPlatformVelocity";
}
