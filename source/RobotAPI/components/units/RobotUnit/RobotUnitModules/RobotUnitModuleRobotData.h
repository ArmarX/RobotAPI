/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/observers/DebugObserver.h>

#include "RobotUnitModuleBase.h"

namespace armarx
{
    using RobotPoolPtr = std::shared_ptr<class RobotPool>;
}

namespace armarx::RobotUnitModule
{
    class RobotDataPropertyDefinitions: public ModuleBasePropertyDefinitions
    {
    public:
        RobotDataPropertyDefinitions(std::string prefix): ModuleBasePropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>(
                "RobotFileName", "Robot file name, e.g. robot_model.xml");
            defineOptionalProperty<std::string>(
                "RobotFileNameProject", "",
                "Project in which the robot filename is located (if robot is loaded from an external project)");

            defineOptionalProperty<std::string>(
                "RobotName", "",
                "Override robot name if you want to load multiple robots of the same type");
            defineOptionalProperty<std::string>(
                "RobotNodeSetName", "Robot",
                "Robot node set name as defined in robot xml file, e.g. 'LeftArm'");
            defineOptionalProperty<std::string>(
                "PlatformName", "Platform",
                "Name of the platform needs to correspond to a node in the virtual robot.");
            defineOptionalProperty<std::string>(
                "PlatformInstanceName", "Platform",
                "Name of the platform instance (will publish values on PlatformInstanceName + 'State')");

        }
    };

    /**
     * @ingroup Library-RobotUnit-Modules
     * @brief This \ref ModuleBase "Module" holds all high-level data about the robot.
     *
     * @see ModuleBase
     */
    class RobotData : virtual public ModuleBase
    {
        friend class ModuleBase;
    public:
        /**
         * @brief Returns the singleton instance of this class
         * @return The singleton instance of this class
         */
        static RobotData& Instance()
        {
            return ModuleBase::Instance<RobotData>();
        }
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////// RobotUnitModule hooks //////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /// @see ModuleBase::_preOnInitRobotUnit
        void _initVirtualRobot();
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////// Module interface /////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    public:
        /**
         * @brief Returns the name of the robot's platform
         * @return The name of the robot's platform
         */
        const std::string& getRobotPlatformName() const;
        /**
         * @brief Returns the name of the robot's RobotNodeSet
         * @return The name of the robot's RobotNodeSet
         */
        const std::string& getRobotNodetSeName() const;
        /**
         * @brief Returns the name of the project containing the robot's model
         * @return The name of the project containing the robot's model
         */
        const std::string& getRobotProjectName() const;
        /**
         * @brief Returns the file name of the robot's model
         * @return The file name of the robot's model
         */
        const std::string& getRobotFileName() const;

        /**
         * @brief Returns the robot's name
         * @return The robot's name
         */
        std::string getRobotName() const;

        /**
         * @brief Returns the name of the robot platform instance. Used for the platform topic: RobotPlatformInstance + "State"
         */
        std::string getRobotPlatformInstanceName() const;


        /**
         * @brief Returns a clone of the robot's model
         * @return A clone of the robot's model
         */
        VirtualRobot::RobotPtr cloneRobot(bool updateCollisionModel = false) const;
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////////////// Data ///////////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //

    private:
        /// @brief The name of the robot's RobotNodeSet
        std::string robotNodeSetName;
        /// @brief The name of the project containing the robot's model
        std::string robotProjectName;
        /// @brief The file name of the robot's model
        std::string robotFileName;
        /// @brief The name of the robot's platform
        std::string robotPlatformName;
        /// @brief The name of the robot's platform instance
        std::string robotPlatformInstanceName;

        /// @brief The robot's model.
        VirtualRobot::RobotPtr robot;
        RobotPoolPtr robotPool;
        /// @brief A mutex guarding \ref robot
        mutable std::mutex robotMutex;
    };
}

