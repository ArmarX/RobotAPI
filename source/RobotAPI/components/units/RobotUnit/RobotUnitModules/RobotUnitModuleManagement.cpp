/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/ArmarXManager.h>

#include "RobotUnitModuleManagement.h"
#include <ArmarXCore/core/time/TimeUtil.h>

namespace armarx::RobotUnitModule
{
    void Management::_preOnInitRobotUnit()
    {
        throwIfInControlThread(BOOST_CURRENT_FUNCTION);
        additionalObjectSchedulerCount = getProperty<std::uint64_t>("AdditionalObjectSchedulerCount").getValue();
        heartbeatRequired              = getProperty<bool>("HeartbeatRequired").getValue();
        heartbeatMaxCycleMS            = getProperty<long>("HeartbeatMaxCycleMS").getValue();
        heartbeatStartupMarginMS       = getProperty<long>("HeartbeatStartupMarginMS").getValue();
        getArmarXManager()->increaseSchedulers(static_cast<int>(additionalObjectSchedulerCount));

        usingTopic(getProperty<std::string>("AggregatedRobotHealthTopicName").getValue());
    }

    void Management::_postFinishControlThreadInitialization()
    {
        controlLoopStartTime = TimeUtil::GetTime(true).toMilliSeconds();
    }


    void Management::aggregatedHeartbeat(RobotHealthState overallHealthState, const Ice::Current&)
    {
        heartbeatRequired = true;
        if (overallHealthState == RobotHealthState::HealthOK || overallHealthState == RobotHealthState::HealthWarning)
        {
            lastHeartbeat = TimeUtil::GetTime(true).toMilliSeconds();
        }
    }

}
