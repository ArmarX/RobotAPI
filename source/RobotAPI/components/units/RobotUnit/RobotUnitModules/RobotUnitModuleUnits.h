/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

#include "RobotUnitModuleBase.h"
#include "../Units/RobotUnitSubUnit.h"

namespace armarx::RobotUnitModule
{
    class UnitsPropertyDefinitions: public ModuleBasePropertyDefinitions
    {
    public:
        UnitsPropertyDefinitions(std::string prefix): ModuleBasePropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>(
                "KinematicUnitName", "KinematicUnit",
                "The name of the created kinematic unit");
            defineOptionalProperty<std::string>(
                "KinematicUnitNameTopicPrefix", "",
                "Prefix for the kinematic sensor values topic");


            defineOptionalProperty<std::string>(
                "PlatformUnitName", "PlatformUnit",
                "The name of the created platform unit");

            defineOptionalProperty<std::string>(
                "ForceTorqueUnitName", "ForceTorqueUnit",
                "The name of the created force troque unit (empty = default)");
            defineOptionalProperty<std::string>(
                "ForceTorqueTopicName", "ForceTorqueValues",
                "Name of the topic on which the force torque sensor values are provided");

            defineOptionalProperty<std::string>(
                "InertialMeasurementUnitName", "InertialMeasurementUnit",
                "The name of the created inertial measurement unit (empty = default)");
            defineOptionalProperty<std::string>(
                "IMUTopicName", "IMUValues",
                "Name of the IMU Topic.");

            defineOptionalProperty<bool>(
                "CreateTCPControlUnit", false,
                "If true the TCPControl SubUnit is created and added");
            defineOptionalProperty<std::string>(
                "TCPControlUnitName", "TCPControlUnit",
                "Name of the TCPControlUnit.");

            defineOptionalProperty<bool>(
                "CreateTrajectoryPlayer", true,
                "If true the TrajectoryPlayer SubUnit is created and added");
            defineOptionalProperty<std::string>(
                "TrajectoryControllerUnitName", "TrajectoryPlayer",
                "Name of the TrajectoryControllerUnit. The respective component outside of the RobotUnit is TrajectoryPlayer");

            defineOptionalProperty<std::string>(
                "EmergencyStopMasterName", "EmergencyStopMaster",
                "The name used to register as an EmergencyStopMaster");
            defineOptionalProperty<std::string>(
                "EmergencyStopTopic", "EmergencyStop",
                "The name of the topic over which changes of the emergencyStopState are sent.");
        }
    };

    /**
     * @ingroup Library-RobotUnit-Modules
     * @brief This \ref ModuleBase "Module" manages all Units of a \ref RobotUnit
     *
     * These are the managed Units:
     *   - \ref getKinematicUnit "KinematicUnit"
     *   - \ref getForceTorqueUnit "ForceTorqueUnit"
     *   - \ref getInertialMeasurementUnit "InertialMeasurementUnit"
     *   - \ref getPlatformUnit "PlatformUnit"
     *   - \ref getTCPControlUnit "TCPControlUnit"
     *   - \ref getTrajectoryPlayer "TrajectoryPlayer"
     *
     * @see ModuleBase
     */
    class Units : virtual public ModuleBase, virtual public RobotUnitUnitInterface
    {
        friend class ModuleBase;
    public:
        /**
         * @brief Returns the singleton instance of this class
         * @return The singleton instance of this class
         */
        static Units& Instance();
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////// RobotUnitModule hooks //////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /// @see ModuleBase::_preOnInitRobotUnit
        void _preOnInitRobotUnit();
        /// @see ModuleBase::_icePropertiesInitialized
        void _icePropertiesInitialized();
        /// @see ModuleBase::_preFinishRunning
        void _preFinishRunning();
        /// @see ModuleBase::_postOnExitRobotUnit
        void _postOnExitRobotUnit();
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////////// ice interface //////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    public:
        /**
         * @brief Returns proxies to all units
         * @return Proxies to all units
         */
        Ice::ObjectProxySeq getUnits(const Ice::Current&) const override;
        /**
         * @brief Returns a proxy to the Unit with the given ice id (or null if there is none)
         * @param staticIceId The Unit's ice id
         * @return A proxy to the Unit with the given ice id (or null if there is none)
         */
        Ice::ObjectPrx getUnit(const std::string& staticIceId, const Ice::Current&) const override;

        /**
         * @brief Returns a proxy to the KinematicUnit
         * @return A proxy to the KinematicUnit
         */
        KinematicUnitInterfacePrx getKinematicUnit(const Ice::Current&) const override;
        /**
         * @brief Returns a proxy to the ForceTorqueUnit
         * @return A proxy to the ForceTorqueUnit
         */
        ForceTorqueUnitInterfacePrx getForceTorqueUnit(const Ice::Current&) const override;
        /**
         * @brief Returns a proxy to the InertialMeasurementUnit
         * @return A proxy to the InertialMeasurementUnit
         */
        InertialMeasurementUnitInterfacePrx getInertialMeasurementUnit(const Ice::Current&) const override;
        /**
         * @brief Returns a proxy to the PlatformUnit
         * @return A proxy to the PlatformUnit
         */
        PlatformUnitInterfacePrx getPlatformUnit(const Ice::Current&) const override;
        /**
         * @brief Returns a proxy to the TCPControlUnit
         * @return A proxy to the TCPControlUnit
         */
        TCPControlUnitInterfacePrx getTCPControlUnit(const Ice::Current&) const override;
        /**
         * @brief Returns a proxy to the TrajectoryPlayer
         * @return A proxy to the TrajectoryPlayer
         */
        TrajectoryPlayerInterfacePrx getTrajectoryPlayer(const Ice::Current&) const override;
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////// Module interface /////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    public:
        //general getters
        /**
         * @brief Returns a pointer to the Unit with the given ice id (or null if there is none)
         * @param staticIceId The Unit's ice id
         * @return A pointer to the Unit with the given ice id (or null if there is none)
         */
        const ManagedIceObjectPtr& getUnit(const std::string& staticIceId) const;

        /**
         * @brief Returns a pointer to the Unit for the given type (or null if there is none)
         * @return A pointer to the Unit for the given type (or null if there is none)
         */
        template<class T> typename T::PointerType getUnit() const;
        /**
         * @brief Returns a proxy to the Unit for the given type (or null if there is none)
         * @return A proxy to the Unit for the given type (or null if there is none)
         */
        template<class T> typename T::ProxyType getUnitPrx() const;

        //specific getters
        /**
         * @brief Returns a pointer to the KinematicUnit
         * @return A pointer to the KinematicUnit
         */
        KinematicUnitInterfacePtr getKinematicUnit() const;
        /**
         * @brief Returns a pointer to the ForceTorqueUnit
         * @return A pointer to the ForceTorqueUnit
         */
        ForceTorqueUnitInterfacePtr getForceTorqueUnit() const;
        /**
         * @brief Returns a pointer to the InertialMeasurementUnit
         * @return A pointer to the InertialMeasurementUnit
         */
        InertialMeasurementUnitInterfacePtr getInertialMeasurementUnit() const;
        /**
         * @brief Returns a pointer to the PlatformUnit
         * @return A pointer to the PlatformUnit
         */
        PlatformUnitInterfacePtr getPlatformUnit() const;
        /**
         * @brief Returns a pointer to the TCPControlUnit
         * @return A pointer to the TCPControlUnit
         */
        TCPControlUnitInterfacePtr getTCPControlUnit() const;
        /**
         * @brief Returns a pointer to the TrajectoryPlayer
         * @return A pointer to the TrajectoryPlayer
         */
        TrajectoryPlayerInterfacePtr getTrajectoryPlayer() const;
        /**
         * @brief Returns a pointer to the EmergencyStopMaster
         * @return A pointer to the EmergencyStopMaster
         */
        const EmergencyStopMasterInterfacePtr& getEmergencyStopMaster() const;
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////// implementation //////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    protected:
        /**
         * @brief Calls all init hooks for all managed Units
         *
         * The called hooks are
         *   - \ref initializeKinematicUnit
         *   - \ref initializePlatformUnit
         *   - \ref initializeForceTorqueUnit
         *   - \ref initializeInertialMeasurementUnit
         *   - \ref initializeTrajectoryControllerUnit
         *   - \ref initializeTcpControllerUnit
         */
        virtual void initializeDefaultUnits();
        /// @brief Initializes the KinematicUnit
        virtual void initializeKinematicUnit();
        /// @brief Initializes the PlatformUnit
        virtual void initializePlatformUnit();
        /// @brief Initializes the ForceTorqueUnit
        virtual void initializeForceTorqueUnit();
        /// @brief Initializes the InertialMeasurementUnit
        virtual void initializeInertialMeasurementUnit();
        /// @brief Initializes the TrajectoryControllerUnit
        virtual void initializeTrajectoryControllerUnit();
        /// @brief Initializes the TcpControllerUnit
        virtual void initializeTcpControllerUnit();
        /// @brief Initializes the TcpControllerUnit
        virtual void initializeLocalizationUnit();
        /// @brief Create the \ref KinematicUnit (this function should be called in \ref initializeKinematicUnit)
        ManagedIceObjectPtr createKinematicSubUnit(const Ice::PropertiesPtr& properties,
                const std::string& positionControlMode = ControlModes::Position1DoF,
                const std::string& velocityControlMode = ControlModes::Velocity1DoF,
                const std::string& torqueControlMode   = ControlModes::Torque1DoF,
                const std::string& pwmControlMode      = ControlModes::PWM1DoF);
        /**
         * @brief Registers a unit for management (the Unit is added and removed to the \ref ArmarXManager
         * and updated with new values.
         * @param unit The Unit to add.
         */
        void addUnit(ManagedIceObjectPtr unit);
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////////////// Data ///////////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /// @brief holds all units managed by the RobotUnit
        std::vector<ManagedIceObjectPtr> units;
        /// @brief holds a copy of all units managed by the RobotUnit derived from RobotUnitSubUnit
        /// this is done to prevent casting
        std::vector<RobotUnitSubUnitPtr> subUnits;

        /// @brief Pointer to the EmergencyStopMaster used by the \ref RobotUnit
        EmergencyStopMasterInterfacePtr emergencyStopMaster;

        /// @brief Temporary ptr to the TCPControlUnit
        ManagedIceObjectPtr tcpControllerSubUnit;
        /// @brief Temporary ptr to the TrajectoryPlayer
        ManagedIceObjectPtr trajectoryControllerSubUnit;

        /// @brief A copy of the VirtualRobot to be used inside of this Module
        VirtualRobot::RobotPtr unitCreateRobot;
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// Attorneys ////////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /**
        * \brief This class allows minimal access to private members of \ref Units in a sane fashion for \ref Publisher.
        * \warning !! DO NOT ADD ADDITIONAL FRIENDS IF YOU DO NOT KNOW WAHT YOU ARE DOING! IF YOU DO SOMETHING WRONG YOU WILL CAUSE UNDEFINED BEHAVIOUR !!
        */
        friend class UnitsAttorneyForPublisher;
    };
}

#include "RobotUnitModuleUnits.ipp"
