/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <thread>

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

#include "RobotUnitModuleBase.h"

namespace armarx::RobotUnitModule
{
    /**
     * @ingroup Library-RobotUnit-Modules
     * @brief This \ref ModuleBase "Module" manages \ref NJointControllerBase "NJointControllers".
     *
     * @see ModuleBase
     */
    class ControllerManagement : virtual public ModuleBase, virtual public RobotUnitControllerManagementInterface
    {
        friend class ModuleBase;
    public:
        /**
         * @brief Returns the singleton instance of this class
         * @return The singleton instance of this class
         */
        static ControllerManagement& Instance()
        {
            return ModuleBase::Instance<ControllerManagement>();
        }

        ~ControllerManagement();

        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////// RobotUnitModule hooks //////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /// @see ModuleBase::_preOnInitRobotUnit
        void _preOnInitRobotUnit();
        /// @see ModuleBase::_preFinishRunning
        void _preFinishRunning();
        /// @see ModuleBase::_postFinishRunning
        void _postFinishRunning();
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////////// ice interface //////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    public:
        /**
         * @brief Returns a proxy to the \ref NJointControllerBase.
         * @param name The \ref NJointControllerBase's name.
         * @return A proxy to the \ref NJointControllerBase.
         * @see getAllNJointControllers
         */
        NJointControllerInterfacePrx getNJointController(const std::string& name, const Ice::Current& = Ice::emptyCurrent) const override;
        /**
         * @brief Returns proxies to all \ref NJointControllerBase "NJointControllers"
         * @return Proxies to all \ref NJointControllerBase "NJointControllers"
         * @see getNJointController
         */
        StringNJointControllerPrxDictionary getAllNJointControllers(const Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * @brief Returns the status of the \ref NJointControllerBase.
         * @param name The \ref NJointControllerBase's name.
         * @return The status of the \ref NJointControllerBase.
         * @see NJointControllerStatus
         * @see getNJointControllerStatuses
         * @see getNJointControllerDescriptionWithStatus
         * @see getNJointControllerDescriptionsWithStatuses
         */
        NJointControllerStatus getNJointControllerStatus(const std::string& name, const Ice::Current& = Ice::emptyCurrent) const override;
        /**
         * @brief Returns the status of all \ref NJointControllerBase "NJointControllers".
         * @return The status of all \ref NJointControllerBase "NJointControllers".
         * @see NJointControllerStatus
         * @see getNJointControllerStatus
         * @see getNJointControllerDescriptionWithStatus
         * @see getNJointControllerDescriptionsWithStatuses
         */
        NJointControllerStatusSeq getNJointControllerStatuses(const Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * @brief Returns the description of the \ref NJointControllerBase.
         * @param name The \ref NJointControllerBase's name.
         * @return The description of the \ref NJointControllerBase.
         * @see NJointControllerDescription
         * @see getNJointControllerDescriptions
         * @see getNJointControllerDescriptionWithStatus
         * @see getNJointControllerDescriptionsWithStatuses
         */
        NJointControllerDescription getNJointControllerDescription(const std::string& name, const Ice::Current& = Ice::emptyCurrent) const override;
        /**
         * @brief Returns the description of all \ref NJointControllerBase "NJointControllers".
         * @return The description of all \ref NJointControllerBase "NJointControllers".
         * @see NJointControllerDescription
         * @see getNJointControllerDescription
         * @see getNJointControllerDescriptionWithStatus
         * @see getNJointControllerDescriptionsWithStatuses
         */

        NJointControllerDescriptionSeq getNJointControllerDescriptions(const Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * @brief Returns the status and description of the \ref NJointControllerBase.
         * @param name The \ref NJointControllerBase's name.
         * @return The status and description of the \ref NJointControllerBase.
         * @see getNJointControllerDescriptionsWithStatuses
         * @see NJointControllerStatus
         * @see getNJointControllerStatus
         * @see getNJointControllerStatuses
         * @see NJointControllerDescription
         * @see getNJointControllerDescription
         * @see getNJointControllerDescriptions
         */
        NJointControllerDescriptionWithStatus getNJointControllerDescriptionWithStatus(
            const std::string& name, const Ice::Current& = Ice::emptyCurrent) const override;
        /**
         * @brief Returns the status and description of all \ref NJointControllerBase "NJointControllers".
         * @return The status and description of all \ref NJointControllerBase "NJointControllers".
         * @see getNJointControllerDescriptionsWithStatus
         * @see NJointControllerStatus
         * @see getNJointControllerStatus
         * @see getNJointControllerStatuses
         * @see NJointControllerDescription
         * @see getNJointControllerDescription
         * @see getNJointControllerDescriptions
         */
        NJointControllerDescriptionWithStatusSeq getNJointControllerDescriptionsWithStatuses(const Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * @brief getNJointControllerClassDescription
         * @param className
         * @return
         */
        NJointControllerClassDescription getNJointControllerClassDescription(
            const std::string& className, const Ice::Current& = Ice::emptyCurrent) const override;
        /**
         * @brief getNJointControllerClassDescriptions
         * @return
         */
        NJointControllerClassDescriptionSeq getNJointControllerClassDescriptions(const Ice::Current& = Ice::emptyCurrent) const override;
        /**
         * @brief Loads the given lib. (calls `getArmarXManager()->loadLibFromPath(path)`)
         * @param path Path to the lib to load.
         * @return Whether loading the lib was successful.
         * @see ArmarXManager::loadLibFromPath
         */
        bool loadLibFromPath(const std::string& path, const Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Loads the given lib. (calls `getArmarXManager()->loadLibFromPath(package, lib)`)
         * @param package The armarx package containing the lib
         * @param lib The lib name to load.
         * @return Whether loading the lib was successful.
         * @see ArmarXManager::loadLibFromPackage
         */
        bool loadLibFromPackage(const std::string& package, const std::string& lib, const Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Returns the names of all available classes of \ref NJointControllerBase.
         * @return The names of all available classes of \ref NJointControllerBase.
         */
        Ice::StringSeq getNJointControllerClassNames(const Ice::Current& = Ice::emptyCurrent) const override;
        /**
         * @brief Returns the names of all \ref NJointControllerBase "NJointControllers"
         * @return The names of all \ref NJointControllerBase "NJointControllers"
         */
        Ice::StringSeq getNJointControllerNames(const Ice::Current& = Ice::emptyCurrent) const override;
        /**
         * @brief Returns the names of all requested \ref NJointControllerBase "NJointControllers"
         * @return The names of all requested \ref NJointControllerBase "NJointControllers"
         */
        Ice::StringSeq getRequestedNJointControllerNames(const Ice::Current& = Ice::emptyCurrent) const override;
        /**
         * @brief Returns the names of all activated \ref NJointControllerBase "NJointControllers"
         * @return The names of all activated \ref NJointControllerBase "NJointControllers"
         */
        Ice::StringSeq getActivatedNJointControllerNames(const Ice::Current& = Ice::emptyCurrent) const override;

        /**
         * @brief Queues the given \ref NJointControllerBase for deletion.
         * @param name The \ref NJointControllerBase to delete.
         * @see removeNJointControllersToBeDeleted
         * @see nJointControllersToBeDeleted
         * @see deleteNJointControllers
         */
        void deleteNJointController(const std::string& name, const Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Queues the given \ref NJointControllerBase "NJointControllers" for deletion.
         * @param names The \ref NJointControllerBase "NJointControllers" to delete.
         * @see removeNJointControllersToBeDeleted
         * @see nJointControllersToBeDeleted
         * @see deleteNJointController
         */
        void deleteNJointControllers(const Ice::StringSeq& names, const Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Queues the given \ref NJointControllerBase for deletion and deactivates it if necessary.
         * @param name The \ref NJointControllerBase to delete.
         * @see removeNJointControllersToBeDeleted
         * @see nJointControllersToBeDeleted
         * @see deleteNJointController
         * @see deleteNJointControllers
         * @see deactivateAnddeleteNJointControllers
         */
        void deactivateAndDeleteNJointController(const std::string& name, const Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Queues the given \ref NJointControllerBase "NJointControllers" for deletion and deactivates them if necessary.
         * @param names The \ref NJointControllerBase "NJointControllers" to delete.
         * @see removeNJointControllersToBeDeleted
         * @see nJointControllersToBeDeleted
         * @see deleteNJointController
         * @see deleteNJointControllers
         * @see deactivateAnddeleteNJointController
         */
        void deactivateAndDeleteNJointControllers(const Ice::StringSeq& names, const Ice::Current&) override;

        /**
         * @brief Requests activation for the given \ref NJointControllerBase.
         * @param name The requested \ref NJointControllerBase.
         * @see activateNJointControllers
         */
        void activateNJointController(const std::string& name, const Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Requests activation for the given \ref NJointControllerBase "NJointControllers".
         * @param names The requested \ref NJointControllerBase "NJointControllers".
         * @see activateNJointController
         */
        void activateNJointControllers(const Ice::StringSeq& names, const Ice::Current& = Ice::emptyCurrent) override;

        /**
         * @brief Requests deactivation for the given \ref NJointControllerBase.
         * @param name The \ref NJointControllerBase to be deactivated.
         * @see deactivateNJointControllers
         */
        void deactivateNJointController(const std::string& name, const Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Requests deactivation for the given \ref NJointControllerBase "NJointControllers".
         * @param names The \ref NJointControllerBase "NJointControllers" to be deactivated.
         * @see deactivateNJointController
         */
        void deactivateNJointControllers(const Ice::StringSeq& names, const Ice::Current& = Ice::emptyCurrent) override;

        /**
         * @brief Cretes a \ref NJointControllerBase.
         * @param className The \ref NJointControllerBase's class.
         * @param instanceName The \ref NJointControllerBase's name.
         * @param config A config passed to the \ref NJointControllerBase's ctor.
         * @return A proxy to the created \ref NJointControllerBase.
         */
        NJointControllerInterfacePrx createNJointController(
            const std::string& className, const std::string& instanceName,
            const NJointControllerConfigPtr& config, const Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Cretes a \ref NJointControllerBase.
         * @param className The \ref NJointControllerBase's class.
         * @param instanceName The \ref NJointControllerBase's name.
         * @param variants A map of \ref Variant "Variants" passed to the \ref NJointControllerBase's ctor.
         * @return A proxy to the created \ref NJointControllerBase.
         */
        NJointControllerInterfacePrx createNJointControllerFromVariantConfig(
            const std::string& className, const std::string& instanceName,
            const StringVariantBaseMap& variants, const Ice::Current& = Ice::emptyCurrent) override;
        /**
         * @brief Deletes any \ref NJointControllerBase with the given name and creates a new one.
         * @param className The \ref NJointControllerBase's class.
         * @param instanceName The \ref NJointControllerBase's name.
         * @param config A config passed to the \ref NJointControllerBase's ctor.
         * @return A proxy to the created \ref NJointControllerBase.
         */
        NJointControllerInterfacePrx createOrReplaceNJointController(
            const std::string& className, const std::string& instanceName,
            const NJointControllerConfigPtr& config, const Ice::Current& = Ice::emptyCurrent) override;

        /**
         * @brief Changes the set of requested \ref NJointControllerBase "NJointControllers" to the given set.
         * @param newSetup The new set of requested \ref NJointControllerBase "NJointControllers"
         */
        void switchNJointControllerSetup(const Ice::StringSeq& newSetup, const Ice::Current& = Ice::emptyCurrent) override;
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////// Module interface /////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    public:
        /**
         * @brief Cretes a \ref NJointControllerBase.
         * @param className The \ref NJointControllerBase's class.
         * @param instanceName The \ref NJointControllerBase's name.
         * @param config A config passed to the \ref NJointControllerBase's ctor.
         * @param deletable Whether the \ref NJointControllerBase can be deleted.
         * @param internal Whether the \ref NJointControllerBase should be tagged as internal.
         * @return A pointer to the created \ref NJointControllerBase
         */
        const NJointControllerBasePtr& createNJointController(
            const std::string& className, const std::string& instanceName,
            const NJointControllerConfigPtr& config, bool deletable, bool internal);

        /**
         * @brief Returns pointers to the \ref NJointControllerBase "NJointControllers". (If one does not exist, an exception is thrown.)
         * @param names The \ref NJointControllerBase "NJointControllers" names.
         * @return Pointers to the \ref NJointControllerBase "NJointControllers".
         * @throw If there is no \ref NJointControllerBase for \param name
         */
        std::vector<armarx::NJointControllerBasePtr> getNJointControllersNotNull(const std::vector<std::string>& names) const;
        /**
         * @brief Returns a pointer to the \ref NJointControllerBase. (If it does not exist, an exception is thrown.)
         * @param name The \ref NJointControllerBase
         * @return A pointer to the \ref NJointControllerBase.
         * @throw If there is no \ref NJointControllerBase for \param name
         */
        const NJointControllerBasePtr& getNJointControllerNotNull(const std::string& name) const;

        /**
         * @brief Returns the names of given \ref NJointControllerBase "NJointControllers"
         * @param ctrls The \ref NJointControllerBase "NJointControllers"
         * @return The names of given \ref NJointControllerBase "NJointControllers"
         */
        std::vector<std::string> getNJointControllerNames(const std::vector<armarx::NJointControllerBasePtr>& ctrls) const;

        /**
         * @brief Queues the given \ref NJointControllerBase for deletion.
         * @param ctrl The \ref NJointControllerBase to delete.
         * @see removeNJointControllersToBeDeleted
         * @see nJointControllersToBeDeleted
         * @see deleteNJointControllers
         */
        void deleteNJointController(const NJointControllerBasePtr& ctrl);
        /**
         * @brief Queues the given \ref NJointControllerBase "NJointControllers" for deletion.
         * @param ctrls The \ref NJointControllerBase "NJointControllers" to delete.
         * @see removeNJointControllersToBeDeleted
         * @see nJointControllersToBeDeleted
         * @see deleteNJointController
         */
        void deleteNJointControllers(const std::vector<NJointControllerBasePtr>& ctrls);
        /**
         * @brief Queues the given \ref NJointControllerBase for deletion and deactivates it if necessary.
         * @param ctrl The \ref NJointControllerBase to delete.
         * @see removeNJointControllersToBeDeleted
         * @see nJointControllersToBeDeleted
         * @see deleteNJointController
         * @see deleteNJointControllers
         * @see deactivateAnddeleteNJointControllers
         */
        void deactivateAndDeleteNJointController(const NJointControllerBasePtr& ctrl);
        /**
         * @brief Queues the given \ref NJointControllerBase "NJointControllers" for deletion and deactivates them if necessary.
         * @param ctrls The \ref NJointControllerBase "NJointControllers" to delete.
         * @see removeNJointControllersToBeDeleted
         * @see nJointControllersToBeDeleted
         * @see deleteNJointController
         * @see deleteNJointControllers
         * @see deactivateAnddeleteNJointController
         */
        void deactivateAndDeleteNJointControllers(const std::vector<NJointControllerBasePtr>& ctrls);


        /**
         * @brief Requests activation for the given \ref NJointControllerBase.
         * @param ctrl The requested \ref NJointControllerBase.
         * @see activateNJointControllers
         */
        void activateNJointController(const NJointControllerBasePtr& ctrl);
        /**
         * @brief Requests activation for the given \ref NJointControllerBase "NJointControllers".
         * @param ctrls The requested \ref NJointControllerBase "NJointControllers".
         * @see activateNJointController
         */
        void activateNJointControllers(const std::vector<NJointControllerBasePtr>& ctrls);

        /**
         * @brief Requests deactivation for the given \ref NJointControllerBase.
         * @param ctrl The \ref NJointControllerBase to be deactivated.
         * @see deactivateNJointControllers
         */
        void deactivateNJointController(const NJointControllerBasePtr& ctrl);
        /**
         * @brief Requests deactivation for the given \ref NJointControllerBase "NJointControllers".
         * @param ctrls The \ref NJointControllerBase "NJointControllers" to be deactivated.
         * @see deactivateNJointController
         */
        void deactivateNJointControllers(const std::vector<NJointControllerBasePtr>& ctrls);

        // //////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////// implementation //////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /**
         * @brief Deletes the given \ref NJointControllerBase "NJointControllers" and removes them
         * from the \ref ArmarXManager
         * @param ctrls \ref NJointControllerBase "NJointControllers" to delete.
         * @param blocking Whether removal from the \ref ArmarXManager should be blocking.
         * @param l Proxy to the listener topic all removal events should be sent to.
         */
        void removeNJointControllers(std::map<std::string, NJointControllerBasePtr>& ctrls, bool blocking = true, RobotUnitListenerPrx l = nullptr);
        /**
         * @brief Calls \ref removeNJointControllers for all \ref NJointControllerBase "NJointControllers"
         * queued for deletion (\ref nJointControllersToBeDeleted).
         * @param blocking Whether removal from the \ref ArmarXManager should be blocking.
         * @param l Proxy to the listener topic all removal events should be sent to.
         * @see removeNJointControllers
         * @see nJointControllersToBeDeleted
         */
        void removeNJointControllersToBeDeleted(bool blocking = true, RobotUnitListenerPrx l = nullptr);

        /**
         * @brief Sets the requested flag for all given \ref NJointControllerBase "NJointControllers" and unsets it for the rest.
         * @param request The \ref NJointControllerBase "NJointControllers" where the requested flag should be set.
         */
        void updateNJointControllerRequestedState(const std::set<NJointControllerBasePtr>& request);
        /**
         * @brief Throws if there is no factory for \ref NJointControllerBase "NJointControllers" for the given class name.
         * @param className The class to check for.
         * @throw If there is no factory for \ref NJointControllerBase "NJointControllers" for the given class name.
         */
        void checkNJointControllerClassName(const std::string& className) const;

        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////////////// Data ///////////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /// @brief Holds all currently loaded NJoint controllers (index: [instancename])(May not be accessed in rt.)
        std::map<std::string, NJointControllerBasePtr> nJointControllers;
        /// @brief These controllers will be deleted in the next iteration of publish
        std::map<std::string, NJointControllerBasePtr> nJointControllersToBeDeleted;
        /// @brief VirtualRobot used when creating controllers / calling other functions in this module
        VirtualRobot::RobotPtr controllerCreateRobot;

        std::recursive_mutex controllerMutex;
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// Attorneys ////////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /**
        * \brief This class allows minimal access to private members of \ref ControllerManagement in a sane fashion for \ref Publisher.
        * \warning !! DO NOT ADD ADDITIONAL FRIENDS IF YOU DO NOT KNOW WAHT YOU ARE DOING! IF YOU DO SOMETHING WRONG YOU WILL CAUSE UNDEFINED BEHAVIOUR !!
        */
        friend class ControllerManagementAttorneyForPublisher;
        /**
        * \brief This class allows minimal access to private members of \ref ControllerManagement in a sane fashion for \ref ControlThreadDataBuffer.
        * \warning !! DO NOT ADD ADDITIONAL FRIENDS IF YOU DO NOT KNOW WAHT YOU ARE DOING! IF YOU DO SOMETHING WRONG YOU WILL CAUSE UNDEFINED BEHAVIOUR !!
        */
        friend class ControllerManagementAttorneyForControlThreadDataBuffer;
    };
}
