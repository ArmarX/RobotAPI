/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "RobotUnitModuleBase.h"

namespace armarx::RobotUnitModule
{

    class ManagementPropertyDefinitions: public ModuleBasePropertyDefinitions
    {
    public:
        ManagementPropertyDefinitions(std::string prefix): ModuleBasePropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::uint64_t>(
                "AdditionalObjectSchedulerCount", 10,
                "Number of ObjectSchedulers to be added");

            defineOptionalProperty<bool>(
                "HeartbeatRequired", true,
                "Whether the Heatbeat is required.");
            defineOptionalProperty<long>(
                "HeartbeatMaxCycleMS", 100,
                "The heartbeats cycle time");
            defineOptionalProperty<long>(
                "HeartbeatStartupMarginMS", 1000,
                "Startup time for heartbeats");
            defineOptionalProperty<std::string>(
                "AggregatedRobotHealthTopicName", "AggregatedRobotHealthTopic",
                "Name of the AggregatedRobotHealthTopic");
        }
    };

    /**
     * @ingroup Library-RobotUnit-Modules
     * @brief This \ref ModuleBase "Module" handles some general management tasks.
     * It implements the \ref RobotUnitManagementInterface.
     *
     * @see ModuleBase
     */
    class Management : virtual public ModuleBase, virtual public RobotUnitManagementInterface
    {
        friend class ModuleBase;
    public:
        /**
         * @brief Returns the singleton instance of this class
         * @return The singleton instance of this class
         */
        static Management& Instance()
        {
            return ModuleBase::Instance<Management>();
        }
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////// RobotUnitModule hooks //////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /// @see ModuleBase::_preOnInitRobotUnit
        void _preOnInitRobotUnit();
        void _postFinishControlThreadInitialization();
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////////// ice interface //////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    public:
        /**
         * @brief Returns whether the RobotUnit is running.
         * @return Whether the RobotUnit is running.
         */
        bool isRunning(const Ice::Current& = Ice::emptyCurrent) const override
        {
            throwIfInControlThread(BOOST_CURRENT_FUNCTION);
            return getRobotUnitState() == RobotUnitState::Running;
        }
        void aggregatedHeartbeat(RobotHealthState overallHealthState, const Ice::Current&) override;

        bool isSimulation(const Ice::Current& = Ice::emptyCurrent) const override
        {
            return false;
        }

        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////////////// Data ///////////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /// @brief The number of additional object schedulers
        std::int64_t additionalObjectSchedulerCount {0};
        std::int64_t controlLoopStartTime {0};
        std::int64_t heartbeatStartupMarginMS {0};
        std::atomic_bool heartbeatRequired{false};
        std::atomic_long heartbeatMaxCycleMS{100};
        std::atomic_long lastHeartbeat{0};

        // //////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// Attorneys ////////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /**
        * \brief This class allows minimal access to private members of \ref Management in a sane fashion for \ref ControlThread.
        * \warning !! DO NOT ADD ADDITIONAL FRIENDS IF YOU DO NOT KNOW WAHT YOU ARE DOING! IF YOU DO SOMETHING WRONG YOU WILL CAUSE UNDEFINED BEHAVIOUR !!
        */
        friend class ManagementAttorneyForControlThread;
    };
}
