/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <atomic>
#include <chrono>
#include <mutex>

#include <boost/preprocessor/variadic/to_seq.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/current_function.hpp>

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/util/CPPUtility/Pointer.h>
#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

#include "../JointControllers/JointController.h"
#include "../util/JointAndNJointControllers.h"
#include "../util.h"

/**
 * @defgroup Library-RobotUnit-Modules RobotUnitModules
 * @ingroup Library-RobotUnit
 */

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(RobotUnit);
    TYPEDEF_PTRS_HANDLE(NJointControllerBase);

    template<class Targ, class Src>
    Targ& CheckedCastAndDeref(Src* src)
    {
        Targ* ptr = dynamic_cast<Targ*>(src);
        if (!ptr)
        {
            if (!src)
            {
                throw std::invalid_argument {"Ptr passed to CheckedCastAndDeref is NULL"};
            }
            throw std::invalid_argument
            {
                "Ptr of type '" +
                GetTypeString<Src>() +
                "' passed to CheckedCastAndDeref is is not related to target type '" +
                GetTypeString<Targ>()
            };
        }
        return *ptr;
    }

    /// @brief The current state of the multi step initialization of a RobotUnit
    enum class RobotUnitState : std::size_t
    {
        InitializingComponent,
        InitializingDevices,
        //ok to use devices/create controllers
        InitializingUnits,
        InitializingControlThread,
        Running,
        Exiting
    };

    std::string to_string(armarx::RobotUnitState s);
    inline std::ostream& operator<<(std::ostream& o, RobotUnitState s)
    {
        return o << to_string(s);
    }
}

namespace armarx::RobotUnitModule::detail
{
    class ModuleBaseIntermediatePropertyDefinitions: public ComponentPropertyDefinitions
    {
    public:
        ModuleBaseIntermediatePropertyDefinitions(): ComponentPropertyDefinitions("") {}
    };
}

namespace armarx::RobotUnitModule
{
    class ModuleBasePropertyDefinitions: virtual public detail::ModuleBaseIntermediatePropertyDefinitions
    {
    public:
        ModuleBasePropertyDefinitions(std::string prefix)
        {
            setDescription(prefix + " properties");
            setPrefix(prefix + ".");
        }
    };


#define forward_declare(r, data, Mod) TYPEDEF_PTRS_HANDLE(Mod);
    BOOST_PP_SEQ_FOR_EACH(forward_declare,, BOOST_PP_VARIADIC_TO_SEQ(RobotUnitModules))
#undef forward_declare

    /**
     * @ingroup Library-RobotUnit-Modules
     * @brief Base class for all RobotUnitModules
     *
     * Since the robot unit is a complex class, it is split into several modules.
     * One reason behind this is stopping developers from accidently using datastructures in the wrong way (e.g. causing racing conditions)
     * For a \ref RobotUnit to work, ALL modules are required (A class has to derive all of them)
     *
     * \section Modules
     * The list of Modules is:
     *    - \ref ControllerManagement
     *    - \ref ControlThread
     *    - \ref ControlThreadDataBuffer
     *    - \ref Devices
     *    - \ref Logging
     *    - \ref Management
     *    - \ref Publisher
     *    - \ref RobotData
     *    - \ref SelfCollisionChecker
     *    - \ref Units
     *
     * \section LifeCycle Life cycle
     * The RobotUnit has multiple Phases (see \ref RobotUnitState) as well as the life cycle of a \ref ManagedIceObject.
     *
     * \subsection LifeCycleMIO ManagedIceObject Life cycle
     * The hooks (\ref onInitComponent, \ref onConnectComponent, \ref onDisconnectComponent, \ref onExitComponent)
     * for state transitions provided by \ref ManagedIceObject have a final overrider.
     * Deriving classes have to use the replacement hooks:
     *
    <table>
        <tr><th> Hook                       <th> Replacement           \th
        <tr><td> \ref onInitComponent       <td> \ref onInitRobotUnit
        <tr><td> \ref onConnectComponent    <td> \ref onConnectRobotUnit
        <tr><td> \ref onDisconnectComponent <td> \ref onDisconnectRobotUnit
        <tr><td> \ref onExitComponent       <td> \ref onExitRobotUnit
    </table>

     * \subsection LifeCycleRU RobotUnit Life cycle
     *
     * The initial phase is \ref RobotUnitState::InitializingComponent.
     * The following table shows all Phase transitions and the functions causing it:
     *
    <table>
        <tr><th> From                                            <th> To                                             <th> Transition function                <th> Note                              \th
        <tr><td> \ref RobotUnitState::InitializingComponent      <td> \ref RobotUnitState::InitializingDevices       <td> \ref finishComponentInitialization <td> Called in \ref onInitComponent
        <tr><td> \ref RobotUnitState::InitializingDevices        <td> \ref RobotUnitState::InitializingUnits         <td> \ref finishDeviceInitialization    <td>
        <tr><td> \ref RobotUnitState::InitializingUnits          <td> \ref RobotUnitState::InitializingControlThread <td> \ref finishUnitInitialization      <td>
        <tr><td> \ref RobotUnitState::InitializingControlThread  <td> \ref RobotUnitState::Running                   <td> \ref finishControlThreadInitialization                  <td> Has to be called in the control thread
        <tr><td> \ref RobotUnitState::Running                    <td> \ref RobotUnitState::Exiting                   <td> \ref finishRunning                 <td> Called in \ref onExitComponent
    </table>
     *
     *
     *
     * \subsection RUMLifeCycleHooks RobotUnitModule Hooks for Life Cycle transition functions
     *
     * Each RobotUnitModule has two hooks each transition function ().
     *     -  The '_pre<TRANSITION_FUNCTION_NAME>' hook is called in the transition function before the state has changed.
     *     -  The '_post<TRANSITION_FUNCTION_NAME>' hook is called in the transition function after the state has changed.
     *
     * \warning The same hook of different modules (e.g.: \ref _preFinishDeviceInitialization, \ref _postOnInitRobotUnit) must not depend on each other.
     */
    class ModuleBase : virtual public Component
    {
        /// @brief The singleton instance
        static std::atomic<ModuleBase*> Instance_;
    public:
        /**
         * @brief Returns the singleton instance of this class
         * @return The singleton instance of this class
         */
        static ModuleBase& Instance()
        {
            ARMARX_CHECK_NOT_NULL(Instance_);
            return *Instance_;
        }
        /**
         * @brief Returns the singleton instance of the given class
         * @return The singleton instance of the given class
         */
        template<class T>
        static T& Instance()
        {
            static_assert(std::is_base_of<ModuleBase, T>::value, "The type has to derive ModuleBase");
            ARMARX_CHECK_NOT_NULL(Instance_);
            return dynamic_cast<T&>(*Instance_);
        }
    protected:
        /// @brief Ctor
        ModuleBase();
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////////// types ///////////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    protected:
        /**
         * @brief The type of recursive_mutex used in this class.
         * This typedef is used to quickly replace the mutex type with a logging mutex type for debugging.
         */
        using MutexType = std::recursive_timed_mutex;
        using GuardType = std::unique_lock<MutexType>;
        using ClockType = std::chrono::high_resolution_clock;
        using TimePointType = std::chrono::high_resolution_clock::time_point;

#define using_module_types(...) using_module_types_impl(__VA_ARGS__)
#define using_module_types_impl(r, data, Mod) using Module ## Mod = Mod;
        BOOST_PP_SEQ_FOR_EACH(using_module_types,, BOOST_PP_VARIADIC_TO_SEQ(RobotUnitModules))
#undef using_module_types
#undef using_module_types_impl
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////////// access to modules /////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    protected:
        /// @brief Checks whether the implementing class derives all modules.
        void checkDerivedClasses() const;
    public:
        /**
         * @brief Returns this as ref to the given type.
         * @return This as ref to the given type.
         */
        template<class T>       T& _module()
        {
            return dynamic_cast<      T&>(*this);
        }
        /**
         * @brief Returns this as ref to the given type.
         * @return This as ref to the given type.
         */
        template<class T> const T& _module() const
        {
            return dynamic_cast<const T&>(*this);
        }
        /**
         * @brief Returns this as ptr to the given type.
         * @return This as ptr to the given type.
         */
        template<class T>       T* _modulePtr()
        {
            return &_module<      T>();
        }
        /**
         * @brief Returns this as ptr to the given type.
         * @return This as ptr to the given type.
         */
        template<class T> const T* _modulePtr() const
        {
            return &_module<const T>();
        }
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////// ManagedIceObject life cycle ///////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    protected:
        //ManagedIceObject life cycle
        /**
         * @brief \note This function is protected with \ref getGuard()
         * @see \ref ManagedIceObject::onInitComponent
         */
        void onInitComponent()       final override;
        /**
         * @brief \warning This function is not protected with \ref getGuard()
         * @see \ref ManagedIceObject::onConnectComponent
         */
        void onConnectComponent()    final override;
        /**
         * @brief \warning This function is not protected with \ref getGuard()
         * @see \ref ManagedIceObject::onDisconnectComponent
         */
        void onDisconnectComponent() final override;
        /**
         * @brief \note This function is protected with \ref getGuard()
         * @see \ref ManagedIceObject::onExitComponent
         */
        void onExitComponent()       final override;

        //ManagedIceObject life cycle module hooks
        /**
         * @brief Hook for deriving RobotUnitModules called in \ref onInitComponent (called before \ref onInitRobotUnit was called)
         * \note This function is protected with \ref getGuard()
         * @see onInitComponent
         */
        void _preOnInitRobotUnit() {}
        /**
         * @brief Hook for deriving RobotUnitModules called in \ref onInitComponent (called after \ref onInitRobotUnit was called)
         * \note This function is protected with \ref getGuard()
         * @see onInitComponent
         */
        void _postOnInitRobotUnit() {}

        /**
         * @brief Hook for deriving RobotUnitModules called in \ref onConnectComponent (called before \ref onConnectRobotUnit was called)
         * \warning This function is not protected with \ref getGuard()
         * @see onConnectComponent
         */
        void _preOnConnectRobotUnit() {}
        /**
         * @brief Hook for deriving RobotUnitModules called in \ref onConnectComponent (called after \ref onConnectRobotUnit was called)
         * \warning This function is not protected with \ref getGuard()
         * @see onConnectComponent
         */
        void _postOnConnectRobotUnit() {}

        /**
         * @brief Hook for deriving RobotUnitModules called in \ref onDisconnectComponent (called before \ref onDisconnectRobotUnit was called)
         * \warning This function is not protected with \ref getGuard()
         * @see onDisconnectComponent
         */
        void _preOnDisconnectRobotUnit() {}
        /**
         * @brief Hook for deriving RobotUnitModules called in \ref onDisconnectComponent (called after \ref onDisconnectRobotUnit was called)
         * \warning This function is not protected with \ref getGuard()
         * @see onDisconnectComponent
         */
        void _postOnDisconnectRobotUnit() {}

        /**
         * @brief Hook for deriving RobotUnitModules called in \ref onExitComponent (called before \ref onExitRobotUnit was called)
         * \note This function is protected with \ref getGuard()
         * @see onExitComponent
         */
        void _preOnExitRobotUnit() {}
        /**
         * @brief Hook for deriving RobotUnitModules called in \ref onExitComponent (called after \ref onExitRobotUnit was called)
         * \note This function is protected with \ref getGuard()
         * @see onExitComponent
         */
        void _postOnExitRobotUnit() {}
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ////////////////////// other ManagedIceObject / Component methods ////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    protected:
        /// @see \ref ManagedIceObject::componentPropertiesUpdated
        void componentPropertiesUpdated(const std::set<std::string>& changedProperties) override;
        /// @see \ref ManagedIceObject::icePropertiesInitialized
        void icePropertiesInitialized() override;
        /// @see \ref ManagedIceObject::getDefaultName
        std::string getDefaultName() const override
        {
            return "RobotUnit";
        }

        //module hooks
        /// @brief Hook for deriving RobotUnitModules called in \ref componentPropertiesUpdated
        void _componentPropertiesUpdated(const std::set<std::string>& /*changedProperties*/) {}
        /// @brief Hook for deriving RobotUnitModules called in \ref icePropertiesInitialized
        void _icePropertiesInitialized() {}
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////// state and state transition ////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /**
         * @brief Sets the \ref RobotUnit's state to \param s
         * @param s The state to set
         */
        void setRobotUnitState(RobotUnitState s);
    protected:
        //state transitions
        /**
         * @brief Transition \ref RobotUnitState::InitializingComponent -> \ref RobotUnitState::InitializingDevices
         * \note This function is protected with \ref getGuard()
         * @see _preFinishComponentInitialization
         * @see _postFinishComponentInitialization
         */
        virtual void finishComponentInitialization();
        /**
         * @brief Transition \ref RobotUnitState::InitializingDevices -> \ref RobotUnitState::InitializingUnits
         * \note This function is protected with \ref getGuard()
         * @see _preFinishDeviceInitialization
         * @see _postFinishDeviceInitialization
         */
        virtual void finishDeviceInitialization();
        /**
         * @brief Transition \ref RobotUnitState::InitializingUnits -> \ref RobotUnitState::WaitingForRTThreadInitialization
         * \note This function is protected with \ref getGuard()
         * @see _preFinishUnitInitialization
         * @see _postFinishUnitInitialization
         */
        virtual void finishUnitInitialization();
        /**
         * @brief Transition \ref RobotUnitState::InitializingControlThread -> \ref RobotUnitState::Running
         * \note This function is protected with \ref getGuard()
         * @see _preFinishControlThreadInitialization
         * @see _postFinishControlThreadInitialization
         */
        virtual void finishControlThreadInitialization();
        /**
         * @brief Transition \ref RobotUnitState::Running -> \ref RobotUnitState::Exiting
         * \note This function is protected with \ref getGuard()
         * @see _preFinishRunning
         * @see _postFinishRunning
         */
        virtual void finishRunning();

        //state transition module hooks
        /**
         * @brief Hook for deriving RobotUnitModules called in \ref finishComponentInitialization (called before the state has changed)
         * \note This function is protected with \ref getGuard()
         * @see finishComponentInitialization
         */
        void _preFinishComponentInitialization() {}
        /**
         * @brief Hook for deriving RobotUnitModules called in \ref finishComponentInitialization (called after the state has changed)
         * \note This function is protected with \ref getGuard()
         * @see finishComponentInitialization
         */
        void _postFinishComponentInitialization() {}

        /**
         * @brief Hook for deriving RobotUnitModules called in \ref finishDeviceInitialization (called before the state has changed)
         * \note This function is protected with \ref getGuard()
         * @see finishDeviceInitialization
         */
        void _preFinishDeviceInitialization() {}
        /**
         * @brief Hook for deriving RobotUnitModules called in \ref finishDeviceInitialization (called after the state has changed)
         * \note This function is protected with \ref getGuard()
         * @see finishDeviceInitialization
         */
        void _postFinishDeviceInitialization() {}

        /**
         * @brief Hook for deriving RobotUnitModules called in \ref finishUnitInitialization (called before the state has changed)
         * \note This function is protected with \ref getGuard()
         * @see finishUnitInitialization
         */
        void _preFinishUnitInitialization() {}
        /**
         * @brief Hook for deriving RobotUnitModules called in \ref finishUnitInitialization (called after the state has changed)
         * \note This function is protected with \ref getGuard()
         * @see finishUnitInitialization
         */
        void _postFinishUnitInitialization() {}

        /**
         * @brief Hook for deriving RobotUnitModules called in \ref finishControlThreadInitialization (called before the state has changed)
         * \note This function is protected with \ref getGuard()
         * @see finishControlThreadInitialization
         */
        void _preFinishControlThreadInitialization() {}
        /**
         * @brief Hook for deriving RobotUnitModules called in \ref finishControlThreadInitialization (called after the state has changed)
         * \note This function is protected with \ref getGuard()
         * @see finishControlThreadInitialization
         */
        void _postFinishControlThreadInitialization() {}

        /**
         * @brief Hook for deriving RobotUnitModules called in \ref finishRunning (called before the state has changed)
         * \note This function is protected with \ref getGuard()
         * @see finishRunning
         */
        void _preFinishRunning() {}
        /**
         * @brief Hook for deriving RobotUnitModules called in \ref finishRunning (called after the state has changed)
         * \note This function is protected with \ref getGuard()
         * @see finishRunning
         */
        void _postFinishRunning() {}
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// utility //////////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    public:
        /**
         * @brief Returns the \ref RobotUnit's State
         * @return The \ref RobotUnit's State
         */
        RobotUnitState getRobotUnitState() const;

        /**
         * @brief Throws an exception if the current state is not in \param stateSet
         * @param stateSet The set of acceptable \ref RobotUnitState "RobotUnitStates"
         * @param fnc The callers function name.
         * @param onlyWarn Only print a warning instead of throwing an exception.
         */
        void throwIfStateIsNot(const std::set<RobotUnitState>& stateSet, const std::string& fnc, bool onlyWarn = false) const;
        /**
         * @brief Throws an exception if the current state is not \param state
         * @param state The acceptable \ref RobotUnitState
         * @param fnc The callers function name.
         * @param onlyWarn Only print a warning instead of throwing an exception.
         */
        void throwIfStateIsNot(RobotUnitState s, const std::string& fnc, bool onlyWarn = false) const;
        /**
         * @brief Throws an exception if the current state is in \param stateSet
         * @param stateSet The set of forbidden \ref RobotUnitState "RobotUnitStates"
         * @param fnc The callers function name.
         * @param onlyWarn Only print a warning instead of throwing an exception.
         */
        void throwIfStateIs(const std::set<RobotUnitState>& stateSet, const std::string& fnc, bool onlyWarn = false) const;
        /**
         * @brief Throws an exception if the current state is \param state
         * @param state The forbidden \ref RobotUnitState
         * @param fnc The callers function name.
         * @param onlyWarn Only print a warning instead of throwing an exception.
         */
        void throwIfStateIs(RobotUnitState s, const std::string& fnc, bool onlyWarn = false) const;
        /**
         * @brief Throws if the \ref Device "Devices" are not ready.
         * @param fnc The callers function name.
         * @see areDevicesReady
         */
        void throwIfDevicesNotReady(const std::string& fnc) const;

        /**
         * @brief Returns whether \ref Device "Devices" are ready
         * @return Whether \ref Device "Devices" are ready
         * @see \ref DevicesReadyStates
         */
        bool areDevicesReady() const;

        /**
         * @brief Returns whether the current thread is the \ref ControlThread.
         * @return Whether the current thread is the \ref ControlThread.
         */
        bool inControlThread() const;

        /**
         * @brief Throws if the current thread is the \ref ControlThread.
         * @param fnc The callers function name.
         */
        void throwIfInControlThread(const std::string& fnc) const;

        /**
         * @brief Returns whether the \ref RobotUnit is shutting down
         * @return Whether the \ref RobotUnit is shutting down
         * @see shutDown
         */
        bool isShuttingDown() const ///TODO use this function in implementations
        {
            return isShuttingDown_.load();
        }
        /**
         * @brief Requests the \ref RobotUnit to shut down.
         * @see isShuttingDown
         */
        void shutDown() ///TODO use this function in implementations
        {
            isShuttingDown_.store(true);
        }
        // //////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////// robot unit impl hooks ////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////// //
        /**
         * @brief called in onInitComponent
         * \note This function is protected with \ref getGuard()
         * @see onInitComponent
         */
        virtual void onInitRobotUnit() {}
        /**
         * @brief called in onConnectComponent
         * \warning This function is not protected with \ref getGuard()
         * @see onConnectComponent
         */
        virtual void onConnectRobotUnit() {}
        /**
         * @brief called in onDisconnecComponent
         * \warning This function is not protected with \ref getGuard()
         * @see onDisconnecComponent
         */
        virtual void onDisconnectRobotUnit() {}
        /**
         * @brief called in onExitComponent before calling finishRunning
         * \note This function is protected with \ref getGuard()
         * @see finishRunning
         * @see onExitComponent
         */
        virtual void onExitRobotUnit() {}

        /// @brief Implementations have to join their \ref ControlThread in this hook. (used by RobotUnit::finishRunning())
        virtual void joinControlThread() = 0;

        /// @brief The \ref ControlThread's period
        virtual IceUtil::Time getControlThreadTargetPeriod() const = 0;
        // //////////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////////// other /////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////// //
    protected:
        /**
         * @brief Returns a sentinel value for an index (std::numeric_limits<std::size_t>::max())
         * @return A sentinel value for an index (std::numeric_limits<std::size_t>::max())
         */
        static constexpr std::size_t IndexSentinel()
        {
            return std::numeric_limits<std::size_t>::max();
        }
        /**
         * @brief Returns a guard to the \ref RobotUnits mutex.
         * @return A guard to the \ref RobotUnits mutex.
         * @throw throws If called in the \ref ControlThread or on shutdown.
         */
        GuardType getGuard() const;
        // //////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////////////// Data ///////////////////////////////////////// //
        // //////////////////////////////////////////////////////////////////////////////////////// //
    private:
        /// @brief The \ref RobotUnits mutex.
        mutable MutexType dataMutex;
        /// @brief Set of \ref RobotUnitState "RobotUnitStates" where \ref Device "Devices" are usable
        static const std::set<RobotUnitState> DevicesReadyStates;
        /// @brief The \ref RobotUnit's current state
        std::atomic<RobotUnitState> state {RobotUnitState::InitializingComponent};
        /// @brief Whether the \ref RobotUnit is shutting down
        std::atomic_bool isShuttingDown_ {false};
    };
}

#include "RobotUnitModuleBase.ipp"

