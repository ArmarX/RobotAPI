/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <chrono>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>

namespace armarx::detail
{
    template<class TimeT = std::chrono::microseconds>
    struct TimerTag
    {
        //assume it is std::chrono

        using ClockT = std::chrono::high_resolution_clock;
        const ClockT::time_point beg;

        TimerTag() : beg {ClockT::now()} {}

        TimeT stop()
        {
            return std::chrono::duration_cast<TimeT>(ClockT::now() - beg);
        }
    };

    template<>
    struct TimerTag<long>
    {
        //return micro seconds as long

        using ClockT = std::chrono::high_resolution_clock;
        const ClockT::time_point beg;

        TimerTag() : beg {ClockT::now()} {}

        long stop()
        {
            return std::chrono::duration_cast<std::chrono::microseconds>(ClockT::now() - beg).count();
        }
    };

    template<>
    struct TimerTag<TimestampVariant> : TimerTag<>
    {
        TimestampVariant stop()
        {
            return {TimerTag<std::chrono::microseconds>::stop().count()};
        }
    };

    template<>
    struct TimerTag<IceUtil::Time> : TimerTag<>
    {
        TimestampVariant stop()
        {
            return IceUtil::Time::microSeconds(TimerTag<std::chrono::microseconds>::stop().count());
        }
    };

    template <class Fun, class TimeT>
    TimeT operator*(TimerTag<TimeT>&& t, Fun&& fn)
    {
        fn();
        return t.stop();
    }

    //for virtual time

    template<class TimeT = IceUtil::Time>
    struct VirtualTimerTag;

    template<>
    struct VirtualTimerTag<TimestampVariant>
    {
        const IceUtil::Time beg;
        VirtualTimerTag() : beg {TimeUtil::GetTime()} {}
        TimestampVariant stop()
        {
            return {TimeUtil::GetTime() - beg};
        }
    };

    template<>
    struct VirtualTimerTag<IceUtil::Time>
    {
        const IceUtil::Time beg;
        VirtualTimerTag() : beg {TimeUtil::GetTime()} {}
        TimestampVariant stop()
        {
            return TimeUtil::GetTime() - beg;
        }
    };

    template <class Fun, class TimeT>
    TimeT operator*(VirtualTimerTag<TimeT>&& t, Fun&& fn)
    {
        fn();
        return t.stop();
    }
}

#define ARMARX_STOPWATCH(...) ::armarx::detail::TimerTag<__VA_ARGS__>{} *[&]
#define ARMARX_VIRTUAL_STOPWATCH(...) ::armarx::detail::VirtualTimerTag<__VA_ARGS__>{} *[&]
