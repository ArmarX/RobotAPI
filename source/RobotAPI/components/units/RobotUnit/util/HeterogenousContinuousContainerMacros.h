/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>
#include <type_traits>

#define ARMARX_MINIMAL_PLACEMENT_CONSTRUCTION_HELPER_BASE(CommonBaseType)   \
    using _typeBase = CommonBaseType;                                       \
    virtual std::size_t _alignof() const = 0;                               \
    virtual std::size_t _sizeof() const = 0;                                \
    virtual _typeBase* _placementCopyConstruct(void* place) const = 0;      \
    std::size_t _accumulateSize(std::size_t offset = 0) const               \
    {                                                                       \
        return offset +                                                     \
               (_alignof() - (offset % _alignof())) % _alignof() +          \
               _sizeof();                                                   \
    }

#define ARMARX_PLACEMENT_CONSTRUCTION_HELPER_BASE(CommonBaseType)       \
    ARMARX_MINIMAL_PLACEMENT_CONSTRUCTION_HELPER_BASE(CommonBaseType)   \
    virtual void _copyTo(_typeBase* target) const = 0;                  \
    virtual std::unique_ptr<_typeBase> _clone() const = 0;              \
    virtual _typeBase* _placementConstruct(void* place) const = 0;

#define ARMARX_MINIMAL_PLACEMENT_CONSTRUCTION_HELPER                \
    std::size_t _alignof() const override                           \
    {                                                               \
        using _type = typename std::decay<decltype(*this)>::type;   \
        return alignof(_type);                                      \
    }                                                               \
    std::size_t _sizeof() const override                            \
    {                                                               \
        using _type = typename std::decay<decltype(*this)>::type;   \
        return sizeof(_type);                                       \
    }                                                               \
    _typeBase* _placementCopyConstruct(void* place) const override  \
    {                                                               \
        using _type = typename std::decay<decltype(*this)>::type;   \
        return new(place) _type(*this);                             \
    }                                                               \
    void _checkBaseType()                                           \
    {                                                               \
        using _type = typename std::decay<decltype(*this)>::type;   \
        static_assert(                                              \
                std::is_base_of<_typeBase, _type>::value,               \
                "This class has to derive the common base class");      \
    }

#define ARMARX_PLACEMENT_CONSTRUCTION_HELPER                        \
    ARMARX_MINIMAL_PLACEMENT_CONSTRUCTION_HELPER                    \
    void _copyTo(_typeBase* target) const override                  \
    {                                                               \
        using _type = typename std::decay<decltype(*this)>::type;   \
        _type* const castedTarget = dynamic_cast<_type*>(target);   \
        ARMARX_CHECK_NOT_NULL(castedTarget);                        \
        *castedTarget = *this;                                      \
    }                                                               \
    std::unique_ptr<_typeBase> _clone() const override              \
    {                                                               \
        using _type = typename std::decay<decltype(*this)>::type;   \
        return std::unique_ptr<_typeBase>{new _type(*this)};        \
    }                                                               \
    _typeBase* _placementConstruct(void* place) const override      \
    {                                                               \
        using _type = typename std::decay<decltype(*this)>::type;   \
        return new(place) _type;                                    \
    }
