/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <atomic>
namespace armarx
{
    template <typename T>
    struct AtomicWrapper
    {
        std::atomic<T> atom;

        AtomicWrapper()                           : atom {} {}
        AtomicWrapper(const T& v)                 : atom {v} {}
        AtomicWrapper(const std::atomic<T>& a)    : atom {a.load()} {}
        AtomicWrapper(const AtomicWrapper& other) : atom {other.atom.load()} {}
        AtomicWrapper(AtomicWrapper&&) = default;
        AtomicWrapper& operator=(const T& v)
        {
            atom.store(v);
            return *this;
        }
        AtomicWrapper& operator=(const std::atomic<T>& a)
        {
            atom.store(a.load());
            return *this;
        }
        AtomicWrapper& operator=(const AtomicWrapper& other)
        {
            atom.store(other.atom.load());
            return *this;
        }
        AtomicWrapper& operator=(AtomicWrapper&&) = default;

        operator T() const
        {
            return atom.load();
        }
    };
}
