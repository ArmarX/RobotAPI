/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <chrono>
//#include <ArmarXCore/core/time/TimeUtil.h>
#include "ControlThreadOutputBuffer.h"

namespace armarx
{
    inline IceUtil::Time rtNow()
    {
        return IceUtil::Time::now(IceUtil::Time::Monotonic);
        //        using namespace std::chrono;
        //        auto epoch = time_point_cast<microseconds>(high_resolution_clock::now()).time_since_epoch();
        //        return IceUtil::Time::microSeconds(duration_cast<milliseconds>(epoch).count());
    }
}

#define RT_TIMING_START(name) auto name = armarx::rtNow();
//! \ingroup VirtualTime
//! Prints duration with comment in front of it, yet only once per second.
#define RT_TIMING_END_COMMENT(name, comment) ARMARX_RT_LOGF_INFO("%s - duration: %.3f ms", comment, IceUtil::Time(armarx::rtNow()-name).toMilliSecondsDouble()).deactivateSpam(1);
//! \ingroup VirtualTime
//! Prints duration
#define RT_TIMING_END(name) RT_TIMING_END_COMMENT(name,#name)
//! \ingroup VirtualTime
//! Prints duration with comment in front of it if it took longer than threshold
#define RT_TIMING_CEND_COMMENT(name, comment, thresholdMs) if((armarx::rtNow()-name).toMilliSecondsDouble() >= thresholdMs) RT_TIMING_END_COMMENT(name, comment)
//! \ingroup VirtualTime
//! Prints duration if it took longer than thresholdMs
#define RT_TIMING_CEND(name, thresholdMs) RT_TIMING_CEND_COMMENT(name, #name, thresholdMs)


