/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Dynamics/Dynamics.h>
#include <Eigen/Core>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>

namespace armarx::rtfilters
{
    class RTFilterBase;
    using RTFilterBasePtr = std::shared_ptr<RTFilterBase>;
}

namespace armarx
{
    class RobotUnit;
    class DynamicsHelper
    {
    public:
        struct DynamicsData
        {
            DynamicsData(VirtualRobot::RobotNodeSetPtr rnsJoints, VirtualRobot::RobotNodeSetPtr rnsBodies) :
                rnsJoints(rnsJoints), rnsBodies(rnsBodies),
                dynamics(rnsJoints, rnsBodies)
            {
                q = qDot = qDDot = tau = tauGravity = Eigen::VectorXd::Zero(rnsJoints->getSize());
            }
            DynamicsData(const DynamicsData& r) = default;
            VirtualRobot::RobotNodeSetPtr rnsJoints, rnsBodies;
            VirtualRobot::Dynamics dynamics;
            Eigen::VectorXd q, qDot, qDDot, tau, tauGravity;
            std::vector<std::pair<VirtualRobot::RobotNodePtr, SensorValue1DoFActuator*>> nodeSensorVec;
            std::vector<rtfilters::RTFilterBasePtr> velocityFilter;
            std::vector<rtfilters::RTFilterBasePtr> accelerationFilter;
        };

        DynamicsHelper(RobotUnit* robotUnit);
        ~DynamicsHelper() = default;
        void addRobotPart(VirtualRobot::RobotNodeSetPtr rnsJoints, VirtualRobot::RobotNodeSetPtr rnsBodies,
                          rtfilters::RTFilterBasePtr velocityFilter = rtfilters::RTFilterBasePtr(),
                          rtfilters::RTFilterBasePtr accelerationFilter = rtfilters::RTFilterBasePtr());
        void update(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration);
    protected:
        RobotUnit* robotUnit;
        std::map<VirtualRobot::RobotNodeSetPtr, DynamicsData> dataMap;
    };

}
