/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotUnit
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <vector>

namespace armarx
{
    class JointController;
    class NJointControllerBase;
    /// @brief Structure used by the RobotUnit to swap lists of Joint and NJoint controllers
    struct JointAndNJointControllers
    {
        JointAndNJointControllers(std::size_t n = 0)
            :   jointControllers(n),
                nJointControllers(n),
                jointToNJointControllerAssignement(n, Sentinel())
        {}

        void resetAssignement()
        {
            jointToNJointControllerAssignement.assign(jointControllers.size(), Sentinel());
        }

        std::vector<JointController*> jointControllers;
        std::vector<NJointControllerBase*> nJointControllers;
        /// @brief this is set by RobotUnit::writeRequestedControllers
        std::vector<std::size_t> jointToNJointControllerAssignement;

        static constexpr std::size_t Sentinel()
        {
            return std::numeric_limits<std::size_t>::max();
        }
    };
}
