set(LIB_NAME       RobotUnit)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

set(LIBS
        EmergencyStop
        ArmarXGuiInterfaces
        RobotAPIInterfaces
        RobotAPIUnits
        DefaultWidgetDescriptions
)

set(LIB_FILES
    util.cpp
    util/ControlThreadOutputBuffer.cpp
    util/DynamicsHelper.cpp
    util/introspection/DataFieldsInfo.cpp
    SyntaxCheck.cpp
    RobotUnit.cpp
    RobotUnitObserver.cpp
    BasicControllers.cpp
    DefaultWidgetDescriptions.cpp

    #robot unit modules need to be added to the list below (but not here)
    RobotUnitModules/RobotUnitModuleBase.cpp

    ControlTargets/ControlTargetBase.cpp

    SensorValues/SensorValueBase.cpp

    Units/RobotUnitSubUnit.cpp
    Units/ForceTorqueSubUnit.cpp
    Units/InertialMeasurementSubUnit.cpp
    Units/KinematicSubUnit.cpp
    Units/PlatformSubUnit.cpp
    Units/LocalizationSubUnit.cpp
    Units/TrajectoryControllerSubUnit.cpp
    Units/TCPControllerSubUnit.cpp

    JointControllers/JointController.cpp

    NJointControllers/NJointController.cpp
    NJointControllers/NJointTrajectoryController.cpp
    NJointControllers/NJointKinematicUnitPassThroughController.cpp
    NJointControllers/NJointHolonomicPlatformUnitVelocityPassThroughController.cpp
    NJointControllers/NJointHolonomicPlatformRelativePositionController.cpp
    NJointControllers/NJointHolonomicPlatformGlobalPositionController.cpp
    NJointControllers/NJointTCPController.cpp
    NJointControllers/NJointCartesianVelocityController.cpp
    NJointControllers/NJointCartesianTorqueController.cpp
    NJointControllers/NJointTaskSpaceImpedanceController.cpp
    NJointControllers/NJointCartesianVelocityControllerWithRamp.cpp
    NJointControllers/NJointCartesianWaypointController.cpp
    NJointControllers/NJointCartesianNaturalPositionController.cpp

    ControllerParts/CartesianImpedanceController.cpp

    Devices/SensorDevice.cpp
    Devices/ControlDevice.cpp

    Devices/GlobalRobotPoseSensorDevice.cpp

)
set(LIB_HEADERS
    util.h
    util/EigenForwardDeclarations.h
    util/HeterogenousContinuousContainer.h
    util/HeterogenousContinuousContainerMacros.h
    util/KeyValueVector.h
    util/AtomicWrapper.h
    util/Time.h
    util/DynamicsHelper.h
    util/ControlThreadOutputBuffer.h
    util/JointAndNJointControllers.h
    util/introspection/DataFieldsInfo.h
    util/introspection/ClassMemberInfoEntry.h
    util/introspection/ClassMemberInfo.h
    util/RtLogging.h
    util/RtTiming.h
    util/CtrlUtil.h

    #robot unit modules need to be added to the list below (but not here)
    RobotUnitModules/RobotUnitModuleBase.h
    RobotUnitModules/RobotUnitModuleBase.ipp
    RobotUnitModules/RobotUnitModules.h

    PDController.h
    Constants.h
    ControlModes.h
    RobotUnit.h
    RobotUnitObserver.h
    BasicControllers.h
    DefaultWidgetDescriptions.h

    ControlTargets/ControlTargetBase.h
    ControlTargets/ControlTarget1DoFActuator.h
    ControlTargets/ControlTargetHolonomicPlatformVelocity.h

    SensorValues/SensorValueBase.h
    SensorValues/SensorValue1DoFActuator.h
    SensorValues/SensorValueIMU.h
    SensorValues/SensorValueForceTorque.h
    SensorValues/SensorValueHolonomicPlatform.h
    SensorValues/SensorValueRTThreadTimings.h

    Units/RobotUnitSubUnit.h
    Units/ForceTorqueSubUnit.h
    Units/InertialMeasurementSubUnit.h
    Units/KinematicSubUnit.h
    Units/PlatformSubUnit.h
    Units/LocalizationSubUnit.h
    Units/TrajectoryControllerSubUnit.h
    Units/TCPControllerSubUnit.h

    JointControllers/JointController.h

    NJointControllers/NJointController.h
    NJointControllers/NJointControllerBase.h
    NJointControllers/NJointControllerRegistry.h
    NJointControllers/NJointControllerWithTripleBuffer.h
    NJointControllers/NJointTrajectoryController.h
    NJointControllers/NJointKinematicUnitPassThroughController.h
    NJointControllers/NJointHolonomicPlatformUnitVelocityPassThroughController.h
    NJointControllers/NJointHolonomicPlatformRelativePositionController.h
    NJointControllers/NJointHolonomicPlatformGlobalPositionController.h
    NJointControllers/NJointTCPController.h
    NJointControllers/NJointCartesianVelocityController.h
    NJointControllers/NJointCartesianTorqueController.h
    NJointControllers/NJointCartesianVelocityControllerWithRamp.h
    NJointControllers/NJointCartesianWaypointController.h
    NJointControllers/NJointCartesianNaturalPositionController.h

    ControllerParts/CartesianImpedanceController.h

    Devices/DeviceBase.h
    Devices/SensorDevice.h
    Devices/ControlDevice.h

    Devices/RTThreadTimingsSensorDevice.h
    Devices/GlobalRobotPoseSensorDevice.h
)
###########################################################################
#since the robot unit is a complex class, it is split into several modules.
#one reason behind this is stopping developers from using datastructures in the wrong way (eg causing racing conditions)
#for the robot unit to work, ALL modules are required (a class has to derive all of them)
#modules are autodetected in the RobotUnitModules folder, but should be listed here anyways
#(since glob does not autodetect new files)
#(see: https://cmake.org/cmake/help/v3.0/command/file.html)
# > If no CMakeLists.txt file changes when a source is added or removed then the generated build system cannot know when to ask CMake to regenerate.
set(RobotUnitModules
    Management
    ControllerManagement
    ControlThread
    ControlThreadDataBuffer
    Devices
    Logging
    Publisher
    RobotData
    SelfCollisionChecker
    Units
)
file(GLOB_RECURSE files_full_path "${CMAKE_CURRENT_SOURCE_DIR}/RobotUnitModules/*")
set(files)
foreach(file_full_path ${files_full_path})
    if(NOT "${file_full_path}" MATCHES "^.*\\.(autosave|swp|~|orig)$")
        #remove prefix
        string(REGEX REPLACE "${CMAKE_CURRENT_SOURCE_DIR}/" "" file_rel_path "${file_full_path}")
        list(APPEND files ${file_rel_path})
    endif()
endforeach()
#remove base module / collection header
list(REMOVE_ITEM files RobotUnitModules/RobotUnitModuleBase.h)
list(REMOVE_ITEM files RobotUnitModules/RobotUnitModuleBase.cpp)
list(REMOVE_ITEM files RobotUnitModules/RobotUnitModuleBase.ipp)
list(REMOVE_ITEM files RobotUnitModules/RobotUnitModules.h)

#remove all expected files
foreach(module ${RobotUnitModules})
    set(ModuleName RobotUnitModule${module})
    set(ModuleFileBase "RobotUnitModules/${ModuleName}")

    #h
    list(FIND files "${ModuleFileBase}.h" idx)

    if(idx EQUAL -1)
        message(STATUS "Files:")
        printlist("     " "${files}")
        message(FATAL_ERROR "MODULE FILE NOT FOUND: '${ModuleFileBase}.h'")
    endif()
    list(REMOVE_ITEM files ${ModuleFileBase}.h)
    list(APPEND LIB_HEADERS ${ModuleFileBase}.h)

    #cpp
    list(FIND      files ${ModuleFileBase}.cpp idx)
    if(idx EQUAL -1)
        message(STATUS "Files:")
        printlist("     " "${files}")
        message(FATAL_ERROR "MODULE FILE NOT FOUND: '${ModuleFileBase}.cpp'")
    endif()
    list(REMOVE_ITEM files ${ModuleFileBase}.cpp)
    list(APPEND LIB_FILES ${ModuleFileBase}.cpp)

    #ipp
    list(FIND      files ${ModuleFileBase}.ipp idx)
    if(NOT idx EQUAL -1)
        list(APPEND LIB_HEADERS ${ModuleFileBase}.ipp)
        list(REMOVE_ITEM files ${ModuleFileBase}.ipp)
    endif()
    message(STATUS "Found RobotUnitModule ${ModuleName}")
endforeach()
#check remaining files
foreach(f ${files})
    if(f MATCHES ".*/RobotUnitModule[a-zA-Z]*.*" )
        message(FATAL_ERROR "MODULE NOT ADDED TO LIST OF MODULES: ${f}" )
    endif()
endforeach()

list_to_string(RobotUnitModules "," ${RobotUnitModules})

message(STATUS "collected these names for RobotUnitModules: ${RobotUnitModules}")
#done detecting modules!
###########################################################################
add_subdirectory(test)

armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

target_compile_definitions("${LIB_NAME}" PUBLIC "-DRobotUnitModules=${RobotUnitModules}")
