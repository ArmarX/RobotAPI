/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Peter Kaiser <peter dot kaiser at kit dot edu>
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <RobotAPI/components/units/SensorActorUnit.h>

#include <RobotAPI/interface/units/ForceTorqueUnit.h>

#include <string>

namespace armarx
{
    /**
     * \class ForceTorqueUnitPropertyDefinitions
     * \brief
     */
    class ForceTorqueUnitPropertyDefinitions : public ComponentPropertyDefinitions
    {
    public:
        ForceTorqueUnitPropertyDefinitions(std::string prefix) :
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("AgentName", "Name of the agent for which the sensor values are provided");
            defineOptionalProperty<std::string>("ForceTorqueTopicName", "ForceTorqueValues", "Name of the topic on which the sensor values are provided");


            // No required properties
        }
    };

    /**
     * \defgroup Component-ForceTorqueUnit ForceTorqueUnit
     * \ingroup RobotAPI-SensorActorUnits
     * \brief Base unit for force/torque sensors.
     *
     * The ForceTorqueUnit class is the base unit for force/torque sensors.
     * It implements the ForceTorqueUnit-interface and is responsible for distributing F/T sensor-values in ArmarX.
     * RobotAPI contains the ForceTorqueUnitSimulation class that does a very basic simulation of F/T-sensors.
     * Other (hardware related) implementations of this unit can be found in the respective ArmarX projects.
     */

    /**
     * @ingroup Component-ForceTorqueUnit
     * @brief The ForceTorqueUnit class
     */
    class ForceTorqueUnit :
        virtual public ForceTorqueUnitInterface,
        virtual public SensorActorUnit
    {
    public:
        std::string getDefaultName() const override
        {
            return "ForceTorqueUnit";
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;

        virtual void onInitForceTorqueUnit() = 0;
        virtual void onStartForceTorqueUnit() = 0;
        virtual void onExitForceTorqueUnit() = 0;

        PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        ForceTorqueUnitListenerPrx listenerPrx;
        std::string listenerName;
    };
}
