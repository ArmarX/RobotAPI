/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "OptoForceUnitObserver.h"


#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>
#include <ArmarXCore/observers/checks/ConditionCheckUpdated.h>

#include <RobotAPI/libraries/core/Pose.h>

#include <ArmarXCore/observers/variant/TimestampVariant.h>



namespace armarx
{
    void OptoForceUnitObserver::onInitObserver()
    {
        usingTopic(getProperty<std::string>("OptoForceTopicName").getValue());

        //offerConditionCheck("updated", new ConditionCheckUpdated());
        //offerConditionCheck("larger", new ConditionCheckLarger());
        //offerConditionCheck("equals", new ConditionCheckEquals());
        //offerConditionCheck("smaller", new ConditionCheckSmaller());

        offeringTopic(getProperty<std::string>("DebugDrawerTopic").getValue());
    }



    void OptoForceUnitObserver::onConnectObserver()
    {
        debugDrawerPrx = getTopic<DebugDrawerInterfacePrx>(getProperty<std::string>("DebugDrawerTopic").getValue());
    }


    void OptoForceUnitObserver::onExitObserver()
    {
        //debugDrawerPrx->removePoseVisu("IMU", "orientation");
        //debugDrawerPrx->removeLineVisu("IMU", "acceleration");
    }

    void OptoForceUnitObserver::reportSensorValues(const std::string& device, const std::string& name, float fx, float fy, float fz, const TimestampBasePtr& timestamp, const Ice::Current& c)
    {
        std::unique_lock lock(dataMutex);
        TimestampVariantPtr timestampPtr = TimestampVariantPtr::dynamicCast(timestamp);

        if (!existsChannel(name))
        {
            offerChannel(name, "Force data");
        }

        offerOrUpdateDataField(name, "name", Variant(name), "Name of the sensor");
        offerOrUpdateDataField(name, "device", Variant(device), "Device name");
        offerOrUpdateDataField(name, "fx", Variant(fx), "Force x");
        offerOrUpdateDataField(name, "fy", Variant(fy), "Force y");
        offerOrUpdateDataField(name, "fz", Variant(fz), "Force z");
        offerOrUpdateDataField(name, "timestamp", timestampPtr, "Timestamp");
        offerOrUpdateDataField(name, "force", Vector3(fx, fy, fz), "Force");

        updateChannel(name);
    }

    /*void OptoForceUnitObserver::reportSensorValues(const std::string& device, const std::string& name, const IMUData& values, const TimestampBasePtr& timestamp, const Ice::Current& c)
    {
        std::unique_lock lock(dataMutex);

        TimestampVariantPtr timestampPtr = TimestampVariantPtr::dynamicCast(timestamp);

        Vector3Ptr acceleration = new Vector3(values.acceleration.at(0), values.acceleration.at(1), values.acceleration.at(2));
        Vector3Ptr gyroscopeRotation = new Vector3(values.gyroscopeRotation.at(0), values.gyroscopeRotation.at(1), values.gyroscopeRotation.at(2));
        Vector3Ptr magneticRotation = new Vector3(values.magneticRotation.at(0), values.magneticRotation.at(1), values.magneticRotation.at(2));
        QuaternionPtr orientationQuaternion =  new Quaternion(values.orientationQuaternion.at(0), values.orientationQuaternion.at(1), values.orientationQuaternion.at(2), values.orientationQuaternion.at(3));

        if (!existsChannel(device))
        {
            offerChannel(device, "IMU data");
        }

        offerOrUpdateDataField(device, "name", Variant(name), "Name of the IMU sensor");
        offerValue(device, "acceleration", acceleration);
        offerValue(device, "gyroscopeRotation", gyroscopeRotation);
        offerValue(device, "magneticRotation", magneticRotation);
        offerValue(device, "acceleration", acceleration);
        offerOrUpdateDataField(device, "orientationQuaternion", orientationQuaternion,  "orientation quaternion values");
        offerOrUpdateDataField(device, "timestamp", timestampPtr, "Timestamp");

        updateChannel(device);

        Eigen::Vector3f zero;
        zero.setZero();

        DrawColor color;
        color.r = 1;
        color.g = 1;
        color.b = 0;
        color.a = 0.5;

        Eigen::Vector3f ac = acceleration->toEigen();
        ac *= 10;

        debugDrawerPrx->setLineVisu("IMU", "acceleration", new Vector3(), new Vector3(ac), 2.0f, color);

        PosePtr posePtr = new Pose(orientationQuaternion->toEigen(), zero);
        debugDrawerPrx->setPoseVisu("IMU", "orientation", posePtr);
        debugDrawerPrx->setBoxDebugLayerVisu("floor", new Pose(), new Vector3(5000, 5000, 1), DrawColor {0.7f, 0.7f, 0.7f, 1.0f});
    }*/

    void OptoForceUnitObserver::offerValue(std::string device, std::string fieldName, Vector3Ptr vec)
    {
        offerOrUpdateDataField(device, fieldName, vec, fieldName + " values");
        offerOrUpdateDataField(device, fieldName + "_x", vec->x, fieldName + "_x value");
        offerOrUpdateDataField(device, fieldName + "_y", vec->y, fieldName + "_y value");
        offerOrUpdateDataField(device, fieldName + "_z", vec->z, fieldName + "_z value");

    }


    PropertyDefinitionsPtr OptoForceUnitObserver::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new OptoForceUnitObserverPropertyDefinitions(getConfigIdentifier()));
    }
}
