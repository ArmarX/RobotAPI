/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     David Schiebener ( schiebener at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <RobotAPI/interface/units/HeadIKUnit.h>

#include <mutex>


namespace armarx
{
    /**
     * \class HeadIKUnitPropertyDefinitions
     * \brief
     */
    class HeadIKUnitPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        HeadIKUnitPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("KinematicUnitName", "Name of the KinematicUnit Proxy");
            defineOptionalProperty<std::string>("HeadIKUnitTopicName", "HeadIKUnitTopic",  "Name of the HeadIKUnit Topic");
            defineOptionalProperty<std::string>("RobotStateComponentName", "RobotStateComponent", "Name of the RobotStateComponent that should be used");
            defineOptionalProperty<bool>("VisualizeIKTarget", true, "Visualize the current IK target using the debug drawer");
            defineOptionalProperty<int>("CycleTime", 30, "Cycle time of the tcp control in ms");
        }
    };

    /**
     * \defgroup Component-HeadIKUnit HeadIKUnit
     * \ingroup RobotAPI-SensorActorUnits
     * \brief Unit for controlling a robot head via IK targets.
     */

    /**
     * @ingroup Component-HeadIKUnit
     * @brief The HeadIKUnit class
     */
    class HeadIKUnit : virtual public Component, virtual public HeadIKUnitInterface
    {
    public:
        HeadIKUnit();
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;
        std::string getDefaultName() const override
        {
            return "HeadIKUnit";
        }

        // HeadIKUnitInterface interface
        void setCycleTime(Ice::Int milliseconds, const Ice::Current& c = Ice::emptyCurrent) override;
        void setHeadTarget(const std::string& robotNodeSetName, const FramedPositionBasePtr& targetPosition, const Ice::Current& c = Ice::emptyCurrent) override;

        // UnitExecutionManagementInterface interface
        void init(const Ice::Current& c = Ice::emptyCurrent) override;
        void start(const Ice::Current& c = Ice::emptyCurrent) override;
        void stop(const Ice::Current& c = Ice::emptyCurrent) override;
        UnitExecutionState getExecutionState(const Ice::Current& c = Ice::emptyCurrent) override;

        // UnitResourceManagementInterface interface
        void request(const Ice::Current& c = Ice::emptyCurrent) override;
        void release(const Ice::Current& c = Ice::emptyCurrent) override;

        // PropertyUser interface
        PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        void periodicExec();

        std::mutex accessMutex;
        bool requested;
        int cycleTime;
        PeriodicTask<HeadIKUnit>::pointer_type execTask;

        RobotStateComponentInterfacePrx robotStateComponentPrx;
        KinematicUnitInterfacePrx kinematicUnitPrx;
        //SharedRobotInterfacePrx remoteRobotPrx;
        VirtualRobot::RobotPtr localRobot;
        DebugDrawerInterfacePrx drawer;

        Ice::StringSeq robotNodeSetNames;
        FramedPositionPtr targetPosition;
        bool newTargetSet;

        armarx::HeadIKUnitListenerPrx headIKUnitListener;
    };

}


