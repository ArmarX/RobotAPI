/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     David Schiebener <schiebener at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/GamepadUnit.h>
#include <ArmarXCore/observers/Observer.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/Pose.h>

#include <mutex>

namespace armarx
{
    /**
     * \class GamepadUnitObserverPropertyDefinitions
     * \brief
     */
    class GamepadUnitObserverPropertyDefinitions:
        public ObserverPropertyDefinitions
    {
    public:
        GamepadUnitObserverPropertyDefinitions(std::string prefix):
            ObserverPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("GamepadTopicName", "GamepadValues", "Name of the Gamepad Topic");
            defineOptionalProperty<std::string>("DebugDrawerTopic", "DebugDrawerUpdates", "Name of the DebugDrawerTopic");
        }
    };


    /**
     * \class GamepadUnitObserver
     * \ingroup RobotAPI-SensorActorUnits-observers
     * \brief Observer monitoring @IMU sensor values
     *
     * The GamepadUnitObserver monitors @IMU sensor values published by GamepadUnit-implementations and offers condition checks on these values.
     * Available condition checks are: *updated*, *larger*, *equals* and *smaller*.
     */
    class GamepadUnitObserver :
        virtual public Observer,
        virtual public GamepadUnitObserverInterface
    {
    public:
        GamepadUnitObserver() {}

        std::string getDefaultName() const override
        {
            return "GamepadUnitObserver";
        }
        void onInitObserver() override;
        void onConnectObserver() override;
        void onExitObserver() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        std::mutex dataMutex;
        DebugDrawerInterfacePrx debugDrawerPrx;


        // GamepadUnitListener interface
    public:
        void reportGamepadState(const std::string& device, const std::string& name, const GamepadData& data, const TimestampBasePtr& timestamp, const Ice::Current& c) override;
    };
}

