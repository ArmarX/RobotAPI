/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Christian Boege (boege dot at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "PlatformUnit.h"

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <RobotAPI/libraries/core/PIDController.h>
#include <RobotAPI/interface/core/RobotState.h>

#include <IceUtil/Time.h>

#include <string>

namespace armarx
{
    /**
     * \class PlatformUnitSimulation
     * \brief Simulates a robot platform.
     * \ingroup RobotAPI-SensorActorUnits-simulation
     */
    class PlatformUnitSimulation :
        virtual public PlatformUnit
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "PlatformUnitSimulation";
        }

        void onInitPlatformUnit() override;
        void onStartPlatformUnit() override;
        void onStopPlatformUnit() override;
        void onExitPlatformUnit() override;

        void simulationFunction();

        // proxy implementation
        void moveTo(Ice::Float targetPlatformPositionX, Ice::Float targetPlatformPositionY, Ice::Float targetPlatformRotation, Ice::Float positionalAccuracy, Ice::Float orientationalAccuracy, const Ice::Current& c = Ice::emptyCurrent) override;

        /**
         * \warning Not yet implemented!
         */
        void move(float targetPlatformVelocityX, float targetPlatformVelocityY, float targetPlatformVelocityRotation, const Ice::Current& c = Ice::emptyCurrent) override;

        void moveRelative(float targetPlatformOffsetX, float targetPlatformOffsetY, float targetPlatformOffsetRotation, float positionalAccuracy, float orientationalAccuracy, const Ice::Current& c = Ice::emptyCurrent) override;
        void setMaxVelocities(float positionalVelocity, float orientaionalVelocity, const Ice::Current& c = Ice::emptyCurrent) override;
        void stopPlatform(const Ice::Current& c = Ice::emptyCurrent) override;
        /**
         * \see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        std::mutex currentPoseMutex;
        IceUtil::Time lastExecutionTime;
        int intervalMs;

        enum PlatformMode
        {
            eUndefined,
            ePositionControl,
            eVelocityControl
        }
        platformMode;

        ::Ice::Float targetPositionX;
        ::Ice::Float targetPositionY;
        ::Ice::Float currentPositionX;
        ::Ice::Float currentPositionY;
        ::Ice::Float targetRotation;
        ::Ice::Float currentRotation;

        ::Ice::Float linearVelocityX;
        ::Ice::Float linearVelocityY;
        ::Ice::Float maxLinearVelocity;
        ::Ice::Float angularVelocity;
        ::Ice::Float maxAngularVelocity;

        ::Ice::Float positionalAccuracy;
        ::Ice::Float orientationalAccuracy;

        std::string referenceFrame;
        MultiDimPIDControllerPtr velPID, posPID;
        Eigen::Vector2f currentTranslationVelocity = Eigen::Vector2f::Zero();

        PeriodicTask<PlatformUnitSimulation>::pointer_type simulationTask;


    private:
        Eigen::Matrix4f currentPlatformPose() const;

        RobotStateComponentInterfacePrx robotStateComponent;

        std::string agentName;
        std::string robotRootFrame;


    };
}

