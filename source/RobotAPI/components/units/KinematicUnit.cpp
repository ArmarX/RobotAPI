/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Christian Boege (boege at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "KinematicUnit.h"

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/VirtualRobotException.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

namespace armarx
{
    void KinematicUnit::onInitComponent()
    {
        // read names of kinematic chain elements belonging to this unit from XML and setup a map of all joints
        // the kinematic chain elements belonging to this unit are defined in a RobotNodeSet
        robotNodeSetName = getProperty<std::string>("RobotNodeSetName").getValue();

        const std::string project = getProperty<std::string>("RobotFileNameProject").getValue();
        {
            std::vector<std::string> result;
            const auto& packages = armarx::Application::GetProjectDependencies();
            std::set<std::string> packageSet {packages.begin(), packages.end()};
            packageSet.emplace(Application::GetProjectName());
            packageSet.emplace(project);
            packageSet.erase("");
            armarXPackages.assign(packageSet.begin(), packageSet.end());
        }

        if (relativeRobotFile.empty())
        {
            relativeRobotFile = getProperty<std::string>("RobotFileName").getValue();
        }

        if (!robot)
        {
            Ice::StringSeq includePaths;

            if (!project.empty())
            {
                CMakePackageFinder finder(project);

                auto pathsString = finder.getDataDir();
                Ice::StringSeq projectIncludePaths = simox::alg::split(pathsString, ";,");
                includePaths.insert(includePaths.end(), projectIncludePaths.begin(), projectIncludePaths.end());
            }
            std::string robotFile = relativeRobotFile;
            if (!ArmarXDataPath::getAbsolutePath(robotFile, robotFile, includePaths))
            {
                throw UserException("Could not find robot file " + robotFile);
            }

            // read the robot
            try
            {
                robot = VirtualRobot::RobotIO::loadRobot(robotFile, VirtualRobot::RobotIO::eStructure);
            }
            catch (VirtualRobot::VirtualRobotException& e)
            {
                throw UserException(e.what());
            }
        }
        // read the robot node set and initialize the joints of this kinematic unit
        if (robotNodeSetName == "")
        {
            throw UserException("RobotNodeSet not defined");
        }

        VirtualRobot::RobotNodeSetPtr robotNodeSetPtr = robot->getRobotNodeSet(robotNodeSetName);
        robotNodes = robotNodeSetPtr->getAllRobotNodes();

        // component dependencies
        listenerName = getProperty<std::string>("TopicPrefix").getValue() + robotNodeSetName + "State";
        offeringTopic(listenerName);

        offeringTopicFromProperty("SkillProviderTopic");

        this->onInitKinematicUnit();
    }


    void KinematicUnit::onConnectComponent()
    {
        ARMARX_INFO << "setting report topic to " << listenerName << flush;
        listenerPrx = getTopic<KinematicUnitListenerPrx>(listenerName);

        this->onStartKinematicUnit();
    }


    void KinematicUnit::onExitComponent()
    {
        this->onExitKinematicUnit();
    }


    void KinematicUnit::switchControlMode(const NameControlModeMap& targetJointModes, const Ice::Current& c)
    {
    }

    void KinematicUnit::setJointAngles(const NameValueMap& targetJointAngles, const Ice::Current& c)
    {
    }

    void KinematicUnit::setJointVelocities(const NameValueMap& targetJointVelocities, const Ice::Current& c)
    {
    }

    void KinematicUnit::setJointAnglesToZero()
    {
        NameControlModeMap c;
        NameValueMap       t;
        for (const auto& j : getJoints())
        {
            c[j] = ControlMode::ePositionControl;
            t[j] = 0;
        }
        switchControlMode(c);
        setJointAngles(t);
    }
    void KinematicUnit::setJointVelocitiesToZero()
    {
        NameControlModeMap c;
        NameValueMap       t;
        for (const auto& j : getJoints())
        {
            c[j] = ControlMode::eVelocityControl;
            t[j] = 0;
        }
        switchControlMode(c);
        setJointVelocities(t);
    }

    void KinematicUnit::requestKinematicUnit(const Ice::StringSeq& nodes, const Ice::Current& c)
    {
        SensorActorUnit::request(c);
    }

    void KinematicUnit::releaseKinematicUnit(const Ice::StringSeq& nodes, const Ice::Current& c)
    {
        SensorActorUnit::release(c);
    }


    std::string KinematicUnit::getRobotFilename(const Ice::Current&) const
    {
        return relativeRobotFile;
    }

    std::vector<std::string> KinematicUnit::getArmarXPackages(const Ice::Current&) const
    {
        return armarXPackages;
    }

    std::string KinematicUnit::getRobotName(const Ice::Current&) const
    {
        if (robot)
        {
            return robot->getName();
        }
        else
        {
            throw NotInitializedException("Robot Ptr is NULL", "getName");
        }
    }

    std::string KinematicUnit::getRobotNodeSetName(const Ice::Current&) const
    {
        if (robotNodeSetName.empty())
        {
            throw NotInitializedException("RobotNodeSetName is empty", "getRobotNodeSetName");
        }
        return robotNodeSetName;
    }

    std::string KinematicUnit::getReportTopicName(const Ice::Current&) const
    {
        return listenerName;
    }

    PropertyDefinitionsPtr KinematicUnit::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new KinematicUnitPropertyDefinitions(getConfigIdentifier()));
    }
}
