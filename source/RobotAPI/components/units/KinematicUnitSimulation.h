/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Christian Boege (boege dot at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "KinematicUnit.h"

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <RobotAPI/libraries/core/PIDController.h>

#include <IceUtil/Time.h>

#include <random>
#include <mutex>

namespace armarx
{
    /**
     * \class KinematicUnitSimulationJointState.
     * \brief State of a joint.
     * \ingroup RobotAPI-SensorActorUnits-util
     *
     * Includes the control mode, the joint angle, velocity, torque, and motor temperature.
     * Controlmode defaults to ePositionControl, all other values default to zero.
     */
    class KinematicUnitSimulationJointState
    {
    public:
        KinematicUnitSimulationJointState()
        {
            init();
        }

        void init()
        {
            controlMode = eDisabled;
            angle = 0.0f;
            velocity = 0.0f;
            velocityActual = 0.0f;
            torque = 0.0f;
            temperature = 0.0f;
        }

        ControlMode controlMode;
        float angle;
        float velocity;
        float velocityActual;
        float torque;
        float temperature;
    };
    using KinematicUnitSimulationJointStates = std::map<std::string, KinematicUnitSimulationJointState>;

    /**
     * \class JointInfo.
     * \brief Additional information about a joint.
     * \ingroup RobotAPI-SensorActorUnits-util
     *
     * Joint info including lower and upper joint limits (rad).
     * No limits are set by default (lo = hi).
     */
    class KinematicUnitSimulationJointInfo
    {
    public:
        KinematicUnitSimulationJointInfo()
        {
            reset();
        }

        void reset()
        {
            limitLo = 0.0f;
            limitHi = 0.0f;
        }
        bool hasLimits() const
        {
            return (limitLo != limitHi);
        }
        bool continuousJoint() const
        {
            return !hasLimits() || limitLo - limitHi >= 360.0f;
        }

        float limitLo;
        float limitHi;
    };
    using KinematicUnitSimulationJointInfos = std::map<std::string, KinematicUnitSimulationJointInfo>;

    /**
     * \class KinematicUnitSimulationPropertyDefinitions
     * \brief
     */
    class KinematicUnitSimulationPropertyDefinitions : public KinematicUnitPropertyDefinitions
    {
    public:
        KinematicUnitSimulationPropertyDefinitions(std::string prefix);
    };

    /**
     * \class KinematicUnitSimulation
     * \brief Simulates robot kinematics
     * \ingroup RobotAPI-SensorActorUnits-simulation
     */
    class KinematicUnitSimulation :
        virtual public KinematicUnit
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "KinematicUnitSimulation";
        }

        void onInitKinematicUnit() override;
        void onStartKinematicUnit() override;
        void onExitKinematicUnit() override;

        void simulationFunction();

        // proxy implementation
        void requestJoints(const Ice::StringSeq& joints, const Ice::Current& c = Ice::emptyCurrent) override;
        void releaseJoints(const Ice::StringSeq& joints, const Ice::Current& c = Ice::emptyCurrent) override;
        void switchControlMode(const NameControlModeMap& targetJointModes, const Ice::Current& c = Ice::emptyCurrent) override;
        void setJointAngles(const NameValueMap& targetJointAngles, const Ice::Current& c = Ice::emptyCurrent) override;
        void setJointVelocities(const NameValueMap& targetJointVelocities, const Ice::Current& c = Ice::emptyCurrent) override;
        void setJointTorques(const NameValueMap& targetJointTorques, const Ice::Current& c = Ice::emptyCurrent) override;

        NameControlModeMap getControlModes(const Ice::Current& c = Ice::emptyCurrent) override;

        /// @warning Not implemented yet!
        void setJointAccelerations(const NameValueMap& targetJointAccelerations, const Ice::Current& c = Ice::emptyCurrent) override;

        /// @warning Not implemented yet!
        void setJointDecelerations(const NameValueMap& targetJointDecelerations, const Ice::Current& c = Ice::emptyCurrent) override;

        void stop(const Ice::Current& c = Ice::emptyCurrent) override;

        armarx::NameValueMap getJointAngles(const Ice::Current& c) const override;
        armarx::NameValueMap getJointVelocities(const Ice::Current& c) const override;
        Ice::StringSeq getJoints(const Ice::Current& c) const override;

        DebugInfo getDebugInfo(const Ice::Current& c = Ice::emptyCurrent) const override;

        /// @see PropertyUser::createPropertyDefinitions()
        PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        KinematicUnitSimulationJointStates jointStates;
        KinematicUnitSimulationJointStates previousJointStates;
        KinematicUnitSimulationJointInfos jointInfos;
        mutable std::mutex jointStatesMutex;
        IceUtil::Time lastExecutionTime;
        float noise;
        int intervalMs;
        std::normal_distribution<double> distribution;
        std::default_random_engine generator;
        bool usePDControllerForPosMode;
        std::map<std::string, PIDControllerPtr> positionControlPIDController;
        PeriodicTask<KinematicUnitSimulation>::pointer_type simulationTask;

        mutable std::mutex sensorDataMutex;
        NameValueMap sensorAngles;
        NameValueMap sensorVelocities;
    };
}
