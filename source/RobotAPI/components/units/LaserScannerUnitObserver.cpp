/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "LaserScannerUnitObserver.h"


#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>
#include <ArmarXCore/observers/checks/ConditionCheckUpdated.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>

#include <RobotAPI/libraries/core/Pose.h>

#include <cfloat>

namespace armarx
{
    void LaserScannerUnitObserver::onInitObserver()
    {
        usingTopic(getProperty<std::string>("LaserScannerTopicName").getValue());

        offerConditionCheck("updated", new ConditionCheckUpdated());
        offerConditionCheck("larger", new ConditionCheckLarger());
        offerConditionCheck("equals", new ConditionCheckEquals());
        offerConditionCheck("smaller", new ConditionCheckSmaller());
    }



    void LaserScannerUnitObserver::onConnectObserver()
    {
    }


    void LaserScannerUnitObserver::onExitObserver()
    {
    }


    PropertyDefinitionsPtr LaserScannerUnitObserver::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new LaserScannerUnitObserverPropertyDefinitions(getConfigIdentifier()));
    }

    void LaserScannerUnitObserver::reportSensorValues(const std::string& device, const std::string& name, const LaserScan& scan, const TimestampBasePtr& timestamp, const Ice::Current& c)
    {
        std::unique_lock lock(dataMutex);

        if (!existsChannel(device))
        {
            offerChannel(device, "laser scans");
        }

        TimestampVariantPtr timestampPtr = TimestampVariantPtr::dynamicCast(timestamp);
        offerOrUpdateDataField(device, "timestamp", timestampPtr, "Timestamp");

        // Calculate some statistics on the laser scan
        float minDistance = FLT_MAX;
        float minAngle = 0.0f;
        float maxDistance = -FLT_MAX;
        float maxAngle = 0.0f;
        float distanceSum = 0.0f;
        for (LaserScanStep const& step : scan)
        {
            distanceSum += step.distance;
            if (step.distance < minDistance)
            {
                minDistance = step.distance;
                minAngle = step.angle;
            }
            if (step.distance > maxDistance)
            {
                maxDistance = step.distance;
                maxAngle = step.angle;
            }
        }

        if (scan.size() > 0)
        {
            offerOrUpdateDataField(device, "minDistance", minDistance, "minimal distance in scan");
            offerOrUpdateDataField(device, "minAngle", minAngle, "angle with minimal distance in scan");
            offerOrUpdateDataField(device, "maxDistance", maxDistance, "maximal distance in scan");
            offerOrUpdateDataField(device, "maxAngle", maxAngle, "angle with maximal distance in scan");
            float averageDistance = distanceSum / scan.size();
            offerOrUpdateDataField(device, "averageDistance", averageDistance, "average distance in scan");
        }

        updateChannel(device);
    }
}

