/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Nikolaus Vahrenkamp (vahrenkamp at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/SensorActorUnit.h>

#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <RobotAPI/interface/units/RobotPoseUnitInterface.h>

#include <vector>

namespace armarx
{
    /**
     * \class RobotPoseUnitPropertyDefinitions
     * \brief Defines all necessary properties for armarx::RobotPoseUnit
     */
    class RobotPoseUnitPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        RobotPoseUnitPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("RobotName", "Robot", "Name of the Robot (will publish values on RobotName + '6DPoseState')");
        }
    };


    /**
     * \defgroup Component-RobotPoseUnit RobotPoseUnit
     * \ingroup RobotAPI-SensorActorUnits
     * \brief Base unit for high-level access to robot poses.
     *
     * This class defines an interface for providing high level access to the robot's pose in the world.
     * This unit usually does not exiust on a real robot (instead, use the @see PlatformUnit for moving a platform based robot around).
     * In simulated worlds, this unit allows to position the robot globally in the envirnoment (@see RobotPoseUnitDynamicSimulation).
     * It uses the RobotPoseUnitListener Ice interface to report updates of its current state.
     */

    /**
     * @ingroup Component-RobotPoseUnit
     * @brief The RobotPoseUnit class
     */
    class RobotPoseUnit :
        virtual public RobotPoseUnitInterface,
        virtual public SensorActorUnit
    {
    public:
        // inherited from Component
        std::string getDefaultName() const override
        {
            return "RobotPoseUnit";
        }

        /**
         * Retrieve proxy for publishing State information and call
         * armarx::RobotPoseUnit::onInitRobotPoseUnit().
         * \see armarx::Component::onInitComponent()
         */
        void onInitComponent() override;
        /**
         * Calls armarx::RobotPoseUnit::onStartRobotPoseUnit().
         * \see armarx::Component::onConnectComponent()
         */
        void onConnectComponent() override;

        void onDisconnectComponent() override;
        /**
         * Calls armarx::RobotPoseUnit::onExitRobotPoseUnit().
         * \see armarx::Component::onExitComponent()
         */
        void onExitComponent() override;

        virtual void onInitRobotPoseUnit() = 0;
        virtual void onStartRobotPoseUnit() = 0;
        virtual void onStopRobotPoseUnit() {}
        virtual void onExitRobotPoseUnit() = 0;

        /**
        * moveTo moves the robot to a global pose specified by:
        * @param targetPose Global target pose
        * @param postionalAccuracy Robot stops translating if distance to target position gets lower than this threshhold.
        * @param orientationalAccuracy Robot stops rotating if distance from current to target orientation gets lower than this threshhold.
        **/
        virtual void moveTo(PoseBasePtr targetPose, Ice::Float positionalAccuracy, Ice::Float orientationalAccuracy, const Ice::Current& c = Ice::emptyCurrent);

        void stopMovement(const Ice::Current& c = Ice::emptyCurrent) override {}
        /**
         * \see armarx::PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:
        /**
         * RobotPoseUnitListener proxy for publishing state updates
         */
        RobotPoseUnitListenerPrx listenerPrx;
        /**
         * Ice Topic name on which armarx::RobotPoseUnit::listenerPrx publishes information
         */
        std::string listenerChannelName;
    };
}

