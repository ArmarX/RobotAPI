/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::CyberGloveObserver
 * @author     JuliaStarke ( julia dot starke at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CyberGloveObserver.h"
#include <ArmarXCore/observers/variant/TimestampVariant.h>

namespace armarx
{
    void CyberGloveObserver::onInitObserver()
    {
        usingTopic(getProperty<std::string>("CyberGloveTopicName").getValue());
    }


    void CyberGloveObserver::onConnectObserver()
    {
        lastUpdate = TimeUtil::GetTime();
    }


    //void CyberGloveObserver::onDisconnectComponent()
    //{

    //}


    void CyberGloveObserver::onExitObserver()
    {

    }

    PropertyDefinitionsPtr CyberGloveObserver::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new CyberGloveObserverPropertyDefinitions(
                                          getConfigIdentifier()));
    }

    void CyberGloveObserver::reportGloveValues(const CyberGloveValues& gloveValues, const Ice::Current&)
    {
        std::unique_lock lock(dataMutex);

        IceUtil::Time now = TimeUtil::GetTime();

        long deltaUS = (now - lastUpdate).toMicroSeconds();

        ARMARX_IMPORTANT << deltaUS << " " << gloveValues.time << " " << gloveValues.indexDIP;
        lastUpdate = now;

        latestValues = gloveValues;


        std::string name = gloveValues.name;
        if (!existsChannel(name))
        {
            offerChannel(name, "CyberGlove motor data");
        }

        offerOrUpdateDataField(name, "name", Variant(name), "Name of the prostesis");

        offerOrUpdateDataField(name, "thumbCMC", Variant(gloveValues.thumbCMC), "Value of thumbCMC");
        offerOrUpdateDataField(name, "thumbMCP", Variant(gloveValues.thumbMCP), "Value of thumbMCP");
        offerOrUpdateDataField(name, "thumbIP", Variant(gloveValues.thumbIP), "Value of thumbIP");
        offerOrUpdateDataField(name, "thumbAbd", Variant(gloveValues.thumbAbd), "Value of thumbAbd");
        offerOrUpdateDataField(name, "indexMCP", Variant(gloveValues.indexMCP), "Value of indexMCP");
        offerOrUpdateDataField(name, "indexPIP", Variant(gloveValues.indexPIP), "Value of indexPIP");
        offerOrUpdateDataField(name, "indexDIP", Variant(gloveValues.indexDIP), "Value of indexDIP");
        offerOrUpdateDataField(name, "middleMCP", Variant(gloveValues.middleMCP), "Value of middleMCP");
        offerOrUpdateDataField(name, "middlePIP", Variant(gloveValues.middlePIP), "Value of middlePIP");
        offerOrUpdateDataField(name, "middleDIP", Variant(gloveValues.middleDIP), "Value of middleDIP");
        offerOrUpdateDataField(name, "middleAbd", Variant(gloveValues.middleAbd), "Value of middleAbd");
        offerOrUpdateDataField(name, "ringMCP", Variant(gloveValues.ringMCP), "Value of ringMCP");
        offerOrUpdateDataField(name, "ringPIP", Variant(gloveValues.ringPIP), "Value of ringPIP");
        offerOrUpdateDataField(name, "ringDIP", Variant(gloveValues.ringDIP), "Value of ringDIP");
        offerOrUpdateDataField(name, "ringAbd", Variant(gloveValues.ringAbd), "Value of ringAbd");
        offerOrUpdateDataField(name, "littleMCP", Variant(gloveValues.littleMCP), "Value of littleMCP");
        offerOrUpdateDataField(name, "littlePIP", Variant(gloveValues.littlePIP), "Value of littlePIP");
        offerOrUpdateDataField(name, "littleDIP", Variant(gloveValues.littleDIP), "Value of littleDIP");
        offerOrUpdateDataField(name, "littleAbd", Variant(gloveValues.littleAbd), "Value of littleAbd");
        offerOrUpdateDataField(name, "palmArch", Variant(gloveValues.palmArch), "Value of palmArch");
        offerOrUpdateDataField(name, "wristFlex", Variant(gloveValues.wristFlex), "Value of wristFlex");
        offerOrUpdateDataField(name, "wristAbd", Variant(gloveValues.wristAbd), "Value of wristAbd");


        updateChannel(name);
    }



    CyberGloveValues armarx::CyberGloveObserver::getLatestValues(const Ice::Current&)
    {
        std::unique_lock lock(dataMutex);
        return latestValues;
    }
}
