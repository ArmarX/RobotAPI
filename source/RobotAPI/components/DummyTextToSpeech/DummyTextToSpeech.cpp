/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::DummyTextToSpeech
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DummyTextToSpeech.h"

#include <ArmarXCore/core/time/TimeUtil.h>

namespace armarx
{
    void DummyTextToSpeech::onInitComponent()
    {
        usingTopic(getProperty<std::string>("TextToSpeechTopicName").getValue());
        offeringTopic(getProperty<std::string>("TextToSpeechStateTopicName").getValue());
    }


    void DummyTextToSpeech::onConnectComponent()
    {
        textToSpeechStateTopicPrx = getTopic<TextToSpeechStateInterfacePrx>(getProperty<std::string>("TextToSpeechStateTopicName").getValue());
        textToSpeechStateTopicPrx->reportState(eIdle);
    }


    void DummyTextToSpeech::onDisconnectComponent()
    {

    }


    void DummyTextToSpeech::onExitComponent()
    {

    }

    armarx::PropertyDefinitionsPtr DummyTextToSpeech::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new DummyTextToSpeechPropertyDefinitions(
                getConfigIdentifier()));
    }



    void armarx::DummyTextToSpeech::reportText(const std::string& text, const Ice::Current&)
    {
        //ARMARX_IMPORTANT << "reportState";
        textToSpeechStateTopicPrx->reportState(eStartedSpeaking);

        ARMARX_IMPORTANT << "DummyTTS StartedSpeaking: " << text;

        //ARMARX_IMPORTANT << "sleep";
        sleep(1);
        TimeUtil::SleepMS(text.length() * 10);
        //ARMARX_IMPORTANT << "reportState";
        ARMARX_IMPORTANT << "DummyTTS FinishedSpeaking";
        textToSpeechStateTopicPrx->reportState(eFinishedSpeaking);
    }

    void armarx::DummyTextToSpeech::reportTextWithParams(const std::string& text, const Ice::StringSeq& params, const Ice::Current&)
    {
        ARMARX_WARNING << "reportTextWithParams is not implemented";
    }
}
