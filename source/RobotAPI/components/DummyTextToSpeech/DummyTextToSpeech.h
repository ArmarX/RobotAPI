/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::DummyTextToSpeech
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <RobotAPI/interface/speech/SpeechInterface.h>

namespace armarx
{
    /**
     * @class DummyTextToSpeechPropertyDefinitions
     * @brief
     */
    class DummyTextToSpeechPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        DummyTextToSpeechPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            defineOptionalProperty<std::string>("TextToSpeechTopicName", "TextToSpeech", "Name of the TextToSpeechTopic");
            defineOptionalProperty<std::string>("TextToSpeechStateTopicName", "TextToSpeechState", "Name of the TextToSpeechStateTopic");
        }
    };

    /**
     * @defgroup Component-DummyTextToSpeech DummyTextToSpeech
     * @ingroup RobotAPI-Components
     * A description of the component DummyTextToSpeech.
     *
     * @class DummyTextToSpeech
     * @ingroup Component-DummyTextToSpeech
     * @brief Brief description of class DummyTextToSpeech.
     *
     * Detailed description of class DummyTextToSpeech.
     */
    class DummyTextToSpeech :
        virtual public armarx::Component,
        virtual public TextListenerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "DummyTextToSpeech";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // TextListenerInterface interface
    public:
        void reportText(const std::string& text, const Ice::Current&) override;
        void reportTextWithParams(const std::string& text, const Ice::StringSeq& params, const Ice::Current&) override;

    private:
        TextToSpeechStateInterfacePrx textToSpeechStateTopicPrx;
    };
}
