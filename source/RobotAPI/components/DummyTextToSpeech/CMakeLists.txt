armarx_component_set_name("DummyTextToSpeech")
set(COMPONENT_LIBS ArmarXCore RobotAPIInterfaces)
set(SOURCES DummyTextToSpeech.cpp)
set(HEADERS DummyTextToSpeech.h)
armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)

armarx_generate_and_add_component_executable(APPLICATION_APP_SUFFIX)
