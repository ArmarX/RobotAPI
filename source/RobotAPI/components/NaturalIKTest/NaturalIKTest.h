/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::NaturalIKTest
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/RemoteGuiComponentPlugin.h>
//#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>
#include <Eigen/Dense>

#include <RobotAPI/libraries/diffik/NaturalDiffIK.h>

#include <RobotAPI/libraries/natik/NaturalIK.h>


namespace armarx
{
    /**
     * @class NaturalIKTestPropertyDefinitions
     * @brief
     */
    class NaturalIKTestPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        NaturalIKTestPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-NaturalIKTest NaturalIKTest
     * @ingroup RobotAPI-Components
     * A description of the component NaturalIKTest.
     *
     * @class NaturalIKTest
     * @ingroup Component-NaturalIKTest
     * @brief Brief description of class NaturalIKTest.
     *
     * Detailed description of class NaturalIKTest.
     */
    class NaturalIKTest :
        virtual public armarx::Component,
        virtual public RemoteGuiComponentPluginUser
    {
    public:

        struct GuiSideParams
        {
            Eigen::Vector3f target;
            Eigen::Vector3f targetRotation;
            float scale = 1.3f;
            float minElbZ;
            std::atomic_bool setOri;
            NaturalIK::Parameters ikparams;
        };

        struct GuiParams
        {
            GuiSideParams p_R;
            GuiSideParams p_L;
            std::atomic_bool targetValid = false;
            Eigen::Vector3f err_R = Eigen::Vector3f::Zero(), err_L = Eigen::Vector3f::Zero();

            std::atomic_bool useCompositeIK;

        };

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;


        RemoteGui::WidgetPtr buildGui();
        void processGui(RemoteGui::TabProxy& prx);
    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        virtual void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        virtual void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        virtual void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        virtual void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void vizTaskRun();
        void testTaskRun();
        Eigen::Matrix4f mirrorPose(Eigen::Matrix4f oldPose);
        Eigen::Quaternionf mirrorOri(Eigen::Quaternionf oriOld);


    private:

        // Private methods and member variables go here.

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;
        /// Debug drawer. Used for 3D visualization.
        //armarx::DebugDrawerTopic debugDrawer;

        RunningTask<NaturalIKTest>::pointer_type vizTask;
        RunningTask<NaturalIKTest>::pointer_type runTestTask;
        GuiParams p;
    };
}
