/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::GamepadControlUnit
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/components/EmergencyStopInterface.h>

#include <RobotAPI/interface/units/GamepadUnit.h>

#include <RobotAPI/interface/units/HandUnitInterface.h>

#include <RobotAPI/interface/components/RobotHealthInterface.h>

namespace armarx
{
    /**
     * @class GamepadControlUnitPropertyDefinitions
     * @brief
     */
    class GamepadControlUnitPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        GamepadControlUnitPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
            defineOptionalProperty<std::string>("PlatformUnitName", "Armar6IcePlatformUnit", "Name of the platform unit to use");
            defineOptionalProperty<std::string>("GamepadTopicName", "GamepadValues", "Name of the Gamepad Topic");
            defineOptionalProperty<float>("ScaleX", 1000.0f, "scaling factor in mm per second");
            defineOptionalProperty<float>("ScaleY", 1000.0f, "scaling factor in mm per second");
            defineOptionalProperty<float>("ScaleAngle", 1.0f, "scaling factor in radian per second");
        }
    };

    /**
     * @defgroup Component-GamepadControlUnit GamepadControlUnit
     * @ingroup RobotAPI-Components
     * A description of the component GamepadControlUnit.
     *
     * @class GamepadControlUnit
     * @ingroup Component-GamepadControlUnit
     * @brief Brief description of class GamepadControlUnit.
     *
     * Detailed description of class GamepadControlUnit.
     */
    class GamepadControlUnit :
        virtual public armarx::Component,
        virtual public GamepadUnitListener
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "GamepadControlUnit";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        PlatformUnitInterfacePrx platformUnitPrx;



	bool enableHeartBeat = false;
        RobotHealthInterfacePrx robotHealthTopic;
        float scaleX;
        float scaleY;
        float scaleRotation;
        EmergencyStopMasterInterfacePrx emergencyStop;

	bool leftHandOpen = true;
	bool rightHandOpen = true;

	long leftHandTime = 0;
	long rightHandTime = 0;

    public:
        void reportGamepadState(const std::string& device, const std::string& name, const GamepadData& data,
                                const TimestampBasePtr& timestamp, const Ice::Current& c) override;

    };
}

