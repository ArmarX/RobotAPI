/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArticulatedObjectLocalizerExample.h"

#include <memory>

#include <Eigen/Geometry>

#include <IceUtil/Time.h>

#include <SimoxUtility/math/pose/pose.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/XML/RobotIO.h>

#include <ArmarXCore/core/PackagePath.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/time/CycleUtil.h>

#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/client/query/query_fns.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem/core/wm/ice_conversions.h>
#include <RobotAPI/libraries/armem/server/MemoryRemoteGui.h>
#include <RobotAPI/libraries/armem_objects/types.h>

namespace armarx::articulated_object
{
    ArticulatedObjectLocalizerExample::ArticulatedObjectLocalizerExample() :
        articulatedObjectWriter(
            new ::armarx::armem::articulated_object::ArticulatedObjectWriter(memoryNameSystem())),
        articulatedObjectReader(
            new ::armarx::armem::articulated_object::ArticulatedObjectReader(memoryNameSystem()))
    {
    }

    armarx::PropertyDefinitionsPtr
    ArticulatedObjectLocalizerExample::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs =
            new ComponentPropertyDefinitions(getConfigIdentifier());

        defs->topic(debugObserver);

        defs->optional(p.updateFrequency, "updateFrequency", "Memory update frequency (write).");

        defs->optional(p.obj.dataset, "p.obj.dataset", "");
        defs->optional(p.obj.className, "p.obj.class", "");

        // Reader will override some properties of writer.
        articulatedObjectWriter->registerPropertyDefinitions(defs);
        articulatedObjectReader->registerPropertyDefinitions(defs);

        return defs;
    }

    std::string
    ArticulatedObjectLocalizerExample::getDefaultName() const
    {
        return "ArticulatedObjectLocalizerExample";
    }

    void
    ArticulatedObjectLocalizerExample::onInitComponent()
    {
        // Reader overwrote property registered property of articulatedObjectWriter.
        articulatedObjectWriter->setProviderName(articulatedObjectReader->getProviderName());
    }

    void
    ArticulatedObjectLocalizerExample::onConnectComponent()
    {
        articulatedObjectWriter->connect();
        articulatedObjectReader->connect();

        ARMARX_IMPORTANT << "Running example.";
        start = armem::Time::Now();

        task = new PeriodicTask<ArticulatedObjectLocalizerExample>(
            this,
            &ArticulatedObjectLocalizerExample::run,
            static_cast<int>(1000.f / p.updateFrequency));
        task->start();
    }

    void
    ArticulatedObjectLocalizerExample::onDisconnectComponent()
    {
        task->stop();
    }

    void
    ArticulatedObjectLocalizerExample::onExitComponent()
    {
    }

    VirtualRobot::RobotPtr
    ArticulatedObjectLocalizerExample::createArticulatedObject()
    {
        const std::string dishwasherName = p.obj.dataset + "/" + p.obj.className;

        const auto descriptions = articulatedObjectReader->queryDescriptions(armem::Time::Now());

        ARMARX_INFO << "Found " << descriptions.size() << " articulated object descriptions";

        for(const auto& description: descriptions)
        {
            ARMARX_INFO << "- " << description.name;
        }

        const auto it = std::find_if(
            descriptions.begin(),
            descriptions.end(),
            [&](const armem::articulated_object::ArticulatedObjectDescription& desc) -> bool
            { return desc.name == dishwasherName; });

        if (it == descriptions.end())
        {
            ARMARX_WARNING << "Articulated object " << dishwasherName << " not (yet) available";
            return nullptr;
        }

        auto obj =
            VirtualRobot::RobotIO::loadRobot(ArmarXDataPath::resolvePath(it->xml.serialize().path),
                                             VirtualRobot::RobotIO::eStructure);

        if (not obj)
        {
            return nullptr;
        }

        obj->setName("0"); // aka instance name
        obj->setType(it->name); // aka dataset/class name

        return obj;
    }


    void
    ArticulatedObjectLocalizerExample::run()
    {
        if (articulatedObject == nullptr)
        {
            articulatedObject = createArticulatedObject();

            if (articulatedObject == nullptr) // still
            {
                return;
            }
        }

        ARMARX_DEBUG << "Reporting articulated objects";

        const armem::Time now = armem::Time::Now();
        const float t = static_cast<float>((now - start).toSecondsDouble());

        // move joints at certain frequency
        const float k = (1 + std::sin(t / (M_2_PIf32))) / 2; // in [0,1]

        auto jointValues = articulatedObject->getJointValues();

        for (auto& [name, jointValue] : jointValues)
        {
            const auto node = articulatedObject->getRobotNode(name);
            jointValue = node->unscaleJointValue(k, 0, 1);
        }

        articulatedObject->setGlobalPose(simox::math::pose(Eigen::Vector3f(1000, 0, 0)));
        articulatedObject->setJointValues(jointValues);

        articulatedObjectWriter->storeArticulatedObject(articulatedObject, now);
    }

} // namespace armarx::articulated_object
