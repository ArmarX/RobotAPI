set(LIB_NAME       ArticulatedObjectLocalizerExample)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

# Add the component
armarx_add_component(
    COMPONENT_LIBS
        ArmarXCore 
        armem_objects
    SOURCES
        ArticulatedObjectLocalizerExample.cpp

    HEADERS
        ArticulatedObjectLocalizerExample.h
)

# Add unit tests
add_subdirectory(test)

# Generate the application
armarx_generate_and_add_component_executable(
    # If your component is not defined in ::armarx, specify its namespace here:
    COMPONENT_NAMESPACE "armarx::articulated_object"
)
