
#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/util/tasks.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include "RobotAPI/libraries/ArmarXObjects/ObjectID.h"
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>
#include <RobotAPI/interface/armem/server/MemoryInterface.h>
#include <RobotAPI/libraries/armem/client/plugins.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem_objects/client/articulated_object/ArticulatedObjectReader.h>
#include <RobotAPI/libraries/armem_objects/client/articulated_object/ArticulatedObjectWriter.h>

namespace armarx::articulated_object
{

    /**
     * @defgroup Component-ExampleClient ExampleClient
     * @ingroup RobotAPI-Components
     * A description of the component ExampleClient.
     *
     * @class ExampleClient
     * @ingroup Component-ExampleClient
     * @brief Brief description of class ExampleClient.
     *
     * Detailed description of class ExampleClient.
     */
    class ArticulatedObjectLocalizerExample :
        virtual public armarx::Component,
        virtual public armarx::armem::client::ComponentPluginUser
    {
    public:
        ArticulatedObjectLocalizerExample();

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

    protected:
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        void run();

    private:
        VirtualRobot::RobotPtr createArticulatedObject();
        std::shared_ptr<VirtualRobot::Robot> articulatedObject;

        /// Reference timestamp for object movement
        armem::Time start;

        armarx::PeriodicTask<ArticulatedObjectLocalizerExample>::pointer_type task;

        armarx::DebugObserverInterfacePrx debugObserver;

        std::unique_ptr<::armarx::armem::articulated_object::ArticulatedObjectWriter> articulatedObjectWriter;
        std::unique_ptr<::armarx::armem::articulated_object::ArticulatedObjectReader>
        articulatedObjectReader;

        struct Properties
        {
            float updateFrequency{25.F};

            struct
            {
                std::string dataset = "Kitchen";
                std::string className = "mobile-dishwasher";
            } obj;
        } p;
    };

} // namespace armarx::articulated_object
