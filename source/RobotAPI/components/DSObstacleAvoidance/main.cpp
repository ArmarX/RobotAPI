/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::DSObstacleAvoidance
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// STD/STL
#include <string>

// ArmarX
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>

// armar6_skills
#include <RobotAPI/components/DSObstacleAvoidance/DSObstacleAvoidance.h>


int main(int argc, char* argv[])
{
    using namespace armarx;
    const std::string name = DSObstacleAvoidance::default_name;
    return runSimpleComponentApp<DSObstacleAvoidance>(argc, argv, name);
}
