#pragma once

#include <list>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseProviderPlugin.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>
#include <RobotAPI/components/ObjectMemoryEditor/InteractionObserver.h>

namespace armarx
{

    class Editor;


    struct VisualizationDescription
    {
        Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
        bool allowTransforming = true;

        std::vector<std::string> options {};
        std::optional<float> alpha {};
        std::optional<simox::Color> color {};

        // Context menu indices.
        size_t cloneIndex = std::numeric_limits<size_t>::max();
        size_t deleteIndex = std::numeric_limits<size_t>::max();
        size_t resetIndex = std::numeric_limits<size_t>::max();
        size_t prototypeIndex = std::numeric_limits<size_t>::max();
        size_t commitIndex = std::numeric_limits<size_t>::max();
        size_t updateIndex = std::numeric_limits<size_t>::max();
        size_t resetAllIndex = std::numeric_limits<size_t>::max();
    };



    class ChangeState
    {
    public:
        explicit ChangeState(Editor* editor) : editor(editor) {} ;

        void clear();
        void moveNewObjectsTo(objpose::ObjectPoseSeq& seq);
        bool applyTo(objpose::ObjectPose& pose);
        void visualizeNewObjects();
        VisualizationDescription buildVisualizationDescription(objpose::ObjectPose& object);
        Eigen::Matrix4f getTransform(objpose::ObjectPose const& object);

        void cloneObject(objpose::ObjectPose const& object);
        void createObject(std::string const& objectID, Eigen::Matrix4f const& pose);
        void deleteObject(objpose::ObjectPose const& object);
        void resetObject(objpose::ObjectPose const& object);
        void moveObject(objpose::ObjectPose const& object, Eigen::Matrix4f const& transform);

    private:
        enum ChangeKind
        {
            MOVE,
            CREATE,
            DELETE
        };

        using ObjectPoseList = std::list<objpose::ObjectPose>;

        struct Change
        {
            ChangeKind kind = MOVE;
            Eigen::Matrix4f transform = Eigen::Matrix4f::Identity();
            ObjectPoseList::iterator iterator {};
        };

        Editor* editor;

        ObjectPoseList newPoses;
        std::map<std::string, Change> changed;
    };



    class PlaceholderState
    {
    public:
        explicit PlaceholderState(Editor* editor) : editor(editor) {} ;

        struct Placeholder
        {
            simox::OrientedBoxf box;
            Eigen::Matrix4f transform;
        };

        void addPlaceholder(simox::OrientedBoxf box);
        void visualizePlaceholders();
        void movePlaceholder(size_t id, Eigen::Matrix4f const& transform);
        void removePlaceholder(size_t id);
        void specifyObject(size_t id, std::string const& objectID, ChangeState& changeState);

    private:
        size_t getID();

        Editor* editor;

        std::priority_queue<size_t, std::vector<size_t>, std::greater<>> unusedIDs;
        struct PlaceholderEntry
        {
            Placeholder placeholder;
            bool isActive;
        };
        std::vector<PlaceholderEntry> placeholders;
    };



    class Editor
    {
    public:
        struct Properties
        {
            std::string providerName;

            float const& objectScaling;
            float const& confidenceThreshold;

            std::vector<data::ObjectID> const& availableObjects;
        };


    public:

        explicit Editor(viz::Client& client,
                        Properties properties,
                        std::function<void(objpose::ProvidedObjectPoseSeq&)> pushToMemory,
                        std::function<objpose::ObjectPoseSeq(void)> pullFromMemory);
        void step();

    private:

        void visualizeMemory();
        void visualizeMeta();

        void commit();
        objpose::ObjectPoseSeq update();
        void reset();

        void visualizeObject(objpose::ObjectPose &objectPose);
        void visualizePlaceholder(PlaceholderState::Placeholder const& placeholder, size_t id);

    private:
        static constexpr const char* memoryLayerName = "Memory";
        static constexpr const char* metaLayerName = "Meta";
        static constexpr const char* lineString = "---------------------------";

        std::vector<std::string> placeholderOptions;

        Properties properties;
        ObjectFinder objectFinder;

        viz::Client& client;
        const std::function<void(objpose::ProvidedObjectPoseSeq&)> pushToMemory;
        const std::function<objpose::ObjectPoseSeq(void)> pullFromMemory;

        viz::Layer memoryLayer {client.layer(memoryLayerName)};
        viz::Layer metaLayer {client.layer(metaLayerName)};

        InteractionObserver observer;

        objpose::ObjectPoseSeq storedPoses;

        ChangeState changes{this};     
        PlaceholderState placeholders{this};

        bool isCommitRequired;
        bool isUpdateRequired;
        bool isResetRequired;
        bool isMemoryVizRequired;
        bool isMetaVizRequired;

        friend ChangeState;
        friend PlaceholderState;
    };
}  // namespace armarx
