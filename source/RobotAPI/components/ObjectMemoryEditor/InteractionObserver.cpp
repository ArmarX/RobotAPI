#include "InteractionObserver.h"

#include <utility>

armarx::InteractionObserver::Observation &
armarx::InteractionObserver::Observation::onContextMenu(size_t index, std::function<void()> action)
{
    contextMenuActions[index] = std::move(action);
    return *this;
}

armarx::InteractionObserver::Observation &
armarx::InteractionObserver::Observation::onTransformEnd(std::function<void(Eigen::Matrix4f const&)> action)
{
    transformEndAction = std::make_optional(std::move(action));
    return *this;
}

void armarx::InteractionObserver::Observation::process(viz::InteractionFeedback const& interaction)
{
    if (interaction.type() == viz::InteractionFeedbackType::ContextMenuChosen)
    {
        auto iterator = contextMenuActions.find(interaction.chosenContextMenuEntry());

        if (iterator != contextMenuActions.end())
        {
            auto [index, action] = *iterator;
            action();
        }
    }

    if (interaction.type() == viz::InteractionFeedbackType::Transform && interaction.isTransformEnd())
    {
        if (transformEndAction.has_value())
        {
            transformEndAction.value()(interaction.transformation());
        }
    }
}

void armarx::InteractionObserver::clearObservedLayer(armarx::viz::Layer & layer)
{
    layer.clear();
    observedLayers.erase(layer.data_.name);
}

void armarx::InteractionObserver::requestInteractions(armarx::viz::StagedCommit& stage)
{
    for (auto& [name, observedLayer] : observedLayers)
    {
        stage.requestInteraction(observedLayer.layer);
    }
}

void armarx::InteractionObserver::process(viz::InteractionFeedbackRange const& interactions)
{
    for (viz::InteractionFeedback const& interaction: interactions)
    {
        auto layerIterator = observedLayers.find(interaction.layer());

        if (layerIterator != observedLayers.end())
        {
            auto& [name, layer] = *layerIterator;

            auto observationIterator = layer.observations.find(interaction.element());

            if (observationIterator != layer.observations.end())
            {
                auto& [element, observation] = *observationIterator;
                observation.process(interaction);
            }
        }
    }
}
