#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseProviderPlugin.h>
#include "InteractionObserver.h"

namespace armarx
{
    class ObjectMemoryEditor :
            virtual public armarx::Component,
            virtual public armarx::DebugObserverComponentPluginUser,
            virtual public armarx::ArVizComponentPluginUser,
            virtual public armarx::ObjectPoseClientPluginUser,
            virtual public armarx::ObjectPoseProviderPluginUser
    {
    public:
        std::string getDefaultName() const override;

    protected:
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

    public:
        objpose::ProviderInfo getProviderInfo(const Ice::Current& = Ice::emptyCurrent) override;
        objpose::provider::RequestObjectsOutput requestObjects(const objpose::provider::RequestObjectsInput& input, const Ice::Current&) override;

    private:
        void run();

        armarx::ObjectFinder objectFinder;
        objpose::ProviderInfo providerInfo;

        float objectScaling = 1.01F;
        float confidenceThreshold = 0.0F;

        armarx::SimpleRunningTask<>::pointer_type objectVizTask;
    };
}  // namespace armarx
