#pragma once

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

namespace armarx
{
    class InteractionObserver
    {
    public:
        class Observation
        {
        public:
            Observation& onContextMenu(size_t index, std::function<void()> action);
            Observation& onTransformEnd(std::function<void(Eigen::Matrix4f const&)> action);

            void process(viz::InteractionFeedback const& interaction);

        private:
            std::map<size_t, std::function<void()>> contextMenuActions;
            std::optional<std::function<void(Eigen::Matrix4f const&)>> transformEndAction;
        };

        template <typename ElementT>
        Observation& addObserved(viz::Layer & layer, ElementT const& element)
        {
            layer.add(element);

            auto iterator = observedLayers.find(layer.data_.name);

            if (iterator == observedLayers.end())
            {
                std::tie(iterator, std::ignore) = observedLayers.emplace(layer.data_.name, layer);
            }

            auto& [name, observedLayer] = *iterator;
            return observedLayer.observations[element.data_->id];
        }

        void clearObservedLayer(viz::Layer & layer);

        void requestInteractions(viz::StagedCommit & stage);
        void process(viz::InteractionFeedbackRange const& interactions);

    private:
        struct ObservedLayer
        {
            explicit ObservedLayer(viz::Layer & layer) : layer(layer) {}

            std::reference_wrapper<viz::Layer> layer;
            std::map<std::string, Observation> observations;
        };

        std::map<std::string, ObservedLayer> observedLayers;
    };
}  // namespace armarx
