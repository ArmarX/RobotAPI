/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ObjectPoseClientExample
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

// Include the ClientPlugin
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseClientPlugin.h>


namespace armarx
{

    /**
     * @defgroup Component-ObjectPoseClientExample ObjectPoseClientExample
     * @ingroup RobotAPI-Components
     *
     * An example showing how to get object poses from the ObjectPoseStorage.
     *
     * @class ObjectPoseClientExample
     * @ingroup Component-ObjectPoseClientExample
     * @brief Brief description of class ObjectPoseClientExample.
     *
     * Gets and visualizes object poses from the ObjectPoseStorage.
     */
    class ObjectPoseClientExample :
        virtual public armarx::Component
            , virtual public armarx::DebugObserverComponentPluginUser
            , virtual public armarx::ArVizComponentPluginUser
        // Derive from the client plugin.
        , virtual public armarx::ObjectPoseClientPluginUser

    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


 protected:

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;


    private:

        void objectProcessingTaskRun();

        viz::Layer visualizePredictions(const objpose::ObjectPoseClient& client,
                                        const objpose::ObjectPoseSeq& objectPoses);


    private:

        armarx::SimpleRunningTask<>::pointer_type objectProcessingTask;

        struct Properties
        {
            int predictionsPerObject = 5; // NOLINT
            int millisecondPredictionIncrement = 200; // NOLINT
        };
        Properties p;

    };
}
