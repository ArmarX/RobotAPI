/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotStateComponent::
 * @author     ( stefan dot ulbrich at kit dot edu)
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <optional>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/interface/units/PlatformUnitInterface.h>
#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>
#include <RobotAPI/libraries/core/remoterobot/RobotStateObserver.h>

#include "SharedRobotServants.h"

#include <shared_mutex>
#include <mutex>


namespace armarx
{
    /**
     * \class RobotStatePropertyDefinition
     * \brief
     */
    class RobotStatePropertyDefinitions :
        public ComponentPropertyDefinitions
    {
    public:
        RobotStatePropertyDefinitions(std::string prefix);
    };


    /**
     * \defgroup Component-RobotStateComponent RobotStateComponent
     * \ingroup RobotAPI-Components
     * \brief Maintains a robot representation based on VirtualRobot (see [Simox](http://gitlab.com/Simox/simox)).
     *
     * The robot can be loaded from a Simox robot XML file.
     * Upon request, an Ice proxy to a shared instance of this internal robot
     * can be acquired through a call to RobotStateComponent::getSynchronizedRobot().
     * Additionally it is possible to retrieve a proxy for robot snapshot with
     * RobotStateComponent::getRobotSnapshot().
     * While the synchronized robot will constantly update its internal state
     * the robot snapshot is a clone of the original robot and won't update its
     * configuration over time.  This is useful, if several components need
     * to calculate on the same values.
     * See \ref armarx::RemoteRobot "RemoteRobot" for more details and the usage of this component.
     */

    /**
     * @brief The RobotStateComponent class
     * @ingroup Component-RobotStateComponent
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT RobotStateComponent :
        virtual public Component,
        virtual public RobotStateComponentInterface
    {
    public:

        std::string getDefaultName() const override;


        // RobotStateComponentInterface interface

        /// \return SharedRobotInterface proxy to the internal synchronized robot.
        SharedRobotInterfacePrx getSynchronizedRobot(const Ice::Current&) const override;

        /**
         * Creates a snapshot of the robot state at this moment in time.
         * \note You need to call destroy() or ref()/unref() to trigger deletion of this object.
         * \return Clone of the internal synchronized robot fixed to the configuration from the time of calling this function.
         */
        // [[deprecated]]
        SharedRobotInterfacePrx getRobotSnapshot(const std::string& deprecated, const Ice::Current&) override;

        SharedRobotInterfacePrx getRobotSnapshotAtTimestamp(double time, const Ice::Current& current) override;
        NameValueMap getJointConfigAtTimestamp(double timestamp, const Ice::Current&) const override;
        RobotStateConfig getRobotStateAtTimestamp(double timestamp, const Ice::Current&) const override;

        /// \return the robot xml filename as specified in the configuration
        std::string getRobotFilename(const Ice::Current&) const override;

        /// \return All dependent packages, which might contain a robot file.
        std::vector<std::string> getArmarXPackages(const Ice::Current&) const override;

        /// \return  The name of this robot instance.
        std::string getRobotName(const Ice::Current&) const override;

        std::string getRobotStateTopicName(const Ice::Current&) const override;

        /// \return  The name of this robot instance.
        std::string getRobotNodeSetName(const Ice::Current&) const override;

        float getScaling(const Ice::Current&) const override;

        RobotInfoNodePtr getRobotInfo(const Ice::Current&) const override;

        // GlobalRobotPoseLocalizationListener
        void reportGlobalRobotPose(const TransformStamped& globalRobotPose, const Ice::Current& = Ice::Current()) override;

        // Own interface.
        void setRobotStateObserver(RobotStateObserverPtr observer);



        // PlatformUnitListener interface
        // TODO: Remove this interface and use GlobalRobotPoseLocalizationListener only.
        /// Stores the platform pose in the pose history.
        void reportPlatformPose(const PlatformPose& currentPose, const Ice::Current& = Ice::emptyCurrent) override;
        /// Does nothing.
        void reportNewTargetPose(Ice::Float newPlatformPositionX, Ice::Float newPlatformPositionY, Ice::Float newPlatformRotation, const Ice::Current& = Ice::emptyCurrent) override {}
        /// Does nothing.
        void reportPlatformVelocity(Ice::Float currentPlatformVelocityX, Ice::Float currentPlatformVelocityY, Ice::Float currentPlatformVelocityRotation, const Ice::Current& = Ice::emptyCurrent) override {}
        /// Does nothing.
        void reportPlatformOdometryPose(Ice::Float x, Ice::Float y, Ice::Float angle, const Ice::Current& = Ice::emptyCurrent) override {}



    protected:

        // Component interface.

        /**
         * Load and create a VirtualRobot::Robot instance from the RobotFileName
         * property. Additionally listen on the KinematicUnit topic for the
         * RobotNodeSet specified in the RobotNodeSetName property.
         */
        void onInitComponent() override;
        /// Setup RobotStateObjectFactories needed for creating RemoteRobot instances.
        void onConnectComponent() override;
        void onDisconnectComponent() override;

        /// Calls unref() on RobotStateComponent::_synchronizedPrx.
        ~RobotStateComponent() override;

        /// Create an instance of RobotStatePropertyDefinitions.
        PropertyDefinitionsPtr createPropertyDefinitions() override;


        // Inherited from KinematicUnitInterface

        /// Does nothing.
        void reportControlModeChanged(const NameControlModeMap& jointModes, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c = Ice::emptyCurrent) override;
        /// Stores the reported joint angles in the joint history and publishes the new joint angles.
        void reportJointAngles(const NameValueMap& jointAngles, Ice::Long timestamp,  bool aValueChanged, const Ice::Current& c = Ice::emptyCurrent) override;
        /// Sends the joint velocities to the robot state observer.
        void reportJointVelocities(const NameValueMap& jointVelocities, Ice::Long timestamp,  bool aValueChanged, const Ice::Current& c = Ice::emptyCurrent) override;
        /// Does nothing.
        void reportJointTorques(const NameValueMap& jointTorques, Ice::Long timestamp,  bool aValueChanged, const Ice::Current& c = Ice::emptyCurrent) override;
        /// Does nothing.
        void reportJointAccelerations(const NameValueMap& jointAccelerations, Ice::Long timestamp, bool aValueChanged, const Ice::Current& c) override;
        /// Does nothing.
        void reportJointCurrents(const NameValueMap& jointCurrents, Ice::Long timestamp,  bool aValueChanged, const Ice::Current& c = Ice::emptyCurrent) override;
        /// Does nothing.
        void reportJointMotorTemperatures(const NameValueMap& jointMotorTemperatures, Ice::Long timestamp,  bool aValueChanged, const Ice::Current& c = Ice::emptyCurrent) override;
        /// Does nothing.
        void reportJointStatuses(const NameStatusMap& jointStatuses, Ice::Long timestamp,  bool aValueChanged, const Ice::Current& c = Ice::emptyCurrent) override;

        void simulatorWasReset(const Ice::Current& = Ice::emptyCurrent) override;
    private:

        void readRobotInfo(const std::string& robotFile);
        RobotInfoNodePtr readRobotInfo(const RapidXmlReaderNode& infoNode);

        void insertPose(IceUtil::Time timestamp, const Eigen::Matrix4f& globalPose);


        template <class ValueT>
        struct Timestamped
        {
            IceUtil::Time timestamp;
            ValueT value;
        };

        /// Interpolate the robot state from histories and store it in `config`.
        std::optional<RobotStateConfig> interpolate(IceUtil::Time time) const;
        /// Interpolate the joint angles from history and store it in `jointAngles`.
        std::optional<Timestamped<NameValueMap>> interpolateJoints(IceUtil::Time time) const;
        /// Interpolate the robot pose from history and store it in `pose`.
        std::optional<Timestamped<FramedPosePtr>> interpolatePose(IceUtil::Time time) const;


    private:

        /// Local robot model.
        VirtualRobot::RobotPtr _synchronized;
        /// Local shared robot.
        SharedRobotServantPtr _sharedRobotServant;
        /// Ice proxy to `_sharedRobotServant`.
        SharedRobotInterfacePrx _synchronizedPrx;

        RobotStateListenerInterfacePrx robotStateListenerPrx;
        RobotStateObserverPtr robotStateObs;

        std::string robotStateTopicName;
        std::string robotFile;
        std::string relativeRobotFile;

        mutable std::shared_mutex jointHistoryMutex;
        std::map<IceUtil::Time, NameValueMap> jointHistory;
        size_t jointHistoryLength;

        mutable std::shared_mutex poseHistoryMutex;
        std::map<IceUtil::Time, FramedPosePtr> poseHistory;
        size_t poseHistoryLength;

        std::string robotNodeSetName;

        float robotModelScaling;

        RobotInfoNodePtr robotInfo;

    };

}

