/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ObjectPoseProviderExample
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
// #include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

// Include the ProviderPlugin
#include <RobotAPI/libraries/ArmarXObjects/plugins/ObjectPoseProviderPlugin.h>
#include <RobotAPI/libraries/ArmarXObjects/plugins/RequestedObjects.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>


namespace armarx
{

    /**
     * @defgroup Component-ObjectPoseProviderExample ObjectPoseProviderExample
     * @ingroup RobotAPI-Components
     * A description of the component ObjectPoseProviderExample.
     *
     * @class ObjectPoseProviderExample
     * @ingroup Component-ObjectPoseProviderExample
     * @brief Brief description of class ObjectPoseProviderExample.
     *
     * Detailed description of class ObjectPoseProviderExample.
     */
    class ObjectPoseProviderExample :
        virtual public armarx::Component
        // Derive from the provider plugin.
        , virtual public armarx::ObjectPoseProviderPluginUser
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        // Implement the ObjectPoseProvider interface
    public:
        objpose::ProviderInfo getProviderInfo(const Ice::Current& = Ice::emptyCurrent) override;
        objpose::provider::RequestObjectsOutput requestObjects(const objpose::provider::RequestObjectsInput& input, const Ice::Current&) override;


    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


    private:

        void poseEstimationTaskRun();


    private:

        // To be moved to plugin
        std::mutex requestedObjectsMutex;
        objpose::RequestedObjects requestedObjects;


        objpose::ProviderInfo providerInfo;

        armarx::ObjectFinder objectFinder;
        std::vector<std::string> initialObjectIDs = { "KIT/Amicelli", "KIT/YellowSaltCylinder" };
        bool singleShot = false;


        armarx::SimpleRunningTask<>::pointer_type poseEstimationTask;

        /// Debug observer. Used to visualize e.g. time series.
        armarx::DebugObserverInterfacePrx debugObserver;

    };
}
