
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore RobotHealthDummy)
 
armarx_add_test(RobotHealthDummyTest RobotHealthDummyTest.cpp "${LIBS}")