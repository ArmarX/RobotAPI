/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::TopicTimingServer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TopicTimingServer.h"

#include <ArmarXCore/core/time/CycleUtil.h>

#include <ArmarXCore/observers/variant/Variant.h>


namespace armarx
{
    TopicTimingServerPropertyDefinitions::TopicTimingServerPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("TimingTestTopicName", "TimingTestTopic", "-");
        defineOptionalProperty<int>("UpdatePeriodInMS", 10, "-");

        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");

    }


    std::string TopicTimingServer::getDefaultName() const
    {
        return "TopicTimingServer";
    }


    void TopicTimingServer::onInitComponent()
    {
        offeringTopicFromProperty("TimingTestTopicName");

        offeringTopicFromProperty("DebugObserverName");

        updatePeriodInMS = getProperty<int>("UpdatePeriodInMS").getValue();
    }


    void TopicTimingServer::onConnectComponent()
    {
        topic = getTopicFromProperty<armarx::topic_timing::TopicPrx>("TimingTestTopicName");

        debugObserver = getTopicFromProperty<DebugObserverInterfacePrx>("DebugObserverName");

        lastSmallTime = IceUtil::Time::now();

        task = new RunningTask<TopicTimingServer>(this, &TopicTimingServer::run);
        task->start();
    }


    void TopicTimingServer::onDisconnectComponent()
    {
        task->stop();
        task = nullptr;
    }


    void TopicTimingServer::onExitComponent()
    {

    }




    armarx::PropertyDefinitionsPtr TopicTimingServer::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new TopicTimingServerPropertyDefinitions(
                getConfigIdentifier()));
    }

    void TopicTimingServer::run()
    {
        CycleUtil c(updatePeriodInMS);
        while (!task->isStopped())
        {
            IceUtil::Time time_now = IceUtil::Time::now();
            {
                topic_timing::SmallData data;
                data.sentTimestamp = time_now.toMicroSeconds();

                topic->reportSmall(data);
            }
            IceUtil::Time time_sent = IceUtil::Time::now();

            IceUtil::Time dur_send = time_sent - time_now;

            IceUtil::Time dur_cycle = time_now - lastSmallTime;
            lastSmallTime = time_now;

            StringVariantBaseMap channel;
            channel["DurationSend"] = new Variant(dur_send.toMilliSecondsDouble());
            channel["DurationCycle"] = new Variant(dur_cycle.toMilliSecondsDouble());
            debugObserver->setDebugChannel("TopicTimingServerSmall", channel);

            c.waitForCycleDuration();
        }
    }
}
