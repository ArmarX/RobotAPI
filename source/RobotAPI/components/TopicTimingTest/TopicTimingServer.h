/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::TopicTimingServer
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/interface/core/TopicTimingTest.h>


namespace armarx
{
    /**
     * @class TopicTimingServerPropertyDefinitions
     * @brief
     */
    class TopicTimingServerPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        TopicTimingServerPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-TopicTimingServer TopicTimingServer
     * @ingroup RobotAPI-Components
     * A description of the component TopicTimingServer.
     *
     * @class TopicTimingServer
     * @ingroup Component-TopicTimingServer
     * @brief Brief description of class TopicTimingServer.
     *
     * Detailed description of class TopicTimingServer.
     */
    class TopicTimingServer :
        virtual public armarx::Component
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;


    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        virtual void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        virtual void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        virtual void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        virtual void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        void run();

    private:
        armarx::topic_timing::TopicPrx topic;
        int updatePeriodInMS = 10;

        armarx::DebugObserverInterfacePrx debugObserver;

        RunningTask<TopicTimingServer>::pointer_type task;

        IceUtil::Time lastSmallTime = IceUtil::Time::microSeconds(0);
    };
}
