/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::TopicTimingClient
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <RobotAPI/interface/core/TopicTimingTest.h>

#include <boost/circular_buffer.hpp>

#include <mutex>


namespace armarx
{
    /**
     * @class TopicTimingClientPropertyDefinitions
     * @brief
     */
    class TopicTimingClientPropertyDefinitions :
        public armarx::ComponentPropertyDefinitions
    {
    public:
        TopicTimingClientPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-TopicTimingClient TopicTimingClient
     * @ingroup RobotAPI-Components
     * A description of the component TopicTimingClient.
     *
     * @class TopicTimingClient
     * @ingroup Component-TopicTimingClient
     * @brief Brief description of class TopicTimingClient.
     *
     * Detailed description of class TopicTimingClient.
     */
    class TopicTimingClient
        : virtual public armarx::Component
        , virtual public armarx::topic_timing::Topic
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;

        // Topic interface
        void reportSmall(const topic_timing::SmallData& data, const Ice::Current&) override;
        void reportBig(const topic_timing::BigData& data, const Ice::Current&) override;

    protected:

        /// @see armarx::ManagedIceObject::onInitComponent()
        virtual void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        virtual void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        virtual void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        virtual void onExitComponent() override;

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


    private:
        armarx::DebugObserverInterfacePrx debugObserver;

        std::mutex mutex;
        IceUtil::Time lastSmallTime = IceUtil::Time::microSeconds(0);

        boost::circular_buffer<IceUtil::Time> updateTimes;
        int simulateWorkForMS = 20;
    };
}
