#include <RobotAPI/components/TopicTimingTest/TopicTimingClient.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>

int main(int argc, char* argv[])
{
    return armarx::runSimpleComponentApp < armarx::TopicTimingClient > (argc, argv, "TopicTimingClient");
}
