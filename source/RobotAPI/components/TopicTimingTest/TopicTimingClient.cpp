/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::TopicTimingClient
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TopicTimingClient.h"

#include <ArmarXCore/observers/variant/Variant.h>


namespace armarx
{
    TopicTimingClientPropertyDefinitions::TopicTimingClientPropertyDefinitions(std::string prefix) :
        armarx::ComponentPropertyDefinitions(prefix)
    {
        defineOptionalProperty<std::string>("TimingTestTopicName", "TimingTestTopic", "-");
        defineOptionalProperty<int>("SimulateWorkForMS", 20, "Sleeps until it returns from the topic call");

        defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");

    }


    std::string TopicTimingClient::getDefaultName() const
    {
        return "TopicTimingClient";
    }


    void TopicTimingClient::onInitComponent()
    {
        usingTopicFromProperty("TimingTestTopicName");

        offeringTopicFromProperty("DebugObserverName");
    }


    void TopicTimingClient::onConnectComponent()
    {
        // Get topics and proxies here. Pass the *InterfacePrx type as template argument.
        debugObserver = getTopicFromProperty<DebugObserverInterfacePrx>("DebugObserverName");
        // debugDrawer.getTopic(*this);  // Calls this->getTopic().

        // getProxyFromProperty<MyProxyInterfacePrx>("MyProxyName");

        lastSmallTime = IceUtil::Time::now();

        updateTimes = boost::circular_buffer<IceUtil::Time>(11);

        simulateWorkForMS = getProperty<int>("SimulateWorkForMS").getValue();
    }


    void TopicTimingClient::onDisconnectComponent()
    {

    }


    void TopicTimingClient::onExitComponent()
    {

    }




    armarx::PropertyDefinitionsPtr TopicTimingClient::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new TopicTimingClientPropertyDefinitions(
                getConfigIdentifier()));
    }
}


void armarx::TopicTimingClient::reportSmall(const topic_timing::SmallData& data, const Ice::Current&)
{
    IceUtil::Time time_now = IceUtil::Time::now();

    IceUtil::Time time_sent = IceUtil::Time::microSeconds(data.sentTimestamp);

    IceUtil::Time dur_transfer = time_now - time_sent;

    IceUtil::Time dur_cycle;
    {
        std::unique_lock<std::mutex> lock(mutex);
        dur_cycle = time_now - lastSmallTime;
        lastSmallTime = time_now;
        updateTimes.push_back(time_now);

        usleep(simulateWorkForMS * 1000);
    }

    // Calculate updates per second
    float updatesPerSecond = 0.0f;
    IceUtil::Time duration = IceUtil::Time::microSeconds(0);
    if (updateTimes.size() > 1)
    {
        duration = updateTimes.back() - updateTimes.front();
        updatesPerSecond = (updateTimes.size() - 1) / duration.toSecondsDouble();
    }

    StringVariantBaseMap channel;
    channel["DurationTransfer"] = new Variant(dur_transfer.toMilliSecondsDouble());
    channel["DurationCycle"] = new Variant(dur_cycle.toMilliSecondsDouble());
    channel["Duration10"] = new Variant(duration.toMilliSecondsDouble());
    channel["UpdatesPerSecond"] = new Variant(updatesPerSecond);
    debugObserver->setDebugChannel("TopicTimingClientSmall-" + getName(), channel);
}

void armarx::TopicTimingClient::reportBig(const topic_timing::BigData& data, const Ice::Current&)
{
    IceUtil::Time sentTime = IceUtil::Time::microSeconds(data.sentTimestamp);
    (void) sentTime;
}
