/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::KITHandUnit
 * @author     Stefan Reither ( stefan dot reither at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/RemoteGuiComponentPlugin.h>

#include <RobotAPI/components/units/HandUnit.h>

#include <KITHandCommunicationDriver.h>

namespace armarx
{
    /**
     * @class KITHandUnitPropertyDefinitions
     * @brief
     */
    class KITHandUnitPropertyDefinitions :
        public armarx::HandUnitPropertyDefinitions
    {
    public:
        KITHandUnitPropertyDefinitions(std::string prefix);
    };



    /**
     * @defgroup Component-KITHandUnit KITHandUnit
     * @ingroup RobotAPI-Components
     * A description of the component KITHandUnit.
     *
     * @class KITHandUnit
     * @ingroup Component-KITHandUnit
     * @brief Brief description of class KITHandUnit.
     *
     * Detailed description of class KITHandUnit.
     */
    class KITHandUnit :
        virtual public RemoteGuiComponentPluginUser,
        virtual public armarx::HandUnit
    {
    public:
        /// @see armarx::ManagedIceObject::getDefaultName()
        virtual std::string getDefaultName() const override;

        void onInitHandUnit() override;
        void onStartHandUnit() override;
        void onExitHandUnit() override;
        void setShape(const std::string& shapeName, const Ice::Current& = Ice::emptyCurrent) override;
        void setJointAngles(const NameValueMap& targetJointAngles, const Ice::Current& = Ice::emptyCurrent) override;
        NameValueMap getCurrentJointValues(const Ice::Current& = Ice::emptyCurrent) override;

        void addShape(const std::string& name, const std::map<std::string, float>& shape);
        void addShapeName(const std::string& name);

    protected:

        /// @see PropertyUser::createPropertyDefinitions()
        virtual armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void connectionStateChangedCallback(const KITHand::State state);

        RemoteGui::WidgetPtr buildGui();
        void processGui(RemoteGui::TabProxy& prx);

    private:
        KITHand::KITHandCommunicationDriverPtr _driver;

        std::map<std::string, std::map<std::string, float>> _shapes;

    };
}
