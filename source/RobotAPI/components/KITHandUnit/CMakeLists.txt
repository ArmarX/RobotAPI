armarx_component_set_name("KITHandUnit")

find_package(KITHand QUIET)
armarx_build_if(KITHand_FOUND "KITHand stand-alone lib not available")

set(COMPONENT_LIBS
    ArmarXCore
    ArmarXGuiComponentPlugins
    RobotAPIUnits
    KITHandCommunicationDriver
)

set(SOURCES KITHandUnit.cpp)
set(HEADERS KITHandUnit.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)

armarx_generate_and_add_component_executable(APPLICATION_APP_SUFFIX)
