/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::FrameTracking
 * @author     Adrian Knobloch ( adrian dot knobloch at student dot kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXObjects::ArViz

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include <RobotAPI/components/ArViz/Client/elements/point_cloud_type_traits.hpp>
#include <RobotAPI/components/ArViz/Client/elements/PointCloud.h>

#include <iostream>

#include <SimoxUtility/color/cmaps.h>


namespace viz = armarx::viz;

namespace PointCloudTestNamespace
{

    struct PointXYZ
    {
        float x, y, z;
    };

    struct PointXYZRGBA
    {
        float x, y, z;
        uint8_t r, g, b, a;
    };

    struct PointXYZL
    {
        float x, y, z;
        uint32_t label;
    };

    struct PointXYZRGBL
    {
        float x, y, z;
        uint8_t r, g, b, a;
        uint32_t label;
    };


    struct PointCloudFixture
    {
        PointCloudFixture()
        {

        }
        ~PointCloudFixture()
        {

        }

        viz::PointCloud pc {"pointcloud"};

        const std::vector<PointXYZRGBL> points
        {
            { 10, 20, 30, 1, 2, 3, 4, 100 },
        };

        const std::vector<PointXYZRGBL> points2
        {
            { 40, 50, 60, 4, 5, 6,  7, 100 },
            { 70, 80, 90, 7, 8, 9, 10, 200 },
        };
        const std::vector<int> indices
        {
            1,
        };
    };

}


BOOST_AUTO_TEST_CASE(test_has_member_rgba)
{
    using namespace PointCloudTestNamespace;
    static_assert(!armarx::viz::detail::has_members_rgba<PointXYZ>::value,
                  "has_members_rgba<PointXYZ>::value must be false.");

    static_assert(armarx::viz::detail::has_members_rgba<PointXYZRGBA>::value,
                  "has_members_rgba<PointXYZRGBA>::value must be true.");

    static_assert(armarx::viz::detail::has_members_rgba<PointXYZRGBL>::value,
                  "has_members_rgba<PointXYZRGBL>::value must be true.");


    static_assert(!armarx::viz::detail::has_member_label<PointXYZ>::value,
                  "has_member_label<PointXYZ>::value must be false.");

    static_assert(!armarx::viz::detail::has_member_label<PointXYZRGBA>::value,
                  "has_member_label<PointXYZRGBA>::value must be false.");

    static_assert(armarx::viz::detail::has_member_label<PointXYZRGBL>::value,
                  "has_member_label<PointXYZRGBL>::value must be true.");

    BOOST_CHECK_EQUAL(true, true);
}



BOOST_AUTO_TEST_CASE(test_addPoint_color_types)
{
    using namespace PointCloudTestNamespace;
    namespace viz = armarx::viz;
    viz::PointCloud pc("pointcloud");

    viz::ColoredPoint* begin = (viz::ColoredPoint*)pc.data_->points.data();
    viz::ColoredPoint* end = begin + (pc.data_->points.size() / sizeof (viz::ColoredPoint));
    viz::ColoredPointList points(begin, end);

    // XYZ, default color
    pc.addPoint(PointXYZ());
    BOOST_CHECK_EQUAL(points.back().color, simox::Color::gray());

    // XYZ, color
    pc.addPoint(PointXYZ(), simox::Color(1, 2, 3));
    BOOST_CHECK_EQUAL(points.back().color, simox::Color(1, 2, 3));


    // RGBA
    pc.addPoint(PointXYZRGBA{0., 0., 0., 2, 3, 4, 5});
    BOOST_CHECK_EQUAL(points.back().color, simox::Color(2, 3, 4, 5));

    // RGBA, color override
    pc.addPoint(PointXYZRGBA{0., 0., 0., 2, 3, 4, 5}, simox::Color(5, 4, 3, 2));
    BOOST_CHECK_EQUAL(points.back().color, simox::Color(5, 4, 3, 2));


    // Label
    pc.addPoint(PointXYZL{0., 0., 0., 1});
    BOOST_CHECK_EQUAL(points.back().color, simox::color::GlasbeyLUT::at(1));

    // Label, color override
    pc.addPoint(PointXYZL{0., 0., 0., 2}, simox::Color(5, 6, 7));
    BOOST_CHECK_EQUAL(points.back().color, simox::Color(5, 6, 7));


    // RGBL, label
    pc.addPoint(PointXYZRGBL{0., 0., 0., 6, 7, 8, 9, 100});
    BOOST_CHECK_EQUAL(points.back().color, simox::color::GlasbeyLUT::at(100));

    // RGBL, color override
    pc.addPoint(PointXYZRGBL{0., 0., 0., 6, 7, 8, 9, 100}, simox::Color(10, 11, 12, 13));
    BOOST_CHECK_EQUAL(points.back().color, simox::Color(10, 11, 12, 13));

    // RGBL, label
    pc.addPoint(PointXYZRGBL{0., 0., 0., 6, 7, 8, 9, 200}, true);
    BOOST_CHECK_EQUAL(points.back().color, simox::color::GlasbeyLUT::at(200));

    // RGBL, RGBA
    pc.addPoint(PointXYZRGBL{0., 0., 0., 6, 7, 8, 9, 100}, false);
    BOOST_CHECK_EQUAL(points.back().color, simox::Color(6, 7, 8, 9));

}


BOOST_FIXTURE_TEST_SUITE(test_pointCloud_suite, PointCloudTestNamespace::PointCloudFixture)



BOOST_AUTO_TEST_CASE(test_pointCloud_colorByLabel)
{
    bool colorByLabel = false;

    pc.pointCloud(points, colorByLabel);
    BOOST_CHECK_EQUAL(ps.size(), 1);
    BOOST_CHECK_EQUAL(ps.front().color, simox::Color(1, 2, 3, 4));

    colorByLabel = true;

    pc.pointCloud(points, colorByLabel);
    BOOST_CHECK_EQUAL(ps.size(), 1);
    BOOST_CHECK_EQUAL(ps.front().color, simox::color::GlasbeyLUT::at(points.front().label));
}


BOOST_AUTO_TEST_CASE(test_pointCloud_color_func)
{
    using namespace PointCloudTestNamespace;
    // Test returning a viz::Color.

    pc.pointCloud(points, [](const PointXYZRGBL & p)
    {
        return viz::Color::fromRGBA(p.r, 0, 0, 0);
    });
    BOOST_CHECK_EQUAL(ps.size(), 1);
    BOOST_CHECK_EQUAL(ps.front().color, simox::Color(points.front().r, 0, 0, 0));

    pc.pointCloud(points2, indices, [](const PointXYZRGBL & p)
    {
        return viz::Color::fromRGBA(p.r, 0, 0, 0);
    });
    BOOST_CHECK_EQUAL(ps.size(), 1);
    BOOST_CHECK_EQUAL(ps.front().color, simox::Color(points2.back().r, 0, 0, 0));


    // Test returning a simox::Color.

    pc.pointCloud(points, [](PointXYZRGBL p)
    {
        return simox::Color(0, p.g, 0, 0);
    });
    BOOST_CHECK_EQUAL(ps.size(), 1);
    BOOST_CHECK_EQUAL(ps.front().color, simox::Color(0, points.front().g, 0, 0));

    pc.pointCloud(points2, indices, [](const PointXYZRGBL & p)
    {
        return simox::Color(0, p.g, 0, 0);
    });
    BOOST_CHECK_EQUAL(ps.size(), 1);
    BOOST_CHECK_EQUAL(ps.front().color, simox::Color(0, points2.back().g, 0, 0));

}


BOOST_AUTO_TEST_CASE(test_pointCloud_scalar_func_cmap)
{
    using namespace PointCloudTestNamespace;
    const simox::ColorMap cmap = simox::color::cmaps::viridis();

    pc.pointCloud(points, cmap, [](const PointXYZRGBL & p)
    {
        return p.x;
    });
    BOOST_CHECK_EQUAL(ps.size(), 1);
    BOOST_CHECK_EQUAL(ps.front().color, cmap(ps.front().x));

    pc.pointCloud(points2, indices, cmap, [](const PointXYZRGBL & p)
    {
        return p.x;
    });
    BOOST_CHECK_EQUAL(ps.size(), 1);
    BOOST_CHECK_EQUAL(ps.front().color, cmap(points2.back().x));


    pc.pointCloud(points, cmap, [](PointXYZRGBL p)
    {
        return p.y;
    });
    BOOST_CHECK_EQUAL(ps.size(), 1);
    BOOST_CHECK_EQUAL(ps.front().color, cmap(ps.front().y));

    pc.pointCloud(points2, indices, cmap, [](const PointXYZRGBL & p)
    {
        return p.y;
    });
    BOOST_CHECK_EQUAL(ps.size(), 1);
    BOOST_CHECK_EQUAL(ps.front().color, cmap(points2.back().y));
}


BOOST_AUTO_TEST_SUITE_END()

