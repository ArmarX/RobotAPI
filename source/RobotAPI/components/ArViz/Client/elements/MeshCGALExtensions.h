#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Surface_mesh/Surface_mesh.h>
#include <CGAL/boost/graph/iterator.h>
#pragma GCC diagnostic pop

#include "Mesh.h"

namespace armarx::viz
{
    template<class T>
    inline Mesh& Mesh::mesh(const CGAL::Surface_mesh<T>& sm)
    {
        using sm_t = CGAL::Surface_mesh<T>;
        using vidx_t = typename sm_t::Vertex_index;

        auto& vertices = data_->vertices;
        auto& faces = data_->faces;
        vertices.clear();
        faces.clear();
        vertices.reserve(sm.number_of_vertices());
        faces.reserve(sm.number_of_faces());
        std::map<vidx_t, std::size_t> index;
        for (const auto& vidx : sm.vertices())
        {
            index[vidx] = vertices.size();
            const auto& p = sm.point(vidx);
            armarx::Vector3f visp;
            visp.e0 = p.x();
            visp.e1 = p.y();
            visp.e2 = p.z();
            vertices.emplace_back(visp);
        }
        for (const auto& fidx : sm.faces())
        {
            data::Face f;
            const auto hf = sm.halfedge(fidx);
            std::size_t i = 0;
            for (auto hi : CGAL::halfedges_around_face(hf, sm))
            {
                const auto vidx = CGAL::target(hi, sm);
                switch (i++)
                {
                    case 0:
                        f.v0 = index.at(vidx);
                        break;
                    case 1:
                        f.v1 = index.at(vidx);
                        break;
                    case 2:
                        f.v2 = index.at(vidx);
                        break;
                    default:
                        break; // error handling below
                }
            }
            ARMARX_CHECK_EQUAL(3, i) << "One face is no triangle!";
            faces.emplace_back(f);
        }
        return *this;
    }

    template < class PolyhedronTraits_3,
               class PolyhedronItems_3,
               template < class T, class I, class A>
               class T_HDS,
               class Alloc>
    inline Mesh& Mesh::mesh(const CGAL::Polyhedron_3 <
                            PolyhedronTraits_3,
                            PolyhedronItems_3,
                            T_HDS,
                            Alloc
                            > & p3)
    {
        auto& vertices = data_->vertices;
        auto& faces = data_->faces;
        vertices.clear();
        faces.clear();
        vertices.reserve(p3.size_of_vertices());
        faces.reserve(p3.size_of_facets());
        auto vbeg = p3.vertices_begin();
        for (const auto& v : IteratorRange{vbeg, p3.vertices_end()})
        {
            const auto& p = v.point();
            armarx::Vector3f visp;
            visp.e0 = p.x();
            visp.e1 = p.y();
            visp.e2 = p.z();
            vertices.emplace_back(visp);
        }
        for (const auto& fidx : IteratorRange{p3.facets_begin(), p3.facets_end()})
        {
            auto circ = fidx.facet_begin();
            ARMARX_CHECK_EQUAL(3, CGAL::circulator_size(circ))
                    << "One face is no triangle!";
            data::Face f;
            f.v0 = std::distance(vbeg, circ->vertex());
            ++circ;
            f.v1 = std::distance(vbeg, circ->vertex());
            ++circ;
            f.v2 = std::distance(vbeg, circ->vertex());
            ++circ;
            ARMARX_CHECK_EXPRESSION(circ == fidx.facet_begin())
                    << "Internal error while circulating a facet";
            faces.emplace_back(f);
        }
        return *this;
    }
}
