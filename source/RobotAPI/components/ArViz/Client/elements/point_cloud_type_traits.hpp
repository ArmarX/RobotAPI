#pragma once


#include <SimoxUtility/meta/has_member_macros/has_member.hpp>


namespace armarx::viz::detail
{

    define_has_member(r);
    define_has_member(g);
    define_has_member(b);
    define_has_member(a);
    define_has_member(label);


    template <typename PointT>
    class has_members_rgba
    {
    public:
        static constexpr bool value =
            has_member_r<PointT>::value && has_member_g<PointT>::value
            && has_member_b<PointT>::value && has_member_a<PointT>::value;

    };


}

