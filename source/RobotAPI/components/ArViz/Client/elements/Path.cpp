#include "Path.h"

#include <iterator>

#include <ArmarXCore/interface/core/BasicVectorTypes.h>
#include <ArmarXCore/interface/core/BasicVectorTypesHelpers.h>

namespace armarx::viz
{

    Path& Path::clear()
    {
        data_->points.clear();

        return *this;
    }

    Path& Path::width(float w)
    {
        data_->lineWidth = w;

        return *this;
    }

    Path& Path::points(std::vector<Eigen::Vector3f> const& ps)
    {
        auto& points = data_->points;
        points.clear();
        points.reserve(ps.size());

        std::transform(ps.begin(),
                       ps.end(),
                       std::back_inserter(points),
                       [](const auto & e)
        {
            return ToBasicVectorType(e);
        });

        return *this;
    }

    Path& Path::addPoint(Eigen::Vector3f p)
    {
        data_->points.emplace_back(ToBasicVectorType(p));

        return *this;
    }

} // namespace armarx::viz