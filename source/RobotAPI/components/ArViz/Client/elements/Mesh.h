#pragma once

#include "ElementOps.h"

#include <RobotAPI/interface/ArViz/Elements.h>

#include <Eigen/Core>

#include <functional>
#include <numeric>  // for std::accumulate
#include <vector>


namespace CGAL
{
    template < class PolyhedronTraits_3,
               class PolyhedronItems_3,
               template < class T, class I, class A>
               class T_HDS,
               class Alloc>
    class Polyhedron_3;
    template<class> class Surface_mesh;
}

namespace armarx::viz
{

    class Mesh : public ElementOps<Mesh, data::ElementMesh>
    {
    public:
        using ElementOps::ElementOps;

        Mesh& vertices(const Eigen::Vector3f* vs, std::size_t size)
        {
            auto& vertices = data_->vertices;
            vertices.clear();
            vertices.reserve(size);

            for (std::size_t i = 0; i < size; ++i)
            {
                vertices.push_back(armarx::Vector3f{vs[i].x(), vs[i].y(), vs[i].z()});
            }

            return *this;
        }
        Mesh& vertices(const std::vector<Eigen::Vector3f>& vs)
        {
            return this->vertices(vs.data(), vs.size());
        }

        Mesh& vertices(const armarx::Vector3f* vs, std::size_t size)
        {
            data_->vertices.assign(vs, vs + size);

            return *this;
        }
        Mesh& vertices(const std::vector<armarx::Vector3f>& vs)
        {
            return this->vertices(vs.data(), vs.size());
        }

        Mesh& colors(const data::Color* cs, std::size_t size)
        {
            data_->colors.assign(cs, cs + size);

            return *this;
        }
        Mesh& colors(const std::vector<data::Color>& cs)
        {
            return this->colors(cs.data(), cs.size());
        }

        Mesh& faces(const data::Face* fs, std::size_t size)
        {
            data_->faces.assign(fs, fs + size);

            return *this;
        }
        Mesh& faces(const std::vector<data::Face>& fs)
        {
            return this->faces(fs.data(), fs.size());
        }

        template<class T>
        Mesh& mesh(const CGAL::Surface_mesh<T>& sm);

        template < class PolyhedronTraits_3,
                   class PolyhedronItems_3,
                   template < class T, class I, class A>
                   class T_HDS,
                   class Alloc>
        Mesh& mesh(const CGAL::Polyhedron_3 <
                   PolyhedronTraits_3,
                   PolyhedronItems_3,
                   T_HDS,
                   Alloc
                   > & p3);
        /**
         * @brief Builds a regular 2D grid in the xy-plane.
         * @param extents The full extents in x and y direction.
         * @param numPoints The number of points in x and y direction.
         * @param colorFunc A function determining the color of each vertex.
         */
        Mesh& grid2D(Eigen::Vector2f extents, Eigen::Vector2i numPoints,
                     std::function<viz::Color(size_t i, size_t j, const Eigen::Vector3f& p)> colorFunc);

        /**
         * @brief Builds a regular 2D grid.
         *
         * The shape of `vertices` and `colors` must match, i.e.:
         * - `vertices` and `colors` must have equal size.
         * - Each element (nested vector) of `vertices` and `colors` must have equal size.
         *
         * @param vertices The vertices.
         * @param colors The colors.
         */
        Mesh& grid2D(const std::vector<std::vector<Eigen::Vector3f>>& vertices,
                     const std::vector<std::vector<viz::data::Color>>& colors);
    };



    namespace grid
    {
        /**
         * @brief Builds vertices of a regular 2D grid in the xy-plane.
         *
         * If the result is indexed as result[i][j], the i index represents the x-axis,
         * the j index represents the y-axis.
         *
         * @param extents The full extents per axis.
         * @param numPoints The number of points per axis.
         * @param height The height (z-value).
         * @return The vertices.
         */
        std::vector<std::vector<Eigen::Vector3f>> makeGrid2DVertices(
                Eigen::Vector2f extents, Eigen::Vector2i numPoints, float height = 0);


        /**
         * @brief Build colors of a 2D grid.
         * @param vertices The vertices.
         * @param colorFunc A function determining the color of each vertex.
         * @return The colors.
         *
         * @see `makeGrid2DVertices()`
         */
        std::vector<std::vector<viz::data::Color>> makeGrid2DColors(
                const std::vector<std::vector<Eigen::Vector3f>>& vertices,
                std::function<viz::Color(size_t x, size_t y, const Eigen::Vector3f& p)> colorFunc);


        /**
         * @brief Builds faces of a 2D grid.
         *
         * The built indexes refer to flattened arrays of vertices and colors,
         * such as produced by `flatten()` applied to the result of `makeGrid2DVertices()`.
         *
         * @param num_x The number of vertices in x-axis.
         * @param num_y The number of vertices in y-axis.
         * @return The faces.
         */
        std::vector<viz::data::Face> makeGrid2DFaces(size_t num_x, size_t num_y);


        template <class T>
        /// @brief Flattens a 2D vector of nested vectors to a 1D vector.
        std::vector<T> flatten(const std::vector<std::vector<T>>& vector)
        {
            size_t size = std::accumulate(vector.begin(), vector.end(), size_t(0), [](size_t s, const auto & v)
            {
                return s + v.size();
            });

            std::vector<T> flat;
            flat.reserve(size);
            for (const auto& v : vector)
            {
                for (const auto& val : v)
                {
                    flat.push_back(val);
                }
            }
            return flat;
        }
    }

}
