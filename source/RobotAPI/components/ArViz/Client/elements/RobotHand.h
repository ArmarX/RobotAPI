#pragma once

#include <optional>

#include <VirtualRobot/VirtualRobot.h>

#include <RobotAPI/interface/core/RobotState.h>
#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>

#include "Robot.h"


namespace armarx::viz
{

    /**
     * @brief Left or right hand of a robot.
     */
    class RobotHand : public Robot
    {
    public:

        using Robot::Robot;

        /**
         * @brief Set the robot file according the desired side.
         * @param side The side ("Left" or "Right", see `RobotNameHelper::LocationLeft, LocationRight`)
         * @param robotInfo
         *  The robot info.
         *  You can get it from a RobotStateComponent: `robotStateComponent->getRobotInfo()`.
         *  If you do this in a loop, you should get the robot info just once before the loop.
         */
        RobotHand& fileBySide(const std::string& side, RobotInfoNodePtr robotInfo);
        RobotHand& fileBySide(const std::string& side, const RobotNameHelper& nameHelper);
        RobotHand& fileByArm(const RobotNameHelper::Arm& arm);
        RobotHand& fileByArm(const RobotNameHelper::RobotArm& arm);


        /**
         * @brief Set the pose of `robotViz` according to the given TCP pose.
         *
         * You must specify the side beforehand using `fileBySide()`.
         */
        RobotHand& tcpPose(const Eigen::Matrix4f& tcpPose, VirtualRobot::RobotPtr robot);


        /// The arm name helper. Set by `fileBySide()`.
        std::optional<RobotNameHelper::Arm> arm;

    };

}
