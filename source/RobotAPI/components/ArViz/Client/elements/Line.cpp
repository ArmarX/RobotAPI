#include "Line.h"

#include <ArmarXCore/interface/core/BasicVectorTypesHelpers.h>

namespace armarx::viz
{
    Line& Line::lineWidth(float w)
    {
        data_->lineWidth = w;

        return *this;
    }
    Line& Line::fromTo(Eigen::Vector3f from, Eigen::Vector3f to)
    {
        data_->from = ToBasicVectorType(from);
        data_->to   = ToBasicVectorType(to);

        return *this;
    }
} // namespace armarx::viz
