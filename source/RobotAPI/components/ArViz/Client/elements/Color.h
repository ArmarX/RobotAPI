#pragma once

#include <RobotAPI/interface/ArViz/Elements.h>

#include <SimoxUtility/color/Color.h>

#include <Eigen/Core>


namespace armarx::viz
{

    struct Color : data::Color
    {
        using data::Color::Color;

        Color(const data::Color& c) : data::Color(c) {}

        Color(simox::Color c)
        {
            this->r = c.r;
            this->g = c.g;
            this->b = c.b;
            this->a = c.a;
        }
        Color(int r, int g, int b, int a = 255)
        {
            this->r = simox::color::to_byte(r);
            this->g = simox::color::to_byte(g);
            this->b = simox::color::to_byte(b);
            this->a = simox::color::to_byte(a);
        }
        Color(float r, float g, float b, float a = 1.0)
        {
            this->r = simox::color::to_byte(r);
            this->g = simox::color::to_byte(g);
            this->b = simox::color::to_byte(b);
            this->a = simox::color::to_byte(a);
        }

        /// Construct a byte color from R, G, B and optional alpha.
        static inline Color fromRGBA(int r, int g, int b, int a = 255)
        {
            return {r, g, b, a};
        }

        /// Construct a float color from R, G, B and optional alpha.
        static inline Color fromRGBA(float r, float g, float b, float a = 1.0)
        {
            return {r, g, b, a};
        }

        /// Construct from a simox Color.
        static inline Color fromRGBA(simox::Color c)
        {
            return {c};
        }


        // Colorless

        static inline Color black(int a = 255)
        {
            return simox::Color::black(a);
        }

        static inline Color white(int a = 255)
        {
            return simox::Color::white(a);
        }

        static inline Color gray(int g = 128, int a = 255)
        {
            return simox::Color::gray(g, a);
        }

        // Primary colors

        static inline Color red(int r = 255, int a = 255)
        {
            return simox::Color::red(r, a);
        }

        static inline Color green(int g = 255, int a = 255)
        {
            return simox::Color::green(g, a);
        }

        static inline Color blue(int b = 255, int a = 255)
        {
            return simox::Color::blue(b, a);
        }


        // Secondary colors

        /// Green + Blue
        static inline Color cyan(int c = 255, int a = 255)
        {
            return simox::Color::cyan(c, a);
        }

        /// Red + Green
        static inline Color yellow(int y = 255, int a = 255)
        {
            return simox::Color::yellow(y, a);
        }

        /// Red + Blue
        static inline Color magenta(int m = 255, int a = 255)
        {
            return simox::Color::magenta(m, a);
        }


        // 2:1 Mixed colors

        /// 2 Red + 1 Green
        static inline Color orange(int o = 255, int a = 255)
        {
            return simox::Color::orange(o, a);
        }

        /// 2 Red + 1 Blue
        static inline Color pink(int p = 255, int a = 255)
        {
            return simox::Color::pink(p, a);
        }

        /// 2 Green + 1 Red
        static inline Color lime(int l = 255, int a = 255)
        {
            return simox::Color::lime(l, a);
        }

        /// 2 Green + 1 Blue
        static inline Color turquoise(int t = 255, int a = 255)
        {
            return simox::Color::turquoise(t, a);
        }

        /// 2 Blue + 1 Green
        static inline Color azure(int az = 255, int a = 255)
        {
            return simox::Color::azure(az, a);
        }

        /// 2 Blue + 1 Red
        static inline Color purple(int p = 255, int a = 255)
        {
            return simox::Color::purple(p, a);
        }

    };


    inline std::ostream& operator<<(std::ostream& os, const Color& c)
    {
        return os << "(" << int(c.r) << " " << int(c.g) << " " << int(c.b)
               << " | " << int(c.a) << ")";
    }

    inline bool operator==(const Color& lhs, const Color& rhs)
    {
        return lhs.r == rhs.r && lhs.g == rhs.g && lhs.b == rhs.b && lhs.a == rhs.a;
    }

    inline bool operator!=(const Color& lhs, const Color& rhs)
    {
        return !(lhs == rhs);
    }

    namespace data
    {
        // viz::Color == simox::Color
        inline bool operator==(const viz::data::Color& lhs, const simox::color::Color& rhs)
        {
            return lhs.r == rhs.r && lhs.g == rhs.g && lhs.b == rhs.b && lhs.a == rhs.a;
        }

        // simox::Color == viz::Color
        inline bool operator==(const simox::color::Color& lhs, const armarx::viz::data::Color& rhs)
        {
            return rhs == lhs;
        }

        inline std::ostream& operator<<(std::ostream& os, const viz::data::Color& c)
        {
            return os << "(" << int(c.r) << " " << int(c.g) << " " << int(c.b)
                   << " | " << int(c.a) << ")";
        }
    }
}
