/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/ArViz/Client/elements/ElementOps.h>
#include <RobotAPI/interface/ArViz/Elements.h>

namespace armarx::viz
{
    class Line : public ElementOps<Line, data::ElementLine>
    {
    public:
        using ElementOps::ElementOps;

        Line& lineWidth(float w);

        Line& fromTo(Eigen::Vector3f from, Eigen::Vector3f to);
    };
} // namespace armarx::viz
