#pragma once

#include <RobotAPI/components/ArViz/Client/elements/ElementOps.h>
#include <RobotAPI/interface/ArViz/Component.h>

#include <Eigen/Core>

namespace armarx::viz
{
    enum class InteractionFeedbackType
    {
        /** Nothing happened. */
        None,

        /** An element was selected. */
        Select,
        /** An element was deselected. */
        Deselect,

        /** A context menu entry was chosen. */
        ContextMenuChosen,

        /** The element was transformed (translated or rotated). */
        Transform,
    };

    inline const char* toString(InteractionFeedbackType type)
    {
        switch (type)
        {
        case InteractionFeedbackType::None:
            return "None";
        case InteractionFeedbackType::Select:
            return "Select";
        case InteractionFeedbackType::Deselect:
            return "Deselect";
        case InteractionFeedbackType::ContextMenuChosen:
            return "ContextMenuChosen";
        case InteractionFeedbackType::Transform:
            return "Transform";
        default:
            return "<Unknown>";
        }
    }

    inline Eigen::Matrix4f toEigen(data::GlobalPose const& pose)
    {
        Eigen::Quaternionf ori(pose.qw, pose.qx, pose.qy, pose.qz);

        Eigen::Matrix4f result;
        result.block<3, 3>(0, 0) = ori.toRotationMatrix();
        result.block<3, 1>(0, 3) = Eigen::Vector3f(pose.x, pose.y, pose.z);
        result.block<1, 4>(3, 0) = Eigen::Vector4f(0.0f, 0.0f, 0.0f, 1.0f);
        return result;
    }

    struct InteractionFeedback
    {
        InteractionFeedbackType type() const
        {
            // Maks out all the flags in the higher bits
            int type = data_.type & 0x7;

            switch (type)
            {
            case data::InteractionFeedbackType::NONE:
                return InteractionFeedbackType::None;

            case data::InteractionFeedbackType::SELECT:
                return InteractionFeedbackType::Select;

            case data::InteractionFeedbackType::DESELECT:
                return InteractionFeedbackType::Deselect;

            case data::InteractionFeedbackType::CONTEXT_MENU_CHOSEN:
                return InteractionFeedbackType::ContextMenuChosen;

            case data::InteractionFeedbackType::TRANSFORM:
                return InteractionFeedbackType::Transform;

            default:
                throw std::runtime_error("Unexpected InteractionFeedbackType");
            }
        }

        bool isTransformBegin() const
        {
            return data_.type & data::InteractionFeedbackType::TRANSFORM_BEGIN_FLAG;
        }

        bool isTransformDuring() const
        {
            return data_.type & data::InteractionFeedbackType::TRANSFORM_DURING_FLAG;
        }

        bool isTransformEnd() const
        {
            return data_.type & data::InteractionFeedbackType::TRANSFORM_END_FLAG;
        }

        std::string const& layer() const
        {
            return data_.layer;
        }

        std::string const& element() const
        {
            return data_.element;
        }

        long revision() const
        {
            return data_.revision;
        }

        int chosenContextMenuEntry() const
        {
            return data_.chosenContextMenuEntry;
        }

        Eigen::Matrix4f transformation() const
        {
            return toEigen(data_.transformation);
        }

        Eigen::Vector3f scale() const
        {
            Eigen::Vector3f result(data_.scale.e0, data_.scale.e1, data_.scale.e2);
            return result;
        }

        data::InteractionFeedback data_;
    };

    struct InteractionFeedbackRange
    {
        InteractionFeedback const* begin() const
        {
            return begin_;
        }

        InteractionFeedback const* end() const
        {
            return end_;
        }

        std::size_t size() const
        {
            return end_ - begin_;
        }

        bool empty() const
        {
            return begin_ == end_;
        }

        InteractionFeedback const* begin_;
        InteractionFeedback const* end_;
    };

    struct TransformationResult
    {
        // Indicates that a transfomation was applied during the interaction
        bool wasTransformed = false;

        // Indicates that a layer update is needed
        // If this flag is set, you should commit the containing layer with the next arviz.commit()
        bool needsLayerUpdate = false;
    };

    template <typename ElementT>
    struct Transformable
    {
        Transformable(std::string const& name)
            : element(name)
        {
        }

        Transformable& pose(Eigen::Matrix4f const& pose)
        {
            element.pose(pose);
            initialPose = pose;
            return *this;
        }

        Transformable& position(Eigen::Vector3f const& position)
        {
            element.position(position);
            initialPose.block<3, 1>(0, 3) = position;
            return *this;
        }

        Transformable& orientation(Eigen::Matrix3f const& rotationMatrix)
        {
            element.orientation(rotationMatrix);
            initialPose.block<3, 3>(0, 0) = rotationMatrix;
            return *this;
        }

        Transformable& orientation(Eigen::Quaternionf const& quaternion)
        {
            element.orientation(quaternion);
            initialPose.block<3, 3>(0, 0) = quaternion.toRotationMatrix();
            return *this;
        }

        Transformable& enable(viz::InteractionDescription const& interaction)
        {
            element.enable(interaction);
            // A movable element is always hidden during the interaction
            element.data_->interaction.enableFlags |= viz::data::InteractionEnableFlags::TRANSFORM_HIDE;
            return *this;
        }

        // The pose after the current transformation has been applied
        Eigen::Matrix4f getCurrentPose() const
        {
            return transformation * initialPose;
        }

        // Returns true, if the element has been changed and the layer needs to be comitted again
        TransformationResult handle(viz::InteractionFeedback const& interaction)
        {
            TransformationResult result;
            if (interaction.element() == element.data_->id)
            {
                switch (interaction.type())
                {
                case viz::InteractionFeedbackType::Transform:
                {
                    // Keep track of the transformation
                    transformation = interaction.transformation();
                    result.wasTransformed = true;
                } break;
                case viz::InteractionFeedbackType::Deselect:
                {
                    // If an object is deselected, we apply the transformation
                    initialPose = transformation * initialPose;
                    transformation = Eigen::Matrix4f::Identity();
                    element.pose(initialPose);

                    // In this case, the user needs to commit the layer
                    result.needsLayerUpdate = true;
                } break;
                default:
                {
                    // Ignore the other events
                }
                }
            }
            return result;
        }


        Eigen::Matrix4f initialPose = Eigen::Matrix4f::Identity();
        Eigen::Matrix4f transformation = Eigen::Matrix4f::Identity();

        ElementT element;
    };

}
