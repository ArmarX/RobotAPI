#pragma once

#include <functional>
#include <vector>

#include "Layer.h"
#include "Interaction.h"


namespace armarx
{
    class Component;
    class ManagedIceObject;

namespace viz
{
    /**
     * A staged commit prepares multiple layers to be committed.
     *
     * Add all relevant layer updates via .add(layer).
     * Add layers for which you want interaction feedback via .requestInteraction(layer).
     * Then, commit via client.apply(stagedCommit).
     *
     * A staged commit can be reused for subsequent commits.
     * Remember to call .reset() to clear the internal data structures.
     *
     * As long as you keep the staged commits separate, this method is thread-safe.
     * So you can have two threads calling .commit(A) and .commit(B)
     * simultaneously without any locking mechanism.
     */
    struct StagedCommit
    {
        /**
         * @brief Stage a layer to be committed later via client.apply(*this)
         * @param layer The layer to be added to this staged commit.
         */
        void add(Layer const& layer)
        {
            data_.updates.push_back(layer.data_);
        }

        void add(std::initializer_list<Layer> layers)
        {
            data_.updates.reserve(data_.updates.size() + layers.size());
            for (Layer const& layer : layers)
            {
                data_.updates.push_back(layer.data_);
            }
        }

        /**
         * @brief Request interaction feedback for a particular layer.
         * @param layer The layer you want to get interaction feedback for.
         */
        void requestInteraction(Layer const& layer)
        {
            data_.interactionComponent = layer.data_.component;
            data_.interactionLayers.push_back(layer.data_.name);
        }

        /**
         * @brief Reset all staged layers and interaction requests.
         */
        void reset()
        {
            data_.updates.clear();
            data_.interactionComponent.clear();
            data_.interactionLayers.clear();
        }

        viz::data::CommitInput data_;
    };

    struct CommitResult
    {
        long revision() const
        {
            return data_.revision;
        }

        InteractionFeedbackRange interactions() const
        {
            InteractionFeedback* begin = (InteractionFeedback*) data_.interactions.data();
            InteractionFeedback* end = begin + data_.interactions.size();
            return InteractionFeedbackRange{begin, end};
        }

        data::CommitResult data_;
    };

    struct CommitResultAsync
    {
        bool isAvailable() const
        {
            return async && async->isCompleted();
        }

        CommitResult waitAndGet() const
        {
            CommitResult result;
            result.data_ = storage->end_commitAndReceiveInteractions(async);
            return result;
        }

        Ice::AsyncResultPtr async;
        armarx::viz::StorageInterfacePrx storage;
    };

    struct Client
    {
        Client() = default;
        Client(const Client&) = default;

        Client(armarx::Component& component,
               std::string const& topicNameProperty = "ArVizTopicName",
               std::string const& storageNameProperty = "ArVizStorageName");

        Client(ManagedIceObject& obj,
               std::string const& topicName = "ArVizTopic",
               std::string const& storageName = "ArVizStorage");

        static Client createFromTopic(std::string const& componentName,
                                      armarx::viz::Topic::ProxyType const& topic);

        static Client createFromProxies(std::string const& componentName,
                                        armarx::viz::Topic::ProxyType const& topic,
                                        armarx::viz::StorageAndTopicInterfacePrx const& storage);

        static Client createForGuiPlugin(armarx::Component& component,
                                         std::string const& topicName = "ArVizTopic",
                                         std::string const& storageName = "ArVizStorage");

        Layer layer(std::string const& name) const
        {
            return Layer(componentName, name);
        }

        StagedCommit stage()
        {
            return StagedCommit();
        }

        CommitResult commit(StagedCommit const& commit);

        CommitResultAsync commitAsync(StagedCommit const& commit);

        void commit(Layer const& layer)
        {
            std::vector<Layer> layers;
            layers.push_back(layer);
            commit(layers);
        }

        void commit(std::vector<Layer> const& layers);

        void commitLayerContaining(std::string const& name)
        {
            std::vector<viz::Layer> layers;
            layers.push_back(this->layer(name));
            commit(layers);
        }

        template <typename ElementT>
        void commitLayerContaining(std::string const& name, ElementT const& element)
        {
            std::vector<viz::Layer> layers;
            viz::Layer& newLayer = layers.emplace_back(this->layer(name));
            newLayer.add(element);
            commit(layers);
        }

        void commitDeleteLayer(std::string const& name)
        {
            std::vector<viz::Layer> layers;
            viz::Layer& layerToDelete = layers.emplace_back(this->layer(name));
            layerToDelete.markForDeletion();
            commit(layers);
        }

    private:
        std::string componentName;
        armarx::viz::StorageInterfacePrx storage;
        armarx::viz::TopicPrx topic;
    };

}
}
