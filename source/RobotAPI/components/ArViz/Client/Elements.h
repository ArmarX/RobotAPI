#pragma once






#include "elements/Color.h"
#include "elements/ElementOps.h"
#include "elements/Line.h"
#include "elements/Mesh.h"
#include "elements/Path.h"
#include "elements/PointCloud.h"
#include "elements/Robot.h"
//#include "elements/RobotHand.h"  // Not included by default (exposes additional headers).

#include <RobotAPI/interface/ArViz/Elements.h>

#include <SimoxUtility/shapes/OrientedBoxBase.h>

#include <Eigen/Geometry>
#include <Eigen/Core>

#include <ctime>
#include <string>

// The has_member macro causes compile errors if *any* other header uses
// the identifier has_member. Boost.Thread does, so this causes compile
// errors down the line.
// Offending file: simox/SimoxUtility/meta/has_member_macros/has_member.hpp
#ifdef has_member
#undef has_member
#endif


namespace Eigen
{
    using Hyperplane3f = Hyperplane<float, 3>;
}

namespace armarx
{
    // <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
    class ObjectID;
}

namespace armarx::viz
{
    using data::ColoredPoint;

    struct Box : ElementOps<Box, data::ElementBox>
    {
        using ElementOps::ElementOps;

        Box& size(Eigen::Vector3f const& s)
        {
            data_->size.e0 = s.x();
            data_->size.e1 = s.y();
            data_->size.e2 = s.z();

            return *this;
        }

        Box& size(float s)
        {
            return size(Eigen::Vector3f(s, s, s));
        }

        Box& set(const simox::OrientedBoxBase<float>& b);
        Box& set(const simox::OrientedBoxBase<double>& b);
    };


    struct Cylinder : ElementOps<Cylinder, data::ElementCylinder>
    {
        using ElementOps::ElementOps;

        Cylinder& radius(float r)
        {
            data_->radius = r;

            return *this;
        }

        Cylinder& height(float h)
        {
            data_->height = h;

            return *this;
        }

        Cylinder& fromTo(Eigen::Vector3f from, Eigen::Vector3f to);

        Cylinder& direction(Eigen::Vector3f direction);
    };


    struct Cylindroid : ElementOps<Cylindroid, data::ElementCylindroid>
    {
        Cylindroid(const std::string& name) :
            ElementOps(name)
        {
            data_->curvature.e0 = 1;
            data_->curvature.e1 = 1;
        }

        Cylindroid& height(float height)
        {
            data_->height = height;

            return *this;
        }

        Cylindroid& axisLengths(const Eigen::Vector2f& axisLengths)
        {
            data_->axisLengths.e0 = axisLengths.x();
            data_->axisLengths.e1 = axisLengths.y();

            return *this;
        }

        Cylindroid& curvature(const Eigen::Vector2f& curvature)
        {
            // Warning:  Custom curvatures are not yet supported by the visualization backend and
            // are thus ignored.
            data_->curvature.e0 = curvature.x();
            data_->curvature.e1 = curvature.y();

            return *this;
        }
    };


    struct Sphere : ElementOps<Sphere, data::ElementSphere>
    {
        using ElementOps::ElementOps;

        Sphere& radius(float r)
        {
            data_->radius = r;

            return *this;
        }
    };


    struct Ellipsoid : ElementOps<Ellipsoid, data::ElementEllipsoid>
    {
        Ellipsoid(const std::string& name) :
            ElementOps(name)
        {
            data_->curvature.e0 = 1;
            data_->curvature.e1 = 1;
            data_->curvature.e2 = 1;
        }

        Ellipsoid& axisLengths(const Eigen::Vector3f& axisLengths)
        {
            data_->axisLengths.e0 = axisLengths.x();
            data_->axisLengths.e1 = axisLengths.y();
            data_->axisLengths.e2 = axisLengths.z();

            return *this;
        }

        Ellipsoid& curvature(const Eigen::Vector3f& curvature)
        {
            // Warning:  Custom curvatures are not yet supported by the visualization backend and
            // are thus ignored.
            data_->curvature.e0 = curvature.x();
            data_->curvature.e1 = curvature.y();
            data_->curvature.e2 = curvature.z();

            return *this;
        }
    };


    struct Pose : ElementOps<Pose, data::ElementPose>
    {
        using ElementOps::ElementOps;
    };


    struct Text : ElementOps<Text, data::ElementText>
    {
        using ElementOps::ElementOps;

        Text& text(std::string const& t)
        {
            data_->text = t;

            return *this;
        }
    };


    struct Arrow : ElementOps<Arrow, data::ElementArrow>
    {
        using ElementOps::ElementOps;

        Arrow& direction(Eigen::Vector3f dir);

        Arrow& length(float l)
        {
            data_->length = l;

            return *this;
        }

        Arrow& width(float w)
        {
            data_->width = w;

            return *this;
        }

        Arrow& fromTo(const Eigen::Vector3f& from, const Eigen::Vector3f& to)
        {
            position(from);
            direction((to - from).normalized());
            length((to - from).norm());

            return *this;
        }
    };


    struct ArrowCircle : ElementOps<ArrowCircle, data::ElementArrowCircle>
    {
        using ElementOps::ElementOps;

        ArrowCircle& normal(Eigen::Vector3f dir);

        ArrowCircle& radius(float r)
        {
            data_->radius = r;

            return *this;
        }

        ArrowCircle& width(float w)
        {
            data_->width = w;

            return *this;
        }

        ArrowCircle& completion(float c)
        {
            data_->completion = c;

            return *this;
        }
    };


    struct Polygon : ElementOps<Polygon, data::ElementPolygon>
    {
        using ElementOps::ElementOps;

        Polygon& clear()
        {
            data_->points.clear();
            return *this;
        }

        Polygon& lineColor(Color color)
        {
            data_->lineColor = color;

            return *this;
        }

        Polygon& lineColor(int r, int g, int b)
        {
            return lineColor(viz::Color(r, g, b));
        }

        Polygon& lineColorGlasbeyLUT(std::size_t id, int alpha = 255)
        {
            return lineColor(Color::fromRGBA(simox::color::GlasbeyLUT::at(id, alpha)));
        }

        Polygon& lineWidth(float w)
        {
            data_->lineWidth = w;

            return *this;
        }

        Polygon& points(std::vector<Eigen::Vector3f> const& ps);

        Polygon& addPoint(Eigen::Vector3f p)
        {
            data_->points.push_back(armarx::Vector3f{p.x(), p.y(), p.z()});

            return *this;
        }

        /**
         * @brief Add points representing a plane as rectangle.
         * @param plane The plane.
         * @param at Center of rectangle, is projected onto plane.
         * @param size Extents of rectangle.
         */
        Polygon& plane(Eigen::Hyperplane3f plane, Eigen::Vector3f at, Eigen::Vector2f size);

        /**
         * @brief Add points representing the XY-plane of the given coordinate system as rectangle.
         * @param center The rectangle center.
         * @param orientation The orientation of the coordinate system.
         * @param size The XY-size of the rectangle.
         */
        Polygon& plane(Eigen::Vector3f center, Eigen::Quaternionf orientation, Eigen::Vector2f size);

        Polygon& circle(Eigen::Vector3f center, Eigen::Vector3f normal, float radius, std::size_t tessellation = 64);
    };


    struct Object : ElementOps<Object, data::ElementObject>
    {
        static const std::string DefaultObjectsPackage;
        static const std::string DefaultRelativeObjectsDirectory;

        using ElementOps::ElementOps;

        Object& file(std::string const& project, std::string const& filename)
        {
            data_->project = project;
            data_->filename = filename;

            return *this;
        }
        Object& file(std::string const& filename)
        {
            return file("", filename);
        }

        /**
         * @brief Set the file so it could be found using `armarx::ObjectFinder` (also on remote machine).
         * @param objectID The object ID, see <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
         * @param objectsPackage The objects package ("ArmarXObjects" by default)
         * @see <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>
         */
        Object& fileByObjectFinder(const armarx::ObjectID& objectID,
                                   const std::string& objectsPackage = DefaultObjectsPackage,
                                   const std::string& relativeObjectsDirectory = DefaultRelativeObjectsDirectory);
        Object& fileByObjectFinder(const std::string& objectID,
                                   const std::string& objectsPackage = DefaultObjectsPackage,
                                   const std::string& relativeObjectsDirectory = DefaultRelativeObjectsDirectory);

        Object& alpha(float alpha);

        Object& useCollisionModel()
        {
            data_->drawStyle |= data::ModelDrawStyle::COLLISION;

            return *this;
        }

        Object& useFullModel()
        {
            data_->drawStyle &= ~data::ModelDrawStyle::COLLISION;

            return *this;
        }

        Object& overrideColor(Color c)
        {
            data_->drawStyle |= data::ModelDrawStyle::OVERRIDE_COLOR;

            return color(c);
        }

        Object& useOriginalColor()
        {
            data_->drawStyle &= ~data::ModelDrawStyle::OVERRIDE_COLOR;

            return *this;
        }

    };

}
