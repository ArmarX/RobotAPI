#include "Client.h"

#include <ArmarXCore/core/Component.h>

namespace armarx::viz
{

Client::Client(Component& component, std::string const& topicNameProperty, std::string const& storageNameProperty)
{
    componentName = component.getName();
    component.getTopicFromProperty(topic, topicNameProperty);

    // Optional dependency on ArVizStorage
    std::string storageName;
    if (component.hasProperty(storageNameProperty))
    {
        storageName = component.getProperty<std::string>(storageNameProperty);
    }
    else
    {
        storageName = "ArVizStorage";
    }
    component.getProxy(storage, storageName);
}

Client::Client(ManagedIceObject& obj,
               std::string const& topicName,
               std::string const& storageName)
{
    componentName = obj.getName();
    obj.getTopic(topic, topicName);
    obj.getProxy(storage, storageName);
}

Client Client::createFromTopic(std::string const& componentName, Topic::ProxyType const& topic)
{
    Client client;
    client.componentName = componentName;
    client.topic = topic;
    return client;
}

Client Client::createFromProxies(std::string const& componentName,
                                 Topic::ProxyType const& topic,
                                 StorageAndTopicInterfacePrx const& storage)
{
    Client client;
    client.componentName = componentName;
    client.topic = topic;
    client.storage = storage;
    return client;
}

Client Client::createForGuiPlugin(Component& component,
                                  std::string const& topicName,
                                  std::string const& storageName)
{
    Client client;
    std::string name = component.getName();
    std::size_t dashPos = name.find('-');
    if (dashPos != std::string::npos)
    {
        name = name.substr(0, dashPos);
    }
    client.componentName = name;
    component.getTopic(client.topic, topicName);
    component.getProxy(client.storage, storageName);
    return client;
}

CommitResult Client::commit(const StagedCommit& commit)
{
    CommitResult result;

    ARMARX_CHECK_NOT_NULL(storage);
    result.data_ = storage->commitAndReceiveInteractions(commit.data_);
    return result;
}

CommitResultAsync Client::commitAsync(const StagedCommit& commit)
{
    CommitResultAsync result;

    ARMARX_CHECK_NOT_NULL(storage);
    result.async = storage->begin_commitAndReceiveInteractions(commit.data_);
    result.storage = storage;
    return result;
}

void Client::commit(const std::vector<Layer>& layers)
{
    data::LayerUpdateSeq updates;
    updates.reserve(layers.size());
    for (Layer const& layer : layers)
    {
        updates.push_back(layer.data_);
    }
    // This commit call still uses the legacy topic API
    ARMARX_CHECK_NOT_NULL(topic);
    topic->updateLayers(updates);
}



}
