#pragma once

#include "Elements.h"

#include <RobotAPI/interface/ArViz/Component.h>

#include <ArmarXCore/interface/core/BasicVectorTypes.h>

namespace armarx::viz
{

    struct Layer
    {
        Layer() = default;

        Layer(std::string const& component, std::string const& name)
        {
            data_.component = component;
            data_.name = name;
            data_.action = data::Layer_CREATE_OR_UPDATE;
        }

        void clear()
        {
            data_.elements.clear();
        }

        template <typename ElementT>
        void add(ElementT const& element)
        {
            data_.elements.push_back(element.data_);
        }

//        template <typename ElementT>
//        void add(std::vector<ElementT> const& elements)
//        {
//            for (ElementT const& e : elements)
//            {
//                add(e);
//            }
//        }

        void markForDeletion()
        {
            data_.action = data::LayerAction::Layer_DELETE;
        }

        std::size_t size() const noexcept
        {
            return data_.elements.size();
        }

        data::LayerUpdate data_;
    };

}
