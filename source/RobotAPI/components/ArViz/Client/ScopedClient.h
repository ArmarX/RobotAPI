/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <set>

#include "Client.h"

namespace armarx::viz
{
    /**
     * @brief `viz::Client` that will delete (clear) committed layers when destroyed.
     *
     * Note that, as a consequence, a network call will be performed in the destructor.
     *
     * This might be useful if you have a class `MyTask` perform visualizing while performing some task,
     * but whose visualization should be removed when the task finished.
     * In this case, `MyTask` can have a `viz::ScopedClient` (which can be created from a regular `viz::Client`).
     * When destructing the instance of `MyTask`, all visualization done by `MyTask` will be cleared
     * (as `viz::ScopedClient` will go out of scope as well).
     *
     */
    class ScopedClient: virtual public Client
    {
    public:
        using Client::Client;
        ScopedClient(const Client& client);

        Layer layer(std::string const& name) const;

        virtual ~ScopedClient();

    private:
        mutable std::set<std::string> layers;
    };
}  // namespace armarx::viz
