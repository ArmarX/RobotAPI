#pragma once

#include <stdexcept>

#include <SimoxUtility/json/json.hpp>


namespace armarx::viz::error
{

    /// Base exception class.
    class ArvizReflectionError : public std::runtime_error
    {
    public:
        ArvizReflectionError(const std::string& msg);
    };


    /// Indicates that a JSON document did not contain the type name.
    class NoTypeNameEntryInJsonObject : public ArvizReflectionError
    {
    public:
        NoTypeNameEntryInJsonObject(const std::string& missingKey, const nlohmann::json& j);
    private:
        static std::string makeMsg(const std::string& missingKey, const nlohmann::json& j);
    };


    /// The TypeNameEntryAlreadyInJsonObject class
    class TypeNameEntryAlreadyInJsonObject : public ArvizReflectionError
    {
    public:
        TypeNameEntryAlreadyInJsonObject(const std::string& key, const std::string& typeName,
                                         const nlohmann::json& j);
    private:
        static std::string makeMsg(const std::string& key, const std::string& typeName,
                                   const nlohmann::json& j);
    };


    /// Indicates that the type name in a JSON object did not match the type of the passed C++ object.
    class TypeNameMismatch : public ArvizReflectionError
    {
    public:
        TypeNameMismatch(const std::string& typeInJson, const std::string& typeOfObject);
    private:
        static std::string makeMsg(const std::string& typeInJson, const std::string& typeOfObject);
    };

    /// Indicates that there was no registered serializer for a type.
    class NoSerializerForType : public ArvizReflectionError
    {
    public:
        NoSerializerForType(const std::string& typeName);
    };


    /// Indicates that there already was a serializer registered for a type when trying to
    /// register a serializer
    class SerializerAlreadyRegisteredForType : public ArvizReflectionError
    {
    public:
        SerializerAlreadyRegisteredForType(const std::string& typeName,
                                           const std::vector<std::string>& acceptedTypes = {});
    private:
        static std::string makeMsg(const std::string& typeName, const std::vector<std::string>& acceptedTypes);
    };

}
