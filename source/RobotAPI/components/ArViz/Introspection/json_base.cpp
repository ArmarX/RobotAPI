#include "json_base.h"

#include <SimoxUtility/math/pose/pose.h>

#include "ElementJsonSerializers.h"


void armarx::to_json(nlohmann::json& j, const armarx::Vector2f& value)
{
    j["x"] = value.e0;
    j["y"] = value.e1;
}
void armarx::from_json(const nlohmann::json& j, armarx::Vector2f& value)
{
    value.e0 = j.at("x").get<float>();
    value.e1 = j.at("y").get<float>();
}


void armarx::to_json(nlohmann::json& j, const armarx::Vector3f& value)
{
    j["x"] = value.e0;
    j["y"] = value.e1;
    j["z"] = value.e2;
}
void armarx::from_json(const nlohmann::json& j, armarx::Vector3f& value)
{
    value.e0 = j.at("x").get<float>();
    value.e1 = j.at("y").get<float>();
    value.e2 = j.at("z").get<float>();
}


namespace armarx::viz
{
    void data::to_json(nlohmann::json& j, const data::GlobalPose& pose)
    {
        armarx::Vector3f pos;
        pos.e0 = pose.x;
        pos.e1 = pose.y;
        pos.e2 = pose.z;
        j["position"] = pos;

        j["orientation"] = Eigen::Quaternionf(pose.qw, pose.qx, pose.qy, pose.qz);

        j[json::meta::KEY]["orientation"] = json::meta::ORIENTATION;
    }
    void data::from_json(const nlohmann::json& j, data::GlobalPose& pose)
    {
        const armarx::Vector3f pos = j.at("position").get<armarx::Vector3f>();
        const Eigen::Quaternionf ori = j.at("orientation").get<Eigen::Quaternionf>();
        pose.x = pos.e0;
        pose.y = pos.e1;
        pose.z = pos.e2;
        pose.qw = ori.w();
        pose.qx = ori.x();
        pose.qy = ori.y();
        pose.qz = ori.z();
    }

    void data::to_json(nlohmann::json& j, const data::Color& color)
    {
        j["r"] = color.r;
        j["g"] = color.g;
        j["b"] = color.b;
        j["a"] = color.a;
    }
    void data::from_json(const nlohmann::json& j, data::Color& color)
    {
        color.r = j.at("r").get<Ice::Byte>();
        color.g = j.at("g").get<Ice::Byte>();
        color.b = j.at("b").get<Ice::Byte>();
        color.a = j.at("a").get<Ice::Byte>();
    }


    const std::string json::meta::KEY = "__meta__";

    const std::string json::meta::HIDE = "hide";
    const std::string json::meta::READ_ONLY = "read_only";

    const std::string json::meta::COLOR = "color";
    const std::string json::meta::ORIENTATION = "orientation";


    void json::to_json_base(nlohmann::json& j, const data::Element& element)
    {
        j["id"] = element.id;
        j["pose"] = element.pose;
        j["color"] = element.color;
        j["flags"] = element.flags;
        j["scale"] = element.scale;

        j[meta::KEY]["id"] = meta::HIDE;
        j[meta::KEY]["flags"] = meta::HIDE;
        j[meta::KEY]["color"] = meta::COLOR;
    }
    void json::from_json_base(const nlohmann::json& j, data::Element& element)
    {
        element.id = j.at("id").get<std::string>();
        element.pose = j.at("pose").get<data::GlobalPose>();
        element.color = j.at("color").get<data::Color>();
        element.flags = j.at("flags").get<int>();
        element.scale = j.at("scale").get<armarx::Vector3f>();
    }

}

