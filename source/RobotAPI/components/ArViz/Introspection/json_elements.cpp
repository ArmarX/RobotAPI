#include "json_elements.h"

#include <SimoxUtility/algorithm/string.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/interface/core/BasicVectorTypes.h>

#include "json_base.h"


namespace armarx::viz
{

    const simox::meta::IntEnumNames data::ElementFlags::names =
    {
        { NONE, "None" },
        { OVERRIDE_MATERIAL, "Override_Material" },
    };


    const simox::meta::IntEnumNames data::ModelDrawStyle::names =
    {
        { ORIGINAL, "Original" },
        { COLLISION, "Collision" },
        { OVERRIDE_COLOR, "Override_Color" },
    };


    void data::to_json(nlohmann::json& j, const ColoredPoint& coloredPoint)
    {
        j["x"] = coloredPoint.x;
        j["y"] = coloredPoint.y;
        j["z"] = coloredPoint.z;
        j["color"] = coloredPoint.color;
        j[json::meta::KEY]["color"] = json::meta::COLOR;
    }
    void data::from_json(const nlohmann::json& j, ColoredPoint& coloredPoint)
    {
        coloredPoint.x = j.at("x");
        coloredPoint.y = j.at("y");
        coloredPoint.z = j.at("z");
        coloredPoint.color = j.at("color");
    }


    void data::to_json(nlohmann::json& j, const ElementArrow& arrow)
    {
        json::to_json_base(j, arrow);
        j["length"] = arrow.length;
        j["width"] = arrow.width;
    }
    void data::from_json(const nlohmann::json& j, ElementArrow& arrow)
    {
        json::from_json_base(j, arrow);
        arrow.length = j.at("length");
        arrow.width = j.at("width");
    }


    void data::to_json(nlohmann::json& j, const ElementArrowCircle& arrowCircle)
    {
        json::to_json_base(j, arrowCircle);
        j["radius"] = arrowCircle.radius;
        j["completion"] = arrowCircle.completion;
        j["width"] = arrowCircle.width;
    }
    void data::from_json(const nlohmann::json& j, ElementArrowCircle& arrowCircle)
    {
        json::from_json_base(j, arrowCircle);
        arrowCircle.radius = j.at("radius");
        arrowCircle.completion = j.at("completion");
        arrowCircle.width = j.at("width");
    }


    void data::to_json(nlohmann::json& j, const ElementBox& box)
    {
        json::to_json_base(j, box);
        j["size"] = box.size;
    }
    void data::from_json(const nlohmann::json& j, ElementBox& box)
    {
        json::from_json_base(j, box);
        box.size = j.at("size").get<armarx::Vector3f>();
    }


    void data::to_json(nlohmann::json& j, const ElementCylinder& cylinder)
    {
        json::to_json_base(j, cylinder);
        j["height"] = cylinder.height;
        j["radius"] = cylinder.radius;
    }
    void data::from_json(const nlohmann::json& j, ElementCylinder& cylinder)
    {
        json::from_json_base(j, cylinder);
        cylinder.height = j.at("height");
        cylinder.radius = j.at("radius");
    }


    void data::to_json(nlohmann::json& j, const ElementCylindroid& cylindroid)
    {
        json::to_json_base(j, cylindroid);
        j["height"] = cylindroid.height;
        j["axisLengths"] = cylindroid.axisLengths;
        j["curvature"] = cylindroid.curvature;
    }
    void data::from_json(const nlohmann::json& j, ElementCylindroid& cylindroid)
    {
        json::from_json_base(j, cylindroid);
        cylindroid.height = j.at("height");
        cylindroid.axisLengths = j.at("axisLengths").get<armarx::Vector2f>();
        cylindroid.curvature = j.at("curvature").get<armarx::Vector2f>();
    }


    void data::to_json(nlohmann::json& j, const ElementLine& line)
    {
        json::to_json_base(j, line);
        j["from"] = line.from;
        j["to"] = line.to;
        j["lineWidth"] = line.lineWidth;
    }
    void data::from_json(const nlohmann::json& j, ElementLine& line)
    {
        json::from_json_base(j, line);
        line.from = j.at("from").get<armarx::Vector3f>();
        line.to = j.at("to").get<armarx::Vector3f>();
        line.lineWidth = j.at("lineWidth").get<float>();
    }


    void data::to_json(nlohmann::json& j, const ElementMesh& mesh)
    {
        json::to_json_base(j, mesh);
        j["# Vertices"] = mesh.vertices.size();
        j["# Colors"] = mesh.colors.size();
        j["# Faces"] = mesh.faces.size();

        j[json::meta::KEY]["# Vertices"] = json::meta::READ_ONLY;
        j[json::meta::KEY]["# Colors"] = json::meta::READ_ONLY;
        j[json::meta::KEY]["# Faces"] = json::meta::READ_ONLY;
    }
    void data::from_json(const nlohmann::json& j, ElementMesh& mesh)
    {
        json::from_json_base(j, mesh);
    }


    void data::to_json(nlohmann::json& j, const ElementPointCloud& pointCloud)
    {
        json::to_json_base(j, pointCloud);
        j["transparency"] = pointCloud.transparency;
        j["pointSizeInPixels"] = pointCloud.pointSizeInPixels;

        std::size_t numPoints = pointCloud.points.size() / sizeof (ColoredPoint);
        j["# Points"] = numPoints;
        j[json::meta::KEY]["# Points"] = json::meta::READ_ONLY;

        ColoredPoint const* begin = (ColoredPoint const*)pointCloud.points.data();
        ColoredPoint const* end = begin + std::min(std::size_t(10), numPoints);
        j["Points[0:10]"] = ColoredPointList(begin, end);
    }
    void data::from_json(const nlohmann::json& j, ElementPointCloud& pointCloud)
    {
        json::from_json_base(j, pointCloud);
        pointCloud.transparency = j.at("transparency");
        pointCloud.pointSizeInPixels = j.at("pointSizeInPixels");
    }


    void data::to_json(nlohmann::json& j, const ElementPolygon& polygon)
    {
        json::to_json_base(j, polygon);
        j["lineColor"] = polygon.lineColor;
        j[json::meta::KEY]["lineColor"] = json::meta::COLOR;
        j["lineWidth"] = polygon.lineWidth;
        j["points"] = polygon.points;
    }
    void data::from_json(const nlohmann::json& j, ElementPolygon& polygon)
    {
        json::from_json_base(j, polygon);
        polygon.lineColor = j.at("lineColor");
        polygon.lineWidth = j.at("lineWidth");
        polygon.points = j.at("points").get<std::vector<armarx::Vector3f>>();
    }


    void data::to_json(nlohmann::json& j, const ElementPose& pose)
    {
        json::to_json_base(j, pose);

    }
    void data::from_json(const nlohmann::json& j, ElementPose& pose)
    {
        json::from_json_base(j, pose);
    }

    void data::to_json(nlohmann::json& j, const ElementPath& path)
    {
        json::to_json_base(j, path);
        j["points"] = path.points;
    }

    void data::from_json(const nlohmann::json& j, ElementPath& path)
    {
        json::from_json_base(j, path);
        path.points = j.at("points").get<armarx::Vector3fSeq>();
    }


    void data::to_json(nlohmann::json& j, const ElementSphere& sphere)
    {
        json::to_json_base(j, sphere);
        j["radius"] = sphere.radius;
    }
    void data::from_json(const nlohmann::json& j, ElementSphere& sphere)
    {
        json::from_json_base(j, sphere);
        sphere.radius = j.at("radius");
    }


    void data::to_json(nlohmann::json& j, const ElementEllipsoid& ellipsoid)
    {
        json::to_json_base(j, ellipsoid);
        j["axisLengths"] = ellipsoid.axisLengths;
        j["curvature"] = ellipsoid.curvature;
    }
    void data::from_json(const nlohmann::json& j, ElementEllipsoid& ellipsoid)
    {
        json::from_json_base(j, ellipsoid);
        ellipsoid.axisLengths = j.at("axisLengths");
        ellipsoid.curvature = j.at("curvature");
    }


    void data::to_json(nlohmann::json& j, const ElementText& text)
    {
        json::to_json_base(j, text);
        j["text"] = text.text;
    }
    void data::from_json(const nlohmann::json& j, ElementText& text)
    {
        json::from_json_base(j, text);
        text.text = j.at("text");
    }


    namespace data::ModelDrawStyle
    {
        std::string to_flag_names(const int drawStyle)
        {
            std::vector<std::string> flag_names;
            for (int flag : names.values())
            {
                if (drawStyle == flag  // Necessary if flag is 0
                    || drawStyle & flag)
                {
                    flag_names.push_back(names.to_name(flag));
                }
            }
            return simox::alg::join(flag_names, " | ");
        }

        int from_flag_names(const std::string& flagString)
        {
            bool trim_elements = true;
            std::vector<std::string> split = simox::alg::split(flagString, "|", trim_elements);
            int flag = 0;
            for (auto& s : split)
            {
                flag |= names.from_name(s);
            }
            return flag;
        }
    }


    void data::to_json(nlohmann::json& j, const ElementObject& object)
    {
        json::to_json_base(j, object);
        j["project"] = object.project;
        j["filename"] = object.filename;

        j["drawStyle"] = ModelDrawStyle::to_flag_names(object.drawStyle) + " (" + std::to_string(object.drawStyle) + ")";
        j[json::meta::KEY]["drawStyle"] = json::meta::READ_ONLY;
    }
    void data::from_json(const nlohmann::json& j, ElementObject& object)
    {
        json::from_json_base(j, object);
        object.project = j.at("project");
        object.filename = j.at("filename");
        object.drawStyle = ModelDrawStyle::from_flag_names(j.at("drawStyle"));
    }


    void data::to_json(nlohmann::json& j, const ElementRobot& robot)
    {
        json::to_json_base(j, robot);
        j["project"] = robot.project;
        j["filename"] = robot.filename;
        j["jointValues"] = robot.jointValues;

        j["drawStyle"] = ModelDrawStyle::to_flag_names(robot.drawStyle);
        j[json::meta::KEY]["drawStyle"] = json::meta::READ_ONLY;
    }
    void data::from_json(const nlohmann::json& j, ElementRobot& robot)
    {
        json::from_json_base(j, robot);
        robot.project = j.at("project");
        robot.filename = j.at("filename");
        robot.jointValues = j.at("jointValues").get<armarx::StringFloatDictionary>();
        robot.drawStyle = ModelDrawStyle::from_flag_names(j.at("drawStyle"));
    }

}
