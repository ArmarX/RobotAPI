#pragma once

#include <functional>

#include <SimoxUtility/json.h>
#include <SimoxUtility/meta/type_name.h>

#include <RobotAPI/interface/ArViz/Elements.h>

#include "exceptions.h"


namespace armarx::viz::data
{
    /// @see `ElementSerializers::to_json()`
    void to_json(nlohmann::json& j, const Element& element);
    /// @see `ElementSerializers::from_json()`
    void from_json(const nlohmann::json& j, Element& element);

    /// @see `ElementSerializers::to_json()`
    void to_json(nlohmann::json& j, const ElementPtr& elementPtr);
    /// @see `ElementSerializers::from_json()`
    void from_json(const nlohmann::json& j, ElementPtr& elementPtr);

    /// @see `ElementSerializers::to_json()`
    void to_json(nlohmann::json& j, const Element* elementPtr);
}


namespace armarx::viz::json
{
    /**
     * @brief Get the type name stored in `j`.
     * @param key The key of the type name entry.
     * @throw `error::NoTypeNameEntryInJsonObject`
     *      If `j` is not an object or does not contain `key`.
     */
    std::string getTypeName(const nlohmann::json& j, const std::string& key);


    /**
     * @brief Store the type name in `j`.
     * @param key The key where the type name shall be stored.
     * @param typeName The type name.
     *
     * @throws `error::TypeNameEntryAlreadyInJsonObject`
     *      If `j` already contains `key`.
     */
    void setTypeName(nlohmann::json& j, const std::string& key, const std::string& typeName);


    /**
     * A raw function pointer to a function with signature:
     * @code
     * void to_json(nlohmann::json& j, const Value& v);
     * @endcode
     */
    template <class ValueT>
    using RawToJsonFn = void (*)(nlohmann::json& j, const ValueT& v);
    /**
     * A raw function pointer to a function with signature:
     * @code
     * void from_json(const nlohmann::json& j, Value& v);
     * @endcode
     */
    template <class ValueT>
    using RawFromJsonFn = void (*)(const nlohmann::json& j, ValueT& v);


    /**
     * A `std::function` pointer to a function with signature:
     * @code
     * void to_json(nlohmann::json& j, const Value& v);
     * @endcode
     */
    template <class ValueT>
    using ToJsonFn = std::function<void(nlohmann::json& j, const ValueT& v)>;
    /**
     * A `std::function` pointer to a function with signature:
     * @code
     * void from_json(const nlohmann::json& j, Value& v);
     * @endcode
     */
    template <class ValueT>
    using FromJsonFn = std::function<void(const nlohmann::json& j, ValueT& v)>;


    /**
     * @brief Handles serialization and deserialization of dynamic `data::Element`
     * objects to and from JSON.
     *
     * This class allows serialization of newly defined types through the
     * standard `nlohmann::json` interface, which builds upon user-defined
     * `to_json()` and `from_json()` functions for a user-defined type.
     *
     * @section How to register your custom data::Element type
     *
     * Suppose you have a custom class deriving from `semrel::data::Element` in a
     * namespace `myelement`, along with serializing functions following the
     * standard pattern employed by `nlohmann::json`:
     * @code
     * namespace myelement
     * {
     *     class Mydata::Element : public semrel::data::Element { ... };
     *
     *     void to_json(nlohmann::json& j, const Mydata::Element& element);
     *     void from_json(const nlohmann::json& j, Mydata::Element& element);
     * }
     * @endcode
     *
     * To enable serialization through the general serialization functions
     * for `data::Element`, register your functions to `ElementSerializers`, specifying
     * your custom type as template argument:
     * @code
     * // in namespace myelement
     * json::ElementSerializers::registerSerializer<Mydata::Element>(to_json, from_json);
     * @endcode
     * You can also specify the namespace, like in `myelement::to_json`.
     *
     * To make sure the serializer is always registered, you can create a
     * static variable (extern free or a static class member) whose
     * initialization will register the serializers, e.g.:
     * @code
     * // In the header file:
     * namespace myelement
     * {
     *     // Extern free or static member variable.
     *     extern const int _MY_SHAPE_JSON_REGISTRATION;
     * }
     *
     * // In the sourcer file:
     * const int myelement::_MY_SHAPE_JSON_REGISTRATION = []()
     * {
     *     // Register serializer when initializing variable.
     *     json::ElementSerializers::registerSerializer<Mydata::Element>(to_json, from_json);
     *     return 0;
     * }();
     * @endcode
     *
     *
     * @section How it is done
     *
     * `ElementSerializers` has a global map, which maps a (demangled) type name
     * to the respective serializer. When serializing an object of type
     * `data::Element`, the map is searched for an entry for the instance's dynamic
     * type. If one is found, the registered `to_json()` is used to write the
     * JSON document. In addition, a type name entry specifying the (demangled)
     * derived type name is stored (under the key
     * `ElementSerializers::JSON_TYPE_NAME_KEY`).
     *
     * When deserializing from a JSON document, the type name entry is used to
     * determine the correct serializer, which casts the to-be-deserialized
     * `data::Element` instance to the derived type and passes it to the registered
     * `from_json` method. When deserializing into a `data::ElementPtr`, the pointer
     * is allocated a new instance of the deriving type first.
     */
    class ElementJsonSerializers
    {
    public:

        /// JSON key under which demangled type name is stored.
        static const std::string JSON_TYPE_NAME_KEY;


    public:

        // PUBLIC STATIC INTERFACE

        /**
         * @brief Register a JSON seralizer for `DerivedElement`.
         *
         * Can be called with raw function pointers with automatic type deduction e.g.:
         *
         * @code
         * namespace my_element {
         *     // Standard serialization methods.
         *     void to_json(nlohmann::json& j, const Mydata::Element& box);
         *     void from_json(const nlohmann::json& j, Mydata::Element& box);
         * }
         *
         * void register() {
         *     // Register serializer:
         *     registerSerializer<my_element::Mydata::Element>(my_element::to_json, my_element::from_json);
         * }
         * @endcode
         *
         * @throw `error::SerializerAlreadyRegisteredForType`
         *      If there already is a registered serializer for that type.
         */
        template <class DerivedElement>
        static void registerSerializer(RawToJsonFn<DerivedElement> to_json,
                                       RawFromJsonFn<DerivedElement> from_json,
                                       bool overwrite = false)
        {
            registerSerializer<DerivedElement>(ToJsonFn<DerivedElement>(to_json), FromJsonFn<DerivedElement>(from_json), overwrite);
        }

        /**
         * @brief Register a JSON seralizer for `DerivedElement`.
         *
         * Can be called with `std::function` objects, e.g. lambdas:
         * @code
         * // Capture `serializer` by reference.
         * registerSerializer<Box>(
         *      [&](nlohmann::json& j, const Box& box) { myserializer.to_json(j, box); },
         *      [&](const nlohmann::json& j, Box& box) { myserializer.from_json(j, cyl); }
         * );
         * @endcode
         *
         * @throw `error::SerializerAlreadyRegisteredForType`
         *      If there already is a registered serializer for that type.
         */
        template <class DerivedElement>
        static void registerSerializer(ToJsonFn<DerivedElement> to_json,
                                       FromJsonFn<DerivedElement> from_json,
                                       bool overwrite = false)
        {
            _instance._registerSerializer<DerivedElement>(to_json, from_json, overwrite);
        }

        /**
         * Remove a registered serializer for `DerivedElement`.
         */
        template <class DerivedElement>
        static void removeSerializer()
        {
            _instance._removeSerializer<DerivedElement>();
        }


        /// Get the type names for which serializers are registered.
        static std::vector<std::string> getRegisteredTypes();

        /// Indicates whether there is a serializer registered for the given type name.
        static bool isTypeRegistered(const std::string& typeName);


        /**
         * @brief Serialize `element` to JSON according to its dynamic type.
         * @throw `error::NoSerializerForType` If there is no serializer for the given name.
         */
        static void to_json(nlohmann::json& j, const data::Element& element);
        /**
         * @brief Deserialize `element` from JSON according to its dynamic type.
         * @throws `error::NoTypeNameEntryInJsonObject`
         *      If `j` does not contain the key `JSON_TYPE_NAME_KEY` (or is not a JSON object).
         * @throws `error::TypeNameMismatch`
         *      If the type name in `j` does not match `element`'s dynamic type.
         * @throws `error::NoSerializerForType`
         *      If there is no serializer for the given name.
         */
        static void from_json(const nlohmann::json& j, data::Element& element);

        /**
         * @brief Serialize `element` to JSON according to its dynamic type.
         * @throw `error::data::ElementPointerIsNull` If `elementPtr` is null.
         * @throw `error::NoSerializerForType`
         *      If there is no serializer for the element type.
         * @see `to_json(nlohmann::json& j, const data::Element& element)`
         */
        static void to_json(nlohmann::json& j, const data::Element* elementPtr);
        /**
         * @brief Deserialize `elementPtr` from JSON according the type name in `j`.
         * If there is a registered serializer for the type name in `j`,
         * assigns a new instance of the dynamic type to `elementPtr` and
         * deserializes the created instance from `j`.
         *        /// Get the accepted demangled type names.

         * @throws `error::NoTypeNameEntryInJsonObject`
         *      If `j` does not contain the key `JSON_TYPE_NAME_KEY` (or is not a JSON object).
         * @throw `error::NoSerializerForType`
         *      If there is no serializer for the type in `j`.
         */
        static void from_json(const nlohmann::json& j, data::ElementPtr& elementPtr);

        static std::string getTypeName(const nlohmann::json& j);


    private:

        // PRIVATE STATIC INTERFACE

        /// A serializer for a specific derived `data::Element` type.
        struct ElementJsonSerializer
        {
            template <class DerivedElement>
            ElementJsonSerializer(ToJsonFn<DerivedElement> to_json,
                                  FromJsonFn<DerivedElement> from_json)
            {
                _to_json = [to_json](nlohmann::json & j, const data::Element & element)
                {
                    to_json(j, *dynamic_cast<const DerivedElement*>(&element));
                    setTypeName(j, JSON_TYPE_NAME_KEY, simox::meta::get_type_name<DerivedElement>());
                };
                _from_json = [this, from_json](const nlohmann::json & j, data::Element & element)
                {
                    from_json(j, *dynamic_cast<DerivedElement*>(&element));
                };
                _from_json_ptr = [this, from_json](const nlohmann::json & j, data::ElementPtr & elementPtr)
                {
                    // Referencing this->from_json() here causes a memory access violation.
                    // Therefore we use the from_json argument.
                    elementPtr = { new DerivedElement() };
                    from_json(j, *dynamic_cast<DerivedElement*>(elementPtr.get()));
                    // this->_from_json(j, *elementPtr));  // Causes memory access violation.
                };
            }

            void to_json(nlohmann::json& j, const data::Element& element);
            void from_json(const nlohmann::json& j, data::Element& element);

            void to_json(nlohmann::json& j, const data::ElementPtr& elementPtr);
            void from_json(const nlohmann::json& j, data::ElementPtr& elementPtr);


        public:

            ToJsonFn<data::Element> _to_json;
            FromJsonFn<data::Element> _from_json;
            FromJsonFn<data::ElementPtr> _from_json_ptr;

        };


        static ElementJsonSerializer& getSerializer(const nlohmann::json& j);
        static ElementJsonSerializer& getSerializer(const std::string& demangledTypeName);


        /// The instance, holding the registered serializers.
        static ElementJsonSerializers _instance;

        static void registerElements();


    private:

        // PRIVATE NON-STATIC INTERFACE

        /**
         * @brief Construct the instance.
         *
         * When initialized, serializers for builtin types are registered
         * via `registerElements()`.
         */
        ElementJsonSerializers();


        /**
         * @brief Register a serializer for `DerivedElement`.
         * @throw `error::SerializerAlreadyRegisteredForType`
         *      If there already is a registered serializer for that type.
         */
        template <class DerivedElement>
        void _registerSerializer(ToJsonFn<DerivedElement> to_json,
                                 FromJsonFn<DerivedElement> from_json,
                                 bool overwrite)
        {
            const std::string typeName = simox::meta::get_type_name<DerivedElement>();
            if (!overwrite && _serializers.count(typeName))
            {
                throw error::SerializerAlreadyRegisteredForType(typeName, getRegisteredTypes());
            }
            _serializers.emplace(typeName, ElementJsonSerializer(to_json, from_json));
        }

        /**
         * @brief Remove the serializer for `DerivedElement`.
         */
        template <class DerivedElement>
        void _removeSerializer()
        {
            _serializers.erase(simox::meta::get_type_name<DerivedElement>());
        }


        /**
         * @brief Get the serializer for the given demangled type name.
         * @throw `error::NoSerializerForType` If there is no serializer for the given name.
         */
        ElementJsonSerializer& _getSerializer(const std::string& demangledTypeName);

        /// Get the type names for which serializers are registered.
        std::vector<std::string> _getRegisteredTypes() const;

        /// Indicates whether there is a serializer registered for the given type name.
        bool _isTypeRegistered(const std::string& typeName) const;


        /// The serializers. Map of demangled type name to serializer.
        std::map<std::string, ElementJsonSerializer> _serializers;

    };

}


