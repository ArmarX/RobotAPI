#pragma once

#include <SimoxUtility/json.h>
#include <SimoxUtility/meta/EnumNames.hpp>

#include <RobotAPI/interface/ArViz/Elements.h>


namespace armarx::viz::data
{

    namespace ElementFlags
    {
        extern const simox::meta::IntEnumNames names;
    }

    namespace ModelDrawStyle
    {
        extern const simox::meta::IntEnumNames names;
    }


    void to_json(nlohmann::json& j, const ColoredPoint& coloredPoint);
    void from_json(const nlohmann::json& j, ColoredPoint& coloredPoint);


    // Elements

    void to_json(nlohmann::json& j, const ElementArrow& line);
    void from_json(const nlohmann::json& j, ElementArrow& line);


    void to_json(nlohmann::json& j, const ElementArrowCircle& arrowCircle);
    void from_json(const nlohmann::json& j, ElementArrowCircle& arrowCircle);


    void to_json(nlohmann::json& j, const ElementBox& box);
    void from_json(const nlohmann::json& j, ElementBox& box);


    void to_json(nlohmann::json& j, const ElementCylinder& cylinder);
    void from_json(const nlohmann::json& j, ElementCylinder& cylinder);


    void to_json(nlohmann::json& j, const ElementCylindroid& cylindroid);
    void from_json(const nlohmann::json& j, ElementCylindroid& cylindroid);


    void to_json(nlohmann::json& j, const ElementLine& line);
    void from_json(const nlohmann::json& j, ElementLine& line);


    void to_json(nlohmann::json& j, const ElementMesh& mesh);
    void from_json(const nlohmann::json& j, ElementMesh& mesh);


    void to_json(nlohmann::json& j, const ElementObject& object);
    void from_json(const nlohmann::json& j, ElementObject& object);


    void to_json(nlohmann::json& j, const ElementPointCloud& pointCloud);
    void from_json(const nlohmann::json& j, ElementPointCloud& pointCloud);


    void to_json(nlohmann::json& j, const ElementPolygon& polygon);
    void from_json(const nlohmann::json& j, ElementPolygon& polygon);


    void to_json(nlohmann::json& j, const ElementPose& pose);
    void from_json(const nlohmann::json& j, ElementPose& pose);


    void to_json(nlohmann::json& j, const ElementPath& path);
    void from_json(const nlohmann::json& j, ElementPath& path);


    void to_json(nlohmann::json& j, const ElementRobot& robot);
    void from_json(const nlohmann::json& j, ElementRobot& robot);


    void to_json(nlohmann::json& j, const ElementSphere& sphere);
    void from_json(const nlohmann::json& j, ElementSphere& sphere);


    void to_json(nlohmann::json& j, const ElementEllipsoid& sphere);
    void from_json(const nlohmann::json& j, ElementEllipsoid& sphere);


    void to_json(nlohmann::json& j, const ElementText& text);
    void from_json(const nlohmann::json& j, ElementText& text);

}
