#include <RobotAPI/interface/ArViz/Elements.h>
#include "ElementJsonSerializers.h"
#include "json_elements.h"


void armarx::viz::json::ElementJsonSerializers::registerElements()
{
    registerSerializer<data::ElementArrow>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementArrowCircle>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementBox>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementCylinder>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementCylindroid>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementLine>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementMesh>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementObject>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementPointCloud>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementPolygon>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementPose>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementPath>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementRobot>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementSphere>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementEllipsoid>(viz::data::to_json, viz::data::from_json);
    registerSerializer<data::ElementText>(viz::data::to_json, viz::data::from_json);
}
