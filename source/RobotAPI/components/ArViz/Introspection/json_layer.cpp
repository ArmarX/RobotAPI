#include "json_layer.h"

#include "json_base.h"

#include "ElementJsonSerializers.h"


namespace armarx::viz
{

    void data::to_json(nlohmann::json& j, const LayerUpdate& update)
    {
        j["component"] = update.component;
        j["name"] = update.name;
        j["action"] = update.action;
        j["elements"] = update.elements;
    }
    void data::from_json(const nlohmann::json& j, LayerUpdate& update)
    {
        update.component = j.at("component");
        update.name = j.at("name");
        update.action = j.at("action");
        update.elements = j.at("elements").get<ElementSeq>();
    }

    void data::to_json(nlohmann::json& j, const LayerUpdates& updates)
    {
        j["updates"] = updates.updates;
        j["revision"] = updates.revision;
    }
    void data::from_json(const nlohmann::json& j, LayerUpdates& updates)
    {
        updates.updates = j.at("updates").get<data::LayerUpdateSeq>();
        updates.revision = j.at("revision").get<Ice::Long>();
    }

}

