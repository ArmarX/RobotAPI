#pragma once

#include <SimoxUtility/json.h>

#include <RobotAPI/interface/ArViz/Elements.h>


namespace armarx
{

}


namespace armarx
{
    void to_json(nlohmann::json& j, const Vector2f& value);
    void from_json(const nlohmann::json& j, Vector2f& value);

    void to_json(nlohmann::json& j, const Vector3f& value);
    void from_json(const nlohmann::json& j, Vector3f& value);
}


namespace armarx::viz::data
{
    void to_json(nlohmann::json& j, const data::GlobalPose& value);
    void from_json(const nlohmann::json& j, data::GlobalPose& value);

    void to_json(nlohmann::json& j, const data::Color& color);
    void from_json(const nlohmann::json& j, data::Color& color);
}


namespace armarx::viz::json
{

    namespace meta
    {
        extern const std::string KEY;

        /// Do not show.
        extern const std::string HIDE;
        /// Make read-only.
        extern const std::string READ_ONLY;

        // Special edit modes.

        /// Use a color picker.
        extern const std::string COLOR;

        // extern const std::string POSE;
        // extern const std::string POSITION;
        extern const std::string ORIENTATION;
    }


    void to_json_base(nlohmann::json& j, const data::Element& element);
    void from_json_base(const nlohmann::json& j, data::Element& value);

}

