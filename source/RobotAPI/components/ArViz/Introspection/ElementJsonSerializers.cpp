#include "ElementJsonSerializers.h"


namespace armarx::viz
{
    std::string json::getTypeName(const nlohmann::json& j, const std::string& key)
    {
        try
        {
            return j.at(key);
        }
        catch (const nlohmann::detail::out_of_range&)
        {
            throw error::NoTypeNameEntryInJsonObject(key, j);
        }
        catch (const nlohmann::detail::type_error&)
        {
            throw error::NoTypeNameEntryInJsonObject(key, j);
        }
    }

    void json::setTypeName(nlohmann::json& j, const std::string& key, const std::string& typeName)
    {
        if (j.count(key) > 0)
        {
            throw "todo"; //error::TypeNameEntryAlreadyInJsonObject(key, typeName, j);
        }
        j[key] = typeName;
    }

    void data::to_json(nlohmann::json& j, const Element& element)
    {
        json::ElementJsonSerializers::to_json(j, element);
    }

    void data::from_json(const nlohmann::json& j, Element& element)
    {
        json::ElementJsonSerializers::from_json(j, element);
    }

    void data::to_json(nlohmann::json& j, const ElementPtr& shapePtr)
    {
        json::ElementJsonSerializers::to_json(j, shapePtr.get());
    }

    void data::from_json(const nlohmann::json& j, ElementPtr& shapePtr)
    {
        json::ElementJsonSerializers::from_json(j, shapePtr);
    }

    void data::to_json(nlohmann::json& j, const Element* shapePtr)
    {
        json::ElementJsonSerializers::to_json(j, shapePtr);
    }
}





namespace armarx::viz::json
{

    const std::string ElementJsonSerializers::JSON_TYPE_NAME_KEY = "__type__";

    ElementJsonSerializers ElementJsonSerializers::_instance = {};


    void ElementJsonSerializers::ElementJsonSerializer::to_json(nlohmann::json& j, const data::Element& element)
    {
        _to_json(j, element);
    }

    void ElementJsonSerializers::ElementJsonSerializer::from_json(const nlohmann::json& j, data::Element& element)
    {
        _from_json(j, element);
    }

    void ElementJsonSerializers::ElementJsonSerializer::to_json(nlohmann::json& j, const data::ElementPtr& shapePtr)
    {
        _to_json(j, *shapePtr);
    }

    void ElementJsonSerializers::ElementJsonSerializer::from_json(const nlohmann::json& j, data::ElementPtr& shapePtr)
    {
        _from_json_ptr(j, shapePtr);
    }

    ElementJsonSerializers::ElementJsonSerializer& ElementJsonSerializers::getSerializer(const nlohmann::json& j)
    {
        return _instance._getSerializer(getTypeName(j));
    }

    ElementJsonSerializers::ElementJsonSerializer& ElementJsonSerializers::getSerializer(const std::string& demangledTypeName)
    {
        return _instance._getSerializer(demangledTypeName);
    }

    std::vector<std::string> ElementJsonSerializers::getRegisteredTypes()
    {
        return _instance._getRegisteredTypes();
    }

    bool ElementJsonSerializers::isTypeRegistered(const std::string& typeName)
    {
        return _instance._isTypeRegistered(typeName);
    }

    void ElementJsonSerializers::to_json(nlohmann::json& j, const data::Element& element)
    {
        return getSerializer(simox::meta::get_type_name(element)).to_json(j, element);
    }

    void ElementJsonSerializers::from_json(const nlohmann::json& j, data::Element& element)
    {
        const std::string typeName = getTypeName(j);
        if (typeName != simox::meta::get_type_name(element))
        {
            throw error::TypeNameMismatch(typeName, simox::meta::get_type_name(element));
        }
        getSerializer(typeName).from_json(j, element);
    }

    void ElementJsonSerializers::to_json(nlohmann::json& j, const data::Element* elementPtr)
    {
        if (!elementPtr)
        {
            throw error::ArvizReflectionError("data::Element* is null in ElementJsonSerializers::to_json().");
        }
        to_json(j, *elementPtr);
    }

    void ElementJsonSerializers::from_json(const nlohmann::json& j, data::ElementPtr& shapePtr)
    {
        getSerializer(j).from_json(j, shapePtr);
    }

    std::string ElementJsonSerializers::getTypeName(const nlohmann::json& j)
    {
        return json::getTypeName(j, JSON_TYPE_NAME_KEY);
    }

    ElementJsonSerializers::ElementJsonSerializers()
    {
        registerElements();
    }


    ElementJsonSerializers::ElementJsonSerializer& ElementJsonSerializers::_getSerializer(
        const std::string& demangledTypeName)
    {
        if (auto find = _serializers.find(demangledTypeName); find != _serializers.end())
        {
            return find->second;
        }
        else
        {
            throw error::NoSerializerForType(demangledTypeName);
        }
    }

    std::vector<std::string> ElementJsonSerializers::_getRegisteredTypes() const
    {
        std::vector<std::string> types;
        for (const auto& [typeName, serializer] : _serializers)
        {
            types.push_back(typeName);
        }
        return types;
    }

    bool ElementJsonSerializers::_isTypeRegistered(const std::string& typeName) const
    {
        return _serializers.count(typeName) > 0;
    }



}
