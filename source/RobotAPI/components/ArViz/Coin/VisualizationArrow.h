#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTranslation.h>

namespace armarx::viz::coin
{
    struct VisualizationArrow : TypedElementVisualization<SoSeparator>
    {
        using ElementType = data::ElementArrow;

        VisualizationArrow()
        {
            tr = new SoTranslation;
            c = new SoCylinder();
            transl = new SoTranslation;
            cone = new SoCone();

            node->addChild(tr);
            node->addChild(c);
            node->addChild(transl);
            node->addChild(cone);
        }

        bool update(ElementType const& element)
        {
            float coneHeight = element.width * 6.0f;
            float coneBottomRadius = element.width * 2.5f;
            float baseLength = element.length - coneHeight;
            baseLength = std::max(0.0f, baseLength);

            tr->translation.setValue(0, baseLength * 0.5f, 0);

            c->radius = element.width;
            c->height = baseLength;

            transl->translation.setValue(0, element.length * 0.5f, 0);

            cone->bottomRadius.setValue(coneBottomRadius);
            cone->height.setValue(coneHeight);

            return true;
        }

        SoTranslation* tr;
        SoCylinder* c;
        SoTranslation* transl;
        SoCone* cone;
    };
}
