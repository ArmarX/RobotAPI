#include "Visualizer.h"

#include "VisualizationBox.h"
#include "VisualizationCylinder.h"
#include "VisualizationCylindroid.h"
#include "VisualizationSphere.h"
#include "VisualizationEllipsoid.h"
#include "VisualizationPose.h"
#include "VisualizationLine.h"
#include "VisualizationText.h"
#include "VisualizationArrow.h"
#include "VisualizationArrowCircle.h"
#include "VisualizationPointCloud.h"
#include "VisualizationPolygon.h"
#include "VisualizationMesh.h"
#include "VisualizationRobot.h"
#include "VisualizationObject.h"
#include "VisualizationPath.h"


void armarx::viz::CoinVisualizer::registerVisualizationTypes()
{
    using namespace armarx::viz::coin;

    elementVisualizersTypes.reserve(16);
    elementVisualizers.reserve(16);

    registerVisualizerFor<VisualizationBox>();
    registerVisualizerFor<VisualizationCylinder>();
    registerVisualizerFor<VisualizationCylindroid>();
    registerVisualizerFor<VisualizationSphere>();
    registerVisualizerFor<VisualizationEllipsoid>();
    registerVisualizerFor<VisualizationPose>();
    registerVisualizerFor<VisualizationLine>();
    registerVisualizerFor<VisualizationText>();
    registerVisualizerFor<VisualizationArrow>();
    registerVisualizerFor<VisualizationArrowCircle>();
    registerVisualizerFor<VisualizationPointCloud>();
    registerVisualizerFor<VisualizationPolygon>();
    registerVisualizerFor<VisualizationMesh>();
    registerVisualizerFor<VisualizationRobot>();
    registerVisualizerFor<VisualizationObject>();
    registerVisualizerFor<VisualizationPath>();
}
