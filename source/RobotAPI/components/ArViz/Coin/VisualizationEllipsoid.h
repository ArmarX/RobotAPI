#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>

#include <Inventor/nodes/SoComplexity.h>
#include <Inventor/nodes/SoScale.h>
#include <Inventor/nodes/SoSphere.h>

namespace armarx::viz::coin
{
    struct VisualizationEllipsoid : TypedElementVisualization<SoSeparator>
    {
        using ElementType = data::ElementEllipsoid;

        VisualizationEllipsoid()
        {
            complexity = new SoComplexity();
            complexity->type.setValue(SoComplexity::OBJECT_SPACE);
            complexity->value.setValue(1.0f);

            scale = new SoScale;

            sphere = new SoSphere();
            // We create a unit sphere and create an ellipsoid through scaling
            sphere->radius.setValue(1.0f);

            node->addChild(complexity);
            node->addChild(scale);
            node->addChild(sphere);
        }

        bool update(ElementType const& element)
        {
            scale->scaleFactor.setValue(element.axisLengths.e0, element.axisLengths.e1, element.axisLengths.e2);

            return true;
        }

        SoMaterial* material = nullptr;
        SoComplexity* complexity = nullptr;
        SoScale* scale = nullptr;
        SoSphere* sphere = nullptr;
    };
}
