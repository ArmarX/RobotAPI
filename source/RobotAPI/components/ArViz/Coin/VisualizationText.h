#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>

#include <Inventor/nodes/SoAsciiText.h>

namespace armarx::viz::coin
{
    struct VisualizationText : TypedElementVisualization<SoAsciiText>
    {
        using ElementType = data::ElementText;

        bool update(ElementType const& element)
        {
            node->string = element.text.c_str();

            return true;
        }
    };
}
