/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>

class SoCoordinate3;
class SoDrawStyle;
class SoLineSet;

namespace armarx::viz::coin
{
    struct VisualizationPath : TypedElementVisualization<SoSeparator>
    {
        using ElementType = data::ElementPath;

        VisualizationPath();

        bool update(ElementType const& element);

        /// lines
        SoCoordinate3* coordinate3;
        SoDrawStyle* lineStyle;
        SoLineSet* lineSet;
        SoMaterial* lineMaterial;


        /// points
        // SoMaterial* pclMat;
        SoCoordinate3* pclCoords;
        SoDrawStyle* pclStyle;

    };
}  // namespace armarx::viz::coin
