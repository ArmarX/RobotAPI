#pragma once

#include <Inventor/nodes/SoNode.h>

#include <string>

namespace armarx::viz::coin
{

    void exportToVRML(SoNode* node, std::string const& exportFilePath);

}
