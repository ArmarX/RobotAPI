#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>

#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoCoordinate3.h>

namespace armarx::viz::coin
{
    struct VisualizationLine : TypedElementVisualization<SoSeparator>
    {
        using ElementType = data::ElementLine;

        VisualizationLine()
        {
            lineStyle = new SoDrawStyle;
            coordinate3 = new SoCoordinate3;
            SoLineSet* lineSet = new SoLineSet;
            lineSet->numVertices.setValue(2);
            lineSet->startIndex.setValue(0);

            node->addChild(lineStyle);
            node->addChild(coordinate3);
            node->addChild(lineSet);
        }

        bool update(ElementType const& element)
        {
            lineStyle->lineWidth.setValue(element.lineWidth);

            SbVec3f from(element.from.e0, element.from.e1, element.from.e2);
            SbVec3f to(element.to.e0, element.to.e1, element.to.e2);
            coordinate3->point.set1Value(0, from);
            coordinate3->point.set1Value(1, to);

            return true;
        }

        SoDrawStyle* lineStyle;
        SoCoordinate3* coordinate3;
    };
}
