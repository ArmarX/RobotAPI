#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>

#include <VirtualRobot/VirtualRobot.h>

namespace armarx::viz::coin
{
    struct LoadedRobot
    {
        std::string project;
        std::string filename;
        VirtualRobot::RobotPtr robot;
    };

    struct VisualizationRobot : TypedElementVisualization<SoSeparator>
    {
        using ElementType = data::ElementRobot;

        ~VisualizationRobot();

        bool update(ElementType const& element);

        void recreateVisualizationNodes(int drawStyle);

        LoadedRobot loaded;
        int loadedDrawStyle = data::ModelDrawStyle::ORIGINAL;
        armarx::viz::data::Color loadedColor{0, 0, 0, 0};
    };

    void clearRobotCache();
}
