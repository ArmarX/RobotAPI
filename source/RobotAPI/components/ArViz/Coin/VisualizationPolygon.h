#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>

#include <ArmarXCore/core/logging/Logging.h>

#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoFaceSet.h>
#include <Inventor/nodes/SoLineSet.h>


namespace armarx::viz::coin
{
    struct VisualizationPolygon : TypedElementVisualization<SoSeparator>
    {
        using ElementType = data::ElementPolygon;

        VisualizationPolygon()
        {
            coordinate3 = new SoCoordinate3;

            faceSet = new SoFaceSet;

            // create line around polygon
            SoSeparator* lineSep = new SoSeparator;

            lineMaterial = new SoMaterial;
            lineSep->addChild(lineMaterial);
            lineSep->addChild(coordinate3);

            lineStyle = new SoDrawStyle();
            lineSep->addChild(lineStyle);

            lineSet = new SoLineSet;
            lineSep->addChild(lineSet);

            node->addChild(coordinate3);
            node->addChild(faceSet);
            node->addChild(lineSep);
        }

        bool update(ElementType const& element)
        {
            if (element.points.size() < 3)
            {
                // A polygon with < 3 points is invalid.
                ARMARX_WARNING << deactivateSpam(1000)
                               << "Ignoring polygon '" << element.id << "' "
                               << "with " << element.points.size() << " points "
                               << "(a polygon requires >= 3 points).";
                return true;
            }

            int pointSize = (int)element.points.size();

            int i = 0;
            coordinate3->point.setNum(pointSize + 1);
            for (auto& point : element.points)
            {
                SbVec3f pt(point.e0, point.e1, point.e2);
                coordinate3->point.set1Value(i, pt);
                i += 1;
            }

            if (pointSize > 0)
            {
                // Add the first element as last element to close the loop
                auto& first = element.points.front();
                SbVec3f pt0(first.e0, first.e1, first.e2);
                coordinate3->point.set1Value(pointSize, pt0);
            }

            faceSet->numVertices.set1Value(0, pointSize);

            // Line around polygon
            const float conv = 1.0f / 255.0f;
            float r = element.lineColor.r * conv;
            float g = element.lineColor.g * conv;
            float b = element.lineColor.b * conv;
            float a = element.lineColor.a * conv;
            lineMaterial->diffuseColor.setValue(r, g, b);
            lineMaterial->ambientColor.setValue(r, g, b);
            lineMaterial->transparency.setValue(1.0f - a);

            if (element.lineWidth > 0.0f)
            {
                lineStyle->lineWidth.setValue(element.lineWidth);
            }
            else
            {
                lineStyle->style = SoDrawStyleElement::INVISIBLE;
            }

            lineSet->numVertices.set1Value(0, pointSize + 1);

            return true;
        }

        SoCoordinate3* coordinate3;
        SoFaceSet* faceSet;
        SoDrawStyle* lineStyle;
        SoLineSet* lineSet;
        SoMaterial* lineMaterial;
    };
}
