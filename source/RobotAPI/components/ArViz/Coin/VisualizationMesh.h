#pragma once

#include "ElementVisualizer.h"

#include <RobotAPI/interface/ArViz/Elements.h>

#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoShapeHints.h>

namespace armarx::viz::coin
{
    struct VisualizationMesh : TypedElementVisualization<SoSeparator>
    {
        using ElementType = data::ElementMesh;

        VisualizationMesh()
        {
            SoMaterialBinding* myBinding = new SoMaterialBinding;
            myBinding->value = SoMaterialBinding::PER_VERTEX_INDEXED;

            materials = new SoMaterial;
            coords = new SoCoordinate3;

            SoShapeHints* hints = new SoShapeHints;
            // Disable back culling and enable two-sided lighting
            hints->vertexOrdering = SoShapeHints::VertexOrdering::COUNTERCLOCKWISE;
            hints->shapeType = SoShapeHints::ShapeType::UNKNOWN_SHAPE_TYPE;

            faceSet = new SoIndexedFaceSet;

            node->addChild(myBinding);
            node->addChild(materials);
            node->addChild(coords);
            node->addChild(hints);
            node->addChild(faceSet);
        }

        bool update(ElementType const& element)
        {
            int colorSize = (int)element.colors.size();
            bool noColorsArray = colorSize == 0;
            if (colorSize == 0)
            {
                colorSize = 1;
            }
            matColor.resize(colorSize);
            transp.resize(colorSize);

            const float conv = 1.0f / 255.0f;
            if (noColorsArray)
            {
                auto color = element.color;
                float r = color.r * conv;
                float g = color.g * conv;
                float b = color.b * conv;
                float a = color.a * conv;
                matColor[0].setValue(r, g, b);
                transp[0] = 1.0f - a;
            }
            else
            {
                for (int i = 0; i < colorSize; i++)
                {
                    auto color = element.colors[i];
                    float r = color.r * conv;
                    float g = color.g * conv;
                    float b = color.b * conv;
                    float a = color.a * conv;
                    matColor[i].setValue(r, g, b);
                    transp[i] = 1.0f - a;
                }
            }

            // Define colors for the faces
            materials->diffuseColor.setValuesPointer(colorSize, matColor.data());
            materials->ambientColor.setValuesPointer(colorSize, matColor.data());
            materials->transparency.setValuesPointer(colorSize, transp.data());

            // define vertex array
            int vertexSize = (int)element.vertices.size();
            vertexPositions.resize(vertexSize);
            for (int i = 0; i < vertexSize; i++)
            {
                auto v = element.vertices[i];
                vertexPositions[i].setValue(v.e0, v.e1, v.e2);
            }

            // Define coordinates for vertices
            coords->point.setValuesPointer(vertexSize, vertexPositions.data());

            int facesSize = (int)element.faces.size();
            faces.resize(facesSize * 4);
            matInx.resize(facesSize * 4);

            for (int i = 0; i < facesSize; i++)
            {
                auto& face = element.faces[i];

                faces[i * 4 + 0] = face.v0;
                faces[i * 4 + 1] = face.v1;
                faces[i * 4 + 2] = face.v2;
                faces[i * 4 + 3] = SO_END_FACE_INDEX;

                matInx[i * 4 + 0] = face.c0;
                matInx[i * 4 + 1] = face.c1;
                matInx[i * 4 + 2] = face.c2;
                matInx[i * 4 + 3] = SO_END_FACE_INDEX;
            }

            faceSet->coordIndex.setValuesPointer(faces.size(), faces.data());
            faceSet->materialIndex.setValuesPointer(matInx.size(), matInx.data());

            return true;
        }

        SoMaterial* materials;
        SoCoordinate3* coords;
        SoIndexedFaceSet* faceSet;

        std::vector<SbColor> matColor;
        std::vector<float> transp;
        std::vector<SbVec3f> vertexPositions;
        std::vector<int32_t> faces;
        std::vector<int32_t> matInx;
    };
}
