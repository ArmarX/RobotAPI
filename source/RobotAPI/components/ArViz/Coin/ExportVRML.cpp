#include "ExportVRML.h"

#include <ArmarXCore/core/logging/Logging.h>

#include <Inventor/actions/SoWriteAction.h>
#include <Inventor/actions/SoToVRML2Action.h>
#include <Inventor/nodes/SoFile.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/VRMLnodes/SoVRMLGroup.h>

namespace armarx::viz::coin
{

static SoGroup* convertSoFileChildren(SoGroup* orig)
{
    if (!orig)
    {
        return new SoGroup;
    }

    SoGroup* storeResult;

    if (orig->getTypeId() == SoSeparator::getClassTypeId())
    {
        storeResult = new SoSeparator;
    }
    else
    {
        storeResult = new SoGroup;
    }

    storeResult->ref();

    if (orig->getTypeId().isDerivedFrom(SoGroup::getClassTypeId()))
    {
        // process group node
        for (int i = 0; i < orig->getNumChildren(); i++)
        {
            SoNode* n1 = orig->getChild(i);

            if (n1->getTypeId().isDerivedFrom(SoGroup::getClassTypeId()))
            {
                // convert group
                SoGroup* n2 = (SoGroup*)n1;
                SoGroup* gr1 = convertSoFileChildren(n2);
                storeResult->addChild(gr1);
            }
            else if (n1->getTypeId() == SoFile::getClassTypeId())
            {
                // really load file!!
                SoFile* fn = (SoFile*)n1;
                SoGroup* fileChildren;
                fileChildren = fn->copyChildren();
                storeResult->addChild(fileChildren);
            }
            else
            {
                // just copy child node
                storeResult->addChild(n1);
            }
        }
    }

    storeResult->unrefNoDelete();
    return storeResult;
}

void exportToVRML(SoNode* node, std::string const& exportFilePath)
{
    SoOutput* so = new SoOutput();
    if (!so->openFile(exportFilePath.c_str()))
    {
        ARMARX_ERROR << "Could not open file " << exportFilePath << " for writing.";
        return;
    }

    so->setHeaderString("#VRML V2.0 utf8");

    SoGroup* n = new SoGroup;
    n->ref();
    n->addChild(node);
    SoGroup* newVisu = convertSoFileChildren(n);
    newVisu->ref();

    SoToVRML2Action tovrml2;
    tovrml2.apply(newVisu);
    SoVRMLGroup* newroot = tovrml2.getVRML2SceneGraph();
    newroot->ref();
    SoWriteAction wra(so);
    wra.apply(newroot);
    newroot->unref();

    so->closeFile();

    newVisu->unref();
    n->unref();
}

}
