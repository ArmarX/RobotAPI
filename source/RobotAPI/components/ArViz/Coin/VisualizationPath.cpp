#include "VisualizationPath.h"

#include <algorithm>

#include <Inventor/SbVec3f.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/nodes/SoMaterial.h>

namespace armarx::viz::coin
{

    VisualizationPath::VisualizationPath()
    {
        coordinate3 = new SoCoordinate3;

        // create line around polygon
        SoSeparator* lineSep = new SoSeparator;

        lineMaterial = new SoMaterial;
        lineSep->addChild(lineMaterial);
        lineSep->addChild(coordinate3);

        lineStyle = new SoDrawStyle();
        lineSep->addChild(lineStyle);

        lineSet = new SoLineSet;
        lineSep->addChild(lineSet);

        node->addChild(coordinate3);
        node->addChild(lineSep);

        // points (use the following, if the points should be drawn in a different color)

        // pclMat = new SoMaterial;

        // SoMaterialBinding* pclMatBind = new SoMaterialBinding;
        // pclMatBind->value = SoMaterialBinding::PER_PART;

        pclCoords = new SoCoordinate3;
        pclStyle = new SoDrawStyle;

        // node->addChild(pclMat);
        // node->addChild(pclMatBind);
        node->addChild(pclCoords);
        node->addChild(pclStyle);
        node->addChild(new SoPointSet);
    }

    bool VisualizationPath::update(ElementType const& element)
    {
        // set position
        coordinate3->point.setValuesPointer(element.points.size(),
                                            reinterpret_cast<const float*>(element.points.data()));

        // set color
        const auto lineColor = element.color;

        constexpr float toUnit = 1.0F / 255.0F;

        const auto color         = SbVec3f(lineColor.r, lineColor.g, lineColor.b) * toUnit;
        const float transparency = 1.0F - static_cast<float>(lineColor.a) * toUnit;

        lineMaterial->diffuseColor.setValue(color);
        lineMaterial->ambientColor.setValue(color);
        lineMaterial->transparency.setValue(transparency);

        if (element.lineWidth > 0.0F)
        {
            lineStyle->lineWidth.setValue(element.lineWidth);
        }
        else
        {
            lineStyle->style = SoDrawStyleElement::INVISIBLE;
        }

        const int pointSize = static_cast<int>(element.points.size());
        lineSet->numVertices.set1Value(0, pointSize);

        // points at each node
        // const std::vector<SbVec3f> colorData(element.points.size(), color);

        // pclMat->diffuseColor.setValuesPointer(colorData.size(),
        //                                       reinterpret_cast<const float*>(colorData.data()));
        // pclMat->ambientColor.setValuesPointer(colorData.size(),
        //                                       reinterpret_cast<const float*>(colorData.data()));
        // pclMat->transparency = transparency;

        pclCoords->point.setValuesPointer(element.points.size(),
                                          reinterpret_cast<const float*>(element.points.data()));

        pclStyle->pointSize = element.lineWidth + 5;

        return true;
    }
} // namespace armarx::viz::coin
