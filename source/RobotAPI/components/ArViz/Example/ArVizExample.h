/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArVizExample
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>


namespace armarx
{

    /**
     * @defgroup Component-ArVizExample ArVizExample
     * @ingroup RobotAPI-Components
     *
     * An example for how to visualize 3D elements via the 3D visualization
     * framework ArViz.
     *
     * The example creates several layers, fills them with visualization
     * elements, and commits them to ArViz.
     *
     * To see the result:
     * \li Start the component `ArVizStorage`
     * \li Open the gui plugin `ArViz`
     * \li Start the component `ArVizExample`
     *
     * The scenario `ArVizExample` starts the necessary components,
     * including the example component.
     *
     *
     * A component which wants to visualize data via ArViz should:
     * \li `#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>`
     * \li Inherit from the `armarx::ArVizComponentPluginUser`. This adds the
     *     necessary properties (e.g. the topic name) and provides a
     *     ready-to-use ArViz client called `arviz`.
     * \li Use the inherited ArViz client variable `arviz` of type `viz::Client`
     *     to create layers, add visualization elements to the layers,
     *     and commit the layers to the ArViz topic.
     *
     * \see ArVizExample
     *
     *
     * @class ArVizExample
     * @ingroup Component-ArVizExample
     *
     * @brief An example for how to use ArViz.
     *
     * @see @ref Component-ArVizExample
     */
    class ArVizExample :
        virtual public armarx::Component,
        // Deriving from armarx::ArVizComponentPluginUser adds necessary properties
        // and provides a ready-to-use ArViz client called `arviz`.
        virtual public armarx::ArVizComponentPluginUser
    {
    public:

        std::string getDefaultName() const override;
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void onInitComponent() override;
        void onConnectComponent() override;

        void onDisconnectComponent() override;
        void onExitComponent() override;


    private:

        void run();


    private:

        RunningTask<ArVizExample>::pointer_type task;

        struct Properties
        {
            bool manyLayers = false;
        };
        Properties properties;

    };
}
