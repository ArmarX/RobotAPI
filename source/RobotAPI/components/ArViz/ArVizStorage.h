/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArViz
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/ArViz.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <mutex>
#include <atomic>
#include <condition_variable>
#include <filesystem>


namespace armarx
{

    /**
     * @defgroup Component-ArVizStorage ArVizStorage
     * @ingroup RobotAPI-Components
     *
     * The ArViz storage stores visualization elements published by ArViz
     * clients and provides them for visualizing components such as the
     * ArViz gui plugin.
     *
     * In addition, the ArViz storage can be used to record and replay
     * visualization episodes.
     *
     *
     * @class ArVizStorage
     * @ingroup Component-ArVizStorage
     *
     * @brief Stores visualization elements drawn by ArViz clients.
     *
     */
    class ArVizStorage
        : virtual public armarx::Component
        , virtual public armarx::viz::StorageAndTopicInterface
    {
    public:

        /// armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


        /// PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;


        // Topic interface
        void updateLayers(viz::data::LayerUpdateSeq const& updates, const Ice::Current&) override;

        // StorageInterface interface
        viz::data::CommitResult commitAndReceiveInteractions(viz::data::CommitInput const& input, const Ice::Current&) override;

        viz::data::LayerUpdates pullUpdatesSince(Ice::Long revision, const Ice::Current&) override;
        viz::data::LayerUpdates pullUpdatesSinceAndSendInteractions(
                Ice::Long revision,
                viz::data::InteractionFeedbackSeq const& interactions,
                const Ice::Current&) override;
        std::string startRecording(std::string const& prefix, const Ice::Current&) override;
        void stopRecording(const Ice::Current&) override;
        viz::data::RecordingSeq getAllRecordings(const Ice::Current&) override;
        viz::data::RecordingBatch getRecordingBatch(const std::string&, Ice::Long, const Ice::Current&) override;

    private:
        void record();

        void recordBatch(viz::data::RecordingBatch& batch);

    private:
        std::string topicName = "ArVizTopic";
        int maxHistorySize = 1000;
        std::filesystem::path historyPath;

        std::mutex historyMutex;

        std::vector<viz::data::TimestampedLayerUpdate> currentState;
        std::vector<viz::data::TimestampedLayerUpdate> history;
        long revision = 0;

        // We store all interactions in here
        // But we curate them, so that only the last interaction with an element is reported
        // in case the component never retrieves the interactions
        std::vector<viz::data::InteractionFeedback> interactionBuffer;

        std::mutex recordingMutex;
        std::filesystem::path recordingPath;
        std::vector<viz::data::TimestampedLayerUpdate> recordingInitialState;
        std::vector<viz::data::RecordingBatch> recordingBuffer;
        viz::data::Recording recordingMetaData;
        std::condition_variable recordingCondition;
        RunningTask<ArVizStorage>::pointer_type recordingTask;
    };
}
