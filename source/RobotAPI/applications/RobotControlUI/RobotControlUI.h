/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Applications
 * @author     Jan Issac (jan dot issac at gmx dot net)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// ArmarXCore
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/IceManager.h>
#include <RobotAPI/statecharts/operations/RobotControl.h>
#include <string>

namespace armarx
{
    class RobotControlUI :
        virtual public Component
    {
    public:
        int stateId;
        RobotControlInterfacePrx robotProxy;
        std::string getDefaultName() const override
        {
            return "RobotControlUI";
        }
        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;
        void run();
    private:
        RunningTask<RobotControlUI>::pointer_type controlTask;
    };
}

