/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RobotStateComponent::
* @author     ( at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/



#include <ArmarXCore/core/application/Application.h>
#include <RobotAPI/components/RobotState/RobotStateComponent.h>

namespace armarx
{
    /**
     * Application for testing armarx::RobotStateComponent
     */
    class RobotStateComponentApp :
        virtual public armarx::Application
    {
        /**
         * @see armarx::Application::setup()
         */
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override
        {
            auto state = Component::create<RobotStateComponent>(properties);
            auto observer = Component::create<RobotStateObserver>(properties);
            state->setRobotStateObserver(observer);
            registry->addObject(state);
            registry->addObject(observer);
        }
    };
}
