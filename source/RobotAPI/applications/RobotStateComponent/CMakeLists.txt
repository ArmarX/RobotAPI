armarx_component_set_name(RobotStateComponent)
set(COMPONENT_LIBS RobotAPIRobotStateComponent)
set(SOURCES main.cpp RobotStateComponentApp.h)
armarx_add_component_executable("${SOURCES}")
