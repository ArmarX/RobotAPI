/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::application::WeissHapticUnit
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/application/Application.h>
#include <RobotAPI/components/units/HapticObserver.h>
#include <RobotAPI/drivers/WeissHapticSensor/WeissHapticUnit.h>

namespace armarx
{
    /**
     * @class WeissHapticUnitApp
     * @brief Joint Application for WeissHapticUnit and HapticObserver.
     *
     * This Application runs both the WeissHapticUnit and the HapticObserver in one executable to avoid TCP communication.
     */
    class WeissHapticUnitApp :
        virtual public armarx::Application
    {
        /**
         * @see armarx::Application::setup()
         */
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry,
                   Ice::PropertiesPtr properties) override
        {
            registry->addObject(Component::create<WeissHapticUnit>(properties));
            registry->addObject(Component::create<HapticObserver>(properties));
        }
    };
}

