/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::application::AronArmemGenerator
 * @author     fabian.peller-konrad@kit.edu ( fabian dot peller-konrad at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL
#include <iostream>
#include <filesystem>
#include <ctime>

// CXXOPTS
#include "cxxopts.hpp"

// ArmarX Executable
//#include <ArmarXCore/core/application/Application.h>
//#include <ArmarXCore/core/Component.h>
//#include <ArmarXCore/core/logging/Logging.h>

// Aron
#include <RobotAPI/interface/aron.h>

#include <ArmarXCore/libraries/cppgen/CppMethod.h>
#include <ArmarXCore/libraries/cppgen/CppClass.h>

#include <RobotAPI/libraries/aron/codegeneration/typereader/xml/Reader.h>
#include <RobotAPI/libraries/aron/codegeneration/codegenerator/codewriter/cpp/Writer.h>

using namespace armarx;
using namespace aron;


/// Aron Code Generator Main Executable.
/// This executable calls generates a aron code file out of an aron xml file.
int main(int argc, char* argv[])
{
    try
    {
        cxxopts::Options options("AronArmemCodeGenerator", "An application interface for the aron and armem code generation");

        std::string input_default = "/path/to/some/xml/file.xml";
        std::string output_default = "/path/to/some/output/folder/";

        options.add_options("General")
        ("v,verbose", "Enable verbose mode", cxxopts::value<bool>()->default_value("false"))
        ("h,help", "Print usage");

        options.add_options("IO")
        ("i,input", "XML file name", cxxopts::value<std::string>()->default_value(input_default))
        ("o,output", "The output path", cxxopts::value<std::string>()->default_value(output_default))
        ("I,include", "include path", cxxopts::value<std::vector<std::string>>());

        options.add_options("Generation")
        ("f,force", "Enforce the generation", cxxopts::value<bool>()->default_value("false"));

        auto result = options.parse(argc, argv);

        if (result.count("help"))
        {
            std::cout << options.help() << std::endl;
            exit(0);
        }

        bool verbose = result["v"].as<bool>();
        bool force = result["f"].as<bool>();

        std::string filename = result["i"].as<std::string>();
        std::string output = result["o"].as<std::string>();

        if (filename == input_default || output == output_default)
        {
            std::cout << options.help() << std::endl;
            exit(0);
        }

        if (verbose)
        {
            std::cout << "Welcome to the AronCodeGenerator!" << std::endl;
            std::cout << "Received the following parameters:" << std::endl;
            std::cout << "- IO:" << std::endl;
            std::cout << "-- Input: " << filename << std::endl;
            std::cout << "-- Output: " << output << std::endl;
            std::cout << "- Generation:" << std::endl;
            std::cout << "-- Force: " << force << std::endl;
            std::cout << std::endl;
        }

        if (verbose)
        {
            std::cout << "Generating a new cpp file out of <" + filename + ">" << std::endl;
        }

        std::filesystem::path input_file(filename);
        std::filesystem::path output_folder(output);

        std::vector<std::filesystem::path> includePaths;

        if(result.count("I") > 0)
        {
            for(const auto& path: result["I"].as<std::vector<std::string>>())
            {
                includePaths.emplace_back(path);
            }
        }


        if (!std::filesystem::exists(input_file) || !std::filesystem::is_regular_file(input_file))
        {
            std::cerr << "The input file does not exist or is not a regular file." << std::endl;
            exit(1);
        }

        if (!std::filesystem::exists(output_folder) || !std::filesystem::is_directory(output_folder))
        {
            std::cerr << "The output folder does not exist or is not an directory." << std::endl;
            exit(1);
        }

        if (input_file.extension().string() != ".xml")
        {
            std::cerr << "The file you passed has the wrong type." << std::endl;
            exit(1);
        }

        if (verbose)
        {
            std::cout << "Parsing the XML file..." << std::endl;
        }

        typereader::xml::Reader reader;
        reader.parseFile(input_file, includePaths);
        if (verbose)
        {
            std::cout << "Parsing the XML file... done!" << std::endl;
            std::cout << "--> Found " << reader.getGenerateObjects().size() << " types." << std::endl;
            std::cout << "--> They are: " << std::endl;
            for (const auto& generateType : reader.getGenerateObjects())
            {
                std::cout << "----> " << generateType.typeName << std::endl;
            }
        }

        codegenerator::cpp::Writer writer("AronTestSegment", reader.getCodeIncludes());

        if (verbose)
        {
            std::cout << "Running the type class generator..." << std::endl;
        }

        writer.generateTypeIntEnums(reader.getGenerateIntEnums());
        writer.generateTypeObjects(reader.getGenerateObjects());

        if (verbose)
        {
            std::cout << "Running the type class generator... done!" << std::endl;
            std::cout << "--> Found " << writer.getTypeClasses().size() << " type objects." << std::endl;
            std::cout << "--> They are: " << std::endl;
            for (const auto& c : writer.getTypeClasses())
            {
                std::cout << "----> " << c->getName() << std::endl;
            }
        }

        std::vector<MetaClassPtr> classes = writer.getTypeClasses();
        if (verbose)
        {
            std::cout << "Now exporting classes..." << std::endl;
        }

        auto w = CppWriterPtr(new CppWriter());
        CppClass::Write(classes, w);

        std::string new_file_content = w->getString();

        if (verbose)
        {
            std::cout << "Now exporting classes... done!" << std::endl;
        }

        if (new_file_content == "")
        {
            std::cerr << "Found error in code generation. Aborting!" << std::endl;
            exit(1);
        }

        std::string new_file_header = "\
/* \n\
 * This file is part of ArmarX. \n\
 * \n\
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), \n\
 * Karlsruhe Institute of Technology (KIT), all rights reserved. \n\
 * \n\
 * ArmarX is free software; you can redistribute it and/or modify \n\
 * it under the terms of the GNU General Public License version 2 as \n\
 * published by the Free Software Foundation. \n\
 * \n\
 * ArmarX is distributed in the hope that it will be useful, but \n\
 * WITHOUT ANY WARRANTY; without even the implied warranty of \n\
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the \n\
 * GNU General Public License for more details. \n\
 * \n\
 * You should have received a copy of the GNU General Public License \n\
 * along with this program. If not, see <http://www.gnu.org/licenses/>. \n\
 * \n\
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt \n\
 *             GNU General Public License \n\
 ************************************************************************* \n\
 * WARNING: This file is autogenerated. \n\
 * Original file: " + filename + " \n\
 * Please do not edit since your changes may be overwritten on the next generation \n\
 * If you have any questions please contact: Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu) \n\
 ************************************************************************* \n\
 */\n\n\n";

        std::string new_file_full_content = new_file_header + new_file_content;

        std::string new_name_with_extension = input_file.filename().replace_extension("").string();
        new_name_with_extension += ".aron.generated.h";
        std::string output_file_path = output + "/" + new_name_with_extension;
        std::filesystem::path output_file(output_file_path);

        // check if file already exists
        if (!force)
        {
            if (std::filesystem::exists(output_file))
            {
                std::ifstream ifs(output_file);
                std::string file_content((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));

                if (file_content == new_file_full_content)
                {
                    if (verbose)
                    {
                        std::cout << "File content not changed for <" + output_file.string() + ">" << std::endl;
                    }
                    exit(0);
                }
            }
        }

        std::ofstream ofs;
        ofs.open(output_file);
        ofs << new_file_full_content;
        ofs.close();

        if (verbose)
        {
            std::cout << "Finished generating <" + output_file.string() + ">. The new file ist called <" << output_file.string() << ">" << std::endl;
        }
    }
    catch (const cxxopts::OptionException& e)
    {
        std::cerr << "Error in parsing cxxopts options: " << e.what() << std::endl;
        exit(1);
    }
    exit(0);
}
