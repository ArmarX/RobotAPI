/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::SickLaserUnit
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SickLaserUnit.h"
#include <exception>

// Include headers you only need in function definitions in the .cpp.

namespace armarx
{

    void SickLaserScanDevice::run()
    {
        while (!task->isStopped())
        {
            switch (runState)
            {
                case RunState::scannerInit:
                    if (initCnt < 5)
                    {
                        initCnt++;
                        initScanner();
                    }
                    else
                    {
                        ARMARX_WARNING << "Maximum number of reinitializations reached - going to idle state";
                        runState = RunState::scannerFinalize;
                    }
                    break;
                case RunState::scannerRun:
                    if (result == sick_scan::ExitSuccess) // OK -> loop again
                    {
                        scanData.clear();
                        result = scanner->loopOnce(scanData, scanTime, scanInfo, false);
                        if (scanTopic)
                        {
                            TimestampVariantPtr scanT(new TimestampVariant(scanTime));
                            scanTopic->reportSensorValues(scannerName, scannerName, scanData, scanT);
                            //trigger heartbeat
                            scannerHeartbeat->heartbeat(scannerName);
                        }
                        else
                        {
                            ARMARX_WARNING << "No scan topic available: IP: " << ip << ", Port: " << port;
                        }
                    }
                    else
                    {
                        runState = RunState::scannerInit;
                    }
                    break;
                case RunState::scannerFinalize:
                    ARMARX_WARNING << "Scanner offline - stopping task.";
                    this->task->stop();
                    break;
                default:
                    ARMARX_WARNING << "Invalid run state in task loop";
                    break;
            }
        }
    }

    void SickLaserScanDevice::initScanner()
    {
        ARMARX_INFO_S << "Start initialising scanner " << scannerName
                      << " [Ip: " << this->ip << "] [Port: " << this->port << "]";
        // attempt to connect/reconnect
        if (this->scanner)
        {
            ARMARX_WARNING_S << "Scanner already initialized - reinit.";
            this->scanner.reset(); // disconnect scanner
        }
        this->scanner = std::make_unique<SickScanAdapter>(this->ip, this->port, this->timelimit, this->parser.get(), 'A');
        this->result = this->scanner->init();

        if (this->result == sick_scan::ExitSuccess)
        {
            //read the scanner parameters for initialization
            this->result = scanner->loopOnce(scanData, scanTime, scanInfo, true);
        }
        if (this->result == sick_scan::ExitSuccess) // OK -> loop again
        {
            ARMARX_INFO_S << "Scanner successfully initialized.";
            this->runState = RunState::scannerRun; // after initialising switch to run state
        }
        else
        {
            this->runState = RunState::scannerInit; // If there was an error, try to restart scanner
        }
    }


    armarx::PropertyDefinitionsPtr SickLaserUnit::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def =
            new ComponentPropertyDefinitions(getConfigIdentifier());
        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(myComponentProxy)

        def->topic(topic, properties.topicName, "TopicName", "Name of the laserscanner topic to report to.");
        //Scanner parameters
        def->optional(properties.devices, "devices", "List of Devices in format frame1,ip1,port1;frame2,ip2,port2");
        def->optional(properties.scannerType, "scannerType", "Name of the LaserScanner");
        def->optional(properties.timelimit, "timelimit", "timelimit for communication");
        def->optional(properties.rangeMin, "rangeMin", "minimum Range of the Scanner");
        def->optional(properties.rangeMax, "rangeMax", "maximum Range of the Scanner");
        def->optional(properties.heartbeatWarnMS, "heartbeatWarnMS", "maximum cycle time before heartbeat Warning");
        def->optional(properties.heartbeatErrorMS, "heartbeatErrorMS", "maximum cycle time before heartbeat Error");
        return def;
    }

    SickLaserUnit::SickLaserUnit()
    {
        addPlugin(heartbeat);
        ARMARX_CHECK_NOT_NULL(heartbeat);
    }

    void SickLaserUnit::onInitComponent()
    {
        ARMARX_INFO << "On init";
        // Topics and properties defined above are automagically registered.
        //offeringTopic(properties.topicName);

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);

        std::vector<std::string> splitDeviceStrings = Split(properties.devices, ";");
        scanDevices.clear();
        scanDevices.reserve(splitDeviceStrings.size());
        for (std::string const& deviceString : splitDeviceStrings)
        {
            std::vector<std::string> deviceInfo = Split(deviceString, ",");
            if (deviceInfo.size() != 3)
            {
                ARMARX_WARNING << "Unexpected format for laser scanner device: " << deviceString
                               << " (split size: " << deviceInfo.size() << ", expected: 3)";
                continue;
            }
            SickLaserScanDevice& device = scanDevices.emplace_back();
            device.scannerName = deviceInfo[0];
            device.ip = deviceInfo[1];
            device.port = deviceInfo[2];
            device.timelimit = properties.timelimit;
            //scanInfo
            device.scanInfo.device = device.ip;
            device.scanInfo.frame = device.scannerName;
            //scanner Parameters
            try
            {
                device.parser = std::make_unique<sick_scan::SickGenericParser>(properties.scannerType);
                device.parser->set_range_min(properties.rangeMin);
                device.parser->set_range_max(properties.rangeMax);
                device.parser->getCurrentParamPtr()->setUseBinaryProtocol(false);
            }
            catch (std::exception const& e)
            {
                ARMARX_ERROR_S << "Could not create parser. Wrong Scanner name.";
                return;
            }
            armarx::RobotHealthHeartbeatArgs heartbeatArgs =
                armarx::RobotHealthHeartbeatArgs(properties.heartbeatWarnMS, properties.heartbeatErrorMS, "No LaserScan data available");
            device.scannerHeartbeat = heartbeat;
            device.scannerHeartbeat->configureHeartbeatChannel(device.scannerName, heartbeatArgs);
        }
    }

    void SickLaserUnit::onConnectComponent()
    {
        for (SickLaserScanDevice& device : scanDevices)
        {
            device.scanTopic = topic;
            //start the laser scanner
            if (device.task)
            {
                ARMARX_WARNING << "this should not happen.";
                device.task->stop();
                device.task = nullptr;
            }
            device.runState = RunState::scannerInit;
            device.task = new RunningTask<SickLaserScanDevice>(&device, &SickLaserScanDevice::run, "SickLaserScanUpdate_" + device.ip);
            device.task->start();
        }
        // Do things after connecting to topics and components.

        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
        setDebugObserverDatafield("numBoxes", properties.numBoxes);
        setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
        sendDebugObserverBatch();
        }
        */
    }

    void SickLaserUnit::onDisconnectComponent()
    {
        ARMARX_INFO_S << "Disconnecting LaserScanner.";
        for (SickLaserScanDevice& device : scanDevices)
        {
            if (device.task)
            {
                device.task->stop();
                device.task = nullptr;
            }
        }
    }

    void SickLaserUnit::onExitComponent()
    {
    }

    std::string SickLaserUnit::getDefaultName() const
    {
        return "SickLaserUnit";
    }

} // namespace armarx
