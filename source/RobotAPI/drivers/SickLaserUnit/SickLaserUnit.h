/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::SickLaserUnit
 * @author     Johann Mantel ( j-mantel at gmx dot net )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// #include <mutex>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <RobotAPI/interface/units/LaserScannerUnit.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/HeartbeatComponentPlugin.h>

#include <vector>

#include "SickScanAdapter.h"
#include <sick_scan/sick_scan_common.h>
#include <sick_scan/sick_scan_common_tcp.h>
#include <sick_scan/sick_generic_laser.h>

namespace armarx
{

    enum class ScanProtocol
    {
        ASCII,
        Binary
    };

    enum class RunState
    {
        scannerInit,
        scannerRun,
        scannerFinalize
    };

    struct SickLaserScanDevice
    {
        std::string scannerName;
        //communication parameters
        std::string ip;
        std::string port;
        int timelimit = 5;
        //data and task pointers
        IceUtil::Time scanTime;
        LaserScan scanData;
        LaserScannerInfo scanInfo;
        int initCnt = 0;
        int result = sick_scan::ExitError;
        RunState runState = RunState::scannerFinalize;
        RunningTask<SickLaserScanDevice>::pointer_type task;
        LaserScannerUnitListenerPrx scanTopic;
        plugins::HeartbeatComponentPlugin* scannerHeartbeat;
        //scanner pointers
        std::unique_ptr<sick_scan::SickGenericParser> parser;
        std::unique_ptr<SickScanAdapter> scanner;

        void initScanner();
        void run();
    };
    /**
     * @defgroup Component-SickLaserUnit SickLaserUnit
     * @ingroup RobotAPI-Components
     * A description of the component SickLaserUnit.
     *
     * @class SickLaserUnit
     * @ingroup Component-SickLaserUnit
     * @brief Brief description of class SickLaserUnit.
     *
     * Detailed description of class SickLaserUnit.
     */
    class SickLaserUnit :
    //virtual public armarx::LaserScannerUnitInterface,
        virtual public armarx::Component
    // , virtual public armarx::RobotHealthComponentUser
    // , virtual public armarx::LightweightRemoteGuiComponentPluginUser
    // , virtual public armarx::ArVizComponentPluginUser
    {
    public:
        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;
        SickLaserUnit();

    protected:
        /// @see PropertyUser::createPropertyDefinitions()
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        /// @see armarx::ManagedIceObject::onInitComponent()
        void onInitComponent() override;

        /// @see armarx::ManagedIceObject::onConnectComponent()
        void onConnectComponent() override;

        /// @see armarx::ManagedIceObject::onDisconnectComponent()
        void onDisconnectComponent() override;

        /// @see armarx::ManagedIceObject::onExitComponent()
        void onExitComponent() override;

    private:
        // Private methods go here.

    private:
        // Private member variables go here.

        /// Properties shown in the Scenario GUI.
        struct Properties
        {
            std::string topicName = "LaserScans";
            //scanner parameters
            std::string devices = "LaserScannerFront,192.168.8.133,2112";
            std::string scannerType = "sick_tim_5xx";
            int timelimit = 5;
            double rangeMin  = 0.0;
            double rangeMax = 10.0;
            int heartbeatWarnMS = 500;
            int heartbeatErrorMS = 800;
        };
        Properties properties;
        std::vector<SickLaserScanDevice> scanDevices;
        LaserScannerUnitListenerPrx topic;
        plugins::HeartbeatComponentPlugin* heartbeat = NULL;

    };
} // namespace armarx
