# SickLaserUnit
Armarx adapter to the SICK laser scanner driver

Uses the sick_scan_base driver to communicate with the SICK_TiM571 Laser Scanners installed in Armar-DE and translates their timestamps and data to ArmarX types.
After conversion, the scan data is published to the configured topic.


## Installation

1. Download and build the sick_scan_base project from https://github.com/SICKAG/sick_scan_base
   ```console
   git clone https://github.com/SICKAG/sick_scan_base.git

   cd sick_scan_base
   mkdir -p ./build
   cd ./build
   cmake .. && make
   ```

2. The package uses the script RobotAPI/etc/cmake/Findsick_scan_base.cmake to link against the sick drivers.
   For this to work, the CMake variable sick_scan_base_DIR must be set to the path where the driver has been downloaded to.

3. Build this package and configure its parameters to match the Laser Scanner setup.

## Runnig

To use the package, make sure your LaserScanners are connected to the PC and the configured IP addresses match the ones set by the SOPAS tool.

You can use ping to check whether the scanners are connected.
```console
ping 192.168.8.133
```

