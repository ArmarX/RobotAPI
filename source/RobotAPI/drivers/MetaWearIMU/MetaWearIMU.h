/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::MetaWearIMU
 * @author     Lukas Kaul ( lukas dot s dot kaul at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <RobotAPI/interface/units/MetaWearIMUInterface.h>
#include <RobotAPI/interface/units/MetaWearIMU.h>

namespace armarx
{
    /**
     * @class MetaWearIMUPropertyDefinitions
     * @brief
     */
    class MetaWearIMUPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        MetaWearIMUPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            defineOptionalProperty<std::string>("MetaWearPythonTopicName", "MetaWearPythonData", "Name of the topic provided by the python driver");
            defineOptionalProperty<std::string>("MetaWearTopicName", "MetaWearData", "Name of the MetaWear topic");
        }
    };

    /**
     * @defgroup Component-MetaWearIMU MetaWearIMU
     * @ingroup RobotAPI-Components
     * A description of the component MetaWearIMU.
     *
     * @class MetaWearIMU
     * @ingroup Component-MetaWearIMU
     * @brief Brief description of class MetaWearIMU.
     *
     * Detailed description of class MetaWearIMU.
     */
    class MetaWearIMU :
        virtual public armarx::Component,
        virtual public armarx::MetaWearIMUInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "MetaWearIMU";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        MetaWearIMUListenerPrx topicPrx;

        // MetaWearIMUInterface interface
    public:
        void reportIMUValues(const std::string& name, const MetaWearIMUDataExt& data_ext, const Ice::Current&) override;
    };
}

