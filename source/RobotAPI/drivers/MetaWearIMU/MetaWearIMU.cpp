/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::MetaWearIMU
 * @author     Lukas Kaul ( lukas dot s dot kaul at gmail dot com )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MetaWearIMU.h"

#include <ArmarXCore/observers/variant/TimestampVariant.h>


using namespace armarx;


void MetaWearIMU::onInitComponent()
{
    usingTopic(getProperty<std::string>("MetaWearPythonTopicName"));
    offeringTopic(getProperty<std::string>("MetaWearTopicName"));
}


void MetaWearIMU::onConnectComponent()
{
    topicPrx = getTopic<MetaWearIMUListenerPrx>(getProperty<std::string>("MetaWearTopicName").getValue());
}


void MetaWearIMU::onDisconnectComponent()
{

}


void MetaWearIMU::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr MetaWearIMU::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new MetaWearIMUPropertyDefinitions(
            getConfigIdentifier()));
}



void armarx::MetaWearIMU::reportIMUValues(const std::string& name, const MetaWearIMUDataExt& data_ext, const Ice::Current&)
{
    IceUtil::Time now = IceUtil::Time::now();
    TimestampVariantPtr nowTimestamp = new TimestampVariant(now);
    MetaWearIMUData data;
    data.acceleration = data_ext.acceleration;
    data.gyro = data_ext.gyro;
    data.magnetic = data_ext.magnetic;
    data.orientationQuaternion = data_ext.orientationQuaternion;
    topicPrx->reportIMUValues(name, data, nowTimestamp);
}
