/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::XsensIMU
 * @author     Markus Grotz ( markus-grotz at web dot de )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "XsensIMU.h"


namespace armarx
{
    using namespace IMU;

    PropertyDefinitionsPtr XsensIMU::createPropertyDefinitions()
    {
        return PropertyDefinitionsPtr(new XsensIMUPropertyDefinitions(getConfigIdentifier()));
    }

    void XsensIMU::frameAcquisitionTaskLoop()
    {

        std::vector<float> offset(3, 0.0);

        int count = 0;

        IceUtil::Time startTime = armarx::TimeUtil::GetTime();

        if (getProperty<bool>("EnableSimpleCalibration").getValue())
        {
            ARMARX_WARNING << "Estimation of the offset for the IMU, please do not move the IMU";
            while (((armarx::TimeUtil::GetTime() - startTime) < IceUtil::Time::seconds(5))  && !sensorTask->isStopped())
            {

                while (HasPendingEvents())
                {
                    ProcessPendingEvent();

                    offset[0] += m_GyroscopeRotation[0];
                    offset[1] += m_GyroscopeRotation[1];
                    offset[2] += m_GyroscopeRotation[2];

                    count ++;
                }
            }

            offset[0] /= count;
            offset[1] /= count;
            offset[2] /= count;

        }


        while (!sensorTask->isStopped())
        {

            while (HasPendingEvents())
            {

                ProcessPendingEvent();

                TimestampVariantPtr now = TimestampVariant::nowPtr();
                IMUData data;

                data.acceleration.push_back(m_Accelaration[0]);
                data.acceleration.push_back(m_Accelaration[1]);
                data.acceleration.push_back(m_Accelaration[2]);

                data.gyroscopeRotation.push_back(m_GyroscopeRotation[0] - offset[0]);
                data.gyroscopeRotation.push_back(m_GyroscopeRotation[1] - offset[1]);
                data.gyroscopeRotation.push_back(m_GyroscopeRotation[2] - offset[2]);


                data.magneticRotation.push_back(m_MagneticRotation[0]);
                data.magneticRotation.push_back(m_MagneticRotation[1]);
                data.magneticRotation.push_back(m_MagneticRotation[2]);


                data.orientationQuaternion.push_back(m_OrientationQuaternion[0]);
                data.orientationQuaternion.push_back(m_OrientationQuaternion[1]);
                data.orientationQuaternion.push_back(m_OrientationQuaternion[2]);
                data.orientationQuaternion.push_back(m_OrientationQuaternion[3]);

                IMUTopicPrx->reportSensorValues("XsensIMU", "XsensIMU", data, now);

            }

            usleep(10000);
        }
    }


    /*
    void XsensIMU::OnIMUCycle(const timeval& TimeStamp, const CIMUDevice* pIMUDevice)
    {
        const IMUState CurrentState = pIMUDevice->GetIMUState();

        TimestampVariantPtr now = TimestampVariant::nowPtr();
        IMUData data;

        data.acceleration.push_back(CurrentState.m_PhysicalData.m_Acceleration[0]);
        data.acceleration.push_back(CurrentState.m_PhysicalData.m_Acceleration[1]);
        data.acceleration.push_back(CurrentState.m_PhysicalData.m_Acceleration[2]);

        data.gyroscopeRotation.push_back(CurrentState.m_PhysicalData.m_GyroscopeRotation[0]);
        data.gyroscopeRotation.push_back(CurrentState.m_PhysicalData.m_GyroscopeRotation[1]);
        data.gyroscopeRotation.push_back(CurrentState.m_PhysicalData.m_GyroscopeRotation[2]);

        data.magneticRotation.push_back(CurrentState.m_PhysicalData.m_MagneticRotation[0]);
        data.magneticRotation.push_back(CurrentState.m_PhysicalData.m_MagneticRotation[1]);
        data.magneticRotation.push_back(CurrentState.m_PhysicalData.m_MagneticRotation[2]);

        data.orientationQuaternion.push_back(CurrentState.m_PhysicalData.m_OrientationQuaternion[0]);
        data.orientationQuaternion.push_back(CurrentState.m_PhysicalData.m_OrientationQuaternion[1]);
        data.orientationQuaternion.push_back(CurrentState.m_PhysicalData.m_OrientationQuaternion[2]);
        data.orientationQuaternion.push_back(CurrentState.m_PhysicalData.m_OrientationQuaternion[3]);

        IMUTopicPrx->reportSensorValues("XsensIMU", pIMUDevice->GetDeviceId(), data, now);

    }

    */

    void XsensIMU::onInitIMU()
    {
        sensorTask = new RunningTask<XsensIMU>(this, &XsensIMU::frameAcquisitionTaskLoop);

        SetDispatchingMode(IMU::IIMUEventDispatcher::eDecoupled);
        SetMaximalPendingEvents(5);

        //IMUDevice.SetFusion(IMU::CIMUDevice::eGaussianFusion, 2);
        IMUDevice.RegisterEventDispatcher(this);

        IMUDevice.Connect(_IMU_DEVICE_DEFAUL_CONNECTION_, IMU::CIMUDevice::eSamplingFrequency_120HZ);
    }

    void XsensIMU::onStartIMU()
    {
        IMUDevice.Start(false);
        sensorTask->start();
    }

    void XsensIMU::onExitIMU()
    {
        IMUDevice.Stop();
        sensorTask->stop();
    }
}
