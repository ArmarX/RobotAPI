/*
 * Xsens.h
 *
 *  Created on: Mar 17, 2014
 *      Author: gonzalez
 */

#pragma once

#include "../Includes.h"
#include "../IMUState.h"

#ifdef _IMU_USE_XSENS_DEVICE_

#include "XsensMTiModule.h"

namespace IMU::Xsens
{
    struct XsensMTiFrame
    {
        XsensMTiFrame() :
            m_DataLength(0)
        {
            memset(m_Data, 0, sizeof(unsigned char) * MAXMSGLEN);
        }

        short m_DataLength;
        unsigned char m_Data[MAXMSGLEN ];
        IMUState m_IMUState;
    };
}

#endif

