/*
 * IIMUEventDispatcher.h
 *
 *  Created on: Mar 16, 2014
 *      Author: Dr.-Ing. David Israel González Aguirre
 *      Mail:   david.gonzalez@kit.edu
 */

#pragma once

#include "Includes.h"
#include "IMUEvent.h"
#include "IMUHelpers.h"

namespace IMU
{
    class CIMUDevice;
    class IIMUEventDispatcher
    {
    public:

        enum DispatchingMode
        {
            eCoupled, eDecoupled
        };

        IIMUEventDispatcher(CIMUDevice* pIMUDevice);
        IIMUEventDispatcher();

        virtual ~IIMUEventDispatcher();

        void SetIMU(CIMUDevice* pIMUDevice);

        uint32_t GetEventFlags();

        void SetDispatchingMode(const DispatchingMode Mode);
        DispatchingMode GetDispatchingMode();

        void SetMaximalPendingEvents(const uint32_t MaximalPendingEvents);
        uint32_t GetMaximalPendingEvents();

        void SetEventHandling(const CIMUEvent::EventType Type, const bool Enabled);
        uint32_t GetEventHandlingFlags();

        void ReceiveEvent(const CIMUEvent& Event);

        inline uint32_t GetTotalPendingEvents()
        {
            return uint32_t(m_EventsQueue.size());
        }

        inline bool HasPendingEvents()
        {
            return m_EventsQueue.size();
        }

        bool ProcessPendingEvent();

        void SetReferenceTimeStamps(const timeval& Reference);

        timeval GetLastStartTimeStamp();
        timeval GetLastStopTimeStamp();
        timeval GetLastCycleReferenceTimeStamp();
        timeval GetLastFusedCycleReferenceTimeStamp();
        timeval GetLastIntegratedStateReferenceTimeStamp();
        timeval GetLastCustomEventReferenceTimeStamp();

        virtual void OnIMUEvent(const CIMUEvent& Event) = 0;

    private:

        void PurgeEvents();

        DispatchingMode m_DispatchingMode;
        pthread_mutex_t m_DispatchingModeMutex;
        uint32_t m_MaximalPendingEvents;
        pthread_mutex_t m_MaximalPendingEventsMutex;
        uint32_t m_EventFlags;
        pthread_mutex_t m_EventFlagsMutex;
        CIMUDevice* m_pIMUDevice;
        pthread_mutex_t m_IMUDeviceMutex;
        std::list<CIMUEvent> m_EventsQueue;
        pthread_mutex_t m_EventsQueueMutex;
        timeval m_LastStartTimeStamp;
        pthread_mutex_t m_LastStartTimeStampMutex;
        timeval m_LastStopTimeStamp;
        pthread_mutex_t m_LastStopTimeStampMutex;
        timeval m_LastCycleReferenceTimeStamp;
        pthread_mutex_t m_LastCycleReferenceTimeStampMutex;
        timeval m_LastFusedCycleReferenceTimeStamp;
        pthread_mutex_t m_LastFusedCycleReferenceTimeStampMutex;
        timeval m_LastIntegratedStateReferenceTimeStamp;
        pthread_mutex_t m_LastIntegratedStateReferenceTimeStampMutex;
        timeval m_LastCustomEventReferenceTimeStamp;
        pthread_mutex_t m_LastCustomEventReferenceTimeStampMutex;
    };
}

