/*
 * IMUHelpers.cpp
 *
 *  Created on: Mar 16, 2014
 *      Author: Dr.-Ing. David Israel González Aguirre
 *      Mail:   david.gonzalez@kit.edu
 */

#include "IMUHelpers.h"

namespace IMU
{
    const timeval CTimeStamp::s_Zero = { 0, 0 };

    const float CGeolocationInformation::s_G_LPoles = 9.832f;
    const float CGeolocationInformation::s_G_L45 = 9.806f;
    const float CGeolocationInformation::s_G_LEquator = 9.780f;
}
