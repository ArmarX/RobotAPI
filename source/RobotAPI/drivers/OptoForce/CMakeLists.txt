armarx_component_set_name("OptoForce")

find_package(OptoForceOMD QUIET)
armarx_build_if(OptoForceOMD_FOUND "OptoForceOMD not available 11elf!")

set(COMPONENT_LIBS ArmarXCore OMD)
set(SOURCES OptoForce.cpp)
set(HEADERS OptoForce.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

if(OptoForceOMD_FOUND)
    target_include_directories(OptoForce SYSTEM PUBLIC ${OptoForceOMD_INCLUDE_DIR})
endif()

# add unit tests
add_subdirectory(test)
