/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::OptoForceUnit
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <RobotAPI/interface/units/OptoForceUnit.h>
#include <opto.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

namespace armarx
{
    /**
     * @class OptoForceUnitPropertyDefinitions
     * @brief
     */
    class OptoForceUnitPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        OptoForceUnitPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            defineOptionalProperty<std::string>("OptoForceTopicName", "OptoForceValues", "Name of the OptoForce Topic");
            defineOptionalProperty<std::string>("CalibrationFilePath", "RobotAPI/sensors/OptoForceCalibration.xml", "Path of the Calibration File");
        }
    };

    /**
     * @defgroup Component-OptoForceUnit OptoForceUnit
     * @ingroup RobotAPI-Components
     * A description of the component OptoForceUnit.
     *
     * @class OptoForceUnit
     * @ingroup Component-OptoForceUnit
     * @brief Brief description of class OptoForceUnit.
     *
     * Detailed description of class OptoForceUnit.
     */
    class OptoForceUnit :
        virtual public OptoForceUnitInterface,
        virtual public armarx::Component
    {
    private:
        class DaqWrapper
        {
        public:
            DaqWrapper(const std::string& deviceName, const std::string& serialNumber, const RapidXmlReaderNode& daqNode);

            OptoDAQ daq;
            std::string deviceName;
            std::string serialNumber;
            std::vector<float> countsPerN;
            std::vector<std::string> sensorNames;
            std::vector<bool> enableFlags;
            std::vector<std::array<float, 3>> offsets;

            void printInfo();
            void checkSensorCount();
        };
        using DaqWrapperPtr = std::shared_ptr<DaqWrapper>;

    public:
        OptoForceUnit();

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "OptoForceUnit";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        void run();

        OptoForceUnitListenerPrx topicPrx;

        OptoPorts ports;
        //OptoDAQ daq;
        std::vector<DaqWrapperPtr> daqList;
        RunningTask<OptoForceUnit>::pointer_type readTask;

        std::ofstream recordingFile;
        bool isRecording;
        bool stopRecordingFlag;



        // OptoForceUnitInterface interface
    public:
        void startRecording(const std::string& filepath, const Ice::Current&) override;
        void stopRecording(const Ice::Current&) override;
    };
}

