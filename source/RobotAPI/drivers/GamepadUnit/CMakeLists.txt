armarx_component_set_name("GamepadUnit")

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore ArmarXCoreEigen3Variants RobotAPIInterfaces)

set(SOURCES GamepadUnit.cpp)
set(HEADERS Joystick.h GamepadUnit.h)

armarx_add_component("${SOURCES}" "${HEADERS}")

# add unit tests
add_subdirectory(test)

armarx_generate_and_add_component_executable(APPLICATION_APP_SUFFIX)
