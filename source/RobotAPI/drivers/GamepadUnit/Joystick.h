/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::GamepadUnit
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include<linux/joystick.h>
#include<sys/stat.h>
#include<fcntl.h>

#include <ArmarXCore/core/Component.h>

namespace armarx
{

    class Joystick
    {

    private:
        int fd = -1;
        js_event event;

    public:

        std::vector<int16_t> axis;
        std::vector<bool> buttonsPressed;
        int numberOfAxis;
        int numberOfButtons;
        std::string name;

        bool open(std::string const& deviceName)
        {
            fd = ::open(deviceName.c_str(), O_RDONLY);
            if (fd != -1)
            {
                ioctl(fd, JSIOCGAXES, &numberOfAxis);
                ioctl(fd, JSIOCGBUTTONS, &numberOfButtons);
                name.resize(255);
                ioctl(fd, JSIOCGNAME(255), &name[0]);
                axis.resize(numberOfAxis, 0);
                name = name.c_str();
                buttonsPressed.resize(numberOfButtons, false);
            }
            return fd != -1;
        }

        bool opened() const
        {
            return fd != -1;
        }

        bool pollEvent()
        {
            int bytes = read(fd, &event, sizeof(event));

            // NOTE if this condition is not met, we're probably out of sync and this
            // Joystick instance is likely unusable
            if (bytes == -1 || bytes != sizeof(event))
            {
                return false;
            }

            if (event.type & JS_EVENT_BUTTON)
            {
                buttonsPressed[event.number] = event.value != 0;
            }
            else if (event.type & JS_EVENT_AXIS)
            {
                axis[event.number] = event.value;
            }
            return true;
        }

        void close()
        {
            ::close(fd);
            fd = -1;
        }
    };
}
