/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::GamepadUnit
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include<linux/joystick.h>
#include<sys/stat.h>
#include<fcntl.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>

#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <RobotAPI/interface/units/GamepadUnit.h>

#include "Joystick.h"

namespace armarx
{
    /**
     * @class GamepadUnitPropertyDefinitions
     * @brief
     */
    class GamepadUnitPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        GamepadUnitPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
            defineOptionalProperty<std::string>("GamepadTopicName", "GamepadValues", "Name of the Gamepad Topic");
            defineOptionalProperty<std::string>("GamepadDeviceName", "/dev/input/js2", "device that will be opened as a gamepad");
            defineOptionalProperty<int>("PublishTimeout", 2000, "In Milliseconds. Timeout after which the gamepad data is not published after, if no new data was read from the gamepad");
        }
    };

    /**
     * @defgroup Component-GamepadUnit GamepadUnit
     * @ingroup RobotAPI-Components
     * A description of the component GamepadUnit.
     *
     * @class GamepadUnit
     * @ingroup Component-GamepadUnit
     * @brief Brief description of class GamepadUnit.
     *
     * Detailed description of class GamepadUnit.
     */
    class GamepadUnit :
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "GamepadUnit";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        bool openGamepadConnection();

    private:
        GamepadUnitListenerPrx topicPrx;
        RunningTask<GamepadUnit>::pointer_type readTask;
        SimplePeriodicTask<>::pointer_type sendTask;

        void run();
        std::mutex mutex;
        std::string deviceName;
        Joystick js;
        GamepadData data;
        TimestampVariantPtr dataTimestamp;
    };
}

