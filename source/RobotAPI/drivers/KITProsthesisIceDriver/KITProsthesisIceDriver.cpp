#include "KITProsthesisIceDriver.h"
#include <iostream>
#include <memory>

using namespace KITProsthesis;

void KITProsthesisIceDriver::sendGrasp(Ice::Int n, const Ice::Current&)
{
    iface.sendGrasp(n);
}
void KITProsthesisIceDriver::sendThumbPWM(const ProsthesisMotorValues& motorValues, const Ice::Current&)
{
    iface.sendThumbPWM(motorValues.v, motorValues.maxPWM, motorValues.pos);
}

void KITProsthesisIceDriver::sendFingerPWM(const ProsthesisMotorValues& motorValues, const Ice::Current&)
{
    iface.sendFingerPWM(motorValues.v, motorValues.maxPWM, motorValues.pos);
}

ProsthesisSensorValues KITProsthesisIceDriver::getSensorValues(const Ice::Current&)
{
    ProsthesisSensorValues value;
    switch (iface.getState())
    {
        case BLEProthesisInterface::State::Created:
            value.state = ProsthesisState::Created;
            break;
        case BLEProthesisInterface::State::DiscoveringDevices:
            value.state = ProsthesisState::DiscoveringDevices;
            break;
        case BLEProthesisInterface::State::DiscoveringDevicesDone:
            value.state = ProsthesisState::DiscoveringDevicesDone;
            break;
        case BLEProthesisInterface::State::Disconnected:
            value.state = ProsthesisState::Disconnected;
            break;
        case BLEProthesisInterface::State::Connecting:
            value.state = ProsthesisState::Connecting;
            break;
        case BLEProthesisInterface::State::ConnectingDone:
            value.state = ProsthesisState::ConnectingDone;
            break;
        case BLEProthesisInterface::State::DiscoveringServices:
            value.state = ProsthesisState::DiscoveringServices;
            break;
        case BLEProthesisInterface::State::DiscoveringServicesDone:
            value.state = ProsthesisState::DiscoveringServicesDone;
            break;
        case BLEProthesisInterface::State::ConnectingService:
            value.state = ProsthesisState::ConnectingService;
            break;
        case BLEProthesisInterface::State::Running:
            value.state = ProsthesisState::Running;
            break;
        case BLEProthesisInterface::State::Killed:
            value.state = ProsthesisState::Killed;
            break;
    }

    value.thumbPWM = iface.getThumbPWM();
    value.thumbPos = iface.getThumbPos();

    value.fingerPWM = iface.getFingerPWM();
    value.fingerPos = iface.getFingerPos();

    //add IMU here
    return value;
}
