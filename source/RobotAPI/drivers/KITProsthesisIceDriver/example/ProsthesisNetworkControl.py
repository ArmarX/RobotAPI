#!/usr/bin/python
import sys, traceback, Ice
import IceStorm
import time

#from bmp280 import BMP280


def main():
    status = 0
    ic = None
    prosthesis = None
    ########################################################## ICE part
    try:
        properties = Ice.createProperties(sys.argv)
        properties.load("./default.generated.cfg")
        properties.load("./default.cfg")

        init_data = Ice.InitializationData()
        init_data.properties = properties

        ic = Ice.initialize(init_data)

        Ice.loadSlice('-I%s %s -DMININTERFACE' % (Ice.getSliceDir(), 'KITProstheticHandInterface.ice'))

        print("loadSlice")
        import KITProsthesis


        obj = ic.stringToProxy("IceStorm/TopicManager")
        
        print("stringToProxy")
        base = ic.stringToProxy("KITProsthesisControl -t -e 1.1:tcp -h localhost -p 10000 -t 60000")
        print("base = ", base.ice_toString())
        print("cast proxy")
        prosthesis = KITProsthesis.KITProstheticHandInterfacePrx.checkedCast(base)
        if not prosthesis:
                raise RuntimeError("Invalid proxy")
   
    except:
        traceback.print_exc()
        status = 1

    if prosthesis is not None:
        def graspAndStatus(grasp, sleep = 3):
            prosthesis.sendGrasp(grasp)
            val = prosthesis.getSensorValues()
            print("state     ", val.state)
            print("thumbPWM  ", val.thumbPWM)
            print("thumbPos  ", val.thumbPos)
            print("fingerPWM ", val.fingerPWM)
            print("fingerPos ", val.fingerPos)
            time.sleep(sleep)    

        graspAndStatus(0)
        graspAndStatus(1)
        graspAndStatus(0)

    if ic:
        #clean up
        try:
            ic.destroy()
        except:
            traceback.print_exc()
            status = 1

    sys.exit(status)

main()
