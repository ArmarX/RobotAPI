/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::::drivers::KITProsthesisIceDriver
 * @author     Julia Starke ( julia dot starke at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/KITProstheticHandInterface.h>
#include <RobotAPI/drivers/KITProstheticHandDriver/BLEProthesisInterface.h>
#include <Ice/Ice.h>
#include <Ice/Object.h>
#include <QCoreApplication>
#include <iostream>
#include <thread>

[[maybe_unused]] static constexpr auto prosthesis = "CB:43:34:8F:3C:0A";
[[maybe_unused]] static constexpr auto prosthesis_old = "DF:70:E8:81:DB:D6";

class KITProsthesisIceDriver:
    virtual public KITProsthesis::KITProstheticHandInterface
{
public:
    KITProsthesisIceDriver(const std::string& mac = prosthesis_old) : iface{mac}
    {}

    // sender interface
    void sendGrasp(Ice::Int, const Ice::Current& = Ice::emptyCurrent) override;
    void sendThumbPWM(const KITProsthesis::ProsthesisMotorValues&, const ::Ice::Current& = ::Ice::emptyCurrent) override;
    void sendFingerPWM(const KITProsthesis::ProsthesisMotorValues&, const ::Ice::Current& = ::Ice::emptyCurrent) override;

    KITProsthesis::ProsthesisSensorValues getSensorValues(const Ice::Current&) override;
private:
    BLEProthesisInterface iface;

};
