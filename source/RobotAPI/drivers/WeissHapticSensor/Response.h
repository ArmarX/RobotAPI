/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "Types.h"
#include "TransmissionException.h"
//#include <strstream>
#include <stdexcept>
#include <boost/format.hpp>
#include <vector>
#include <ArmarXCore/core/logging/Logging.h>

struct Response
{
public:
    Response(int res, unsigned char cmdId, status_t status, const std::vector<unsigned char>& data, unsigned int len)
        : res(res), cmdId(cmdId), status(status), data(data), len(len) {}

    unsigned int getUInt(int index)
    {
        return (unsigned int)data[index] | ((unsigned int)data[index + 1] << 8) | ((unsigned int)data[index + 2] << 16) | ((unsigned int)data[index + 3] << 24);
    }

    unsigned short getShort(int index)
    {
        return (unsigned short)data[index] | ((unsigned short)data[index + 1] << 8);
    }
    unsigned char getByte(int index)
    {
        return data[index];
    }

    void ensureMinLength(int len)
    {
        if (res < len)
        {
            //std::strstream strStream;
            //strStream << "Response length is too short, should be = " << len << " (is " << res << ")";
            //throw std::runtime_error(strStream.str());
            throw TransmissionException(str(boost::format("Response length is too short, should be = %1% (is %2%)") % len % res));
        }
    }

    void ensureSuccess()
    {
        if (status != E_SUCCESS)
        {
            //std::strstream strStream;
            //strStream << "Command not successful: " << status_to_str( status );
            //throw std::runtime_error(strStream.str());
            std::stringstream ss;
            ss << " status != E_SUCCESS";

            for (int i = 0; i < (int)len; i++)
            {
                ss << boost::format("%02X ") % (int)data[i];
            }

            ARMARX_ERROR_S << ss.str();
            throw TransmissionException(str(boost::format("Command not successful: %1% (0x%2$02X)") % status_to_str(status) % status));
        }
    }

    int res;
    unsigned char cmdId;
    status_t status;
    std::vector<unsigned char> data;
    unsigned int len;

    static const char* status_to_str(status_t status)
    {
        switch (status)
        {
            case E_SUCCESS:
                return ("No error");

            case E_NOT_AVAILABLE:
                return ("Service or data is not available");

            case E_NO_SENSOR:
                return ("No sensor connected");

            case E_NOT_INITIALIZED:
                return ("The device is not initialized");

            case E_ALREADY_RUNNING:
                return ("Service is already running");

            case E_FEATURE_NOT_SUPPORTED:
                return ("The requested feature is not supported");

            case E_INCONSISTENT_DATA:
                return ("One or more dependent parameters mismatch");

            case E_TIMEOUT:
                return ("Timeout error");

            case E_READ_ERROR:
                return ("Error while reading from a device");

            case E_WRITE_ERROR:
                return ("Error while writing to a device");

            case E_INSUFFICIENT_RESOURCES:
                return ("No memory available");

            case E_CHECKSUM_ERROR:
                return ("Checksum error");

            case E_NO_PARAM_EXPECTED:
                return ("No parameters expected");

            case E_NOT_ENOUGH_PARAMS:
                return ("Not enough parameters");

            case E_CMD_UNKNOWN:
                return ("Unknown command");

            case E_CMD_FORMAT_ERROR:
                return ("Command format error");

            case E_ACCESS_DENIED:
                return ("Access denied");

            case E_ALREADY_OPEN:
                return ("Interface already open");

            case E_CMD_FAILED:
                return ("Command failed");

            case E_CMD_ABORTED:
                return ("Command aborted");

            case E_INVALID_HANDLE:
                return ("Invalid handle");

            case E_NOT_FOUND:
                return ("Device not found");

            case E_NOT_OPEN:
                return ("Device not open");

            case E_IO_ERROR:
                return ("General I/O-Error");

            case E_INVALID_PARAMETER:
                return ("Invalid parameter");

            case E_INDEX_OUT_OF_BOUNDS:
                return ("Index out of bounds");

            case E_CMD_PENDING:
                return ("Command is pending...");

            case E_OVERRUN:
                return ("Data overrun");

            case E_RANGE_ERROR:
                return ("Value out of range");

            case E_AXIS_BLOCKED:
                return ("Axis is blocked");

            case E_FILE_EXISTS:
                return ("File already exists");

            default:
                return ("Internal error. Unknown error code.");
        }
    }
};

