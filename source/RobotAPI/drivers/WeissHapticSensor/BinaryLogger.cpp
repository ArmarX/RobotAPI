/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "BinaryLogger.h"
#include <boost/format.hpp>

BinaryLogger::BinaryLogger(std::string filename)
{
    this->log.open(filename.c_str());
}

BinaryLogger::~BinaryLogger()
{
    log.close();
}

void BinaryLogger::logRead(unsigned char* buf, unsigned int len)
{
    log << "READ";

    for (unsigned int i = 0; i < len; i++)
    {
        log << boost::format(" %02X") % (int)buf[i];
    }

    log << std::endl;
    log.flush();
}

void BinaryLogger::logWrite(unsigned char* buf, unsigned int len)
{
    log << "WRITE";

    for (unsigned int i = 0; i < len; i++)
    {
        log << boost::format(" %02X") % (int)buf[i];
    }

    log << std::endl;
    log.flush();
}

void BinaryLogger::logText(std::string message)
{
    log << message << std::endl;
    log.flush();
}
