/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "SerialInterface.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <iostream>
#include <boost/format.hpp>

static inline tcflag_t __bitrate_to_flag(unsigned int bitrate)
{
    switch (bitrate)
    {
        case   1200:
            return   B1200;

        case   2400:
            return   B2400;

        case   4800:
            return   B4800;

        case   9600:
            return   B9600;

        case  19200:
            return  B19200;

        case  38400:
            return  B38400;

        case  57600:
            return  B57600;

        case 115200:
            return B115200;

        case 230400:
            return B230400;

        case 460800:
            return B460800;

        default:
            return 0;
    }
}


SerialInterface::SerialInterface(const char* device, unsigned int bitrate)
{
    this->device = device;
    this->bitrate = bitrate;
    this->fd = -1;
}

SerialInterface::~SerialInterface()
{

}

int SerialInterface::open()
{
    // Convert bitrate to flag
    tcflag_t bitrate = __bitrate_to_flag(this->bitrate);

    if (bitrate == 0)
    {
        fprintf(stderr, "Invalid bitrate '%d' for serial device\n", this->bitrate);
        return -1;
    }


    // Open serial device
    fd = ::open(device, O_RDWR | O_NOCTTY);

    if (fd < 0)
    {
        fprintf(stderr, "Failed to open serial device '%s' (errno: %s)\n", device, strerror(errno));
        return -1;
    }

    if (::ioctl(fd, TIOCEXCL))
    {
        fprintf(stderr, "Failed to lock serial device '%s' (errno: %s)\n", device, strerror(errno));
        return -1;
    }



    // Check if device is a terminal device
    if (!isatty(fd))
    {
        fprintf(stderr, "Device '%s' is not a terminal device (errno: %s)!\n", device, strerror(errno));
        ::close(fd);
        return -1;
    }

    struct termios settings;

    // Set input flags
    settings.c_iflag =  IGNBRK          // Ignore BREAKS on Input
                        |  IGNPAR;         // No Parity

    // ICRNL: map CR to NL (otherwise a CR input on the other computer will not terminate input)

    // Set output flags
    settings.c_oflag = 0;               // Raw output

    // Set controlflags
    settings.c_cflag = bitrate
                       | CS8              // 8 bits per byte
                       | CSTOPB           // Stop bit
                       | CREAD            // characters may be read
                       | CLOCAL;          // ignore modem state, local connection

    // Set local flags
    settings.c_lflag = 0;               // Other option: ICANON = enable canonical input

    // Set maximum wait time on input - cf. Linux Serial Programming HowTo, non-canonical mode
    // http://tldp.org/HOWTO/Serial-Programming-HOWTO/x115.html
    settings.c_cc[VTIME] = 10;          // 0 means timer is not uses

    // Set minimum bytes to read
    settings.c_cc[VMIN]  = 0;           // 1 means wait until at least 1 character is received

    // Now clean the modem line and activate the settings for the port
    tcflush(fd, TCIFLUSH);

    tcsetattr(fd, TCSANOW, &settings);

    connected = true;

    return 0;
}

void SerialInterface::close()
{
    if (connected)
    {
        ::close(fd);
    }

    connected = false;
}

int SerialInterface::readInternal(unsigned char* buf, unsigned int len)
{
    int res;

    res = blockingReadAll(buf, len);

    if (res < 0)
    {
        std::cerr << "Failed to read from serial device" << std::endl;
    }

    return res;

}

int SerialInterface::blockingReadAll(unsigned char* buf, unsigned int len)
{
    int dataToRead = len;

    while (1)
    {
        int res = ::read(fd, buf, dataToRead);

        if (res < 0)
        {
            return res;
        }

        dataToRead -= res;
        buf += res;

        if (dataToRead == 0)
        {
            return len;
        }

        if (dataToRead < 0)
        {
            throw new std::runtime_error("Internal error: dataToRead < 0");
        }

        usleep(1);
    }
}

int SerialInterface::writeInternal(unsigned char* buf, unsigned int len)
{
    return (::write(fd, (void*) buf, len));
}


std::string SerialInterface::toString() const
{
    return str(boost::format("SerialInterface(connected=%1%, device=%2%, bitrate=%3%, fd=%4%)")
               % connected % device % bitrate % fd);
}
