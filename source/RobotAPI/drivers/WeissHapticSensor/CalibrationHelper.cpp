/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "CalibrationHelper.h"

#include <stdexcept>

using namespace armarx;

CalibrationHelper::CalibrationHelper(int rows, int cols, float noiseThreshold)
{
    this->maximumValues = Eigen::MatrixXf::Zero(rows, cols);
    this->noiseThreshold = noiseThreshold;
}

void CalibrationHelper::addNoiseSample(Eigen::MatrixXf data)
{
    this->noiseSamples.push_back(data);
}

bool CalibrationHelper::addMaxValueSample(Eigen::MatrixXf data)
{
    if (data.maxCoeff() <= noiseThreshold)
    {
        this->maximumValues = this->maximumValues.cwiseMax(data);
        return true;
    }
    else
    {
        return false;
    }
}

CalibrationInfo CalibrationHelper::getCalibrationInfo(float calibratedMinimum, float calibratedMaximum)
{
    return CalibrationInfo(getMatrixAverage(noiseSamples), maximumValues, calibratedMinimum, calibratedMaximum);
}

bool CalibrationHelper::checkMaximumValueThreshold(float threshold)
{
    return this->maximumValues.minCoeff() >= threshold;
}

Eigen::MatrixXf CalibrationHelper::getMaximumValues()
{
    return this->maximumValues;
}

int CalibrationHelper::getNoiseSampleCount()
{
    return this->noiseSamples.size();
}

Eigen::MatrixXf CalibrationHelper::getMatrixAverage(std::vector<Eigen::MatrixXf> samples)
{
    if (samples.size() == 0)
    {
        throw std::runtime_error("Average of zero samples not possible");
    }

    Eigen::MatrixXf sum = samples.at(0);

    for (std::vector<Eigen::MatrixXf>::iterator it = samples.begin() + 1; it != samples.end(); ++it)
    {
        sum += *it;
    }

    return sum / (float)samples.size();
}
