/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "AbstractInterface.h"
#include <iostream>


class SerialInterface : public AbstractInterface
{
public:
    SerialInterface(const char* device, const unsigned int bitrate);
    ~SerialInterface() override;

    int open() override;
    void close() override;

    std::string toString() const override;

protected:
    int readInternal(unsigned char*, unsigned int) override;
    int writeInternal(unsigned char*, unsigned int) override;

private:
    const char* device;
    unsigned int bitrate;
    int fd;

    int blockingReadAll(unsigned char* buf, unsigned int len);
};

