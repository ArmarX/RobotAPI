#include "BLEProthesisInterfaceQtWorkerThread.h"


BLEProthesisInterfaceQtWorkerThread::BLEProthesisInterfaceQtWorkerThread(const std::string& mac, BLEProthesisInterface& owner) :
    _owner{&owner},
    _mac{QString::fromStdString(mac)}
{}

BLEProthesisInterfaceQtWorkerThread::~BLEProthesisInterfaceQtWorkerThread()
    = default;

void BLEProthesisInterfaceQtWorkerThread::kill()
{
    _worker->kill();
}

void BLEProthesisInterfaceQtWorkerThread::sendCommand(const std::string& cmd)
{
    _worker->sendCommand(cmd);
}

void BLEProthesisInterfaceQtWorkerThread::run()
{
    _worker = new BLEProthesisInterfaceQtWorker{_mac, *_owner};
    qDebug() << '[' << _mac << ']' << " Starting qt event loop.";
    int r = exec();
    if (_worker)
    {
        _worker->kill();
        delete _worker;
    }
    qDebug() << '[' << _mac << ']' << " Sopped qt event loop -> " << r;
}
