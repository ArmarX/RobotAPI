#pragma once

#include <mutex>

#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothDeviceInfo>
#include <QLowEnergyController>
#include <QLowEnergyService>

#include "BLEProthesisInterface.h"

#include <QThread>


class BLEProthesisInterfaceQtWorker : public QThread
{
    Q_OBJECT
public:
    using State = BLEProthesisInterface::State;
    BLEProthesisInterfaceQtWorker(const QString& mac, BLEProthesisInterface& owner);
    ~BLEProthesisInterfaceQtWorker();

    void kill();
    void sendCommand(const std::string& cmd);
private:
    void cleanup();
protected:
    void timerEvent(QTimerEvent* event) override;
signals:
    void resultReady(const QString& s);
private slots:
    //dev discover
    void deviceDiscovered(const QBluetoothDeviceInfo& device);
    void deviceDiscoverFinished();
    void deviceDiscoverError(QBluetoothDeviceDiscoveryAgent::Error);
    void deviceConnected();
    void deviceDisconnected();
    //service discovery
    void serviceDiscovered(const QBluetoothUuid& gatt);
    void serviceDiscoverFinished();
    void controllerError(QLowEnergyController::Error error);
    //communication
    void receiveDeviceDisconnec(const QLowEnergyDescriptor& d, const QByteArray& value);

    void serviceStateChanged(QLowEnergyService::ServiceState s);
    void readData(const QLowEnergyCharacteristic& c, const QByteArray& value);
private:
    template<BLEProthesisInterface::SensorValueProtocol P> void consumeData();

    BLEProthesisInterface*              _owner;
    QString                             _mac;

    //management (sensor/control)
    int                                 _timer{-1};
    std::atomic_bool                    _killed{false};

    QString                             _valueAkk;

    QString                             _cmd;
    std::mutex                          _cmdMutex;
    //ble discover
    QBluetoothDeviceDiscoveryAgent*     _deviceDiscoveryAgent{nullptr};
    QBluetoothDeviceInfo                _currentDevice;
    bool                                _deviceDiscovered{false};
    //ble dev
    QLowEnergyController*               _control{nullptr};
    //ble ser
    bool                                _serviceDiscovered{false};
    QLowEnergyDescriptor                _notificationDescTx;
    QLowEnergyService*                  _service{nullptr};
};
