#include <sstream>

#include "BLEProthesisInterface.h"
#include "BLEProthesisInterfaceQtWorkerThread.h"

BLEProthesisInterface::BLEProthesisInterface(const std::string& mac, SensorValueProtocol protocol) :
    _worker{new BLEProthesisInterfaceQtWorkerThread{mac, *this}},
        _protocol{protocol}
{
    _worker->start();
}

BLEProthesisInterface::~BLEProthesisInterface()
{
    _worker->kill();
    _worker->quit();
    _worker->exit();
    _worker->wait(1000); // ms
    _worker->terminate();
    _worker->wait();
}

std::int64_t BLEProthesisInterface::getThumbPWM() const
{
    return _thumbPWM;
}

std::int64_t BLEProthesisInterface::getThumbPos() const
{
    return _thumbPos;
}

std::int64_t BLEProthesisInterface::getFingerPWM() const
{
    return _fingerPWM;
}

std::int64_t BLEProthesisInterface::getFingerPos() const
{
    return _fingerPos;
}

BLEProthesisInterface::State BLEProthesisInterface::getState() const
{
    return _state;
}

void BLEProthesisInterface::sendRaw(const std::string& cmd)
{
    if (_state != State::Running)
    {
        throw std::invalid_argument
        {
            "BLEProthesisInterface::sendRaw( cmd = " + cmd +
            " ): The UART service is not connected!"
        };
    }
    _worker->sendCommand(cmd);
}

void BLEProthesisInterface::sendGrasp(std::uint64_t n)
{
    if (n > getMaxG())
    {
        throw std::invalid_argument
        {
            "BLEProthesisInterface::sendGrasp( n = " + std::to_string(n) +
            " ): the maximal value for n is " + std::to_string(getMaxG())
        };
    }
    sendRaw("g" + std::to_string(n) + "\n");
}

void BLEProthesisInterface::sendThumbPWM(uint64_t v, uint64_t mxPWM, uint64_t pos)
{
    if (v < getMinV() || v > getMaxV())
    {
        throw std::invalid_argument
        {
            "sendThumbPWM( v = " + std::to_string(v) +
            " , mxPWM = " + std::to_string(mxPWM) +
            " , pos = " + std::to_string(pos) +
            " ): The interval for v is [" + std::to_string(getMinV()) +
            ", " + std::to_string(getMaxV()) + "]"
        };
    }
    if (mxPWM > getMaxPWM())
    {
        throw std::invalid_argument
        {
            "sendThumbPWM( v = " + std::to_string(v) +
            " , mxPWM = " + std::to_string(mxPWM) +
            " , pos = " + std::to_string(pos) +
            " ): The interval for mxPWM is [0, , " +
            std::to_string(getMaxPWM()) + "]"
        };
    }
    if (pos > getMaxPosThumb())
    {
        throw std::invalid_argument
        {
            "sendThumbPWM( v = " + std::to_string(v) +
            " , mxPWM = " + std::to_string(mxPWM) +
            " , pos = " + std::to_string(pos) +
            " ): The interval for pos is [0, " +
            std::to_string(getMaxPosThumb()) + "]"
        };
    }
    std::stringstream str;
    str << 'M' << 2 << ',' << v << ',' << mxPWM << ',' << pos << '\n';
    sendRaw(str.str());
}

void BLEProthesisInterface::sendFingerPWM(uint64_t v, uint64_t mxPWM, uint64_t pos)
{
    if (v < getMinV() || v > getMaxV())
    {
        throw std::invalid_argument
        {
            "sendThumbPWM( v = " + std::to_string(v) +
            " , mxPWM = " + std::to_string(mxPWM) +
            " , pos = " + std::to_string(pos) +
            " ): The interval for v is [" + std::to_string(getMinV()) +
            ", " + std::to_string(getMaxV()) + "]"
        };
    }
    if (mxPWM > getMaxPWM())
    {
        throw std::invalid_argument
        {
            "sendThumbPWM( v = " + std::to_string(v) +
            " , mxPWM = " + std::to_string(mxPWM) +
            " , pos = " + std::to_string(pos) +
            " ): The interval for mxPWM is [0, , " +
            std::to_string(getMaxPWM()) + "]"
        };
    }
    if (pos > getMaxPosFingers())
    {
        throw std::invalid_argument
        {
            "sendThumbPWM( v = " + std::to_string(v) +
            " , mxPWM = " + std::to_string(mxPWM) +
            " , pos = " + std::to_string(pos) +
            " ): The interval for pos is [0, " +
            std::to_string(getMaxPosFingers()) + "]"
        };
    }
    std::stringstream str;
    str << 'M' << 3 << ',' << v << ',' << mxPWM << ',' << pos << '\n';
    sendRaw(str.str());
}

void BLEProthesisInterface::verboseReceive(bool b)
{
    _verboseReceive = b;
}

void BLEProthesisInterface::verboseSend(bool b)
{
    _verboseSend = b;
}

std::string to_string(BLEProthesisInterface::State s)
{
    switch (s)
    {
        case BLEProthesisInterface::State::Created:
            return "Created";
        case BLEProthesisInterface::State::DiscoveringDevices:
            return "DiscoveringDevices";
        case BLEProthesisInterface::State::DiscoveringDevicesDone:
            return "DiscoveringDevicesDone";
        case BLEProthesisInterface::State::Disconnected:
            return "Disconnected";
        case BLEProthesisInterface::State::Connecting:
            return "Connecting";
        case BLEProthesisInterface::State::ConnectingDone:
            return "ConnectingDone";
        case BLEProthesisInterface::State::DiscoveringServices:
            return "DiscoveringServices";
        case BLEProthesisInterface::State::DiscoveringServicesDone:
            return "DiscoveringServicesDone";
        case BLEProthesisInterface::State::ConnectingService:
            return "ConnectingService";
        case BLEProthesisInterface::State::Running:
            return "Running";
        case BLEProthesisInterface::State::Killed:
            return "Killed";
    }
    return "UNKNOWN VALUE FOR BLEProthesisInterface::State: " + std::to_string(static_cast<int>(s));
}
