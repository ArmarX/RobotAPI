#pragma once

#include <atomic>
#include <string>
#include <memory>

class BLEProthesisInterfaceQtWorkerThread;
class BLEProthesisInterfaceQtWorker;

class BLEProthesisInterface
{
public:
    enum class State
    {
        Created,
        DiscoveringDevices,
        DiscoveringDevicesDone,
        Disconnected,
        Connecting,
        ConnectingDone,
        DiscoveringServices,
        DiscoveringServicesDone,
        ConnectingService,
        Running,
        Killed
    };
    enum class SensorValueProtocol
    {
        tpos_tpwm_fpos_fpwm,
        mx_pos_pwm
    };
public:
    BLEProthesisInterface(const std::string& mac, SensorValueProtocol protocol = SensorValueProtocol::mx_pos_pwm);
    ~BLEProthesisInterface();

    std::int64_t getThumbPWM() const;
    std::int64_t getThumbPos() const;
    std::int64_t getFingerPWM() const;
    std::int64_t getFingerPos() const;

    State getState() const;

    void sendRaw(const std::string& cmd);

    void sendGrasp(std::uint64_t n);
    void sendThumbPWM(std::uint64_t v, std::uint64_t maxPWM, std::uint64_t pos);
    void sendFingerPWM(std::uint64_t v, std::uint64_t maxPWM, std::uint64_t pos);

    void verboseReceive(bool b = true);
    void verboseSend(bool b = true);
private:
    friend class BLEProthesisInterfaceQtWorker;
    //sensor values
    std::atomic_int64_t _thumbPWM{0};
    std::atomic_int64_t _thumbPos{0};
    std::atomic_int64_t _fingerPWM{0};
    std::atomic_int64_t _fingerPos{0};

    //management
    std::unique_ptr<BLEProthesisInterfaceQtWorkerThread> _worker;
    std::atomic<State> _state{State::Created};
    std::atomic_bool _verboseReceive{false};
    std::atomic_bool _verboseSend{true};
    SensorValueProtocol _protocol;
public:
    static constexpr std::uint64_t getMaxG()
    {
        return 8;
    }
    static constexpr std::uint64_t getMinV()
    {
        return 10;
    }
    static constexpr std::uint64_t getMaxV()
    {
        return 200;
    }
    static constexpr std::uint64_t getMaxPWM()
    {
        return  2999;
    }
    static constexpr std::uint64_t getMaxPosThumb()
    {
        return 100'000;
    }
    static constexpr std::uint64_t getMaxPosFingers()
    {
        return 200'000;
    }
};

std::string to_string(BLEProthesisInterface::State s);
