#pragma once

#include <mutex>

#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothDeviceInfo>
#include <QLowEnergyController>
#include <QLowEnergyService>

#include "BLEProthesisInterface.h"
#include "BLEProthesisInterfaceQtWorker.h"

#include <QThread>


class BLEProthesisInterfaceQtWorkerThread : public QThread
{
    Q_OBJECT
public:
    BLEProthesisInterfaceQtWorkerThread(const std::string& mac, BLEProthesisInterface& owner);
    ~BLEProthesisInterfaceQtWorkerThread();
    void kill();
    void sendCommand(const std::string& cmd);
private:
    void run() override;
private:
    BLEProthesisInterface*              _owner{nullptr};
    QString                             _mac;
    BLEProthesisInterfaceQtWorker*      _worker{nullptr};
};
