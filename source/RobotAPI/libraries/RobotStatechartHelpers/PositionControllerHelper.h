/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Dense>

#include "VelocityControllerHelper.h"

#include <VirtualRobot/Robot.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/libraries/core/Pose.h>

namespace Eigen
{
    using Vector6f = Matrix<float, 6, 1>;
}


namespace armarx
{

    class PositionControllerHelper;
    using PositionControllerHelperPtr = std::shared_ptr<PositionControllerHelper>;

    class PositionControllerHelper
    {
    public:
        PositionControllerHelper(const VirtualRobot::RobotNodePtr& tcp, const VelocityControllerHelperPtr& velocityControllerHelper, const Eigen::Matrix4f& target);
        PositionControllerHelper(const VirtualRobot::RobotNodePtr& tcp, const VelocityControllerHelperPtr& velocityControllerHelper, const std::vector<Eigen::Matrix4f>& waypoints);
        PositionControllerHelper(const VirtualRobot::RobotNodePtr& tcp, const VelocityControllerHelperPtr& velocityControllerHelper, const std::vector<PosePtr>& waypoints);

        void readConfig(const CartesianPositionControllerConfigBase& config);
        void readConfig(const CartesianPositionControllerConfigBasePtr& config);

        // read data and write targets, call this if you are unsure, anywhere in your control loop.
        // use updateRead and updateWrite for better performance
        void update();
        // read data, call this after robot sync
        void updateRead();
        // write targets, call this at the end of your control loop, before the sleep
        void updateWrite();

        void setNewWaypoints(const std::vector<Eigen::Matrix4f>& waypoints);
        void setNewWaypoints(const std::vector<PosePtr>& waypoints);
        void addWaypoint(const Eigen::Matrix4f& waypoint);
        void setTarget(const Eigen::Matrix4f& target);
        void setFeedForwardVelocity(const Eigen::Vector6f& feedForwardVelocity);
        void setFeedForwardVelocity(const Eigen::Vector3f& feedForwardVelocityPos, const Eigen::Vector3f& feedForwardVelocityOri);
        void clearFeedForwardVelocity();
        void immediateHardStop(bool clearTargets = true);

        float getPositionError() const;

        float getOrientationError() const;

        bool isCurrentTargetReached() const;
        bool isCurrentTargetNear() const;
        bool isFinalTargetReached() const;
        bool isFinalTargetNear() const;

        bool isLastWaypoint() const;

        const Eigen::Matrix4f& getCurrentTarget() const;
        const Eigen::Vector3f getCurrentTargetPosition() const;

        size_t skipToClosestWaypoint(float rad2mmFactor);

        void setNullSpaceControl(bool enabled);

        std::string getStatusText();

        struct NullspaceOptimizationArgs
        {
            NullspaceOptimizationArgs() {}
            int loops = 100;
            float stepLength = 0.05f;
            float eps = 0.001;
            float maxOriErrorIncrease = 1.f / 180 * M_PI;
            float maxPoseErrorIncrease = 1;
            VirtualRobot::IKSolver::CartesianSelection cartesianSelection = VirtualRobot::IKSolver::All;
            VirtualRobot::JacobiProvider::InverseJacobiMethod invJacMethod = VirtualRobot::JacobiProvider::eSVD;
        };

        /**
         * @brief OptimizeNullspace
         * @param tcp
         * @param rns
         * @param target target pose in root frame of the robot
         * @param args
         * @return
         */
        static bool OptimizeNullspace(const VirtualRobot::RobotNodePtr& tcp, const VirtualRobot::RobotNodeSetPtr& rns, const Eigen::Matrix4f& target, const NullspaceOptimizationArgs& args = NullspaceOptimizationArgs());

        CartesianPositionController posController;
        VelocityControllerHelperPtr velocityControllerHelper;

        std::vector<Eigen::Matrix4f> waypoints;
        size_t currentWaypointIndex = 0;

        float thresholdPositionReached = 0;
        float thresholdOrientationReached = 0;
        float thresholdPositionNear = 0;
        float thresholdOrientationNear = 0;
        Eigen::Vector6f feedForwardVelocity = Eigen::Vector6f::Zero();
        bool autoClearFeedForward = true;
    };
}
