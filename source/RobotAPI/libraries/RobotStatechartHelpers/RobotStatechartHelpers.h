/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotStatechartHelpers
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


namespace armarx
{
    /**
    * @defgroup Library-RobotStatechartHelpers RobotStatechartHelpers
    * @ingroup RobotAPI
    * A description of the library RobotStatechartHelpers.
    *
    * @class RobotStatechartHelpers
    * @ingroup Library-RobotStatechartHelpers
    * @brief Brief description of class RobotStatechartHelpers.
    *
    * Detailed description of class RobotStatechartHelpers.
    */
    class RobotStatechartHelpers
    {
    public:

    };

}
