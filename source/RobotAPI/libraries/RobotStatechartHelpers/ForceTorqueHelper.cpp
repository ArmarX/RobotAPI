/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ForceTorqueHelper.h"

#include <RobotAPI/libraries/core/FramedPose.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>

namespace armarx
{
    ForceTorqueHelper::ForceTorqueHelper(const ForceTorqueUnitObserverInterfacePrx& forceTorqueObserver, const std::string& FTDatefieldName)
    {
        forceDf = DatafieldRefPtr::dynamicCast(forceTorqueObserver->getForceDatafield(FTDatefieldName));
        torqueDf = DatafieldRefPtr::dynamicCast(forceTorqueObserver->getTorqueDatafield(FTDatefieldName));
        setZero();
    }

    Eigen::Vector3f ForceTorqueHelper::getForce()
    {
        return forceDf->getDataField()->get<FramedDirection>()->toEigen() - initialForce;
    }

    FramedDirection ForceTorqueHelper::getFramedForce()
    {
        const auto force = forceDf->getDataField()->get<FramedDirection>();

        return FramedDirection(force->toEigen() - initialForce, force->getFrame(), force->agent);
    }

    Eigen::Vector3f ForceTorqueHelper::getTorque()
    {
        return torqueDf->getDataField()->get<FramedDirection>()->toEigen() - initialTorque;
    }

    void ForceTorqueHelper::setZero()
    {
        initialForce = forceDf->getDataField()->get<FramedDirection>()->toEigen();
        initialTorque = torqueDf->getDataField()->get<FramedDirection>()->toEigen();
    }
}
