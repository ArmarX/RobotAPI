/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "VelocityControllerHelper.h"

#include <ArmarXCore/core/time/TimeUtil.h>

namespace armarx
{
    VelocityControllerHelper::VelocityControllerHelper(const RobotUnitInterfacePrx& robotUnit, const std::string& nodeSetName, const std::string& controllerName)
        : robotUnit(robotUnit), controllerName(controllerName)
    {
        config = new NJointCartesianVelocityControllerWithRampConfig(nodeSetName, "", CartesianSelectionMode::eAll, 500, 1, 2, 1, 2);
        init();
    }

    VelocityControllerHelper::VelocityControllerHelper(const RobotUnitInterfacePrx& robotUnit, NJointCartesianVelocityControllerWithRampConfigPtr config, const std::string& controllerName)
        : robotUnit(robotUnit), controllerName(controllerName)
    {
        this->config = config;
        init();
    }

    void VelocityControllerHelper::init()
    {
        NJointControllerInterfacePrx ctrl = robotUnit->getNJointController(controllerName);
        if (ctrl)
        {
            controllerCreated = false;
        }
        else
        {
            ctrl = robotUnit->createOrReplaceNJointController("NJointCartesianVelocityControllerWithRamp", controllerName, config);
            controllerCreated = true;
        }
        controller = NJointCartesianVelocityControllerWithRampInterfacePrx::checkedCast(ctrl);
        controller->activateController();
    }

    void VelocityControllerHelper::setTargetVelocity(const Eigen::Vector6f& cv)
    {
        controller->setTargetVelocity(cv(0), cv(1), cv(2), cv(3), cv(4), cv(5));
    }

    void VelocityControllerHelper::setNullSpaceControl(bool enabled)
    {
        if (enabled)
        {
            controller->setJointLimitAvoidanceScale(config->jointLimitAvoidanceScale);
            controller->setKpJointLimitAvoidance(config->KpJointLimitAvoidance);
        }
        else
        {
            controller->setJointLimitAvoidanceScale(0);
            controller->setKpJointLimitAvoidance(0);
        }
    }

    void VelocityControllerHelper::cleanup()
    {
        if (controllerCreated)
        {
            // delete controller only if it was created
            controller->deactivateAndDeleteController();
        }
        else
        {
            // if the controller existed, only deactivate it
            controller->deactivateController();
        }
    }
}
