/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ObstacleAvoidingPlatformUnit
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// Eigen
#include <Eigen/Core>

// Simox
#include <VirtualRobot/Nodes/RobotNode.h>

// RobotAPI
#include <RobotAPI/components/units/ObstacleAvoidingPlatformUnit/ObstacleAvoidingPlatformUnit.h>


namespace armarx
{

    class ObstacleAvoidingPlatformUnitHelper
    {

    public:

        struct Config
        {
            float pos_reached_threshold = 10;  // [mm]
            float ori_reached_threshold = 0.1;  // [rad]
            float pos_near_threshold = 50;  // [mm]
            float ori_near_threshold = 0.2;  // [rad]
        };

        struct Target
        {
            Eigen::Vector2f pos;
            float ori;
        };

    public:

        ObstacleAvoidingPlatformUnitHelper(
            armarx::PlatformUnitInterfacePrx platform_unit,
            VirtualRobot::RobotPtr robot);

        ObstacleAvoidingPlatformUnitHelper(
            armarx::PlatformUnitInterfacePrx platform_unit,
            VirtualRobot::RobotPtr robot,
            const Config& cfg);

        virtual
        ~ObstacleAvoidingPlatformUnitHelper();

        void
        setTarget(const Eigen::Vector2f& target_pos, float target_ori);

        void
        setTarget(const Target& target);

        Target
        getCurrentTarget() const;

        void
        update();

        void
        setWaypoints(const std::vector<Target>& waypoints);

        void
        addWaypoint(const Eigen::Vector2f& waypoint_pos, float waypoint_ori);

        void
        addWaypoint(const Target& waypoint);

        bool
        isLastWaypoint()
        const;

        bool
        isCurrentTargetNear()
        const;

        bool
        isCurrentTargetReached()
        const;

        bool
        isFinalTargetNear()
        const;

        bool
        isFinalTargetReached()
        const;

        void
        setMaxVelocities(float max_vel, float max_angular_vel);

        float
        getPositionError()
        const;

        float
        getOrientationError()
        const;

    private:

        armarx::PlatformUnitInterfacePrx m_platform_unit;

        VirtualRobot::RobotPtr m_robot;

        std::vector<Target> m_waypoints;
        unsigned int m_current_waypoint_index = 0;
        bool m_waypoint_changed = false;

        Config m_cfg;

    };

}
