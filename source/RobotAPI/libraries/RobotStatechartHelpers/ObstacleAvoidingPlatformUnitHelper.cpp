/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ObstacleAvoidingPlatformUnit
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <RobotAPI/libraries/RobotStatechartHelpers/ObstacleAvoidingPlatformUnitHelper.h>


// Simox
#include <SimoxUtility/math.h>


armarx::ObstacleAvoidingPlatformUnitHelper::ObstacleAvoidingPlatformUnitHelper(
    armarx::PlatformUnitInterfacePrx platform_unit,
    VirtualRobot::RobotPtr robot) :
    ObstacleAvoidingPlatformUnitHelper{platform_unit, robot, Config{}}
{
    // pass
}


armarx::ObstacleAvoidingPlatformUnitHelper::ObstacleAvoidingPlatformUnitHelper(
    armarx::PlatformUnitInterfacePrx platform_unit,
    VirtualRobot::RobotPtr robot,
    const Config& cfg) :
    m_platform_unit{platform_unit},
    m_robot{robot},
    m_cfg{cfg}
{
    // pass
}


armarx::ObstacleAvoidingPlatformUnitHelper::~ObstacleAvoidingPlatformUnitHelper()
{
    m_platform_unit->stopPlatform();
}


void
armarx::ObstacleAvoidingPlatformUnitHelper::setTarget(
    const Eigen::Vector2f& target_pos,
    const float target_ori)
{
    setTarget(Target{target_pos, target_ori});
}


void
armarx::ObstacleAvoidingPlatformUnitHelper::setTarget(const Target& target)
{
    m_waypoints.clear();
    m_waypoints.push_back(target);
    m_current_waypoint_index = 0;
    m_waypoint_changed = true;
}


armarx::ObstacleAvoidingPlatformUnitHelper::Target
armarx::ObstacleAvoidingPlatformUnitHelper::getCurrentTarget()
const
{
    return m_waypoints.at(m_current_waypoint_index);
}


void
armarx::ObstacleAvoidingPlatformUnitHelper::update()
{
    // Skip to next waypoint if in proximity.
    if (isCurrentTargetNear() and not isLastWaypoint())
    {
        m_current_waypoint_index++;
        m_waypoint_changed = true;
    }

    // Only call `moveTo` on platform unit if the target actually changed, as `update` is designed
    // to be called within a high-frequency loop.  Prevents constantly invalidating the buffers in
    // platform unit.
    if (m_waypoint_changed)
    {
        // Use reached-thresholds regardless of whether this is the final target or just a waypoint.
        // The near-thresholds are more unconstrained as the reached-thresholds, so this helper will
        // change the position target before the unit will actually reach it, preventing slow downs
        // or stops.
        const float pos_thresh = m_cfg.pos_reached_threshold;
        const float ori_thresh = m_cfg.ori_reached_threshold;

        // Control.
        const Target target = getCurrentTarget();
        m_platform_unit->moveTo(target.pos.x(), target.pos.y(), target.ori, pos_thresh, ori_thresh);

        m_waypoint_changed = false;
    }
}


void
armarx::ObstacleAvoidingPlatformUnitHelper::addWaypoint(const Eigen::Vector2f& waypoint_pos, float waypoint_ori)
{
    addWaypoint(Target{waypoint_pos, waypoint_ori});
}


void
armarx::ObstacleAvoidingPlatformUnitHelper::setWaypoints(const std::vector<Target>& waypoints)
{
    m_waypoints = waypoints;
    m_current_waypoint_index = 0;
    m_waypoint_changed = true;
}


void
armarx::ObstacleAvoidingPlatformUnitHelper::addWaypoint(const Target& waypoint)
{
    m_waypoints.push_back(waypoint);
}


bool
armarx::ObstacleAvoidingPlatformUnitHelper::isLastWaypoint()
const
{
    return m_current_waypoint_index + 1 >= m_waypoints.size();
}


bool
armarx::ObstacleAvoidingPlatformUnitHelper::isCurrentTargetNear()
const
{
    return getPositionError() < m_cfg.pos_near_threshold and getOrientationError() < m_cfg.ori_near_threshold;
}


bool
armarx::ObstacleAvoidingPlatformUnitHelper::isCurrentTargetReached()
const
{
    return getPositionError() < m_cfg.pos_reached_threshold and getOrientationError() < m_cfg.ori_reached_threshold;
}


bool
armarx::ObstacleAvoidingPlatformUnitHelper::isFinalTargetNear()
const
{
    return isLastWaypoint() and isCurrentTargetNear();
}


bool
armarx::ObstacleAvoidingPlatformUnitHelper::isFinalTargetReached()
const
{
    return isLastWaypoint() and isCurrentTargetReached();
}


void
armarx::ObstacleAvoidingPlatformUnitHelper::setMaxVelocities(float max_vel, float max_angular_vel)
{
    m_platform_unit->setMaxVelocities(max_vel, max_angular_vel);
}


float
armarx::ObstacleAvoidingPlatformUnitHelper::getPositionError()
const
{
    const Eigen::Vector2f agent_pos = m_robot->getGlobalPosition().head<2>();
    return (getCurrentTarget().pos - agent_pos).norm();
}


float
armarx::ObstacleAvoidingPlatformUnitHelper::getOrientationError()
const
{
    using namespace simox::math;

    const float agent_ori =
        periodic_clamp<float>(mat4f_to_rpy(m_robot->getGlobalPose()).z(), -M_PI, M_PI);
    return std::fabs(periodic_clamp<float>(getCurrentTarget().ori - agent_ori, -M_PI, M_PI));
}
