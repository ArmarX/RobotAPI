/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Raphael Grimm (raphael dot grimm at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>

#include <Eigen/Dense>

#include <VirtualRobot/Robot.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/interface/core/CartesianWaypointControllerConfig.h>

#include "CartesianVelocityControllerWithRamp.h"

namespace Eigen
{
    using Vector6f = Matrix<float, 6, 1>;
}


namespace armarx
{
    class CartesianWaypointController;
    using CartesianWaypointControllerPtr = std::shared_ptr<CartesianWaypointController>;

    class CartesianWaypointController
    {
    public:
        CartesianWaypointController(const VirtualRobot::RobotNodeSetPtr& rns,
                                    const Eigen::VectorXf& currentJointVelocity,
                                    float maxPositionAcceleration,
                                    float maxOrientationAcceleration,
                                    float maxNullspaceAcceleration,
                                    const VirtualRobot::RobotNodePtr& tcp = nullptr,
                                    const VirtualRobot::RobotNodePtr& referenceFrame = nullptr
                                   );


        [[deprecated("compued null space velocity does not match pseudo inverse svd method in simox. never use this function.")]]
        void setCurrentJointVelocity(const Eigen::Ref<const Eigen::VectorXf>& currentJointVelocity);

        const Eigen::VectorXf& calculate(float dt);

        void setWaypoints(const std::vector<Eigen::Matrix4f>& waypoints);
        void swapWaypoints(std::vector<Eigen::Matrix4f>& waypoints);
        void addWaypoint(const Eigen::Matrix4f& waypoint);
        void setTarget(const Eigen::Matrix4f& target);
        void setFeedForwardVelocity(const Eigen::Vector6f& feedForwardVelocity);
        void setFeedForwardVelocity(const Eigen::Vector3f& feedForwardVelocityPos, const Eigen::Vector3f& feedForwardVelocityOri);
        void clearFeedForwardVelocity();

        float getPositionError() const;

        float getOrientationError() const;

        bool isCurrentTargetReached() const;
        bool isCurrentTargetNear() const;
        bool isFinalTargetReached() const;
        bool isFinalTargetNear() const;

        bool isLastWaypoint() const;

        const Eigen::Matrix4f& getCurrentTarget() const;
        const Eigen::Vector3f getCurrentTargetPosition() const;

        size_t skipToClosestWaypoint(float rad2mmFactor);

        void setNullSpaceControl(bool enabled);

        std::string getStatusText();

        void setConfig(const CartesianWaypointControllerConfig& cfg);

        CartesianVelocityControllerWithRamp  ctrlCartesianVelWithRamps;
        CartesianPositionController ctrlCartesianPos2Vel;

        std::vector<Eigen::Matrix4f> waypoints;
        size_t currentWaypointIndex         = 0.f;

        float thresholdPositionReached      = 0.f;
        float thresholdOrientationReached   = 0.f;
        float thresholdPositionNear         = 0.f;
        float thresholdOrientationNear      = 0.f;

        Eigen::Vector6f feedForwardVelocity = Eigen::Vector6f::Zero();
        Eigen::Vector6f cartesianVelocity = Eigen::Vector6f::Zero();
        bool autoClearFeedForward           = true;


        bool nullSpaceControlEnabled        = true;
        float jointLimitAvoidanceScale      = 0.f;
        float KpJointLimitAvoidance         = 0.f;

    private:
        Eigen::VectorXf _out;
        Eigen::VectorXf _jnv;
    };
}
