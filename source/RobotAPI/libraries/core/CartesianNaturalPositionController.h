/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Raphael Grimm (raphael dot grimm at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>

#include <Eigen/Dense>

#include <VirtualRobot/Robot.h>

#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <RobotAPI/interface/core/CartesianNaturalPositionControllerConfig.h>

#include "CartesianVelocityControllerWithRamp.h"

namespace Eigen
{
    using Vector6f = Matrix<float, 6, 1>;
}


namespace armarx
{
    class CartesianNaturalPositionController;
    using CartesianNaturalPositionControllerPtr = std::shared_ptr<CartesianNaturalPositionController>;

    class CartesianNaturalPositionController
    {
    public:
        CartesianNaturalPositionController(const VirtualRobot::RobotNodeSetPtr& rns,
                                           const VirtualRobot::RobotNodePtr& elbow,
                                           float maxPositionAcceleration,
                                           float maxOrientationAcceleration,
                                           float maxNullspaceAcceleration,
                                           const VirtualRobot::RobotNodePtr& tcp = nullptr
                                          );


        static Eigen::VectorXf LimitInfNormTo(const Eigen::VectorXf& vec, const std::vector<float>& maxValue);
        const Eigen::VectorXf calculateNullspaceTargetDifference();
        const Eigen::VectorXf calculate(float dt, VirtualRobot::IKSolver::CartesianSelection mode = VirtualRobot::IKSolver::All);

        void setTarget(const Eigen::Matrix4f& tcpTarget, const Eigen::Vector3f& elbowTarget);
        void setFeedForwardVelocity(const Eigen::Vector6f& feedForwardVelocity);
        void setFeedForwardVelocity(const Eigen::Vector3f& feedForwardVelocityPos, const Eigen::Vector3f& feedForwardVelocityOri);
        void setNullspaceTarget(const Eigen::VectorXf& nullspaceTarget);
        void setNullspaceTarget(const std::vector<float>& nullspaceTarget);
        void clearNullspaceTarget();
        void clearFeedForwardVelocity();

        float getPositionError() const;

        float getOrientationError() const;

        bool isTargetReached() const;
        bool isTargetNear() const;


        const Eigen::Matrix4f& getCurrentTarget() const;
        const Eigen::Vector3f getCurrentTargetPosition() const;
        const Eigen::Vector3f& getCurrentElbowTarget() const;
        const Eigen::VectorXf& getCurrentNullspaceTarget() const;
        bool hasNullspaceTarget() const;

        void setNullSpaceControl(bool enabled);

        std::string getStatusText();

        void setConfig(const CartesianNaturalPositionControllerConfig& cfg);

        Eigen::VectorXf actualTcpVel(const Eigen::VectorXf& jointVel);
        Eigen::VectorXf actualElbVel(const Eigen::VectorXf& jointVel);

        CartesianVelocityControllerWithRamp  vcTcp;
        CartesianPositionController pcTcp;
        CartesianVelocityController vcElb;
        CartesianPositionController pcElb;
        Eigen::VectorXf lastJointVelocity;

        Eigen::Matrix4f tcpTarget;
        Eigen::Vector3f elbowTarget;
        Eigen::VectorXf nullspaceTarget = Eigen::VectorXf(0);

        float thresholdPositionReached      = 0.f;
        float thresholdOrientationReached   = 0.f;
        float thresholdPositionNear         = 0.f;
        float thresholdOrientationNear      = 0.f;

        std::vector<float> maxJointVelocity;
        std::vector<float> maxNullspaceVelocity;


        Eigen::Vector6f feedForwardVelocity = Eigen::Vector6f::Zero();
        Eigen::Vector6f cartesianVelocity = Eigen::Vector6f::Zero();
        bool autoClearFeedForward           = true;


        bool nullSpaceControlEnabled        = true;
        float jointLimitAvoidanceScale      = 0.f;
        float jointLimitAvoidanceKp         = 0.f;
        float nullspaceTargetKp             = 0;

    private:
        VirtualRobot::RobotNodeSetPtr rns;
    };
}
