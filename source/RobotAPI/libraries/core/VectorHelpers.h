/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/exceptions/Exception.h>

#include <Eigen/Geometry>
#include <cmath>
#include <map>
#include <vector>
#include <limits>

namespace armarx
{
    template<class T>
    inline void checkValue(const T& value)
    {
        if (std::numeric_limits<T>::infinity() == value)
        {
            throw LocalException("result value is inf");
        }

        if (value != value)
        {
            throw LocalException("result value is nan");
        }
    }

    template<class T>
    inline void checkValues(const std::vector<T >& values)
    {
        for (size_t i = 0; i < values.size(); ++i)
        {
            checkValue(values[i]);
        }
    }



    template <class T> std::vector<T> operator -(const std::vector<T>& v1, const std::vector<T>& v2)
    {
        std::vector<T> res(std::min(v1.size(), v2.size()), T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v1[j] - v2[j];
            ++j;
        }

        return res;
    }

    template <class T> std::vector<T> operator +(const std::vector<T>& v1, const std::vector<T>& v2)
    {
        std::vector<T> res(std::min(v1.size(), v2.size()), T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v1[j] + v2[j];
            ++j;
        }

        return res;
    }

    template <class T> std::vector<T>& operator +=(std::vector<T>& v1, const std::vector<T>& v2)
    {
        int j = 0;

        for (typename std::vector<T>::iterator i = v1.begin(); i != v1.end(); ++i)
        {
            *i += v2[j];
            ++j;
        }

        return v1;
    }


    template <class T> std::vector<T>& operator -=(std::vector<T>& v1, const std::vector<T>& v2)
    {
        int j = 0;

        for (typename std::vector<T>::iterator i = v1.begin(); i != v1.end(); ++i)
        {
            *i -= v2[j];
            ++j;
        }

        return v1;
    }

    template <class T> std::vector<T>& operator *=(std::vector<T>& v1, double c)
    {
        int j = 0;

        for (typename std::vector<T>::iterator i = v1.begin(); i != v1.end(); ++i)
        {
            *i *= c;
            ++j;
        }

        return v1;
    }



    template <class T> std::vector<T>& operator /=(std::vector<T>& v1, double c)
    {
        int j = 0;

        for (typename std::vector<T>::iterator i = v1.begin(); i != v1.end(); ++i)
        {
            *i /= c;
            ++j;
        }

        return v1;
    }

    template <class T> std::vector<T> operator *(double c, const std::vector<T>& v)
    {
        std::vector<T> res(v.size(), T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = c * v[j];
            ++j;
        }

        return res;
    }

    template <class T> double operator *(const std::vector<T>& v, const std::vector<T>& v2)
    {
        double res = 0;

        if (v.size() != v2.size())
        {
            throw LocalException("vectors do not match in size: ") << v.size() << " vs. " << v2.size();
        }

        int j = 0;

        for (typename std::vector<T>::const_iterator i = v.begin(); i != v.end(); ++i)
        {
            res += *i * v2[j];
            ++j;
        }

        return res;
    }


    template <class T> std::vector<T> operator +(const std::vector<T>& v, double s)
    {
        std::vector<T> res(v.size(), T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v[j] + s;
            ++j;
        }

        return res;
    }

    template <class T> std::vector<T> operator -(const std::vector<T>& v, double s)
    {
        std::vector<T> res(v.size(), T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v[j] - s;
            ++j;
        }

        return res;
    }

    template <class T> double vecLength(const std::vector<T>& v)
    {
        double result = 0.0;

        for (typename std::vector<T>::const_iterator it = v.begin(); it != v.end(); it++)
        {
            result += *it** it;
        }

        return sqrt(result);
    }

    template <class T> double vecSum(const std::vector<T>& v)
    {
        double result = 0.0;

        for (typename std::vector<T>::const_iterator it = v.begin(); it != v.end(); it++)
        {
            result += *it;
        }

        return result;
    }

    template <class T> std::vector<T> normalizedVec(const std::vector<T>& v)
    {
        double length = vecLength(v);
        std::vector<T> result = v;
        result /= length;
        return result;
    }

    template <class T> std::vector<T> operator /(const std::vector<T>& v, double c)
    {
        std::vector<T> res(v.size(),  T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v[j] / c;
            ++j;
        }

        return res;
    }

    template <class T> std::vector<T> abs(const std::vector<T>& v)
    {
        std::vector<T> res(v.size(), T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = fabs(v[j]);
            ++j;
        }

        return res;
    }

    template <class T> std::vector<T> max(const std::vector<T>& v1, const std::vector<T>& v2)
    {
        std::vector<T> res(std::min(v1.size(), v2.size()), T());
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = std::max(v1[j], v2[j]);
            ++j;
        }

        return res;
    }

    template <class T> T max(const std::vector<T>& v1)
    {
        T maxValue = std::numeric_limits<T>::min();

        for (typename std::vector<T>::const_iterator i = v1.begin(); i != v1.end(); ++i)
        {
            maxValue = std::max(maxValue, *i);

        }

        return maxValue;
    }

    template <class T> std::vector<T> min(const std::vector<T>& v1, const std::vector<T>& v2)
    {
        std::vector<T> res(std::min(v1.size(), v2.size()));
        int j = 0;

        for (typename std::vector<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = std::min(v1[j], v2[j]);
            ++j;
        }

        return res;
    }

    template <class T> T min(const std::vector<T>& v1)
    {
        T minValue = std::numeric_limits<T>::max();

        for (typename std::vector<T>::const_iterator i = v1.begin(); i != v1.end(); ++i)
        {
            minValue = std::min(minValue, *i);

        }

        return minValue;
    }
}
