/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Sonja Marahrens (sonja dot marahrens)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartesianVelocityRamp.h"

#include <ArmarXCore/core/logging/Logging.h>

namespace armarx
{
    CartesianVelocityRamp::CartesianVelocityRamp()
    { }

    void CartesianVelocityRamp::setState(const Eigen::VectorXf& state, VirtualRobot::IKSolver::CartesianSelection mode)
    {
        this->state = state;
        this->mode = mode;
    }

    Eigen::VectorXf CartesianVelocityRamp::update(const Eigen::VectorXf& target, float dt)
    {
        if (dt <= 0)
        {
            return state;
        }
        Eigen::VectorXf delta = target - state;
        int i = 0;
        float factor = 1 ;

        if (mode & VirtualRobot::IKSolver::Position)
        {
            Eigen::Vector3f posDelta = delta.block<3, 1>(i, 0);
            float posFactor = posDelta.norm() / maxPositionAcceleration / dt;
            factor = std::max(factor, posFactor);
            i += 3;
        }

        if (mode & VirtualRobot::IKSolver::Orientation)
        {
            Eigen::Vector3f oriDelta = delta.block<3, 1>(i, 0);
            float oriFactor = oriDelta.norm() / maxOrientationAcceleration / dt;
            factor = std::max(factor, oriFactor);
        }

        state += delta / factor;
        //    ARMARX_IMPORTANT << "CartRamp state " << state;
        return state;
    }

    void CartesianVelocityRamp::setMaxPositionAcceleration(float maxPositionAcceleration)
    {
        this->maxPositionAcceleration = maxPositionAcceleration;

    }

    void CartesianVelocityRamp::setMaxOrientationAcceleration(float maxOrientationAcceleration)
    {
        this->maxOrientationAcceleration = maxOrientationAcceleration;

    }

    float CartesianVelocityRamp::getMaxPositionAcceleration()
    {
        return maxPositionAcceleration;
    }

    float CartesianVelocityRamp::getMaxOrientationAcceleration()
    {
        return maxOrientationAcceleration;
    }
}

