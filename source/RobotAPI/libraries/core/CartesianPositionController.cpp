/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartesianPositionController.h"

#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <ArmarXCore/util/CPPUtility/trace.h>

namespace armarx
{
    CartesianPositionController::CartesianPositionController(const VirtualRobot::RobotNodePtr& tcp,
            const VirtualRobot::RobotNodePtr& referenceFrame)
        : tcp(tcp), referenceFrame(referenceFrame ? referenceFrame : tcp->getRobot()->getRootNode())
    {
    }

    Eigen::VectorXf CartesianPositionController::calculate(const Eigen::Matrix4f& targetPose, VirtualRobot::IKSolver::CartesianSelection mode) const
    {
        ARMARX_TRACE;
        int posLen = mode & VirtualRobot::IKSolver::Position ? 3 : 0;
        int oriLen = mode & VirtualRobot::IKSolver::Orientation ? 3 : 0;
        Eigen::VectorXf cartesianVel(posLen + oriLen);

        if (posLen)
        {
            ARMARX_TRACE;
            Eigen::Vector3f targetPos = targetPose.block<3, 1>(0, 3);
            Eigen::Vector3f currentPos = tcp->getPositionInFrame(referenceFrame);
            Eigen::Vector3f errPos = targetPos - currentPos;
            Eigen::Vector3f posVel =  errPos * KpPos;
            if (maxPosVel > 0)
            {
                posVel = math::MathUtils::LimitTo(posVel, maxPosVel);
            }
            cartesianVel.block<3, 1>(0, 0) = posVel;
        }

        if (oriLen)
        {
            ARMARX_TRACE;
            Eigen::Matrix3f targetOri = targetPose.block<3, 3>(0, 0);
            Eigen::Matrix3f tcpOri = tcp->getPoseInFrame(referenceFrame).block<3, 3>(0, 0);
            Eigen::Matrix3f oriDir = targetOri * tcpOri.inverse();
            Eigen::AngleAxisf aa(oriDir);
            Eigen::Vector3f errOri = aa.axis() * aa.angle();
            Eigen::Vector3f oriVel = errOri * KpOri;

            if (maxOriVel > 0)
            {
                oriVel = math::MathUtils::LimitTo(oriVel, maxOriVel);
            }
            cartesianVel.block<3, 1>(posLen, 0) = oriVel;
        }
        return cartesianVel;
    }

    Eigen::VectorXf CartesianPositionController::calculatePos(const Eigen::Vector3f& targetPos) const
    {
        ARMARX_TRACE;
        Eigen::VectorXf cartesianVel(3);
        Eigen::Vector3f currentPos = tcp->getPositionInFrame(referenceFrame);
        Eigen::Vector3f errPos = targetPos - currentPos;
        Eigen::Vector3f posVel =  errPos * KpPos;
        if (maxPosVel > 0)
        {
            ARMARX_TRACE;
            posVel = math::MathUtils::LimitTo(posVel, maxPosVel);
        }
        cartesianVel.block<3, 1>(0, 0) = posVel;
        return cartesianVel;
    }

    float CartesianPositionController::getPositionError(const Eigen::Matrix4f& targetPose) const
    {
        return GetPositionError(targetPose, tcp, referenceFrame);
    }

    float CartesianPositionController::getOrientationError(const Eigen::Matrix4f& targetPose) const
    {
        return GetOrientationError(targetPose, tcp, referenceFrame);
    }

    float CartesianPositionController::GetPositionError(const Eigen::Matrix4f& targetPose,
            const VirtualRobot::RobotNodePtr& tcp,
            const VirtualRobot::RobotNodePtr& referenceFrame)
    {
        ARMARX_TRACE;
        Eigen::Vector3f targetPos = targetPose.block<3, 1>(0, 3);
        Eigen::Vector3f tcpPos = referenceFrame ? tcp->getPositionInFrame(referenceFrame) : tcp->getPositionInRootFrame();
        Eigen::Vector3f errPos = targetPos - tcpPos;
        return errPos.norm();
    }

    float CartesianPositionController::GetOrientationError(const Eigen::Matrix4f& targetPose,
            const VirtualRobot::RobotNodePtr& tcp,
            const VirtualRobot::RobotNodePtr& referenceFrame)
    {
        ARMARX_TRACE;
        Eigen::Matrix3f targetOri = targetPose.block<3, 3>(0, 0);
        Eigen::Matrix4f tcpPose = referenceFrame ? tcp->getPoseInFrame(referenceFrame) : tcp->getPoseInRootFrame();
        Eigen::Matrix3f tcpOri = tcpPose.block<3, 3>(0, 0);
        Eigen::Matrix3f oriDir = targetOri * tcpOri.inverse();
        Eigen::AngleAxisf aa(oriDir);
        return aa.angle();
    }

    bool CartesianPositionController::Reached(const Eigen::Matrix4f& targetPose,
            const VirtualRobot::RobotNodePtr& tcp,
            bool checkOri,
            float thresholdPosReached,
            float thresholdOriReached,
            const VirtualRobot::RobotNodePtr& referenceFrame)
    {
        return GetPositionError(targetPose, tcp, referenceFrame) < thresholdPosReached
               && (!checkOri || GetOrientationError(targetPose, tcp, referenceFrame) < thresholdOriReached);
    }

    bool CartesianPositionController::reached(const Eigen::Matrix4f& targetPose, VirtualRobot::IKSolver::CartesianSelection mode, float thresholdPosReached, float thresholdOriReached)
    {
        ARMARX_TRACE;
        if (mode & VirtualRobot::IKSolver::Position)
        {
            if (GetPositionError(targetPose, tcp, referenceFrame) > thresholdPosReached)
            {
                return false;
            }
        }
        if (mode & VirtualRobot::IKSolver::Orientation)
        {
            if (GetOrientationError(targetPose, tcp, referenceFrame) > thresholdOriReached)
            {
                return false;
            }
        }
        return true;
    }

    Eigen::Vector3f CartesianPositionController::getPositionDiff(const Eigen::Matrix4f& targetPose) const
    {
        ARMARX_TRACE;
        Eigen::Vector3f targetPos = targetPose.block<3, 1>(0, 3);
        return targetPos - tcp->getPositionInFrame(referenceFrame);
    }

    Eigen::Vector3f CartesianPositionController::getPositionDiffVec3(const Eigen::Vector3f& targetPosition) const
    {
        return targetPosition - tcp->getPositionInFrame(referenceFrame);
    }

    Eigen::Vector3f CartesianPositionController::getOrientationDiff(const Eigen::Matrix4f& targetPose) const
    {
        ARMARX_TRACE;
        Eigen::Matrix3f targetOri = targetPose.block<3, 3>(0, 0);
        Eigen::Matrix3f tcpOri = tcp->getPoseInFrame(referenceFrame).block<3, 3>(0, 0);
        Eigen::Matrix3f oriDir = targetOri * tcpOri.inverse();
        Eigen::AngleAxisf aa(oriDir);
        return aa.axis() * aa.angle();
    }

    VirtualRobot::RobotNodePtr CartesianPositionController::getTcp() const
    {
        return tcp;
    }



#define SS_OUT(x) ss << #x << " = " << x << "\n"
#define SET_FLT(x) obj->setFloat(#x, x)
#define GET_FLT(x) x = obj->getFloat(#x);

    CartesianPositionControllerConfig::CartesianPositionControllerConfig()
    {
    }

    std::string CartesianPositionControllerConfig::output(const Ice::Current&) const
    {
        std::stringstream ss;

        SS_OUT(KpPos);
        SS_OUT(KpOri);
        SS_OUT(maxPosVel);
        SS_OUT(maxOriVel);
        SS_OUT(thresholdOrientationNear);
        SS_OUT(thresholdOrientationReached);
        SS_OUT(thresholdPositionNear);
        SS_OUT(thresholdPositionReached);

        return ss.str();
    }

    void CartesianPositionControllerConfig::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        SET_FLT(KpPos);
        SET_FLT(KpOri);
        SET_FLT(maxPosVel);
        SET_FLT(maxOriVel);
        SET_FLT(thresholdOrientationNear);
        SET_FLT(thresholdOrientationReached);
        SET_FLT(thresholdPositionNear);
        SET_FLT(thresholdPositionReached);

    }

    void CartesianPositionControllerConfig::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        GET_FLT(KpPos);
        GET_FLT(KpOri);
        GET_FLT(maxPosVel);
        GET_FLT(maxOriVel);
        GET_FLT(thresholdOrientationNear);
        GET_FLT(thresholdOrientationReached);
        GET_FLT(thresholdPositionNear);
        GET_FLT(thresholdPositionReached);

    }
}
