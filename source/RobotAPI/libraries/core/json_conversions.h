/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::Core
 * @author     Christian R. G. Dreher <c.dreher@kit.edu>
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// Simox
#include <SimoxUtility/json.h>

// RobotAPI
#include <RobotAPI/libraries/core/FramedPose.h>


namespace armarx
{
    void to_json(nlohmann::json& j, const FramedPose& fp)
    {
        j = nlohmann::json
        {
            {"agent", fp.agent},
            {"frame", fp.frame},
            {"qw", fp.orientation->qw},
            {"qx", fp.orientation->qx},
            {"qy", fp.orientation->qy},
            {"qz", fp.orientation->qz},
            {"x", fp.position->x},
            {"y", fp.position->y},
            {"z", fp.position->z}
        };
    }

    void from_json(const nlohmann::json& j, FramedPose& fp)
    {
        j.at("agent").get_to(fp.agent);
        j.at("frame").get_to(fp.frame);
        j.at("qw").get_to(fp.orientation->qw);
        j.at("qx").get_to(fp.orientation->qx);
        j.at("qy").get_to(fp.orientation->qy);
        j.at("qz").get_to(fp.orientation->qz);
        j.at("x").get_to(fp.position->x);
        j.at("y").get_to(fp.position->y);
        j.at("z").get_to(fp.position->z);
    }
}
