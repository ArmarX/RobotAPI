/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MedianDerivativeFilterV3.h"

#include <ArmarXCore/core/logging/Logging.h>


using namespace armarx;
using namespace filters;

armarx::filters::MedianDerivativeFilterV3::MedianDerivativeFilterV3(std::size_t offsetWindowSize, std::size_t windowSize)
    : MedianFilter(windowSize)
{
    this->valueIndex = 0;
    this->offsetIndex = 0;
    this->currentValue = Eigen::Vector3f::Zero();
    this->currentOffset = Eigen::Vector3f::Zero();

    this->offsetWindowSize = offsetWindowSize;
}

armarx::VariantBasePtr armarx::filters::MedianDerivativeFilterV3::calculate(const Ice::Current& c) const
{
    std::unique_lock lock(historyMutex);

    if (dataHistory.size() == 0)
    {
        return NULL;
    }

    VariantPtr var = VariantPtr::dynamicCast(dataHistory.begin()->second);
    VariantTypeId type = var->getType();

    if (type == VariantType::Vector3)
    {
        Vector3Ptr vecVar = new Vector3(currentValue);
        return new Variant(vecVar);
    }
    else if (type == VariantType::FramedDirection)
    {
        FramedDirectionPtr p = var->get<FramedDirection>();
        FramedDirectionPtr vecVar = new FramedDirection(currentValue, p->frame, p->agent);
        return new Variant(vecVar);
    }
    else if (type == VariantType::FramedPosition)
    {
        FramedPositionPtr p = var->get<FramedPosition>();
        FramedPositionPtr vecVar = new FramedPosition(currentValue, p->frame, p->agent);
        return new Variant(vecVar);
    }
    else
    {
        ARMARX_WARNING_S << "Unsupported Variant Type: " << var->getTypeName();
        return NULL;
    }

}

armarx::ParameterTypeList armarx::filters::MedianDerivativeFilterV3::getSupportedTypes(const Ice::Current& c) const
{
    ParameterTypeList result = MedianFilter::getSupportedTypes(c);
    result.push_back(VariantType::Vector3);
    result.push_back(VariantType::FramedDirection);
    result.push_back(VariantType::FramedPosition);
    return result;
}

float armarx::filters::MedianDerivativeFilterV3::median(std::vector<float>& values)
{
    std::sort(values.begin(), values.end());
    return values.size() % 2 == 0 ? (values.at(values.size() / 2 - 1) + values.at(values.size() / 2)) / 2 : values.at(values.size() / 2);
}

Eigen::Vector3f armarx::filters::MedianDerivativeFilterV3::calculateMedian(const std::vector<Eigen::Vector3f>& data)
{
    // increase cache-efficiency by iterating over data one time, storing all three vectors
    std::vector<float> values[3];
    for (int i = 0; i < 3; ++i)
    {
        values[i].reserve(data.size());
    }
    for (const Eigen::Vector3f& v : data)
    {
        for (int i = 0; i < 3; ++i)
        {
            values[i].push_back(v(i));
        }
    }

    Eigen::Vector3f result;
    for (int i = 0; i < 3; ++i)
    {
        result(i) = median(values[i]);
    }
    return result;
}

void armarx::filters::MedianDerivativeFilterV3::update(Ice::Long timestamp, const armarx::VariantBasePtr& value, const Ice::Current& c)
{
    VariantTypeId type = value->getType();
    if (type == VariantType::Vector3 || type == VariantType::FramedDirection || type == VariantType::FramedPosition)
    {
        Eigen::Vector3f inputValue = VariantPtr::dynamicCast(value)->get<Vector3>()->toEigen();

        bool offsetValid = false;
        if (offsetData.size() < unsigned(offsetWindowSize))
        {
            offsetData.push_back(inputValue);
        }
        else
        {
            offsetData[offsetIndex] = inputValue;
            offsetIndex = (offsetIndex + 1) % offsetWindowSize;
            this->currentOffset = calculateMedian(offsetData);
            offsetValid = true;
        }

        if (valueData.size() < unsigned(windowFilterSize))
        {
            valueData.push_back(inputValue);
        }
        else
        {
            valueData[valueIndex] = inputValue;
            valueIndex = (valueIndex + 1) % windowFilterSize;
        }

        // Keep currentValue at zero while currentOffset is invalid.
        if (offsetValid)
        {
            // otherwise, compute it
            this->currentValue = calculateMedian(valueData) - currentOffset;
        }
    }
    else
    {
        ARMARX_WARNING_S << "Unsupported Variant Type: " << value->getTypeName();
    }
    DatafieldFilter::update(timestamp, value, c);
}
