/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "PoseAverageFilter.h"

#include <ArmarXCore/core/logging/Logging.h>


namespace armarx::filters
{

    VariantBasePtr PoseAverageFilter::calculate(const Ice::Current& c) const
    {
        std::unique_lock lock(historyMutex);

        if (dataHistory.size() == 0)
        {
            return NULL;
        }

        VariantTypeId type = dataHistory.begin()->second->getType();

        if ((type == VariantType::Vector3) || (type == VariantType::FramedDirection) || (type == VariantType::FramedPosition))
        {

            Eigen::Vector3f vec;
            vec.setZero();
            std::string frame = "";
            std::string agent = "";
            VariantPtr var = VariantPtr::dynamicCast(dataHistory.begin()->second);

            if (type == VariantType::FramedDirection)
            {
                FramedDirectionPtr p = var->get<FramedDirection>();
                frame = p->frame;
                agent = p->agent;
            }
            else if (type == VariantType::FramedPosition)
            {
                FramedPositionPtr p = var->get<FramedPosition>();
                frame = p->frame;
                agent = p->agent;
            }
            Eigen::MatrixXf keypointPositions(dataHistory.size(), 3);
            int i = 0;
            for (auto& v : dataHistory)
            {
                VariantPtr v2 = VariantPtr::dynamicCast(v.second);
                Eigen::Vector3f value = v2->get<Vector3>()->toEigen();
                keypointPositions(i, 0) = value(0);
                keypointPositions(i, 1) = value(1);
                keypointPositions(i, 2) = value(2);
                i++;
            }
            vec = keypointPositions.colwise().mean();


            if (type == VariantType::Vector3)
            {
                Vector3Ptr vecVar = new Vector3(vec);
                return new Variant(vecVar);
            }
            else if (type == VariantType::FramedDirection)
            {

                FramedDirectionPtr vecVar = new FramedDirection(vec, frame, agent);
                return new Variant(vecVar);
            }
            else if (type == VariantType::FramedPosition)
            {
                FramedPositionPtr vecVar = new FramedPosition(vec, frame, agent);
                return new Variant(vecVar);
            }
            else
            {
                ARMARX_WARNING_S << "Implementation missing here";
                return NULL;
            }
        }
        else if (type == VariantType::Double)
        {
            //                    auto values = SortVariants<double>(dataHistory);
            //                    return new Variant(values.at(values.size()/2));
        }
        else if (type == VariantType::Int)
        {
            //                    auto values = SortVariants<int>(dataHistory);
            //                    return new Variant(values.at(values.size()/2));
        }

        return AverageFilter::calculate(c);
    }



}
