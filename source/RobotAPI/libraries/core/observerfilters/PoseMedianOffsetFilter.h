/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/observers/filters/MedianFilter.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/interface/core/PoseBase.h>

namespace armarx::filters
{

    /**
     * @class PoseMedianOffsetFilter
     * @ingroup ObserverFilters
     * @brief The MedianFilter class provides an implementation
     *  for a median for datafields of type float, int and double.
     */
    class PoseMedianOffsetFilter :
        public ::armarx::PoseMedianOffsetFilterBase,
        public MedianFilter
    {
    public:
        PoseMedianOffsetFilter(int windowSize = 11);

        // DatafieldFilterBase interface
    public:
        VariantBasePtr calculate(const Ice::Current& c) const override;

        /**
         * @brief This filter supports: Vector3, FramedDirection, FramedPosition
         * @return List of VariantTypes
         */
        ParameterTypeList getSupportedTypes(const Ice::Current& c) const override;

    private:
        Eigen::Vector3f offset;
        Eigen::Vector3f currentValue;
        std::vector<Eigen::Vector3f> data;
        int dataIndex;

        float median(std::vector<float>& values);
        Eigen::Vector3f calculateMedian();

    public:
        void update(Ice::Long timestamp, const VariantBasePtr& value, const Ice::Current& c) override;

    };

} // namespace Filters
