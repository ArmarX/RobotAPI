/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/interface/observers/ObserverFilters.h>

#include <ArmarXCore/observers/filters/DatafieldFilter.h>


namespace armarx::filters
{

    /**
     * @class OffsetFilter
     * @ingroup ObserverFilters
     * @brief The OffsetFilter class returns values
     * relative to value from the first call of the filter.
     * E.g. this is useful for Forces which should be nulled
     * at a specific moment.
     */
    class OffsetFilter :
        public ::armarx::OffsetFilterBase,
        public DatafieldFilter
    {
    public:

        OffsetFilter();


        // DatafieldFilterBase interface
    public:

        VariantBasePtr calculate(const Ice::Current& = Ice::emptyCurrent) const override;

        ParameterTypeList getSupportedTypes(const Ice::Current&) const override;


    private:
        bool firstRun;
        VariantPtr initialValue;


        // DatafieldFilterBase interface
    public:
        void update(Ice::Long timestamp, const VariantBasePtr& value, const Ice::Current& c) override;

    };
}


