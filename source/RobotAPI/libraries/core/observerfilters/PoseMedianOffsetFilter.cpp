/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PoseMedianOffsetFilter.h"

#include <ArmarXCore/core/logging/Logging.h>

using namespace armarx;
using namespace filters;

armarx::filters::PoseMedianOffsetFilter::PoseMedianOffsetFilter(int windowSize)
    : MedianFilter(windowSize)
{
    this->windowFilterSize = windowSize;
    this->dataIndex = -windowSize;
    this->offset = Eigen::Vector3f::Zero();
    this->currentValue = Eigen::Vector3f::Zero();
}

armarx::VariantBasePtr armarx::filters::PoseMedianOffsetFilter::calculate(const Ice::Current& c) const
{
    std::unique_lock lock(historyMutex);

    if (dataHistory.size() == 0)
    {
        return NULL;
    }

    VariantPtr var = VariantPtr::dynamicCast(dataHistory.begin()->second);
    VariantTypeId type = var->getType();

    if (type == VariantType::Vector3)
    {
        Vector3Ptr vecVar = new Vector3(currentValue);
        return new Variant(vecVar);
    }
    else if (type == VariantType::FramedDirection)
    {
        FramedDirectionPtr p = var->get<FramedDirection>();
        FramedDirectionPtr vecVar = new FramedDirection(currentValue, p->frame, p->agent);
        return new Variant(vecVar);
    }
    else if (type == VariantType::FramedPosition)
    {
        FramedPositionPtr p = var->get<FramedPosition>();
        FramedPositionPtr vecVar = new FramedPosition(currentValue, p->frame, p->agent);
        return new Variant(vecVar);
    }
    else
    {
        ARMARX_WARNING_S << "Unsupported Variant Type: " << var->getTypeName();
        return NULL;
    }

}

armarx::ParameterTypeList armarx::filters::PoseMedianOffsetFilter::getSupportedTypes(const Ice::Current& c) const
{
    ParameterTypeList result = MedianFilter::getSupportedTypes(c);
    result.push_back(VariantType::Vector3);
    result.push_back(VariantType::FramedDirection);
    result.push_back(VariantType::FramedPosition);
    return result;
}

float armarx::filters::PoseMedianOffsetFilter::median(std::vector<float>& values)
{
    std::sort(values.begin(), values.end());
    return values.size() % 2 == 0 ? (values.at(values.size() / 2 - 1) + values.at(values.size() / 2)) / 2 : values.at(values.size() / 2);
}

Eigen::Vector3f armarx::filters::PoseMedianOffsetFilter::calculateMedian()
{
    Eigen::Vector3f result;
    for (int i = 0; i < 3; ++i)
    {
        std::vector<float> values;
        values.reserve(data.size());

        for (const Eigen::Vector3f& v : data)
        {
            values.push_back(v(i));
        }
        result(i) = median(values);
    }
    return result;
}

void armarx::filters::PoseMedianOffsetFilter::update(Ice::Long timestamp, const armarx::VariantBasePtr& value, const Ice::Current& c)
{
    VariantTypeId type = value->getType();
    if (type == VariantType::Vector3 || type == VariantType::FramedDirection || type == VariantType::FramedPosition)
    {
        Eigen::Vector3f currentValue = VariantPtr::dynamicCast(value)->get<Vector3>()->toEigen();
        if (dataIndex < 0)
        {
            data.push_back(currentValue);
            this->currentValue = Eigen::Vector3f::Zero();
            dataIndex++;
            if (dataIndex == 0)
            {
                offset = calculateMedian();
            }
        }
        else
        {
            data.at(dataIndex) = currentValue;
            dataIndex = (dataIndex + 1) % windowFilterSize;
            this->currentValue = calculateMedian() - offset;
        }
    }
    else
    {
        ARMARX_WARNING_S << "Unsupported Variant Type: " << value->getTypeName();
    }
    DatafieldFilter::update(timestamp, value, c);
}
