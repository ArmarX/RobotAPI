/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#define BOOST_TEST_MODULE RobotAPI::FramedPose::Test
#define ARMARX_BOOST_TEST
#include <RobotAPI/Test.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <RobotAPI/libraries/core/FramedPose.h>


using namespace armarx;


BOOST_AUTO_TEST_CASE(complexVariantToDict)
{
    armarx::JSONObjectPtr json = new JSONObject();

    FramedPositionPtr pos = new FramedPosition();
    pos->x = 10;
    pos->y = 20;
    pos->z = 3;
    pos->frame = "Base";
    VariantPtr var = new Variant(pos);
    auto dict = json->ConvertToBasicVariantMap(json, var);

    for (auto e : dict)
    {
        ARMARX_INFO_S << e.first << ": " << *VariantPtr::dynamicCast(e.second);
    }
}
