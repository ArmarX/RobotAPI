/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/ConditionCheck.h>
#include <RobotAPI/libraries/core/FramedPose.h>

namespace armarx
{
    class ARMARXCORE_IMPORT_EXPORT ConditionCheckApproxPose :
        public ConditionCheck
    {
    public:
        ConditionCheckApproxPose()
        {
            setNumberParameters(3);
            addSupportedType(VariantType::FramedPosition, createParameterTypeList(2, VariantType::FramedPosition, VariantType::Float));
            addSupportedType(VariantType::FramedOrientation, createParameterTypeList(2, VariantType::FramedOrientation, VariantType::Float));
            addSupportedType(VariantType::Vector3, createParameterTypeList(2, VariantType::Vector3, VariantType::Float));
            addSupportedType(VariantType::Quaternion, createParameterTypeList(2, VariantType::Quaternion, VariantType::Float));
            addSupportedType(VariantType::FramedPose, createParameterTypeList(3, VariantType::FramedPose, VariantType::Float, VariantType::Float));
        }

        ConditionCheck* clone() override
        {
            return new ConditionCheckApproxPose(*this);
        }

        bool evaluate(const StringVariantMap& dataFields) override
        {
            if (dataFields.size() != 1)
            {
                printf("Size of dataFields: %d\n", (int)dataFields.size());
                throw InvalidConditionException("Wrong number of datafields for condition equals ");
            }

            const Variant& value = dataFields.begin()->second;
            VariantTypeId type = value.getType();

            if (type == VariantType::Vector3)
            {
                const Vector3Ptr& typedValue =  value.getClass<Vector3>();
                const Vector3Ptr& param =  getParameter(0).getClass<Vector3>();
                return (sqrt(((typedValue->x - param->x) * (typedValue->x - param->x)) +
                             ((typedValue->y - param->y) * (typedValue->y - param->y)) +
                             ((typedValue->z - param->x) * (typedValue->x - param->z))) < getParameter(1).getFloat());
            }

            if (type == VariantType::Quaternion)
            {
                const QuaternionPtr& typedValue =  value.getClass<Quaternion>();
                const QuaternionPtr& param =  getParameter(0).getClass<Quaternion>();
                Eigen::Matrix3f diffRot = typedValue->toEigen() * param->toEigen().transpose();
                Eigen::AngleAxisf aa(diffRot);
                return fabs(aa.angle()) < getParameter(1).getFloat();
            }


            if (type == VariantType::FramedPosition)
            {
                const FramedPositionPtr& typedValue =  value.getClass<FramedPosition>();
                const FramedPositionPtr& param =  getParameter(0).getClass<FramedPosition>();
                return param->getFrame() == typedValue->getFrame()
                       && (sqrt(((typedValue->x - param->x) * (typedValue->x - param->x)) +
                                ((typedValue->y - param->y) * (typedValue->y - param->y)) +
                                ((typedValue->z - param->x) * (typedValue->x - param->z))) < getParameter(1).getFloat());
            }


            if (type == VariantType::FramedOrientation)
            {
                const FramedOrientationPtr& typedValue =  value.getClass<FramedOrientation>();
                const FramedOrientationPtr& param =  getParameter(0).getClass<FramedOrientation>();
                Eigen::Matrix3f diffRot = typedValue->toEigen() * param->toEigen().transpose();
                Eigen::AngleAxisf aa(diffRot);
                return fabs(aa.angle()) < getParameter(1).getFloat();
            }


            if (type == VariantType::FramedPose)
            {
                const PosePtr& typedValue =  value.getClass<FramedPose>();
                const PosePtr& param =  getParameter(0).getClass<FramedPose>();
                bool positionOk = (sqrt(((typedValue->position->x - param->position->x) * (typedValue->position->x - param->position->x)) +
                                        ((typedValue->position->y - param->position->y) * (typedValue->position->y - param->position->y)) +
                                        ((typedValue->position->z - param->position->x) * (typedValue->position->x - param->position->z)))
                                   <  getParameter(1).getFloat());

                Eigen::Matrix3f diffRot = typedValue->toEigen().block<3, 3>(0, 0) * param->toEigen().block<3, 3>(0, 0).transpose();
                Eigen::AngleAxisf aa(diffRot);
                bool orientationOk =
                    fabs(aa.angle()) < getParameter(2).getFloat();
                return positionOk && orientationOk;
            }

            return false;
        }
    };
}

