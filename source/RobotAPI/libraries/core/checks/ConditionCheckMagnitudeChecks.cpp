/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ConditionCheckMagnitudeChecks.h"

#include <ArmarXCore/core/util/StringHelpers.h>


namespace armarx
{

    ConditionCheckMagnitudeLarger::ConditionCheckMagnitudeLarger()
    {
        setNumberParameters(1);
        addSupportedType(VariantType::Vector3, createParameterTypeList(1, VariantType::Float));
        addSupportedType(VariantType::FramedDirection, createParameterTypeList(1, VariantType::Float));
        addSupportedType(VariantType::LinkedDirection, createParameterTypeList(1, VariantType::Float));

    }

    ConditionCheck* ConditionCheckMagnitudeLarger::clone()
    {
        return new ConditionCheckMagnitudeLarger(*this);
    }

    bool ConditionCheckMagnitudeLarger::evaluate(const StringVariantMap& dataFields)
    {

        if (dataFields.size() != 1)
        {
            throw InvalidConditionException("Wrong number of datafields for condition magnitude larger: expected 1 actual: " + ValueToString(dataFields.size()));
        }

        const Variant& value = dataFields.begin()->second;
        VariantTypeId type = value.getType();

        if (type == VariantType::Vector3)
        {
            Eigen::Vector3f vec = value.getClass<Vector3>()->toEigen();
            return (vec).norm() > getParameter(0).getFloat();
        }

        if (type == VariantType::FramedDirection)
        {
            FramedDirectionPtr fV1 = value.getClass<FramedDirection>();
            Eigen::Vector3f vec = value.getClass<FramedDirection>()->toEigen();
            return (vec).norm() > getParameter(0).getFloat();
        }

        if (type == VariantType::LinkedDirection)
        {
            LinkedDirectionPtr lV1 = value.getClass<LinkedDirection>();

            Eigen::Vector3f vec = value.getClass<LinkedDirection>()->toEigen();
            return (vec).norm() > getParameter(0).getFloat();
        }

        return false;
    }


    ConditionCheckMagnitudeSmaller::ConditionCheckMagnitudeSmaller()
    {
        setNumberParameters(1);
        addSupportedType(VariantType::Vector3, createParameterTypeList(1, VariantType::Float));
        addSupportedType(VariantType::FramedDirection, createParameterTypeList(1, VariantType::Float));
        addSupportedType(VariantType::LinkedDirection, createParameterTypeList(1, VariantType::Float));

    }

    ConditionCheck* ConditionCheckMagnitudeSmaller::clone()
    {
        return new ConditionCheckMagnitudeSmaller(*this);
    }

    bool ConditionCheckMagnitudeSmaller::evaluate(const StringVariantMap& dataFields)
    {

        if (dataFields.size() != 1)
        {
            ARMARX_WARNING_S << "Size of dataFields: %d\n" << dataFields.size();
            throw InvalidConditionException("Wrong number of datafields for condition equals ");
        }

        const Variant& value = dataFields.begin()->second;
        VariantTypeId type = value.getType();

        if (type == VariantType::Vector3)
        {
            Eigen::Vector3f vec = value.getClass<Vector3>()->toEigen();
            return (vec).norm() < getParameter(0).getFloat();
        }

        if (type == VariantType::FramedDirection)
        {
            //            FramedDirectionPtr fV1 = value.getClass<FramedDirection>();
            Eigen::Vector3f vec = value.getClass<FramedDirection>()->toEigen();
            return (vec).norm() < getParameter(0).getFloat();
        }

        if (type == VariantType::LinkedDirection)
        {
            //            LinkedDirectionPtr lV1 = value.getClass<LinkedDirection>();
            Eigen::Vector3f vec = value.getClass<LinkedDirection>()->toEigen();
            return (vec).norm() < getParameter(0).getFloat();
        }

        return false;
    }

    ConditionCheckMagnitudeInRange::ConditionCheckMagnitudeInRange()
    {
        setNumberParameters(2);
        addSupportedType(VariantType::Vector3, createParameterTypeList(2, VariantType::Float, VariantType::Float));
        addSupportedType(VariantType::FramedDirection, createParameterTypeList(2, VariantType::Float, VariantType::Float));
        addSupportedType(VariantType::LinkedDirection, createParameterTypeList(2, VariantType::Float, VariantType::Float));

    }

    ConditionCheck* ConditionCheckMagnitudeInRange::clone()
    {
        return new ConditionCheckMagnitudeInRange(*this);
    }

    bool ConditionCheckMagnitudeInRange::evaluate(const StringVariantMap& dataFields)
    {
        if (dataFields.size() != 1)
        {
            ARMARX_WARNING_S << "Size of dataFields: " << dataFields.size();
            throw InvalidConditionException("Wrong number of datafields for condition InRange ");
        }

        const Variant& value = dataFields.begin()->second;
        VariantTypeId type = value.getType();

        if (type == VariantType::Vector3)
        {
            Eigen::Vector3f vec = value.getClass<Vector3>()->toEigen();
            return ((vec).norm() > getParameter(0).getFloat()) && ((vec).norm() < getParameter(1).getFloat());
        }

        if (type == VariantType::FramedDirection)
        {
            //            FramedDirectionPtr fV1 = value.getClass<FramedDirection>();
            Eigen::Vector3f vec = value.getClass<FramedDirection>()->toEigen();
            return ((vec).norm() > getParameter(0).getFloat()) && ((vec).norm() < getParameter(1).getFloat());
        }

        if (type == VariantType::LinkedDirection)
        {
            //            LinkedDirectionPtr lV1 = value.getClass<LinkedDirection>();
            Eigen::Vector3f vec = value.getClass<LinkedDirection>()->toEigen();
            return ((vec).norm() > getParameter(0).getFloat()) && ((vec).norm() < getParameter(1).getFloat());
        }

        return false;
    }

}
