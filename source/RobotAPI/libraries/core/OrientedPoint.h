/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Martin Miller (martin dot miller at student dot kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


#include <RobotAPI/interface/core/OrientedPoint.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <sstream>

namespace armarx::VariantType
{
    // variant types
    const VariantTypeId OrientedPoint = Variant::addTypeName("::armarx::OrientedPointBase");
}

namespace armarx
{
    class OrientedPoint:
        public virtual OrientedPointBase
    {

    public:
        OrientedPoint();
        OrientedPoint(const Eigen::Vector3f& position, const Eigen::Vector3f& normal);
        OrientedPoint(::Ice::Float px, ::Ice::Float py, ::Ice::Float pz, ::Ice::Float nx, ::Ice::Float ny, ::Ice::Float nz);

        virtual Eigen::Vector3f positionToEigen() const;
        virtual Eigen::Vector3f normalToEigen() const;

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const override
        {
            return this->clone();
        }
        VariantDataClassPtr clone(const Ice::Current& c = Ice::emptyCurrent) const override
        {
            return new OrientedPoint(*this);
        }
        std::string output(const Ice::Current& c = Ice::emptyCurrent) const override;
        VariantTypeId getType(const Ice::Current& c = Ice::emptyCurrent) const override
        {
            return VariantType::OrientedPoint;
        }
        bool validate(const Ice::Current& c = Ice::emptyCurrent) override
        {
            return true;
        }

        friend std::ostream& operator<<(std::ostream& stream, const OrientedPoint& rhs)
        {
            stream << "OrientedPoint: " << std::endl << rhs.output() << std::endl;
            return stream;
        }

    public: // serialization
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) const override;
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = Ice::emptyCurrent) override;
    };

    using OrientedPointPtr = IceInternal::Handle<OrientedPoint>;
}

