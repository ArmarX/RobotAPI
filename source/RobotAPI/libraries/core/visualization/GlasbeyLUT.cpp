#include "GlasbeyLUT.h"

#include <SimoxUtility/color/GlasbeyLUT.h>


armarx::DrawColor armarx::toDrawColor(Eigen::Vector4f c)
{
    return { c(0), c(1), c(2), c(3) };
}

armarx::DrawColor armarx::toDrawColor(simox::Color c)
{
    return toDrawColor(c.to_vector4f());
}

armarx::DrawColor24Bit armarx::toDrawColor24Bit(simox::Color c)
{
    return { c.r, c.g, c.b };
}


namespace armarx
{

    DrawColor GlasbeyLUT::at(std::size_t id, float alpha)
    {
        return toDrawColor(simox::color::GlasbeyLUT::atf(id, alpha));
    }

    DrawColor24Bit GlasbeyLUT::atByte(std::size_t id)
    {
        return toDrawColor24Bit(simox::color::GlasbeyLUT::at(id));
    }


    std::size_t GlasbeyLUT::size()
    {
        return simox::color::GlasbeyLUT::size();
    }

    const std::vector<unsigned char>& GlasbeyLUT::data()
    {
        return simox::color::GlasbeyLUT::data();
    }

}
