#pragma once

#include <cmath>
#include <type_traits>
#include <vector>

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>

#include <SimoxUtility/color/Color.h>


namespace armarx
{

    struct DrawColor;
    struct DrawColor24Bit;


    DrawColor toDrawColor(Eigen::Vector4f c);
    DrawColor toDrawColor(simox::Color c);
    DrawColor24Bit toDrawColor24Bit(simox::Color c);


    /**
     * "Color lookup table consisting of 256 colors structured in a maximally
     *  discontinuous manner. Generated using the method of Glasbey et al.
     *  (see https://github.com/taketwo/glasbey)" [1]
     *
     *
     * [1](https://github.com/PointCloudLibrary/pcl/blob/master/common/include/pcl/common/colors.h)
     */
    class GlasbeyLUT
    {
    public:

        static DrawColor at(std::size_t id, float alpha = 1.f);
        static DrawColor24Bit atByte(std::size_t id);

        /**
         * @brief Get a color from the lookup table with given ID (with float values).
         * The ID is automaticall wrapped if greater than `size()`.
         * If `id` is negative, its absolute value is used.
         */
        template <typename UIntT, std::enable_if_t<std::is_unsigned<UIntT>::value, int> = 0>
        static DrawColor at(UIntT id, float alpha = 1.f)
        {
            return at(static_cast<std::size_t>(id), alpha);
        }

        // If `id` is negative, its absolute value is used.
        template <typename IntT, std::enable_if_t<std::is_signed<IntT>::value, int> = 0>
        static DrawColor at(IntT id, float alpha = 1.f)
        {
            return at(static_cast<std::size_t>(id >= 0 ? id : std::abs(id)), alpha);
        }


        /**
         * @brief Get a color from the lookup table with given ID (with integer values).
         * The ID is automaticall wrapped if greater than `size()`.
         * If `id` is negative, its absolute value is used.
         */
        template <typename UIntT, std::enable_if_t<std::is_unsigned<UIntT>::value, int> = 0>
        static DrawColor24Bit atByte(UIntT id)
        {
            return atByte(static_cast<std::size_t>(id));
        }

        template <typename IntT, std::enable_if_t<std::is_signed<IntT>::value, int> = 0>
        static DrawColor24Bit atByte(IntT id)
        {
            return atByte(static_cast<std::size_t>(id >= 0 ? id : std::abs(id)));
        }


        /// Get the number of colors in the lookup table.;
        static std::size_t size();

        /// Get the raw lookup table (flat).
        static const std::vector<unsigned char>& data();


    private:

        /// Private constructor.
        GlasbeyLUT() = default;

    };

}
