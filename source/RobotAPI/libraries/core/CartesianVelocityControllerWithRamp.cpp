/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Sonja Marahrens (sonja dot marahrens)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CartesianVelocityControllerWithRamp.h"

#include <ArmarXCore/core/logging/Logging.h>

namespace armarx
{
    CartesianVelocityControllerWithRamp::CartesianVelocityControllerWithRamp(const VirtualRobot::RobotNodeSetPtr& rns, const Eigen::VectorXf& currentJointVelocity, VirtualRobot::IKSolver::CartesianSelection mode,
            float maxPositionAcceleration, float maxOrientationAcceleration, float maxNullspaceAcceleration, const VirtualRobot::RobotNodePtr& tcp)
        : controller(rns, tcp), mode(mode)
    {
        setMaxAccelerations(maxPositionAcceleration, maxOrientationAcceleration, maxNullspaceAcceleration);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
        setCurrentJointVelocity(currentJointVelocity);
#pragma GCC diagnostic pop
    }

    void CartesianVelocityControllerWithRamp::setCurrentJointVelocity(const Eigen::Ref<const Eigen::VectorXf>& currentJointVelocity)
    {
        Eigen::MatrixXf jacobi = controller.ik->getJacobianMatrix(controller._tcp, mode);
        //Eigen::MatrixXf inv = controller.ik->computePseudoInverseJacobianMatrix(jacobi, controller.ik->getJacobiRegularization(mode));


        Eigen::FullPivLU<Eigen::MatrixXf> lu_decomp(jacobi);
        Eigen::MatrixXf nullspace = lu_decomp.kernel();
        Eigen::VectorXf nsv = Eigen::VectorXf::Zero(nullspace.rows());

        for (int i = 0; i < nullspace.cols(); i++)
        {
            nsv += nullspace.col(i) * nullspace.col(i).dot(currentJointVelocity) / nullspace.col(i).squaredNorm();
        }
        cartesianVelocityRamp.setState(jacobi * currentJointVelocity, mode);
        nullSpaceVelocityRamp.setState(nsv);

    }

    void CartesianVelocityControllerWithRamp::switchMode(const Eigen::VectorXf& currentJointVelocity, VirtualRobot::IKSolver::CartesianSelection mode)
    {
        Eigen::MatrixXf jacobi = controller.ik->getJacobianMatrix(controller._tcp, mode);
        cartesianVelocityRamp.setState(jacobi * currentJointVelocity, mode);
        this->mode = mode;
    }

    VirtualRobot::IKSolver::CartesianSelection CartesianVelocityControllerWithRamp::getMode()
    {
        return mode;
    }

    Eigen::VectorXf CartesianVelocityControllerWithRamp::calculate(const Eigen::VectorXf& cartesianVel, float jointLimitAvoidanceScale, float dt)
    {
        Eigen::VectorXf nullspaceVel = controller.calculateNullspaceVelocity(cartesianVel, jointLimitAvoidanceScale, mode);
        return calculate(cartesianVel, nullspaceVel, dt);
    }

    Eigen::VectorXf CartesianVelocityControllerWithRamp::calculate(const Eigen::VectorXf& cartesianVel, const Eigen::VectorXf& nullspaceVel, float dt)
    {
        ARMARX_TRACE;
        return controller.calculate(cartesianVelocityRamp.update(cartesianVel, dt), nullSpaceVelocityRamp.update(nullspaceVel, dt), mode);
    }

    void CartesianVelocityControllerWithRamp::setMaxAccelerations(float maxPositionAcceleration, float maxOrientationAcceleration, float maxNullspaceAcceleration)
    {
        cartesianVelocityRamp.setMaxOrientationAcceleration(maxOrientationAcceleration);
        cartesianVelocityRamp.setMaxPositionAcceleration(maxPositionAcceleration);
        nullSpaceVelocityRamp.setMaxAccelaration(maxNullspaceAcceleration);
    }

    float CartesianVelocityControllerWithRamp::getMaxOrientationAcceleration()
    {
        return cartesianVelocityRamp.getMaxOrientationAcceleration();
    }

    float CartesianVelocityControllerWithRamp::getMaxPositionAcceleration()
    {
        return cartesianVelocityRamp.getMaxPositionAcceleration();
    }

    float CartesianVelocityControllerWithRamp::getMaxNullspaceAcceleration()
    {
        return nullSpaceVelocityRamp.getMaxAccelaration();
    }
}
