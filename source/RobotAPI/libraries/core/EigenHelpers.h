/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

namespace armarx
{

    template<class ScalarType>
    inline Eigen::Matrix<ScalarType, Eigen::Dynamic, 1> MakeVectorX(std::initializer_list<ScalarType> ilist)
    {
        Eigen::Matrix<ScalarType, Eigen::Dynamic, 1> r(ilist.size());
        std::size_t i = 0;
        for (const auto e : ilist)
        {
            r(i++) = e;
        }
        return r;
    }
    inline Eigen::Matrix<float, Eigen::Dynamic, 1> MakeVectorXf(std::initializer_list<float> ilist)
    {
        return MakeVectorX<float>(ilist);
    }

    template<class ScalarType, class...Ts>
    inline Eigen::Matrix<ScalarType, Eigen::Dynamic, 1> MakeVectorXWarnNarrowing(Ts&& ...ts)
    {
        return MakeVectorX<ScalarType>(std::initializer_list<ScalarType> {std::forward<Ts>(ts)...});
    }
    template<class ScalarType, class...Ts>
    inline Eigen::Matrix<ScalarType, Eigen::Dynamic, 1> MakeVectorXIgnoreNarrowing(Ts&& ...ts)
    {
        return MakeVectorX<ScalarType>(std::initializer_list<ScalarType> {static_cast<ScalarType>(std::forward<Ts>(ts))...});
    }
    template<class ScalarType, class...Ts>
    inline Eigen::Matrix<ScalarType, Eigen::Dynamic, 1> MakeVectorX(Ts&& ...ts)
    {
        return MakeVectorX<ScalarType>(std::initializer_list<ScalarType> {static_cast<ScalarType>(std::forward<Ts>(ts))...});
    }

    inline Eigen::Matrix<float, Eigen::Dynamic, 1> MakeVectorXf(std::vector<float> vec)
    {
        Eigen::Matrix<float, Eigen::Dynamic, 1> r(vec.size());
        std::size_t i = 0;
        for (const auto e : vec)
        {
            r(i++) = e;
        }
        return r;
    }

    /* Qt-Creator does not support variable amount of parameters. The following methods are only for Qt-Creator. */
    inline Eigen::Matrix<float, Eigen::Dynamic, 1> MakeVectorXf(float f1)
    {
        return MakeVectorX<float>(f1);
    }
    inline Eigen::Matrix<float, Eigen::Dynamic, 1> MakeVectorXf(float f1, float f2)
    {
        return MakeVectorX<float>(f1, f2);
    }
    inline Eigen::Matrix<float, Eigen::Dynamic, 1> MakeVectorXf(float f1, float f2, float f3)
    {
        return MakeVectorX<float>(f1, f2, f3);
    }
    inline Eigen::Matrix<float, Eigen::Dynamic, 1> MakeVectorXf(float f1, float f2, float f3, float f4)
    {
        return MakeVectorX<float>(f1, f2, f3, f4);
    }
    inline Eigen::Matrix<float, Eigen::Dynamic, 1> MakeVectorXf(float f1, float f2, float f3, float f4, float f5)
    {
        return MakeVectorX<float>(f1, f2, f3, f4, f5);
    }
    inline Eigen::Matrix<float, Eigen::Dynamic, 1> MakeVectorXf(float f1, float f2, float f3, float f4, float f5, float f6)
    {
        return MakeVectorX<float>(f1, f2, f3, f4, f5, f6);
    }
    inline Eigen::Matrix<float, Eigen::Dynamic, 1> MakeVectorXf(float f1, float f2, float f3, float f4, float f5, float f6, float f7)
    {
        return MakeVectorX<float>(f1, f2, f3, f4, f5, f6, f7);
    }
    inline Eigen::Matrix<float, Eigen::Dynamic, 1> MakeVectorXf(float f1, float f2, float f3, float f4, float f5, float f6, float f7, float f8)
    {
        return MakeVectorX<float>(f1, f2, f3, f4, f5, f6, f7, f8);
    }
    inline Eigen::Matrix<float, Eigen::Dynamic, 1> MakeVectorXf(float f1, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9)
    {
        return MakeVectorX<float>(f1, f2, f3, f4, f5, f6, f7, f8, f9);
    }
    inline Eigen::Matrix<float, Eigen::Dynamic, 1> MakeVectorXf(float f1, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10)
    {
        return MakeVectorX<float>(f1, f2, f3, f4, f5, f6, f7, f8, f9, f10);
    }


}
