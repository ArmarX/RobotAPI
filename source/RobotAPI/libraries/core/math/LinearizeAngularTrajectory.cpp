/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author      ()
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "LinearizeAngularTrajectory.h"
#include "MathUtils.h"

namespace armarx::math
{
    LinearizeAngularTrajectory::LinearizeAngularTrajectory(float initialLinearValue)
        : linearValue(initialLinearValue)
    {
    }

    float LinearizeAngularTrajectory::update(float angle)
    {
        linearValue = linearValue + MathUtils::angleModPI(angle - linearValue);
        return linearValue;
    }

    float LinearizeAngularTrajectory::getLinearValue()
    {
        return linearValue;
    }

    std::vector<float> LinearizeAngularTrajectory::Linearize(const std::vector<float>& data)
    {
        std::vector<float> result;
        result.reserve(data.size());
        if (data.size() == 0)
        {
            return result;
        }
        LinearizeAngularTrajectory lat(data.at(0));
        for (float v : data)
        {
            result.push_back(lat.update(v));
        }
        return result;
    }

    void LinearizeAngularTrajectory::LinearizeRef(std::vector<float>& data)
    {
        if (data.size() == 0)
        {
            return;
        }
        LinearizeAngularTrajectory lat(data.at(0));
        for (size_t i = 0; i < data.size(); i++)
        {
            data.at(i) = lat.update(data.at(i));
        }
    }

    std::vector<float> LinearizeAngularTrajectory::Angularize(const std::vector<float>& data, float center)
    {
        std::vector<float> result;
        result.reserve(data.size());
        for (float v : data)
        {
            result.push_back(MathUtils::angleModX(v, center));
        }
        return result;
    }

    void LinearizeAngularTrajectory::AngularizeRef(std::vector<float>& data, float center)
    {
        for (size_t i = 0; i < data.size(); i++)
        {
            data.at(i) = MathUtils::angleModX(data.at(i), center);
        }

    }
}
