/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author      ()
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>
#include <memory>

namespace armarx::math
{
    class LinearizeAngularTrajectory;
    using LinearizedAngularTrajectoryPtr = std::shared_ptr<LinearizeAngularTrajectory>;

    class LinearizeAngularTrajectory
    {
    public:
        LinearizeAngularTrajectory(float initialLinearValue);

        float update(float angle);
        float getLinearValue();

        static std::vector<float> Linearize(const std::vector<float>& data);
        static void LinearizeRef(std::vector<float>& data);
        static std::vector<float> Angularize(const std::vector<float>& data, float center = 0);
        static void AngularizeRef(std::vector<float>& data, float center = 0);

    private:
        float linearValue;
    };
}
