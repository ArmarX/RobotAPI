/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author      ()
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MathUtils.h"
#include "TimeSeriesUtils.h"
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

namespace armarx::math
{
    TimeSeriesUtils::TimeSeriesUtils()
    {
    }

    std::vector<float> TimeSeriesUtils::Resample(const std::vector<float>& timestamps, const std::vector<float>& data, const std::vector<float>& newTimestamps)
    {
        ARMARX_CHECK_EQUAL(data.size(), timestamps.size());
        ARMARX_CHECK_EQUAL(data.size(), newTimestamps.size());
        std::vector<float> result;
        result.reserve(data.size());

        if (data.size() == 0)
        {
            return result;
        }
        if (data.size() == 1)
        {
            result.push_back(data.at(0));
            return result;
        }

        size_t i = 0;
        size_t j = 0;

        while (j < data.size())
        {
            while (newTimestamps.at(j) > timestamps.at(i + 1) && i < data.size() - 2)
            {
                i++;
            }
            float f = math::MathUtils::ILerp(timestamps.at(i), timestamps.at(i + 1), newTimestamps.at(j));
            result.push_back(math::MathUtils::LerpClamp(data.at(i), data.at(i + 1), f));
            j++;
        }

        return result;
    }

    std::vector<float> TimeSeriesUtils::ApplyFilter(const std::vector<float>& data, const std::vector<float>& filter, BorderMode mode)
    {
        std::vector<float> result;
        size_t start = filter.size() / 2;
        for (size_t i = start; i < data.size() + start; i++)
        {
            float y = 0;
            float w = 0;
            for (size_t j = 0; j < filter.size(); j++)
            {
                int k = (int)i - (int)j;
                if (k < 0)
                {
                    k = 0;
                }
                if (k >= (int)data.size())
                {
                    k = data.size() - 1;
                }
                y += data.at(k) * filter.at(j);
                w += filter.at(j);
            }
            result.push_back(w == 0 ? 0 : y / w);
        }
        return result;
    }


    std::vector<float> TimeSeriesUtils::ApplyGaussianFilter(const std::vector<float>& data, float sigma, float sampleTime, BorderMode mode)
    {
        std::vector<float> filter = CreateGaussianFilter(sigma, sampleTime);
        return ApplyFilter(data, filter, mode);
    }

    std::vector<float> TimeSeriesUtils::CreateGaussianFilter(const float sigma, float sampleTime, float truncate)
    {
        std::vector<float> filter;
        int range = (int)(truncate * sigma / sampleTime);
        for (int i = -range; i <= range; i++)
        {
            float x = i * sampleTime;
            filter.push_back(exp(-x * x / (2 * sigma * sigma) / (sigma * sqrt(2 * M_PI))));
        }
        return filter;
    }

    std::vector<float> TimeSeriesUtils::MakeTimestamps(float start, float end, size_t count)
    {
        std::vector<float> result;
        for (size_t i = 0; i < count; i++)
        {
            result.push_back(MathUtils::Lerp(start, end, (float)i / (float)(count - 1)));
        }
        return result;
    }
}
