/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/exceptions/LocalException.h>

#include <cmath>
#include <vector>

namespace armarx::math
{
    class StatUtils
    {
    public:
        static float GetPercentile(const std::vector<float>& sortedData, float percentile)
        {
            if (sortedData.size() == 0)
            {
                throw LocalException("GetPercentile not possible for empty vector");
            }

            float indexf = (sortedData.size() - 1) * percentile;
            indexf = std::max(0.f, std::min(sortedData.size() - 1.f, indexf));
            int index = (int)indexf;
            float f = indexf - index;

            if (index == (int)sortedData.size() - 1)
            {
                return sortedData.at(sortedData.size() - 1);
            }

            return sortedData.at(index) * (1 - f) + sortedData.at(index + 1) * f;
        }
        static std::vector<float> GetPercentiles(const std::vector<float>& sortedData, int percentiles)
        {
            std::vector<float> result;
            result.push_back(sortedData.at(0));

            for (int i = 1; i < percentiles; i++)
            {
                result.push_back(GetPercentile(sortedData, 1.f / percentiles * i));
            }

            result.push_back(sortedData.at(sortedData.size() - 1));
            return result;
        }
        static float GetMedian(const std::vector<float>& sortedData)
        {
            return GetPercentile(sortedData, 0.5f);
        }
    };
}


