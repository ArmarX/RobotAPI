/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include "MathUtils.h"
namespace armarx::colorutils
{

    DrawColor24Bit HsvToRgb(const HsvColor& in)
    {
        double      hh, p, q, t, ff;
        long        i;
        double r, g, b;
        if (in.s <= 0.0)        // < is bogus, just shuts up warnings
        {
            r = in.v;
            g = in.v;
            b = in.v;
            return DrawColor24Bit {(Ice::Byte)(r * 255), (Ice::Byte)(g * 255), (Ice::Byte)(b * 255)};
        }
        hh = in.h;
        if (hh >= 360.0)
        {
            hh = 0.0;
        }
        hh /= 60.0;
        i = (long)hh;
        ff = hh - i;
        p = in.v * (1.0 - in.s);
        q = in.v * (1.0 - (in.s * ff));
        t = in.v * (1.0 - (in.s * (1.0 - ff)));

        switch (i)
        {
            case 0:
                r = in.v;
                g = t;
                b = p;
                break;
            case 1:
                r = q;
                g = in.v;
                b = p;
                break;
            case 2:
                r = p;
                g = in.v;
                b = t;
                break;

            case 3:
                r = p;
                g = q;
                b = in.v;
                break;
            case 4:
                r = t;
                g = p;
                b = in.v;
                break;
            case 5:
            default:
                r = in.v;
                g = p;
                b = q;
                break;
        }

        return DrawColor24Bit {(Ice::Byte)(r * 255), (Ice::Byte)(g * 255), (Ice::Byte)(b * 255)};
    }

    HsvColor RgbToHsv(const DrawColor24Bit& in)
    {
        double r = 0.00392156862 * in.r;
        double g = 0.00392156862 * in.g;
        double b = 0.00392156862 * in.b;
        HsvColor         out;
        double      min, max, delta;

        min = r < g ? r : g;
        min = min  < b ? min  : b;

        max = r > g ? r : g;
        max = max  > b ? max  : b;

        out.v = max;                                // v
        delta = max - min;
        if (delta < 0.00001)
        {
            out.s = 0;
            out.h = 0; // undefined, maybe nan?
            return out;
        }
        if (max > 0.0)    // NOTE: if Max is == 0, this divide would cause a crash
        {
            out.s = (delta / max);                  // s
        }
        else
        {
            // if max is 0, then r = g = b = 0
            // s = 0, h is undefined
            out.s = 0.0;
            out.h = 0;                            // its now undefined
            return out;
        }
        if (r >= max)                            // > is bogus, just keeps compilor happy
        {
            out.h = (g - b) / delta;    // between yellow & magenta
        }
        else if (g >= max)
        {
            out.h = 2.0 + (b - r) / delta;    // between cyan & yellow
        }
        else
        {
            out.h = 4.0 + (r - g) / delta;    // between magenta & cyan
        }

        out.h *= 60.0;                              // degrees

        if (out.h < 0.0)
        {
            out.h += 360.0;
        }

        return out;


    }

    /**
     * @brief HeatMapColor calculates the color of a value between 0 and 1 on a heat map.
     * @param percentage value between 0..1
     * @return color on a heatmap corresponding to parameter. 0 -> blue, 1 -> red. Color has full (255) saturation and value.
     */
    HsvColor HeatMapColor(float percentage)
    {
        percentage = math::MathUtils::LimitMinMax(0.0f, 1.0f, percentage);

        return HsvColor {((1.0f - percentage) * 240.f), 1.0f, 1.0f};
    }

    DrawColor24Bit HeatMapRGBColor(float percentage)
    {
        return HsvToRgb(HeatMapColor(percentage));
    }

    DrawColor HeatMapRGBAColor(float percentage)
    {
        auto color = HsvToRgb(HeatMapColor(percentage));
        return DrawColor {0.0039215686f * color.r, //divide by 255
                          0.0039215686f * color.g,
                          0.0039215686f * color.b,
                          1.0
                         };
    }

}
