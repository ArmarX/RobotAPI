/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author      ()
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/IK/DifferentialIK.h>
#include <VirtualRobot/VirtualRobot.h>

#include <Eigen/Core>

namespace armarx
{
    class CartesianVelocityController;
    using CartesianVelocityControllerPtr = std::shared_ptr<CartesianVelocityController>;

    class CartesianVelocityController
    {
    public:
        CartesianVelocityController(const VirtualRobot::RobotNodeSetPtr& rns, const VirtualRobot::RobotNodePtr& tcp = nullptr,
                                    const VirtualRobot::JacobiProvider::InverseJacobiMethod invJacMethod = VirtualRobot::JacobiProvider::eSVDDamped,
                                    bool _considerJointLimits = true);

        CartesianVelocityController(CartesianVelocityController&&) = default;
        CartesianVelocityController& operator=(CartesianVelocityController&&) = default;

        Eigen::VectorXf calculate(const Eigen::VectorXf& cartesianVel, VirtualRobot::IKSolver::CartesianSelection mode);
        Eigen::VectorXf calculate(const Eigen::VectorXf& cartesianVel, float KpJointLimitAvoidanceScale, VirtualRobot::IKSolver::CartesianSelection mode);
        Eigen::VectorXf calculate(const Eigen::VectorXf& cartesianVel, const Eigen::VectorXf& nullspaceVel, VirtualRobot::IKSolver::CartesianSelection mode);
        Eigen::VectorXf calculateJointLimitAvoidance();
        Eigen::VectorXf calculateJointLimitAvoidanceWithMargins(const Eigen::VectorXf& margins);
        Eigen::VectorXf calculateNullspaceVelocity(const Eigen::VectorXf& cartesianVel, float KpScale, VirtualRobot::IKSolver::CartesianSelection mode);

        void setCartesianRegularization(float cartesianMMRegularization, float cartesianRadianRegularization);

        bool getConsiderJointLimits() const;
        void setConsiderJointLimits(bool value);

        Eigen::MatrixXf jacobi;
        VirtualRobot::RobotNodeSetPtr rns;
        VirtualRobot::DifferentialIKPtr ik;
        VirtualRobot::RobotNodePtr _tcp;
        Eigen::VectorXf maximumJointVelocities;

        void setJointCosts(const std::vector<float>& _jointCosts);
        Eigen::VectorXf calculateRegularization(VirtualRobot::IKSolver::CartesianSelection mode);

    private:
        void calculateJacobis(VirtualRobot::IKSolver::CartesianSelection mode);
        Eigen::MatrixXf _jacobiWithCosts;
        Eigen::MatrixXf _inv;
        bool clampJacobiAtJointLimits(VirtualRobot::IKSolver::CartesianSelection mode, const Eigen::VectorXf& cartesianVel, Eigen::MatrixXf& jacobi, Eigen::MatrixXf& _inv, float jointLimitCheckAccuracy = 0.001f);
        bool _considerJointLimits = true;
        float _cartesianMMRegularization;
        float _cartesianRadianRegularization;
        Eigen::VectorXf _jointCosts;
    };
}

