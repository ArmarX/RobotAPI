/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/IK/DifferentialIK.h>
#include <VirtualRobot/Robot.h>
#include <Eigen/Dense>
#include <VirtualRobot/math/AbstractFunctionR1R6.h>

namespace armarx
{
    class CartesianFeedForwardPositionController;
    using CartesianFeedForwardPositionControllerPtr = std::shared_ptr<CartesianFeedForwardPositionController>;

    class CartesianFeedForwardPositionController
    {
    public:
        CartesianFeedForwardPositionController(const VirtualRobot::RobotNodePtr& tcp);
        Eigen::VectorXf calculate(const ::math::AbstractFunctionR1R6Ptr& trajectory, float t, VirtualRobot::IKSolver::CartesianSelection mode);

        float getPositionError(const ::math::AbstractFunctionR1R6Ptr& trajectory, float t);
        float getOrientationError(const ::math::AbstractFunctionR1R6Ptr& trajectory, float t);
        Eigen::Vector3f getPositionDiff(const ::math::AbstractFunctionR1R6Ptr& trajectory, float t);
        Eigen::Vector3f getOrientationDiff(const ::math::AbstractFunctionR1R6Ptr& trajectory, float t);

        float KpPos = 1;
        float KpOri = 1;
        float maxPosVel = -1;
        float maxOriVel = -1;
        bool enableFeedForward = true;

    private:
        VirtualRobot::RobotNodePtr tcp;
    };
}
