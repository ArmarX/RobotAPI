/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::DebugDrawerTopicTest::Test

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>

#include <ArmarXCore/core/test/IceTestHelper.h>

#include <RobotAPI/libraries/core/visualization/DebugDrawerTopic.h>


using namespace armarx;


// PCL-like dummy types.

struct PointXYZ
{
    float x, y, z;
};

struct PointXYZRGBA : public PointXYZ
{
    uint8_t r, g, b, a;
};

struct PointXYZRGBL : public PointXYZRGBA
{
    uint32_t label;
};

template <class PointT>
struct PointCloud
{
private:
    /// The point container type.
    using VectorT = std::vector<PointT>;

public:

    PointCloud() {}
    PointCloud(const VectorT& points) : points(points) {}

    // Container methods.
    std::size_t size() const
    {
        return points.size();
    }

    PointT& operator[](std::size_t i)
    {
        return points[i];
    }
    const PointT& operator[](std::size_t i) const
    {
        return points[i];
    }

    // Iterators.
    typename VectorT::iterator begin()
    {
        return points.begin();
    }
    typename VectorT::const_iterator begin() const
    {
        return points.begin();
    }
    typename VectorT::iterator end()
    {
        return points.end();
    }
    typename VectorT::const_iterator end() const
    {
        return points.end();
    }


    /// The points.
    VectorT points;
};


/* These test do not actually check any behaviour,
 * but check whether this code compiles.
 */

template <class PointT>
struct Fixture
{
    Fixture()
    {
    }

    const DebugDrawerTopic::VisuID id {"layer", "name"};
    const int pointSize = 10;

    DebugDrawerTopic drawer;

    PointCloud<PointT> pointCloudMutable;
    const PointCloud<PointT>& pointCloud = pointCloudMutable;
};


BOOST_FIXTURE_TEST_CASE(test_drawPointCloud_PointXYZ, Fixture<PointXYZ>)
{
    pointCloudMutable.points = { {1, 2, 3}, {2, 3, 4}, {3, 4, 5} };

    drawer.drawPointCloud(id, pointCloud);
    drawer.drawPointCloud(id, pointCloud.points, DrawColor {0, 0.5, 1, 1});

    drawer.drawPointCloud(id, pointCloud,
                          [](const PointXYZ&)
    {
        return DrawColor{0, 0.5, 1, 1};
    }, pointSize);
}


BOOST_FIXTURE_TEST_CASE(test_drawPointCloud_PointXYZRGBA, Fixture<PointXYZRGBA>)
{
    drawer.drawPointCloud(id, pointCloud);
    drawer.drawPointCloud(id, pointCloud.points, DrawColor {0, 0.5, 1, 1});

    drawer.drawPointCloud(id, pointCloud,
                          [](const PointXYZRGBA&)
    {
        return DrawColor{0, 0.5, 1, 1};
    }, pointSize);

    drawer.drawPointCloudRGBA(id, pointCloud, pointSize);
}


BOOST_FIXTURE_TEST_CASE(test_drawPointCloud_PointXYZRGBL, Fixture<PointXYZRGBL>)
{
    drawer.drawPointCloud(id, pointCloud);
    drawer.drawPointCloud(id, pointCloud.points, DrawColor {0, 0.5, 1, 1});

    drawer.drawPointCloud(id, pointCloud,
                          [](const PointXYZRGBL&)
    {
        return DrawColor{0, 0.5, 1, 1};
    }, pointSize);

    drawer.drawPointCloudRGBA(id, pointCloud, pointSize);
}



