/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::RobotTest::Test
#define ARMARX_BOOST_TEST
#include <RobotAPI/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <VirtualRobot/XML/RobotIO.h>
#include "../RobotPool.h"

using namespace armarx;


BOOST_AUTO_TEST_CASE(RobotPoolTest)
{
    const std::string project = "RobotAPI";
    armarx::CMakePackageFinder finder(project);

    ARMARX_CHECK_EXPRESSION(finder.packageFound()) << project;


    armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());

    std::string fn = "RobotAPI/robots/Armar3/ArmarIII.xml";
    ArmarXDataPath::getAbsolutePath(fn, fn);
    std::string robotFilename = fn;
    VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(robotFilename, VirtualRobot::RobotIO::eCollisionModel);
    RobotPool pool(robot, 2);
    {
        BOOST_CHECK_EQUAL(pool.getPoolSize(), 2);
        auto clone = pool.getRobot();
        BOOST_CHECK_EQUAL(pool.getPoolSize(), 2);
        BOOST_CHECK_EQUAL(pool.getRobotsInUseCount(), 1);
        {
            auto clone2 = pool.getRobot();
            BOOST_CHECK_EQUAL(pool.getPoolSize(), 2);
            BOOST_CHECK_EQUAL(pool.getRobotsInUseCount(), 2);
        }
        BOOST_CHECK_EQUAL(pool.getPoolSize(), 2);
        BOOST_CHECK_EQUAL(pool.getRobotsInUseCount(), 1);
        auto clone2 = pool.getRobot();
        BOOST_CHECK_EQUAL(pool.getPoolSize(), 2);
        BOOST_CHECK_EQUAL(pool.getRobotsInUseCount(), 2);
    }
    BOOST_CHECK_EQUAL(pool.getRobotsInUseCount(), 0);
    BOOST_CHECK_EQUAL(pool.clean(), 2);
    BOOST_CHECK_EQUAL(pool.getPoolSize(), 0);

}

