/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE RobotAPI::Trajectory::Test
#define ARMARX_BOOST_TEST
#include <RobotAPI/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include "../Trajectory.h"
#include "../TrajectoryController.h"
#include <RobotAPI/libraries/core/math/MathUtils.h>
#include <random>
using namespace armarx;


BOOST_AUTO_TEST_CASE(TrajectoryBasicUsage)
{
    //! [TrajectoryDocumentation BasicUsage]
    Ice::DoubleSeq joint1Values {0, 5, 1}; // fill with your values;
    Ice::DoubleSeq joint2Values {1, 3, 1}; // fill with your values;
    Ice::DoubleSeq joint3Values {0, 2, 5}; // fill with your values;
    Ice::DoubleSeq timestamps = Trajectory::GenerateTimestamps(0, 1, 1.0 / (joint1Values.size() - 1)); // if you dont have timestamps
    TrajectoryPtr trajectory(new Trajectory(DoubleSeqSeq {joint1Values, joint2Values}, timestamps, {"Shoulder 0 R", "Shoulder 1 R"}));
    trajectory->addDimension(joint3Values, timestamps, "Shoulder 2 R");

    double timestamp = 0.1;
    double dimension = 0;
    double derivation = 0;
    double j1posAtT = trajectory->getState(timestamp, dimension, derivation);
    // or
    Ice::DoubleSeq j1positions = trajectory->getDimensionData(dimension, derivation);
    //! [TrajectoryDocumentation BasicUsage]
    ARMARX_INFO_S << VAROUT(j1posAtT) << VAROUT(j1positions);
}

BOOST_AUTO_TEST_CASE(TrajectoryIteratorUsage)
{
    Ice::DoubleSeq myPositionValues {0, 5, 1}; // fill with your values;
    Ice::DoubleSeq timestamps = Trajectory::GenerateTimestamps(0, 1, 1.0 / (myPositionValues.size() - 1)); // if you dont have timestamps
    TrajectoryPtr trajectory(new Trajectory);
    trajectory->addDimension(myPositionValues, timestamps, "Shoulder 1 L");
    //! [TrajectoryDocumentation IteratorUsage]
    Trajectory::ordered_view::iterator it; // or just auto it;
    it = trajectory->begin();
    for (; it != trajectory->end(); it++)
    {
        const Trajectory::TrajData& data = *it;
        ARMARX_INFO_S << "Position at " << data.getTimestamp() << " :" << data.getPosition(0);
    }

    // or with c++11 for loop:
    for (const Trajectory::TrajData& element : *trajectory)
    {
        ARMARX_INFO_S << "Position at " << element.getTimestamp() << " :" << element.getPosition(0);
    }
    //! [TrajectoryDocumentation IteratorUsage]
}

BOOST_AUTO_TEST_CASE(testInplaceDerivation2)
{

    Ice::DoubleSeq timestamps { 0, 1, 2 };
    Ice::DoubleSeq positions { 1, 2, 5 };

    TrajectoryPtr traj = new Trajectory(positions, timestamps);
    TrajectoryPtr traj2 = new Trajectory(positions, timestamps);
    traj->differentiateDiscretly(1);
    BOOST_CHECK_EQUAL(traj->getState(-1, 0, 1), traj2->getState(-1, 0, 1));
}

BOOST_AUTO_TEST_CASE(testInplaceDerivation3)
{

    Ice::DoubleSeq timestamps { 0, 1, 2 };
    Ice::DoubleSeq positions { 1, 2, 5 };

    TrajectoryPtr traj = new Trajectory(positions, timestamps);
    TrajectoryPtr traj2 = new Trajectory(positions, timestamps);
    traj->differentiateDiscretly(2);
    BOOST_CHECK_EQUAL(traj->getState(-1, 0, 2), traj2->getState(-1, 0, 2));
}

BOOST_AUTO_TEST_CASE(testDerivationRemoval)
{

    Ice::DoubleSeq timestamps { 0, 1, 2 };
    Ice::DoubleSeq positions { 1, 2, 5 };

    TrajectoryPtr traj = new Trajectory(positions, timestamps);
    //    TrajectoryPtr traj2 = new Trajectory(timestamps, positions);
    traj->differentiateDiscretly(2);
    BOOST_CHECK_EQUAL(traj->begin()->getData().at(0)->size(), 3);
    traj->removeDerivation(1);
    BOOST_CHECK_EQUAL(traj->begin()->getData().at(0)->size(), 1);
}



BOOST_AUTO_TEST_CASE(TrajectoryMarshalTest)
{

    Ice::DoubleSeq timestamps { 0, 1, 2 };
    Ice::DoubleSeq positions { 1, 2, 5 };

    TrajectoryPtr traj = new Trajectory(positions, timestamps);
    traj->ice_preMarshal();
    TrajectoryPtr traj2 = TrajectoryPtr::dynamicCast(traj->clone());
    traj2->clear();
    traj2->ice_postUnmarshal();
    BOOST_CHECK_EQUAL(traj->getState(0), traj2->getState(0));
}

BOOST_AUTO_TEST_CASE(TrajectoryPerformanceTest)
{

    Ice::DoubleSeq timestamps;
    int count = 1000;
    for (int i = 0; i < count; ++i)
    {
        timestamps.push_back(i);
    }
    TrajectoryPtr traj(new Trajectory());
    for (int var = 0; var < 8; ++var)
    {
        Ice::DoubleSeq p;
        p.reserve(count);
        for (int i = 0; i < count; ++i)
        {
            p.push_back(rand());
        }
        traj->addDimension(p, timestamps);
    }
    ARMARX_INFO_S << "Starting";
    traj->ice_preMarshal();
    ARMARX_INFO_S << "Marshalled";
    TrajectoryPtr traj2 = TrajectoryPtr::dynamicCast(traj->clone());
    ARMARX_INFO_S << "Cloned";

    traj2->clear();
    ARMARX_INFO_S << "Cleared";
    traj2->ice_postUnmarshal();
    ARMARX_INFO_S << "Unmarshalled";
    traj->getLength();
    ARMARX_INFO_S << "Length calculated";
    IceUtil::Time start = IceUtil::Time::now();
    float dummyVar = 0;
    int dims = traj->dim();
    for (const Trajectory::TrajData& data : *traj)
    {
        for (int d = 0; d < dims; ++d)
        {
            dummyVar += data.getPosition(d);
        }
    }
    IceUtil::Time end = IceUtil::Time::now();
    ARMARX_INFO << "time per iteration in microseconds: " << (end - start).toMicroSecondsDouble() / traj->size();
    int iterationCount = 0;
    start = IceUtil::Time::now();
    for (float t = 0; t < 1000; t += 0.32)
    {
        for (int d = 0; d < dims; ++d)
        {
            dummyVar += traj->getState(t, d);
        }
        iterationCount++;
    }
    end = IceUtil::Time::now();
    ARMARX_INFO << "time per iteration with interpolation in microseconds: " << (end - start).toMicroSecondsDouble() / iterationCount;
    ARMARX_INFO << VAROUT(dummyVar);
    BOOST_CHECK_EQUAL(traj->getState(0), traj2->getState(0));
}

BOOST_AUTO_TEST_CASE(TrajectoryUnmarshalTest)
{
    Ice::DoubleSeq timestamps;
    int count = 1000;
    for (int i = 0; i < count; ++i)
    {
        timestamps.push_back(i);
    }
    TrajectoryPtr traj(new Trajectory());
    for (int var = 0; var < 8; ++var)
    {
        Ice::DoubleSeq p;
        p.reserve(count);
        for (int i = 0; i < count; ++i)
        {
            p.push_back(rand());
        }
        traj->addDimension(p, timestamps);
    }
    ARMARX_INFO_S << "Starting";
    traj->ice_preMarshal();

    traj->clear();
    traj->ice_postUnmarshal();
    ARMARX_INFO << "length: " << traj->getLength();
    traj->getLength(0, 50, 350);
}


BOOST_AUTO_TEST_CASE(TrajectorySerializationTest)
{

    FloatSeqSeq positions {{ 1, 2, 5 }, { 10, 2332, 53425 }};
    JSONObjectPtr json = new JSONObject();
    TrajectoryPtr traj =  new Trajectory(positions, {}, {"joint1", "joint2"});
    traj->serialize(json);
    ARMARX_INFO_S << json->asString(true);
    TrajectoryPtr traj2 =  new Trajectory();
    traj2->deserialize(json);
    BOOST_CHECK_CLOSE(traj->getState(0), traj2->getState(0), 0.01);
    BOOST_CHECK_CLOSE(traj->getState(0.4, 1), traj2->getState(0.4, 1), 0.01);
    BOOST_CHECK_EQUAL(traj->getDimensionName(0), traj2->getDimensionName(0));
}

BOOST_AUTO_TEST_CASE(TrajectoryLengthTest)
{

    FloatSeqSeq jointtrajectories {{ 0, 0, 1 }, { 0, 1, 1 }};
    TrajectoryPtr traj = new Trajectory(jointtrajectories, {}, {"joint1", "joint2"});

    BOOST_CHECK_EQUAL(traj->getLength(), 2);
}

BOOST_AUTO_TEST_CASE(TrajectoryCopyIfTest)
{
    //! [TrajectoryDocumentation CopyIf]
    FloatSeqSeq positions {{ 0, 0, 1 }, { 0, 1, 1 }};
    TrajectoryPtr traj = new Trajectory(positions, {}, {"joint1", "joint2"});
    std::vector<Trajectory::TrajData> selection(traj->size());
    // select all elements with velocity > 0
    auto it = std::copy_if(traj->begin(), traj->end(), selection.begin(),
                           [](const Trajectory::TrajData & elem)
    {
        return fabs(elem.getDeriv(0, 1)) > 0;
    });
    selection.resize(std::distance(selection.begin(), it));
    //! [TrajectoryDocumentation CopyIf]
    ARMARX_INFO_S << VAROUT(*traj);
    ARMARX_INFO_S << VAROUT(selection);
}

BOOST_AUTO_TEST_CASE(TrajectoryControllerTest)
{
    FloatSeqSeq positions {{ 0, 0, 1 }, { 0, 1, 1 }};
    TrajectoryPtr traj = new Trajectory(positions, {}, {"joint1", "joint2"});

    TrajectoryController c(traj, 0, 1);
    float t = 0.0f;
    float deltaT = 0.001f;

    std::default_random_engine generator;
    std::normal_distribution<float> distribution(1.0, 0.001);

    Eigen::VectorXf pos = Eigen::VectorXf::Zero(traj->dim(), 1);
    while (t < 1.0f)
    {
        Eigen::VectorXf vel = c.update(deltaT, pos);
        pos += vel * deltaT *  distribution(generator);
        ARMARX_INFO /*<< deactivateSpam(0.01)*/ << VAROUT(t) << VAROUT(pos);
        t += deltaT;
    }


}

BOOST_AUTO_TEST_CASE(TrajectoryControllerUnfoldingTest)
{
    BOOST_CHECK_LT(std::abs(armarx::math::MathUtils::AngleDelta(1.5 * M_PI, 0.25 * M_PI) / M_PI + armarx::math::MathUtils::AngleDelta(0.25 * M_PI, 1.5 * M_PI) / M_PI), 0.001);
    Ice::FloatSeq waypoints = {3 * M_PI, 2.5 * M_PI, -2 * M_PI};
    Ice::FloatSeq trajPoints;
    float currentPoint = 0.0f;
    float step = 1.f;
    for (auto waypoint : waypoints)
    {
        int direction = armarx::math::MathUtils::Sign(waypoint - currentPoint);
        while (std::abs(currentPoint - waypoint) > step)
        {
            currentPoint += step * direction;
            trajPoints.push_back(armarx::math::MathUtils::angleModPI(currentPoint));
        }
    }
    //    ARMARX_INFO << VAROUT(trajPoints);
    //FloatSeqSeq positions {{ 0, -M_PI * 0.99, -1.5 * M_PI * 0.99, 0, 0.1 * M_PI, - M_PI }/*, { -M_PI, 0, M_PI, 1.5 * M_PI, M_PI, - 2 * M_PI }*/};
    TrajectoryPtr origTraj = new Trajectory(FloatSeqSeq {trajPoints}, {}, {"joint1"/*, "joint2"*/});
    TrajectoryPtr traj = new Trajectory(FloatSeqSeq {trajPoints}, {}, {"joint1"/*, "joint2"*/});

    traj->setLimitless({{true, -M_PI, M_PI}});
    TrajectoryController::UnfoldLimitlessJointPositions(traj);
    ARMARX_INFO << "unfolded traj:\n" << traj->toEigen();
    TrajectoryController::FoldLimitlessJointPositions(traj);
    ARMARX_INFO << "folded traj:\n" << traj->toEigen();
    for (auto& data : *origTraj)
    {
        auto t = data.getTimestamp();
        auto pos = data.getPosition(0);
        BOOST_CHECK(std::abs(pos - traj->getState(t, 0, 0)) < 0.01);
    }

}

BOOST_AUTO_TEST_CASE(TrajectoryShiftTimeTest)
{
    FloatSeqSeq positions {{ 0, 0, 1 }, { 0, 1, 1 }};
    TrajectoryPtr traj = new Trajectory(positions, {}, {"joint1", "joint2"});
    ARMARX_INFO << VAROUT(traj->getDimensionNames());
    auto startTime = traj->begin()->timestamp;
    auto shift = 2.0;
    traj->shiftTime(shift);
    ARMARX_INFO << "pos: " << traj->begin()->getPosition(0);
    BOOST_CHECK_LE(std::abs(startTime + shift - traj->begin()->timestamp), 0.0001);
    ARMARX_INFO << VAROUT(traj->getDimensionNames());
    BOOST_CHECK_EQUAL(traj->getDimensionNames().at(0), "joint1");
}

