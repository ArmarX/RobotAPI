set(LIBS ${LIBS} RobotAPICore )

armarx_add_test(RobotTest RobotTest.cpp "${LIBS}")
armarx_add_test(TrajectoryTest TrajectoryTest.cpp "${LIBS}")
armarx_add_test(PIDControllerTest PIDControllerTest.cpp "${LIBS}")

armarx_add_test(MathUtilsTest MathUtilsTest.cpp "${LIBS}")
armarx_add_test(CartesianVelocityControllerTest CartesianVelocityControllerTest.cpp "${LIBS}")

armarx_add_test(CartesianVelocityRampTest CartesianVelocityRampTest.cpp "${LIBS}")
armarx_add_test(CartesianVelocityControllerWithRampTest CartesianVelocityControllerWithRampTest.cpp "${LIBS}")

armarx_add_test(DebugDrawerTopicTest    DebugDrawerTopicTest.cpp    "${LIBS}")
