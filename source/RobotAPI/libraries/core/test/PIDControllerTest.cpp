/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE RobotAPI::PIDController::Test
#define ARMARX_BOOST_TEST
#include <RobotAPI/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include "../PIDController.h"

using namespace armarx;


BOOST_AUTO_TEST_CASE(PIDControllerTest)
{
    PIDController c(1, 0, 0, 0.1, 0.1);
    c.update(0.1, 0, 1);
    ARMARX_INFO << "velocity: " << c.getControlValue();
    BOOST_CHECK_LE(c.getControlValue(), 0.1);
    BOOST_CHECK_LE(c.controlValueDerivation, 0.11);

}

void check_close(float a, float b, float tolerance)
{
    if (fabs(a - b) > tolerance)
    {
        ARMARX_ERROR << "fabs(a-b) > tolerance. a=" << a << " b=" << b << " tolerance=" << tolerance;
        BOOST_FAIL("");
    }
}

BOOST_AUTO_TEST_CASE(PIDControllerContinuousTest)
{
    PIDController c(1, 0, 0, 10, 10, true);

    for (int a = -2; a <= 2; a++)
    {
        for (int b = -2; b <= 2; b++)
        {
            for (int t = -2; t <= 2; t++)
            {
                c.update(1, 0 + a * M_PI * 2, t + b * M_PI * 2);
                check_close(c.getControlValue(), t, 0.01);

                c.update(1, t + a * M_PI * 2, 0 + b * M_PI * 2);
                check_close(c.getControlValue(), -t, 0.01);
            }
        }
    }
}
