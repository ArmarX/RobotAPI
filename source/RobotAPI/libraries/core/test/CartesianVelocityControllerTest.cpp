/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::RobotAPI::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE RobotAPI::CartesianVelocityController::Test
#define ARMARX_BOOST_TEST
#include <RobotAPI/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include "../CartesianVelocityController.h"

using namespace armarx;

void check_close(float a, float b, float tolerance)
{
    if (fabs(a - b) > tolerance)
    {
        ARMARX_ERROR << "fabs(a-b) > tolerance. a=" << a << " b=" << b << " tolerance=" << tolerance;
        BOOST_FAIL("");
    }
}




BOOST_AUTO_TEST_CASE(test1)
{
    const std::string project = "RobotAPI";
    armarx::CMakePackageFinder finder(project);

    if (!finder.packageFound())
    {
        ARMARX_ERROR_S << "ArmarX Package " << project << " has not been found!";
    }
    else
    {
        armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());
    }
    std::string fn = "RobotAPI/robots/Armar3/ArmarIII.xml";
    ArmarXDataPath::getAbsolutePath(fn, fn);
    std::string robotFilename = fn;
    VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(robotFilename, VirtualRobot::RobotIO::eStructure);
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet("HipYawRightArm");
    CartesianVelocityControllerPtr h(new CartesianVelocityController(rns));
    Eigen::VectorXf jointVel(8);
    jointVel << 1, 1, 1, 1, 1, 1, 1, 1;

    Eigen::VectorXf targetTcpVel(6);
    targetTcpVel << 0, 0, 0, 0, 0, 0;

    ARMARX_IMPORTANT << rns->getJointValues();
    Eigen::VectorXf vel = h->calculate(targetTcpVel, jointVel, VirtualRobot::IKSolver::CartesianSelection::All);
    ARMARX_IMPORTANT << vel.transpose();
    Eigen::VectorXf tcpVel = (h->jacobi * vel).transpose();
    ARMARX_IMPORTANT << tcpVel;
    BOOST_CHECK_LE((targetTcpVel - tcpVel).norm(), 0.01f);

}

BOOST_AUTO_TEST_CASE(testJointLimitAwareness)
{
    const std::string project = "RobotAPI";
    armarx::CMakePackageFinder finder(project);

    if (!finder.packageFound())
    {
        ARMARX_ERROR_S << "ArmarX Package " << project << " has not been found!";
    }
    else
    {
        armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());
    }
    std::string fn = "RobotAPI/robots/Armar3/ArmarIII.xml";
    ArmarXDataPath::getAbsolutePath(fn, fn);
    std::string robotFilename = fn;
    VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(robotFilename, VirtualRobot::RobotIO::eStructure);
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet("TorsoRightArm");
    CartesianVelocityControllerPtr h(new CartesianVelocityController(rns, nullptr, VirtualRobot::JacobiProvider::eSVDDamped));
    const Eigen::VectorXf oldJV = rns->getJointValuesEigen();
    ARMARX_INFO << VAROUT(oldJV);
    Eigen::VectorXf cartesianVel(6);
    srand(123);

    for (int i = 0; i < 10000; ++i)
    {


        cartesianVel << rand() % 100, rand() % 100, rand() % 100, 0.01 * (rand() % 100), 0.01 * (rand() % 100), 0.01 * (rand() % 100);
        //        cartesianVel << 100,0,0;//, 0.01*(rand()%100), 0.01*(rand()%100), 0.01*(rand()%100);
        auto mode = VirtualRobot::IKSolver::CartesianSelection::All;
        Eigen::MatrixXf jacobi = h->ik->getJacobianMatrix(rns->getTCP(), mode);
        Eigen::MatrixXf inv = h->ik->computePseudoInverseJacobianMatrix(jacobi, h->ik->getJacobiRegularization(mode));
        int jointIndex = rand() % rns->getSize();
        jacobi.block(0, jointIndex, jacobi.rows(), 1) = Eigen::VectorXf::Zero(jacobi.rows());
        Eigen::MatrixXf inv2 = h->ik->computePseudoInverseJacobianMatrix(jacobi, h->ik->getJacobiRegularization(mode));

        //    ARMARX_INFO << "diff\n" << (inv-inv2);
        rns->setJointValues(oldJV);
        Eigen::Vector3f posBefore = h->_tcp->getPositionInRootFrame();

        float accuracy = 0.001f;

        Eigen::VectorXf jointVel = inv * cartesianVel;
        rns->setJointValues(oldJV + jointVel * accuracy);
        Eigen::VectorXf resultCartesianVel = ((h->_tcp->getPositionInRootFrame() - posBefore) / accuracy);



        Eigen::VectorXf jointVel2 = inv2 * cartesianVel;
        rns->setJointValues(oldJV + jointVel2 * accuracy);
        Eigen::VectorXf resultCartesianVel2 = ((h->_tcp->getPositionInRootFrame() - posBefore) / accuracy);

        Eigen::Vector3f diff = (resultCartesianVel - cartesianVel.head<3>());
        Eigen::Vector3f diff2 = (resultCartesianVel2 - cartesianVel.head<3>());

        if (!((diff - diff2).norm() < 0.5 || diff2.norm() < diff.norm()))
        {
            ARMARX_INFO << "Target cartesian velo:\n" << cartesianVel;
            ARMARX_INFO << "jacobi\n" << jacobi;
            ARMARX_INFO << "inv\n" << inv;
            ARMARX_INFO << "inv2\n" << inv2;
            ARMARX_INFO << "\n\nResulting joint cart velocity: " << resultCartesianVel << "\n jointVel:\n" << jointVel;
            ARMARX_INFO << "\n\nResulting joint cart velocity with joint limit avoidance: " << resultCartesianVel2 << "\n jointVel:\n" << jointVel2;

            ARMARX_INFO << "Diff:\n" << diff;
            ARMARX_INFO << "Diff2:\n" << diff2;
        }
        else
        {
            ARMARX_INFO << "Success for \n" << cartesianVel;
        }


        BOOST_REQUIRE((diff - diff2).norm() < 1 || diff2.norm() < diff.norm());

    }
}
