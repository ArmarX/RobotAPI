/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Sonja Marahrens (sonja dot marahrens)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/IK/DifferentialIK.h>
#include <VirtualRobot/Robot.h>
#include <Eigen/Dense>

namespace armarx
{
    class CartesianVelocityRamp;
    using CartesianVelocityRampPtr = std::shared_ptr<CartesianVelocityRamp>;

    class CartesianVelocityRamp
    {
    public:
        CartesianVelocityRamp();
        void setState(const Eigen::VectorXf& state, VirtualRobot::IKSolver::CartesianSelection mode);

        Eigen::VectorXf update(const Eigen::VectorXf& target, float dt);

        void setMaxPositionAcceleration(float maxPositionAcceleration);
        void setMaxOrientationAcceleration(float maxOrientationAcceleration);

        float getMaxPositionAcceleration();
        float getMaxOrientationAcceleration();

    private:
        float maxPositionAcceleration = 0;
        float maxOrientationAcceleration = 0;

        Eigen::VectorXf state = Eigen::VectorXf::Zero(6);
        VirtualRobot::IKSolver::CartesianSelection mode = VirtualRobot::IKSolver::CartesianSelection::All;


    };
}
