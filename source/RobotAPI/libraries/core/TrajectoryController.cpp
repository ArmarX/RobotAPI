/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "TrajectoryController.h"
#include <RobotAPI/libraries/core/math/LinearizeAngularTrajectory.h>
#include <RobotAPI/libraries/core/math/MathUtils.h>

namespace armarx
{

    TrajectoryController::TrajectoryController(const TrajectoryPtr& traj, float kp, float ki, float kd, bool threadSafe, float maxIntegral) :
        traj(traj)
    {
        std::vector<bool> limitless;
        for (auto ls : traj->getLimitless())
        {
            limitless.push_back(ls.enabled);
        }
        pid.reset(new MultiDimPIDController(kp, ki, kd, std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), threadSafe, limitless));
        pid->maxIntegral = maxIntegral;
        pid->preallocate(traj->dim());
        ARMARX_CHECK_EXPRESSION(traj);
        currentTimestamp = traj->begin()->getTimestamp();
        //for (size_t i = 0; i < traj->dim(); i++)
        //{
        //    PIDControllerPtr pid(new PIDController(kp, ki, kd, std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), traj->getLimitless().at(i).enabled));
        //    pids.push_back(pid);
        //}
        positions.resize(traj->dim(), 1);
        veloctities.resize(traj->dim(), 1);
        currentError.resize(traj->dim(), 1);
    }

    const Eigen::VectorXf& TrajectoryController::update(double deltaT, const Eigen::VectorXf& currentPosition)
    {
        ARMARX_CHECK_EXPRESSION(pid);
        ARMARX_CHECK_EXPRESSION(traj);
        ARMARX_CHECK_EXPRESSION(traj->size() > 0);
        ARMARX_CHECK_EQUAL(static_cast<std::size_t>(currentPosition.rows()), traj->dim());
        size_t dim = traj->dim();
        currentTimestamp  = currentTimestamp + deltaT;
        const Trajectory& traj = *this->traj;
        if (currentTimestamp < 0.0)
        {
            currentTimestamp = 0.0;
        }
        if (currentTimestamp <= traj.rbegin()->getTimestamp())
        {
            for (size_t i = 0; i < dim; ++i)
            {

                positions(i) = traj.getState(currentTimestamp, i, 0);
                veloctities(i) = (std::signbit(deltaT) ? -1.0 : 1.0) * traj.getState(currentTimestamp, i, 1);
                //pids.at(i)->update(std::abs(deltaT), currentPosition(i), positions(i));
                //veloctities(i) += pids.at(i)->getControlValue();
            }
        }
        else // hold position in the end
        {
            for (size_t i = 0; i < dim; ++i)
            {
                positions(i) = traj.rbegin()->getPosition(i);
                veloctities(i) = 0;
            }
        }
        for (size_t i = 0; i < dim; ++i)
        {
            if (pid->limitless.size() > i && pid->limitless.at(i))
            {
                currentError(i) = math::MathUtils::AngleDelta(currentPosition(i), positions(i));
            }
            else
            {
                currentError(i) = positions(i) - currentPosition(i);
            }

        }

        pid->update(std::abs(deltaT), currentPosition, positions);
        veloctities += pid->getControlValue();
        return veloctities;
    }

    /*
    const MultiDimPIDControllerPtr& TrajectoryController::getPid() const
    {
        return pid;
    }

    void TrajectoryController::setPid(const MultiDimPIDControllerPtr& value)
    {
        pid = value;
    }*/

    double TrajectoryController::getCurrentTimestamp() const
    {
        return currentTimestamp;
    }

    const TrajectoryPtr& TrajectoryController::getTraj() const
    {
        return traj;
    }

    void TrajectoryController::UnfoldLimitlessJointPositions(TrajectoryPtr traj)
    {
        if (traj->size() == 0)
        {
            return;
        }
        auto dim = traj->dim();
        Ice::DoubleSeq prevPos = traj->begin()->getPositions();
        std::vector<math::LinearizeAngularTrajectory> linTraj;
        for (size_t i = 0; i < dim; ++i)
        {
            linTraj.push_back(math::LinearizeAngularTrajectory(prevPos.at(i)));
        }
        auto limitlessStates = traj->getLimitless();
        for (auto& state : *traj)
        {
            for (size_t i = 0; i < dim; ++i)
            {
                //if (!limitlessStates.at(i).enabled)
                //{
                //    continue;
                //}
                //double pos = state.getPosition(i);
                //double newPos = prevPos.at(i) + math::MathUtils::angleModPI(pos - prevPos.at(i));
                //state.getData().at(i)->at(0) = newPos;
                //prevPos.at(i) = newPos;

                if (limitlessStates.at(i).enabled)
                {
                    state.getData().at(i)->at(0) = linTraj.at(i).update(state.getData().at(i)->at(0));
                }

            }
        }
    }

    void TrajectoryController::FoldLimitlessJointPositions(TrajectoryPtr traj)
    {
        if (traj->size() == 0)
        {
            return;
        }
        auto dim = traj->dim();
        auto limitlessStates = traj->getLimitless();
        ARMARX_CHECK_EQUAL(limitlessStates.size(), dim);
        for (auto& state : *traj)
        {
            for (size_t i = 0; i < dim; ++i)
            {
                if (!limitlessStates.at(i).enabled)
                {
                    continue;
                }
                double pos = state.getPosition(i);
                double center = (limitlessStates.at(i).limitHi + limitlessStates.at(i).limitLo) * 0.5;
                double newPos = math::MathUtils::angleModX(pos, center);
                state.getData().at(i)->at(0) = newPos;
            }
        }
    }

    const Eigen::VectorXf& TrajectoryController::getCurrentError() const
    {
        return currentError;
    }

    const Eigen::VectorXf& TrajectoryController::getPositions() const
    {
        return positions;
    }


} // namespace armarx
