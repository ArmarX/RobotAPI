/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Sonja Marahrens (sonja dot marahrens)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "CartesianVelocityController.h"
#include "CartesianVelocityRamp.h"
#include "JointVelocityRamp.h"

#include <VirtualRobot/RobotNodeSet.h>

#include <VirtualRobot/IK/DifferentialIK.h>
#include <VirtualRobot/Robot.h>
#include <Eigen/Dense>


namespace armarx
{
    class CartesianVelocityControllerWithRamp;
    using CartesianVelocityControllerWithRampPtr = std::shared_ptr<CartesianVelocityControllerWithRamp>;

    class CartesianVelocityControllerWithRamp
    {
    public:
        CartesianVelocityControllerWithRamp(const VirtualRobot::RobotNodeSetPtr& rns, const Eigen::VectorXf& currentJointVelocity, VirtualRobot::IKSolver::CartesianSelection mode,
                                            float maxPositionAcceleration, float maxOrientationAcceleration, float maxNullspaceAcceleration,
                                            const VirtualRobot::RobotNodePtr& tcp = nullptr);

        CartesianVelocityControllerWithRamp(CartesianVelocityControllerWithRamp&&) = default;
        CartesianVelocityControllerWithRamp& operator=(CartesianVelocityControllerWithRamp&&) = default;

        [[deprecated("computed null space velocity does not match pseudo inverse svd method in simox. never use this function.")]]
        void setCurrentJointVelocity(const Eigen::Ref<const Eigen::VectorXf>& currentJointVelocity);

        void switchMode(const Eigen::VectorXf& currentJointVelocity, VirtualRobot::IKSolver::CartesianSelection mode);
        VirtualRobot::IKSolver::CartesianSelection getMode();

        Eigen::VectorXf calculate(const Eigen::VectorXf& cartesianVel, float jointLimitAvoidanceScale, float dt);
        Eigen::VectorXf calculate(const Eigen::VectorXf& cartesianVel, const Eigen::VectorXf& nullspaceVel, float dt);

        void setMaxAccelerations(float maxPositionAcceleration, float maxOrientationAcceleration, float maxNullspaceAcceleration);

        float getMaxOrientationAcceleration();
        float getMaxPositionAcceleration();
        float getMaxNullspaceAcceleration();


        CartesianVelocityController controller;
    private:
        VirtualRobot::IKSolver::CartesianSelection mode;
        CartesianVelocityRamp cartesianVelocityRamp;
        JointVelocityRamp nullSpaceVelocityRamp;
    };
}
