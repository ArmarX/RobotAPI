/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <VirtualRobot/Robot.h>

#include <mutex>

namespace armarx
{

    /** This class holds a pool of local VirtualRobots for multi threaded applications
     *  that can be requested by the user.
     *  When calling getRobot() it is guaranteed that that robot is not requested by any other user/thread.
     *  When the return shared ptr goes out of the, the robot is available again.
     *
     */
    class RobotPool
    {
    public:
        RobotPool(VirtualRobot::RobotPtr robot, size_t defaultSize = 1);
        /**
         * @brief getRobot
         * @param inflation inflation of collision model in mm
         * @return
         */
        VirtualRobot::RobotPtr getRobot(int inflation = 0);
        size_t getPoolSize() const;
        size_t getRobotsInUseCount() const;
        /**
         * @brief Removes unused robots from the pool.
         * @return number of cleaned up robots
         */
        size_t clean();
    private:
        std::map<int, std::vector<VirtualRobot::RobotPtr>> robots;
        VirtualRobot::RobotPtr baseRobot;
        mutable std::mutex mutex;
    };
    using RobotPoolPtr = std::shared_ptr<RobotPool>;
}


