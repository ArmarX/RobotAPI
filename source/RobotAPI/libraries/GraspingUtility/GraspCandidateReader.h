#pragma once

#include <vector>

#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client.h>
#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>


namespace armarx::armem
{

    class GraspCandidateReader
    {
    public:
        GraspCandidateReader(armem::client::MemoryNameSystem& memoryNameSystem);

        void connect(bool use = true);

        grasping::GraspCandidatePtr queryGraspCandidateInstanceByID(armem::MemoryID const& id) const;
        grasping::GraspCandidateDict queryGraspCandidateInstancesByID(std::vector<armem::MemoryID> const& ids) const;

        grasping::BimanualGraspCandidatePtr queryBimanualGraspCandidateInstanceByID(armem::MemoryID const& id) const;

        grasping::GraspCandidateDict queryLatestGraspCandidateEntity(
                std::string const& provider, std::string const& entity) const;

        std::map<std::string, grasping::BimanualGraspCandidatePtr> queryLatestBimanualGraspCandidateEntity(
                std::string const& provider, std::string const& entity) const;

        grasping::GraspCandidateDict queryLatestGraspCandidates(
                std::string const& provider = "") const;

        std::map<std::string, grasping::BimanualGraspCandidatePtr> queryLatestBimanualGraspCandidates(
                std::string const& provider = "") const;



        void registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def);


    private:

        grasping::GraspCandidateDict getGraspCandidatesFromResultSet(
                armem::client::QueryResult const& qResult) const;

        std::map<std::string, grasping::BimanualGraspCandidatePtr> getBimanualGraspCandidatesFromResultSet(
                armem::client::QueryResult const& qResult) const;

        armem::client::Reader memoryReader;

        // Properties
        struct Properties
        {
            std::string memoryName = "Grasp";
            std::string graspCandidateMemoryName = "GraspCandidate";
            std::string bimanualGraspCandidateMemoryName = "BimanualGraspCandidate";
        } properties;


        const std::string propertyPrefix = "mem.grasping.";

        armem::client::MemoryNameSystem& memoryNameSystem;

    };

}
