#pragma once

#include <RobotAPI/components/ArViz/Client/Layer.h>
#include <RobotAPI/components/ArViz/Client/Client.h>

#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>


namespace armarx::grasping
{
    class GraspCandidateVisu
    {
    public:
        GraspCandidateVisu();


        void visualize(
                const grasping::GraspCandidateDict& candidates,
                viz::Client& arviz);

        void visualize(
                const grasping::GraspCandidateDict& candidates,
                viz::Layer& layerReachable,
                viz::Layer& layerNonReachable);

        void visualize(
                const std::string& id,
                const grasping::GraspCandidate& candidate,
                viz::Layer& layerReachable,
                viz::Layer& layerNonReachable);

        viz::Robot visualize(
                const std::string& name,
                const grasping::GraspCandidate& candidate);
        viz::Robot visualize(
                const std::string& name,
                const grasping::GraspCandidate& candidate,
                int alpha);


    public:

        int alpha_default = 255;
        std::map<std::string, int> alphasByKey = {};


    };
}
