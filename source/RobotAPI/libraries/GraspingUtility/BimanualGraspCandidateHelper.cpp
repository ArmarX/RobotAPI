/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "BimanualGraspCandidateHelper.h"
#include <VirtualRobot/math/Helpers.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

using namespace armarx;


BimanualGraspCandidateHelper::BimanualGraspCandidateHelper(const grasping::BimanualGraspCandidatePtr& candidate, VirtualRobot::RobotPtr robot)
    : candidate(candidate), robot(robot)
{
    ARMARX_CHECK_NOT_NULL(candidate);
    ARMARX_CHECK_NOT_NULL(robot);
}

Eigen::Matrix4f BimanualGraspCandidateHelper::Side::getGraspPoseInRobotRoot() const
{
    const Eigen::Matrix4f curRobotPose = helper->robot->getGlobalPose();
    const Eigen::Matrix4f graspPose = curRobotPose.inverse() * getGraspPoseInGlobal();
    return graspPose;
}

Eigen::Matrix4f BimanualGraspCandidateHelper::Side::getPrePoseInRobotRoot(float approachDistance) const
{
    return math::Helpers::TranslatePose(getGraspPoseInRobotRoot(), getApproachVector() * approachDistance);
}

Eigen::Matrix3f BimanualGraspCandidateHelper::Side::getGraspOrientationInRobotRoot() const
{
    return getGraspPoseInRobotRoot().topLeftCorner<3, 3>();
}

Eigen::Vector3f BimanualGraspCandidateHelper::Side::getGraspPositionInRobotRoot() const
{
    return math::Helpers::GetPosition(getGraspPoseInRobotRoot());
}

Eigen::Matrix4f BimanualGraspCandidateHelper::Side::getGraspPoseInGlobal() const
{
    const Eigen::Matrix4f oldRobotPose = helper->defrost(helper->candidate->robotPose);
    return oldRobotPose * graspPose;
}
Eigen::Matrix3f BimanualGraspCandidateHelper::Side::getGraspOrientationInGlobal() const
{
    return getGraspPoseInGlobal().topLeftCorner<3, 3>();
}
Eigen::Vector3f BimanualGraspCandidateHelper::Side::getGraspPositionInGlobal() const
{
    return math::Helpers::GetPosition(getGraspPoseInGlobal());
}

Eigen::Vector3f BimanualGraspCandidateHelper::Side::getApproachVector() const
{
    return approachVector;
}
