#pragma once

#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/client.h>
#include <mutex>

#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>
#include <ArmarXCore/core/application/properties/forward_declarations.h>

namespace armarx::armem
{
    class GraspCandidateWriter
    {
    public:
        GraspCandidateWriter(armem::client::MemoryNameSystem& memoryNameSystem);

        void connect();

        void registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def);

        bool commitGraspCandidate(const armarx::grasping::GraspCandidate& candidate,
                                  const armem::Time& timestamp, const std::string& provider);
        bool commitBimanualGraspCandidate(const armarx::grasping::BimanualGraspCandidate& candidate,
                                          const armem::Time& timestamp, const std::string& provider);

        bool commitGraspCandidateSeq(const armarx::grasping::GraspCandidateSeq& candidates,
                                     const armem::Time& timestamp, const std::string& provider);
        bool commitBimanualGraspCandidateSeq(const armarx::grasping::BimanualGraspCandidateSeq& candidates,
                                             const armem::Time& timestamp, const std::string& provider);



    private:

        bool commitToMemory(const std::vector<armarx::aron::data::DictPtr>& instances,
                            const std::string& providerName, const std::string& entityName, const armem::Time& timestamp,
                            const std::string& coreMemoryName);

        armem::client::MemoryNameSystem& memoryNameSystem;

        armem::client::Writer memoryWriter;

        struct Properties
        {
            std::string memoryName         = "Grasp";
            std::string graspMemoryName  = "GraspCandidate";
            std::string bimanualGraspMemoryName = "BimanualGraspCandidate";
        } properties;


        std::mutex memoryWriterMutex;

        const std::string propertyPrefix = "mem.grasping.";

    };


}
