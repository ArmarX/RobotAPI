#pragma once

////plugins
//#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>
//#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/RemoteGuiComponentPlugin.h>
//#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>
//#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
//#include <RobotAPI/libraries/RobotAPIComponentPlugins/GraspCandidateObserverComponentPlugin.h>

////other
//#include <ArmarXCore/util/CPPUtility/ConfigIntrospection/create_macro.h>
//#include <RobotAPI/interface/observers/GraspCandidateObserverInterface.h>

namespace armarx
{
    //    class GraspCandidateCollisionFilter :
    //        virtual public visionx::PointCloudProcessor,
    //        virtual public armarx::RemoteGuiComponentPluginUser,
    //        virtual public armarx::DebugObserverComponentPluginUser,
    //        virtual public armarx::ArVizComponentPluginUser,
    //        virtual public armarx::RobotStateComponentPluginUser,
    //        virtual public armarx::GraspCandidateObserverComponentPluginUser,
    //        public arches::time_util<std::chrono::high_resolution_clock>
    //    {
    //    public:
    //        using clock_t      = std::chrono::high_resolution_clock;
    //        using time_point_t = typename clock_t::time_point;
    //        using config_t     = arches::cfg::GCColFilter::config;

    //        std::string getDefaultName() const override;

    //        void requestService(const std::string& providerName, Ice::Int relativeTimeoutMS, const Ice::Current&);
    //        armarx::grasping::ProviderInfoPtr getProviderInfo() const;
    //    protected:
    //        void onInitPointCloudProcessor()                           override;
    //        void onConnectPointCloudProcessor()                        override;
    //        void onDisconnectPointCloudProcessor()                     override {}
    //        void onExitPointCloudProcessor()                           override {}
    //        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
    //    private:
    //        void process() override;
    //        template<class PointT>
    //        void process();


    //    private:
    //        struct provider_data
    //        {
    //            std::string                             ref_frame_name_default;
    //            std::string                             ref_frame_name_used;
    //            std::string                             name;
    //            visionx::PointCloudProviderInfo         info;
    //            visionx::PointCloudProviderInterfacePrx proxy;
    //        };
    //        provider_data                                      _provider;
    //        armarx::WriteBufferedTripleBuffer<config_t>        _cfg_buf;
    //        VirtualRobot::RobotPtr                             _robot;
    //        //        grasp_candidate_collision_checker                  _gc_col_checker;
    //        std::string                                        _upstream_gc_provider_csv;
    //        std::size_t                                        _old_num_visualized;
    //        armarx::grasping::GraspCandidatesTopicInterfacePrx _gc_topic;
    //        armarx::RequestableServiceListenerInterfacePrx     _service_request_topic;
    //    };
}
