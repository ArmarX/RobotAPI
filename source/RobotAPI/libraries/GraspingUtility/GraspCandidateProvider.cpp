

//#include <ArmarXCore/core/application/properties/PluginAll.h>
//#include <ArmarXGui/libraries/RemoteGui/ConfigIntrospection.h>


#include "GraspCandidateProvider.h"

////boilerplate (properties / cycle)
//namespace arches
//{
//    std::string GraspCandidateCollisionFilter::getDefaultName() const
//    {
//        return "GraspCandidateCollisionFilter";
//    }

//    armarx::PropertyDefinitionsPtr
//    GraspCandidateCollisionFilter::createPropertyDefinitions()
//    {
//        armarx::PropertyDefinitionsPtr ptr =
//            new visionx::PointCloudProcessorPropertyDefinitions(getConfigIdentifier());
//        ptr->optional(_cfg_buf.getWriteBuffer(), "", "");
//        ptr->optional(_provider.ref_frame_name_default,
//                      "ReferenceFrame",
//                      "ReferenceFrame (if not set, autodetection is performed)");
//        ptr->optional(_upstream_gc_provider_csv,
//                      "UpstreamGraspCandidateProviderCSV",
//                      "Upstream Provider(s) (if empty all)");
//        ptr->topic(_gc_topic,
//                   "GraspCandidatesTopicName",
//                   "GraspCandidatesTopic",
//                   "Name of the Grasp Candidate Topic");
//        ptr->topic(_service_request_topic,
//                   "ServiceRequestsTopicName",
//                   "ServiceRequests",
//                   "Name of the ServiceRequestsTopic");
//        return ptr;
//    }

//    void GraspCandidateCollisionFilter::onInitPointCloudProcessor()
//    {
//        ARMARX_TRACE;
//        ARMARX_INFO << "init...";
//        setDebugObserverBatchModeEnabled(true);
//        getProperty(_provider.name, "ProviderName");
//        ARMARX_INFO << "init...done!";
//    }
//    void GraspCandidateCollisionFilter::onConnectPointCloudProcessor()
//    {
//        ARMARX_TRACE;
//        ARMARX_INFO << "connect...";
//        ARMARX_INFO << "prov frame";
//        {
//            ARMARX_TRACE;
//            _provider.info = getPointCloudProvider(_provider.name, true);
//            _provider.proxy = _provider.info.proxy;
//            ARMARX_CHECK_NOT_NULL(_provider.proxy) << VAROUT(_provider.name);
//            _provider.ref_frame_name_used =
//                _provider.ref_frame_name_default.empty() ?
//                getPointCloudFrame(_provider.name, _provider.ref_frame_name_default) :
//                _provider.ref_frame_name_default;
//            ARMARX_CHECK_EXPRESSION(!_provider.ref_frame_name_used.empty())
//                    << VAROUT(_provider.name) << " default = "
//                    << VAROUT(_provider.ref_frame_name_default);
//            ARMARX_INFO << "Provider '" << _provider.name
//                        << "' has ref frame '" << _provider.ref_frame_name_used << "'";
//        }
//        ARMARX_INFO << "robot";
//        _robot = addRobot("grounding", VirtualRobot::RobotIO::eStructure);
//        ARMARX_INFO << "gui";
//        createOrUpdateRemoteGuiTab(_cfg_buf);
//        ARMARX_INFO << "configure gc observer";
//        getGraspCandidateObserverComponentPlugin().setUpstreamGraspCandidateProvidersFromCSV(_upstream_gc_provider_csv);
//        ARMARX_INFO << "configure colision checker!";
//        //        _gc_col_checker.reset(getRobotNameHelper(), _robot);
//        ARMARX_INFO << "start reporting gc config!";
//        startPeriodicTask("reportConfigTask", [&]()
//        {
//            _gc_topic->reportProviderInfo(getName(), getProviderInfo());
//        }, 1000, false);
//        ARMARX_INFO << "connect...done!";
//    }

//    void GraspCandidateCollisionFilter::requestService(
//        const std::string& providerName,
//        Ice::Int relativeTimeoutMS,
//        const Ice::Current&)
//    {
//        if (providerName == getName() || providerName == "AllGraspProviders")
//        {
//            ///TODO do not ignore this!
//            _service_request_topic->requestService(getProperty<std::string>("ServiceRequestsTopicName").getValue(), relativeTimeoutMS);
//        }
//    }
//    armarx::grasping::ProviderInfoPtr GraspCandidateCollisionFilter::getProviderInfo() const
//    {
//        armarx::grasping::ProviderInfoPtr info = new armarx::grasping::ProviderInfo();
//        info->objectType = armarx::grasping::AnyObject;
//        return info;
//    }
//}


