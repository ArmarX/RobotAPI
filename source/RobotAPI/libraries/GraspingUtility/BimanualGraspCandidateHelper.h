/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>
#include <RobotAPI/libraries/core/Pose.h>

#include <VirtualRobot/Robot.h>

#include <memory>


namespace armarx
{
    typedef std::shared_ptr<class BimanualGraspCandidateHelper> BimanualGraspCandidateHelperPtr;

    class BimanualGraspCandidateHelper
    {
    public:
        BimanualGraspCandidateHelper(const grasping::BimanualGraspCandidatePtr& candidate, VirtualRobot::RobotPtr robot);
        BimanualGraspCandidateHelper(const BimanualGraspCandidateHelper&) = default;
        BimanualGraspCandidateHelper(BimanualGraspCandidateHelper&&) = default;
        BimanualGraspCandidateHelper& operator=(const BimanualGraspCandidateHelper&) = default;
        BimanualGraspCandidateHelper& operator=(BimanualGraspCandidateHelper&&) = default;

        struct Side
        {
            Eigen::Matrix4f getGraspPoseInRobotRoot() const;
            Eigen::Matrix4f getPrePoseInRobotRoot(float approachDistance) const;
            Eigen::Matrix3f getGraspOrientationInRobotRoot() const;
            Eigen::Vector3f getGraspPositionInRobotRoot() const;

            Eigen::Matrix4f getGraspPoseInGlobal() const;
            Eigen::Matrix3f getGraspOrientationInGlobal() const;
            Eigen::Vector3f getGraspPositionInGlobal() const;

            Eigen::Vector3f getApproachVector() const;

        private:
            friend class BimanualGraspCandidateHelper;
            const BimanualGraspCandidateHelper* helper;
            Eigen::Matrix4f graspPose;
            Eigen::Vector3f approachVector;
            Eigen::Vector3f inwardsVector;
        };

        Side left() const
        {
            Side s;
            s.helper         = this;
            s.graspPose      = defrost(candidate->graspPoseLeft);
            s.approachVector = defrost(candidate->approachVectorLeft);
            s.inwardsVector  = defrost(candidate->inwardsVectorLeft);
            return s;
        }
        Side right() const
        {
            Side s;
            s.helper         = this;
            s.graspPose      = defrost(candidate->graspPoseRight);
            s.approachVector = defrost(candidate->approachVectorRight);
            s.inwardsVector  = defrost(candidate->inwardsVectorRight);
            return s;
        }

        const grasping::BimanualGraspCandidatePtr getGraspCandidate() const
        {
            return candidate;
        }

        Eigen::Vector3f defrost(const Vector3BasePtr& base) const
        {
            return Vector3Ptr::dynamicCast(base)->toEigen();
        }
        Eigen::Matrix4f defrost(const PoseBasePtr& base) const
        {
            return PosePtr::dynamicCast(base)->toEigen();
        }

    private:
        grasping::BimanualGraspCandidatePtr candidate;
        VirtualRobot::RobotPtr robot;
    };
}
