#pragma once

#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>
#include <RobotAPI/libraries/GraspingUtility/aron/GraspCandidate.aron.generated.h>
#include <RobotAPI/libraries/ArmarXObjects/aron/ObjectType.aron.generated.h>

namespace armarx
{
    namespace grasping
    {
        void fromAron(const arondto::ApproachType& dto, ApproachType& bo);
        void toAron(arondto::ApproachType& dto, const ApproachType& bo);

        void fromAron(const arondto::ApertureType& dto, ApertureType& bo);
        void toAron(arondto::ApertureType& dto, const ApertureType& bo);

        void fromAron(const arondto::BoundingBox& dto, BoundingBox& bo);
        void toAron(arondto::BoundingBox& dto, const BoundingBox& bo);

        void fromAron(const arondto::GraspCandidateSourceInfo& dto, GraspCandidateSourceInfo& bo);
        void toAron(arondto::GraspCandidateSourceInfo& dto, const GraspCandidateSourceInfo& bo);

        void fromAron(const arondto::GraspCandidateExecutionHints& dto, GraspCandidateExecutionHints& bo);
        void toAron(arondto::GraspCandidateExecutionHints& dto, const GraspCandidateExecutionHints& bo);

        void fromAron(const arondto::GraspCandidateReachabilityInfo& dto, GraspCandidateReachabilityInfo& bo);
        void toAron(arondto::GraspCandidateReachabilityInfo& dto, const GraspCandidateReachabilityInfo& bo);

        void fromAron(const arondto::GraspCandidate& dto, GraspCandidate& bo);
        void toAron(arondto::GraspCandidate& dto, const GraspCandidate& bo);

        void fromAron(const arondto::BimanualGraspCandidate& dto, BimanualGraspCandidate& bo);
        void toAron(arondto::BimanualGraspCandidate& dto, const BimanualGraspCandidate& bo);
    }
}
