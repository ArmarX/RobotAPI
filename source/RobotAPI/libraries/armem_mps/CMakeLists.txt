set(LIB_NAME       armem_mps)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")
find_package(DMP QUIET) # needs to be changed to new MP package

armarx_build_if(DMP_FOUND "DMP not available")

armarx_add_library(
    LIBS     
        ArmarXCoreInterfaces
        ArmarXCore
        ArmarXCoreObservers

        RobotAPI::Core
        RobotAPI::armem
        RobotAPI::PriorKnowledge::Motions
        RobotAPI::armem_motions

        VirtualRobot
        ${DMP_LIBRARIES}

    SOURCES  
        aron_conversions.cpp
        #traj_conversions.cpp

    HEADERS  
        aron_conversions.h
        #traj_conversions.h
)


add_library(RobotAPI::armem_mps ALIAS armem_mps)

add_subdirectory(server)
