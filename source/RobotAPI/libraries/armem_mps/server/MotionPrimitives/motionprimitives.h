#ifndef MOTIONPRIMITIVES_H
#define MOTIONPRIMITIVES_H

#include <filesystem>
#include <iostream>
#include <fstream>
#include <optional>

// ArmarX
#include <RobotAPI/libraries/aron/common/aron/trajectory.aron.generated.h>

namespace armarx::armem::server::motions::mps::segment::util
{

    std::optional<armarx::arondto::Trajectory> createFromFile(const std::filesystem::path& pathToInfoJson, bool taskspace);

}
#endif // MOTIONPRIMITIVES_H
