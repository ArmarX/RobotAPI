#pragma once

// STD/STL
#include <mutex>
#include <string>

// BaseClass
#include <RobotAPI/libraries/armem/server/segment/SpecializedSegment.h>

// ArmarX
#include <RobotAPI/libraries/aron/common/aron/trajectory.aron.generated.h>

namespace armarx::armem::server::motions::mps::segment
{
    class MPSegment : public armem::server::segment::SpecializedProviderSegment
    {
        using Base = armem::server::segment::SpecializedProviderSegment;

    public:
        MPSegment(armem::server::MemoryToIceAdapter& iceMemory);

        virtual void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "") override;
        virtual void init() override;
        virtual void onConnect();

    private:
        int loadByMotionFinder(const std::string&);
        void loadSingleMotionFinder(const std::string&, const std::string &entityName, bool taskspace);

    private:
        struct Properties
        {
            std::string motionsPackage = "PriorKnowledgeData";
            bool loadFromMotionsPackage = true;
        };
        Properties properties;
    };
}
