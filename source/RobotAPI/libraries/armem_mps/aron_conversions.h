#pragma once

#include <ArmarXCore/interface/core/Profiler.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>

#include <RobotAPI/libraries/aron/common/aron/trajectory.aron.generated.h>

#include <dmp/representation/trajectory.h>
//#include <dmp

namespace armarx
{

    void fromAron(const armarx::arondto::Trajectory& dto, DMP::SampledTrajectoryV2& bo, bool taskspace);
    void toAron(armarx::arondto::Trajectory& dto, const DMP::SampledTrajectoryV2& bo_taskspace, const DMP::SampledTrajectoryV2& bo_jointspace, const std::string name);

}
