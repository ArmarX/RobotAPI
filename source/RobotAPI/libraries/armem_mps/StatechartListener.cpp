#include "StatechartListener.h"


namespace armarx
{
    void StatechartListener::setName(const std::string& name)
    {
        armarx::Component::setName(name);
    }

    void StatechartListener::setTopicName(const std::string& name)
    {
        this->topicName = name;
    }

    std::string StatechartListener::getTopicName() const
    {
        return topicName;
    }

    StatechartListener::~StatechartListener() = default;

    std::string StatechartListener::getDefaultName() const
    {
        return "StatechartListener";
    }

    void StatechartListener::onInitComponent()
    {
        ARMARX_INFO << getName() << "::" << __FUNCTION__ << "()";
        usingTopic(topicName);
    }
    void StatechartListener::onConnectComponent()
    {
        ARMARX_INFO << getName() << "::" << __FUNCTION__ << "()";
    }

    void StatechartListener::registerCallback(const StatechartListener::Callback& callback)
    {
        callbacks.push_back(callback);
    }

    void StatechartListener::publish(const std::vector<Transition>& message)
    {
        for (Callback& callback : callbacks)
        {
            callback(message, *this);
        }
    }

    void
    StatechartListener::reportStatechartTransitionWithParameters(const ProfilerStatechartTransitionWithParameters& transition,
            const Ice::Current&)
    {
        publish({transition});
    }

    void StatechartListener::reportStatechartTransitionWithParametersList(
        const ProfilerStatechartTransitionWithParametersList& transitions, const Ice::Current&)
    {
        publish(transitions);
    }
}
