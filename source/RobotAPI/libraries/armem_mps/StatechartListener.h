#pragma once


#include <ArmarXCore/core/Component.h>

#include <ArmarXCore/interface/core/Profiler.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>


namespace armarx
{
    class StatechartListener :
        virtual public armarx::Component
        , virtual public armarx::ProfilerListener
    {
    public:
        using Transition = armarx::ProfilerStatechartTransitionWithParameters;
        using Callback = std::function<void(const std::vector<StatechartListener::Transition>& transitions, StatechartListener& source)>;

    public:
        ~StatechartListener() override;

        void setTopicName(const std::string& topicName);
        std::string getTopicName() const;

        void setName(const std::string& name);
        void registerCallback(const Callback& callback);

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;

    protected:
        void onInitComponent() override;
        void onConnectComponent() override;

        // ProfilerListener interface
    public:
        void reportStatechartTransitionWithParameters(const ProfilerStatechartTransitionWithParameters&, const Ice::Current&) override;
        void reportStatechartTransitionWithParametersList(const ProfilerStatechartTransitionWithParametersList&, const Ice::Current&) override;

        void reportNetworkTraffic(const std::string&, const std::string&, Ice::Int, Ice::Int, const Ice::Current&) override {}
        void reportEvent(const ProfilerEvent&, const Ice::Current&) override {}
        void reportStatechartTransition(const ProfilerStatechartTransition& event, const Ice::Current&) override {}
        void reportStatechartInputParameters(const ProfilerStatechartParameters& event, const Ice::Current&) override {}
        void reportStatechartLocalParameters(const ProfilerStatechartParameters& event, const Ice::Current&) override {}
        void reportStatechartOutputParameters(const ProfilerStatechartParameters&, const Ice::Current&) override {}
        void reportProcessCpuUsage(const ProfilerProcessCpuUsage&, const Ice::Current&) override {}
        void reportProcessMemoryUsage(const ProfilerProcessMemoryUsage&, const Ice::Current&) override {}

        void reportEventList(const ProfilerEventList& events, const Ice::Current&) override {}
        void reportStatechartTransitionList(const ProfilerStatechartTransitionList&, const Ice::Current&) override {}
        void reportStatechartInputParametersList(const ProfilerStatechartParametersList& data, const Ice::Current&) override {}
        void reportStatechartLocalParametersList(const ProfilerStatechartParametersList&, const Ice::Current&) override {}
        void reportStatechartOutputParametersList(const ProfilerStatechartParametersList&, const Ice::Current&) override {}
        void reportProcessCpuUsageList(const ProfilerProcessCpuUsageList&, const Ice::Current&) override {}
        void reportProcessMemoryUsageList(const ProfilerProcessMemoryUsageList&, const Ice::Current&) override {}


    private:
        std::string topicName;

        std::vector<Callback> callbacks;
        void publish(const std::vector<Transition>& message);
    };
}
