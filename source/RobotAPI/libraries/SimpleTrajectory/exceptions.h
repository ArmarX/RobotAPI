#pragma once

#include <sstream>
#include <stdexcept>

#include "VariantValue.h" // for TrackID


namespace armarx::trajectory::error
{


    struct TrajectoryException : public std::logic_error
    {
        TrajectoryException(const std::string& msg);
    };


    struct InterpolateDifferentTypesError : public TrajectoryException
    {
        InterpolateDifferentTypesError();
    };


    struct NoTrackWithID : public TrajectoryException
    {
        NoTrackWithID(const TrackID& id);
        static std::string makeMsg(const TrackID& id);
    };


    struct EmptyTrack : public TrajectoryException
    {
        EmptyTrack(const TrackID& id);
        static std::string makeMsg(const TrackID& id);
    };


    struct WrongValueTypeInKeyframe : public TrajectoryException
    {
        WrongValueTypeInKeyframe(const TrackID& trackID, int typeIndex, int expectedTypeIndex);
    };



}
