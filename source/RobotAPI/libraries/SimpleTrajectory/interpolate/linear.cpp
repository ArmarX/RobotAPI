#include "linear.h"


namespace armarx::trajectory
{

    template <>
    VariantValue interpolate::linear<VariantValue>(float t, const VariantValue& lhs, const VariantValue& rhs)
    {
        // dont use std::get
        return std::visit(Linear {t}, lhs, rhs);
    }

}
