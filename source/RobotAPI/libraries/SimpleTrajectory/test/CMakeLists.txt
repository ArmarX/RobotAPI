
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore ${LIB_NAME})

armarx_add_test(${LIB_NAME}TrajectoryTest TrajectoryTest.cpp "${LIBS}")
