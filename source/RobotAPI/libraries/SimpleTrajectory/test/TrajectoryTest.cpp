
#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::RobotAPISimpleTrajectory

#define ARMARX_BOOST_TEST

#include <iostream>

#include <RobotAPI/Test.h>
#include "../Trajectory.h"


using namespace armarx::trajectory;

namespace std
{
    std::ostream& operator<<(std::ostream& os, const std::type_info& rhs)
    {
        os << "TypeInfo (" << rhs.name() << ", " << rhs.hash_code() << ")";
        return os;
    }
}


struct Fixture
{
    Trajectory trajectory;

    float float_;
    Eigen::Vector3f vec;
    Eigen::MatrixXf mat;
    Eigen::Quaternionf quat;

    float ftol = 1e-9f;
};


BOOST_FIXTURE_TEST_SUITE(TrajectoryTest, Fixture)


BOOST_AUTO_TEST_CASE(testNoTrackWithID)
{
    for (auto* id :
         { "id1", "id2"
         })
    {
        BOOST_CHECK_THROW(trajectory.addKeyframe(id, 0, 0.f), error::NoTrackWithID);
        BOOST_CHECK_THROW(trajectory[id].addKeyframe(0, 0.f), error::NoTrackWithID);
        BOOST_CHECK_THROW(trajectory[id][0] = 0.f, error::NoTrackWithID);
    }

    trajectory.addTrack("id1", updateValue(float_));

    BOOST_CHECK_NO_THROW(trajectory.addKeyframe("id1", 0, 0.f));
    BOOST_CHECK_NO_THROW(trajectory["id1"].addKeyframe(1, 0.f));
    BOOST_CHECK_NO_THROW(trajectory["id1"][2] = 0.f);

    BOOST_CHECK_THROW(trajectory.addKeyframe("id2", 0, 0.f), error::NoTrackWithID);
    BOOST_CHECK_THROW(trajectory["id2"].addKeyframe(0, 0.f), error::NoTrackWithID);
    BOOST_CHECK_THROW(trajectory["id2"][0] = 0.f, error::NoTrackWithID);
}


BOOST_AUTO_TEST_CASE(testEmptyTrack)
{
    trajectory.addTrack("float", updateValue(float_));

    BOOST_CHECK(trajectory["float"].empty());

    BOOST_CHECK_THROW(trajectory["float"].update(0), error::EmptyTrack);
    BOOST_CHECK_THROW(trajectory.update(0), error::EmptyTrack);

    trajectory.addKeyframe("float", VariantKeyframe {0.f, 1.f});

    BOOST_CHECK_NO_THROW(trajectory["float"].update(0));
    BOOST_CHECK_NO_THROW(trajectory.update(0));
    BOOST_CHECK_NO_THROW(trajectory.update(-1));
    BOOST_CHECK_NO_THROW(trajectory.update(1));
}

template <typename Derived>
std::ostream& operator<<(std::ostream& os, const Eigen::MatrixBase<Derived>& rhs)
{
    static const Eigen::IOFormat iof(5, 0, " ", "\n", "| ", " |", "");
    os << rhs.format(iof);
    return os;
}

template <typename Derived>
std::ostream& operator<<(std::ostream& os, const Eigen::QuaternionBase<Derived>& rhs)
{
    static const Eigen::IOFormat iof(5, 0, " ", "\n", "| ", " |", "");
    os << rhs.format(iof);
    return os;
}




BOOST_AUTO_TEST_CASE(testDifferentTypes)
{
    trajectory.addTrack("float", updateValue(float_));

    std::vector<VariantValue> values
    {
        1.f, Eigen::MatrixXf::Zero(3, 3), Eigen::Quaternionf::Identity()
    };

    for (std::size_t i = 1; i < values.size(); ++i)
    {
        for (std::size_t j = 0; j < i; ++j)
        {
            BOOST_CHECK_NE(values[i].index(), values[j].index());
        }
    }

    BOOST_CHECK_NO_THROW(trajectory.addKeyframe("float", 0, 0.f));
    BOOST_CHECK_THROW(trajectory.addKeyframe("float", 1, Eigen::MatrixXf::Zero(3, 3)),
                      error::WrongValueTypeInKeyframe);
    BOOST_CHECK_THROW(trajectory.addKeyframe("float", 1, Eigen::Quaternionf::Identity()),
                      error::WrongValueTypeInKeyframe);
}

BOOST_AUTO_TEST_CASE(testInterpolateFloat)
{
    float f1 = -2;
    float f2 =  5;

    VariantValue v1(f1);
    VariantValue v2(f2);

    BOOST_CHECK_EQUAL(interpolate::linear<float>(0, v1, v2), f1);
    BOOST_CHECK_EQUAL(interpolate::linear<float>(.5, v1, v2), .5f * (f1 + f2));
    BOOST_CHECK_EQUAL(interpolate::linear<float>(1, v1, v2), f2);
}

BOOST_AUTO_TEST_CASE(testInterpolateMatrix)
{
    Eigen::MatrixXf m1 = Eigen::MatrixXf::Zero(3, 3);
    Eigen::MatrixXf m2 = Eigen::MatrixXf::Identity(3, 3);

    VariantValue v1(m1);
    VariantValue v2(m2);

    Eigen::MatrixXf mi;

    mi = interpolate::linear<Eigen::MatrixXf>(0, v1, v2);
    BOOST_CHECK(mi.isApprox(m1, ftol));

    mi = interpolate::linear<Eigen::MatrixXf>(0.5, v1, v2);
    BOOST_CHECK(mi.isApprox(0.5f * (m1 + m2), ftol));

    mi = interpolate::linear<Eigen::MatrixXf>(1, v1, v2);
    BOOST_CHECK(mi.isApprox(m2, ftol));
}

BOOST_AUTO_TEST_CASE(testInterpolateQuaternion)
{
    Eigen::Quaternionf q1(Eigen::AngleAxisf(0.f, Eigen::Vector3f(1, 0, 0)));
    Eigen::Quaternionf q2(Eigen::AngleAxisf(2.f, Eigen::Vector3f(0, 1, 0)));;

    VariantValue v1(q1);
    VariantValue v2(q2);

    Eigen::Quaternionf mi;

    mi = interpolate::linear<Eigen::Quaternionf>(0, v1, v2);
    BOOST_CHECK(mi.isApprox(q1, ftol));

    mi = interpolate::linear<Eigen::Quaternionf>(0.5, v1, v2);
    BOOST_CHECK(mi.isApprox(q1.slerp(.5, q2), ftol));

    mi = interpolate::linear<Eigen::Quaternionf>(1, v1, v2);
    BOOST_CHECK(mi.isApprox(q2, ftol));
}

BOOST_AUTO_TEST_CASE(testInterpolateVector3)
{
    using ValueT = Eigen::Vector3f;

    ValueT vec1 = Eigen::Vector3f::Zero();
    ValueT vec2 = Eigen::Vector3f::Ones();

    VariantValue v1(vec1);
    VariantValue v2(vec2);

    ValueT mi;

    mi = interpolate::linear<Eigen::MatrixXf>(0, v1, v2);
    BOOST_CHECK(mi.isApprox(vec1, ftol));

    mi = interpolate::linear<Eigen::MatrixXf>(0.5, v1, v2);
    BOOST_CHECK(mi.isApprox(0.5 * (vec1 + vec2), ftol));

    mi = interpolate::linear<Eigen::MatrixXf>(1, v1, v2);
    BOOST_CHECK(mi.isApprox(vec2, ftol));
}



BOOST_AUTO_TEST_CASE(testTrajectoryFloat)
{
    trajectory.addTrack("float", updateValue(float_));

    trajectory.addKeyframe("float", 0.f, 0.f);
    trajectory.addKeyframe("float", VariantKeyframe {1.f, 1.f});

    int num = 10;

    for (float i = 0; i <= num; ++i)
    {
        float time = -i / num;
        trajectory.update(time);
        BOOST_CHECK_CLOSE(float_, 0, ftol);
    }

    for (float i = 0; i <= num; ++i)
    {
        float time = i / num;
        trajectory.update(time);
        BOOST_CHECK_CLOSE(float_, time, ftol);
    }

    for (float i = 0; i <= num; ++i)
    {
        float time = 1 + i / num;
        trajectory.update(time);
        BOOST_CHECK_CLOSE(float_, 1, ftol);
    }
}

BOOST_AUTO_TEST_CASE(testTrajectoryMatrix)
{
    trajectory.addTrack("matrix", updateValue(mat));

    trajectory.addKeyframe("matrix", 0.f, Eigen::MatrixXf::Zero(3, 3));
    trajectory.addKeyframe("matrix", 1.f, Eigen::MatrixXf::Identity(3, 3));

    int num = 10;

    for (float i = 0; i <= num; ++i)
    {
        float time = -i / num;
        trajectory.update(time);
        BOOST_CHECK_CLOSE(mat(0, 0), 0, ftol);
        BOOST_CHECK_CLOSE(mat(0, 1), 0, ftol);
    }

    for (float i = 0; i <= num; ++i)
    {
        float time = i / num;
        trajectory.update(time);
        BOOST_CHECK_CLOSE(mat(0, 0), time, ftol);
        BOOST_CHECK_CLOSE(mat(0, 1), 0, ftol);
    }

    for (float i = 0; i <= num; ++i)
    {
        float time = 1 + i / num;
        trajectory.update(time);
        BOOST_CHECK_CLOSE(mat(0, 0), 1, ftol);
        BOOST_CHECK_CLOSE(mat(0, 1), 0, ftol);
    }
}

BOOST_AUTO_TEST_SUITE_END()

