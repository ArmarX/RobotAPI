set(LIB_NAME       RobotAPISimpleTrajectory)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")


set(LIBS
    ArmarXCoreInterfaces ArmarXCore
)

set(LIB_FILES
    exceptions.cpp
    Trajectory.cpp
    Track.cpp

    interpolate/linear.cpp
)
set(LIB_HEADERS
    exceptions.h
    Track.h
    Trajectory.h
    VariantValue.h

    interpolate/linear.h
)


armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

# add unit tests
add_subdirectory(test)
