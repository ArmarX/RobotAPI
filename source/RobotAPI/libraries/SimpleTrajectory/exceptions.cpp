#include "exceptions.h"

namespace armarx::trajectory::error
{
    TrajectoryException::TrajectoryException(const std::string& msg) : std::logic_error(msg)
    {}


    InterpolateDifferentTypesError::InterpolateDifferentTypesError() :
        TrajectoryException("Interpolating between two different types.")
    {}


    NoTrackWithID::NoTrackWithID(const TrackID& id) : TrajectoryException(makeMsg(id))
    {}

    std::string NoTrackWithID::makeMsg(const TrackID& id)
    {
        std::stringstream ss;
        ss << "No track with ID '" << id << "'. \n"
           << "Add a track with ID '" << id << "' before before adding keyframes.";
        return ss.str();
    }


    EmptyTrack::EmptyTrack(const TrackID& id) : TrajectoryException(makeMsg(id))
    {}

    std::string EmptyTrack::makeMsg(const TrackID& id)
    {
        std::stringstream ss;
        ss << "Track with ID '" << id << "' is empty. \n"
           "Add a keyframe to track '" << id << "' before updating.";
        return ss.str();
    }

    static std::string makeMsg(const TrackID& id, int typeIndex, int expectedTypeIndex)
    {
        std::stringstream ss;
        ss << "Tried to add keyframe with value type '" << typeIndex << "' to non-empty track '"
           << id << "' containing values of type '" << expectedTypeIndex << "'. \n"
           << "Only one value type per track is allowed.";
        return ss.str();
    }

    WrongValueTypeInKeyframe::WrongValueTypeInKeyframe(const TrackID& trackID, int typeIndex, int expectedTypeIndex) :
        TrajectoryException(makeMsg(trackID, typeIndex, expectedTypeIndex))
    {}


}
