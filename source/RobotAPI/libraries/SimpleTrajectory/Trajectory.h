#pragma once

#include <map>

#include "Track.h"


namespace armarx::trajectory
{

    /**
     * @brief This class is used to update entities based on trajectory
     * defined by keyframes.
     *
     * A Trajectory is a collection of tracks, where each track represents a
     * timeline of a single value. A track is composed of keyframes, where
     * each keyframe represents a value at a specific time on the timeline.
     *
     * @see Track
     */
    class Trajectory
    {
    public:

        /// Constructor.
        Trajectory();


        /// Clear the trajectory of all tracks.
        void clear();


        /**
         * @brief Add track with the given ID (and no update function).
         * If there is already a track with the given ID, it is overwritten.
         */
        VariantTrack& addTrack(const TrackID& id);

        /**
         * @brief Add track with the given ID and update function.
         * If there is already a track with the given ID, it is overwritten.
         */
        VariantTrack& addTrack(const TrackID& id, const VariantTrack::UpdateFunc& updateFunc);


        /// Add a keyframe to the specified track.
        /// @throw `error::NoTrackWithID` if there is not track with the given ID
        void addKeyframe(const TrackID& id, const VariantKeyframe& keyframe);

        /// Add a keyframe to the specified track.
        /// @throw `error::NoTrackWithID` if there is not track with the given ID
        void addKeyframe(const TrackID& id, float time, const VariantValue& value);


        /**
         * @brief Update all tracks for the given time.
         * @param ignoreEmptyTracks If true, empty tracks are ignored.
         * @throws `error::EmptyTrack` If `ignoreEmptyTracks` is false and a track is empty.
         */
        void update(float time, bool ignoreEmptyTracks = false);


        /// Get the track with the given ID.
        /// @throw `error::NoTrackWithID` if there is not track with the given ID
        VariantTrack& operator[](const TrackID& id);

        /// Get the track with the given ID.
        /// @throw `error::NoTrackWithID` if there is not track with the given ID
        const VariantTrack& operator[](const TrackID& id) const;

        friend std::ostream& operator<<(std::ostream& os, const Trajectory& trajectory);


    private:

        /// The tracks.
        std::map<TrackID, VariantTrack> tracks;

    };


    std::ostream& operator<<(std::ostream& os, const Trajectory& trajectory);


    // UPDATE FUNCTION HELPERS

    /// Get an update function for value assignments.
    template <typename ValueT>
    VariantTrack::UpdateFunc updateValue(ValueT& v)
    {
        return [&v](VariantValue value)
        {
            v = std::get<ValueT>(value);
        };
    }

    /// Wrap the function in a Track::UpdateFunc.
    VariantTrack::UpdateFunc toUpdateFunc(std::function<void(float)> func);
    /// Wrap the function in a Track::UpdateFunc.
    VariantTrack::UpdateFunc toUpdateFunc(std::function<void(const Eigen::MatrixXf&)> func);
    /// Wrap the function in a Track::UpdateFunc.
    VariantTrack::UpdateFunc toUpdateFunc(std::function<void(const Eigen::Quaternionf&)> func);

}

