#include "Track.h"


namespace armarx::trajectory
{

    template <>
    void Track<VariantValue>::checkValueType(const VariantValue& value)
    {
        if (!empty() && value.index() != keyframes.front().value.index())
        {
            throw error::WrongValueTypeInKeyframe(id, value.index(), keyframes.front().value.index());
        }
    }

}
