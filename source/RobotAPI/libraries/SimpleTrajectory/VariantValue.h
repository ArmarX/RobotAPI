#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <variant>


namespace armarx::trajectory
{

    /// Variant for trajectory values.
    using VariantValue = std::variant<float, Eigen::MatrixXf, Eigen::Quaternionf>;

    /// ID of tracks.
    using TrackID = std::string;

}
