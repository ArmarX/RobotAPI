/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>

//#include <RobotAPI/libraries/core/SimpleDiffIK.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <RobotAPI/libraries/diffik/DiffIKProvider.h>
#include <RobotAPI/libraries/diffik/NaturalDiffIK.h>
#include <optional>

namespace armarx
{
    using NaturalIKPtr = std::shared_ptr<class NaturalIK>;

    /**
     * @brief The NaturalIK class
     *
     * Calculates natural looking IK solutions by
     * 1. Calculate Soechting angles
     * 2. Calculate ForwardKinematics with Soechting angles
     * 3. Use calculated elbow position as a nullspace target for the robot's elbow during diffik
     *
     * WARNING: Only works if robot model is facing forward in Y-direction (like ARMAR-III, ARMAR-IV, ARMAR-6)
     * WARNING2: Soechting model is not valid for all targets. However, elbow position seems to extrapolate fine.
     */
    class NaturalIK
    {
    public:
        struct Parameters
        {
            Parameters() {}
            NaturalDiffIK::Parameters diffIKparams;
            std::optional<float> minimumElbowHeight = std::nullopt;

        };
        struct ArmJoints
        {
            VirtualRobot::RobotNodeSetPtr rns;
            VirtualRobot::RobotNodePtr elbow;
            VirtualRobot::RobotNodePtr shoulder;
            VirtualRobot::RobotNodePtr tcp;
        };


        struct SoechtingAngles
        {
            float SE;
            float SY;
            float EE;
            float EY;


            static constexpr float MMM_LS2LE = 0.188;
            static constexpr float MMM_LE2LW = 0.145;
            static constexpr float MMM_L = 1700;
            static constexpr float MMM_UPPER_ARM_LENGTH = MMM_LS2LE * MMM_L;
            static constexpr float MMM_LOWER_ARM_LENGTH = MMM_LE2LW * MMM_L;

            static constexpr float MMM_LSC2LS = 0.023;
            static constexpr float MMM_BT2LSC = 0.087;

            static constexpr float MMM_CLAVICULAR_WIDTH = MMM_BT2LSC * MMM_L;
            static constexpr float MMM_SHOULDER_POS = (MMM_LSC2LS + MMM_BT2LSC) * MMM_L;
        };

        struct SoechtingForwardPositions
        {
            Eigen::Vector3f shoulder;
            Eigen::Vector3f elbow;
            Eigen::Vector3f wrist;
            SoechtingAngles soechtingAngles;
        };

    public:
        NaturalIK(std::string side, Eigen::Vector3f shoulderPos = Eigen::Vector3f::Zero(), float scale = 1);

        NaturalIK::SoechtingForwardPositions solveSoechtingIK(const Eigen::Vector3f& targetPos, std::optional<float> minElbowHeight = std::nullopt);
        NaturalDiffIK::Result calculateIK(const Eigen::Matrix4f& targetPose, ArmJoints arm, NaturalIK::Parameters params = NaturalIK::Parameters());
        NaturalDiffIK::Result calculateIKpos(const Eigen::Vector3f& targetPos, ArmJoints arm, NaturalIK::Parameters params = NaturalIK::Parameters());

        NaturalDiffIK::Result calculateIK(const Eigen::Matrix4f& targetPose, ArmJoints arm, NaturalDiffIK::Mode setOri, NaturalIK::Parameters params = NaturalIK::Parameters());

        //static SimpleDiffIK::Result CalculateNaturalIK(const Eigen::Matrix4f targetPose, ArmJoints armjoints, Parameters params = Parameters());

        Eigen::Vector3f getShoulderPos();

        /**
         * @brief NaturalIK::CalculateSoechtingAngles
         * @param target Pointing target in mm, relative to the soulder position. X: right, Y: forward, Z: up
         * @param side: Left or Right
         * @param scale: scale factor to match the robots arm length to the human arm length
         * @return Angles described in Soechting and Flanders "Errors in Pointing are Due to Approximations in Sensorimotor Transformations"
         */
        SoechtingAngles CalculateSoechtingAngles(Eigen::Vector3f target);

        SoechtingForwardPositions forwardKinematics(SoechtingAngles sa);

        void setScale(float scale);
        float getScale();


        float getUpperArmLength() const;
        void setUpperArmLength(float value);

        float getLowerArmLength() const;
        void setLowerArmLength(float value);

    private:
        std::string side;
        Eigen::Vector3f shoulderPos;
        float scale;

        int soechtingIterations = 2;

        float upperArmLength = SoechtingAngles::MMM_UPPER_ARM_LENGTH;
        float lowerArmLength = SoechtingAngles::MMM_LOWER_ARM_LENGTH;
    };

    class NaturalIKProvider
        : public DiffIKProvider
    {
    public:
        NaturalIKProvider(const NaturalIK& natik, const NaturalIK::ArmJoints& arm, const NaturalDiffIK::Mode& setOri, const NaturalIK::Parameters& params = NaturalIK::Parameters());
        DiffIKResult SolveAbsolute(const Eigen::Matrix4f& targetPose);
        DiffIKResult SolveRelative(const Eigen::Matrix4f& targetPose, const Eigen::VectorXf& startJointValues);

    private:
        NaturalIK natik;
        NaturalIK::ArmJoints arm;
        NaturalDiffIK::Mode setOri;
        NaturalIK::Parameters params;
    };
}
