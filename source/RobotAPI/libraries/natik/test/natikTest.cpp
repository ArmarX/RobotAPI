/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::natik
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::NaturalIK

#define ARMARX_BOOST_TEST

#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/Test.h>
#include "../NaturalIK.h"

#include <iostream>

using namespace armarx;

float calcAvgError(float upperArmLength, float lowerArmLength)
{
    float errSum = 0;
    int count = 0;
    NaturalIK ik("Right");
    for (int y = 200; y <= 600; y += 100)
    {
        for (int zi = -5; zi <= 5; zi++)
        {
            for (int xi = -5; xi <= 5; xi++)
            {
                float x = y * xi / 5;
                float z = y * zi / 5;
                Eigen::Vector3f target(x, y, z);
                NaturalIK::SoechtingAngles sa = ik.CalculateSoechtingAngles(target);
                ik.setUpperArmLength(upperArmLength);
                ik.setLowerArmLength(lowerArmLength);
                NaturalIK::SoechtingForwardPositions fwd = ik.forwardKinematics(sa);
                Eigen::Vector3f wri = fwd.wrist;

                //ARMARX_IMPORTANT << "err: " << (target - wri).norm();
                errSum += (target - wri).norm();
                count++;
                //ARMARX_IMPORTANT << "is: " << wri.transpose() << " should be: " << target.transpose();
            }
        }
    }
    return errSum / count;
}

BOOST_AUTO_TEST_CASE(testSoechtingAngles)
{
    for (float ual = 0.15; ual <= 0.251; ual += 0.01)
    {
        for (float lal = 0.15; lal <= 0.251; lal += 0.01)
        {
            float err = calcAvgError(ual * 1700, lal * 1700);

            ARMARX_IMPORTANT << VAROUT(ual) << VAROUT(lal) << VAROUT(err);
        }
    }
    /*
        for(int y = 200; y <= 600; y+=100)
        {
            for(int zi = -5; zi <= 5; zi++)
            {
                for(int xi = -5; xi <= 5; xi++)
                {
                    float x = y * xi / 5;
                    float z = y * zi / 5;
                    Eigen::Vector3f target(x,y,z);
                    NaturalIK::SoechtingAngles sa = NaturalIK::CalculateSoechtingAngles(target, "Right", 1);
                    Eigen::Vector3f wri = sa.forwardKinematics(0.188 * 1700, 0.188 * 1700);

                    ARMARX_IMPORTANT << "err: " << (target - wri).norm();
                    //ARMARX_IMPORTANT << "is: " << wri.transpose() << " should be: " << target.transpose();
                }
            }
        }*/

    /*
    Eigen::Vector3f t2(0,300,-300);
    Eigen::Vector3f a_000 = NaturalIK::CalculateSoechtingAngles(t2, "Right", 1).forwardKinematics(0.188 * 1700, 0.188 * 1700);
    Eigen::Vector3f a_100 = NaturalIK::CalculateSoechtingAngles(t2 + Eigen::Vector3f(10, 0, 0), "Right", 1).forwardKinematics(0.188 * 1700, 0.188 * 1700);
    Eigen::Vector3f a_010 = NaturalIK::CalculateSoechtingAngles(t2 + Eigen::Vector3f(0, 10, 0), "Right", 1).forwardKinematics(0.188 * 1700, 0.188 * 1700);
    Eigen::Vector3f a_001 = NaturalIK::CalculateSoechtingAngles(t2 + Eigen::Vector3f(0, 0, 10), "Right", 1).forwardKinematics(0.188 * 1700, 0.188 * 1700);

    ARMARX_IMPORTANT << (a_100 - a_000).transpose();
    ARMARX_IMPORTANT << (a_010 - a_000).transpose();
    ARMARX_IMPORTANT << (a_001 - a_000).transpose();


    BOOST_CHECK_EQUAL(true, true);*/
}

