#include "ArVizComponentPlugin.h"

#include <ArmarXCore/core/Component.h>


namespace armarx::plugins
{
    static const std::string ARVIZ_TOPIC_PROPERTY_NAME = "ArVizTopicName";
    static const std::string ARVIZ_TOPIC_PROPERTY_DEFAULT = "ArVizTopic";

    static const std::string ARVIZ_STORAGE_PROPERTY_NAME = "ArVizStorageName";
    static const std::string ARVIZ_STORAGE_PROPERTY_DEFAULT = "ArVizStorage";


    std::string ArVizComponentPlugin::getTopicName()
    {
        return parentDerives<Component>() ?
               parent<Component>().getProperty<std::string>(makePropertyName(ARVIZ_TOPIC_PROPERTY_NAME)) :
                    ARVIZ_TOPIC_PROPERTY_DEFAULT;
    }

    std::string ArVizComponentPlugin::getStorageName()
    {
        return parentDerives<Component>() ?
               parent<Component>().getProperty<std::string>(makePropertyName(ARVIZ_STORAGE_PROPERTY_NAME)) :
                    ARVIZ_STORAGE_PROPERTY_DEFAULT;
    }

    void ArVizComponentPlugin::preOnInitComponent()
    {
        parent().offeringTopic(getTopicName());
        parent().usingProxy(getStorageName());
    }

    void ArVizComponentPlugin::preOnConnectComponent()
    {
        if (parentDerives<ArVizComponentPluginUser>())
        {
            parent<ArVizComponentPluginUser>().arviz = createClient();
        }
    }

    void ArVizComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
    {
        if (!properties->hasDefinition(makePropertyName(ARVIZ_TOPIC_PROPERTY_NAME)))
        {
            properties->defineOptionalProperty<std::string>(
                makePropertyName(ARVIZ_TOPIC_PROPERTY_NAME),
                ARVIZ_TOPIC_PROPERTY_DEFAULT,
                "Name of the ArViz topic");
        }
        if (!properties->hasDefinition(makePropertyName(ARVIZ_STORAGE_PROPERTY_NAME)))
        {
            properties->defineOptionalProperty<std::string>(
                makePropertyName(ARVIZ_STORAGE_PROPERTY_NAME),
                ARVIZ_STORAGE_PROPERTY_DEFAULT,
                "Name of the ArViz storage");
        }
    }

    viz::Client ArVizComponentPlugin::createClient()
    {
        return viz::Client(parent(), getTopicName(), getStorageName());
    }
}

namespace armarx
{
    ArVizComponentPluginUser::ArVizComponentPluginUser()
    {
        addPlugin(plugin);
    }

    viz::Client ArVizComponentPluginUser::createArVizClient()
    {
        return plugin->createClient();
    }
}
