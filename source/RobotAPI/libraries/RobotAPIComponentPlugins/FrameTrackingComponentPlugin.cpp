#include "FrameTrackingComponentPlugin.h"

#include <ArmarXCore/core/Component.h>


namespace armarx
{
    namespace plugins
    {

        void FrameTrackingComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
        }

        void FrameTrackingComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getProxyFromProperty(_frameTracking, PROPERTY_NAME);
        }

        void FrameTrackingComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    PROPERTY_NAME,
                    "FrameTracking",
                    "Name of the FrameTracking");
            }
        }

        FrameTrackingInterfacePrx FrameTrackingComponentPlugin::getFrameTracking()
        {
            return _frameTracking;
        }


    }
}

namespace armarx
{

    FrameTrackingComponentPluginUser::FrameTrackingComponentPluginUser()
    {
        addPlugin(plugin);
    }

    FrameTrackingInterfacePrx FrameTrackingComponentPluginUser::getFrameTracking()
    {
        return plugin->getFrameTracking();
    }


}


