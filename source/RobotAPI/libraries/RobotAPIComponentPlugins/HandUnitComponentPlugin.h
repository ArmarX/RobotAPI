#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>
#include <RobotAPI/interface/units/HandUnitInterface.h>


namespace armarx
{
    namespace plugins
    {

        class HandUnitComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            HandUnitInterfacePrx getHandUnit();

        private:
            static constexpr const char* PROPERTY_NAME = "HandUnitName";
            HandUnitInterfacePrx _handUnit;
        };

    }
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    /**
     * @brief Provides a ready-to-use HandUnit.
     */
    class HandUnitComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        HandUnitComponentPluginUser();
        HandUnitInterfacePrx getHandUnit();

    private:
        armarx::plugins::HandUnitComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using HandUnitComponentPluginUser = armarx::HandUnitComponentPluginUser;
    }
}
