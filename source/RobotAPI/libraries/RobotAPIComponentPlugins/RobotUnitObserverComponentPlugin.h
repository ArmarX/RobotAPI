#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>


namespace armarx
{
    namespace plugins
    {

        class RobotUnitObserverComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            ObserverInterfacePrx getRobotUnitObserver();

        private:
            static constexpr const char* PROPERTY_NAME = "RobotUnitObserverName";
            ObserverInterfacePrx _robotUnitObserver;
        };

    }
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    /**
     * @brief Provides a ready-to-use RobotUnitObserver.
     */
    class RobotUnitObserverComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        RobotUnitObserverComponentPluginUser();
        ObserverInterfacePrx getRobotUnitObserver();

    private:
        armarx::plugins::RobotUnitObserverComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using RobotUnitObserverComponentPluginUser = armarx::RobotUnitObserverComponentPluginUser;
    }
}
