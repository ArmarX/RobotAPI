#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>


namespace armarx
{
    namespace plugins
    {

        class KinematicUnitComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            KinematicUnitInterfacePrx getKinematicUnit();

        private:
            static constexpr const char* PROPERTY_NAME = "KinematicUnitName";
            KinematicUnitInterfacePrx _kinematicUnit;
        };

    }
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    /**
     * @brief Provides a ready-to-use KinematicUnit.
     */
    class KinematicUnitComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        KinematicUnitComponentPluginUser();
        KinematicUnitInterfacePrx getKinematicUnit();

    private:
        armarx::plugins::KinematicUnitComponentPlugin* plugin = nullptr;
    };


}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using KinematicUnitComponentPluginUser = armarx::KinematicUnitComponentPluginUser;
    }
}
