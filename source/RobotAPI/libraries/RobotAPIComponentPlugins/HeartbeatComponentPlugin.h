/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>

#include <RobotAPI/interface/components/RobotHealthInterface.h>

namespace armarx::plugins
{

    class HeartbeatComponentPlugin : public ComponentPlugin
    {
    public:
        using ComponentPlugin::ComponentPlugin;

        /**
         * @brief Configures a heartbeat subchannel.
         *
         * @param channel Identifier of the heartbeat channel
         * @param args Configuration of this channel's heartbeat properties
         */
        void configureHeartbeatChannel(const std::string& channel,
                                       const RobotHealthHeartbeatArgs& args);

        /**
         * @brief Sends out a heartbeat using the default config
         *
         */
        void heartbeat();

        /**
         * @brief Sends out a heartbeat for a subchannel.
         *
         * Note: You must call configureHeartbeatChannel(...) first to register the channel config!
         *
         * @param channel Identifier of the heartbeat channel
         */
        void heartbeat(const std::string& channel);

    protected:
        void preOnInitComponent() override;
        void postOnInitComponent() override;
        void preOnConnectComponent() override;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

    private:
        //! heartbeat topic name (outgoing)
        std::string topicName{"DebugObserver"};

        //! name of this component used as identifier for heartbeats
        std::string componentName;

        //
        static constexpr auto topicPropertyName = "heartbeat.TopicName";
        static constexpr auto maximumCycleTimeWarningMSPropertyName =
            "heartbeat.maximumCycleTimeWarningMS";
        static constexpr auto maximumCycleTimeErrorMSPropertyName =
            "heartbeat.maximumCycleTimeErrorMS";

        RobotHealthInterfacePrx robotHealthTopic;

        //! default config used in heartbeat(), set via properties
        RobotHealthHeartbeatArgs heartbeatArgs;

        //! configs used in heartbeat(channel), set by user via configureHeartbeatChannel(...)
        std::unordered_map<std::string, RobotHealthHeartbeatArgs> channelHeartbeatConfig;
    };
} // namespace armarx::plugins
