#include <SimoxUtility/algorithm/string/string_tools.h>

#include <ArmarXCore/core/Component.h>

#include "GraspCandidateObserverComponentPlugin.h"

namespace armarx
{
    namespace plugins
    {

        void GraspCandidateObserverComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
            if (!_graspCandidateObserver && _graspCandidateObserverName.empty())
            {
                parent<Component>().getProperty(_graspCandidateObserverName, makePropertyName(PROPERTY_NAME));
            }

            if (!_graspCandidateObserver)
            {
                parent<Component>().usingProxy(_graspCandidateObserverName);
            }
        }

        void GraspCandidateObserverComponentPlugin::preOnConnectComponent()
        {
            if (!_graspCandidateObserver)
            {
                parent<Component>().getProxy(_graspCandidateObserver, _graspCandidateObserverName);
            }
        }

        void GraspCandidateObserverComponentPlugin::postOnDisconnectComponent()
        {
            _graspCandidateObserver = nullptr;
        }

        void GraspCandidateObserverComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    PROPERTY_NAME,
                    "GraspCandidateObserver",
                    "Name of the GraspCandidateObserver");
            }
        }

        grasping::GraspCandidateObserverInterfacePrx GraspCandidateObserverComponentPlugin::getGraspCandidateObserver()
        {
            return _graspCandidateObserver;
        }

        const std::string& GraspCandidateObserverComponentPlugin::getGraspCandidateObserverName() const
        {
            return _graspCandidateObserverName;
        }

        grasping::GraspCandidateSeq GraspCandidateObserverComponentPlugin::getAllCandidates() const
        {
            if (_graspCandidateObserver)
            {
                return _graspCandidateObserver->getAllCandidates();
            }
            return {};
        }

        grasping::GraspCandidateSeq GraspCandidateObserverComponentPlugin::getCandidates() const
        {
            if (!_graspCandidateObserver)
            {
                return {};
            }
            if (_upstream_providers.empty())
            {
                return _graspCandidateObserver->getAllCandidates();
            }
            return _graspCandidateObserver->getCandidatesByProviders(_upstream_providers);
        }

        void GraspCandidateObserverComponentPlugin::setUpstreamGraspCandidateProviders(std::vector<std::string> pr)
        {
            _upstream_providers = std::move(pr);
        }

        void GraspCandidateObserverComponentPlugin::setUpstreamGraspCandidateProvidersFromCSV(const std::string& csv, const std::string& delim)
        {
            setUpstreamGraspCandidateProviders(simox::alg::split(csv, delim));
        }

        void GraspCandidateObserverComponentPlugin::setGraspCandidateObserverName(const std::string& name)
        {
            _graspCandidateObserverName = name;
        }
    }
}

namespace armarx
{
    GraspCandidateObserverComponentPluginUser::GraspCandidateObserverComponentPluginUser()
    {
        addPlugin(plugin);
    }

    grasping::GraspCandidateObserverInterfacePrx GraspCandidateObserverComponentPluginUser::getGraspCandidateObserver()
    {
        return plugin->getGraspCandidateObserver();
    }

    plugins::GraspCandidateObserverComponentPlugin& GraspCandidateObserverComponentPluginUser::getGraspCandidateObserverComponentPlugin()
    {
        ARMARX_CHECK_NOT_NULL(plugin);
        return *plugin;
    }

    const plugins::GraspCandidateObserverComponentPlugin& GraspCandidateObserverComponentPluginUser::getGraspCandidateObserverComponentPlugin() const
    {
        ARMARX_CHECK_NOT_NULL(plugin);
        return *plugin;
    }

    grasping::GraspCandidateSeq GraspCandidateObserverComponentPluginUser::getAllGraspCandidates() const
    {
        return getGraspCandidateObserverComponentPlugin().getAllCandidates();
    }

    grasping::GraspCandidateSeq GraspCandidateObserverComponentPluginUser::getGraspCandidates() const
    {
        return getGraspCandidateObserverComponentPlugin().getCandidates();
    }
}
