#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>
#include <RobotAPI/interface/components/FrameTrackingInterface.h>


namespace armarx
{
    namespace plugins
    {

        class FrameTrackingComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            FrameTrackingInterfacePrx getFrameTracking();

        private:
            static constexpr const char* PROPERTY_NAME = "FrameTrackingName";
            FrameTrackingInterfacePrx _frameTracking;
        };

    }
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    /**
     * @brief Provides a ready-to-use FrameTracking.
     */
    class FrameTrackingComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        FrameTrackingComponentPluginUser();
        FrameTrackingInterfacePrx getFrameTracking();

    private:
        armarx::plugins::FrameTrackingComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using FrameTrackingComponentPluginUser = armarx::FrameTrackingComponentPluginUser;
    }
}
