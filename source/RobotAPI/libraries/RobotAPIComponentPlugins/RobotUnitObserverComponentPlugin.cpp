#include "RobotUnitObserverComponentPlugin.h"

#include <ArmarXCore/core/Component.h>


namespace armarx
{
    namespace plugins
    {

        void RobotUnitObserverComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
        }

        void RobotUnitObserverComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getProxyFromProperty(_robotUnitObserver, PROPERTY_NAME);
        }

        void RobotUnitObserverComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    PROPERTY_NAME,
                    "RobotUnitObserver",
                    "Name of the RobotUnitObserver");
            }
        }

        ObserverInterfacePrx RobotUnitObserverComponentPlugin::getRobotUnitObserver()
        {
            return _robotUnitObserver;
        }


    }
}

namespace armarx
{

    RobotUnitObserverComponentPluginUser::RobotUnitObserverComponentPluginUser()
    {
        addPlugin(plugin);
    }

    ObserverInterfacePrx RobotUnitObserverComponentPluginUser::getRobotUnitObserver()
    {
        return plugin->getRobotUnitObserver();
    }


}


