/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotAPIComponentPlugins
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/util/CPPUtility/trace.h>
#include <ArmarXCore/core/Component.h>

#include "DebugDrawerHelperComponentPlugin.h"


namespace armarx::plugins
{
    DebugDrawerHelperComponentPlugin::DebugDrawerHelperComponentPlugin(ManagedIceObject& parent, std::string pre) :
        ComponentPlugin(parent, pre),
        _pre{pre}
    {
        ARMARX_TRACE;
        addPlugin(_robotStateComponentPlugin, pre);
        addPluginDependency(_robotStateComponentPlugin);
    }

    const DebugDrawerHelper& DebugDrawerHelperComponentPlugin::getDebugDrawerHelper() const
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(_debugDrawerHelper);
        return *_debugDrawerHelper;
    }

    DebugDrawerHelper& DebugDrawerHelperComponentPlugin::getDebugDrawerHelper()
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(_debugDrawerHelper);
        return *_debugDrawerHelper;
    }

    const DebugDrawerInterfacePrx& DebugDrawerHelperComponentPlugin::getDebugDrawerTopic() const
    {
        ARMARX_TRACE;
        return getDebugDrawerHelper().getDebugDrawer();
    }

    std::unique_lock<std::recursive_mutex> DebugDrawerHelperComponentPlugin::getDebugDrawerHelperLock() const
    {
        ARMARX_TRACE;
        return std::unique_lock{_debugDrawerHelperMutex};
    }

    std::string DebugDrawerHelperComponentPlugin::getDebugDrawerLayerName() const
    {
        ARMARX_TRACE;
        if (_pre.empty())
        {
            return parent().getName();
        }
        return _pre + "_" + parent().getName();
    }

    void DebugDrawerHelperComponentPlugin::preOnInitComponent()
    {
        ARMARX_TRACE;
        if (!_debugDrawerTopic)
        {
            parent<Component>().offeringTopicFromProperty(_propertyName);
        }
    }

    void DebugDrawerHelperComponentPlugin::preOnConnectComponent()
    {
        ARMARX_TRACE;
        if (!_debugDrawerTopic)
        {
            parent<Component>().getTopicFromProperty(_debugDrawerTopic, _propertyName);
        }
        if (!_debugDrawerHelper)
        {
            const auto robotID = _pre + "_DebugDrawerHelperComponentPlugin_" + parent().getName();
            if (!_robotStateComponentPlugin->hasRobot(robotID))
            {
                _robotStateComponentPlugin->addRobot(robotID,
                                                     VirtualRobot::RobotIO::eStructure);
            }

            const auto robot = _robotStateComponentPlugin->getRobot(robotID);
            ARMARX_CHECK_NOT_NULL(_debugDrawerTopic);
            ARMARX_CHECK_NOT_NULL(robot);
            _debugDrawerHelper = std::make_unique<DebugDrawerHelper>(
                                     _debugDrawerTopic,
                                     getDebugDrawerLayerName(),
                                     robot);
        }
    }

    void DebugDrawerHelperComponentPlugin::postOnDisconnectComponent()
    {
        ARMARX_TRACE;
        if (!_doNotClearLayersOnDisconnect)
        {
            try
            {
                _debugDrawerHelper->cyclicCleanup();
                _debugDrawerHelper->cyclicCleanup();
                _debugDrawerHelper->clearLayer();
            }
            catch (...) {}
        }
        _debugDrawerTopic  = nullptr;
    }

    void DebugDrawerHelperComponentPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {
        ARMARX_TRACE;
        if (!properties->hasDefinition(_propertyName))
        {
            properties->defineOptionalProperty<std::string>(
                _propertyName,
                "DebugDrawerUpdates",
                "Name of the debug drawer topic");
        }
    }

    VirtualRobot::RobotPtr DebugDrawerHelperComponentPlugin::getDebugDrawerHelperRobot() const
    {
        ARMARX_TRACE;
        return _debugDrawerHelper->getRobot();
    }

    bool DebugDrawerHelperComponentPlugin::synchronizeDebugDrawerHelperRobot() const
    {
        ARMARX_TRACE;
        const auto g = getDebugDrawerHelperLock();
        ARMARX_CHECK_NOT_NULL(getDebugDrawerHelperRobot());
        return _robotStateComponentPlugin->synchronizeLocalClone(getDebugDrawerHelperRobot());
    }

    bool DebugDrawerHelperComponentPlugin::synchronizeDebugDrawerHelperRobot(Ice::Long timestamp) const
    {
        ARMARX_TRACE;
        const auto g = getDebugDrawerHelperLock();
        ARMARX_CHECK_NOT_NULL(getDebugDrawerHelperRobot());
        return _robotStateComponentPlugin->synchronizeLocalClone(getDebugDrawerHelperRobot(), timestamp);
    }

    bool DebugDrawerHelperComponentPlugin::synchronizeDebugDrawerHelperRobot(const RobotStateConfig& state) const
    {
        ARMARX_TRACE;
        const auto g = getDebugDrawerHelperLock();
        ARMARX_CHECK_NOT_NULL(getDebugDrawerHelperRobot());
        return _robotStateComponentPlugin->synchronizeLocalClone(getDebugDrawerHelperRobot(), state);
    }

    void DebugDrawerHelperComponentPlugin::setClearLayersOnDisconnect(bool b)
    {
        ARMARX_TRACE;
        _doNotClearLayersOnDisconnect = !b;
    }
}

namespace armarx
{
    DebugDrawerHelperComponentPluginUser::DebugDrawerHelperComponentPluginUser()
    {
        ARMARX_TRACE;
        addPlugin(_debugDrawerHelperComponentPlugin);
    }

    const DebugDrawerHelperComponentPluginUser::DebugDrawerHelperComponentPlugin& DebugDrawerHelperComponentPluginUser::getDebugDrawerHelperPlugin() const
    {
        ARMARX_TRACE;
        return *_debugDrawerHelperComponentPlugin;
    }

    DebugDrawerHelperComponentPluginUser::DebugDrawerHelperComponentPlugin& DebugDrawerHelperComponentPluginUser::getDebugDrawerHelperPlugin()
    {
        ARMARX_TRACE;
        return *_debugDrawerHelperComponentPlugin;
    }

    const DebugDrawerHelper& DebugDrawerHelperComponentPluginUser::getDebugDrawerHelper() const
    {
        ARMARX_TRACE;
        return getDebugDrawerHelperPlugin().getDebugDrawerHelper();
    }

    DebugDrawerHelper& DebugDrawerHelperComponentPluginUser::getDebugDrawerHelper()
    {
        ARMARX_TRACE;
        return getDebugDrawerHelperPlugin().getDebugDrawerHelper();
    }

    const DebugDrawerInterfacePrx& DebugDrawerHelperComponentPluginUser::getDebugDrawerTopic() const
    {
        ARMARX_TRACE;
        return getDebugDrawerHelperPlugin().getDebugDrawerTopic();
    }

    std::unique_lock<std::recursive_mutex> DebugDrawerHelperComponentPluginUser::getDebugDrawerHelperLock() const
    {
        ARMARX_TRACE;
        return getDebugDrawerHelperPlugin().getDebugDrawerHelperLock();
    }
    std::string DebugDrawerHelperComponentPluginUser::getDebugDrawerLayerName() const
    {
        ARMARX_TRACE;
        return getDebugDrawerHelperPlugin().getDebugDrawerLayerName();
    }

    VirtualRobot::RobotPtr DebugDrawerHelperComponentPluginUser::getDebugDrawerHelperRobot() const
    {
        ARMARX_TRACE;
        return getDebugDrawerHelperPlugin().getDebugDrawerHelperRobot();
    }

    bool DebugDrawerHelperComponentPluginUser::synchronizeDebugDrawerHelperRobot() const
    {
        ARMARX_TRACE;
        return getDebugDrawerHelperPlugin().synchronizeDebugDrawerHelperRobot();
    }
    bool DebugDrawerHelperComponentPluginUser::synchronizeDebugDrawerHelperRobot(Ice::Long timestamp) const
    {
        ARMARX_TRACE;
        return getDebugDrawerHelperPlugin().synchronizeDebugDrawerHelperRobot(timestamp);
    }
    bool DebugDrawerHelperComponentPluginUser::synchronizeDebugDrawerHelperRobot(const RobotStateConfig& state) const
    {
        ARMARX_TRACE;
        return getDebugDrawerHelperPlugin().synchronizeDebugDrawerHelperRobot(state);
    }
}
