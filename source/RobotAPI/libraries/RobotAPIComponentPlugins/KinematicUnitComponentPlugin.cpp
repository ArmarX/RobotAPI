#include "KinematicUnitComponentPlugin.h"

#include <ArmarXCore/core/Component.h>


namespace armarx
{
    namespace plugins
    {

        void KinematicUnitComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
        }

        void KinematicUnitComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getProxyFromProperty(_kinematicUnit, PROPERTY_NAME);
        }

        void KinematicUnitComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineRequiredProperty<std::string>(
                    PROPERTY_NAME,
                    "Name of the KinematicUnit");
            }
        }

        KinematicUnitInterfacePrx KinematicUnitComponentPlugin::getKinematicUnit()
        {
            return _kinematicUnit;
        }


    }

}

namespace armarx
{

    KinematicUnitComponentPluginUser::KinematicUnitComponentPluginUser()
    {
        addPlugin(plugin);
    }

    KinematicUnitInterfacePrx KinematicUnitComponentPluginUser::getKinematicUnit()
    {
        return plugin->getKinematicUnit();
    }




}


