#include "PlatformUnitComponentPlugin.h"

#include <ArmarXCore/core/Component.h>


namespace armarx
{
    namespace plugins
    {

        void PlatformUnitComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
        }

        void PlatformUnitComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getProxyFromProperty(_platformUnit, PROPERTY_NAME);
        }

        void PlatformUnitComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineRequiredProperty<std::string>(
                    PROPERTY_NAME,
                    "Name of the PlatformUnit");
            }
        }

        PlatformUnitInterfacePrx PlatformUnitComponentPlugin::getPlatformUnit()
        {
            return _platformUnit;
        }


    }
}

namespace armarx
{

    PlatformUnitComponentPluginUser::PlatformUnitComponentPluginUser()
    {
        addPlugin(plugin);
    }

    PlatformUnitInterfacePrx PlatformUnitComponentPluginUser::getPlatformUnit()
    {
        return plugin->getPlatformUnit();
    }


}


