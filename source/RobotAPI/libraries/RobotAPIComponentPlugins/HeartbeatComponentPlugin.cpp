#include "HeartbeatComponentPlugin.h"

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/interface/components/RobotHealthInterface.h>

namespace armarx::plugins
{
    void HeartbeatComponentPlugin::configureHeartbeatChannel(const std::string& channel,
                                                             const RobotHealthHeartbeatArgs& args)
    {
        channelHeartbeatConfig.emplace(channel, args);
    }

    void HeartbeatComponentPlugin::heartbeat()
    {

        if (robotHealthTopic)
        {
            robotHealthTopic->heartbeat(componentName, heartbeatArgs);
        } else
        {
            ARMARX_WARNING << "No robot health topic available!";
        }
    }

    void HeartbeatComponentPlugin::heartbeat(const std::string& channel)
    {
        const auto argsIt = channelHeartbeatConfig.find(channel);
        ARMARX_CHECK(argsIt != channelHeartbeatConfig.end()) << "heartbeat() called for unknown channel '" << channel
                                                             << "'."
                                                             << "You must register the config using configureHeartbeatChannel(channel) first!";

        const auto& args = argsIt->second;

        if (robotHealthTopic)
        {
            robotHealthTopic->heartbeat(componentName + "_" + channel, args);
        } else
        {
            ARMARX_WARNING << "No robot health topic available!";
        }
    }

    void HeartbeatComponentPlugin::preOnInitComponent()
    {
        //        if (topicName.empty())
        //        {
        //            parent<Component>().getProperty(topicName, makePropertyName(topicPropertyName));
        //        }
        //        parent<Component>().offeringTopic(topicName);
    }

    void HeartbeatComponentPlugin::postOnInitComponent()
    {
    }

    void HeartbeatComponentPlugin::preOnConnectComponent()
    {
        //        robotHealthTopic = parent<Component>().getTopic<RobotHealthInterfacePrx>(topicName);
        componentName = parent<Component>().getName();
    }

    void HeartbeatComponentPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {
        if (!properties->hasDefinition(makePropertyName(topicPropertyName)))
        {
            properties->topic(robotHealthTopic, topicName, topicPropertyName,
                              "Name of the topic the DebugObserver listens on");
        }

        if (not properties->hasDefinition(makePropertyName(maximumCycleTimeWarningMSPropertyName)))
        {
            properties->required(heartbeatArgs.maximumCycleTimeWarningMS, maximumCycleTimeWarningMSPropertyName,
                                 "maximum cycle time before warning is emitted");
        }

        if (not properties->hasDefinition(makePropertyName(maximumCycleTimeErrorMSPropertyName)))
        {
            properties->required(heartbeatArgs.maximumCycleTimeErrorMS, maximumCycleTimeErrorMSPropertyName,
                                 "maximum cycle time before error is emitted");
        }
    }

} // namespace armarx::plugins
