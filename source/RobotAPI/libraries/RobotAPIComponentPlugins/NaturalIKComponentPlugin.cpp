#include "NaturalIKComponentPlugin.h"

#include <ArmarXCore/core/Component.h>


namespace armarx
{
    namespace plugins
    {

        void NaturalIKComponentPlugin::preOnInitComponent()
        {
            parent<Component>().usingProxyFromProperty(PROPERTY_NAME);
        }

        void NaturalIKComponentPlugin::preOnConnectComponent()
        {
            parent<Component>().getProxyFromProperty(_naturalIK, PROPERTY_NAME);
        }

        void NaturalIKComponentPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
        {
            if (!properties->hasDefinition(PROPERTY_NAME))
            {
                properties->defineOptionalProperty<std::string>(
                    PROPERTY_NAME,
                    "NaturalIK",
                    "Name of the NaturalIK");
            }
        }

        NaturalIKInterfacePrx NaturalIKComponentPlugin::getNaturalIK()
        {
            return _naturalIK;
        }


    }
}

namespace armarx
{

    NaturalIKComponentPluginUser::NaturalIKComponentPluginUser()
    {
        addPlugin(plugin);
    }

    NaturalIKInterfacePrx NaturalIKComponentPluginUser::getNaturalIK()
    {
        return plugin->getNaturalIK();
    }


}


