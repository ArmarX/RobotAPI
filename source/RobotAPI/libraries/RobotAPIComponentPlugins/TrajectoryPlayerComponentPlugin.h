#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>
#include <RobotAPI/interface/components/TrajectoryPlayerInterface.h>


namespace armarx
{
    namespace plugins
    {

        class TrajectoryPlayerComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            TrajectoryPlayerInterfacePrx getTrajectoryPlayer();

        private:
            static constexpr const char* PROPERTY_NAME = "TrajectoryPlayerName";
            TrajectoryPlayerInterfacePrx _trajectoryPlayer;
        };

    }
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    /**
     * @brief Provides a ready-to-use TrajectoryPlayer.
     */
    class TrajectoryPlayerComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        TrajectoryPlayerComponentPluginUser();
        TrajectoryPlayerInterfacePrx getTrajectoryPlayer();

    private:
        armarx::plugins::TrajectoryPlayerComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using TrajectoryPlayerComponentPluginUser = armarx::TrajectoryPlayerComponentPluginUser;
    }
}
