#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>
#include <RobotAPI/interface/units/PlatformUnitInterface.h>


namespace armarx
{
    namespace plugins
    {

        class PlatformUnitComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            PlatformUnitInterfacePrx getPlatformUnit();

        private:
            static constexpr const char* PROPERTY_NAME = "PlatformUnitName";
            PlatformUnitInterfacePrx _platformUnit;
        };

    }
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    /**
     * @brief Provides a ready-to-use PlatformUnit.
     */
    class PlatformUnitComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        PlatformUnitComponentPluginUser();
        PlatformUnitInterfacePrx getPlatformUnit();

    private:
        armarx::plugins::PlatformUnitComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using PlatformUnitComponentPluginUser = armarx::PlatformUnitComponentPluginUser;
    }
}
