#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>
#include <RobotAPI/interface/components/CartesianPositionControlInterface.h>


namespace armarx
{
    namespace plugins
    {

        class CartesianPositionControlComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            CartesianPositionControlInterfacePrx getCartesianPositionControl();

        private:
            static constexpr const char* PROPERTY_NAME = "CartesianPositionControlName";
            CartesianPositionControlInterfacePrx _cartesianPositionControl;
        };

    }
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    /**
     * @brief Provides a ready-to-use CartesianPositionControl.
     */
    class CartesianPositionControlComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        CartesianPositionControlComponentPluginUser();
        CartesianPositionControlInterfacePrx getCartesianPositionControl();

    private:
        armarx::plugins::CartesianPositionControlComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using CartesianPositionControlComponentPluginUser = armarx::CartesianPositionControlComponentPluginUser;
    }
}
