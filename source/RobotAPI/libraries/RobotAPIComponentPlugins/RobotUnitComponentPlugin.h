#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/libraries/RobotUnitDataStreamingReceiver/RobotUnitDataStreamingReceiver.h>


namespace armarx
{
    namespace plugins
    {

        class RobotUnitComponentPlugin : public ComponentPlugin
        {
        public:
            using ComponentPlugin::ComponentPlugin;

            void preOnInitComponent() override;

            void preOnConnectComponent() override;
            void postOnDisconnectComponent() override;

            void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

            RobotUnitInterfacePrx getRobotUnit() const;

            void setRobotUnitName(const std::string& name);
            const std::string& getRobotUnitName() const;
            bool hasRobotUnitName() const;

            void setRobotUnitAsOptionalDependency(bool isOptional = true);

            /**
             * @brief Waits until the robot unit is running.
             *
             * Although the robot unit proxy might be initialized (\see getRobotUnit()), the robot unit might
             * not be fully initialized.
             *
             * @param termCond Termination condition.
             *      If it evaluates to true, waitUntilRobotUnitIsRunning returns without waiting
             *      for the robot unit to become available.
             */
            void waitUntilRobotUnitIsRunning(const std::function<bool()>& termCond = [] {return false;});

            bool robotUnitIsRunning() const;



            //controllers
        public:
            template<class PrxT>
            PrxT createNJointController(const std::string& className,
                                        const std::string& instanceName,
                                        const NJointControllerConfigPtr& config,
                                        bool doManageController = true)
            {
                return PrxT::checkedCast(createNJointController(className, instanceName, config, doManageController));
            }
            template<class PrxT>
            void createNJointController(PrxT& prx,
                                        const std::string& className,
                                        const std::string& instanceName,
                                        const NJointControllerConfigPtr& config,
                                        bool doManageController = true)
            {
                prx = PrxT::checkedCast(createNJointController(className, instanceName, config, doManageController));
            }
            NJointControllerInterfacePrx createNJointController(const std::string& className,
                    const std::string& instanceName,
                    const NJointControllerConfigPtr& config,
                    bool doManageController = true);
            void manageController(const NJointControllerInterfacePrx& ctrl);
            void manageController(const std::string& ctrl);

            //datastreaming
        public:
            RobotUnitDataStreamingReceiverPtr startDataSatreming(const RobotUnitDataStreaming::Config& cfg);

        private:
            static constexpr const char* PROPERTY_NAME = "RobotUnitName";
            RobotUnitInterfacePrx _robotUnit;
            std::string           _robotUnitName;
            bool                  _isRobotUnitOptionalDependency = false;
            std::set<std::string> _ctrls;
        };
    }
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    /**
     * @brief Provides a ready-to-use RobotUnit.
     */
    class RobotUnitComponentPluginUser : virtual public ManagedIceObject
    {
    public:
        RobotUnitComponentPluginUser();

        RobotUnitInterfacePrx getRobotUnit() const;

        void setRobotUnitAsOptionalDependency(bool isOptional = true)
        {
            plugin->setRobotUnitAsOptionalDependency(isOptional);
        }

        /**
         * @brief Waits until the robot unit is running.
         *
         * Although the robot unit proxy might be initialized (\see getRobotUnit()), the robot unit might
         * not be fully initialized.
         *
         * @param termCond Termination condition. If it evaluates to true, waitUntilRobotUnitIsRunning returns without waiting
        *                  for the robot unit to become available.
         */
        void waitUntilRobotUnitIsRunning(const std::function<bool()>& termCond = [] {return false;}) const;

        plugins::RobotUnitComponentPlugin& getRobotUnitComponentPlugin();
    private:
        armarx::plugins::RobotUnitComponentPlugin* plugin = nullptr;
    };

}


namespace armarx
{
    namespace plugins
    {
        // Legacy typedef.
        using RobotUnitComponentPluginUser = armarx::RobotUnitComponentPluginUser;
    }
}
