/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>

#include <VirtualRobot/MathTools.h>

#include <RobotAPI/interface/units/LaserScannerUnit.h>
#include <RobotAPI/libraries/armem/core/Time.h>

namespace armarx::armem::vision
{

    struct SensorHeader
    {
        std::string agent;
        std::string frame;
        armem::Time timestamp;
    };

    struct LaserScanStamped
    {
        SensorHeader header;
        LaserScan data;
    };

    // template<typename _ValueT = float>
    struct OccupancyGrid
    {
        float resolution; // [mm]

        std::string frame;
        Eigen::Affine3f pose;

        // using ValueType = _ValueT;
        using CellType = float;
        using Grid = Eigen::Array<CellType, Eigen::Dynamic, Eigen::Dynamic>;

        Grid grid;
    };

    struct Ellipsoid
    {
        Eigen::Isometry3f pose = Eigen::Isometry3f::Identity();

        Eigen::Vector2f radii = Eigen::Vector2f::Zero();
    };

    struct Circle
    {
        Eigen::Vector2f center = Eigen::Vector2f::Zero();
        float radius = 0.F;
    };

    struct LaserScannerFeature
    {
        using Points = std::vector<Eigen::Vector2f>;
        using Chain = Points;

        Points convexHull;

        Circle circle;
        Ellipsoid ellipsoid;

        Chain chain;

        Points points;
    };

    struct LaserScannerFeatures
    {
        // TODO(fabian.reister): framed pose
        std::string frame;
        Eigen::Isometry3f frameGlobalPose;

        std::vector<LaserScannerFeature> features;


        // std::vector<Ellipsoid> linesAsEllipsoids(float axisLength) const;
    };


} // namespace armarx::armem::vision
