#pragma once

#include <Eigen/Core>

namespace armarx::armem::vision
{
    struct OccupancyGrid;
}

namespace armarx
{
    using armarx::armem::vision::OccupancyGrid;

    namespace detail
    {
        struct OccupancyGridHelperParams
        {
            float freespaceThreshold = 0.45F;
            float occupiedThreshold = 0.55F;
        };
    }

    class OccupancyGridHelper
    {
    public:
        using Params = detail::OccupancyGridHelperParams;

        OccupancyGridHelper(const OccupancyGrid& occupancyGrid, const Params& params = Params());

        using BinaryArray = Eigen::Array<bool, Eigen::Dynamic, Eigen::Dynamic>;

        BinaryArray knownCells() const;
        BinaryArray freespace() const;
        BinaryArray obstacles() const;

    private:
        const OccupancyGrid& occupancyGrid;
        const Params params;
    };
} // namespace armarx
