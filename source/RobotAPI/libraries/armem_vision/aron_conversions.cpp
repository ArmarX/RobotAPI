#include "aron_conversions.h"

#include <algorithm>
#include <cstdint>
#include <iterator>

#include <RobotAPI/interface/units/LaserScannerUnit.h>
#include <RobotAPI/libraries/armem/core/aron_conversions.h>
#include <RobotAPI/libraries/armem_vision/aron/LaserScan.aron.generated.h>
#include <RobotAPI/libraries/armem_vision/aron/LaserScannerFeatures.aron.generated.h>
#include <RobotAPI/libraries/aron/common/aron_conversions.h>
#include <RobotAPI/libraries/aron/converter/common/Converter.h>
#include <RobotAPI/libraries/aron/core/data/variant/complex/NDArray.h>

#include "types.h"


namespace armarx::armem::vision
{

    /************ fromAron ************/
    SensorHeader
    fromAron(const arondto::SensorHeader& aronSensorHeader)
    {
        return {.agent = aronSensorHeader.agent,
                .frame = aronSensorHeader.frame,
                .timestamp = aron::fromAron<Time>(aronSensorHeader.timestamp)};
    }

    void
    fromAron(const arondto::LaserScanStamped& aronLaserScan, LaserScanStamped& laserScan)
    {
        laserScan.header = fromAron(aronLaserScan.header);
        // laserScan.data = fromAron(aronLaserScan.data);
    }

    void
    fromAron(const arondto::LaserScanStamped& aronLaserScan,
             LaserScan& laserScan,
             std::int64_t& timestamp,
             std::string& frame,
             std::string& agentName)
    {
        const auto header = fromAron(aronLaserScan.header);

        // laserScan = fromAron(aronLaserScan.data);

        timestamp = header.timestamp.toMicroSecondsSinceEpoch();
        frame = header.frame;
        agentName = header.agent;
    }

    /************ toAron ************/

    // auto toAron(const LaserScan& laserScan, aron::LaserScan& aronLaserScan)
    // {
    //     aronLaserScan.scan = toAron(laserScan);
    // }

    int64_t
    toAron(const armem::Time& timestamp)
    {
        return timestamp.toMicroSecondsSinceEpoch();
    }

    arondto::SensorHeader
    toAron(const SensorHeader& sensorHeader)
    {
        arondto::SensorHeader aronSensorHeader;

        aron::toAron(aronSensorHeader.agent, sensorHeader.agent);
        aron::toAron(aronSensorHeader.frame, sensorHeader.frame);
        aron::toAron(aronSensorHeader.timestamp, sensorHeader.timestamp);

        return aronSensorHeader;
    }

    void
    toAron(const LaserScanStamped& laserScanStamped,
           arondto::LaserScanStamped& aronLaserScanStamped)
    {
        aronLaserScanStamped.header = toAron(laserScanStamped.header);
        // toAron(laserScanStamped.data, aronLaserScanStamped.data);
    }

    void
    toAron(const LaserScan& laserScan,
           const armem::Time& timestamp,
           const std::string& frame,
           const std::string& agentName,
           arondto::LaserScanStamped& aronLaserScanStamped)
    {
        const SensorHeader header{.agent = agentName, .frame = frame, .timestamp = timestamp};

        const LaserScanStamped laserScanStamped{.header = header, .data = laserScan};

        toAron(laserScanStamped, aronLaserScanStamped);
    }

    void
    toAron(arondto::OccupancyGrid& dto, const OccupancyGrid& bo)
    {
        aron::toAron(dto.frame, bo.frame);
        aron::toAron(dto.pose, bo.pose);
        aron::toAron(dto.resolution, bo.resolution);
        // bo.grid is NdArray -> need special handling.
    }

    void
    fromAron(const arondto::OccupancyGrid& dto, OccupancyGrid& bo)
    {
        aron::fromAron(dto.frame, bo.frame);
        aron::fromAron(dto.pose, bo.pose);
        aron::fromAron(dto.resolution, bo.resolution);
        // bo.grid is NdArray -> need special handling.
    }

    // LaserScannerFeatures


    void
    toAron(arondto::Circle& dto, const Circle& bo)
    {
        dto.center = bo.center;
        dto.radius = bo.radius;
    }

    void
    toAron(arondto::Ellipsoid& dto, const Ellipsoid& bo)
    {
        dto.globalPose = bo.pose.matrix();
        dto.radii = bo.radii;
    }

    void
    toAron(arondto::LaserScannerFeature& dto, const LaserScannerFeature& bo)
    {
        toAron(dto.circle, bo.circle);
        dto.convexHull = bo.convexHull;
        toAron(dto.ellipsoid, bo.ellipsoid);
        dto.points = bo.points;
    }

    void
    toAron(arondto::LaserScannerFeatures& dto, const LaserScannerFeatures& bo)
    {
        aron::toAron(dto.frame, bo.frame);
        aron::toAron(dto.frameGlobalPose, bo.frameGlobalPose);
        aron::toAron(dto.features, bo.features);
    }

    void
    fromAron(const arondto::Circle& dto, Circle& bo)
    {
        bo.center = dto.center;
        bo.radius = dto.radius;
    }
    void
    fromAron(const arondto::Ellipsoid& dto, Ellipsoid& bo)
    {
        bo.pose = dto.globalPose;
        bo.radii = dto.radii;
    }

    void
    fromAron(const arondto::LaserScannerFeature& dto, LaserScannerFeature& bo)
    {
        bo.convexHull = dto.convexHull;
        fromAron(dto.circle, bo.circle);
        fromAron(dto.ellipsoid, bo.ellipsoid);

        // bo.chain = dto.chain;
        bo.points = dto.points;
    }

    void
    fromAron(const arondto::LaserScannerFeatures& dto, LaserScannerFeatures& bo)
    {
        aron::fromAron(dto.frame, bo.frame);
        aron::fromAron(dto.frameGlobalPose, bo.frameGlobalPose);
        aron::fromAron(dto.features, bo.features);
    }

} // namespace armarx::armem::vision
