/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/units/LaserScannerUnit.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem_vision/aron/LaserScannerFeatures.aron.generated.h>
#include <RobotAPI/libraries/armem_vision/aron/OccupancyGrid.aron.generated.h>
#include <RobotAPI/libraries/armem_vision/types.h>
#include <RobotAPI/libraries/aron/converter/common/VectorConverter.h>
#include <RobotAPI/libraries/aron/converter/eigen/EigenConverter.h>
#include <RobotAPI/libraries/aron/core/data/variant/complex/NDArray.h>

namespace armarx::armem::vision
{

    namespace arondto
    {
        struct LaserScanStamped;
    } // namespace arondto

    // struct LaserScan;
    struct LaserScanStamped;

    void fromAron(const arondto::LaserScanStamped& aronLaserScan,
                  LaserScan& laserScan,
                  std::int64_t& timestamp,
                  std::string& frame,
                  std::string& agentName);

    template <typename T>
    auto
    fromAron(const aron::data::NDArrayPtr& navigator)
    {
        return aron::converter::AronVectorConverter::ConvertToVector<T>(navigator);
    }

    void fromAron(const arondto::LaserScanStamped& aronLaserScan, LaserScanStamped& laserScan);

    void toAron(const LaserScan& laserScan,
                const armem::Time& timestamp,
                const std::string& frame,
                const std::string& agentName,
                arondto::LaserScanStamped& aronLaserScan);

    inline aron::data::NDArrayPtr
    toAron(const LaserScan& laserScan)
    {
        using aron::converter::AronVectorConverter;
        return AronVectorConverter::ConvertFromVector(laserScan);
    }

    // OccupancyGrid
    void toAron(arondto::OccupancyGrid& dto, const OccupancyGrid& bo);
    void fromAron(const arondto::OccupancyGrid& dto, OccupancyGrid& bo);

    inline aron::data::NDArrayPtr
    toAron(const OccupancyGrid::Grid& grid)
    {
        return aron::converter::AronEigenConverter::ConvertFromArray(grid);
    }

    // LaserScannerFeatures
    void toAron(arondto::LaserScannerFeatures& dto, const LaserScannerFeatures& bo);
    void fromAron(const arondto::LaserScannerFeatures& dto, LaserScannerFeatures& bo);

} // namespace armarx::armem::vision
