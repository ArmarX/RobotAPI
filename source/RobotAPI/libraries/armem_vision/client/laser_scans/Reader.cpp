#include "Reader.h"

// STD / STL
#include <algorithm>
#include <cstring>
#include <map>
#include <optional>
#include <ostream>
#include <utility>
#include <vector>

#include <type_traits>

// ICE
#include <IceUtil/Handle.h>
#include <IceUtil/Time.h>

// Simox
#include <SimoxUtility/algorithm/get_map_keys_values.h>

// ArmarXCore
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/LogSender.h>
#include <ArmarXCore/core/logging/Logging.h>

// RobotAPI Interfaces
#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>
#include <RobotAPI/interface/armem/server/ReadingMemoryInterface.h>
#include <RobotAPI/interface/units/LaserScannerUnit.h>

// RobotAPI Aron
#include <RobotAPI/libraries/aron/core/Exception.h>
#include <RobotAPI/libraries/aron/core/data/variant/complex/NDArray.h>

// RobotAPI Armem
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/util/util.h>

#include <RobotAPI/libraries/armem/client/Query.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/client/query/selectors.h>

#include <RobotAPI/libraries/armem_vision/aron/LaserScan.aron.generated.h>
#include <RobotAPI/libraries/armem_vision/aron_conversions.h>
#include <RobotAPI/libraries/armem_vision/types.h>


namespace armarx::armem::vision::laser_scans::client
{

    Reader::Reader(armem::client::MemoryNameSystem& memoryNameSystem) :
        memoryNameSystem(memoryNameSystem)
    {
    }
    Reader::~Reader() = default;


    void
    Reader::registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def)
    {
        ARMARX_DEBUG << "TransformReader: registerPropertyDefinitions";
        registerPropertyDefinitions(def);

        const std::string prefix = propertyPrefix;

        def->optional(properties.coreSegmentName,
                      prefix + "CoreSegment",
                      "Name of the mapping memory core segment to use.");

        def->optional(properties.memoryName, prefix + "MemoryName");
    }

    void Reader::connect()
    {
        // Wait for the memory to become available and add it as dependency.
        ARMARX_IMPORTANT << "MappingDataReader: Waiting for memory '"
                         << properties.memoryName << "' ...";
        try
        {
            memoryReader = memoryNameSystem.useReader(MemoryID().withMemoryName(properties.memoryName));
            ARMARX_IMPORTANT << "MappingDataReader: Connected to memory '" << properties.memoryName << "'";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }
    }

    armarx::armem::client::query::Builder
    Reader::buildQuery(const Query& query) const
    {
        armarx::armem::client::query::Builder qb;

        ARMARX_INFO << "Query for agent: " << query.agent
                    << " memory name: " << properties.memoryName;

        if (query.sensorList.empty()) // all sensors
        {
            // clang-format off
            qb
            .coreSegments().withName(properties.memoryName)
            .providerSegments().withName(query.agent)
            .entities().all()
            .snapshots().timeRange(query.timeRange.min, query.timeRange.max);
            // clang-format on
        }
        else
        {
            // clang-format off
            qb
            .coreSegments().withName(properties.memoryName)
            .providerSegments().withName(query.agent)
            .entities().withNames(query.sensorList)
            .snapshots().timeRange(query.timeRange.min, query.timeRange.max);
            // clang-format on
        }

        return qb;
    }

    std::vector<LaserScanStamped>
    asLaserScans(const wm::ProviderSegment& providerSegment)
    {
        std::vector<LaserScanStamped> outV;
        if (providerSegment.empty())
        {
            ARMARX_WARNING << "No entities!";
        }

        const auto convert =
            [](const auto& aronLaserScanStamped,
               const wm::EntityInstance & ei) -> LaserScanStamped
        {
            LaserScanStamped laserScanStamped;
            fromAron(aronLaserScanStamped, laserScanStamped);

            const auto ndArrayNavigator =
            aron::data::NDArray::DynamicCast(
                ei.data()->getElement("scan"));

            ARMARX_CHECK_NOT_NULL(ndArrayNavigator);

            laserScanStamped.data = fromAron<LaserScanStep>(ndArrayNavigator);
            ARMARX_IMPORTANT << "4";

            return laserScanStamped;
        };

        // loop over all entities and their snapshots
        providerSegment.forEachEntity([&outV, &convert](const wm::Entity & entity)
        {
            // If we don't need this warning, we could directly iterate over the snapshots.
            if (entity.empty())
            {
                ARMARX_WARNING << "Empty history for " << entity.id();
            }
            ARMARX_DEBUG << "History size: " << entity.size();

            entity.forEachInstance([&outV, &convert](const wm::EntityInstance & entityInstance)
            {
                if (const auto o = tryCast<arondto::LaserScanStamped>(entityInstance))
                {
                    outV.push_back(convert(*o, entityInstance));
                }
                return true;
            });
            return true;
        });

        return outV;
    }

    Reader::Result Reader::queryData(const Query& query) const
    {
        const auto qb = buildQuery(query);

        ARMARX_DEBUG << "[MappingDataReader] query ... ";

        const armem::client::QueryResult qResult =
            memoryReader.query(qb.buildQueryInput());

        ARMARX_DEBUG << "[MappingDataReader] result: " << qResult;

        if (not qResult.success)
        {
            ARMARX_WARNING << "Failed to query data from memory: "
                           << qResult.errorMessage;
            return {.laserScans   = {},
                    .sensors      = {},
                    .status       = Result::Status::Error,
                    .errorMessage = qResult.errorMessage};
        }

        // now create result from memory
        const wm::ProviderSegment& providerSegment =
            qResult.memory.getCoreSegment(properties.memoryName).getProviderSegment(query.agent);

        const auto laserScans = asLaserScans(providerSegment);
        std::vector<std::string> sensors;
        providerSegment.forEachEntity([&sensors](const wm::Entity & entity)
        {
            sensors.push_back(entity.name());
            return true;
        });

        return {.laserScans   = laserScans,
                .sensors      = sensors,
                .status       = Result::Status::Success,
                .errorMessage = ""};
    }

} // namespace armarx::armem::vision::laser_scans::client
