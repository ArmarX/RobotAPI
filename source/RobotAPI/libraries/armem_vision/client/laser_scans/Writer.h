/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <RobotAPI/interface/units/LaserScannerUnit.h>
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>


namespace armarx::armem::vision::laser_scans::client
{

    /**
    * @defgroup Component-ExampleClient ExampleClient
    * @ingroup RobotAPI-Components
    * A description of the component ExampleClient.
    *
    * @class ExampleClient
    * @ingroup Component-ExampleClient
    * @brief Brief description of class ExampleClient.
    *
    * Detailed description of class ExampleClient.
    */
    class Writer
    {
    public:

        Writer(armem::client::MemoryNameSystem& memoryNameSystem);
        virtual ~Writer();


        void connect();

        // MappingDataWriterInterface
        /// to be called in Component::onConnectComponent
        // void connect() override;

        /// to be called in Component::addPropertyDefinitions
        void registerPropertyDefinitions(
            armarx::PropertyDefinitionsPtr& def) /*override*/;

        bool storeSensorData(const LaserScan& laserScan,
                             const std::string& frame,
                             const std::string& agentName,
                             const std::int64_t& timestamp);

    private:
        armem::client::MemoryNameSystem& memoryNameSystem;
        armem::client::Writer memoryWriter;

        // Properties
        struct Properties
        {
            std::string memoryName      = "Vision";
            std::string coreSegmentName = "LaserScans";
        } properties;

        std::mutex memoryWriterMutex;

        const std::string propertyPrefix = "mem.vision.laser_scans.";

    };

} // namespace armarx::armem::vision::laser_scans::client
