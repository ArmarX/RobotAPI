#include "Writer.h"

#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem_vision/aron_conversions.h>
#include <RobotAPI/libraries/armem_vision/aron/LaserScan.aron.generated.h>


namespace armarx::armem::vision::laser_scans::client
{

    Writer::Writer(armem::client::MemoryNameSystem& memoryNameSystem)
        : memoryNameSystem(memoryNameSystem) {}
    Writer::~Writer() = default;


    void
    Writer::registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def)
    {
        ARMARX_DEBUG << "LaserScansWriter: registerPropertyDefinitions";

        const std::string prefix = propertyPrefix;

        def->optional(properties.coreSegmentName,
                      prefix + "CoreSegment",
                      "Name of the mapping memory core segment to use.");

        def->optional(properties.memoryName, prefix + "MemoryName");
    }

    void Writer::connect()
    {
        // Wait for the memory to become available and add it as dependency.
        ARMARX_IMPORTANT << "LaserScansWriter: Waiting for memory '"
                         << properties.memoryName << "' ...";
        try
        {
            memoryWriter = memoryNameSystem.useWriter(MemoryID().withMemoryName(properties.memoryName));
            ARMARX_IMPORTANT << "MappingDataWriter: Connected to memory '" << properties.memoryName << "'";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }

        ARMARX_IMPORTANT << "LaserScansWriter: Connected to memory '"
                         << properties.memoryName;
    }

    bool Writer::storeSensorData(const LaserScan& laserScan,
                                 const std::string& frame,
                                 const std::string& agentName,
                                 const std::int64_t& timestamp)
    {
        std::lock_guard g{memoryWriterMutex};

        const auto result =
            memoryWriter.addSegment(properties.memoryName, agentName);

        if (not result.success)
        {
            ARMARX_ERROR << result.errorMessage;

            // TODO(fabian.reister): message
            return false;
        }

        const auto iceTimestamp = Time(Duration::MicroSeconds(timestamp));

        const auto providerId = armem::MemoryID(result.segmentID);
        const auto entityID =
            providerId.withEntityName(frame).withTimestamp(iceTimestamp);

        armem::EntityUpdate update;
        update.entityID = entityID;

        arondto::LaserScanStamped aronSensorData;
        // currently only sets the header
        toAron(laserScan, iceTimestamp, frame, agentName, aronSensorData);

        auto dict = aronSensorData.toAron();
        dict->addElement("scan", toAron(laserScan));

        update.instancesData = {dict};
        update.timeCreated   = iceTimestamp;

        ARMARX_DEBUG << "Committing " << update << " at time " << iceTimestamp;
        armem::EntityUpdateResult updateResult = memoryWriter.commit(update);

        ARMARX_DEBUG << updateResult;

        if (not updateResult.success)
        {
            ARMARX_ERROR << updateResult.errorMessage;
        }

        return updateResult.success;
    }

} // namespace armarx::armem::vision::laser_scans::client
