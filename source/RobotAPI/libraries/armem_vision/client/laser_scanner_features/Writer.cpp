#include "Writer.h"

#include "RobotAPI/libraries/armem_vision/constants.h"
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem_vision/aron/LaserScan.aron.generated.h>
#include <RobotAPI/libraries/armem_vision/aron/LaserScannerFeatures.aron.generated.h>
#include <RobotAPI/libraries/armem_vision/aron_conversions.h>


namespace armarx::armem::vision::laser_scanner_features::client
{

    Writer::Writer(armem::client::MemoryNameSystem& memoryNameSystem) :
        memoryNameSystem(memoryNameSystem)
    {
    }
    Writer::~Writer() = default;


    void
    Writer::registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def)
    {
        ARMARX_DEBUG << "LaserScansWriter: registerPropertyDefinitions";

        const std::string prefix = propertyPrefix;

        // def->optional(properties.coreSegmentName,
        //               prefix + "CoreSegment",
        //               "Name of the mapping memory core segment to use.");

        // def->optional(properties.memoryName, prefix + "MemoryName");
    }

    void
    Writer::connect()
    {
        // Wait for the memory to become available and add it as dependency.
        ARMARX_IMPORTANT << "LaserScansWriter: Waiting for memory '" << constants::memoryName
                         << "' ...";
        try
        {
            memoryWriter =
                memoryNameSystem.useWriter(MemoryID().withMemoryName(constants::memoryName));
            ARMARX_IMPORTANT << "MappingDataWriter: Connected to memory '" << constants::memoryName
                             << "'";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }

        ARMARX_IMPORTANT << "LaserScansWriter: Connected to memory '" << constants::memoryName;
    }

    bool
    Writer::store(const LaserScannerFeatures& features,
                  const std::string& providerName,
                  const Time& timestamp)
    {
        std::lock_guard g{memoryWriterMutex};

        // const auto result = memoryWriter.addSegment(constants::memoryName,
        //                                             constants::laserScannerFeaturesCoreSegment);

        // if (not result.success)
        // {
        //     ARMARX_ERROR << result.errorMessage;

        //     // TODO(fabian.reister): message
        //     return false;
        // }

        const auto entityID = armem::MemoryID()
                                    .withMemoryName(constants::memoryName)
                                    .withCoreSegmentName(constants::laserScannerFeaturesCoreSegment)
                                    .withProviderSegmentName(providerName)
                                    .withEntityName(features.frame)
                                    .withTimestamp(timestamp);

        ARMARX_VERBOSE << "Memory id is " << entityID.str();

        armem::EntityUpdate update;
        update.entityID = entityID;

        ARMARX_TRACE;

        arondto::LaserScannerFeatures dto;
        toAron(dto, features);

        ARMARX_TRACE;

        update.instancesData = {dto.toAron()};
        update.timeCreated = timestamp;

        ARMARX_DEBUG << "Committing " << update << " at time " << timestamp;

        ARMARX_TRACE;
        armem::EntityUpdateResult updateResult = memoryWriter.commit(update);

        ARMARX_DEBUG << updateResult;

        if (not updateResult.success)
        {
            ARMARX_ERROR << updateResult.errorMessage;
        }

        return updateResult.success;
    }

} // namespace armarx::armem::vision::laser_scanner_features::client
