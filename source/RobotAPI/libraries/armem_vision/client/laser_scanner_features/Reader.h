/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <string>
#include <vector>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/time/DateTime.h>

#include <RobotAPI/libraries/armem/client.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem_vision/types.h>


namespace armarx
{
    class ManagedIceObject;
}

namespace armarx::armem::vision::laser_scanner_features::client
{


    /**
    * @defgroup Component-ExampleClient ExampleClient
    * @ingroup RobotAPI-Components
    * A description of the component ExampleClient.
    *
    * @class ExampleClient
    * @ingroup Component-ExampleClient
    * @brief Brief description of class ExampleClient.
    *
    * Detailed description of class ExampleClient.
    */
    class Reader
    {
    public:
        Reader(armem::client::MemoryNameSystem& memoryNameSystem);
        virtual ~Reader();

        void connect();

        struct Query
        {
            std::string providerName;

            std::string name; // sensor name

            armarx::core::time::DateTime timestamp;

            // std::vector<std::string> sensorList;
        };

        struct Result
        {
            std::vector<LaserScannerFeatures> features;

            // std::vector<std::string> sensors;

            enum Status
            {
                Error,
                Success
            } status;

            std::string errorMessage;
        };

        Result queryData(const Query& query) const;

        void registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def);


    protected:
        armarx::armem::client::query::Builder buildQuery(const Query& query) const;

    private:
        armem::client::MemoryNameSystem& memoryNameSystem;
        armem::client::Reader memoryReader;

        // Properties
        struct Properties
        {
            std::string memoryName = "Vision";
            std::string coreSegmentName = "LaserScannerFeatures";
        } properties;

        const std::string propertyPrefix = "mem.vision.laser_scanner_features.";
    };

} // namespace armarx::armem::vision::laser_scanner_features::client
