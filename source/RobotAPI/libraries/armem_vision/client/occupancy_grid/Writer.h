/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <RobotAPI/libraries/armem/client/util/SimpleWriterBase.h>
#include <RobotAPI/libraries/armem_vision/types.h>

namespace armarx::armem::vision::occupancy_grid::client
{

    /**
    * @defgroup Component-ExampleClient ExampleClient
    * @ingroup RobotAPI-Components
    * A description of the component ExampleClient.
    *
    * @class ExampleClient
    * @ingroup Component-ExampleClient
    * @brief Brief description of class ExampleClient.
    *
    * Detailed description of class ExampleClient.
    */
    class Writer : virtual public armarx::armem::client::util::SimpleWriterBase
    {
    public:
        using armarx::armem::client::util::SimpleWriterBase::SimpleWriterBase;
        ~Writer() override;

        bool store(const OccupancyGrid& grid,
                   const std::string& frame,
                   const std::string& providerName,
                   const std::int64_t& timestamp);

    protected:
        std::string propertyPrefix() const override;
        Properties defaultProperties() const override;
    };



} // namespace armarx::armem::vision::occupancy_grid::client
