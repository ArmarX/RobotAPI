/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <RobotAPI/libraries/armem/client/util/SimpleReaderBase.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem_vision/types.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>

namespace armarx::armem::vision::occupancy_grid::client
{

    class Reader : virtual public armarx::armem::client::util::SimpleReaderBase
    {
    public:
        using armarx::armem::client::util::SimpleReaderBase::SimpleReaderBase;
        ~Reader() override;

        struct Query
        {
            std::string providerName;
            armem::Time timestamp;
        };

        struct Result
        {
            std::optional<OccupancyGrid> occupancyGrid = std::nullopt;

            enum Status
            {
                Success,
                NoData,
                Error
            } status;

            std::string errorMessage = "";

            operator bool() const noexcept
            {
                return status == Status::Success;
            }
        };

        Result query(const Query& query) const;

        ::armarx::armem::client::query::Builder buildQuery(const Query& query) const;

    protected:
        std::string propertyPrefix() const override;
        Properties defaultProperties() const override;
    };

} // namespace armarx::armem::vision::occupancy_grid::client
