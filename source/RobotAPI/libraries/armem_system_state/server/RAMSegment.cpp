// BaseClass
#include "RAMSegment.h"

// ArmarX
#include <RobotAPI/libraries/PriorKnowledge/motions/MotionFinder.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/application/properties/ProxyPropertyDefinition.h>

#include <RobotAPI/libraries/aron/core/data/variant/All.h>

namespace armarx::armem::server::systemstate::segment
{
    LightweightRamMonitorProviderSegment::LightweightRamMonitorProviderSegment(armem::server::MemoryToIceAdapter& iceMemory) :
        Base(iceMemory, "LightweightSystemMonitor", "MemoryUsage", nullptr, nullptr, 1000)
    {

    }

    void LightweightRamMonitorProviderSegment::defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix)
    {
        defs->optional(pollFrequencyHz, prefix + "pollFrequencyHz", "The poll frequency in Hz");
    }

    void LightweightRamMonitorProviderSegment::init()
    {
        Base::init();

        pollFrequencyHz = std::clamp(pollFrequencyHz, 0.1f, 100.f);

        usleep(200 * 1000.f); // sleep for 100ms to ensure the monitor is ready

        runningTask = new PeriodicTask<LightweightRamMonitorProviderSegment>(this, &LightweightRamMonitorProviderSegment::loop, (1000.f / pollFrequencyHz));
        runningTask->start();
    }

    void LightweightRamMonitorProviderSegment::loop()
    {
        ARMARX_CHECK_NOT_NULL(segmentPtr);

        MemoryID providerId = segmentPtr->id();

        long total = memoryMonitoring->getTotalMemoryInKB();
        double usage = memoryMonitoring->getCurrentMemUsageInPercent();

        auto data = std::make_shared<aron::data::Dict>();
        data->addElement("total", std::make_shared<aron::data::Long>(total)); // TODO in seperate segment?
        data->addElement("load", std::make_shared<aron::data::Double>(usage));

        ARMARX_DEBUG << "RAM Usage is: " << usage << "% (of " << total << "KB max)";

        EntityUpdate update;
        update.entityID = providerId.withEntityName("CurrentMemoryLoad");
        update.confidence = 1.0;
        update.timeCreated = armem::Time::Now();
        update.instancesData = { data };

        segmentPtr->update(update);
    }
}
