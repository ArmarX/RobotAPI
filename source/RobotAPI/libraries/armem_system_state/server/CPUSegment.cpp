// BaseClass
#include "CPUSegment.h"

// ArmarX
#include <RobotAPI/libraries/PriorKnowledge/motions/MotionFinder.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/application/properties/ProxyPropertyDefinition.h>

#include <RobotAPI/libraries/aron/core/data/variant/All.h>

namespace armarx::armem::server::systemstate::segment
{
    LightweightCpuMonitorProviderSegment::LightweightCpuMonitorProviderSegment(armem::server::MemoryToIceAdapter& iceMemory) :
        Base(iceMemory, "LightweightSystemMonitor", "CPUUsage", nullptr, nullptr, 1000)
    {

    }

    void LightweightCpuMonitorProviderSegment::defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix)
    {
        defs->optional(pollFrequencyHz, prefix + "pollFrequencyHz", "The poll frequency in Hz");
    }

    void LightweightCpuMonitorProviderSegment::init()
    {
        Base::init();

        pollFrequencyHz = std::clamp(pollFrequencyHz, 0.1f, 100.f);
        cpuMonitoring->initCpuUsage();

        float sleepTime = (1000.f / pollFrequencyHz);

        runningTask = new PeriodicTask<LightweightCpuMonitorProviderSegment>(this, &LightweightCpuMonitorProviderSegment::loop, sleepTime);
        runningTask->start();
    }

    void LightweightCpuMonitorProviderSegment::loop()
    {
        ARMARX_CHECK_NOT_NULL(segmentPtr);

        MemoryID providerId = segmentPtr->id();

        double usage = cpuMonitoring->getCurrentCpuUsage();
        std::string model = cpuMonitoring->getCPUName();
        std::vector<double> cores = cpuMonitoring->getCurrentMultiCoreUsage();

        auto data = std::make_shared<aron::data::Dict>();
        data->addElement("model", std::make_shared<aron::data::String>(model)); // TODO in seperate segment?
        data->addElement("load", std::make_shared<aron::data::Double>(usage));

        auto list = std::make_shared<aron::data::List>();
        for (const auto core : cores)
        {
            list->addElement(std::make_shared<aron::data::Double>(core));
        }
        data->addElement("coresLoad", list);

        ARMARX_DEBUG << "Current CPU load: " << usage << " (of model: " << model << ")";

        EntityUpdate update;
        update.entityID = providerId.withEntityName("CurrentCpuLoad");
        update.confidence = 1.0;
        update.timeCreated = armem::Time::Now();
        update.instancesData = { data };

        segmentPtr->update(update);
    }
}
