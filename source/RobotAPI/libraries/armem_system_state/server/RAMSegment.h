#pragma once

// STD/STL
#include <iostream>
#include <csignal>
#include <memory>
#include <atomic>
#include <thread>

// System Monitor
#include "LightweightSystemMonitor/linux_memoryload.hpp"
#include "LightweightSystemMonitor/linux_cpuload.hpp"
#include "LightweightSystemMonitor/linux_process_load.hpp"
#include "LightweightSystemMonitor/linux_networkload.hpp"
#include "LightweightSystemMonitor/linux_systemutil.hpp"
#include "LightweightSystemMonitor/util/record_value.hpp"
#include "LightweightSystemMonitor/util/timer.hpp"

// BaseClass
#include <RobotAPI/libraries/armem/server/segment/SpecializedProviderSegment.h>

// ArmarX
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

namespace armarx::armem::server::systemstate::segment
{
    class LightweightRamMonitorProviderSegment : public armem::server::segment::SpecializedProviderSegment
    {
        using Base = armem::server::segment::SpecializedProviderSegment;

    public:
        LightweightRamMonitorProviderSegment(armem::server::MemoryToIceAdapter& iceMemory);

        void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "") override;
        void init() override;

    private:
        void loop();

    private:
        float pollFrequencyHz = 10;
        std::unique_ptr<memoryLoad> memoryMonitoring = std::make_unique<memoryLoad>();

        armarx::PeriodicTask<LightweightRamMonitorProviderSegment>::pointer_type runningTask;
    };
}
