set(LIB_NAME       armem_system_state)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

armarx_add_library(
    LIBS
        ArmarXCoreInterfaces
        ArmarXCore
        ArmarXCoreObservers

        RobotAPI::Core
        RobotAPI::armem_server
        
    SOURCES
        ./server/CPUSegment.cpp
        ./server/RAMSegment.cpp

        ./server/LightweightSystemMonitor/linux_memoryload.cpp
        ./server/LightweightSystemMonitor/linux_networkload.cpp
        ./server/LightweightSystemMonitor/linux_systemutil.cpp
        ./server/LightweightSystemMonitor/linux_cpuload.cpp
        ./server/LightweightSystemMonitor/linux_process_load.cpp
        ./server/LightweightSystemMonitor/util/timer.cpp
    HEADERS
        ./server/CPUSegment.h
        ./server/RAMSegment.h

        ./server/LightweightSystemMonitor/linux_memoryload.hpp
        ./server/LightweightSystemMonitor/linux_networkload.hpp
        ./server/LightweightSystemMonitor/linux_systemutil.hpp
        ./server/LightweightSystemMonitor/linux_cpuload.hpp
        ./server/LightweightSystemMonitor/linux_process_load.hpp
        ./server/LightweightSystemMonitor/util/timer.hpp
)
