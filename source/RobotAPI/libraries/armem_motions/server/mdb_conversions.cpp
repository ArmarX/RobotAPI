// Header
#include "mdb_conversions.h"

#include <SimoxUtility/json/json.hpp>


namespace armarx::armem::server::motions::mdb::conversion
{
    std::optional<armarx::motion::mdb::arondto::MDBReference> createFromFile(const std::filesystem::path& pathToInfoJson)
    {
        if (std::filesystem::exists(pathToInfoJson) && std::filesystem::is_regular_file(pathToInfoJson))
        {
            std::ifstream ifs(pathToInfoJson);
            std::string file_content((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));

            armarx::motion::mdb::arondto::MDBReference motionReference;
            auto json = nlohmann::json::parse(file_content);

            motionReference.id = json["id"];
            motionReference.associatedInstitution = json["associatedInstitution"];
            motionReference.associatedProject = json["associatedProject"];
            motionReference.comment = json["comment"];
            motionReference.createdDate = IceUtil::Time::seconds(json["createdDate"]);
            motionReference.createdUser = json["createdUser"];
            motionReference.modifiedDate = IceUtil::Time::seconds(json["modifiedDate"]);
            motionReference.modifiedUser = json["modifiedUser"];
            motionReference.date = json["date"];
            for (auto it = json["attachedFiles"].begin(); it != json["attachedFiles"].end(); ++it)
            {
                const auto& key = it.key();
                const auto& value = it.value();

                for (const auto& attachedFile : value)
                {
                    armarx::motion::mdb::arondto::FileReference fileRef;
                    fileRef.attachedToId = attachedFile["attachedToId"];
                    fileRef.createdDate = IceUtil::Time::microSeconds(attachedFile["createdDate"]);
                    fileRef.createdUser = attachedFile["createdUser"];
                    fileRef.description = attachedFile["description"];
                    fileRef.fileName = attachedFile["fileName"];

                    motionReference.attachedFiles[key].emplace_back(fileRef);
                }
            }
            return motionReference;
        }
        else
        {
            return std::nullopt;
        }
    }
}
