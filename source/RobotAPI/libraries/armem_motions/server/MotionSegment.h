#pragma once

// STD/STL
#include <mutex>
#include <string>

// BaseClass
#include <RobotAPI/libraries/armem/server/segment/SpecializedCoreSegment.h>
#include <RobotAPI/libraries/armem/server/segment/SpecializedProviderSegment.h>

// ArmarX
#include <RobotAPI/libraries/armem_motions/aron/MDBReference.aron.generated.h>


namespace armarx::armem::server::motions::mdb::segment
{
    class MDBMotionSegment : public armem::server::segment::SpecializedProviderSegment
    {
        using Base = armem::server::segment::SpecializedProviderSegment;

    public:
        MDBMotionSegment(armem::server::MemoryToIceAdapter& iceMemory);

        virtual void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "") override;
        virtual void init() override;
        virtual void onConnect();

    private:
        int loadByMotionFinder(const std::string&);

    private:
        struct Properties
        {
            std::string motionsPackage = "PriorKnowledgeData";
            bool loadFromMotionsPackage = true;
        };
        Properties properties;
    };
}
