// STD / STL
#include <filesystem>
#include <iostream>
#include <fstream>
#include <optional>

// ArmarX
#include <RobotAPI/libraries/armem_motions/aron/MDBReference.aron.generated.h>

namespace armarx::armem::server::motions::mdb::conversion
{
    std::optional<armarx::motion::mdb::arondto::MDBReference> createFromFile(const std::filesystem::path& pathToInfoJson);
}
