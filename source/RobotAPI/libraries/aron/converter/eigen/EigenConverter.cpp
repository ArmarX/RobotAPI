/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL


// Header
#include "EigenConverter.h"

namespace armarx::aron::converter
{
    Eigen::Quaternion<float> AronEigenConverter::ConvertToQuaternionf(const data::NDArrayPtr& n)
    {
        return ConvertToQuaternion<float>(n);
    }
    Eigen::Quaternion<double> AronEigenConverter::ConvertToQuaterniond(const data::NDArrayPtr& n)
    {
        return ConvertToQuaternion<double>(n);
    }
    Eigen::Vector3f AronEigenConverter::ConvertToVector3f(const data::NDArrayPtr& n)
    {
        return ConvertToVector<float, 3>(n);
    }
    Eigen::Vector3d AronEigenConverter::ConvertToVector3d(const data::NDArrayPtr& n)
    {
        return ConvertToVector<double, 3>(n);
    }
    Eigen::Matrix4f AronEigenConverter::ConvertToMatrix4f(const data::NDArrayPtr& n)
    {
        return ConvertToMatrix<float, 4, 4>(n);
    }
    Eigen::Matrix4d AronEigenConverter::ConvertToMatrix4d(const data::NDArrayPtr& n)
    {
        return ConvertToMatrix<double, 4, 4>(n);
    }

    Eigen::Quaternion<float> AronEigenConverter::ConvertToQuaternionf(const data::NDArray& n)
    {
        return ConvertToQuaternion<float>(n);
    }
    Eigen::Quaternion<double> AronEigenConverter::ConvertToQuaterniond(const data::NDArray& n)
    {
        return ConvertToQuaternion<double>(n);
    }
    Eigen::Vector3f AronEigenConverter::ConvertToVector3f(const data::NDArray& n)
    {
        return ConvertToVector<float, 3>(n);
    }
    Eigen::Vector3d AronEigenConverter::ConvertToVector3d(const data::NDArray& n)
    {
        return ConvertToVector<double, 3>(n);
    }
    Eigen::Matrix4f AronEigenConverter::ConvertToMatrix4f(const data::NDArray& n)
    {
        return ConvertToMatrix<float, 4, 4>(n);
    }
    Eigen::Matrix4d AronEigenConverter::ConvertToMatrix4d(const data::NDArray& n)
    {
        return ConvertToMatrix<double, 4, 4>(n);
    }

    void AronEigenConverter::checkDimensions(
        const data::NDArray& nav, const std::vector<int>& expected,
        const std::string& method, const std::string& caller)
    {
        if (nav.getShape() != expected)
        {
            std::stringstream ss;
            ss << "The size of an NDArray does not match.";
            ss << "\n Expected: \t" << data::NDArray::DimensionsToString(expected);
            ss << "\n Got:      \t" << data::NDArray::DimensionsToString(nav.getShape());
            ss << "\n";
            throw error::AronException(__PRETTY_FUNCTION__, ss.str(), nav.getPath());
        }
    }
}
