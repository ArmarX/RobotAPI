set(LIB_NAME aroneigenconverter)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")

set(LIBS
    aron
    Eigen3::Eigen
)

set(LIB_FILES
    EigenConverter.cpp
)

set(LIB_HEADERS
    EigenConverter.h
)

armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

add_library(RobotAPI::aron::converter::eigen ALIAS aroneigenconverter)
