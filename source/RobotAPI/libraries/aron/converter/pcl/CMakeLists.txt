set(LIB_NAME aronpclconverter)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

find_package(PCL QUIET)
armarx_build_if(PCL_FOUND "PCL not available")

set(LIBS
    aron 
    ${PCL_COMMON_LIBRARIES}
)

set(LIB_FILES
    PCLConverter.cpp
)

set(LIB_HEADERS
    PCLConverter.h
)

armarx_add_library("${LIB_NAME}" "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")

if(PCL_FOUND)
    target_include_directories(aronpclconverter 
        SYSTEM PUBLIC
            ${PCL_INCLUDE_DIRS}
    )
endif()

add_library(RobotAPI::aron::converter::pcl ALIAS aronpclconverter)
