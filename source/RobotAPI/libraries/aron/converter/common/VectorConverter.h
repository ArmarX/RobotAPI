/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <string>
#include <numeric>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/core/data/variant/complex/NDArray.h>


namespace armarx::aron::converter
{
    class AronVectorConverter
    {
    public:
        AronVectorConverter() = delete;

        template<typename T>
        static std::vector<T> ConvertToVector(const data::NDArrayPtr& nav)
        {
            ARMARX_CHECK_NOT_NULL(nav);

            const auto& dims = nav->getShape();

            if (dims.size() != 2)
            {
                throw error::AronException(__PRETTY_FUNCTION__, "The NDArray must have two dimensions.", nav->getPath());
            }

            if (dims.at(1) != sizeof(T))
            {
                throw error::AronException(__PRETTY_FUNCTION__, "Dimension 1 of the array has to match the element size.", nav->getPath());
            }

            const int size = std::accumulate(std::begin(dims), std::end(dims), 1, std::multiplies<>());

            std::vector<T> v(dims.at(0));
            memcpy(v.data(), nav->getData(), size);

            return v;
        }


        template<typename T>
        static data::NDArrayPtr ConvertFromVector(const std::vector<T>& data)
        {
            data::NDArrayPtr ndArr(new data::NDArray);

            ndArr->setShape({static_cast<int>(data.size()), sizeof(T)});
            ndArr->setData(sizeof(T) * data.size(), reinterpret_cast <const unsigned char* >(data.data()));

            return ndArr;
        }

    };

}  // namespace armarx::aron::converter
