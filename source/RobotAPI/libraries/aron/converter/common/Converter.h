/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <algorithm>
#include <vector>

namespace armarx::aron
{

    /**
     * @brief Converter function for vector of aron elements to plain cpp type
     *
     *  You have to provide a converter function for the element with the signature
     *
     *      PlainCppType fromAron(const AronType&)
     *
     * @tparam T the aron vector element
     * @param v the vector of elements
     * @return the vector of aron elements
     */
    template <typename T>
    auto fromAron(const std::vector<T>& v) -> std::vector<decltype(fromAron(T()))>
    {
        std::vector<decltype(fromAron(T()))> r;
        r.reserve(v.size());

        std::transform(v.begin(), v.end(), std::back_inserter(r),
                       [](const T & t)
        {
            return fromAron(t);
        });

        return r;
    }


    template <typename T> auto toAron(const std::vector<T>& v)
    {
        std::vector<decltype(toAron(T()))> r;
        r.reserve(v.size());

        std::transform(v.begin(), v.end(), std::back_inserter(r),
                       [](const T & t)
        {
            return toAron(t);
        });

        return r;
    }

} // namespace armarx::aron
