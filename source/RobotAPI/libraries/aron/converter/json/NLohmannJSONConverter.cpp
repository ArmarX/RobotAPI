#include "NLohmannJSONConverter.h"

namespace armarx::aron::converter
{
    nlohmann::json AronNlohmannJSONConverter::ConvertToNlohmannJSON(const data::VariantPtr& aron)
    {
        nlohmann::json j;
        ConvertToNlohmannJSON(aron, j);
        return j;
    }

    void AronNlohmannJSONConverter::ConvertToNlohmannJSON(const aron::data::VariantPtr& aron, nlohmann::json& j)
    {
        j = aron::data::readAndWrite<data::FromVariantConverter<data::writer::NlohmannJSONWriter>>(aron);
    }

    nlohmann::json AronNlohmannJSONConverter::ConvertToNlohmannJSON(const type::VariantPtr& aron)
    {
        nlohmann::json j;
        ConvertToNlohmannJSON(aron, j);
        return j;
    }

    void AronNlohmannJSONConverter::ConvertToNlohmannJSON(const aron::type::VariantPtr& aron, nlohmann::json& j)
    {
        j = aron::type::readAndWrite<type::FromVariantConverter<type::writer::NlohmannJSONWriter>>(aron);
    }

    data::DictPtr AronNlohmannJSONConverter::ConvertFromNlohmannJSONObject(const nlohmann::json& j)
    {
        // TODO Switch case over json type and add other methods, e.g. for float, array, ...
        // TODO add check if json is object_t
        data::VariantPtr aron = std::make_shared<aron::data::Dict>();
        ConvertFromNlohmannJSON(aron, j);
        return data::Dict::DynamicCastAndCheck(aron);
    }

    type::ObjectPtr AronNlohmannJSONConverter::ConvertFromNlohmannJSONTypeObject(const nlohmann::json& j)
    {
        type::VariantPtr aron = std::make_shared<aron::type::Object>("foo"); // foo is just a placeholder
        ConvertFromNlohmannJSON(aron, j);
        return type::Object::DynamicCastAndCheck(aron);
    }

    void AronNlohmannJSONConverter::ConvertFromNlohmannJSON(aron::data::VariantPtr& a, const nlohmann::json& e, const aron::type::VariantPtr& expectedStructure)
    {
        a = aron::data::readAndWrite<data::FromNlohmannJSONConverter<data::writer::VariantWriter>>(e);
        ARMARX_CHECK_EXPRESSION(a->fullfillsType(expectedStructure));
    }

    void AronNlohmannJSONConverter::ConvertFromNlohmannJSON(aron::type::VariantPtr& a, const nlohmann::json& e)
    {
        a = aron::type::readAndWrite<type::FromNlohmannJSONConverter<type::writer::VariantWriter>>(e);
    }
}
