#pragma once

// STD/STL
#include <memory>
#include <string>
#include <numeric>

// Memory
#include <RobotAPI/libraries/aron/core/data/variant/All.h>

#include <RobotAPI/libraries/aron/core/data/converter/variant/VariantConverter.h>
#include <RobotAPI/libraries/aron/core/data/converter/nlohmannJSON/NlohmannJSONConverter.h>
#include <RobotAPI/libraries/aron/core/data/rw/reader/variant/VariantReader.h>
#include <RobotAPI/libraries/aron/core/data/rw/writer/variant/VariantWriter.h>
#include <RobotAPI/libraries/aron/core/data/rw/reader/nlohmannJSON/NlohmannJSONReader.h>
#include <RobotAPI/libraries/aron/core/data/rw/writer/nlohmannJSON/NlohmannJSONWriter.h>

#include <RobotAPI/libraries/aron/core/type/converter/variant/VariantConverter.h>
#include <RobotAPI/libraries/aron/core/type/converter/nlohmannJSON/NlohmannJSONConverter.h>
#include <RobotAPI/libraries/aron/core/type/rw/reader/variant/VariantReader.h>
#include <RobotAPI/libraries/aron/core/type/rw/writer/variant/VariantWriter.h>
#include <RobotAPI/libraries/aron/core/type/rw/reader/nlohmannJSON/NlohmannJSONReader.h>
#include <RobotAPI/libraries/aron/core/type/rw/writer/nlohmannJSON/NlohmannJSONWriter.h>

// JSON
#include <SimoxUtility/json/json.hpp>

namespace armarx::aron::converter
{
    class AronNlohmannJSONConverter
    {
    public:
        AronNlohmannJSONConverter() = delete;

        static nlohmann::json ConvertToNlohmannJSON(const data::VariantPtr&);
        static void ConvertToNlohmannJSON(const data::VariantPtr&, nlohmann::json&);

        static nlohmann::json ConvertToNlohmannJSON(const type::VariantPtr&);
        static void ConvertToNlohmannJSON(const type::VariantPtr&, nlohmann::json&);

        static data::DictPtr ConvertFromNlohmannJSONObject(const nlohmann::json&);
        static void ConvertFromNlohmannJSON(data::VariantPtr&, const nlohmann::json&, const aron::type::VariantPtr& = nullptr);

        static type::ObjectPtr ConvertFromNlohmannJSONTypeObject(const nlohmann::json& j);
        static void ConvertFromNlohmannJSON(aron::type::VariantPtr& a, const nlohmann::json& e);
    };
}
