/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL
#include <numeric>

// Header
#include "OpenCVConverter.h"

namespace armarx::aron::converter
{
    cv::Mat AronOpenCVConverter::ConvertToMat(const data::NDArrayPtr& nav)
    {
        ARMARX_CHECK_NOT_NULL(nav);

        if (nav->getShape().size() < 3)
        {
            throw error::AronException(__PRETTY_FUNCTION__, "The size of an NDArray does not match.", nav->getPath());
        }
        auto dims = nav->getShape();

        cv::Mat ret(std::vector<int>({dims.begin(), std::prev(dims.end())}), std::stoi(nav->getType()));
        auto size = std::accumulate(std::begin(dims), std::end(dims), 1, std::multiplies<int>());
        memcpy(reinterpret_cast<unsigned char*>(ret.data), nav->getData(), size);
        return ret;
    }

    data::NDArrayPtr AronOpenCVConverter::ConvertFromMat(const cv::Mat& mat)
    {
        std::vector<int> dims;
        for (int i = 0; i < mat.dims; ++i)
        {
            dims.push_back(mat.size[i]);
        }
        dims.push_back(mat.elemSize());

        auto ret = std::make_shared<data::NDArray>();
        ret->setShape(dims);
        ret->setType(std::to_string(mat.type()));
        ret->setData(std::accumulate(std::begin(dims), std::end(dims), 1, std::multiplies<int>()), reinterpret_cast<const unsigned char*>(mat.data));

        return ret;
    }
}

