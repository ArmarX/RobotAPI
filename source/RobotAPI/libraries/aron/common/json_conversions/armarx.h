#pragma once

#include <SimoxUtility/json/json.h>

#include <RobotAPI/libraries/aron/common/forward_declarations.h>


namespace armarx::arondto
{

    void to_json(nlohmann::json& j, const Names& bo);
    void from_json(const nlohmann::json& j, Names& bo);

}
