#include "armarx.h"

#include <RobotAPI/libraries/aron/common/aron/Names.aron.generated.h>


void armarx::arondto::to_json(nlohmann::json& j, const Names& bo)
{
    j["recognized"] = bo.recognized;
    j["spoken"] = bo.spoken;
}

void armarx::arondto::from_json(const nlohmann::json& j, Names& bo)
{
    j.at("recognized").get_to(bo.recognized);
    j.at("spoken").get_to(bo.spoken);
}
