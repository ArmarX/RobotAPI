#pragma once

#include "../aron_conversions/time.h"
#include <RobotAPI/libraries/aron/common/aron/time.aron.generated.h>

namespace armarx
{
    // Helper methods for code generation to convert json/aron to bo and vice versa
    namespace aron
    {
        template<class ReaderT>
        void read(ReaderT& aron_r, typename ReaderT::InputType& input, armarx::core::time::DateTime& ret)
        {
            arondto::DateTime aron;
            aron.read(aron_r, input);

            armarx::fromAron(aron, ret);
        }

        template<class WriterT>
        void write(WriterT& aron_w, const armarx::core::time::DateTime& input, typename WriterT::ReturnType& ret, const armarx::aron::Path& aron_p = armarx::aron::Path())
        {
            arondto::DateTime aron;
            armarx::toAron(aron, input);
            ret = aron.write(aron_w, aron_p);
        }



        template<class ReaderT>
        void read(ReaderT& aron_r, typename ReaderT::InputType& input, armarx::core::time::Duration& ret)
        {
            arondto::Duration aron;
            aron.read(aron_r, input);

            armarx::fromAron(aron, ret);
        }

        template<class WriterT>
        void write(WriterT& aron_w, const armarx::core::time::Duration& input, typename WriterT::ReturnType& ret, const armarx::aron::Path& aron_p = armarx::aron::Path())
        {
            arondto::Duration aron;
            armarx::toAron(aron, input);
            ret = aron.write(aron_w, aron_p);
        }
    }

}  // namespace armarx

