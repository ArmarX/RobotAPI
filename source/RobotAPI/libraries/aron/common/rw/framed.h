#pragma once

#include "../aron_conversions/framed.h"
#include <RobotAPI/libraries/aron/common/aron/framed.aron.generated.h>

namespace armarx
{
    // Helper methods for code generation to convert json/aron to bo and vice versa
    namespace aron
    {
        template<class ReaderT>
        void read(ReaderT& aron_r, typename ReaderT::InputType& input, armarx::FramedPosition& ret)
        {
            arondto::FramedPosition aron;
            aron.read(aron_r, input);

            armarx::fromAron(aron, ret);
        }

        template<class WriterT>
        void write(WriterT& aron_w, const armarx::FramedPosition& input, typename WriterT::ReturnType& ret, const armarx::aron::Path& aron_p = armarx::aron::Path())
        {
            arondto::FramedPosition aron;
            armarx::toAron(aron, input);
            ret = aron.write(aron_w, aron_p);
        }

        template<class ReaderT>
        void read(ReaderT& aron_r, typename ReaderT::InputType& input, armarx::FramedOrientation& ret)
        {
            arondto::FramedOrientation aron;
            aron.read(aron_r, input);

            armarx::fromAron(aron, ret);
        }

        template<class WriterT>
        void write(WriterT& aron_w, const armarx::FramedOrientation& input, typename WriterT::ReturnType& ret, const armarx::aron::Path& aron_p = armarx::aron::Path())
        {
            arondto::FramedOrientation aron;
            armarx::toAron(aron, input);
            ret = aron.write(aron_w, aron_p);
        }

        template<class ReaderT>
        void read(ReaderT& aron_r, typename ReaderT::InputType& input, armarx::FramedPose& ret)
        {
            arondto::FramedPose aron;
            aron.read(aron_r, input);

            armarx::fromAron(aron, ret);
        }

        template<class WriterT>
        void write(WriterT& aron_w, const armarx::FramedPose& input, typename WriterT::ReturnType& ret, const armarx::aron::Path& aron_p = armarx::aron::Path())
        {
            arondto::FramedPose aron;
            armarx::toAron(aron, input);
            ret = aron.write(aron_w, aron_p);
        }
    }

}  // namespace armarx


