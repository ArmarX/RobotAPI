/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::libraries::aron
 * @author     Philip Scherer ( ulila@student.kit.edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <vector>

#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/aron/core/aron_conversions.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>

namespace armarx::aron
{
    /**
     * Finds aron objects with a given type name prefix in aron variants.
     *
     * Construct it with the prefix you want to search for,
     * then use `visitRecursive` to run the finder over the variant to search.
     * Get a map of paths to data and type of the found objects using `getFoundObjects`.
     */
    class SubObjectFinder : public armarx::aron::data::RecursiveConstTypedVariantVisitor
    {
    public:
        SubObjectFinder(const std::string& typeNamePrefix);

        ~SubObjectFinder() override = default;

        /**
         * Get the objects that have been found.
         *
         * The keys are strings instead of `aron::Path`s because those don't support comparisons.
         *
         * @return map of paths to pair of data and type variants
         */
        const std::map<std::string, std::pair<aron::data::VariantPtr, aron::type::VariantPtr>>&
        getFoundObjects();

        void visitObjectOnEnter(DataInput& elementData, TypeInput& elementType) override;

        void visitUnknown(DataInput& elementData, TypeInput& elementType) override;

        MapElements getObjectElements(DataInput& elementData, TypeInput& elementType) override;

    private:
        std::string typeNamePrefix;
        std::map<std::string, std::pair<aron::data::VariantPtr, aron::type::VariantPtr>>
            foundSubObjects;
    };

    /**
     * Finds aron objects with a given type name prefix in aron variants and returns them as BOs.
     *
     * Construct it with the prefix you want to search for,
     * then use `visitRecursive` to run the finder over the variant to search.
     * Get a map of paths to your BOs using `getFoundObjects`.
     *
     * \tparam DTOType the _generated_ aron type of your data.
     * \tparam BOType the type of your final BO.
     */
    template <typename DTOType, typename BOType>
    class BOSubObjectFinder : public armarx::aron::data::RecursiveConstTypedVariantVisitor
    {
    public:
        BOSubObjectFinder() = default;
        ~BOSubObjectFinder() override = default;

        /**
         * Get the objects that have been found.
         *
         * The keys are strings instead of `aron::Path`s because those don't support comparisons.
         *
         * @return map of paths to BOs.
         */
        const std::map<std::string, BOType>&
        getFoundObjects()
        {
            return foundSubObjects;
        }

        void
        visitObjectOnEnter(DataInput& elementData, TypeInput& elementType) override
        {
            if (elementType->getFullName() == DTOType::ToAronType()->getFullName())
            {
                auto dict = armarx::aron::data::Dict::DynamicCastAndCheck(elementData);
                DTOType dto;
                dto.fromAron(dict);
                BOType boObj = armarx::aron::fromAron<BOType, DTOType>(dto);
                foundSubObjects.emplace(elementData->getPath().toString(), boObj);
            }
        }

        void
        visitUnknown(DataInput& elementData, TypeInput& elementType) override
        {
            // Ignore (the base class throws an exception here)
        }

        MapElements
        getObjectElements(DataInput& elementData, TypeInput& elementType) override
        {
            return RecursiveConstTypedVariantVisitor::GetObjectElementsWithNullType(elementData,
                                                                                    elementType);
        }

    private:
        std::map<std::string, BOType> foundSubObjects;
    };

} // namespace armarx::aron
