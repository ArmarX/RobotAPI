/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::libraries::aron
 * @author     Philip Scherer ( ulila@student.kit.edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "object_finders.h"

namespace armarx::aron
{
    SubObjectFinder::SubObjectFinder(const std::string& typeNamePrefix) :
        typeNamePrefix(typeNamePrefix)
    {
    }

    const std::map<std::string, std::pair<aron::data::VariantPtr, aron::type::VariantPtr>>&
    SubObjectFinder::getFoundObjects()
    {
        return foundSubObjects;
    }

    void
    SubObjectFinder::visitObjectOnEnter(DataInput& elementData, TypeInput& elementType)
    {
        if (elementType && simox::alg::starts_with(elementType->getFullName(), typeNamePrefix))
        {
            foundSubObjects.emplace(elementData->getPath().toString(),
                                    std::make_pair(elementData, elementType));
        }
    }

    void
    SubObjectFinder::visitUnknown(DataInput& elementData, TypeInput& elementType)
    {
        // Ignore (the base class throws an exception here)
    }

    SubObjectFinder::MapElements
    SubObjectFinder::getObjectElements(DataInput& elementData, TypeInput& elementType)
    {
        return RecursiveConstTypedVariantVisitor::GetObjectElementsWithNullType(elementData,
                                                                                elementType);
    }
} // namespace armarx::aron
