#pragma once

#include <RobotAPI/libraries/aron/core/aron_conversions.h>
#include <ArmarXCore/core/time/DateTime.h>
#include <ArmarXCore/core/time/Duration.h>
#include <ArmarXCore/core/time/Frequency.h>

// arondto forward declarations
namespace armarx::arondto
{
    class ClockType;
    class Duration;
    class DateTime;
    class Frequency;
}


namespace IceUtil
{
    class Time;

    /* Deprecated stuff from old time type */
    [[deprecated("Using IceUtil::Time in ArmarX is deprecated. Use armarx::DateTime instead.")]]
    void fromAron(const long& dto, IceUtil::Time& bo);

    [[deprecated("Using IceUtil::Time in ArmarX is deprecated. Use armarx::DateTime instead.")]]
    void toAron(long& dto, const IceUtil::Time& bo);

    [[deprecated("Using IceUtil::Time in ArmarX is deprecated. Use armarx::DateTime instead.")]]
    void fromAron(const IceUtil::Time& dto, armarx::DateTime& bo);

    [[deprecated("Using IceUtil::Time in ArmarX is deprecated. Use armarx::DateTime instead.")]]
    void toAron(IceUtil::Time& dto, const armarx::DateTime& bo);
}
namespace armarx
{
    void fromAron(const arondto::ClockType& dto, ClockType& bo);
    void toAron(arondto::ClockType& dto, const ClockType& bo);

    void fromAron(const arondto::Duration& dto, Duration& bo);
    void toAron(arondto::Duration& dto, const Duration& bo);

    void fromAron(const arondto::Frequency& dto, Frequency& bo);
    void toAron(arondto::Frequency& dto, const Frequency& bo);

    void fromAron(const arondto::DateTime& dto, DateTime& bo);
    void toAron(arondto::DateTime& dto, const DateTime& bo);

}  // namespace armarx
