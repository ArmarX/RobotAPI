#include "eigen.h"

namespace armarx::aron
{
    using AronPose = Eigen::Matrix<float, 4, 4>;

    void fromAron(const AronPose& dto, Eigen::Affine3f& bo)
    {
        bo.matrix() = dto;
    }

    void toAron(AronPose& dto, const Eigen::Affine3f& bo)
    {
        dto = bo.matrix();
    }

    void fromAron(const AronPose& dto, Eigen::Isometry3f& bo)
    {
        bo.matrix() = dto;
    }

    void toAron(AronPose& dto, const Eigen::Isometry3f& bo)
    {
        dto = bo.matrix();
    }

    void fromAron(const AronPose& dto, Eigen::Isometry3d& bo)
    {
        bo.matrix() = dto.cast<double>();
    }

    void toAron(AronPose& dto, const Eigen::Isometry3d& bo)
    {
        dto = bo.matrix().cast<float>();
    }


}  // namespace armarx::aron
