#include "simox.h"

#include <RobotAPI/libraries/aron/common/aron/AxisAlignedBoundingBox.aron.generated.h>
#include <RobotAPI/libraries/aron/common/aron/Color.aron.generated.h>
#include <RobotAPI/libraries/aron/common/aron/OrientedBox.aron.generated.h>


void simox::fromAron(const arondto::AxisAlignedBoundingBox& dto, AxisAlignedBoundingBox& bo)
{
    bo.center() = dto.center;
    bo.extents() = dto.extents;
}

void simox::toAron(arondto::AxisAlignedBoundingBox& dto, const AxisAlignedBoundingBox& bo)
{
    dto.center = bo.center();
    dto.extents = bo.extents();
}


void simox::fromAron(const arondto::OrientedBox& dto, OrientedBoxf& bo)
{
    bo = OrientedBoxf(dto.center, dto.orientation, dto.extents);
}

void simox::toAron(arondto::OrientedBox& dto, const OrientedBoxf& bo)
{
    dto.center = bo.center();
    dto.orientation = bo.rotation();
    dto.extents = bo.dimensions();
}


void simox::fromAron(const arondto::Color& dto, Color& bo)
{
    bo.r = dto.r;
    bo.g = dto.g;
    bo.b = dto.b;
    bo.a = dto.a;
}

void simox::toAron(arondto::Color& dto, const Color& bo)
{
    dto.r = bo.r;
    dto.g = bo.g;
    dto.b = bo.b;
    dto.a = bo.a;
}
