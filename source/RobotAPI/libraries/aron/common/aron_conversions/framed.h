#pragma once

#include <RobotAPI/libraries/aron/core/aron_conversions.h>
#include <RobotAPI/libraries/core/FramedPose.h>

// arondto forward declarations
namespace armarx::arondto
{
    class FramedPosition;
    class FramedOrientation;
    class FramedPose;
    class FrameID;
}


namespace armarx
{
    void fromAron(const arondto::FramedPosition& dto, armarx::FramedPosition& bo);
    void toAron(arondto::FramedPosition& dto, const armarx::FramedPosition& bo);

    void fromAron(const arondto::FramedOrientation& dto, armarx::FramedOrientation& bo);
    void toAron(arondto::FramedOrientation& dto, const armarx::FramedOrientation& bo);

    void fromAron(const arondto::FramedPose& dto, armarx::FramedPose& bo);
    void toAron(arondto::FramedPose& dto, const armarx::FramedPose& bo);
}
