#include "time.h"

#include <IceUtil/Time.h>

#include <RobotAPI/libraries/aron/common/aron/time.aron.generated.h>
#include <ArmarXCore/core/time/ice_conversions.h>


void IceUtil::fromAron(const long& dto, IceUtil::Time& bo)
{
    bo = IceUtil::Time::microSeconds(dto);
}

void IceUtil::toAron(long& dto, const IceUtil::Time& bo)
{
    dto = bo.toMicroSeconds();
}


void IceUtil::fromAron(const IceUtil::Time& dto, armarx::DateTime& bo)
{
    fromIce(dto, bo);
}

void IceUtil::toAron(IceUtil::Time& dto, const armarx::DateTime& bo)
{
    toIce(dto, bo);
}

void armarx::fromAron(const arondto::ClockType& dto, armarx::ClockType& bo)
{
    switch (dto.value)
    {
        case arondto::ClockType::Realtime:
            bo = ClockType::Realtime;
            break;
        case arondto::ClockType::Monotonic:
            bo = ClockType::Monotonic;
            break;
        case arondto::ClockType::Virtual:
            bo = ClockType::Virtual;
            break;
        case arondto::ClockType::Unknown:
            bo = ClockType::Unknown;
            break;
    }
}

void armarx::toAron(arondto::ClockType& dto, const armarx::ClockType& bo)
{
    switch (bo)
    {
        case ClockType::Realtime:
            dto = arondto::ClockType::Realtime;
            break;
        case ClockType::Monotonic:
            dto = arondto::ClockType::Monotonic;
            break;
        case ClockType::Virtual:
            dto = arondto::ClockType::Virtual;
            break;
        case ClockType::Unknown:
            dto = arondto::ClockType::Unknown;
            break;
    }
}

void armarx::fromAron(const arondto::Duration& dto, armarx::Duration& bo)
{
    bo = Duration::MicroSeconds(dto.microSeconds);
}

void armarx::toAron(arondto::Duration& dto, const armarx::Duration& bo)
{
    dto.microSeconds = bo.toMicroSeconds();
}

void armarx::fromAron(const arondto::Frequency& dto, armarx::Frequency& bo)
{
    Duration cycleDuration;
    fromAron(dto.cycleDuration, cycleDuration);

    bo = Frequency(cycleDuration);
}

void armarx::toAron(arondto::Frequency& dto, const armarx::Frequency& bo)
{
    toAron(dto.cycleDuration, bo.toCycleDuration());
}

void armarx::fromAron(const arondto::DateTime& dto, armarx::DateTime& bo)
{
    ClockType clockType;
    fromAron(dto.clockType, clockType);

    Duration timeSinceEpoch;
    fromAron(dto.timeSinceEpoch, timeSinceEpoch);

    bo = DateTime(timeSinceEpoch, clockType, dto.hostname);
}

void armarx::toAron(arondto::DateTime& dto, const armarx::DateTime& bo)
{
    toAron(dto.clockType, bo.clockType());
    toAron(dto.timeSinceEpoch, bo.toDurationSinceEpoch());
    dto.hostname = bo.hostname();
}
