#pragma once

// This file is just a legacy include file.
// We moved the stl and core conversions to the aron library.

#include <RobotAPI/libraries/aron/core/aron_conversions.h>

namespace armarx
{


}  // namespace armarx
