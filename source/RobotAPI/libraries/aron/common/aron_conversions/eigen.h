#pragma once

#include <Eigen/Geometry>

#include <RobotAPI/libraries/aron/core/aron_conversions.h>

namespace armarx::aron
{
    using AronPose = Eigen::Matrix<float, 4, 4>;

    void fromAron(const AronPose& dto, Eigen::Affine3f& bo);
    void toAron(AronPose& dto, const Eigen::Affine3f& bo);

    void fromAron(const AronPose& dto, Eigen::Isometry3f& bo);
    void toAron(AronPose& dto, const Eigen::Isometry3f& bo);

    void fromAron(const AronPose& dto, Eigen::Isometry3d& bo);
    void toAron(AronPose& dto, const Eigen::Isometry3d& bo);

}  // namespace armarx
