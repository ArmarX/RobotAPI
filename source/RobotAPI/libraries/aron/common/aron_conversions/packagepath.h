#pragma once

#include <RobotAPI/libraries/aron/core/aron_conversions.h>
#include <ArmarXCore/core/PackagePath.h>

// arondto forward declarations
namespace armarx::arondto
{
    class PackagePath;
}

namespace armarx
{

    void fromAron(const arondto::PackagePath& dto, PackagePath& bo);
    void toAron(arondto::PackagePath& dto, const PackagePath& bo);

}  // namespace armarx
