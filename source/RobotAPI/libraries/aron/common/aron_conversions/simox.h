#pragma once

#include <RobotAPI/libraries/aron/core/aron_conversions.h>

#include <SimoxUtility/color/Color.h>
#include <SimoxUtility/shapes/AxisAlignedBoundingBox.h>
#include <SimoxUtility/shapes/OrientedBox.h>

#include <RobotAPI/libraries/aron/common/forward_declarations.h>


namespace simox
{
    void fromAron(const arondto::AxisAlignedBoundingBox& dto, AxisAlignedBoundingBox& bo);
    void toAron(arondto::AxisAlignedBoundingBox& dto, const AxisAlignedBoundingBox& bo);

    void fromAron(const arondto::OrientedBox& dto, OrientedBoxf& bo);
    void toAron(arondto::OrientedBox& dto, const OrientedBoxf& bo);

    void fromAron(const arondto::Color& dto, Color& bo);
    void toAron(arondto::Color& dto, const Color& bo);
}

