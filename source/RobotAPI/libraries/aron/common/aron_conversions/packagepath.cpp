#include "packagepath.h"

#include <RobotAPI/libraries/aron/common/aron/PackagePath.aron.generated.h>

/* PackagePath */

void armarx::fromAron(const arondto::PackagePath& dto, PackagePath& bo)
{
    bo = {dto.package, dto.path};
}

void armarx::toAron(arondto::PackagePath& dto, const PackagePath& bo)
{
    const data::PackagePath icedto = bo.serialize();
    dto.package                    = icedto.package;
    dto.path                       = icedto.path;
}
