/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::aron_common::aron_common
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "MyCustomType.h"

#include <ostream>

#include <RobotAPI/libraries/aron/core/aron_conversions.h>


template <class CustomTypeT>
std::ostream& ostreamop(std::ostream& os, const CustomTypeT& rhs)
{
    return os << "(name='" << rhs.name << "'"
              << " index=" << rhs.index
              << " value=" << rhs.value
              << ")";
}

std::ostream& my::operator<<(std::ostream& os, const CustomType& rhs)
{
    return ostreamop(os, rhs);
}

std::ostream& my::arondto::operator<<(std::ostream& os, const CustomType& rhs)
{
    return ostreamop(os, rhs);
}

bool my::operator==(const CustomType& lhs, const arondto::CustomType& rhs)
{
    return lhs.name == rhs.name
            && lhs.index == rhs.index
            && lhs.value == rhs.value;
}


void my::toAron(arondto::CustomType& dto, const CustomType& bo)
{
    dto.name = bo.name;
    armarx::aron::toAron(dto.index, bo.index);
    armarx::aron::toAron(dto.value, bo.value);
}
void my::fromAron(const arondto::CustomType& dto, CustomType& bo)
{
    bo.name = dto.name;
    armarx::aron::fromAron(dto.index, bo.index);
    armarx::aron::fromAron(dto.value, bo.value);
}
