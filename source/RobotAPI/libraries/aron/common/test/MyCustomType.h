/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::aron_common::aron_common
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ostream>
#include <string>
#include <vector>

#include <RobotAPI/libraries/aron/core/codegeneration/cpp/AronGeneratedClass.h>

namespace my
{
    struct CustomType
    {
        std::string name;
        int index = 0;
        float value = 0.0;
    };

    std::ostream& operator<<(std::ostream& os, const CustomType& rhs);


    namespace arondto
    {
        class CustomType : public armarx::aron::cpp::AronGeneratedClass
        {
        public:
            std::string name;
            int index;
            float value;

            CustomType() = default;
            CustomType(const std::string& n, int i, float f) : name(n), index(i), value(f) {}
        };

        std::ostream& operator<<(std::ostream& os, const arondto::CustomType& rhs);
    }

    bool operator==(const CustomType& lhs, const arondto::CustomType& rhs);
    bool operator!=(const CustomType& lhs, const arondto::CustomType& rhs)
    {
        return !(lhs == rhs);
    }
    bool operator==(const arondto::CustomType& lhs, const CustomType& rhs)
    {
        return rhs == lhs;
    }
    bool operator!=(const arondto::CustomType& lhs, const CustomType& rhs)
    {
        return !(rhs == lhs);
    }

    void toAron(arondto::CustomType& dto, const CustomType& bo);
    void fromAron(const arondto::CustomType& dto, CustomType& bo);
}


namespace std
{

    template <class L1, class L2, class R1, class R2>
    bool operator==(const std::pair<L1, L2>& lhs,
                    const std::pair<R1, R2>& rhs)
    {
        return lhs.first == rhs.first && lhs.second == rhs.second;
    }
    template <class L1, class L2, class R1, class R2>
    bool operator!=(const std::pair<L1, L2>& lhs,
                    const std::pair<R1, R2>& rhs)
    {
        return !(lhs == rhs);
    }


    template <class L, class R>
    bool operator==(const std::vector<L>& lhs, const std::vector<R>& rhs)
    {
        if (lhs.size() != rhs.size())
        {
            return false;
        };
        for (size_t i = 0; i < lhs.size(); ++i)
        {
            if (lhs[i] != rhs[i])
            {
                return false;
            }
        }
        return true;
    }
    template <class L, class R>
    bool operator!=(const std::vector<L>& lhs, const std::vector<R>& rhs)
    {
        return !(lhs == rhs);
    }

}

