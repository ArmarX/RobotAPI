/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArmarXObjects
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::aron_common

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>

#include <RobotAPI/libraries/aron/core/aron_conversions.h>

#include <iostream>

#include "MyCustomType.h"


BOOST_AUTO_TEST_CASE(test_direct)
{
    const my::CustomType bo { "name", -10, 42.42f };

    my::arondto::CustomType dto;
    toAron(dto, bo);
    BOOST_CHECK_EQUAL(dto, bo);

    my::CustomType boOut;
    fromAron(dto, boOut);
    BOOST_CHECK_EQUAL(boOut, dto);
}


template <class BOs, class DTOs>
void test_complex(const BOs bos, DTOs& dtos)
{
    armarx::aron::toAron(dtos, bos);
    BOOST_CHECK_EQUAL_COLLECTIONS(dtos.begin(), dtos.end(),
                                  bos.begin(), bos.end());

    BOs bosOut;
    armarx::aron::fromAron(dtos, bosOut);
    BOOST_CHECK_EQUAL_COLLECTIONS(bosOut.begin(), bosOut.end(),
                                  dtos.begin(), dtos.end());
}


BOOST_AUTO_TEST_CASE(test_stl_vector)
{
    const std::vector<my::CustomType> bos
    {
        { "name", -10, 42.42f },
        { "name2", 20, -128.128f },
    };
    std::vector<my::arondto::CustomType> dtos;

    test_complex(bos, dtos);
}


BOOST_AUTO_TEST_CASE(test_stl_map)
{
    const std::map<std::string, my::CustomType> bos
    {
        { "key1", { "name", -10,   42.42f  } },
        { "key2", { "name2", 20, -128.128f } },
    };
    std::map<std::string, my::arondto::CustomType> dtos;

    test_complex(bos, dtos);
}


BOOST_AUTO_TEST_CASE(test_stl_vector_of_vector)
{
    const std::vector<std::vector<my::CustomType>> bos
    {
        { { "name", -10,   42.42f  } },
        { },
        { { "name2", 20, -128.128f }, { "name2.1", 40, -64.64f } },
    };
    std::vector<std::vector<my::arondto::CustomType>> dtos;

    test_complex(bos, dtos);
}


BOOST_AUTO_TEST_CASE(test_stl_map_of_vector)
{
    const std::map<std::string, std::vector<my::CustomType>> bos
    {
        { "key1", { { "name", -10,   42.42f  } } },
        { "key2", { { "name2", 20, -128.128f }, { "name2.1", 40, -64.64f } } },
        { "key3", {  } },
    };
    std::map<std::string, std::vector<my::arondto::CustomType>> dtos;

    test_complex(bos, dtos);
}


BOOST_AUTO_TEST_CASE(test_optional)
{
    using std::nullopt;

    std::optional<my::CustomType> bo;
    std::optional<my::arondto::CustomType> dto;

    auto reset = [&](
            std::optional<my::CustomType> theBo,
            std::optional<my::arondto::CustomType> theDto)
    {
        bo = theBo;
        dto = theDto;
    };

    reset(nullopt, nullopt);
    {
        armarx::aron::toAron(dto, bo);
        BOOST_CHECK(!dto.has_value());
    }
    reset(nullopt, nullopt);
    {
        armarx::aron::fromAron(dto, bo);
        BOOST_CHECK(!bo.has_value());
    }

    reset(my::CustomType{ "bo", 30, 16.16f }, nullopt);
    {
        armarx::aron::toAron(dto, bo);
        BOOST_CHECK(dto.has_value());
        BOOST_CHECK_EQUAL(dto.value(), bo.value());
    }
    reset(my::CustomType{ "bo", 30, 16.16f }, nullopt);
    {
        armarx::aron::fromAron(dto, bo);
        BOOST_CHECK(!bo.has_value());
    }

    reset(nullopt, my::arondto::CustomType{ "dto", 30, 16.16f });
    {
        armarx::aron::toAron(dto, bo);
        BOOST_CHECK(!dto.has_value());
    }
    reset(nullopt, my::arondto::CustomType{ "dto", 30, 16.16f });
    {
        armarx::aron::fromAron(dto, bo);
        BOOST_CHECK(bo.has_value());
        BOOST_CHECK_EQUAL(bo.value(), dto.value());
    }

    reset(my::CustomType{ "bo", -30, -16.16f }, my::arondto::CustomType{ "dto", 30, 16.16f });
    {
        armarx::aron::toAron(dto, bo);
        BOOST_CHECK(dto.has_value());
        BOOST_CHECK_EQUAL(dto.value(), bo.value());
        BOOST_CHECK_EQUAL(dto->name, "bo");
    }
    reset(my::CustomType{ "bo", -30, -16.16f }, my::arondto::CustomType{ "dto", 30, 16.16f });
    {
        armarx::aron::fromAron(dto, bo);
        BOOST_CHECK(bo.has_value());
        BOOST_CHECK_EQUAL(bo.value(), dto.value());
        BOOST_CHECK_EQUAL(bo->name, "dto");
    }
}



BOOST_AUTO_TEST_CASE(test_optional_value_flagged)
{
    using std::nullopt;

    std::optional<my::CustomType> bo;
    my::arondto::CustomType dto;
    bool dtoValid;

    auto reset = [&](
            std::optional<my::CustomType> theBo,
            std::optional<my::arondto::CustomType> theDto)
    {
        bo = theBo;
        if (theDto)
        {
            dto = *theDto;
            dtoValid = true;
        }
        else
        {
            dto = {};
            dtoValid = false;
        }
    };

    reset(nullopt, nullopt);
    {
        armarx::aron::toAron(dto, dtoValid, bo);
        BOOST_CHECK(!dtoValid);
    }
    reset(nullopt, nullopt);
    {
        armarx::aron::fromAron(dto, dtoValid, bo);
        BOOST_CHECK(!bo.has_value());
    }

    reset(my::CustomType{ "bo", 30, 16.16f }, nullopt);
    {
        armarx::aron::toAron(dto, dtoValid, bo);
        BOOST_CHECK(dtoValid);
        BOOST_CHECK_EQUAL(dto, bo.value());
    }
    reset(my::CustomType{ "bo", 30, 16.16f }, nullopt);
    {
        armarx::aron::fromAron(dto, dtoValid, bo);
        BOOST_CHECK(!bo.has_value());
    }

    reset(nullopt, my::arondto::CustomType{ "dto", 30, 16.16f });
    {
        armarx::aron::toAron(dto, dtoValid, bo);
        BOOST_CHECK(!dtoValid);
    }
    reset(nullopt, my::arondto::CustomType{ "dto", 30, 16.16f });
    {
        armarx::aron::fromAron(dto, dtoValid, bo);
        BOOST_CHECK(bo.has_value());
        BOOST_CHECK_EQUAL(bo.value(), dto);
    }

    reset(my::CustomType{ "bo", -30, -16.16f }, my::arondto::CustomType{ "dto", 30, 16.16f });
    {
        armarx::aron::toAron(dto, dtoValid, bo);
        BOOST_CHECK(dtoValid);
        BOOST_CHECK_EQUAL(dto, bo.value());
        BOOST_CHECK_EQUAL(dto.name, "bo");
    }
    reset(my::CustomType{ "bo", -30, -16.16f }, my::arondto::CustomType{ "dto", 30, 16.16f });
    {
        armarx::aron::fromAron(dto, dtoValid, bo);
        BOOST_CHECK(bo.has_value());
        BOOST_CHECK_EQUAL(bo.value(), dto);
        BOOST_CHECK_EQUAL(bo->name, "dto");
    }
}
