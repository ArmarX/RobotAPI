/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Path.h"

// ArmarX
#include <RobotAPI/libraries/aron/core/Exception.h>

namespace armarx::aron
{
    Path::Path() :
        rootIdentifier("_ARON"),
        delimeter("->")
    {
    }

    Path::Path(const std::vector<std::string>& p) :
        path(p)
    {
    }

    Path::Path(const Path& p) :
        rootIdentifier(p.getRootIdentifier()),
        delimeter(p.getDelimeter()),
        path(p.getPath())
    {

    }

    Path::Path(const Path& pa, const std::vector<std::string>& p) :
        Path(pa)
    {
        for (const std::string& s : p)
        {
            append(s);
        }
    }

    void Path::setRootIdentifier(const std::string& s)
    {
        rootIdentifier = s;
    }

    std::string Path::getRootIdentifier() const
    {
        return rootIdentifier;
    }

    void Path::setDelimeter(const std::string& d)
    {
        delimeter = d;
    }

    std::string Path::getDelimeter() const
    {
        return delimeter;
    }

    void Path::append(const std::string& str)
    {
        path.push_back(str);
    }

    std::vector<std::string> Path::getPath() const
    {
        return path;
    }

    std::string Path::getLastElement() const
    {
        if (!hasElement())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Try to access last element of empty vector.");
        }
        return path.back();
    }

    std::string Path::getFirstElement() const
    {
        if (!hasElement())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Try to access last element of empty vector.");
        }
        return path[0];
    }

    bool Path::hasElement() const
    {
        return path.size() > 0;
    }

    size_t Path::size() const
    {
        return path.size();
    }

    std::string Path::toString() const
    {
        std::stringstream ss;
        ss << rootIdentifier;
        for (const std::string& s : path)
        {
            ss << delimeter << s;
        }
        return ss.str();
    }

    Path Path::FromString(const std::string& s, const std::string& rootIdentifier, const std::string& delimeter)
    {
        std::vector<std::string> elements = simox::alg::split(s, delimeter);
        if (elements.size())
        {
            elements[0] = simox::alg::remove_prefix(elements[0], rootIdentifier);
        }
        return Path(elements);
    }

    Path Path::withIndex(int i, bool escape) const
    {
        std::string el = std::to_string(i);
        if (escape)
        {
            el = "\"" + el + "\"";
        }
        return Path(*this, {el});
    }

    Path Path::withElement(const std::string& s, bool escape) const
    {
        std::string el = s;
        if (escape)
        {
            el = "\"" + el + "\"";
        }
        return Path(*this, {el});
    }

    Path Path::withAcceptedType(bool escape) const
    {
        std::string el = "::accepted_type";
        if (escape)
        {
            el = "\"" + el + "\"";
        }
        return Path(*this, {el});
    }

    Path Path::withAcceptedTypeIndex(int i, bool escape) const
    {
        std::string el = "::accepted_type_" + std::to_string(i);
        if (escape)
        {
            el = "\"" + el + "\"";
        }
        return Path(*this, {el});
    }

    Path Path::withDetachedLastElement() const
    {
        std::vector<std::string> p = path;
        p.pop_back();
        auto ret = Path(p);
        ret.setRootIdentifier(rootIdentifier);
        ret.setDelimeter(delimeter);
        return ret;
    }

    Path Path::withDetachedFirstElement() const
    {
        std::vector<std::string> p = path;
        p.erase(p.begin());
        auto ret = Path(p);
        ret.setRootIdentifier(rootIdentifier);
        ret.setDelimeter(delimeter);
        return ret;
    }

    Path Path::getWithoutPrefix(const Path& pref) const
    {
        unsigned int firstWithoutMatch = 0;
        for (const std::string& el : pref.getPath())
        {
            if (path.size() <= firstWithoutMatch || el != path[firstWithoutMatch])
            {
                break;
            }
            else
            {
                firstWithoutMatch++;
            }
        }
        std::vector<std::string> elementsWithoutPrefix(path.begin() + firstWithoutMatch, path.end());

        auto ret = Path(elementsWithoutPrefix);
        ret.setRootIdentifier(rootIdentifier);
        ret.setDelimeter(delimeter);
        return ret;
    }
}
