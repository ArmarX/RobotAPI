/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <type_traits>

#include "../../Descriptor.h"
#include "Visitor.h"

namespace armarx::aron::type
{
    /**
     * @brief The visitRecursive function. Calls visitX of a RecursiveVisitorImplementation recursively. For more information please see Visitor.h
     */
    template <class RecursiveVisitorImplementation>
    void visitRecursive(RecursiveVisitorImplementation& v, typename RecursiveVisitorImplementation::Input& t)
    {
        auto descriptor = v.getDescriptor(t);
        switch (descriptor)
        {
            case type::Descriptor::LIST:
            {
                v.visitListOnEnter(t);
                auto acceptedType = v.getListAcceptedType(t);
                visitRecursive(v, acceptedType);
                v.visitListOnExit(t);
                return;
            }
            case type::Descriptor::PAIR:
            {
                v.visitPairOnEnter(t);
                auto acceptedTypes = v.getPairAcceptedTypes(t);
                visitRecursive(v, acceptedTypes.first);
                visitRecursive(v, acceptedTypes.second);
                v.visitPairOnExit(t);
                return;
            }
            case type::Descriptor::TUPLE:
            {
                v.visitTupleOnEnter(t);
                unsigned int i = 0;
                for (const auto& acceptedType : v.getTupleAcceptedTypes(t))
                {
                    visitRecursive(v, acceptedType);
                    i++;
                }
                v.visitTupleOnExit(t);
                return;
            }
            case type::Descriptor::DICT:
            {
                v.visitDictOnEnter(t);
                auto acceptedType = v.getDictAcceptedType(t);
                visitRecursive(v, acceptedType);
                v.visitDictOnExit(t);
                return;
            }
            case type::Descriptor::OBJECT:
            {
                v.visitObjectOnEnter(t);
                for (const auto& [key, acceptedType] : v.getObjectElements(t))
                {
                    visitRecursive(v, acceptedType);
                }
                v.visitObjectOnExit(t);
                return;
            }
            case type::Descriptor::NDARRAY:
                return v.visitNDArray(t);
            case type::Descriptor::MATRIX:
                return v.visitMatrix(t);
            case type::Descriptor::IMAGE:
                return v.visitImage(t);
            case type::Descriptor::POINTCLOUD:
                return v.visitPointCloud(t);
            case type::Descriptor::QUATERNION:
                return v.visitQuaternion(t);
            case type::Descriptor::INT:
                return v.visitInt(t);
            case type::Descriptor::LONG:
                return v.visitLong(t);
            case type::Descriptor::FLOAT:
                return v.visitFloat(t);
            case type::Descriptor::DOUBLE:
                return v.visitDouble(t);
            case type::Descriptor::STRING:
                return v.visitString(t);
            case type::Descriptor::BOOL:
                return v.visitBool(t);
            case type::Descriptor::ANY_OBJECT:
                return v.visitAnyObject(t);
            case type::Descriptor::INT_ENUM:
                return v.visitIntEnum(t);
            case type::Descriptor::UNKNOWN:
                return v.visitUnknown(t);
        }
    }

    /**
     * @brief The RecursiveVisitor struct. It differs from the Visitor struct (@see Visitor.h), because it provides special visitXOnEnter and visitXOnExit methods for container types.
     * Further, it defines abstract methods to get the children from the input representation which is used by the visitRecursive method.
     */
    template <class T>
    struct RecursiveVisitor : virtual public VisitorBase<T>
    {
        using Input = typename VisitorBase<T>::Input;
        using InputNonConst = typename std::remove_const<Input>::type;

        using ObjectElements = std::map<std::string, InputNonConst>;
        using PairElements = std::pair<InputNonConst, InputNonConst>;
        using TupleElements = std::vector<InputNonConst>;

        virtual ObjectElements getObjectAcceptedTypes(Input&) = 0;
        virtual InputNonConst getDictAcceptedType(Input&) = 0;
        virtual InputNonConst getListAcceptedType(Input&) = 0;
        virtual PairElements getPairAcceptedTypes(Input&) = 0;
        virtual TupleElements getTupleAcceptedTypes(Input&) = 0;

        virtual void visitObjectOnEnter(Input&) {};
        virtual void visitObjectOnExit(Input&) {};
        virtual void visitDictOnEnter(Input&) {};
        virtual void visitDictOnExit(Input&) {};
        virtual void visitPairOnEnter(Input&) {};
        virtual void visitPairOnExit(Input&) {};
        virtual void visitTupleOnEnter(Input&) {};
        virtual void visitTupleOnExit(Input&) {};
        virtual void visitListOnEnter(Input&) {};
        virtual void visitListOnExit(Input&) {};

        virtual void visitMatrix(Input&) {};
        virtual void visitNDArray(Input&) {};
        virtual void visitQuaternion(Input&) {};
        virtual void visitOrientation(Input&) {};
        virtual void visitPosition(Input&) {};
        virtual void visitPose(Input&) {};
        virtual void visitImage(Input&) {};
        virtual void visitPointCloud(Input&) {};
        virtual void visitIntEnum(Input&) {};
        virtual void visitInt(Input&) {};
        virtual void visitLong(Input&) {};
        virtual void visitFloat(Input&) {};
        virtual void visitDouble(Input&) {};
        virtual void visitBool(Input&) {};
        virtual void visitString(Input&) {};
        virtual void visitAnyObject(Input&) {};
        virtual void visitUnknown(Input&) {
            throw error::AronException(__PRETTY_FUNCTION__, "Unknown type in visitor.");
        }
        virtual ~RecursiveVisitor() = default;
    };
}
