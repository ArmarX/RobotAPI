/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "VariantVisitor.h"

#include "../../variant/All.h"

namespace armarx::aron::type
{
    /****************************************************************************
     * VariantVisitor
     ***************************************************************************/
    type::Descriptor ConstVariantVisitor::GetDescriptor(Input& n)
    {
        if (!n)
        {
            return type::Descriptor::UNKNOWN;
        }
        return n->getDescriptor();
    }

    type::Descriptor ConstVariantVisitor::getDescriptor(Input& n)
    {
        return GetDescriptor(n);
    }

    void ConstVariantVisitor::visitObject(Input& i)
    {
        auto aron = type::Object::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitDict(Input& i)
    {
        auto aron = type::Dict::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitPair(Input& i)
    {
        auto aron = type::Pair::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitTuple(Input& i)
    {
        auto aron = type::Tuple::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitList(Input& i)
    {
        auto aron = type::List::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitMatrix(Input& i)
    {
        auto aron = type::Matrix::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitNDArray(Input& i)
    {
        auto aron = type::NDArray::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitQuaternion(Input & i)
    {
        auto aron = type::Quaternion::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitImage(Input& i)
    {
        auto aron = type::Image::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitPointCloud(Input& i)
    {
        auto aron = type::PointCloud::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitIntEnum(Input& i)
    {
        auto aron = type::IntEnum::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitInt(Input& i)
    {
        auto aron = type::Int::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitLong(Input& i)
    {
        auto aron = type::Long::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitFloat(Input& i)
    {
        auto aron = type::Float::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitDouble(Input& i)
    {
        auto aron = type::Double::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitBool(Input& i)
    {
        auto aron = type::Bool::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitString(Input& i)
    {
        auto aron = type::String::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitAronVariant(const type::ObjectPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::DictPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::PairPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::TuplePtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::ListPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::NDArrayPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::MatrixPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::QuaternionPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::ImagePtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::PointCloudPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::IntEnumPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::IntPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::LongPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::FloatPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::DoublePtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::BoolPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const type::StringPtr&) {}

    /****************************************************************************
     * RecursiveVariantVisitor
     ***************************************************************************/
    type::Descriptor RecursiveConstVariantVisitor::getDescriptor(Input& n)
    {
        return ConstVariantVisitor::GetDescriptor(n);
    }

    RecursiveConstVariantVisitor::ObjectElements RecursiveConstVariantVisitor::GetObjectAcceptedTypes(Input& t)
    {
        auto o = type::Object::DynamicCastAndCheck(t);
        return o->getMemberTypes();
    }

    RecursiveConstVariantVisitor::ObjectElements RecursiveConstVariantVisitor::getObjectAcceptedTypes(Input& t)
    {
        return GetObjectAcceptedTypes(t);
    }

    RecursiveConstVariantVisitor::InputNonConst RecursiveConstVariantVisitor::GetDictAcceptedType(Input& t)
    {
        auto o = type::Dict::DynamicCastAndCheck(t);
        return o->getAcceptedType();
    }

    RecursiveConstVariantVisitor::InputNonConst RecursiveConstVariantVisitor::getDictAcceptedType(Input& t)
    {
        return GetDictAcceptedType(t);
    }

    RecursiveConstVariantVisitor::InputNonConst RecursiveConstVariantVisitor::GetListAcceptedType(Input& t)
    {
        auto o = type::List::DynamicCastAndCheck(t);
        return o->getAcceptedType();
    }

    RecursiveConstVariantVisitor::InputNonConst RecursiveConstVariantVisitor::getListAcceptedType(Input& t)
    {
        return GetListAcceptedType(t);
    }

    RecursiveConstVariantVisitor::PairElements RecursiveConstVariantVisitor::GetPairAcceptedTypes(Input& t)
    {
        auto o = type::Pair::DynamicCastAndCheck(t);
        return o->getAcceptedTypes();
    }

    RecursiveConstVariantVisitor::PairElements RecursiveConstVariantVisitor::getPairAcceptedTypes(Input& t)
    {
        return GetPairAcceptedTypes(t);
    }

    RecursiveConstVariantVisitor::TupleElements RecursiveConstVariantVisitor::GetTupleAcceptedTypes(Input& t)
    {
        auto o = type::Tuple::DynamicCastAndCheck(t);
        return o->getAcceptedTypes();
    }

    RecursiveConstVariantVisitor::TupleElements RecursiveConstVariantVisitor::getTupleAcceptedTypes(Input& t)
    {
        return GetTupleAcceptedTypes(t);
    }
}
