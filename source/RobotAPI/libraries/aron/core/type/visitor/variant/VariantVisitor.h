/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <string>
#include <vector>

#include "../RecursiveVisitor.h"
#include "../../variant/forward_declarations.h"

namespace armarx::aron::type
{
    /**
     * @brief The VariantVisitor struct. Already implements the method to get the descriptor of an aron variant.
     */
    struct ConstVariantVisitor : virtual public Visitor<const type::VariantPtr>
    {
        static type::Descriptor GetDescriptor(Input& n);
        type::Descriptor getDescriptor(Input& n) override;
        virtual ~ConstVariantVisitor() = default;

        // Already implemented so that they cast the input and call specilized method below
        // Override if you do not want to use the specialized methods
        void visitObject(Input&) override;
        void visitDict(Input&) override;
        void visitPair(Input&) override;
        void visitTuple(Input&) override;
        void visitList(Input&) override;
        void visitMatrix(Input&) override;
        void visitNDArray(Input&) override;
        void visitQuaternion(Input&) override;
        void visitImage(Input&) override;
        void visitPointCloud(Input&) override;
        void visitIntEnum(Input&) override;
        void visitInt(Input&) override;
        void visitLong(Input&) override;
        void visitFloat(Input&) override;
        void visitDouble(Input&) override;
        void visitBool(Input&) override;
        void visitString(Input&) override;

        // Use these if you do not want to cast manually
        virtual void visitAronVariant(const type::ObjectPtr&);
        virtual void visitAronVariant(const type::DictPtr&);
        virtual void visitAronVariant(const type::ListPtr&);
        virtual void visitAronVariant(const type::PairPtr&);
        virtual void visitAronVariant(const type::TuplePtr&);
        virtual void visitAronVariant(const type::MatrixPtr&);
        virtual void visitAronVariant(const type::NDArrayPtr&);
        virtual void visitAronVariant(const type::QuaternionPtr&);
        virtual void visitAronVariant(const type::PointCloudPtr&);
        virtual void visitAronVariant(const type::ImagePtr&);
        virtual void visitAronVariant(const type::IntEnumPtr&);
        virtual void visitAronVariant(const type::IntPtr&);
        virtual void visitAronVariant(const type::LongPtr&);
        virtual void visitAronVariant(const type::FloatPtr&);
        virtual void visitAronVariant(const type::DoublePtr&);
        virtual void visitAronVariant(const type::BoolPtr&);
        virtual void visitAronVariant(const type::StringPtr&);
    };

    /**
     * @brief The RecursiveVariantVisitor struct. Already implements the methods to get the descriptor and children of an aron variant
     */
    struct RecursiveConstVariantVisitor : virtual public RecursiveVisitor<const type::VariantPtr>
    {
        type::Descriptor getDescriptor(Input& n) override;
        static ObjectElements GetObjectAcceptedTypes(Input& t);
        ObjectElements getObjectAcceptedTypes(Input& t) override;
        static InputNonConst GetDictAcceptedType(Input& t);
        InputNonConst getDictAcceptedType(Input& t) override;
        static InputNonConst GetListAcceptedType(Input& t);
        InputNonConst getListAcceptedType(Input& t) override;
        static PairElements GetPairAcceptedTypes(Input& t);
        PairElements getPairAcceptedTypes(Input& t) override;
        static TupleElements GetTupleAcceptedTypes(Input& t);
        TupleElements getTupleAcceptedTypes(Input& t) override;

        virtual ~RecursiveConstVariantVisitor() = default;
    };
}
