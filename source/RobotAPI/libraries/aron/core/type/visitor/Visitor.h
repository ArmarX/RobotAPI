/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <string>
#include <vector>

#include "../../Descriptor.h"
#include "../../Exception.h"

namespace armarx::aron::type
{
    /**
     * @brief The visit function. Takes a visitor implementation as input and a type representation (e.g. aron::type::variant or nlohmann::json).
     * Inspired by std::visit for std::variant
     * This method only performs a single visit. This is useful if you only want to call a specific method depending on the type, or if you want to call a customized recursive function
     * If you want to call a visitX recursively, please refer to "RecursiveVisitor.h"
     */
    template <class VisitorImplementation>
    void visit(VisitorImplementation& v, typename VisitorImplementation::Input& t)
    {
        auto descriptor = v.getDescriptor(t);
        switch (descriptor)
        {
        case type::Descriptor::OBJECT:
            return v.visitObject(t);
        case type::Descriptor::LIST:
            return v.visitList(t);
        case type::Descriptor::DICT:
            return v.visitDict(t);
        case type::Descriptor::PAIR:
            return v.visitPair(t);
        case type::Descriptor::TUPLE:
            return v.visitTuple(t);
        case type::Descriptor::NDARRAY:
            return v.visitNDArray(t);
        case type::Descriptor::MATRIX:
            return v.visitMatrix(t);
        case type::Descriptor::IMAGE:
            return v.visitImage(t);
        case type::Descriptor::POINTCLOUD:
            return v.visitPointCloud(t);
        case type::Descriptor::QUATERNION:
            return v.visitQuaternion(t);
        case type::Descriptor::INT:
            return v.visitInt(t);
        case type::Descriptor::LONG:
            return v.visitLong(t);
        case type::Descriptor::FLOAT:
            return v.visitFloat(t);
        case type::Descriptor::DOUBLE:
            return v.visitDouble(t);
        case type::Descriptor::STRING:
            return v.visitString(t);
        case type::Descriptor::BOOL:
            return v.visitBool(t);
        case type::Descriptor::ANY_OBJECT:
            return v.visitAnyObject(t);
        case type::Descriptor::INT_ENUM:
            return v.visitIntEnum(t);
        case type::Descriptor::UNKNOWN:
            return v.visitUnknown(t);
        }
    }

    /**
     * @brief The VisitorBase struct. Defines basic methods and typedefs for all visitors
     */
    template <class T>
    struct VisitorBase
    {
        using Input = T;

        virtual type::Descriptor getDescriptor(Input&) = 0;
        virtual ~VisitorBase() = default;
    };

    /**
     * @brief The Visitor struct. Defines the visitX methods a visitor has to impelement
     */
    template <class T>
    struct Visitor : virtual public VisitorBase<T>
    {
        using Input = typename VisitorBase<T>::Input;
        virtual void visitObject(Input&) {};
        virtual void visitDict(Input&) {};
        virtual void visitPair(Input&) {};
        virtual void visitTuple(Input&) {};
        virtual void visitList(Input&) {};
        virtual void visitMatrix(Input&) {};
        virtual void visitNDArray(Input&) {};
        virtual void visitQuaternion(Input&) {};
        virtual void visitOrientation(Input&) {};
        virtual void visitPosition(Input&) {};
        virtual void visitPose(Input&) {};
        virtual void visitImage(Input&) {};
        virtual void visitPointCloud(Input&) {};
        virtual void visitIntEnum(Input&) {};
        virtual void visitInt(Input&) {};
        virtual void visitLong(Input&) {};
        virtual void visitFloat(Input&) {};
        virtual void visitDouble(Input&) {};
        virtual void visitBool(Input&) {};
        virtual void visitString(Input&) {};
        virtual void visitAnyObject(Input&) {};
        virtual void visitUnknown(Input&) { throw error::AronException(__PRETTY_FUNCTION__, "Unknown type in visitor."); }
        virtual ~Visitor() = default;
    };
}
