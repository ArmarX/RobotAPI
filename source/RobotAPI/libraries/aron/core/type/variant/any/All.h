#pragma once

#include "AnyObject.h"

/**
 * A convenience header to include all primitive aron files (full include, not forward declared)
 */
namespace armarx::aron::type
{

}
