/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL

// Header
#include "Factory.h"

// ArmarX
#include <RobotAPI/libraries/aron/core/type/variant/All.h>

namespace armarx::aron::type
{
    // Access method
    std::unique_ptr<type::Variant> VariantFactory::create(const type::dto::GenericType& aron, const Path& path) const
    {
        auto descriptor = type::Aron2Descriptor(aron);
        switch(descriptor)
        {
            case type::Descriptor::LIST: return std::make_unique<type::List>(dynamic_cast<const type::dto::List&>(aron), path);
            case type::Descriptor::DICT: return std::make_unique<type::Dict>(dynamic_cast<const type::dto::Dict&>(aron), path);
            case type::Descriptor::PAIR: return std::make_unique<type::Pair>(dynamic_cast<const type::dto::Pair&>(aron), path);
            case type::Descriptor::TUPLE: return std::make_unique<type::Tuple>(dynamic_cast<const type::dto::Tuple&>(aron), path);
            case type::Descriptor::OBJECT: return std::make_unique<type::Object>(dynamic_cast<const type::dto::AronObject&>(aron), path);
            case type::Descriptor::NDARRAY: return std::make_unique<type::NDArray>(dynamic_cast<const type::dto::NDArray&>(aron), path);
            case type::Descriptor::MATRIX: return std::make_unique<type::Matrix>(dynamic_cast<const type::dto::Matrix&>(aron), path);
            case type::Descriptor::IMAGE: return std::make_unique<type::Image>(dynamic_cast<const type::dto::Image&>(aron), path);
            case type::Descriptor::POINTCLOUD: return std::make_unique<type::PointCloud>(dynamic_cast<const type::dto::PointCloud&>(aron), path);
            case type::Descriptor::QUATERNION: return std::make_unique<type::Quaternion>(dynamic_cast<const type::dto::Quaternion&>(aron), path);
            case type::Descriptor::INT_ENUM: return std::make_unique<type::IntEnum>(dynamic_cast<const type::dto::IntEnum&>(aron), path);
            case type::Descriptor::INT: return std::make_unique<type::Int>(dynamic_cast<const type::dto::AronInt&>(aron), path);
            case type::Descriptor::LONG: return std::make_unique<type::Long>(dynamic_cast<const type::dto::AronLong&>(aron), path);
            case type::Descriptor::FLOAT: return std::make_unique<type::Float>(dynamic_cast<const type::dto::AronFloat&>(aron), path);
            case type::Descriptor::DOUBLE: return std::make_unique<type::Double>(dynamic_cast<const type::dto::AronDouble&>(aron), path);
            case type::Descriptor::STRING: return std::make_unique<type::String>(dynamic_cast<const type::dto::AronString&>(aron), path);
            case type::Descriptor::BOOL: return std::make_unique<type::Bool>(dynamic_cast<const type::dto::AronBool&>(aron), path);
            case type::Descriptor::ANY_OBJECT: return std::make_unique<type::AnyObject>(dynamic_cast<const type::dto::AnyObject&>(aron), path);
            case type::Descriptor::UNKNOWN: break;
        }
        throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Got an unknown descriptor", std::to_string((int) descriptor) + " aka " + defaultconversion::string::Descriptor2String.at(descriptor), path);
    }

}
