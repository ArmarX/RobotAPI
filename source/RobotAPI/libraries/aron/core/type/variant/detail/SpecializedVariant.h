/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <string>
#include <unordered_map>

// Base class
#include "../Variant.h"

// ArmarX

namespace armarx::aron::type::detail
{
    template<typename AronTypeT, typename DerivedT>
    class SpecializedVariantBase :
        public type::Variant
    {

    public:
        SpecializedVariantBase(const type::Descriptor& descriptor, const Path& path) :
            Variant(descriptor, path),
            aron(new AronTypeT())
        {
        }

        SpecializedVariantBase(const AronTypeT& o, const type::Descriptor& descriptor, const Path& path) :
            Variant(descriptor, path),
            aron(new AronTypeT(o))
        {
        }

        virtual ~SpecializedVariantBase() = default;

        // operators
        operator AronTypeT()
        {
            return *aron;
        }

        bool operator==(const Variant& other) const override
        {
            const auto& n = dynamic_cast<const DerivedT&>(other);
            return *this == n;
        }

        bool operator==(const DerivedT& other) const
        {
            return *(this->aron) == *(other.aron);
        }

        // virtual implementations
        type::dto::GenericTypePtr toAronDTO() const override
        {
            ARMARX_CHECK_NOT_NULL(aron);
            return aron;
        }
        void setMaybe(const type::Maybe m) override
        {
            aron->maybe = m;
        }

        type::Maybe getMaybe() const override
        {
            return aron->maybe;
        }

        VariantPtr navigateRelative(const Path& path) const override
        {
            Path absoluteFromHere = path.getWithoutPrefix(getPath());
            return navigateAbsolute(absoluteFromHere);
        }


        // static methods
        static DerivedT& DynamicCast(Variant& n)
        {
            return dynamic_cast<DerivedT&>(n);
        }

        static const DerivedT& DynamicCast(const Variant& n)
        {
            return dynamic_cast<const DerivedT&>(n);
        }

        static std::shared_ptr<DerivedT> DynamicCast(const VariantPtr& n)
        {
            return std::dynamic_pointer_cast<DerivedT>(n);
        }

        static std::shared_ptr<DerivedT> DynamicCastAndCheck(const VariantPtr& n)
        {
            if (!n)
            {
                return nullptr;
            }

            auto casted = std::dynamic_pointer_cast<DerivedT>(n);
            ARMARX_CHECK_NOT_NULL(casted);
            return casted;
        }

    protected:
        typename AronTypeT::PointerType aron;
    };
}
