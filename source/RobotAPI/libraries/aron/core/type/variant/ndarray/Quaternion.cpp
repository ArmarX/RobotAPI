/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Quaternion.h"

#include <SimoxUtility/algorithm/string/string_conversion.h>

namespace armarx::aron::type
{
    const std::map<quaternion::ElementType, std::string> Quaternion::Elementtype2String =
    {
        {quaternion::ElementType::FLOAT32, "FLOAT32"},
        {quaternion::ElementType::FLOAT64, "FLOAT64"}
    };
    const std::map<std::string, quaternion::ElementType> Quaternion::String2Elementtype = conversion::util::InvertMap(Elementtype2String);

    // constructors
    Quaternion::Quaternion(const Path& path) :
        detail::NDArrayVariant<type::dto::Quaternion, Quaternion>(type::Descriptor::QUATERNION, path)
    {
    }

    Quaternion::Quaternion(const type::dto::Quaternion& o, const Path& path) :
        detail::NDArrayVariant<type::dto::Quaternion, Quaternion>(o, type::Descriptor::QUATERNION, path)
    {
    }

    type::dto::QuaternionPtr Quaternion::toQuaternionDTO() const
    {
        return this->aron;
    }

    type::quaternion::ElementType Quaternion::getElementType() const
    {
        return this->aron->elementType;
    }

    void Quaternion::setElementType(type::quaternion::ElementType t)
    {
        this->aron->elementType = t;
    }

    // virtual implementations
    std::string Quaternion::getShortName() const
    {
        return "Quaternion";
    }

    std::string Quaternion::getFullName() const
    {
        return "armarx::aron::type::Quaternion<" + Elementtype2String.at(this->aron->elementType) + ">";
    }
}

