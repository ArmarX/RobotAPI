/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Matrix.h"

// Simox
#include <SimoxUtility/algorithm/vector.hpp>

namespace armarx::aron::type
{
    const std::map<matrix::ElementType, std::string> Matrix::Elementtype2String
    {
        {matrix::ElementType::INT16, "INT16"},
        {matrix::ElementType::INT32, "INT32"},
        {matrix::ElementType::INT64, "INT64"},
        {matrix::ElementType::FLOAT32, "FLOAT32"},
        {matrix::ElementType::FLOAT64, "FLOAT64"}
    };

    const std::map<std::string, matrix::ElementType> Matrix::String2Elementtype = conversion::util::InvertMap(Elementtype2String);

    // constructors
    Matrix::Matrix(const Path& path) :
        detail::NDArrayVariant<type::dto::Matrix, Matrix>(type::Descriptor::MATRIX, path)
    {
    }

    Matrix::Matrix(const type::dto::Matrix& o, const Path& path) :
        detail::NDArrayVariant<type::dto::Matrix, Matrix>(o, type::Descriptor::MATRIX, path)
    {
    }

    int Matrix::getRows() const
    {
        return this->aron->rows;
    }

    int Matrix::getCols() const
    {
        return this->aron->cols;
    }

    void Matrix::setRows(const int w)
    {
        if (w == 0 || w < -1)
        {
            throw error::AronException(__PRETTY_FUNCTION__, "The rows cannot be 0 or < -1", getPath());
        }
        this->aron->rows = w;
    }

    void Matrix::setCols(const int h)
    {
        if (h == 0 || h < -1)
        {
            throw error::AronException(__PRETTY_FUNCTION__, "The cols cannot be 0 or < -1", getPath());
        }
        this->aron->cols = h;
    }

    type::matrix::ElementType Matrix::getElementType() const
    {
        return this->aron->elementType;
    }

    void Matrix::setElementType(const type::matrix::ElementType u)
    {
        this->aron->elementType = u;
    }

    type::dto::MatrixPtr Matrix::toMatrixDTO() const
    {
        return this->aron;
    }

    // virtual implementations
    std::string Matrix::getShortName() const
    {
        return "Matrix";
    }

    std::string Matrix::getFullName() const
    {
        std::string rows = std::to_string(aron->rows);
        std::string cols = std::to_string(aron->cols);

        if (aron->rows == -1)
        {
            rows = "Dynamic";
        }

        if (aron->cols == -1)
        {
            cols = "Dynamic";
        }

        return "armarx::aron::type::Matrix<" + rows + ", " + cols + ", " + Elementtype2String.at(this->aron->elementType) + ">";
    }
}

