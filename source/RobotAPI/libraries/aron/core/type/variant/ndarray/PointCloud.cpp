/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "PointCloud.h"

namespace armarx::aron::type
{
    const std::map<pointcloud::VoxelType, std::string> PointCloud::Voxeltype2String
    {
        {pointcloud::VoxelType::POINT_XYZ, "POINT_XYZ"},
        {pointcloud::VoxelType::POINT_XYZI, "POINT_XYZI"},
        {pointcloud::VoxelType::POINT_XYZL, "POINT_XYZL"},
        {pointcloud::VoxelType::POINT_XYZRGB, "POINT_XYZRGB"},
        {pointcloud::VoxelType::POINT_XYZRGBL, "POINT_XYZRGBL"},
        {pointcloud::VoxelType::POINT_XYZRGBA, "POINT_XYZRGBA"},
        {pointcloud::VoxelType::POINT_XYZHSV, "POINT_XYZHSV"}
    };

    const std::map<std::string, pointcloud::VoxelType> PointCloud::String2Voxeltype = conversion::util::InvertMap(Voxeltype2String);

    // constructors
    PointCloud::PointCloud(const Path& path) :
        detail::NDArrayVariant<type::dto::PointCloud, PointCloud>(type::Descriptor::POINTCLOUD, path)
    {
    }

    PointCloud::PointCloud(const type::dto::PointCloud& o, const Path& path) :
        detail::NDArrayVariant<type::dto::PointCloud, PointCloud>(o, type::Descriptor::POINTCLOUD, path)
    {
    }

    type::dto::PointCloudPtr PointCloud::toPointCloudDTO() const
    {
        return this->aron;
    }

    type::pointcloud::VoxelType PointCloud::getVoxelType() const
    {
        return this->aron->voxelType;
    }

    void PointCloud::setVoxelType(type::pointcloud::VoxelType u)
    {
        this->aron->voxelType = u;
    }

    // virtual implementations
    std::string PointCloud::getShortName() const
    {
        return "PointCloud";
    }

    std::string PointCloud::getFullName() const
    {
        return "armarx::aron::type::PointCloud<" + Voxeltype2String.at(this->aron->voxelType) + ">";
    }
}

