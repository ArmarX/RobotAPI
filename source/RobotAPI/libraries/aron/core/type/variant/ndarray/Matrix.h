/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>
#include <map>

// Base class
#include "../detail/NDArrayVariant.h"

namespace armarx::aron::type
{
    /**
     * @brief The Matrix class. It represents the matrix type
     * A Matrix is defined through the number of rows, cols and the element type
     */
    class Matrix :
        public detail::NDArrayVariant<type::dto::Matrix, Matrix>
    {
    public:
        // constructors
        Matrix(const Path& path = Path());
        Matrix(const type::dto::Matrix&, const Path& path = Path());

        int getRows() const;
        int getCols() const;
        type::matrix::ElementType getElementType() const;

        void setRows(const int);
        void setCols(const int);
        void setElementType(const type::matrix::ElementType);

        type::dto::MatrixPtr toMatrixDTO() const;

        // virtual implementations
        std::string getShortName() const override;
        std::string getFullName() const override;

        static const std::map<matrix::ElementType, std::string> Elementtype2String;
        static const std::map<std::string, matrix::ElementType> String2Elementtype;
    };
}
