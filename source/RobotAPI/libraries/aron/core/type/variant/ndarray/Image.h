/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann (rainer dot kartmann at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD / STL
#include <memory>
#include <string>

// Base Class
#include "../detail/NDArrayVariant.h"

namespace armarx::aron::type
{
    /**
     * @brief The Image class. It represents the image type.
     * The code generation manages, which code will be generated for this type object, e.g. c++ generates opencv code.
     */
    class Image :
        public detail::NDArrayVariant<type::dto::Image, Image>
    {
    public:
        // constructors
        Image(const Path& path = {});
        Image(const type::dto::Image&, const Path& path);

        type::dto::ImagePtr toImageDTO() const;

        /// Get the pixel type.
        image::PixelType getPixelType() const;
        void setPixelType(const image::PixelType type) const;

        // virtual implementations
        std::string getShortName() const override;
        std::string getFullName() const override;

        static const std::map<image::PixelType, std::string> Pixeltype2String;
        static const std::map<std::string, image::PixelType> String2Pixeltype;
    };
}
