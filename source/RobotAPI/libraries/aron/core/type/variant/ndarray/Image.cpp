/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann (rainer dot kartmann at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Image.h"

#include <RobotAPI/libraries/aron/core/Exception.h>

#include <SimoxUtility/algorithm/string.h>
#include <SimoxUtility/meta/EnumNames.hpp>


namespace armarx::aron::type
{
    const std::map<image::PixelType, std::string> Image::Pixeltype2String
    {
        {image::PixelType::RGB24, "RGB24"},
        {image::PixelType::DEPTH32, "DEPTH32"}
    };
    const std::map<std::string, image::PixelType> Image::String2Pixeltype = conversion::util::InvertMap(Pixeltype2String);


    // constructors
    Image::Image(const Path& path) :
        detail::NDArrayVariant<type::dto::Image, Image>(type::Descriptor::IMAGE, path)
    {
    }


    Image::Image(const type::dto::Image& o, const Path& path) :
        detail::NDArrayVariant<type::dto::Image, Image>(o, type::Descriptor::IMAGE, path)
    {
    }

    type::dto::ImagePtr Image::toImageDTO() const
    {
        return this->aron;
    }


    image::PixelType Image::getPixelType() const
    {
        return aron->pixelType;
    }

    void Image::setPixelType(const image::PixelType pixelType) const
    {
        aron->pixelType = pixelType;
    }

    // virtual implementations
    std::string Image::getShortName() const
    {
        return "Image";
    }

    std::string Image::getFullName() const
    {
        return "armarx::aron::type::Image<" + Pixeltype2String.at(this->aron->pixelType) + ">";
    }
}

