/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>
#include <map>

// Base class
#include "../detail/NDArrayVariant.h"

namespace armarx::aron::type
{
    /**
     * @brief The PointCloud class. It represents the pointcloud type.
     * A pointcloud is defined through a type. The dimension is NOT part of the type description
     */
    class PointCloud :
        public detail::NDArrayVariant<type::dto::PointCloud, PointCloud>
    {
    public:
        // constructors
        PointCloud(const Path& path = Path());
        PointCloud(const type::dto::PointCloud&, const Path& path);

        // public member functions
        type::pointcloud::VoxelType getVoxelType() const;

        void setVoxelType(type::pointcloud::VoxelType);

        type::dto::PointCloudPtr toPointCloudDTO() const;

        // virtual implementations
        std::string getShortName() const override;
        std::string getFullName() const override;

        static const std::map<pointcloud::VoxelType, std::string> Voxeltype2String;
        static const std::map<std::string, pointcloud::VoxelType> String2Voxeltype;
    };
}
