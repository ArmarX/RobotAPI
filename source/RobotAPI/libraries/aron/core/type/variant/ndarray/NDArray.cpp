/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "NDArray.h"

namespace armarx::aron::type
{
    // constructors
    NDArray::NDArray(const Path& path) :
        detail::NDArrayVariant<type::dto::NDArray, NDArray>(type::Descriptor::NDARRAY, path)
    {
    }

    NDArray::NDArray(const type::dto::NDArray& o, const Path& path) :
        detail::NDArrayVariant<type::dto::NDArray, NDArray>(o, type::Descriptor::NDARRAY, path)
    {
    }

    type::dto::NDArrayPtr NDArray::toNDArrayDTO() const
    {
        return this->aron;
    }

    int NDArray::getNumberDimensions() const
    {
        return this->aron->ndimensions;
    }

    type::ndarray::ElementType NDArray::getElementType() const
    {
        return this->aron->elementType;
    }

    void NDArray::setElementType(type::ndarray::ElementType s)
    {
        this->aron->elementType = s;
    }

    void NDArray::setNumberDimensions(int v)
    {
        this->aron->ndimensions = v;
    }

    void NDArray::addDimension()
    {
        this->aron->ndimensions++;
    }

    // virtual implementations
    std::string NDArray::getShortName() const
    {
        return "NDArray";
    }

    std::string NDArray::getFullName() const
    {
        return "armarx::aron::type::NDArray";
    }
}

