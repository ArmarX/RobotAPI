/*
* This file is part of ArmarX.
*
* Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
* Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <vector>
#include <map>
#include <string>

// Simox
// #include <SimoxUtility/algorithm/string.h>

// ArmarX
#include <RobotAPI/libraries/aron/core/Path.h>
#include <RobotAPI/libraries/aron/core/Exception.h>
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/core/Descriptor.h>

namespace armarx::aron::type
{
    class VariantFactory;
    typedef std::unique_ptr<VariantFactory> VariantFactoryPtr;

    class Variant;
    typedef std::shared_ptr<Variant> VariantPtr;

    /**
     * @brief The Variant class. This is the parent class of all type variant objects. It provides basic methods to get the children of a type, to get the descriptor, etc.
     * Usually, you get a VariantPtr as input to your code. You can use the descriptor to find out, which specific type is behind a variant. It is type safe.
     * For casting, every variant implementation provides the DynamicCast method, which casts a pointer to the static used type.
     *
     * This implementation wraps around the underlying ice object (armarx::aron::type::dto::XXX). All data is stored in the ice handle.
     * However, for simplicity, container types also contain pointers to their children variants
     *
     * Each ice type object has a special member 'maybeType' which holds an enum containing the information of if this type is a pointer, an optional or something else.
     */
    class Variant
    {
    public:
        using PointerType = VariantPtr;

    public:
        // constructors
        Variant(const type::Descriptor& descriptor, const Path& path = Path()) :
            descriptor(descriptor),
            path(path)
        {
        }
        virtual ~Variant() = default;

        // operators
        virtual bool operator==(const Variant& other) const = 0;
        bool operator==(const VariantPtr& other) const
        {
            if (!other)
            {
                return false;
            }

            return *this == *other;
        }

        // static methods
        /// create a variant object from an dto object
        static VariantPtr FromAronDTO(const type::dto::GenericType&, const Path& = Path());

        // public methods
        type::Descriptor getDescriptor() const
        {
            return descriptor;
        }

        Path getPath() const
        {
            return path;
        }

        std::string pathToString() const
        {
            return path.toString();
        }

        // virtual methods
        /// naviate absolute
        virtual VariantPtr navigateAbsolute(const Path& path) const = 0;

        /// navigate relative
        virtual VariantPtr navigateRelative(const Path& path) const = 0;

        /// get a short name of this specific type
        virtual std::string getShortName() const = 0;

        /// get the full name of this specific type
        virtual std::string getFullName() const = 0;

        /// get all child elements
        virtual std::vector<VariantPtr> getChildren() const = 0;
        virtual size_t childrenSize() const = 0;

        /// set the maybetype of this type
        virtual void setMaybe(const type::Maybe m) = 0;

        /// get the maybe type
        virtual type::Maybe getMaybe() const = 0;

        /// convert this variant to a dto object.
        /// In most cases you have to use the specific conversion methods of each type implementation (e.g. to toIntDTO() to get an aron::type::dto::Int object)
        virtual type::dto::GenericTypePtr toAronDTO() const = 0;

    protected:
        const type::Descriptor descriptor;
        const Path path;

    private:
        static const VariantFactoryPtr FACTORY;
    };

    template <class T>
    concept isVariant = std::is_base_of<Variant, T>::value;
}
