#pragma once

#include <memory>

/**
 * forward declarations of ALL aron type objects
 */
namespace armarx::aron::type
{
    class Variant;
    using VariantPtr = std::shared_ptr<Variant>;

    using DictPtr = std::shared_ptr<class Dict>;
    using ListPtr = std::shared_ptr<class List>;
    using ObjectPtr = std::shared_ptr<class Object>;
    using PairPtr = std::shared_ptr<class Pair>;
    using TuplePtr = std::shared_ptr<class Tuple>;

    using NDArrayPtr = std::shared_ptr<class NDArray>;
    using MatrixPtr = std::shared_ptr<class Matrix>;
    using QuaternionPtr = std::shared_ptr<class Quaternion>;
    using ImagePtr = std::shared_ptr<class Image>;
    using PointCloudPtr = std::shared_ptr<class PointCloud>;

    using IntEnumPtr = std::shared_ptr<class IntEnum>;

    using IntPtr = std::shared_ptr<class Int>;
    using LongPtr = std::shared_ptr<class Long>;
    using FloatPtr = std::shared_ptr<class Float>;
    using DoublePtr = std::shared_ptr<class Double>;
    using StringPtr = std::shared_ptr<class String>;
    using BoolPtr = std::shared_ptr<class Bool>;

}

