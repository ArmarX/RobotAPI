/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Dict.h"


namespace armarx::aron::type
{
    // constructors
    Dict::Dict(const VariantPtr& acceptedType, const Path& path) :
        detail::ContainerVariant<type::dto::Dict, Dict>(type::Descriptor::DICT, path),
        acceptedType(acceptedType)
    {
        aron->acceptedType = acceptedType->toAronDTO();
    }

    Dict::Dict(const type::dto::Dict& o, const Path& path) :
        detail::ContainerVariant<type::dto::Dict, Dict>(o, type::Descriptor::DICT, path),
        acceptedType(FromAronDTO(*o.acceptedType, path.withAcceptedType()))
    {
    }

    VariantPtr Dict::getAcceptedType() const
    {
        ARMARX_CHECK_NOT_NULL(acceptedType);
        return acceptedType;
    }

    void Dict::setAcceptedType(const VariantPtr& a)
    {
        ARMARX_CHECK_NOT_NULL(a);
        aron->acceptedType = a->toAronDTO();
        acceptedType = a;
    }

    type::dto::DictPtr Dict::toDictDTO() const
    {
        return aron;
    }

    // virtual implementations
    std::vector<VariantPtr> Dict::getChildren() const
    {
        return {acceptedType};
    }

    size_t Dict::childrenSize() const
    {
        return 1;
    }

    std::string Dict::getShortName() const
    {
        return "Dict<" + acceptedType->getShortName() + ">";
    }

    std::string Dict::getFullName() const
    {
        return "armarx::aron::type::Dict<" + acceptedType->getFullName() + ">";
    }

    VariantPtr Dict::navigateAbsolute(const Path& path) const
    {
        if (!path.hasElement())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Could not navigate without a valid path", path);
        }

        if (path.size() != 1)
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Could not get more than 1 value from a dict.", path);
        }

        std::string el = path.getFirstElement();
        if (el != "::accepted_type")
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Could not find an element of a path.", el, path);
        }


        return acceptedType;
    }
}

