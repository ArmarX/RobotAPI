/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>
#include <map>

// Base Class
#include "../detail/ContainerVariant.h"


namespace armarx::aron::type
{
    class Object;
    using ObjectPtr = std::shared_ptr<Object>;

    /**
     * @brief The Object class. It represents the object type
     * An object may have different types for each member, here represented as a map, mapping from the member name (as string) to the type (as a variant)
     */
    class Object :
        public detail::ContainerVariant<type::dto::AronObject, Object>
    {
    public:
        // constructors
        Object(const std::string&, const std::vector<std::string>& templates = {}, const std::vector<std::string>& templateInstantiations = {}, const std::map<std::string, VariantPtr>& = {}, const Path& = Path());
        Object(const type::dto::AronObject&, const Path& = Path());

        static ObjectPtr FromAronObjectDTO(const type::dto::AronObjectPtr&);
        static type::dto::AronObjectPtr ToAronObjectDTO(const ObjectPtr&);

        // public member functions
        bool checkObjectName(const std::string&) const;

        std::map<std::string, VariantPtr> getMemberTypes() const;
        std::map<std::string, VariantPtr> getDirectMemberTypes() const;
        VariantPtr getMemberType(const std::string&) const;
        std::string getObjectName() const;
        std::shared_ptr<Object> getExtends() const;
        std::vector<std::string> getTemplates() const;
        std::vector<std::string> getTemplateInstantiations() const;

        void setObjectName(const std::string&);
        void setExtends(const std::shared_ptr<Object>&);
        void addMemberType(const std::string&, const VariantPtr&);
        void addTemplate(const std::string&) const;
        void addTemplateInstantiation(const std::string&) const;

        bool hasMemberType(const std::string&) const;

        std::string getObjectNameWithoutNamespace() const;
        std::string getObjectNameWithTemplates() const;
        std::string getObjectNameWithTemplateInstantiations() const;

        std::vector<std::string> getAllKeys() const;

        type::dto::AronObjectPtr toAronObjectDTO() const;

        // virtual implementations
        VariantPtr navigateAbsolute(const Path& path) const override;

        std::string getShortName() const override;
        std::string getFullName() const override;
        std::vector<VariantPtr> getChildren() const override;
        size_t childrenSize() const override;

    private:
        // members
        std::shared_ptr<Object> extends;
        std::map<std::string, VariantPtr> memberTypes;
    };
}
