/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>
#include <map>

// Base Class
#include "../detail/ContainerVariant.h"

namespace armarx::aron::type
{
    /**
     * @brief The Tuple class. It represents the tuple type
     * A tuple has a list of accepted types
     */
    class Tuple :
            public detail::ContainerVariant<type::dto::Tuple, Tuple>
    {
    public:
        // constructors
        Tuple(const std::vector<VariantPtr>& acceptedTypes, const Path& path = Path());
        Tuple(const type::dto::Tuple&, const Path& path = Path());

        // public member functions
        std::vector<VariantPtr> getAcceptedTypes() const;
        VariantPtr getAcceptedType(unsigned int i) const;
        void addAcceptedType(const VariantPtr&);

        type::dto::TuplePtr toTupleDTO() const;

        // virtual implementations
        VariantPtr navigateAbsolute(const Path& path) const override;

        std::string getShortName() const override;
        std::string getFullName() const override;
        std::vector<VariantPtr> getChildren() const override;
        size_t childrenSize() const override;

    private:
        std::vector<VariantPtr> acceptedTypes;
    };
}
