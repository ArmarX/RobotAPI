/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>
#include <map>

// Base Class
#include "../detail/ContainerVariant.h"

namespace armarx::aron::type
{
    /**
     * @brief The Dict class. It represents the dict type
     * A dict only has an accepted type (a variant)
     * for the corresponding data object @see data/variant/container/Dict.h
     */
    class Dict :
        public detail::ContainerVariant<type::dto::Dict, Dict>
    {
    public:
        // constructors
        Dict(const VariantPtr& acceptedType, const Path& path = Path());
        Dict(const type::dto::Dict&, const Path& path = Path());

        // public member functions
        VariantPtr getAcceptedType() const;
        void setAcceptedType(const VariantPtr&);

        type::dto::DictPtr toDictDTO() const;

        // virtual implementations
        VariantPtr navigateAbsolute(const Path& path) const override;

        std::string getShortName() const override;
        std::string getFullName() const override;
        std::vector<VariantPtr> getChildren() const override;
        size_t childrenSize() const override;

    private:
        // members
        VariantPtr acceptedType;
    };
}
