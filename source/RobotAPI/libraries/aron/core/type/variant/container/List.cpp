/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "List.h"

namespace armarx::aron::type
{
    // constructors
    List::List(const VariantPtr& acceptedType, const Path& path) :
        detail::ContainerVariant<type::dto::List, List>(type::Descriptor::LIST, path),
        acceptedType(acceptedType)
    {
        aron->acceptedType = acceptedType->toAronDTO();
    }

    List::List(const type::dto::List& o, const Path& path) :
        detail::ContainerVariant<type::dto::List, List>(o, type::Descriptor::LIST, path),
        acceptedType(FromAronDTO(*o.acceptedType, path.withAcceptedType()))
    {
    }

    // Member functions
    VariantPtr List::getAcceptedType() const
    {
        return acceptedType;
    }

    void List::setAcceptedType(const VariantPtr& a)
    {
        ARMARX_CHECK_NOT_NULL(a);
        aron->acceptedType = a->toAronDTO();
        acceptedType = a;
    }

    // static methods
    type::dto::ListPtr List::toListDTO() const
    {
        return aron;
    }

    // virtual implementations
    std::vector<VariantPtr> List::getChildren() const
    {
        return {acceptedType};
    }

    size_t List::childrenSize() const
    {
        return 1;
    }

    std::string List::getShortName() const
    {
        return "List<" + acceptedType->getShortName() + ">";
    }

    std::string List::getFullName() const
    {
        return "armarx::aron::type::List<" + acceptedType->getFullName() + ">";
    }

    VariantPtr List::navigateAbsolute(const Path& path) const
    {
        if (!path.hasElement())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Could not navigate without a valid path", path);
        }

        if (path.size() != 1)
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Could not get more than 1 value from a dict.", path);
        }

        std::string el = path.getFirstElement();
        if (el != "::accepted_type")
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Could not find an element of a path.", el, path);
        }


        return acceptedType;
    }
}
