/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Tuple.h"

#include <SimoxUtility/algorithm/string/string_conversion.h>

namespace armarx::aron::type
{
    // constructors
    Tuple::Tuple(const std::vector<VariantPtr>& acceptedTypes, const Path& path) :
        detail::ContainerVariant<type::dto::Tuple, Tuple>(type::Descriptor::TUPLE, path),
        acceptedTypes(acceptedTypes)
    {
        for (const auto& acceptedType : acceptedTypes)
        {
            aron->elementTypes.push_back(acceptedType->toAronDTO());
        }
    }

    Tuple::Tuple(const type::dto::Tuple& o, const Path& path) :
        detail::ContainerVariant<type::dto::Tuple, Tuple>(o, type::Descriptor::TUPLE, path)
    {
        unsigned int i = 0;
        for (const auto& t : o.elementTypes)
        {
            acceptedTypes.push_back(FromAronDTO(*t, path.withAcceptedTypeIndex(i++)));
        }
    }

    type::dto::TuplePtr Tuple::toTupleDTO() const
    {
        if (acceptedTypes.empty())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "No accepted types set", getPath());
        }
        return this->aron;
    }

    // public member functions
    std::vector<VariantPtr> Tuple::getAcceptedTypes() const
    {
        return acceptedTypes;
    }

    VariantPtr Tuple::getAcceptedType(unsigned int i) const
    {
        return acceptedTypes[i];
    }

    void Tuple::addAcceptedType(const VariantPtr& v)
    {
        ARMARX_CHECK_NOT_NULL(v);
        this->aron->elementTypes.push_back(v->toAronDTO());
        acceptedTypes.push_back(v);
    }

    // virtual implementations
    std::vector<VariantPtr> Tuple::getChildren() const
    {
        return acceptedTypes;
    }

    size_t Tuple::childrenSize() const
    {
        return acceptedTypes.size();
    }

    std::string Tuple::getShortName() const
    {
        std::vector<std::string> names;
        for (const auto& n : acceptedTypes)
        {
            names.push_back(n->getShortName());
        }
        return "Tuple<" + simox::alg::to_string(names, ", ") + ">";
    }

    std::string Tuple::getFullName() const
    {
        std::vector<std::string> names;
        for (const auto& n : acceptedTypes)
        {
            names.push_back(n->getFullName());
        }
        return "armarx::aron::type::Tuple<" + simox::alg::to_string(names, ", ") + ">";
    }

    VariantPtr Tuple::navigateAbsolute(const Path& path) const
    {
        if (!path.hasElement())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Could not navigate without a valid path", path);
        }

        std::string el = path.getFirstElement();
        for (unsigned int i = 0; i < acceptedTypes.size(); ++i)
        {
            if (el == "::accepted_type_" + std::to_string(i))
            {
                if (path.size() == 1)
                {
                    return acceptedTypes[i];
                }
                else
                {
                    Path next = path.withDetachedFirstElement();
                    return acceptedTypes[i]->navigateAbsolute(next);
                }
            }
        }
        throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Could not find an element of a path.", el, path);
    }
}

