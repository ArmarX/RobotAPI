#pragma once

#include "Dict.h"
#include "List.h"
#include "Object.h"
#include "Pair.h"
#include "Tuple.h"

/**
 * A convenience header to include all container aron files (full include, not forward declared)
 */
namespace armarx::aron::type
{

}
