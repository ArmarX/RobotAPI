/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>
#include <map>

// Base Class
#include "../detail/ContainerVariant.h"

namespace armarx::aron::type
{
    /**
     * @brief The Pair class. It represents the pair type
     * A pair has two accepted types
     */
    class Pair :
            public detail::ContainerVariant<type::dto::Pair, Pair>
    {
    public:
        // constructors
        Pair(const VariantPtr& acceptedType1, const VariantPtr& acceptedType2, const Path& path = Path());
        Pair(const type::dto::Pair&, const Path& path = Path());

        // public member functions
        std::pair<VariantPtr, VariantPtr> getAcceptedTypes() const;
        VariantPtr getFirstAcceptedType() const;
        VariantPtr getSecondAcceptedType() const;
        void addAcceptedType(const VariantPtr&);
        void setFirstAcceptedType(const VariantPtr&);
        void setSecondAcceptedType(const VariantPtr&);

        type::dto::PairPtr toPairDTO() const;

        // virtual implementations
        VariantPtr navigateAbsolute(const Path& path) const override;

        std::string getShortName() const override;
        std::string getFullName() const override;
        std::vector<VariantPtr> getChildren() const override;
        size_t childrenSize() const override;

    private:
        // members
        VariantPtr acceptedType1;
        VariantPtr acceptedType2;

    };
}
