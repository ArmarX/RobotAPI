/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Pair.h"


namespace armarx::aron::type
{
    // constructors
    Pair::Pair(const VariantPtr& acceptedType1, const VariantPtr& acceptedType2, const Path& path) :
        detail::ContainerVariant<type::dto::Pair, Pair>(type::Descriptor::PAIR, path),
        acceptedType1(acceptedType1),
        acceptedType2(acceptedType2)
    {
        aron->acceptedType1 = acceptedType1->toAronDTO();
        aron->acceptedType2 = acceptedType2->toAronDTO();
    }

    Pair::Pair(const type::dto::Pair& o, const Path& path) :
        detail::ContainerVariant<type::dto::Pair, Pair>(o, type::Descriptor::PAIR, path),
        acceptedType1(FromAronDTO(*o.acceptedType1, path.withAcceptedTypeIndex(0))),
        acceptedType2(FromAronDTO(*o.acceptedType2, path.withAcceptedTypeIndex(1)))
    {
    }

    // Member functions
    std::pair<VariantPtr, VariantPtr> Pair::getAcceptedTypes() const
    {
        return {acceptedType1, acceptedType2};
    }

    VariantPtr Pair::getFirstAcceptedType() const
    {
        return acceptedType1;
    }

    VariantPtr Pair::getSecondAcceptedType() const
    {
        return acceptedType2;
    }

    void Pair::addAcceptedType(const VariantPtr& n)
    {
        ARMARX_CHECK_NOT_NULL(n);

        if (acceptedType1 == nullptr)
        {
            acceptedType1 = n;
            return;
        }
        if (acceptedType2 == nullptr)
        {
            acceptedType2 = n;
            return;
        }
        throw error::AronException(__PRETTY_FUNCTION__, "Both types are already set", getPath());
    }

    void Pair::setFirstAcceptedType(const VariantPtr& n)
    {
        ARMARX_CHECK_NOT_NULL(n);
        acceptedType1 = n;
    }

    void Pair::setSecondAcceptedType(const VariantPtr& n)
    {
        ARMARX_CHECK_NOT_NULL(n);
        acceptedType2 = n;
    }

    type::dto::PairPtr Pair::toPairDTO() const
    {
        return this->aron;
    }

    // virtual implementations
    std::vector<VariantPtr> Pair::getChildren() const
    {
        return {acceptedType1, acceptedType2};
    }

    size_t Pair::childrenSize() const
    {
        return 2;
    }

    std::string Pair::getShortName() const
    {
        return "Pair<" + acceptedType1->getShortName() + ", " + acceptedType2->getShortName() + ">";
    }

    std::string Pair::getFullName() const
    {
        return "armarx::aron::type::Pair<" + acceptedType1->getFullName() + ", " + acceptedType2->getFullName() + ">";
    }

    VariantPtr Pair::navigateAbsolute(const Path& path) const
    {
        if (!path.hasElement())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Could not navigate without a valid path", path);
        }

        std::string el = path.getFirstElement();
        if (el == "::accepted_type_0")
        {
            if (path.size() == 1)
            {
                return acceptedType1;
            }
            else
            {
                Path next = path.withDetachedFirstElement();
                return acceptedType1->navigateAbsolute(next);
            }
        }
        else if (el == "::accepted_type_1")
        {
            if (path.size() == 1)
            {
                return acceptedType2;
            }
            else
            {
                Path next = path.withDetachedFirstElement();
                return acceptedType2->navigateAbsolute(next);
            }
        }
        else
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Could not find an element of a path.", el, path);
        }
    }
}

