/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Object.h"


#include <SimoxUtility/algorithm/get_map_keys_values.h>
#include <SimoxUtility/algorithm/string/string_conversion.h>


namespace armarx::aron::type
{

    // constructors
    Object::Object(const std::string& name, const std::vector<std::string>& templates, const std::vector<std::string>& templateInstantiations, const std::map<std::string, VariantPtr>& m, const Path& path) :
        detail::ContainerVariant<type::dto::AronObject, Object>(type::Descriptor::OBJECT, path),
        memberTypes(m)
    {
        aron->templates = templates;
        aron->templateInstantiations = templateInstantiations;
        setObjectName(name);
        for (const auto& [key, value] : memberTypes)
        {
            aron->elementTypes[key] = value->toAronDTO();
        }
    }

    Object::Object(const type::dto::AronObject& o, const Path& path) :
        detail::ContainerVariant<type::dto::AronObject, Object>(o, type::Descriptor::OBJECT, path)
    {
        setObjectName(o.objectName);
        for (const auto& [key, t] : o.elementTypes)
        {
            memberTypes[key] = FromAronDTO(*t, path.withElement(key));
        }
    }

    ObjectPtr Object::FromAronObjectDTO(const type::dto::AronObjectPtr& aron)
    {
        if (!aron)
        {
            return nullptr;
        }
        return std::make_shared<Object>(*aron);
    }

    type::dto::AronObjectPtr Object::ToAronObjectDTO(const ObjectPtr& aron)
    {
        if (!aron)
        {
            return nullptr;
        }
        return aron->toAronObjectDTO();
    }

    bool Object::checkObjectName(const std::string& s) const
    {
        if (s.empty())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "The object name is empty.", getPath());
        }

        return true;
    }

    // public member functions
    std::map<std::string, VariantPtr> Object::getMemberTypes() const
    {
        std::map<std::string, VariantPtr> ret = memberTypes;
        if (extends)
        {
            for (const auto& [key, t] : extends->getMemberTypes())
            {
                ret.insert({key, t});
            }
        }
        return ret;
    }

    std::map<std::string, VariantPtr> Object::getDirectMemberTypes() const
    {
        return memberTypes;
    }

    VariantPtr Object::getMemberType(const std::string& s) const
    {
        if (memberTypes.find(s) == memberTypes.end() and not (extends and extends->hasMemberType(s)))
        {
            throw error::ValueNotValidException("ObjectNavigator", "getMemberType", "Member not set. The list of all members is: " + simox::alg::to_string(simox::alg::get_keys(memberTypes)), s);
        }
        if (memberTypes.find(s) == memberTypes.end())
        {
            return extends->getMemberType(s);
        }
        return memberTypes.at(s);
    }

    void Object::addMemberType(const std::string& k, const VariantPtr& v)
    {
        if (k.empty())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Cannot set an element with an empty key.", getPath());
        }

        ARMARX_CHECK_NOT_NULL(v);

        this->aron->elementTypes[k] = v->toAronDTO();
        memberTypes[k] = v;
    }

    void Object::setObjectName(const std::string& n)
    {
        checkObjectName(n);
        //path.setRootIdentifier(n);
        this->aron->objectName = n;
    }

    void Object::setExtends(const std::shared_ptr<Object>& p)
    {
        ARMARX_CHECK_NOT_NULL(p);
        type::dto::AronObjectPtr ex = p->toAronObjectDTO();
        ARMARX_CHECK_NOT_NULL(ex);
        extends = p;
    }

    bool Object::hasMemberType(const std::string& k) const
    {
        return memberTypes.count(k) > 0 or (extends && extends->hasMemberType(k));
    }

    std::string Object::getObjectNameWithoutNamespace() const
    {
        std::vector<std::string> split = simox::alg::split(aron->objectName, "::");
        return split[split.size() -1];
    }

    std::string Object::getObjectNameWithTemplateInstantiations() const
    {
        if (aron->templateInstantiations.empty())
        {
            return getObjectName();
        }
        return getObjectName() + "<" + simox::alg::join(aron->templateInstantiations, ", ") + ">";
    }

    std::string Object::getObjectNameWithTemplates() const
    {
        if (aron->templates.empty())
        {
            return getObjectName();
        }
        return getObjectName() + "<" + simox::alg::join(aron->templates, ", ") + ">";
    }

    void Object::addTemplate(const std::string& s) const
    {
        if (std::find(aron->templates.begin(), aron->templates.end(), s) != aron->templates.end())
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The template already exists!", s);
        }
        aron->templates.push_back(s);
    }

    void Object::addTemplateInstantiation(const std::string& s) const
    {
        if (std::find(aron->templateInstantiations.begin(), aron->templateInstantiations.end(), s) != aron->templateInstantiations.end())
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The template arg already exists!", s);
        }
        aron->templateInstantiations.push_back(s);
    }

    std::vector<std::string> Object::getTemplates() const
    {
        return aron->templates;
    }

    std::vector<std::string> Object::getTemplateInstantiations() const
    {
        return aron->templateInstantiations;
    }

    std::string Object::getObjectName() const
    {
        return this->aron->objectName;
    }

    std::shared_ptr<Object> Object::getExtends() const
    {
        return extends;
    }

    std::vector<std::string> Object::getAllKeys() const
    {
        std::vector<std::string> ret;
        for (const auto& [k, _] : memberTypes)
        {
            ret.push_back(k);
        }
        if (extends)
        {
            for (const auto& s : extends->getAllKeys())
            {
                ret.push_back(s);
            }
        }
        return ret;
    }

    type::dto::AronObjectPtr Object::toAronObjectDTO() const
    {
        return this->aron;
    }

    // virtual implementations
    std::vector<VariantPtr> Object::getChildren() const
    {
        std::vector<VariantPtr> ret;
        for (const auto& [k, t] : memberTypes)
        {
            ret.push_back(t);
        }
        if (extends)
        {
            for (const auto& t : extends->getChildren())
            {
                ret.push_back(t);
            }
        }
        return ret;
    }

    size_t Object::childrenSize() const
    {
        return memberTypes.size();
    }

    std::string Object::getShortName() const
    {
        return "Object<" + this->aron->objectName + (extends ? (" : " + extends->getShortName()) : "") + ">";
    }

    std::string Object::getFullName() const
    {
        return "armarx::aron::type::Object<" + this->aron->objectName + (extends ? (" : " + extends->getFullName()) : "") + ">";
    }

    VariantPtr Object::navigateAbsolute(const Path& path) const
    {
        if (!path.hasElement())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Could not navigate without a valid path", path);
        }

        std::string el = path.getFirstElement();
        if (!hasMemberType(el))
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Could not find an element of a path.", el, path);
        }

        if (path.size() == 1)
        {
            return memberTypes.at(el);
        }
        else
        {
            Path next = path.withDetachedFirstElement();
            if (!memberTypes.at(el))
            {
                throw error::AronException(__PRETTY_FUNCTION__, "Could not navigate into a NULL member.", next);
            }
            return memberTypes.at(el)->navigateAbsolute(next);
        }
    }
}

