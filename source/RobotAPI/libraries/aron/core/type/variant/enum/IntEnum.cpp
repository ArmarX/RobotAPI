/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "IntEnum.h"

namespace armarx::aron::type
{
    // constructors
    IntEnum::IntEnum(const std::string& name, const std::map<std::string, int>& valueMap, const Path& path) :
        detail::EnumVariant<type::dto::IntEnum, IntEnum>(type::Descriptor::INT_ENUM, path)
    {
        aron->enumName = name;
        aron->acceptedValues = valueMap;
    }

    IntEnum::IntEnum(const type::dto::IntEnum& o, const Path& path) :
        detail::EnumVariant<type::dto::IntEnum, IntEnum>(o, type::Descriptor::INT_ENUM, path)
    {
    }

    std::map<std::string, int> IntEnum::getAcceptedValueMap() const
    {
        return this->aron->acceptedValues;
    }

    std::vector<std::string> IntEnum::getAcceptedValueNames() const
    {
        std::vector<std::string> names;
        for (const auto& [k, _] : aron->acceptedValues)
        {
            names.push_back(k);
        }
        return names;
    }

    std::vector<int> IntEnum::getAcceptedValues() const
    {
        std::vector<int> vals;
        for (const auto& [_, i] : aron->acceptedValues)
        {
            vals.push_back(i);
        }
        return vals;
    }

    std::string IntEnum::getValueName(int i) const
    {
        for (const auto& [k, v] : this->aron->acceptedValues)
        {
            if (v == i)
            {
                return k;
            }
        }
        throw error::AronException(__PRETTY_FUNCTION__, "Enum could not be resolved. Input was: " + std::to_string(i), getPath());
    }

    int IntEnum::getValue(const std::string& s) const
    {
        return this->aron->acceptedValues[s];
    }

    std::string IntEnum::getEnumName() const
    {
        return this->aron->enumName;
    }

    void IntEnum::setEnumName(const std::string& s)
    {
        this->aron->enumName = s;
    }

    void IntEnum::addAcceptedValue(const std::string& s, int i)
    {
        this->aron->acceptedValues[s] = i;
    }

    type::dto::IntEnumPtr IntEnum::toIntEnumDTO() const
    {
        return this->aron;
    }

    // virtual implementations
    std::string IntEnum::getShortName() const
    {
        return "IntEnum";
    }

    std::string IntEnum::getFullName() const
    {
        return "armarx::aron::type::IntEnum";
    }
}

