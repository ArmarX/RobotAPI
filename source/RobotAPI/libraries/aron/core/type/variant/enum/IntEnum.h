/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>
#include <map>

// Base class
#include "../detail/EnumVariant.h"

namespace armarx::aron::type
{
    class IntEnum;
    using IntEnumPtr = std::shared_ptr<IntEnum>;

    /**
     * @brief The IntEnum class. It represents the int enum type
     * An int enum contains a map from strings to int values
     */
    class IntEnum :
        public detail::EnumVariant<type::dto::IntEnum, IntEnum>
    {
    public:
        // constructors
        IntEnum(const std::string& name, const std::map<std::string, int>& valueMap, const Path& path = Path());
        IntEnum(const type::dto::IntEnum&, const Path& path = Path());

        // public member functions
        std::map<std::string, int> getAcceptedValueMap() const;
        std::vector<std::string> getAcceptedValueNames() const;
        std::vector<int> getAcceptedValues() const;

        std::string getValueName(int) const;
        int getValue(const std::string&) const;
        std::string getEnumName() const;

        void setEnumName(const std::string&);
        void addAcceptedValue(const std::string&, int);

        type::dto::IntEnumPtr toIntEnumDTO() const;

        // virtual implementations
        std::string getShortName() const override;
        std::string getFullName() const override;
    };
}
