/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL

// Header
#include "Variant.h"

// ArmarX
#include <RobotAPI/libraries/aron/core/type/variant/Factory.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>

namespace armarx::aron::type
{
    /* Navigator */
    // static data members
    const VariantFactoryPtr Variant::FACTORY = VariantFactoryPtr(new VariantFactory());

    VariantPtr Variant::FromAronDTO(const type::dto::GenericType& a, const Path& path)
    {
        return FACTORY->create(a, path);
    }
}


