/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// STD/STL
#include <string>
#include <map>

// Header
#include "Double.h"

namespace armarx::aron::type
{
    /* constructors */
    Double::Double(const Path& path) :
        detail::PrimitiveVariant<type::dto::AronDouble, Double>(type::Descriptor::DOUBLE, path)
    {
    }

    Double::Double(const type::dto::AronDouble&o, const Path& path) :
        detail::PrimitiveVariant<type::dto::AronDouble, Double>(o, type::Descriptor::DOUBLE, path)
    {
    }

    type::dto::AronDoublePtr Double::toDoubleDTO() const
    {
        return this->aron;
    }

    /* virtual implementations */
    std::string Double::getShortName() const
    {
        return "Double";
    }

    std::string Double::getFullName() const
    {
        return "armarx::aron::type::Double";
    }
}
