/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>

// Base class
#include "../detail/PrimitiveVariant.h"

namespace armarx::aron::type
{
    /**
     * @brief The String class. It represents the string type
     */
    class String :
        public detail::PrimitiveVariant<type::dto::AronString, String>
    {
    public:
        /* constructors */
        String(const Path& = Path());
        String(const type::dto::AronString&, const Path& = Path());

        type::dto::AronStringPtr toStringDTO() const;

        /* virtual implementations */
        virtual std::string getShortName() const override;
        virtual std::string getFullName() const override;
    };
}
