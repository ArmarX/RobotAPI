/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// STD/STL
#include <string>
#include <map>

// Header
#include "Float.h"

namespace armarx::aron::type
{
    /* constructors */
    Float::Float(const Path& path) :
        detail::PrimitiveVariant<type::dto::AronFloat, Float>(type::Descriptor::FLOAT, path)
    {
    }

    Float::Float(const type::dto::AronFloat&o, const Path& path) :
        detail::PrimitiveVariant<type::dto::AronFloat, Float>(o, type::Descriptor::FLOAT, path)
    {
    }

    type::dto::AronFloatPtr Float::toFloatDTO() const
    {
        return this->aron;
    }

    /* virtual implementations */
    std::string Float::getShortName() const
    {
        return "Float";
    }

    std::string Float::getFullName() const
    {
        return "armarx::aron::type::Float";
    }
}
