/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>

// Base class
#include "../detail/PrimitiveVariant.h"

namespace armarx::aron::type
{
    /**
     * @brief The Bool class. It represents the bool type
     */
    class Bool :
        public detail::PrimitiveVariant<type::dto::AronBool, Bool>
    {
    public:
        /* constructors */
        Bool(const Path& = Path());
        Bool(const type::dto::AronBool&, const Path& = Path());

        type::dto::AronBoolPtr toBoolDTO() const;

        /* virtual implementations */
        virtual std::string getShortName() const override;
        virtual std::string getFullName() const override;
    };
}
