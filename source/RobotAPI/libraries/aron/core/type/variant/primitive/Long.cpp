/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// STD/STL
#include <string>
#include <map>

// Header
#include "Long.h"

namespace armarx::aron::type
{
    /* constructors */
    Long::Long(const Path& path) :
        detail::PrimitiveVariant<type::dto::AronLong, Long>(type::Descriptor::LONG, path)
    {
    }

    Long::Long(const type::dto::AronLong&o, const Path& path) :
        detail::PrimitiveVariant<type::dto::AronLong, Long>(o, type::Descriptor::LONG, path)
    {
    }

    type::dto::AronLongPtr Long::toLongDTO() const
    {
        return this->aron;
    }

    /* virtual implementations */
    std::string Long::getShortName() const
    {
        return "Long";
    }

    std::string Long::getFullName() const
    {
        return "armarx::aron::type::Long";
    }
}
