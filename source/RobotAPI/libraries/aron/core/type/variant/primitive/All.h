#pragma once

#include "Int.h"
#include "Long.h"
#include "Float.h"
#include "Double.h"
#include "String.h"
#include "Bool.h"

/**
 * A convenience header to include all primitive aron files (full include, not forward declared)
 */
namespace armarx::aron::type
{

}
