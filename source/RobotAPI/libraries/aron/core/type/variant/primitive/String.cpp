/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// STD/STL
#include <string>
#include <map>

// Header
#include "String.h"

namespace armarx::aron::type
{
    /* constructors */
    String::String(const Path& path) :
        detail::PrimitiveVariant<type::dto::AronString, String>(type::Descriptor::STRING, path)
    {
    }

    String::String(const type::dto::AronString&o, const Path& path) :
        detail::PrimitiveVariant<type::dto::AronString, String>(o, type::Descriptor::STRING, path)
    {
    }

    /* public member functions */
    type::dto::AronStringPtr String::toStringDTO() const
    {
        return aron;
    }

    /* virtual implementations */
    std::string String::getShortName() const
    {
        return "String";
    }

    std::string String::getFullName() const
    {
        return "armarx::aron::type::String";
    }
}
