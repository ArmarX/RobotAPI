﻿/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <string>

// ArmarX
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/core/Exception.h>


namespace armarx::aron::type
{
    /**
     * @brief The ReaderInterface class. It defines the interface to extract information from an aron representation
     */
    template <class I>
    class ReaderInterface
    {
    public:
        using InputType = I;
        using InputTypeNonConst = typename std::remove_const<InputType>::type;

        virtual ~ReaderInterface() = default;

        virtual type::Descriptor getDescriptor(InputType& input) = 0;

        /// Extract information from an Object type
        virtual void readObject(const InputType& input, std::string& name, std::vector<std::string>& templates, std::vector<std::string>& templateInstantiations, std::map<std::string, InputTypeNonConst>& memberTypes, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from a list type
        virtual void readList(const InputType& input, InputTypeNonConst& acceptedType, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from a dict type
        virtual void readDict(const InputType& input, InputTypeNonConst& acceptedType, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from a tuple type
        virtual void readTuple(const InputType& input, std::vector<InputTypeNonConst>& acceptedTypes, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from a pair type
        virtual void readPair(const InputType& input, InputTypeNonConst& acceptedTypes1, InputTypeNonConst& acceptedTypes2, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from a ndarray type
        virtual void readNDArray(const InputType& input, int& ndim, type::ndarray::ElementType& type, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from a matrix type
        virtual void readMatrix(const InputType& input, int& rows, int& cols, type::matrix::ElementType& type, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from a quaternion type
        virtual void readQuaternion(const InputType& input, type::quaternion::ElementType& type, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from a pointcloud type
        virtual void readPointCloud(const InputType& input, type::pointcloud::VoxelType& type, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from an image type
        virtual void readImage(const InputType& input, type::image::PixelType& type, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from an int enum type
        virtual void readIntEnum(const InputType& input, std::string& name, std::map<std::string, int>& acceptedValues, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from an int type
        virtual void readInt(const InputType& input, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from an long type
        virtual void readLong(const InputType& input, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from an float type
        virtual void readFloat(const InputType& input, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from an double type
        virtual void readDouble(const InputType& input, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from an string type
        virtual void readString(const InputType& input, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from an bool type
        virtual void readBool(const InputType& input, type::Maybe& maybe, Path& p) = 0;

        /// Extract information from an time type
        virtual void readAnyObject(const InputType& input, type::Maybe& maybe, Path& p) = 0;

        /// Check if input is null
        virtual bool readNull(InputType& input) // defaulted implementation
        {
            InputType nil = {};
            return input == nil;
        }

        // Convenience methods without path
        /*void readObject(const InputType& input, std::string& name, std::map<std::string, InputTypeNonConst>& memberTypes, type::Maybe& maybe)
        {
            Path p;
            return readObject(input, name, memberTypes, maybe, p);
        }

        void readList(const InputType& input, InputTypeNonConst& acceptedType, type::Maybe& maybe)
        {
            Path p;
            return readList(input, acceptedType, maybe, p);
        }

        void readDict(const InputType& input, InputTypeNonConst& acceptedType, type::Maybe& maybe)
        {
            Path p;
            return readDict(input, acceptedType, maybe, p);
        }

        void readTuple(const InputType& input, std::vector<InputTypeNonConst>& acceptedTypes, type::Maybe& maybe)
        {
            Path p;
            return readTuple(input, acceptedTypes, maybe, p);
        }

        void readPair(const InputType& input, InputTypeNonConst& acceptedTypes1, InputTypeNonConst& acceptedTypes2, type::Maybe& maybe)
        {
            Path p;
            return readPair(input, acceptedTypes1, acceptedTypes2, maybe, p);
        }

        void readNDArray(const InputType& input, int& ndim, type::ndarray::ElementType& type, type::Maybe& maybe)
        {
            Path p;
            return readNDArray(input, ndim, type, maybe, p);
        }

        void readMatrix(const InputType& input, int& rows, int& cols, type::matrix::ElementType& type, type::Maybe& maybe)
        {
            Path p;
            return readMatrix(input, rows, cols, type, maybe, p);
        }

        void readQuaternion(const InputType& input, type::quaternion::ElementType& type, type::Maybe& maybe)
        {
            Path p;
            return readQuaternion(input, type, maybe, p);
        }

        void readPointCloud(const InputType& input, type::pointcloud::VoxelType& type, type::Maybe& maybe)
        {
            Path p;
            return readPointCloud(input, type, maybe, p);
        }

        void readImage(const InputType& input, type::image::PixelType& type, type::Maybe& maybe)
        {
            Path p;
            return readImage(input, type, maybe, p);
        }

        void readIntEnum(const InputType& input, std::string& name, std::map<std::string, int>& acceptedValues, type::Maybe& maybe)
        {
            Path p;
            return readIntEnum(input, name, acceptedValues, maybe, p);
        }

        void readInt(const InputType& input, type::Maybe& maybe)
        {
            Path p;
            return readInt(input, maybe, p);
        }

        void readLong(const InputType& input, type::Maybe& maybe)
        {
            Path p;
            return readLong(input, maybe, p);
        }

        void readFloat(const InputType& input, type::Maybe& maybe)
        {
            Path p;
            return readFloat(input, maybe, p);
        }

        void readDouble(const InputType& input, type::Maybe& maybe)
        {
            Path p;
            return readDouble(input, maybe, p);
        }

        void readString(const InputType& input, type::Maybe& maybe)
        {
            Path p;
            return readString(input, maybe, p);
        }

        void readBool(const InputType& input, type::Maybe& maybe)
        {
            Path p;
            return readBool(input, maybe, p);
        }

        void readAnyObject(const InputType& input, type::any::AnyObjectType& t, type::Maybe& maybe)
        {
            Path p;
            return readBool(input, t, maybe, p);
        }*/
    };

    template <class T>
    concept isReader = std::is_base_of<ReaderInterface<typename T::InputType>, T>::value;
}
