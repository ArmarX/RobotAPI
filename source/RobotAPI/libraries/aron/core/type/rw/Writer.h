/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <string>
#include <optional>

// ArmarX
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/core/Exception.h>


namespace armarx::aron::type
{
    /**
     * @brief The WriterInterface class. It defines the interface to construct aron representations (e.g. variant or nlohmann::json)
     */
    template <class R>
    class WriterInterface
    {
    public:
        using ReturnType = R;
        using ReturnTypeConst = typename std::add_const<ReturnType>::type;

        virtual ~WriterInterface() = default;

        virtual type::Descriptor getDescriptor(ReturnTypeConst& input) = 0;

        /// Construct an object from the params
        virtual ReturnType writeObject(const std::string& name, const std::vector<std::string>& templates, const std::vector<std::string>& templateInstantiations, const std::map<std::string, ReturnType>& memberTypes, const std::optional<ReturnType>& extends, const type::Maybe maybe, const Path& p) = 0;

        /// Construct a list from the params
        virtual ReturnType writeList(const ReturnType& acceptedType, const type::Maybe maybe, const Path& p) = 0;

        /// Construct a dict from the params
        virtual ReturnType writeDict(const ReturnType& acceptedType, const type::Maybe maybe, const Path& p) = 0;

        /// Construct a pair from the params
        virtual ReturnType writePair(const ReturnType& acceptedType1, const ReturnType& acceptedType2, const type::Maybe maybe, const Path& p) = 0;

        /// Construct a tuple from the params
        virtual ReturnType writeTuple(const std::vector<ReturnType>& acceptedTypes, const type::Maybe maybe, const Path& p) = 0;

        /// Construct a ndarray from the params
        virtual ReturnType writeNDArray(const int ndim, const type::ndarray::ElementType, const type::Maybe maybe, const Path& p) = 0;

        /// Construct a matrix from the params
        virtual ReturnType writeMatrix(const int rows, const int cols, const type::matrix::ElementType type, const type::Maybe maybe, const Path& p) = 0;

        /// Construct a quaternion from the params
        virtual ReturnType writeQuaternion(const type::quaternion::ElementType, const type::Maybe maybe, const Path& p) = 0;

        /// Construct a image from the params
        virtual ReturnType writeImage(const type::image::PixelType, const type::Maybe maybe, const Path& p) = 0;

        /// Construct a pointcloud from the params
        virtual ReturnType writePointCloud(const type::pointcloud::VoxelType, const type::Maybe maybe, const Path& p) = 0;

        /// Construct a int enum from the params
        virtual ReturnType writeIntEnum(const std::string& name, const std::map<std::string, int>& acceptedValues, const type::Maybe maybe, const Path& p) = 0;

        /// Construct a int from the params
        virtual ReturnType writeInt(const type::Maybe maybe, const Path& p) = 0;

        /// Construct a long from the params
        virtual ReturnType writeLong(const type::Maybe maybe, const Path& p) = 0;

        /// Construct a float from the params
        virtual ReturnType writeFloat(const type::Maybe maybe, const Path& p) = 0;

        /// Construct a double from the params
        virtual ReturnType writeDouble(const type::Maybe maybe, const Path& p) = 0;

        /// Construct a string from the params
        virtual ReturnType writeString(const type::Maybe maybe, const Path& p) = 0;

        /// Construct a bool from the params
        virtual ReturnType writeBool(const type::Maybe maybe, const Path& p) = 0;

        /// Construct a time from the params
        virtual ReturnType writeAnyObject(const type::Maybe maybe, const Path& p) = 0;

        /// write a null
        virtual ReturnType writeNull(const Path& p = Path()) // defaulted implementation
        {
            (void) p;
            return {};
        }
    };

    template <class T>
    concept isWriter = std::is_base_of<WriterInterface<typename T::ReturnType>, T>::value;
}
