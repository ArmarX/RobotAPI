/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// STD/STL
#include <memory>
#include <numeric>

// Header
#include "NlohmannJSONReader.h"

// ArmarX
#include <RobotAPI/libraries/aron/core/Exception.h>
#include "../../json/Data.h"
#include <RobotAPI/libraries/aron/core/type/visitor/nlohmannJSON/NlohmannJSONVisitor.h>

namespace armarx::aron::type::reader
{
    namespace
    {
        /// Throw an exception if the type is other than expected
        void getAronMetaInformationForType(const nlohmann::json& input, const std::string& expectedType, Path& p)
        {
            if (input[rw::json::constantes::TYPE_SLUG] != expectedType)
            {
                throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Wrong type in json encountered.", input[rw::json::constantes::TYPE_SLUG], expectedType);
            }

            // get Path
            std::vector<std::string> pathElements = input[rw::json::constantes::PATH_SLUG];
            p = Path(pathElements);
        }
    }

    type::Descriptor NlohmannJSONReader::getDescriptor(InputType& input)
    {
        return ConstNlohmannJSONVisitor::GetDescriptor(input);
    }

    void NlohmannJSONReader::readObject(const nlohmann::json& input, std::string& name, std::vector<std::string>& templates, std::vector<std::string>& templateInstantiations, std::map<std::string, nlohmann::json>& memberTypes, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::OBJECT_TYPENAME_SLUG, p);

        name = input[rw::json::constantes::NAME_SLUG];
        if (input.count(rw::json::constantes::TEMPLATES_SLUG))
        {
            templates = input[rw::json::constantes::TEMPLATES_SLUG].get<std::vector<std::string>>();
        }
        if (input.count(rw::json::constantes::TEMPLATE_INSTANTIATIONS_SLUG))
        {
            templateInstantiations = input[rw::json::constantes::TEMPLATE_INSTANTIATIONS_SLUG].get<std::vector<std::string>>();
        }

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);

        memberTypes = input[rw::json::constantes::MEMBERS_SLUG].get<std::map<std::string, nlohmann::json>>();
    }
    void NlohmannJSONReader::readList(const nlohmann::json& input, nlohmann::json& acceptedType, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::LIST_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
        acceptedType = input[rw::json::constantes::ACCEPTED_TYPE_SLUG];
    }

    void NlohmannJSONReader::readDict(const nlohmann::json& input, nlohmann::json& acceptedType, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::DICT_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
        acceptedType = input[rw::json::constantes::ACCEPTED_TYPE_SLUG];
    }

    void NlohmannJSONReader::readTuple(const nlohmann::json& input, std::vector<nlohmann::json>& acceptedTypes, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::TUPLE_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
        acceptedTypes = input[rw::json::constantes::ACCEPTED_TYPE_SLUG].get<std::vector<nlohmann::json>>();
    }

    void NlohmannJSONReader::readPair(const nlohmann::json& input, nlohmann::json& acceptedType1, nlohmann::json& acceptedType2, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::PAIR_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
        auto list = input[rw::json::constantes::ACCEPTED_TYPE_SLUG].get<std::vector<nlohmann::json>>();
        acceptedType1 = list[0];
        acceptedType2 = list[1];
    }

    void NlohmannJSONReader::readNDArray(const nlohmann::json& input, int& ndim, type::ndarray::ElementType& type, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::NDARRAY_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
        ndim = input[rw::json::constantes::DIMENSIONS_SLUG];

        std::string t = input[rw::json::constantes::USED_TYPE_SLUG];
        type = armarx::aron::type::rw::json::conversion::String2NDArrayType.at(t);
    }

    void NlohmannJSONReader::readMatrix(const nlohmann::json& input, int& rows, int& cols, type::matrix::ElementType& type, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::MATRIX_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
        auto list = input[rw::json::constantes::DIMENSIONS_SLUG].get<std::vector<nlohmann::json>>();
        rows = list[0];
        cols = list[1];

        std::string t = input[rw::json::constantes::USED_TYPE_SLUG];
        type = armarx::aron::type::rw::json::conversion::String2MatrixType.at(t);
    }

    void NlohmannJSONReader::readQuaternion(const nlohmann::json& input, type::quaternion::ElementType& type, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::QUATERNION_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);

        std::string t = input[rw::json::constantes::USED_TYPE_SLUG];
        type = armarx::aron::type::rw::json::conversion::String2QuaternionType.at(t);
    }

    void NlohmannJSONReader::readPointCloud(const nlohmann::json& input, type::pointcloud::VoxelType& type, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::POINT_CLOUD_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);

        std::string t = input[rw::json::constantes::USED_TYPE_SLUG];
        type = armarx::aron::type::rw::json::conversion::String2VoxelType.at(t);
    }

    void NlohmannJSONReader::readImage(const nlohmann::json& input, type::image::PixelType& type, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::IMAGE_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);

        std::string t = input[rw::json::constantes::USED_TYPE_SLUG];
        type = armarx::aron::type::rw::json::conversion::String2PixelType.at(t);
    }

    void NlohmannJSONReader::readIntEnum(const nlohmann::json& input, std::string& name, std::map<std::string, int>& acceptedValues, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::INT_ENUM_TYPENAME_SLUG, p);

        name = input[rw::json::constantes::NAME_SLUG];
        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);

        acceptedValues = input[rw::json::constantes::ELEMENTS_SLUG].get<std::map<std::string, int>>();
    }

    void NlohmannJSONReader::readInt(const nlohmann::json& input, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::INT_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
    }

    void NlohmannJSONReader::readLong(const nlohmann::json& input, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::LONG_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
    }

    void NlohmannJSONReader::readFloat(const nlohmann::json& input, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::FLOAT_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
    }

    void NlohmannJSONReader::readDouble(const nlohmann::json& input, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::DOUBLE_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
    }

    void NlohmannJSONReader::readString(const nlohmann::json& input, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::STRING_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
    }

    void NlohmannJSONReader::readBool(const nlohmann::json& input, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::BOOL_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
    }

    void NlohmannJSONReader::readAnyObject(const nlohmann::json& input, type::Maybe& maybe, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::ANY_OBJECT_TYPENAME_SLUG, p);

        maybe = rw::json::conversion::String2Maybe.at(input[rw::json::constantes::MAYBE_SLUG]);
    }
}
