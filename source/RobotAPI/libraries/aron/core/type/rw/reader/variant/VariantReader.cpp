/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// STD/STL
#include <memory>
#include <numeric>

// Header
#include "VariantReader.h"

// ArmarX
#include <RobotAPI/libraries/aron/core/Exception.h>
#include <RobotAPI/libraries/aron/core/type/variant/All.h>
#include <RobotAPI/libraries/aron/core/type/visitor/variant/VariantVisitor.h>

namespace armarx::aron::type::reader
{

    type::Descriptor VariantReader::getDescriptor(InputType& input)
    {
        return ConstVariantVisitor::GetDescriptor(input);
    }

    void VariantReader::readObject(const aron::type::VariantPtr& input, std::string& name, std::vector<std::string>& templates, std::vector<std::string>& templateInstantiations, std::map<std::string, aron::type::VariantPtr>& memberTypes, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::Object::DynamicCastAndCheck(input);

        name = o->getObjectName();
        templates = o->getTemplates();
        templateInstantiations = o->getTemplateInstantiations();
        maybe = o->getMaybe();
        memberTypes = o->getMemberTypes();
        p = o->getPath();
    }
    void VariantReader::readList(const aron::type::VariantPtr& input, aron::type::VariantPtr& acceptedType, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::List::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        acceptedType = o->getAcceptedType();
        p = o->getPath();
    }

    void VariantReader::readDict(const aron::type::VariantPtr& input, aron::type::VariantPtr& acceptedType, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::Dict::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        acceptedType = o->getAcceptedType();
        p = o->getPath();
    }

    void VariantReader::readTuple(const aron::type::VariantPtr& input, std::vector<aron::type::VariantPtr>& acceptedTypes, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::Tuple::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        acceptedTypes = o->getAcceptedTypes();
        p = o->getPath();
    }

    void VariantReader::readPair(const aron::type::VariantPtr& input, aron::type::VariantPtr& acceptedType1, aron::type::VariantPtr& acceptedType2, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::Pair::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        acceptedType1 = o->getAcceptedTypes().first;
        acceptedType2 = o->getAcceptedTypes().second;
        p = o->getPath();
    }

    void VariantReader::readNDArray(const aron::type::VariantPtr& input, int& ndim, type::ndarray::ElementType& type, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::NDArray::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        ndim = o->getNumberDimensions();
        type = o->getElementType();
        p = o->getPath();
    }

    void VariantReader::readMatrix(const aron::type::VariantPtr& input, int& rows, int& cols, type::matrix::ElementType& type, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::Matrix::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        rows = o->getRows();
        cols = o->getCols();
        type = o->getElementType();
        p = o->getPath();
    }

    void VariantReader::readQuaternion(const aron::type::VariantPtr& input, type::quaternion::ElementType& type, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::Quaternion::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        type = o->getElementType();
        p = o->getPath();
    }

    void VariantReader::readPointCloud(const aron::type::VariantPtr& input, type::pointcloud::VoxelType& type, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::PointCloud::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        type = o->getVoxelType();
        p = o->getPath();
    }

    void VariantReader::readImage(const aron::type::VariantPtr& input, type::image::PixelType& type, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::Image::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        type = o->getPixelType();
        p = o->getPath();
    }

    void VariantReader::readIntEnum(const aron::type::VariantPtr& input, std::string& name, std::map<std::string, int>& acceptedValues, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::IntEnum::DynamicCastAndCheck(input);

        name = o->getEnumName();
        acceptedValues = o->getAcceptedValueMap();
        maybe = o->getMaybe();
        p = o->getPath();
    }

    void VariantReader::readInt(const aron::type::VariantPtr& input, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::Int::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        p = o->getPath();
    }

    void VariantReader::readLong(const aron::type::VariantPtr& input, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::Long::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        p = o->getPath();
    }

    void VariantReader::readFloat(const aron::type::VariantPtr& input, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::Float::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        p = o->getPath();
    }

    void VariantReader::readDouble(const aron::type::VariantPtr& input, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::Double::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        p = o->getPath();
    }

    void VariantReader::readString(const aron::type::VariantPtr& input, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::String::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        p = o->getPath();
    }

    void VariantReader::readBool(const aron::type::VariantPtr& input, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::Bool::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        p = o->getPath();
    }

    void VariantReader::readAnyObject(const aron::type::VariantPtr& input, type::Maybe& maybe, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = type::AnyObject::DynamicCastAndCheck(input);

        maybe = o->getMaybe();
        p = o->getPath();
    }
}
