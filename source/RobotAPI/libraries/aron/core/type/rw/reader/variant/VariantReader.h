/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <stack>

// BaseClass
#include <RobotAPI/libraries/aron/core/type/rw/Reader.h>

// ArmarX
#include <RobotAPI/libraries/aron/core/type/variant/Variant.h>


namespace armarx::aron::type::reader
{
    class VariantReader :
        public ReaderInterface<const aron::type::VariantPtr>
    {
    public:
        // constructors
        VariantReader() = default;

        type::Descriptor getDescriptor(InputType& input) final;

        void readObject(InputType& input, std::string& name, std::vector<std::string>& templates, std::vector<std::string>& templateInstantiations, std::map<std::string, InputTypeNonConst>& memberTypes, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readList(InputType& input, InputTypeNonConst& acceptedType, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readDict(InputType& input, InputTypeNonConst& acceptedType, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readTuple(InputType& input, std::vector<InputTypeNonConst>& acceptedTypes, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readPair(InputType& input, InputTypeNonConst& acceptedType1, InputTypeNonConst& acceptedType2, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;

        void readNDArray(InputType& input, int& ndim, type::ndarray::ElementType& type, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readMatrix(InputType& input, int& rows, int& cols, type::matrix::ElementType& type, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readQuaternion(InputType& input, type::quaternion::ElementType& type, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readPointCloud(InputType& input, type::pointcloud::VoxelType& type, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readImage(InputType& input, type::image::PixelType& type, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;

        void readIntEnum(InputType& input, std::string& name, std::map<std::string, int>& acceptedValues, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;

        void readInt(InputType& input, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readLong(InputType& input, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readFloat(InputType& input, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readDouble(InputType& input, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readString(InputType& input, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
        void readBool(InputType& input, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;

        void readAnyObject(InputType& input, type::Maybe& maybe, Path& p = *std::unique_ptr<Path>(new Path())) override;
    };
}
