/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller (fabian dot peller at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "NlohmannJSONWriter.h"

// Constantes
#include "../../json/Data.h"
#include <RobotAPI/libraries/aron/core/type/visitor/nlohmannJSON/NlohmannJSONVisitor.h>


namespace armarx::aron::type::writer
{
    namespace
    {
        /// Set important members for json object (aron meta information)
        void setupAronMetaInformationForType(nlohmann::json& json, const std::string& type, const type::Maybe& maybe, const Path& p)
        {
            json[rw::json::constantes::TYPE_SLUG] = type;
            json[rw::json::constantes::PATH_SLUG] = p.getPath();
            json[rw::json::constantes::MAYBE_SLUG] = rw::json::conversion::Maybe2String.at(maybe);
        }
    }

    type::Descriptor NlohmannJSONWriter::getDescriptor(ReturnTypeConst& input)
    {
        return ConstNlohmannJSONVisitor::GetDescriptor(input);
    }

    nlohmann::json NlohmannJSONWriter::writeObject(const std::string& name, const std::vector<std::string>& templates, const std::vector<std::string>& templateInstantiations, const std::map<std::string, nlohmann::json>& memberTypes, const std::optional<nlohmann::json>& extends, const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::OBJECT_TYPENAME_SLUG, maybe, p);
        o[rw::json::constantes::NAME_SLUG] = name;
        if (extends.has_value())
        {
            o[rw::json::constantes::EXTENDS_SLUG] = *extends;
        }
        if (!templates.empty())
        {
            o[rw::json::constantes::TEMPLATES_SLUG] = templates;
        }
        if (!templateInstantiations.empty())
        {
            o[rw::json::constantes::TEMPLATE_INSTANTIATIONS_SLUG] = templateInstantiations;
        }

        o[rw::json::constantes::MEMBERS_SLUG] = nlohmann::json(nlohmann::json::value_t::object);
        for (const auto& [key, value] : memberTypes)
        {
            o[rw::json::constantes::MEMBERS_SLUG][key] = value;
        }
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeList(const nlohmann::json& acceptedType, const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::LIST_TYPENAME_SLUG, maybe, p);
        o[rw::json::constantes::ACCEPTED_TYPE_SLUG] = acceptedType;
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeDict(const nlohmann::json& acceptedType, const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::DICT_TYPENAME_SLUG, maybe, p);
        o[rw::json::constantes::ACCEPTED_TYPE_SLUG] = acceptedType;
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writePair(const nlohmann::json& acceptedType1, const nlohmann::json& acceptedType2, const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::PAIR_TYPENAME_SLUG, maybe, p);
        o[rw::json::constantes::ACCEPTED_TYPE_SLUG] = {acceptedType1, acceptedType2};
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeTuple(const std::vector<nlohmann::json>& acceptedTypes, const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::TUPLE_TYPENAME_SLUG, maybe, p);
        o[rw::json::constantes::ACCEPTED_TYPE_SLUG] = acceptedTypes;
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeNDArray(const int ndim, const type::ndarray::ElementType type, const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::NDARRAY_TYPENAME_SLUG, maybe, p);
        o[rw::json::constantes::USED_TYPE_SLUG] = rw::json::conversion::NDArrayType2String.at(type);
        o[rw::json::constantes::DIMENSIONS_SLUG] = ndim;
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeMatrix(const int rows, const int cols, const type::matrix::ElementType type, const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::MATRIX_TYPENAME_SLUG, maybe, p);
        o[rw::json::constantes::USED_TYPE_SLUG] = rw::json::conversion::MatrixType2String.at(type);
        o[rw::json::constantes::DIMENSIONS_SLUG] = {rows, cols};
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeQuaternion(const type::quaternion::ElementType type, const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::QUATERNION_TYPENAME_SLUG, maybe, p);
        o[rw::json::constantes::USED_TYPE_SLUG] = rw::json::conversion::QuaternionType2String.at(type);
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeImage(const type::image::PixelType type, const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::IMAGE_TYPENAME_SLUG, maybe, p);
        o[rw::json::constantes::USED_TYPE_SLUG] = rw::json::conversion::PixelType2String.at(type);
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writePointCloud(const type::pointcloud::VoxelType type, const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::POINT_CLOUD_TYPENAME_SLUG, maybe, p);
        o[rw::json::constantes::USED_TYPE_SLUG] = rw::json::conversion::VoxelType2String.at(type);
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeIntEnum(const std::string& name, const std::map<std::string, int>& acceptedValues, const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::INT_ENUM_TYPENAME_SLUG, maybe, p);
        o[rw::json::constantes::NAME_SLUG] = name;

        o[rw::json::constantes::ELEMENTS_SLUG] = nlohmann::json(nlohmann::json::value_t::object);
        for (const auto& [key, value] : acceptedValues)
        {
            o[rw::json::constantes::ELEMENTS_SLUG][key] = value;
        }
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeInt(const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::INT_TYPENAME_SLUG, maybe, p);
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeLong(const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::LONG_TYPENAME_SLUG, maybe, p);
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeFloat(const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::FLOAT_TYPENAME_SLUG, maybe, p);
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeDouble(const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::DOUBLE_TYPENAME_SLUG, maybe, p);
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeString(const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::STRING_TYPENAME_SLUG, maybe, p);
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeBool(const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::BOOL_TYPENAME_SLUG, maybe, p);
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeAnyObject(const type::Maybe maybe, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::ANY_OBJECT_TYPENAME_SLUG, maybe, p);
        return o;
    }
}
