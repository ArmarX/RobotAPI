/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "VariantWriter.h"

#include <RobotAPI/libraries/aron/core/type/variant/All.h>

#include <memory>
#include <numeric>
#include <RobotAPI/libraries/aron/core/type/visitor/variant/VariantVisitor.h>

namespace armarx::aron::type::writer
{

    type::Descriptor VariantWriter::getDescriptor(ReturnTypeConst& input)
    {
        return ConstVariantVisitor::GetDescriptor(input);
    }

    aron::type::VariantPtr VariantWriter::writeObject(const std::string& name, const std::vector<std::string>& templates, const std::vector<std::string>& templateInstantiations, const std::map<std::string, aron::type::VariantPtr>& memberTypes, const std::optional<aron::type::VariantPtr>& extends, const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::Object>(name, templates, templateInstantiations, memberTypes, p);
        o->setMaybe(maybe);
        if (extends.has_value())
        {
            auto ex = type::Object::DynamicCast(*extends);
            o->setExtends(ex);
        }
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeList(const aron::type::VariantPtr& acceptedType, const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::List>(acceptedType, p);
        o->setMaybe(maybe);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeDict(const aron::type::VariantPtr& acceptedType, const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::Dict>(acceptedType, p);
        o->setMaybe(maybe);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writePair(const aron::type::VariantPtr& acceptedType1, const aron::type::VariantPtr& acceptedType2, const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::Pair>(acceptedType1, acceptedType2, p);
        o->setMaybe(maybe);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeTuple(const std::vector<aron::type::VariantPtr>& acceptedTypes, const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::Tuple>(acceptedTypes, p);
        o->setMaybe(maybe);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeNDArray(const int ndim, const type::ndarray::ElementType, const type::Maybe maybe, const Path& p)
    {
       auto o = std::make_shared<type::NDArray>(p);
       return o;
    }

    aron::type::VariantPtr VariantWriter::writeMatrix(const int rows, const int cols, const type::matrix::ElementType type, const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::Matrix>(p);
        o->setMaybe(maybe);
        o->setRows(rows);
        o->setCols(cols);
        o->setElementType(type);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeQuaternion(const type::quaternion::ElementType type, const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::Quaternion>(p);
        o->setMaybe(maybe);
        o->setElementType(type);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeImage(const type::image::PixelType type, const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::Image>(p);
        o->setMaybe(maybe);
        o->setPixelType(type);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writePointCloud(const type::pointcloud::VoxelType type, const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::PointCloud>(p);
        o->setMaybe(maybe);
        o->setVoxelType(type);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeIntEnum(const std::string& name, const std::map<std::string, int>& acceptedValues, const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::IntEnum>(name, acceptedValues, p);
        o->setMaybe(maybe);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeInt(const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::Int>(p);
        o->setMaybe(maybe);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeLong(const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::Long>(p);
        o->setMaybe(maybe);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeFloat(const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::Float>(p);
        o->setMaybe(maybe);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeDouble(const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::Double>(p);
        o->setMaybe(maybe);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeString(const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::String>(p);
        o->setMaybe(maybe);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeBool(const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::Bool>(p);
        o->setMaybe(maybe);
        return o;
    }

    aron::type::VariantPtr VariantWriter::writeAnyObject(const type::Maybe maybe, const Path& p)
    {
        auto o = std::make_shared<type::AnyObject>(p);
        o->setMaybe(maybe);
        return o;
    }
}
