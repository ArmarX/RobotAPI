/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <RobotAPI/libraries/aron/core/type/rw/Writer.h>
#include <RobotAPI/libraries/aron/core/type/variant/Variant.h>

#include <memory>
#include <stack>


namespace armarx::aron::type::writer
{
    class VariantWriter :
        public type::WriterInterface<aron::type::VariantPtr>
    {
    public:
        VariantWriter() = default;

        type::Descriptor getDescriptor(ReturnTypeConst& input) final;

        ReturnType writeObject(const std::string& name, const std::vector<std::string>& templates, const std::vector<std::string>& templateInstantiations, const std::map<std::string, ReturnType>& memberTypes, const std::optional<ReturnType>& extends, const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writeList(const ReturnType& acceptedType, const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writeDict(const ReturnType& acceptedType, const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writePair(const ReturnType& acceptedType1, const ReturnType& acceptedType2, const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writeTuple(const std::vector<ReturnType>& acceptedTypes, const type::Maybe maybe, const Path& p = Path()) override;

        ReturnType writeNDArray(const int ndim, const type::ndarray::ElementType, const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writeMatrix(const int rows, const int cols, const type::matrix::ElementType type, const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writeQuaternion(const type::quaternion::ElementType, const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writeImage(const type::image::PixelType, const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writePointCloud(const type::pointcloud::VoxelType, const type::Maybe maybe, const Path& p = Path()) override;

        ReturnType writeIntEnum(const std::string& name, const std::map<std::string, int>& acceptedValues, const type::Maybe maybe, const Path& p = Path()) override;

        ReturnType writeInt(const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writeLong(const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writeFloat(const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writeDouble(const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writeString(const type::Maybe maybe, const Path& p = Path()) override;
        ReturnType writeBool(const type::Maybe maybe, const Path& p = Path()) override;

        ReturnType writeAnyObject(const type::Maybe maybe, const Path& p = Path()) override;
    };
}
