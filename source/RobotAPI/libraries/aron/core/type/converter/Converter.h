/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "../visitor/Visitor.h"
#include "../rw/Reader.h"
#include "../rw/Writer.h"

namespace armarx::aron::type
{
    // prototypes
    template <class ReaderImplementation, class WriterImplementation, class DerivedT>
    requires isReader<ReaderImplementation> && isWriter<WriterImplementation>
    struct Converter;

    template <class T>
    concept isConverter = std::is_base_of<Converter<typename T::ReaderType, typename T::WriterType, typename T::This>, T>::value;

    template <class ConverterImplementation>
    requires isConverter<ConverterImplementation>
    typename ConverterImplementation::WriterReturnType readAndWrite(typename ConverterImplementation::ReaderInputType& o);

    /// Converter struct providing the needed methods.
    /// WriterImplementation is a writer class, TODO: add concepts
    template <class ReaderImplementation, class WriterImplementation, class DerivedT>
    requires isReader<ReaderImplementation> && isWriter<WriterImplementation>
    struct Converter : virtual public Visitor<typename ReaderImplementation::InputType>
    {
        using WriterType = WriterImplementation;
        using ReaderType = ReaderImplementation;
        using This = DerivedT;
        using WriterReturnType = typename WriterImplementation::ReturnType;
        using ReaderInputType = typename ReaderImplementation::InputType;
        using ReaderInputTypeNonConst = typename ReaderImplementation::InputTypeNonConst;

        ReaderImplementation r;
        WriterImplementation w;
        WriterReturnType last_returned;

        virtual ~Converter() = default;

        type::Descriptor getDescriptor(ReaderInputType& o) final
        {
            return r.getDescriptor(o);
        }

        void visitObject(ReaderInputType& o) final
        {
            std::map<std::string, ReaderInputTypeNonConst> elementsOfInput;
            std::string name;
            std::vector<std::string> templates;
            std::vector<std::string> templateInstantiations;
            type::Maybe maybe;
            std::map<std::string, WriterReturnType> elementsReturn;
            Path p;
            r.readObject(o, name, templates, templateInstantiations, elementsOfInput, maybe, p);
            for (const auto& [key, value] : elementsOfInput)
            {
                auto converted = readAndWrite<DerivedT>(value);
                elementsReturn.insert({key, converted});
            }

            last_returned = w.writeObject(name, templates, templateInstantiations, elementsReturn, std::nullopt, maybe, p);
        }

        void visitDict(ReaderInputType& o) final
        {
            ReaderInputTypeNonConst acceptedType;
            type::Maybe maybe;
            Path p;
            r.readDict(o, acceptedType, maybe, p);

            auto converted = readAndWrite<DerivedT>(acceptedType);

            last_returned = w.writeDict(converted, maybe, p);
        };

        void visitList(ReaderInputType& o) final
        {
            ReaderInputTypeNonConst acceptedType;
            type::Maybe maybe;
            Path p;
            r.readList(o, acceptedType, maybe, p);

            auto converted = readAndWrite<DerivedT>(acceptedType);

            last_returned = w.writeList(converted, maybe, p);
        };

        void visitPair(ReaderInputType& o) final
        {
            ReaderInputTypeNonConst acceptedType1;
            ReaderInputTypeNonConst acceptedType2;
            type::Maybe maybe;
            Path p;
            r.readPair(o, acceptedType1, acceptedType2, maybe, p);

            auto converted1 = readAndWrite<DerivedT>(acceptedType1);
            auto converted2 = readAndWrite<DerivedT>(acceptedType2);

            last_returned = w.writePair(converted1, converted2, maybe, p);
        };

        void visitTuple(ReaderInputType& o) final
        {
            std::vector<ReaderInputTypeNonConst> acceptedTypes;
            std::vector<WriterReturnType> elementsReturn;
            type::Maybe maybe;
            Path p;
            r.readTuple(o, acceptedTypes, maybe, p);

            for (const auto& el : acceptedTypes)
            {
                auto converted = readAndWrite<DerivedT>(el);
                elementsReturn.push_back(converted);
            }

            last_returned = w.writeTuple(elementsReturn, maybe, p);
        };

        void visitNDArray(ReaderInputType& o) final
        {
            type::Maybe maybe;
            type::ndarray::ElementType type;
            int ndim;
            Path p;
            r.readNDArray(o, ndim, type, maybe, p);
            last_returned = w.writeNDArray(ndim, type, maybe, p);
        };

        void visitMatrix(ReaderInputType& o) final
        {
            type::Maybe maybe;
            type::matrix::ElementType type;
            int rows;
            int cols;
            Path p;
            r.readMatrix(o, rows, cols, type, maybe, p);
            last_returned = w.writeMatrix(rows, cols, type, maybe, p);
        };

        void visitQuaternion(ReaderInputType& o) final
        {
            type::Maybe maybe;
            type::quaternion::ElementType type;
            Path p;
            r.readQuaternion(o, type, maybe, p);
            last_returned = w.writeQuaternion(type, maybe, p);
        };

        void visitImage(ReaderInputType& o) final
        {
            type::Maybe maybe;
            type::image::PixelType type;
            Path p;
            r.readImage(o, type, maybe, p);
            last_returned = w.writeImage(type, maybe, p);
        };

        void visitPointCloud(ReaderInputType& o) final
        {
            type::Maybe maybe;
            type::pointcloud::VoxelType type;
            Path p;
            r.readPointCloud(o, type, maybe, p);
            last_returned = w.writePointCloud(type, maybe, p);
        };

        void visitIntEnum(ReaderInputType& o) final
        {
            type::Maybe maybe;
            std::string name;
            std::map<std::string, int> values;
            Path p;
            r.readIntEnum(o, name, values, maybe, p);
            last_returned = w.writeIntEnum(name, values, maybe, p);
        };

        void visitInt(ReaderInputType& o) final
        {
            type::Maybe maybe;
            Path p;
            r.readInt(o, maybe, p);
            last_returned = w.writeInt(maybe, p);
        };

        void visitLong(ReaderInputType& o) final
        {
            type::Maybe maybe;
            Path p;
            r.readLong(o, maybe, p);
            last_returned = w.writeLong(maybe, p);
        };

        void visitFloat(ReaderInputType& o) final
        {
            type::Maybe maybe;
            Path p;
            r.readFloat(o, maybe, p);
            last_returned = w.writeFloat(maybe, p);
        };

        void visitDouble(ReaderInputType& o) final
        {
            type::Maybe maybe;
            Path p;
            r.readDouble(o, maybe, p);
            last_returned = w.writeDouble(maybe, p);
        };

        void visitBool(ReaderInputType& o) final
        {
            type::Maybe maybe;
            Path p;
            r.readBool(o, maybe, p);
            last_returned = w.writeBool(maybe, p);
        };

        void visitString(ReaderInputType& o) final
        {
            type::Maybe maybe;
            Path p;
            r.readString(o, maybe, p);
            last_returned = w.writeString(maybe, p);
        };

        void visitUnknown(ReaderInputType& o) final
        {
            if (!r.readNull(o))
            {
                throw error::AronException(__PRETTY_FUNCTION__, "A visitor got type but the enum is unknown.");
            }
            w.writeNull();
        }
    };

    /// the function to read from a variant and write to a writer T
    /// returns the returntype of T
    template <class ConverterImplementation>
    requires isConverter<ConverterImplementation>
    typename ConverterImplementation::WriterReturnType readAndWrite(typename ConverterImplementation::ReaderInputType& o)
    {
        ConverterImplementation v;
        type::visit(v, o);
        return v.last_returned;
    }
}
