/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "../Converter.h"
#include "../../visitor/nlohmannJSON/NlohmannJSONVisitor.h"
#include "../../rw/reader/nlohmannJSON/NlohmannJSONReader.h"
#include "../../rw/writer/nlohmannJSON/NlohmannJSONWriter.h"

namespace armarx::aron::type
{
    // WriterImplementation is a writer class
    template <class WriterImplementation>
    requires isWriter<WriterImplementation>
    struct FromNlohmannJSONConverter :
            virtual public Converter<aron::type::reader::NlohmannJSONReader, WriterImplementation, FromNlohmannJSONConverter<WriterImplementation>>
    {
        virtual ~FromNlohmannJSONConverter() = default;
    };

    // WriterImplementation is a reader class
    template <class ReaderImplementation>
    requires isReader<ReaderImplementation>
    struct ToNlohmannJSONConverter :
            virtual public Converter<ReaderImplementation, aron::type::writer::NlohmannJSONWriter, ToNlohmannJSONConverter<ReaderImplementation>>
    {
        virtual ~ToNlohmannJSONConverter() = default;
    };
}
