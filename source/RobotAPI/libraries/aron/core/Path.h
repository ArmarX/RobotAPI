/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include<vector>
#include<string>

namespace armarx::aron
{
    /**
     * @brief The Path class. It is used to keep track of the internal tree-like structure of a variant and can be converted into a string for debugging purposes and exception message generation.
     * Further, it can be used to navigate through a type or data object
     */
    class Path
    {
    public:
        /// default constructor
        Path();

        /// constructor, taking a list of strings (a path)
        Path(const std::vector<std::string>&);

        /// copy constructor
        Path(const Path&);

        /// append constructor
        Path(const Path&, const std::vector<std::string>&);

        /// comparison operator
        Path& operator=(const armarx::aron::Path&) = default;

        std::vector<std::string> getPath() const;
        std::string getFirstElement() const;
        std::string getLastElement() const;
        bool hasElement() const;
        size_t size() const;

        Path withIndex(int, bool escape = false) const;
        Path withElement(const std::string&, bool escape = false) const;
        Path withAcceptedType(bool escape = false) const;
        Path withAcceptedTypeIndex(int, bool escape = false) const;

        void setRootIdentifier(const std::string&);
        std::string getRootIdentifier() const;

        void setDelimeter(const std::string&);
        std::string getDelimeter() const;

        std::string toString() const;
        static Path FromString(const std::string&, const std::string& rootIdentifier = "_ARON", const std::string& delimeter = "->");

        Path withDetachedFirstElement() const;
        Path withDetachedLastElement() const;
        Path getWithoutPrefix(const Path&) const;

    private:
        void append(const std::string&);

    private:
        std::string rootIdentifier;
        std::string delimeter;
        std::vector<std::string> path;
    };
}
