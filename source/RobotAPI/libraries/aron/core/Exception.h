/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL

// ArmarX
#include <RobotAPI/interface/aron.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/exceptions/Exception.h>
#include <RobotAPI/libraries/aron/core/Path.h>
#include <RobotAPI/libraries/aron/core/Descriptor.h>


namespace armarx::aron::error
{
    /**
     * @brief A base class for aron exceptions. All aron exceptions inherit from this class
     */
    class AronException :
        public armarx::LocalException
    {
    public:
        AronException() = delete;
        AronException(const std::string& prettymethod, const std::string& reason) :
            LocalException(prettymethod + ": " + reason + ".")
        {
        }

        AronException(const std::string& prettymethod, const std::string& reason, const Path& path) :
            LocalException(prettymethod + ": " + reason + ". The path was: " + path.toString())
        {
        }
    };

    /**
     * @brief A base class for aron exceptions. All aron exceptions inherit from this class
     */
    class AronEOFException :
        public AronException
    {
    public:
        AronEOFException() = delete;
        AronEOFException(const std::string& prettymethod) :
            AronException(prettymethod, "REACHED THE END OF A NON VOID METHOD. PERHAPS YOU FORGOT TO ADD A VALUE TO SOME SWITCH-CASE STATEMEMT?.")
        {
        }
    };

    /**
     * @brief The NotImplementedYetException class
     */
    class NotImplementedYetException :
        public AronException
    {
    public:
        NotImplementedYetException() = delete;
        NotImplementedYetException(const std::string& prettymethod) :
            AronException(prettymethod, "This method is not yet implemented!")
        {
        }
    };

    /**
     * @brief The AronNotValidException class. Takes a dto object as input
     */
    class AronNotValidException :
        public AronException
    {
    public:
        AronNotValidException() = delete;
        AronNotValidException(const std::string& prettymethod, const std::string& reason, const data::dto::GenericDataPtr& data) :
            AronException(prettymethod, reason + ". The ice_id of the data was: " + data->ice_id())
        {

        }

        AronNotValidException(const std::string& prettymethod, const std::string& reason, const data::dto::GenericDataPtr& data, const Path& path) :
            AronException(prettymethod, reason + ". The ice_id of the data was: " + data->ice_id(), path)
        {

        }

        AronNotValidException(const std::string& prettymethod, const std::string& reason, const type::dto::GenericTypePtr& type) :
            AronException(prettymethod, reason + ". The ice_id of the type was: " + type->ice_id())
        {

        }

        AronNotValidException(const std::string& prettymethod, const std::string& reason, const type::dto::GenericTypePtr& type, const Path& path) :
            AronException(prettymethod, reason + ". The ice_id of the type was: " + type->ice_id(), path)
        {

        }
    };

    /**
     * @brief The ValueNotValidException class. Only takes strings as input (convert before)
     */
    class ValueNotValidException :
        public AronException
    {
    public:
        ValueNotValidException() = delete;
        ValueNotValidException(const std::string& prettymethod, const std::string& reason, const std::string& input) :
            AronException(prettymethod, reason + ". Got: " + input)
        {

        }

        ValueNotValidException(const std::string& prettymethod, const std::string& reason, const std::string& input, const Path& path) :
            AronException(prettymethod, reason + ". Got: " + input, path)
        {

        }

        ValueNotValidException(const std::string& prettymethod, const std::string& reason, const std::string& input, const std::string& expectation) :
            AronException(prettymethod, reason + ". Got: " + input + ". Expected: " + expectation)
        {

        }

        ValueNotValidException(const std::string& prettymethod, const std::string& reason, const std::string& input, const std::string& expectation, const Path& path) :
            AronException(prettymethod, reason + ". Got: " + input + ". Expected: " + expectation, path)
        {

        }
    };
}
