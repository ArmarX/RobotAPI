#pragma once

#include <map>
#include <memory>
#include <optional>
#include <vector>

#include "aron_conversions.h"
#include "data/rw/Reader.h"
#include "data/rw/Writer.h"

namespace armarx::aron
{
    template<class ReaderT, class T>
    requires (data::isReader<ReaderT>)
    inline void read(ReaderT& aron_r, typename ReaderT::InputType& input, T& ret)
    {
        ret.read(aron_r, input);
    }

    template<class WriterT, class T>
    requires (data::isWriter<WriterT>)
    inline void write(WriterT& aron_w, const T& input, typename WriterT::ReturnType& ret, const armarx::aron::Path& aron_p = armarx::aron::Path())
    {
        ret = input.write(aron_w, aron_p);
    }

    template<class ReaderT, class DtoT, class BoT>
    requires (data::isReader<ReaderT> && !DtoAndBoAreSame<DtoT, BoT>)
    inline void read(ReaderT& aron_r, typename ReaderT::InputType& input, BoT& ret)
    {
        DtoT aron;
        aron.read(aron_r, input);

        armarx::fromAron(aron, ret);
    }

    template<class WriterT, class DtoT, class BoT>
    requires (data::isWriter<WriterT> && !DtoAndBoAreSame<DtoT, BoT>)
    inline void write(WriterT& aron_w, const BoT& input, typename WriterT::ReturnType& ret, const armarx::aron::Path& aron_p = armarx::aron::Path())
    {
        DtoT aron;
        armarx::toAron(aron, input);
        ret = aron.write(aron_w, aron_p);
    }
}
