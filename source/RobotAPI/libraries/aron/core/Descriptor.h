/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <typeindex>
#include <typeinfo>
#include <map>
#include <string>

// Simox
#include <SimoxUtility/algorithm/string.h>

// ArmarX
#include <RobotAPI/interface/aron.h>

namespace armarx::aron::conversion::util
{
    template <class T1, class T2>
    std::map<T2, T1> InvertMap(const std::map<T1, T2>& m)
    {
        std::map<T2, T1> ret;
        for (const auto &[key, val] : m)
        {
            ret.emplace(val, key);
        }
        return ret;
    }

    template <class T>
    std::map<T, std::string> InvertMap(const std::map<std::string, T>& m)
    {
        return InvertMap<std::string, T>(m);
    }

    template <class T>
    std::map<std::string, T> InvertMap(const std::map<T, std::string>& m)
    {
        return InvertMap<T, std::string>(m);
    }
}

namespace armarx::aron::type
{
    const std::vector<type::Maybe> AllMaybeTypes =
    {
        type::Maybe::NONE,
        type::Maybe::OPTIONAL,
        type::Maybe::RAW_PTR,
        type::Maybe::SHARED_PTR,
        type::Maybe::UNIQUE_PTR
    };

    enum class Descriptor
    {
        LIST,
        OBJECT,
        TUPLE,
        PAIR,
        DICT,
        NDARRAY,
        MATRIX,
        QUATERNION,
        POINTCLOUD,
        IMAGE,
        INT_ENUM,
        INT,
        LONG,
        FLOAT,
        DOUBLE,
        BOOL,
        STRING,
        ANY_OBJECT,
        UNKNOWN = -1
    };

    const std::vector<type::Descriptor> AllDescriptors =
    {
        Descriptor::LIST,
        Descriptor::OBJECT,
        Descriptor::TUPLE,
        Descriptor::PAIR,
        Descriptor::DICT,
        Descriptor::NDARRAY,
        Descriptor::MATRIX,
        Descriptor::QUATERNION,
        Descriptor::POINTCLOUD,
        Descriptor::IMAGE,
        Descriptor::INT_ENUM,
        Descriptor::INT,
        Descriptor::LONG,
        Descriptor::FLOAT,
        Descriptor::DOUBLE,
        Descriptor::BOOL,
        Descriptor::STRING,
        Descriptor::ANY_OBJECT,
        Descriptor::UNKNOWN
    };

    namespace defaultconversion::string
    {
        // Maybe
        const std::map<type::Maybe, std::string> Maybe2String =
        {
            {Maybe::NONE, "armarx::aron::type::Maybe::NONE"},
            {Maybe::RAW_PTR, "armarx::aron::type::Maybe::RAW_PTR"},
            {Maybe::SHARED_PTR, "armarx::aron::type::Maybe::SHARED_PTR"},
            {Maybe::UNIQUE_PTR, "armarx::aron::type::Maybe::UNIQUE_PTR"},
            {Maybe::OPTIONAL, "armarx::aron::type::Maybe::OPTIONAL"}
        };

        // Descriptor
        const std::map<type::Descriptor, std::string> Descriptor2String =
        {
            {Descriptor::LIST, "armarx::aron::type::Descriptor::LIST"},
            {Descriptor::OBJECT, "armarx::aron::type::Descriptor::OBJECT"},
            {Descriptor::DICT, "armarx::aron::type::Descriptor::DICT"},
            {Descriptor::PAIR, "armarx::aron::type::Descriptor::PAIR"},
            {Descriptor::TUPLE, "armarx::aron::type::Descriptor::TUPLE"},
            {Descriptor::NDARRAY, "armarx::aron::type::Descriptor::NDARRAY"},
            {Descriptor::MATRIX, "armarx::aron::type::Descriptor::MATRIX"},
            {Descriptor::QUATERNION, "armarx::aron::type::Descriptor::QUATERNION"},
            {Descriptor::IMAGE, "armarx::aron::type::Descriptor::IMAGE"},
            {Descriptor::POINTCLOUD, "armarx::aron::type::Descriptor::POINTCLOUD"},
            {Descriptor::NDARRAY, "armarx::aron::type::Descriptor::NDARRAY"},
            {Descriptor::INT_ENUM, "armarx::aron::type::Descriptor::INT_ENUM"},
            {Descriptor::INT, "armarx::aron::type::Descriptor::INT"},
            {Descriptor::LONG, "armarx::aron::type::Descriptor::LONG"},
            {Descriptor::FLOAT, "armarx::aron::type::Descriptor::FLOAT"},
            {Descriptor::DOUBLE, "armarx::aron::type::Descriptor::DOUBLE"},
            {Descriptor::BOOL, "armarx::aron::type::Descriptor::BOOL"},
            {Descriptor::STRING, "armarx::aron::type::Descriptor::STRING"},
            {Descriptor::ANY_OBJECT, "armarx::aron::type::Descriptor::ANY_OBJECT"},
            {Descriptor::UNKNOWN, "armarx::aron::type::Descriptor::UNKNOWN"}
        };
    }

    namespace defaultconversion::typeinfo
    {
        // hash type
        const std::map<size_t, Descriptor> TypeId2Descriptor = {
            {typeid(aron::type::dto::List).hash_code(), Descriptor::LIST},
            {typeid(aron::type::dto::AronObject).hash_code(), Descriptor::OBJECT},
            {typeid(aron::type::dto::Tuple).hash_code(), Descriptor::TUPLE},
            {typeid(aron::type::dto::Pair).hash_code(), Descriptor::PAIR},
            {typeid(aron::type::dto::Dict).hash_code(), Descriptor::DICT},
            {typeid(aron::type::dto::NDArray).hash_code(), Descriptor::NDARRAY},
            {typeid(aron::type::dto::Matrix).hash_code(), Descriptor::MATRIX},
            {typeid(aron::type::dto::Quaternion).hash_code(), Descriptor::QUATERNION},
            {typeid(aron::type::dto::PointCloud).hash_code(), Descriptor::POINTCLOUD},
            {typeid(aron::type::dto::Image).hash_code(), Descriptor::IMAGE},
            {typeid(aron::type::dto::IntEnum).hash_code(), Descriptor::INT_ENUM},
            {typeid(aron::type::dto::AronInt).hash_code(), Descriptor::INT},
            {typeid(aron::type::dto::AronLong).hash_code(), Descriptor::LONG},
            {typeid(aron::type::dto::AronFloat).hash_code(), Descriptor::FLOAT},
            {typeid(aron::type::dto::AronDouble).hash_code(), Descriptor::DOUBLE},
            {typeid(aron::type::dto::AronString).hash_code(), Descriptor::STRING},
            {typeid(aron::type::dto::AronBool).hash_code(), Descriptor::BOOL},
            {typeid(aron::type::dto::AnyObject).hash_code(), Descriptor::ANY_OBJECT}
        };
    }

    inline type::Descriptor Aron2Descriptor(const type::dto::GenericType& t)
    {
        return defaultconversion::typeinfo::TypeId2Descriptor.at(typeid(t).hash_code());
    }
}

namespace armarx::aron::data
{
    enum class Descriptor
    {
        LIST,
        DICT,
        NDARRAY,
        INT,
        LONG,
        FLOAT,
        DOUBLE,
        STRING,
        BOOL,
        UNKNOWN = -1
    };

    const std::vector<data::Descriptor> AllDescriptors =
    {
        Descriptor::LIST,
        Descriptor::DICT,
        Descriptor::NDARRAY,
        Descriptor::INT,
        Descriptor::LONG,
        Descriptor::FLOAT,
        Descriptor::DOUBLE,
        Descriptor::STRING,
        Descriptor::BOOL,
        Descriptor::UNKNOWN
    };

    namespace defaultconversion::string
    {
        // Descriptor
        const std::map<data::Descriptor, std::string> Descriptor2String =
        {
            {Descriptor::LIST, "armarx::aron::data::Descriptor::LIST"},
            {Descriptor::DICT, "armarx::aron::data::Descriptor::DICT"},
            {Descriptor::NDARRAY, "armarx::aron::data::Descriptor::NDARRAY"},
            {Descriptor::INT, "armarx::aron::data::Descriptor::INT"},
            {Descriptor::LONG, "armarx::aron::data::Descriptor::LONG"},
            {Descriptor::FLOAT, "armarx::aron::data::Descriptor::FLOAT"},
            {Descriptor::DOUBLE, "armarx::aron::data::Descriptor::DOUBLE"},
            {Descriptor::STRING, "armarx::aron::data::Descriptor::STRING"},
            {Descriptor::BOOL, "armarx::aron::data::Descriptor::BOOL"},
            {Descriptor::UNKNOWN, "armarx::aron::data::Descriptor::UNKNOWN"}
        };
    }

    namespace defaultconversion::typeinfo
    {
        // TypeID
        const std::map<size_t, Descriptor> TypeId2Descriptor = {
            {typeid(aron::data::dto::List).hash_code(), Descriptor::LIST},
            {typeid(aron::data::dto::Dict).hash_code(), Descriptor::DICT},
            {typeid(aron::data::dto::NDArray).hash_code(), Descriptor::NDARRAY},
            {typeid(aron::data::dto::AronInt).hash_code(), Descriptor::INT},
            {typeid(aron::data::dto::AronFloat).hash_code(), Descriptor::FLOAT},
            {typeid(aron::data::dto::AronLong).hash_code(), Descriptor::LONG},
            {typeid(aron::data::dto::AronDouble).hash_code(), Descriptor::DOUBLE},
            {typeid(aron::data::dto::AronString).hash_code(), Descriptor::STRING},
            {typeid(aron::data::dto::AronBool).hash_code(), Descriptor::BOOL},
        };
    }

    inline data::Descriptor Aron2Descriptor(const data::dto::GenericData& t)
    {
        return defaultconversion::typeinfo::TypeId2Descriptor.at(typeid(t).hash_code());
    }

    namespace defaultconversion
    {
        // Useful if data is present but no type information. Try to infer type from data.
        const std::map<Descriptor, aron::type::Descriptor> Data2TypeDescriptor = {
            {Descriptor::LIST, aron::type::Descriptor::LIST},
            {Descriptor::DICT, aron::type::Descriptor::DICT},
            {Descriptor::NDARRAY, aron::type::Descriptor::NDARRAY},
            {Descriptor::INT, aron::type::Descriptor::INT},
            {Descriptor::FLOAT, aron::type::Descriptor::FLOAT},
            {Descriptor::LONG, aron::type::Descriptor::LONG},
            {Descriptor::DOUBLE, aron::type::Descriptor::DOUBLE},
            {Descriptor::STRING, aron::type::Descriptor::STRING},
            {Descriptor::BOOL, aron::type::Descriptor::BOOL},
            {Descriptor::UNKNOWN, aron::type::Descriptor::UNKNOWN}
        };

        const std::map<aron::type::Descriptor, Descriptor> Type2DataDescriptor = {
            // containers
            {aron::type::Descriptor::LIST, Descriptor::LIST},
            {aron::type::Descriptor::OBJECT, Descriptor::DICT},
            {aron::type::Descriptor::TUPLE, Descriptor::LIST},
            {aron::type::Descriptor::PAIR, Descriptor::LIST},
            {aron::type::Descriptor::DICT, Descriptor::DICT},

            // everything which gets an ndarray
            {aron::type::Descriptor::NDARRAY, Descriptor::NDARRAY},
            {aron::type::Descriptor::MATRIX, Descriptor::NDARRAY},
            {aron::type::Descriptor::QUATERNION, Descriptor::NDARRAY},
            {aron::type::Descriptor::POINTCLOUD, Descriptor::NDARRAY},
            {aron::type::Descriptor::IMAGE, Descriptor::NDARRAY},

            // enums
            {aron::type::Descriptor::INT_ENUM, Descriptor::INT},

            // primitives
            {aron::type::Descriptor::INT, Descriptor::INT},
            {aron::type::Descriptor::LONG, Descriptor::LONG},
            {aron::type::Descriptor::FLOAT, Descriptor::FLOAT},
            {aron::type::Descriptor::DOUBLE, Descriptor::DOUBLE},
            {aron::type::Descriptor::STRING, Descriptor::STRING},
            {aron::type::Descriptor::BOOL, Descriptor::BOOL},
            {aron::type::Descriptor::UNKNOWN,  Descriptor::UNKNOWN},
        };
    }
}
