#pragma once

#include <map>
#include <memory>
#include <optional>
#include <vector>

#include "Path.h"

namespace armarx::aron
{
    /**
     * Framework for converting ARON DTOs (Data Transfer Objects) to C++ BOs
     * (Business Objects) and back.
     *
     * To allow conversion between custom ARON and C++ types, declare two
     * functions in the namespace of the BO:
     *
     * @code
     * // aron_conversions.h
     *
     * namespace bo_namespace
     * {
     *     void toAron(arondto::MyObject& dto, const MyObject& bo);
     *     void fromAron(const arondto::MyObject& dto, MyObject& bo);
     * }
     * @endcode
     *
     * Note that the DTO always comes first, and the target object is
     * non-const.
     *
     * In the implementation,
     *
     *
     * @code
     * // aron_conversions.cpp
     *
     * #include "aron_conversions.h"
     * #include <Path/to/MyValue/aron_conversions.h>
     *
     * void bo_namespace::toAron(arondto::MyObject& dto, const MyObject& bo)
     * {
     *     dto.name = bo.name;
     *     toAron(dto.myValue, bo.myValue);
     * }
     *
     * void bo_namespace::fromAron(const arondto::MyObject& dto, MyObject& bo)
     * {
     *     bo.name = dto.name;
     *     fromAron(dto.myValue, bo.myValue);
     * }
     * @endcode
     */

    // Helper concept to avoid ambiguities
    template<typename DtoT, typename BoT>
    concept DtoAndBoAreSame = std::is_same<DtoT, BoT>::value;


    // Same type
    template <class T>
    void toAron(T& dto, const T& bo)
    {
        dto = bo;
    }
    template <class T>
    void fromAron(const T& dto, T& bo)
    {
        bo = dto;
    }


    // Generic return version
    template <class DtoT, class BoT>
    DtoT toAron(const BoT& bo)
    {
        DtoT dto;
        toAron(dto, bo);
        return dto;
    }
    template <class BoT, class DtoT>
    BoT fromAron(const DtoT& dto)
    {
        BoT bo;
        fromAron(dto, bo);
        return bo;
    }







    // std::unique_ptr
    template <class DtoT, class BoT>
    void toAron(DtoT& dto, const std::unique_ptr<BoT>& bo)
    {
        if (bo)
        {
            toAron(dto, *bo);
        }
    }

    template <class DtoT, class BoT>
    void fromAron(const DtoT& dto, std::unique_ptr<BoT>& bo)
    {
        bo = std::make_unique<BoT>();
        fromAron(dto, *bo);
    }


    // std::optional
    template <class DtoT, class BoT>
    void toAron(std::optional<DtoT>& dto, const std::optional<BoT>& bo)
    {
        if (bo.has_value())
        {
            dto = DtoT{};
            toAron(*dto, *bo);
        }
        else
        {
            dto = std::nullopt;
        }
    }

    template <class DtoT, class BoT>
    void fromAron(const std::optional<DtoT>& dto, std::optional<BoT>& bo)
    {
        if (dto.has_value())
        {
            bo = BoT{};
            fromAron(*dto, *bo);
        }
        else
        {
            bo = std::nullopt;
        }
    }

    // Flag-controlled optional
    template <class DtoT, class BoT>
    void toAron(DtoT& dto, bool& dtoValid, const BoT& bo, bool boValid)
    {
        dtoValid = boValid;
        if (boValid)
        {
            toAron(dto, bo);
        }
        else
        {
            dto = {};
        }
    }

    template <class DtoT, class BoT>
    void fromAron(const DtoT& dto, bool dtoValid, BoT& bo, bool& boValid)
    {
        boValid = dtoValid;
        if (dtoValid)
        {
            fromAron(dto, bo);
        }
        else
        {
            bo = {};
        }
    }

    template <class DtoT, class BoT>
    void toAron(DtoT& dto, bool& dtoValid, const std::optional<BoT>& bo)
    {
        dtoValid = bo.has_value();
        if (dtoValid)
        {
            toAron(dto, *bo);
        }
        else
        {
            dto = {};
        }
    }

    template <class DtoT, class BoT>
    void fromAron(const DtoT& dto, bool dtoValid, std::optional<BoT>& bo)
    {
        if (dtoValid)
        {
            bo = BoT{};
            fromAron(dto, *bo);
        }
        else
        {
            bo = std::nullopt;
        }
    }


    // std::vector
    template <class DtoT, class BoT>
    void toAron(std::vector<DtoT>& dtos, const std::vector<BoT>& bos)
    {
        dtos.clear();
        dtos.reserve(bos.size());
        for (const auto& bo : bos)
        {
            toAron(dtos.emplace_back(), bo);
        }
    }
    template <class DtoT, class BoT>
    void fromAron(const std::vector<DtoT>& dtos, std::vector<BoT>& bos)
    {
        bos.clear();
        bos.reserve(dtos.size());
        for (const auto& dto : dtos)
        {
            fromAron(dto, bos.emplace_back());
        }
    }


    // std::map
    template <class DtoKeyT, class DtoValueT, class BoKeyT, class BoValueT>
    requires (!(DtoAndBoAreSame<DtoKeyT, BoKeyT> and DtoAndBoAreSame<DtoValueT, BoValueT>))
    void toAron(std::map<DtoKeyT, DtoValueT>& dtoMap,
                const std::map<BoKeyT, BoValueT>& boMap)
    {
        dtoMap.clear();
        for (const auto& [boKey, boValue] : boMap)
        {
            DtoKeyT dtoKey;
            toAron(dtoKey, boKey);
            auto [it, _] = dtoMap.emplace(std::move(dtoKey), DtoValueT{});
            toAron(it->second, boValue);
        }
    }
    template <class DtoKeyT, class DtoValueT, class BoKeyT, class BoValueT>
    requires (!(DtoAndBoAreSame<DtoKeyT, BoKeyT> and DtoAndBoAreSame<DtoValueT, BoValueT>))
    void fromAron(const std::map<DtoKeyT, DtoValueT>& dtoMap,
                  std::map<BoKeyT, BoValueT>& boMap)
    {
        boMap.clear();
        for (const auto& [dtoKey, dtoValue] : dtoMap)
        {
            BoKeyT boKey;
            fromAron(dtoKey, boKey);
            auto [it, _] = boMap.emplace(boKey, BoValueT{});
            fromAron(dtoValue, it->second);
        }
    }


    template <class DtoKeyT, class DtoValueT, class BoKeyT, class BoValueT>
    requires (!(DtoAndBoAreSame<DtoKeyT, BoKeyT> and DtoAndBoAreSame<DtoValueT, BoValueT>))
    std::map<DtoKeyT, DtoValueT> toAron(const std::map<BoKeyT, BoValueT>& boMap)
    {
        std::map<DtoKeyT, DtoValueT> dtoMap;
        toAron(dtoMap, boMap);
        return dtoMap;
    }
}





// And do the same for the armarx namespace to ensure consistency with all the other aron_conversions declaraions
// (which are usually in the armarx namespace)
namespace armarx
{
    // Same type
    template <class T>
    void toAron(T& dto, const T& bo)
    {
        armarx::aron::toAron(dto, bo);
    }
    template <class T>
    void fromAron(const T& dto, T& bo)
    {
        armarx::aron::fromAron(dto, bo);
    }


    // Generic return version
    template <class DtoT, class BoT>
    DtoT toAron(const BoT& bo)
    {
        return armarx::aron::toAron<DtoT, BoT>(bo);
    }
    template <class BoT, class DtoT>
    BoT fromAron(const DtoT& dto)
    {
        return armarx::aron::fromAron<BoT, DtoT>(dto);
    }

    // std::unique_ptr
    template <class DtoT, class BoT>
    void toAron(DtoT& dto, const std::unique_ptr<BoT>& bo)
    {
        armarx::aron::toAron(dto, bo);
    }
    template <class DtoT, class BoT>
    void fromAron(const DtoT& dto, std::unique_ptr<BoT>& bo)
    {
        armarx::aron::fromAron(dto, bo);
    }

    // std::optional
    template <class DtoT, class BoT>
    void toAron(std::optional<DtoT>& dto, const std::optional<BoT>& bo)
    {
        armarx::aron::toAron(dto, bo);
    }
    template <class DtoT, class BoT>
    void fromAron(const std::optional<DtoT>& dto, std::optional<BoT>& bo)
    {
        armarx::aron::fromAron(dto, bo);
    }

    // Flag-controlled optional
    template <class DtoT, class BoT>
    void toAron(DtoT& dto, bool& dtoValid, const BoT& bo, bool boValid)
    {
        armarx::aron::toAron(dto, dtoValid, bo, boValid);
    }
    template <class DtoT, class BoT>
    void fromAron(const DtoT& dto, bool dtoValid, BoT& bo, bool& boValid)
    {
        armarx::aron::fromAron(dto, dtoValid, bo, boValid);
    }

    template <class DtoT, class BoT>
    void toAron(DtoT& dto, bool& dtoValid, const std::optional<BoT>& bo)
    {
        armarx::aron::toAron(dto, dtoValid, bo);
    }
    template <class DtoT, class BoT>
    void fromAron(const DtoT& dto, bool dtoValid, std::optional<BoT>& bo)
    {
        armarx::aron::fromAron(dto, dtoValid, bo);
    }

    // std::vector
    template <class DtoT, class BoT>
    requires (!aron::DtoAndBoAreSame<DtoT, BoT>)
    void toAron(std::vector<DtoT>& dtos, const std::vector<BoT>& bos)
    {
        armarx::aron::toAron(dtos, bos);
    }
    template <class DtoT, class BoT>
    requires (!aron::DtoAndBoAreSame<DtoT, BoT>)
    void fromAron(const std::vector<DtoT>& dtos, std::vector<BoT>& bos)
    {
        armarx::aron::fromAron(dtos, bos);
    }

    template <class DtoT, class BoT>
    requires (!aron::DtoAndBoAreSame<DtoT, BoT>)
    std::vector<DtoT> toAron(const std::vector<BoT>& bos)
    {
        return armarx::aron::toAron<DtoT, BoT>(bos);
    }


    // std::map
    template <class DtoKeyT, class DtoValueT, class BoKeyT, class BoValueT>
    requires (!(aron::DtoAndBoAreSame<DtoKeyT, BoKeyT> and aron::DtoAndBoAreSame<DtoValueT, BoValueT>))
    void toAron(std::map<DtoKeyT, DtoValueT>& dtoMap, const std::map<BoKeyT, BoValueT>& boMap)
    {
        armarx::aron::toAron(dtoMap, boMap);
    }
    template <class DtoKeyT, class DtoValueT, class BoKeyT, class BoValueT>
    requires (!(aron::DtoAndBoAreSame<DtoKeyT, BoKeyT> and aron::DtoAndBoAreSame<DtoValueT, BoValueT>))
    void fromAron(const std::map<DtoKeyT, DtoValueT>& dtoMap, std::map<BoKeyT, BoValueT>& boMap)
    {
        armarx::aron::fromAron(dtoMap, boMap);
    }


    template <class DtoKeyT, class DtoValueT, class BoKeyT, class BoValueT>
    requires (!(aron::DtoAndBoAreSame<DtoKeyT, BoKeyT> and aron::DtoAndBoAreSame<DtoValueT, BoValueT>))
    std::map<DtoKeyT, DtoValueT> toAron(const std::map<BoKeyT, BoValueT>& boMap)
    {
        armarx::aron::toAron<DtoKeyT, DtoValueT, BoKeyT, BoValueT>(boMap);
    }
}
