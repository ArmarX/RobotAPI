/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <string>

// ArmarX
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/core/Descriptor.h>

namespace armarx::aron::data::rw::json
{
    namespace constantes
    {
        const std::string TYPE_SLUG = "_ARON_TYPE";
        const std::string PATH_SLUG = "_ARON_PATH";
        const std::string VERSION_SLUG = "_ARON_VERSION";

        const std::string KEY_SLUG = "_ARON_KEY";
        const std::string VALUE_SLUG = "_ARON_VALUE";
        const std::string MEMBERS_SLUG = "_ARON_MEMBERS";
        const std::string ELEMENTS_SLUG = "_ARON_ELEMENTS";

        const std::string NAME_SLUG = "_ARON_NAME";
        const std::string DIMENSIONS_SLUG = "_ARON_DIMESIONS";
        const std::string DATA_SLUG = "_ARON_DATA";
        const std::string USED_TYPE_SLUG = "_ARON_USED_TYPE";

        const std::string LIST_TYPENAME_SLUG = "_ARON_LIST";
        const std::string DICT_TYPENAME_SLUG = "_ARON_DICT";
        const std::string NDARRAY_TYPENAME_SLUG = "_ARON_NDARRAY";
        const std::string INT_TYPENAME_SLUG = "_ARON_INT";
        const std::string LONG_TYPENAME_SLUG = "_ARON_LONG";
        const std::string FLOAT_TYPENAME_SLUG = "_ARON_FLOAT";
        const std::string DOUBLE_TYPENAME_SLUG = "_ARON_DOUBLE";
        const std::string STRING_TYPENAME_SLUG = "_ARON_STRING";
        const std::string BOOL_TYPENAME_SLUG = "_ARON_BOOL";
        const std::string TIME_TYPENAME_SLUG = "_ARON_TIME";
    }

    namespace conversion
    {
        const std::map<data::Descriptor, std::string> Descriptor2String = {
            {data::Descriptor::DICT, rw::json::constantes::DICT_TYPENAME_SLUG},
            {data::Descriptor::LIST, rw::json::constantes::LIST_TYPENAME_SLUG},
            {data::Descriptor::NDARRAY, rw::json::constantes::NDARRAY_TYPENAME_SLUG},
            {data::Descriptor::INT, rw::json::constantes::INT_TYPENAME_SLUG},
            {data::Descriptor::LONG, rw::json::constantes::LONG_TYPENAME_SLUG},
            {data::Descriptor::FLOAT, rw::json::constantes::FLOAT_TYPENAME_SLUG},
            {data::Descriptor::DOUBLE, rw::json::constantes::DOUBLE_TYPENAME_SLUG},
            {data::Descriptor::BOOL, rw::json::constantes::BOOL_TYPENAME_SLUG},
            {data::Descriptor::STRING, rw::json::constantes::STRING_TYPENAME_SLUG}
        };
        const auto String2Descriptor = aron::conversion::util::InvertMap(Descriptor2String);

        const std::map<type::Maybe, std::string> Maybe2String =
        {
            {type::Maybe::NONE, "type::maybe::NONE"},
            {type::Maybe::OPTIONAL, "type::maybe::OPTIONAL"},
            {type::Maybe::RAW_PTR, "type::maybe::RAW_PTR"},
            {type::Maybe::SHARED_PTR, "type::maybe::SHARED_PTR"},
            {type::Maybe::UNIQUE_PTR, "type::maybe::UNIQUE_PTR"}
        };
        const auto String2Maybe = aron::conversion::util::InvertMap(Maybe2String);

        const std::map<type::ndarray::ElementType, std::string> NDArrayType2String =
        {
            {type::ndarray::ElementType::INT8, "type::ndarray::INT8"},
            {type::ndarray::ElementType::INT16, "type::ndarray::INT16"},
            {type::ndarray::ElementType::INT32, "type::ndarray::INT32"},
            {type::ndarray::ElementType::UINT8, "type::ndarray::UINT8"},
            {type::ndarray::ElementType::UINT16, "type::ndarray::UINT16"},
            {type::ndarray::ElementType::UINT32, "type::ndarray::UINT32"},
            {type::ndarray::ElementType::FLOAT32, "type::ndarray::FLOAT32"},
            {type::ndarray::ElementType::FLOAT64, "type::ndarray::FLOAT64"}
        };
        const auto String2NDArrayType = aron::conversion::util::InvertMap(NDArrayType2String);

        const std::map<type::matrix::ElementType, std::string> MatrixType2String =
        {
            {type::matrix::ElementType::INT16, "type::matrix::INT16"},
            {type::matrix::ElementType::INT32, "type::matrix::INT32"},
            {type::matrix::ElementType::INT64, "type::matrix::INT64"},
            {type::matrix::ElementType::FLOAT32, "type::matrix::FLOAT32"},
            {type::matrix::ElementType::FLOAT64, "type::matrix::FLOAT64"}
        };
        const auto String2MatrixType = aron::conversion::util::InvertMap(MatrixType2String);

        const std::map<type::quaternion::ElementType, std::string> QuaternionType2String =
        {
            {type::quaternion::ElementType::FLOAT32, "type::quaternion::FLOAT32"},
            {type::quaternion::ElementType::FLOAT64, "type::quaternion::FLOAT64"}
        };
        const auto String2QuaternionType = aron::conversion::util::InvertMap(QuaternionType2String);

        const std::map<type::image::PixelType, std::string> PixelType2String =
        {
            {type::image::PixelType::RGB24, "type::image::RGB24"},
            {type::image::PixelType::DEPTH32, "type::image::DEPTH32"}
        };
        const auto String2PixelType = aron::conversion::util::InvertMap(PixelType2String);

        const std::map<type::pointcloud::VoxelType, std::string> VoxelType2String =
        {
            {type::pointcloud::VoxelType::POINT_XYZ, "type::pointcloud::POINT_XYZ"},
            {type::pointcloud::VoxelType::POINT_XYZI, "type::pointcloud::POINT_XYZI"},
            {type::pointcloud::VoxelType::POINT_XYZL, "type::pointcloud::POINT_XYZL"},
            {type::pointcloud::VoxelType::POINT_XYZRGB, "type::pointcloud::POINT_XYZRGB"},
            {type::pointcloud::VoxelType::POINT_XYZRGBA, "type::pointcloud::POINT_XYZRGBA"},
            {type::pointcloud::VoxelType::POINT_XYZRGBL, "type::pointcloud::POINT_XYZRGBL"},
            {type::pointcloud::VoxelType::POINT_XYZHSV, "type::pointcloud::POINT_XYZHSV"}
        };
        const auto String2VoxelType = aron::conversion::util::InvertMap(VoxelType2String);
    }
}
