/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <string>
#include <optional>

// ArmarX
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/core/Exception.h>

// Known types
#include <ArmarXCore/core/time/forward_declarations.h>

namespace armarx::aron::data
{
    template <class R>
    class WriterInterface
    {
    public:
        using ReturnType = R;
        using ReturnTypeConst = typename std::add_const<ReturnType>::type;

        virtual ~WriterInterface() = default;

        virtual data::Descriptor getDescriptor(ReturnTypeConst& input) = 0;

        virtual ReturnType writeList(const std::vector<ReturnType>& elements, const Path& p) = 0;
        virtual ReturnType writeDict(const std::map<std::string, ReturnType>& elements, const std::optional<ReturnType>& extends, const Path& p) = 0;

        virtual ReturnType writeNDArray(const std::vector<int>& shape, const std::string& typeAsString, const unsigned char* data, const Path& p) = 0;

        virtual ReturnType writeInt(const int i, const Path& p) = 0;
        virtual ReturnType writeLong(const long i, const Path& p) = 0;
        virtual ReturnType writeFloat(const float i, const Path& p) = 0;
        virtual ReturnType writeDouble(const double i, const Path& p) = 0;
        virtual ReturnType writeString(const std::string& i, const Path& p) = 0;
        virtual ReturnType writeBool(const bool i, const Path& p) = 0;

        virtual ReturnType writeNull() // defaulted implementation
        {
            return {};
        }

        // Convenience methods
        ReturnType writePrimitive(const int i, const Path& p = Path())
        {
            return writeInt(i, p);
        }

        ReturnType writePrimitive(const long i, const Path& p = Path())
        {
            return writeLong(i, p);
        }

        ReturnType writePrimitive(const float i, const Path& p = Path())
        {
            return writeFloat(i, p);
        }

        ReturnType writePrimitive(const double i, const Path& p = Path())
        {
            return writeDouble(i, p);
        }

        ReturnType writePrimitive(const std::string& i, const Path& p = Path())
        {
            return writeString(i, p);
        }

        ReturnType writePrimitive(const bool i, const Path& p = Path())
        {
            return writeBool(i, p);
        }
    };

    template <class T>
    concept isWriter = std::is_base_of<WriterInterface<typename T::ReturnType>, T>::value;
}
