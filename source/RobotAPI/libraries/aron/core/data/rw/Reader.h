/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <string>
#include <vector>
#include <optional>

// ArmarX
#include <RobotAPI/libraries/aron/core/Path.h>
#include <RobotAPI/libraries/aron/core/Descriptor.h>

namespace armarx::aron::data
{
    template <class I>
    class ReaderInterface
    {
    public:
        using InputType = I;
        using InputTypeNonConst = typename std::remove_const<InputType>::type;

        virtual ~ReaderInterface() = default;

        virtual data::Descriptor getDescriptor(InputType& input) = 0;

        virtual void readList(InputType& input, std::vector<InputTypeNonConst>& elements, Path&) = 0;
        virtual void readDict(InputType& input, std::map<std::string, InputTypeNonConst>& elements, Path&) = 0;

        virtual void readNDArray(InputType& input, std::vector<int>& shape, std::string& typeAsString, std::vector<unsigned char>& data, Path&) = 0;

        virtual void readInt(InputType& input, int& i, Path&) = 0;
        virtual void readLong(InputType& input, long& i, Path&) = 0;
        virtual void readFloat(InputType& input, float& i, Path&) = 0;
        virtual void readDouble(InputType& input, double& i, Path&) = 0;
        virtual void readString(InputType& input, std::string& s, Path&) = 0;
        virtual void readBool(InputType& input, bool& i, Path&) = 0;

        virtual bool readNull(InputType& input) // defaulted implementation
        {
            InputType nil = {};
            return (input == nil);
        }

        // Convenience methods
        void readPrimitive(InputType& input, int& i, Path& p)
        {
            return readInt(input, i, p);
        }

        void readPrimitive(InputType& input, long& i, Path& p)
        {
            return readLong(input, i, p);
        }

        void readPrimitive(InputType& input, float& i, Path& p)
        {
            return readFloat(input, i, p);
        }

        void readPrimitive(InputType& input, double& i, Path& p)
        {
            return readDouble(input, i, p);
        }

        void readPrimitive(InputType& input, std::string& i, Path& p)
        {
            return readString(input, i, p);
        }

        void readPrimitive(InputType& input, bool& i, Path& p)
        {
            return readBool(input, i, p);
        }

        // Convenience methods without path object
        void readList(InputType& input, std::vector<InputTypeNonConst>& elements)
        {
            Path p;
            return readList(input, elements, p);
        }

        void readDict(InputType& input, std::map<std::string, InputTypeNonConst>& elements)
        {
            Path p;
            return readDict(input, elements, p);
        }

        void readNDArray(InputType& input, std::vector<int>& shape, std::string& typeAsString, std::vector<unsigned char>& data)
        {
            Path p;
            return readNDArray(input, shape, typeAsString, data, p);
        }

        void readInt(InputType& input, int& i)
        {
            Path p;
            return readInt(input, i, p);
        }

        void readLong(InputType& input, long& i)
        {
            Path p;
            return readLong(input, i, p);
        }

        void readFloat(InputType& input, float& i)
        {
            Path p;
            return readFloat(input, i, p);
        }

        void readDouble(InputType& input, double& i)
        {
            Path p;
            return readDouble(input, i, p);
        }

        void readString(InputType& input, std::string& s)
        {
            Path p;
            return readString(input, s, p);
        }

        void readBool(InputType& input, bool& i)
        {
            Path p;
            return readBool(input, i, p);
        }

        void readPrimitive(InputType& input, int& i)
        {
            return readInt(input, i);
        }

        void readPrimitive(InputType& input, long& i)
        {
            return readLong(input, i);
        }

        void readPrimitive(InputType& input, float& i)
        {
            return readFloat(input, i);
        }

        void readPrimitive(InputType& input, double& i)
        {
            return readDouble(input, i);
        }

        void readPrimitive(InputType& input, std::string& i)
        {
            return readString(input, i);
        }

        void readPrimitive(InputType& input, bool& i)
        {
            return readBool(input, i);
        }


    };

    template <class T>
    concept isReader = std::is_base_of<ReaderInterface<typename T::InputType>, T>::value;
}
