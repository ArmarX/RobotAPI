/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD / STL
#include <memory>
#include <stack>
#include <sstream>

// Simox
#include <SimoxUtility/json.h>

// Base Class
#include <RobotAPI/libraries/aron/core/data/rw/Writer.h>

namespace armarx::aron::data::writer
{
    class NlohmannJSONWriter :
        virtual public WriterInterface<nlohmann::json>
    {
    public:
        NlohmannJSONWriter() = default;

        data::Descriptor getDescriptor(ReturnTypeConst& input) final;

        nlohmann::json writeList(const std::vector<nlohmann::json>& elements, const Path& p = Path()) override;
        nlohmann::json writeDict(const std::map<std::string, nlohmann::json>& elements, const std::optional<nlohmann::json>& extends = std::nullopt, const Path& p = Path()) override;

        nlohmann::json writeNDArray(const std::vector<int>& shape, const std::string& typeAsString, const unsigned char* data, const Path& p = Path()) override;

        nlohmann::json writeInt(const int i, const Path& p = Path()) override;
        nlohmann::json writeLong(const long i, const Path& p = Path()) override;
        nlohmann::json writeFloat(const float i, const Path& p = Path()) override;
        nlohmann::json writeDouble(const double i, const Path& p = Path()) override;
        nlohmann::json writeString(const std::string& i, const Path& p = Path()) override;
        nlohmann::json writeBool(const bool i, const Path& p = Path()) override;
    };
}
