/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <stack>

// BaseClass
#include <RobotAPI/libraries/aron/core/data/rw/Writer.h>

// ArmarX
#include <RobotAPI/libraries/aron/core/data/variant/Variant.h>

namespace armarx::aron::data::writer
{
    class VariantWriter :
        public WriterInterface<data::VariantPtr>
    {
    public:
        VariantWriter() = default;

        data::Descriptor getDescriptor(ReturnTypeConst& input) final;

        data::VariantPtr writeList(const std::vector<data::VariantPtr>& elements, const Path& p = Path()) override;
        data::VariantPtr writeDict(const std::map<std::string, data::VariantPtr>& elements, const std::optional<data::VariantPtr>& extends = std::nullopt, const Path& p = Path()) override;

        data::VariantPtr writeNDArray(const std::vector<int>& shape, const std::string& typeAsString, const unsigned char* data, const Path& p = Path()) override;

        data::VariantPtr writeInt(const int i, const Path& p = Path()) override;
        data::VariantPtr writeLong(const long i, const Path& p = Path()) override;
        data::VariantPtr writeFloat(const float i, const Path& p = Path()) override;
        data::VariantPtr writeDouble(const double i, const Path& p = Path()) override;
        data::VariantPtr writeString(const std::string& i, const Path& p = Path()) override;
        data::VariantPtr writeBool(const bool i, const Path& p = Path()) override;

        using WriterInterface<data::VariantPtr>::writeDict;
    };
}
