/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller (fabian dot peller at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <numeric>

// Header
#include "NlohmannJSONWriter.h"

// ArmarX
#include "../../json/Data.h"
#include <RobotAPI/libraries/aron/core/data/visitor/nlohmannJSON/NlohmannJSONVisitor.h>

namespace armarx::aron::data::writer
{
    namespace
    {
        /// Set important members for json object (aron meta information)
        void setupAronMetaInformationForType(nlohmann::json& json, const std::string& type, const Path& p)
        {
            json[rw::json::constantes::TYPE_SLUG] = type;
            json[rw::json::constantes::PATH_SLUG] = p.getPath();
        }
    }

    data::Descriptor NlohmannJSONWriter::getDescriptor(ReturnTypeConst& input)
    {
        return ConstNlohmannJSONVisitor::GetDescriptor(input);
    }

    nlohmann::json NlohmannJSONWriter::writeList(const std::vector<nlohmann::json>& elements, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::LIST_TYPENAME_SLUG, p);
        o[rw::json::constantes::ELEMENTS_SLUG] = elements;
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeDict(const std::map<std::string, nlohmann::json>& elements, const std::optional<nlohmann::json>& extends, const Path& p)
    {
        auto o = extends ? extends.value() : nlohmann::json();

        setupAronMetaInformationForType(o, rw::json::constantes::DICT_TYPENAME_SLUG, p);
        o[rw::json::constantes::ELEMENTS_SLUG] = elements;
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeNDArray(const std::vector<int>& shape, const std::string& typeAsString, const unsigned char* data, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::NDARRAY_TYPENAME_SLUG, p);
        o[rw::json::constantes::DIMENSIONS_SLUG] = shape;
        o[rw::json::constantes::USED_TYPE_SLUG] = typeAsString;

        int elements = shape.empty() ? 0 : std::accumulate(std::begin(shape), std::end(shape), 1, std::multiplies<int>());
        std::vector<unsigned char> d = std::vector<unsigned char>(elements);
        memcpy(d.data(), data, elements);
        o[rw::json::constantes::DATA_SLUG] = d;
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeInt(const int i, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::INT_TYPENAME_SLUG, p);
        o[rw::json::constantes::VALUE_SLUG] = i;
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeLong(const long i, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::LONG_TYPENAME_SLUG, p);
        o[rw::json::constantes::VALUE_SLUG] = i;
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeFloat(const float i, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::FLOAT_TYPENAME_SLUG, p);
        o[rw::json::constantes::VALUE_SLUG] = i;
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeDouble(const double i, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::DOUBLE_TYPENAME_SLUG, p);
        o[rw::json::constantes::VALUE_SLUG] = i;
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeString(const std::string& i, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::STRING_TYPENAME_SLUG, p);
        o[rw::json::constantes::VALUE_SLUG] = i;
        return o;
    }

    nlohmann::json NlohmannJSONWriter::writeBool(const bool i, const Path& p)
    {
        nlohmann::json o;
        setupAronMetaInformationForType(o, rw::json::constantes::BOOL_TYPENAME_SLUG, p);
        o[rw::json::constantes::VALUE_SLUG] = i;
        return o;
    }
}
