/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// STD/STL
#include <memory>
#include <numeric>

// Header
#include "VariantWriter.h"

// ArmarX
#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>

namespace armarx::aron::data::writer
{

    data::Descriptor VariantWriter::getDescriptor(ReturnTypeConst& input)
    {
        return ConstVariantVisitor::GetDescriptor(input);
    }

    data::VariantPtr VariantWriter::writeList(const std::vector<data::VariantPtr>& elements, const Path& p)
    {
        auto o = std::make_shared<data::List>(p);
        for (const auto& el : elements)
        {
            o->addElement(el);
        }
        return o;
    }

    data::VariantPtr VariantWriter::writeDict(const std::map<std::string, data::VariantPtr>& elements, const std::optional<data::VariantPtr>& extends, const Path& p)
    {
        auto o = extends ? data::Dict::DynamicCastAndCheck(extends.value()) : std::make_shared<data::Dict>(p);

        for(const auto& [key, value] : elements)
        {
            o->addElement(key, value);
        }
        return o;
    }

    data::VariantPtr VariantWriter::writeNDArray(const std::vector<int>& shape, const std::string& typeAsString, const unsigned char* data, const Path& p)
    {
        auto o = std::make_shared<data::NDArray>(p);
        o->setShape(shape);
        o->setType(typeAsString);
        int size = shape.empty() ? 0 : std::accumulate(std::begin(shape), std::end(shape), 1, std::multiplies<int>());
        o->setData(static_cast<unsigned int>(size), data);
        return o;
    }

    data::VariantPtr VariantWriter::writeInt(const int i, const Path& p)
    {
        auto o = std::make_shared<data::Int>(p);
        o->setValue(i);
        return o;
    }

    data::VariantPtr VariantWriter::writeLong(const long i, const Path& p)
    {
        auto o = std::make_shared<data::Long>(p);
        o->setValue(i);
        return o;
    }

    data::VariantPtr VariantWriter::writeFloat(const float i, const Path& p)
    {
        auto o = std::make_shared<data::Float>(p);
        o->setValue(i);
        return o;
    }

    data::VariantPtr VariantWriter::writeDouble(const double i, const Path& p)
    {
        auto o = std::make_shared<data::Double>(p);
        o->setValue(i);
        return o;
    }

    data::VariantPtr VariantWriter::writeString(const std::string& i, const Path& p)
    {
        auto o = std::make_shared<data::String>(p);
        o->setValue(i);
        return o;
    }

    data::VariantPtr VariantWriter::writeBool(const bool i, const Path& p)
    {
        auto o = std::make_shared<data::Bool>(p);
        o->setValue(i);
        return o;
    }
}
