/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// STD/STL
#include <memory>
#include <numeric>

// Header
#include "VariantReader.h"

// ArmarX
#include <RobotAPI/libraries/aron/core/Exception.h>
#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>

namespace armarx::aron::data::reader
{

    data::Descriptor VariantReader::getDescriptor(InputType& input)
    {
        return ConstVariantVisitor::GetDescriptor(input);
    }

    void VariantReader::readList(const data::VariantPtr& input, std::vector<data::VariantPtr>& elements, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = data::List::DynamicCastAndCheck(input);
        elements = o->getElements();
        p = o->getPath();
    }

    void VariantReader::readDict(const data::VariantPtr& input, std::map<std::string, data::VariantPtr>& elements, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = data::Dict::DynamicCastAndCheck(input);
        elements = o->getElements();
        p = o->getPath();
    }

    void VariantReader::readNDArray(const data::VariantPtr& input, std::vector<int>& shape, std::string& typeAsString, std::vector<unsigned char>& data, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = data::NDArray::DynamicCastAndCheck(input);
        shape = o->getShape();
        typeAsString = o->getType();
        data = o->getDataAsVector();
        p = o->getPath();
    }

    void VariantReader::readInt(const data::VariantPtr& input, int& i, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = data::Int::DynamicCastAndCheck(input);
        i = o->getValue();
        p = o->getPath();
    }

    void VariantReader::readLong(const data::VariantPtr& input, long& i, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = data::Long::DynamicCastAndCheck(input);
        i = o->getValue();
        p = o->getPath();
    }

    void VariantReader::readFloat(const data::VariantPtr& input, float& i, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = data::Float::DynamicCastAndCheck(input);
        i = o->getValue();
        p = o->getPath();
    }

    void VariantReader::readDouble(const data::VariantPtr& input, double& i, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = data::Double::DynamicCastAndCheck(input);
        i = o->getValue();
        p = o->getPath();
    }

    void VariantReader::readString(const data::VariantPtr& input, std::string& i, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = data::String::DynamicCastAndCheck(input);
        i = o->getValue();
        p = o->getPath();
    }

    void VariantReader::readBool(const data::VariantPtr& input, bool& i, Path& p)
    {
        ARMARX_CHECK_NOT_NULL(input);
        auto o = data::Bool::DynamicCastAndCheck(input);
        i = o->getValue();
        p = o->getPath();
    }
}
