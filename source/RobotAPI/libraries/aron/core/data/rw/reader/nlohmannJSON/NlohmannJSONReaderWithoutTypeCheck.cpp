/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// STD/STL
#include <memory>
#include <numeric>

// Header
#include "NlohmannJSONReaderWithoutTypeCheck.h"

// ArmarX
#include <RobotAPI/libraries/aron/core/Exception.h>
#include <RobotAPI/libraries/aron/core/data/visitor/nlohmannJSON/NlohmannJSONVisitor.h>

#include "../../json/Data.h"


namespace armarx::aron::data::reader
{
    data::Descriptor
    NlohmannJSONReaderWithoutTypeCheck::getDescriptor(InputType& input)
    {
        return ConstNlohmannJSONVisitor::GetDescriptor(input);
    }

    void
    NlohmannJSONReaderWithoutTypeCheck::readList(const nlohmann::json& input,
                                                 std::vector<nlohmann::json>& elements,
                                                 Path& p)
    {
        elements = input.get<std::vector<nlohmann::json>>();
    }

    void
    NlohmannJSONReaderWithoutTypeCheck::readDict(const nlohmann::json& input,
                                                 std::map<std::string, nlohmann::json>& elements,
                                                 Path& p)
    {
        elements = input.get<std::map<std::string, nlohmann::json>>();
    }

    void
    NlohmannJSONReaderWithoutTypeCheck::readNDArray(const nlohmann::json& input,
                                                    std::vector<int>& shape,
                                                    std::string& typeAsString,
                                                    std::vector<unsigned char>& data,
                                                    Path& p)
    {
        shape = input.at("dims").get<std::vector<int>>();
        data = input.at("data").get<std::vector<unsigned char>>();
    }

    void
    NlohmannJSONReaderWithoutTypeCheck::readInt(const nlohmann::json& input, int& i, Path& p)
    {
        i = input;
    }

    void
    NlohmannJSONReaderWithoutTypeCheck::readLong(const nlohmann::json& input, long& i, Path& p)
    {
        i = input;
    }

    void
    NlohmannJSONReaderWithoutTypeCheck::readFloat(const nlohmann::json& input, float& i, Path& p)
    {
        i = input;
    }

    void
    NlohmannJSONReaderWithoutTypeCheck::readDouble(const nlohmann::json& input, double& i, Path& p)
    {
        i = input;
    }

    void
    NlohmannJSONReaderWithoutTypeCheck::readString(const nlohmann::json& input,
                                                   std::string& i,
                                                   Path& p)
    {
        i = input;
    }

    void
    NlohmannJSONReaderWithoutTypeCheck::readBool(const nlohmann::json& input, bool& i, Path& p)
    {
        i = input;
    }
} // namespace armarx::aron::data::reader
