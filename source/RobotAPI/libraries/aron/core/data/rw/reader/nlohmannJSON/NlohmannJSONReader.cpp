/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// STD/STL
#include <memory>
#include <numeric>

// Header
#include "NlohmannJSONReader.h"

// ArmarX
#include "../../json/Data.h"
#include <RobotAPI/libraries/aron/core/Exception.h>
#include <RobotAPI/libraries/aron/core/data/visitor/nlohmannJSON/NlohmannJSONVisitor.h>


namespace armarx::aron::data::reader
{
    namespace
    {
        /// Throw an exception if the type is other than expected
        void getAronMetaInformationForType(const nlohmann::json& input, const std::string& expectedType, Path& p)
        {
            // check type
            if (input.at(rw::json::constantes::TYPE_SLUG) != expectedType)
            {
                throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Wrong type in json encountered.", input.at(rw::json::constantes::TYPE_SLUG), expectedType);
            }

            // set path
            std::vector<std::string> pathElements = input.at(rw::json::constantes::PATH_SLUG);
            p = Path(pathElements);
        }
    }

    data::Descriptor NlohmannJSONReader::getDescriptor(InputType& input)
    {
        return ConstNlohmannJSONVisitor::GetDescriptor(input);
    }

    void NlohmannJSONReader::readList(const nlohmann::json& input, std::vector<nlohmann::json>& elements, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::LIST_TYPENAME_SLUG, p);
        elements = input.at(rw::json::constantes::ELEMENTS_SLUG).get<std::vector<nlohmann::json>>();
    }

    void NlohmannJSONReader::readDict(const nlohmann::json& input, std::map<std::string, nlohmann::json>& elements, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::DICT_TYPENAME_SLUG, p);
        elements = input.at(rw::json::constantes::ELEMENTS_SLUG).get<std::map<std::string, nlohmann::json>>();
    }

    void NlohmannJSONReader::readNDArray(const nlohmann::json& input, std::vector<int>& shape, std::string& typeAsString, std::vector<unsigned char>& data, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::NDARRAY_TYPENAME_SLUG, p);
        shape = input.at(rw::json::constantes::DIMENSIONS_SLUG).get<std::vector<int>>();
        typeAsString = input.at(rw::json::constantes::USED_TYPE_SLUG);
        data = input.at(rw::json::constantes::DATA_SLUG).get<std::vector<unsigned char>>();
    }

    void NlohmannJSONReader::readInt(const nlohmann::json& input, int& i, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::INT_TYPENAME_SLUG, p);
        i = input.at(rw::json::constantes::VALUE_SLUG);
    }

    void NlohmannJSONReader::readLong(const nlohmann::json& input, long& i, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::LONG_TYPENAME_SLUG, p);
        i = input.at(rw::json::constantes::VALUE_SLUG);
    }

    void NlohmannJSONReader::readFloat(const nlohmann::json& input, float& i, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::FLOAT_TYPENAME_SLUG, p);
        i = input.at(rw::json::constantes::VALUE_SLUG);
    }

    void NlohmannJSONReader::readDouble(const nlohmann::json& input, double& i, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::DOUBLE_TYPENAME_SLUG, p);
        i = input.at(rw::json::constantes::VALUE_SLUG);
    }

    void NlohmannJSONReader::readString(const nlohmann::json& input, std::string& i, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::STRING_TYPENAME_SLUG, p);
        i = input.at(rw::json::constantes::VALUE_SLUG);
    }

    void NlohmannJSONReader::readBool(const nlohmann::json& input, bool& i, Path& p)
    {
        getAronMetaInformationForType(input, rw::json::constantes::BOOL_TYPENAME_SLUG, p);
        i = input.at(rw::json::constantes::VALUE_SLUG);
    }
}
