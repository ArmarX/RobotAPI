/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <stack>

// Simox
#include <SimoxUtility/json.h>

// BaseClass
#include <RobotAPI/libraries/aron/core/data/rw/Reader.h>

namespace armarx::aron::data::reader
{
    class NlohmannJSONReaderWithoutTypeCheck : public ReaderInterface<const nlohmann::json>
    {
        using Base = ReaderInterface<const nlohmann::json>;

    public:
        // constructors
        NlohmannJSONReaderWithoutTypeCheck() = default;

        data::Descriptor getDescriptor(InputType& input) final;

        using Base::readBool;
        using Base::readDict;
        using Base::readDouble;
        using Base::readFloat;
        using Base::readInt;
        using Base::readList;
        using Base::readLong;
        using Base::readNDArray;
        using Base::readString;

        void readList(InputType& input, std::vector<InputTypeNonConst>& elements, Path& p) override;
        void readDict(InputType& input,
                      std::map<std::string, InputTypeNonConst>& elements,
                      Path& p) override;

        void readNDArray(InputType& input,
                         std::vector<int>& shape,
                         std::string& typeAsString,
                         std::vector<unsigned char>& data,
                         Path& p) override;

        void readInt(InputType& input, int& i, Path& p) override;
        void readLong(InputType& input, long& i, Path& p) override;
        void readFloat(InputType& input, float& i, Path& p) override;
        void readDouble(InputType& input, double& i, Path& p) override;
        void readString(InputType& input, std::string& i, Path& p) override;
        void readBool(InputType& input, bool& i, Path& p) override;
    };
} // namespace armarx::aron::data::reader
