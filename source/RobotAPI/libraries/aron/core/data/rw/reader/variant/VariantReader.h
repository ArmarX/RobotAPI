/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller (fabian dot peller at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// STD/STL
#include <memory>
#include <stack>

// BaseClass
#include <RobotAPI/libraries/aron/core/data/rw/Reader.h>

// ArmarX
#include <RobotAPI/libraries/aron/core/data/variant/Variant.h>


namespace armarx::aron::data::reader
{
    class VariantReader :
        public ReaderInterface<const data::VariantPtr>
    {
        using Base = ReaderInterface<const data::VariantPtr>;

    public:
        // constructors
        VariantReader() = default;

        using Base::readList;
        using Base::readDict;
        using Base::readNDArray;
        using Base::readInt;
        using Base::readLong;
        using Base::readFloat;
        using Base::readDouble;
        using Base::readString;
        using Base::readBool;

        data::Descriptor getDescriptor(InputType& input) final;

        void readList(InputType& input, std::vector<InputTypeNonConst>& elements, Path& p) final;
        void readDict(InputType& input, std::map<std::string, InputTypeNonConst>& elements, Path& p) final;

        void readNDArray(InputType& input, std::vector<int>& shape, std::string& typeAsString, std::vector<unsigned char>& data, Path& p) final;

        void readInt(InputType& input, int& i, Path& p) final;
        void readLong(InputType& input, long& i, Path& p) final;
        void readFloat(InputType& input, float& i, Path& p) final;
        void readDouble(InputType& input, double& i, Path& p) final;
        void readString(InputType& input, std::string& s, Path& p) final;
        void readBool(InputType& input, bool& i, Path& p) final;
    };
}
