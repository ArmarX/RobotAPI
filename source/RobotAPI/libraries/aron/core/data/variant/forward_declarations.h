#pragma once

#include <memory>

/**
 * forward declarations of ALL aron data objects
 */
namespace armarx::aron::data
{
    class Variant;
    using VariantPtr = std::shared_ptr<Variant>;

    class Dict;
    using DictPtr = std::shared_ptr<Dict>;

    class List;
    using ListPtr = std::shared_ptr<List>;

    class NDArray;
    using NDArrayPtr = std::shared_ptr<NDArray>;

    class Int;
    using IntPtr = std::shared_ptr<Int>;

    class Long;
    using LongPtr = std::shared_ptr<Long>;

    class Float;
    using FloatPtr = std::shared_ptr<Float>;

    class Double;
    using DoublePtr = std::shared_ptr<Double>;

    class String;
    using StringPtr = std::shared_ptr<String>;

    class Bool;
    using BoolPtr = std::shared_ptr<Bool>;
}
