/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <string>
#include <unordered_map>

// Base class
#include "../Variant.h"

// ArmarX

namespace armarx::aron::data::detail
{
    template<typename AronDataT, typename DerivedT>
    class SpecializedVariantBase :
        public data::Variant
    {
    public:
        using PointerType = std::shared_ptr<DerivedT>;
        using AronDataType = AronDataT;

    public:
        SpecializedVariantBase() = delete;

        SpecializedVariantBase(const data::Descriptor descriptor, const Path& path = Path()) :
            Variant(descriptor, path),
            aron(new AronDataType())
        {
            aron->VERSION = aron::VERSION;
        }

        SpecializedVariantBase(const typename AronDataType::PointerType& o, const data::Descriptor descriptor, const Path& path = Path()) :
            Variant(descriptor, path),
            aron(o)
        {
            ARMARX_CHECK_NOT_NULL(aron);
        }

        virtual ~SpecializedVariantBase() = default;

        // operators
        operator typename AronDataType::PointerType()
        {
            return aron;
        }

        bool operator==(const Variant& other) const override
        {
            const auto& n = DerivedT::DynamicCast(other);
            return *this == n;
        }

        virtual bool operator==(const DerivedT&) const = 0;
        virtual bool operator==(const PointerType& other) const = 0;

        // virtual implementations
        data::dto::GenericDataPtr toAronDTO() const override
        {
            return aron;
        }

        VariantPtr navigateRelative(const Path& path) const override
        {
            Path absoluteFromHere = path.getWithoutPrefix(getPath());
            return navigateAbsolute(absoluteFromHere);
        }


        // static methods
        static PointerType DynamicCast(const VariantPtr& n)
        {
            return std::dynamic_pointer_cast<DerivedT>(n);
        }

        static DerivedT& DynamicCast(Variant& n)
        {
            return dynamic_cast<DerivedT&>(n);
        }

        static const DerivedT& DynamicCast(const Variant& n)
        {
            return dynamic_cast<const DerivedT&>(n);
        }

        static PointerType DynamicCastAndCheck(const VariantPtr& n)
        {
            if (!n)
            {
                return nullptr;
            }

            PointerType casted = DerivedT::DynamicCast(n);
            ARMARX_CHECK_NOT_NULL(casted) << "The path was: " << n->getPath().toString() << ".\n"
                                          << "It has the descriptor: '" << data::defaultconversion::string::Descriptor2String.at(n->getDescriptor()) << "'.\n"
                                          << "And the typeid: " << typeid(n).name() << ". Tried to cast to " << typeid(PointerType).name();
            return casted;
        }

    protected:
        typename AronDataType::PointerType aron;
    };
}
