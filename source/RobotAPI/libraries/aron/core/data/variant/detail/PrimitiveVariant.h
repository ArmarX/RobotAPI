/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <string>
#include <unordered_map>

// Base class
#include "SpecializedVariant.h"

// ArmarX
#include "../container/Dict.h"

namespace armarx::aron::data::detail
{
    template<typename AronDataT, typename ValueT, typename DerivedT>
    class PrimitiveVariant :
        public SpecializedVariantBase<AronDataT, DerivedT>
    {
    public:
        using ValueType = ValueT;

    public:
        using SpecializedVariantBase<AronDataT, DerivedT>::SpecializedVariantBase;

        PrimitiveVariant(const ValueT& v, const data::Descriptor descriptor, const Path& path = Path()):
            SpecializedVariantBase<AronDataT, DerivedT>(descriptor, path)
        {
            this->aron->value = v;
        }

        virtual ~PrimitiveVariant() = default;

        operator ValueT() const
        {
            return this->aron->value;
        }

        DerivedT& operator=(const ValueT& x)
        {
            this->aron->value = x;
            return *this;
        }

        // Already implemented through thge constructor of a primitive navigator
        /*bool operator==(const ValueT& x) const
        {
            return this->aron->value == x;
        }*/

        /// set a primitive from a std string
        virtual void fromString(const std::string& setter) = 0;

        // virtual implementations
        VariantPtr navigateAbsolute(const Path &path) const override
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Could not navigate through a non container navigator. The input path was: " + path.toString(), Variant::getPath());
        }

        VariantPtr clone() const override
        {
            typename DerivedT::PointerType ret(new DerivedT(getValue(), this->getPath()));
            return ret;
        }

        std::vector<VariantPtr> getChildren() const override
        {
            return {};
        }

        size_t childrenSize() const override
        {
            return 0;
        }

        // static methods

        /* public member functions */
        DictPtr getAsDict() const
        {
            auto dict = std::make_shared<Dict>();
            auto copy_this = FromAronDTO(this->toAronDTO(), this->getPath());
            dict->addElement("data", copy_this);
            return dict;
        }

        void setValue(const ValueT& x)
        {
            this->aron->value = x;
        }
        ValueT getValue() const
        {
            return this->aron->value;
        }

        ValueT& getValue()
        {
            return this->aron->value;
        }
    };
}
