/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Int.h"

#include <RobotAPI/libraries/aron/core/data/variant/Factory.h>
#include <RobotAPI/libraries/aron/core/type/variant/primitive/Int.h>

namespace armarx::aron::data
{
    /* constructors */
    Int::Int(const data::dto::AronIntPtr& o, const Path& path) :
        detail::PrimitiveVariant<data::dto::AronInt, int, Int>::PrimitiveVariant(o, data::Descriptor::INT, path)
    {
    }

    Int::Int(const Path& path) :
        detail::PrimitiveVariant<data::dto::AronInt, int, Int>::PrimitiveVariant(data::Descriptor::INT, path)
    {
    }

    Int::Int(const int d, const Path& path) :
        detail::PrimitiveVariant<data::dto::AronInt, int, Int>::PrimitiveVariant(d, data::Descriptor::INT, path)
    {
    }

    /* operators */
    bool Int::operator==(const Int& other) const
    {
        const auto& otherAron = other.toAronIntDTO();
        if (this->aron->value != otherAron->value)
        {
            return false;
        }
        return true;
    }
    bool Int::operator==(const IntPtr& other) const
    {
        if (!other)
        {
            return false;
        }
        return *this == *other;
    }


    /* static methods */
    IntPtr Int::FromAronIntDTO(const data::dto::AronIntPtr& aron)
    {
        if (!aron)
        {
            return nullptr;
        }
        return std::make_shared<Int>(aron);
    }

    data::dto::AronIntPtr Int::ToAronIntDTO(const IntPtr& navigator)
    {
        return navigator ? navigator->toAronIntDTO() : nullptr;
    }

    /* public member functions */
    data::dto::AronIntPtr Int::toAronIntDTO() const
    {
        return aron;
    }


    /* virtual implementations */
    void Int::fromString(const std::string& setter)
    {
        setValue(std::stoi(setter));
    }

    std::string Int::getShortName() const
    {
        return "Int";
    }
    std::string Int::getFullName() const
    {
        return "armarx::aron::data::Int";
    }

    bool Int::fullfillsType(const type::VariantPtr& type) const
    {
        if (!type) return true;
        return type->getDescriptor() == type::Descriptor::INT || type->getDescriptor() == type::Descriptor::INT_ENUM;
    }

    type::VariantPtr Int::recalculateType() const
    {
        return std::make_shared<type::Int>(getPath());
    }
}
