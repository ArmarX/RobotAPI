/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>

// Base class
#include "../detail/PrimitiveVariant.h"

// ArmarX
#include "../../../type/variant/primitive/Long.h"

namespace armarx::aron::data
{

    class Long;
    typedef std::shared_ptr<Long> LongPtr;

    class Long :
        public detail::PrimitiveVariant<data::dto::AronLong, long, Long>
    {
    public:
        /* constructors */
        Long(const Path& = Path());
        Long(const data::dto::AronLongPtr&, const Path& = Path());
        Long(const long, const Path& = Path());

        /* operators */
        bool operator==(const Long&) const override;
        bool operator==(const LongPtr&) const override;

        /* static methods */
        static LongPtr FromAronLongDTO(const data::dto::AronLongPtr& aron);
        static data::dto::AronLongPtr ToAronLongDTO(const LongPtr& navigator);

        /* public member functions */
        data::dto::AronLongPtr toAronLongDTO() const;

        /* virtual implementations */
        void fromString(const std::string& setter) override;

        std::string getShortName() const override;
        std::string getFullName() const override;

        type::VariantPtr recalculateType() const override;
        bool fullfillsType(const type::VariantPtr&) const override;
    };
}

namespace armarx::aron
{
    template<typename... _Args>
    aron::data::LongPtr make_long(_Args&&... args)
    {
        return std::make_shared<aron::data::Long>(args...);
    }
}
