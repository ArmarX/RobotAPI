/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>

// Base class
#include "../detail/PrimitiveVariant.h"

// ArmarX
#include "../../../type/variant/primitive/Bool.h"

namespace armarx::aron::data
{

    class Bool;
    typedef std::shared_ptr<Bool> BoolPtr;

    class Bool :
        public detail::PrimitiveVariant<data::dto::AronBool, bool, Bool>
    {
    public:
        /* constructors */
        Bool(const Path& = Path());
        Bool(const data::dto::AronBoolPtr&, const Path& = Path());
        Bool(const bool, const Path& = Path());

        // operators
        bool operator==(const Bool& other) const override;
        bool operator==(const BoolPtr&) const override;

        // static methods
        static BoolPtr FromAronBoolDTO(const data::dto::AronBoolPtr& aron);
        static data::dto::AronBoolPtr ToAronBoolDTO(const BoolPtr& navigator);

        // class methods
        data::dto::AronBoolPtr toAronBoolDTO() const;

        /* virtual implementations */
        void fromString(const std::string& setter) override;

        std::string getShortName() const override;
        std::string getFullName() const override;

        type::VariantPtr recalculateType() const override;
        bool fullfillsType(const type::VariantPtr&) const override;
    };
}

namespace armarx::aron
{
    template<typename... _Args>
    aron::data::BoolPtr make_bool(_Args&&... args)
    {
        return std::make_shared<aron::data::Bool>(args...);
    }
}
