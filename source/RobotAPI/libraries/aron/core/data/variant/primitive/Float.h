/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>

// Base class
#include "../detail/PrimitiveVariant.h"

// ArmarX
#include "../../../type/variant/primitive/Float.h"

namespace armarx::aron::data
{

    class Float;
    typedef std::shared_ptr<Float> FloatPtr;

    class Float :
        public detail::PrimitiveVariant<data::dto::AronFloat, float, Float>
    {
    public:
        /* constructors */
        Float(const Path& = Path());
        Float(const data::dto::AronFloatPtr&, const Path& = Path());
        Float(const float, const Path& = Path());

        /* operators */
        bool operator==(const Float&) const override;
        bool operator==(const FloatPtr&) const override;

        /* static methods */
        static FloatPtr FromAronFloatDTO(const data::dto::AronFloatPtr& aron);
        static data::dto::AronFloatPtr ToAronFloatDTO(const FloatPtr& navigator);

        /* public member functions */
        data::dto::AronFloatPtr toAronFloatDTO() const;

        /* virtual implementations */
        void fromString(const std::string& setter) override;

        std::string getShortName() const override;
        std::string getFullName() const override;

        type::VariantPtr recalculateType() const override;
        bool fullfillsType(const type::VariantPtr&) const override;
    };
}

namespace armarx::aron
{
    template<typename... _Args>
    aron::data::FloatPtr make_float(_Args&&... args)
    {
        return std::make_shared<aron::data::Float>(args...);
    }
}
