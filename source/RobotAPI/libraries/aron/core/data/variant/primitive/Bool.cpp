/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Bool.h"

namespace armarx::aron::data
{
    /* constructors */
    Bool::Bool(const data::dto::AronBoolPtr& o, const Path& path) :
        detail::PrimitiveVariant<data::dto::AronBool, bool, Bool>::PrimitiveVariant(o, data::Descriptor::BOOL, path)
    {
    }

    Bool::Bool(const Path& path) :
        detail::PrimitiveVariant<data::dto::AronBool, bool, Bool>::PrimitiveVariant(data::Descriptor::BOOL, path)
    {
    }

    Bool::Bool(const bool d, const Path& path) :
        detail::PrimitiveVariant<data::dto::AronBool, bool, Bool>::PrimitiveVariant(d, data::Descriptor::BOOL, path)
    {
    }

    /* operators */
    bool Bool::operator==(const Bool& other) const
    {
        const auto& otherAron = other.toAronBoolDTO();
        if (aron->value != otherAron->value)
        {
            return false;
        }
        return true;
    }
    bool Bool::operator==(const BoolPtr& other) const
    {
        if (!other)
        {
            return false;
        }
        return *this == *other;
    }


    /* static methods */
    BoolPtr Bool::FromAronBoolDTO(const data::dto::AronBoolPtr& aron)
    {
        if (!aron)
        {
            return nullptr;
        }
        return std::make_shared<Bool>(aron);
    }

    data::dto::AronBoolPtr Bool::ToAronBoolDTO(const BoolPtr& navigator)
    {
        return navigator ? navigator->toAronBoolDTO() : nullptr;
    }

    /* public member functions */
    data::dto::AronBoolPtr Bool::toAronBoolDTO() const
    {
        return aron;
    }

    /* virtual implementations */
    void Bool::fromString(const std::string& setter)
    {
        if (setter == "true" || setter == "1" || setter == "yes")
        {
            setValue(true);
        }
        else if (setter == "false" || setter == "0" || setter == "no")
        {
            setValue(false);
        }
        else
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Could not set from string. Got: '" + setter + "'");
        }
    }

    std::string Bool::getShortName() const
    {
        return "Bool";
    }
    std::string Bool::getFullName() const
    {
        return "armarx::aron::data::Bool";
    }

    bool Bool::fullfillsType(const type::VariantPtr& type) const
    {
        if (!type) return true;
        return type->getDescriptor() == type::Descriptor::BOOL;
    }

    type::VariantPtr Bool::recalculateType() const
    {
        return std::make_shared<type::Bool>(getPath());
    }
}
