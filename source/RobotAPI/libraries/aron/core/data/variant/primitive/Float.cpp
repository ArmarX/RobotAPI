/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Float.h"

namespace armarx::aron::data
{
    /* constructors */
    Float::Float(const data::dto::AronFloatPtr& o, const Path& path) :
        detail::PrimitiveVariant<data::dto::AronFloat, float, Float>::PrimitiveVariant(o, data::Descriptor::FLOAT, path)
    {
    }

    Float::Float(const Path& path) :
        detail::PrimitiveVariant<data::dto::AronFloat, float, Float>::PrimitiveVariant(data::Descriptor::FLOAT, path)
    {
    }

    Float::Float(const float d, const Path& path) :
        detail::PrimitiveVariant<data::dto::AronFloat, float, Float>::PrimitiveVariant(d, data::Descriptor::FLOAT, path)
    {
    }

    /* operators */
    bool Float::operator==(const Float& other) const
    {
        const auto& otherAron = other.toAronFloatDTO();
        if (this->aron->value != otherAron->value)
        {
            return false;
        }
        return true;
    }
    bool Float::operator==(const FloatPtr& other) const
    {
        if (!other)
        {
            return false;
        }
        return *this == *other;
    }


    /* static methods */
    FloatPtr Float::FromAronFloatDTO(const data::dto::AronFloatPtr& aron)
    {
        if (!aron)
        {
            return nullptr;
        }
        return std::make_shared<Float>(aron);
    }

    data::dto::AronFloatPtr Float::ToAronFloatDTO(const FloatPtr& navigator)
    {
        return navigator ? navigator->toAronFloatDTO() : nullptr;
    }

    /* public member functions */
    data::dto::AronFloatPtr Float::toAronFloatDTO() const
    {
        return aron;
    }


    /* virtual implementations */
    void Float::fromString(const std::string& setter)
    {
        setValue(std::stof(setter));
    }

    std::string Float::getShortName() const
    {
        return "Float";
    }
    std::string Float::getFullName() const
    {
        return "armarx::aron::data::Float";
    }

    bool Float::fullfillsType(const type::VariantPtr& type) const
    {
        if (!type) return true;
        return type->getDescriptor() == type::Descriptor::FLOAT;
    }

    type::VariantPtr Float::recalculateType() const
    {
        return std::make_shared<type::Float>(getPath());
    }
}
