/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Double.h"

// ArmarX
#include "Float.h"

namespace armarx::aron::data
{
    /* constructors */
    Double::Double(const data::dto::AronDoublePtr& o, const Path& path) :
        detail::PrimitiveVariant<data::dto::AronDouble, double, Double>::PrimitiveVariant(o, data::Descriptor::DOUBLE, path)
    {
    }

    Double::Double(const Path& path) :
        detail::PrimitiveVariant<data::dto::AronDouble, double, Double>::PrimitiveVariant(data::Descriptor::DOUBLE, path)
    {
    }

    Double::Double(const double d, const Path& path) :
        detail::PrimitiveVariant<data::dto::AronDouble, double, Double>::PrimitiveVariant(d, data::Descriptor::DOUBLE, path)
    {
    }

    /* operators */
    bool Double::operator==(const Double& other) const
    {
        const auto& otherAron = other.toAronDoubleDTO();
        if (this->aron->value != otherAron->value)
        {
            return false;
        }
        return true;
    }
    bool Double::operator==(const DoublePtr& other) const
    {
        if (!other)
        {
            return false;
        }
        return *this == *other;
    }

    /* static methods */
    DoublePtr Double::FromAronDoubleDTO(const data::dto::AronDoublePtr& aron)
    {
        if (!aron)
        {
            return nullptr;
        }
        return std::make_shared<Double>(aron);
    }

    data::dto::AronDoublePtr Double::ToAronDoubleDTO(const DoublePtr& navigator)
    {
        return navigator ? navigator->toAronDoubleDTO() : nullptr;
    }


    /* public member functions */
    data::dto::AronDoublePtr Double::toAronDoubleDTO() const
    {
        return aron;
    }

    /* virtual implementations */
    void Double::fromString(const std::string& setter)
    {
        setValue(std::stod(setter));
    }

    std::string Double::getShortName() const
    {
        return "Double";
    }
    std::string Double::getFullName() const
    {
        return "armarx::aron::data::Double";
    }

    bool Double::fullfillsType(const type::VariantPtr& type) const
    {
        if (!type) return true;
        return type->getDescriptor() == type::Descriptor::DOUBLE;
    }

    type::VariantPtr Double::recalculateType() const
    {
        return std::make_shared<type::Double>(getPath());
    }
}
