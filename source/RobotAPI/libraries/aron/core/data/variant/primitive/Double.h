/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>

// Base class
#include "../detail/PrimitiveVariant.h"

// ArmarX
#include "../../../type/variant/primitive/Double.h"

namespace armarx::aron::data
{

    class Double;
    typedef std::shared_ptr<Double> DoublePtr;

    class Double :
        public detail::PrimitiveVariant<data::dto::AronDouble, double, Double>
    {
    public:
        /* constructors */
        Double(const Path& = Path());
        Double(const data::dto::AronDoublePtr&, const Path& = Path());
        Double(const double, const Path& = Path());

        /* operators */
        bool operator==(const Double&) const override;
        bool operator==(const DoublePtr&) const override;

        /* static methods */
        static DoublePtr FromAronDoubleDTO(const data::dto::AronDoublePtr& aron);
        static data::dto::AronDoublePtr ToAronDoubleDTO(const DoublePtr& navigator);

        /* public member functions */
        data::dto::AronDoublePtr toAronDoubleDTO() const;

        /* virtual implementations */
        void fromString(const std::string& setter) override;

        std::string getShortName() const override;
        std::string getFullName() const override;

        type::VariantPtr recalculateType() const override;
        bool fullfillsType(const type::VariantPtr&) const override;
    };
}

namespace armarx::aron
{
    template<typename... _Args>
    aron::data::DoublePtr make_double(_Args&&... args)
    {
        return std::make_shared<aron::data::Double>(args...);
    }
}
