/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>

// Base class
#include "../detail/PrimitiveVariant.h"

// ArmarX
#include "../../../type/variant/primitive/Int.h"
#include "../../../type/variant/enum/IntEnum.h"

namespace armarx::aron::data
{

    class Int;
    typedef std::shared_ptr<Int> IntPtr;

    class Int :
        public detail::PrimitiveVariant<data::dto::AronInt, int, Int>
    {
    public:
        /* constructors */
        Int(const Path& = Path());
        Int(const data::dto::AronIntPtr&, const Path& = Path());
        Int(const int, const Path& = Path());

        /* operators */
        bool operator==(const Int&) const override;
        bool operator==(const IntPtr&) const override;

        /* static methods */
        static IntPtr FromAronIntDTO(const data::dto::AronIntPtr& aron);
        static data::dto::AronIntPtr ToAronIntDTO(const IntPtr& navigator);

        /* public member functions */
        data::dto::AronIntPtr toAronIntDTO() const;

        /* virtual implementations */
        void fromString(const std::string& setter) override;

        std::string getShortName() const override;
        std::string getFullName() const override;

        type::VariantPtr recalculateType() const override;
        bool fullfillsType(const type::VariantPtr&) const override;
    };
}

namespace armarx::aron
{
    template<typename... _Args>
    aron::data::IntPtr make_int(_Args&&... args)
    {
        return std::make_shared<aron::data::Int>(args...);
    }
}
