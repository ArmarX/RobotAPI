/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Long.h"

// ArmarX
#include "Int.h"

namespace armarx::aron::data
{
    /* constructors */
    Long::Long(const data::dto::AronLongPtr& o, const Path& path) :
        detail::PrimitiveVariant<data::dto::AronLong, long, Long>::PrimitiveVariant(o, data::Descriptor::LONG, path)
    {
    }

    Long::Long(const Path& path) :
        detail::PrimitiveVariant<data::dto::AronLong, long, Long>::PrimitiveVariant(data::Descriptor::LONG, path)
    {
    }

    Long::Long(const long d, const Path& path) :
        detail::PrimitiveVariant<data::dto::AronLong, long, Long>::PrimitiveVariant(d, data::Descriptor::LONG, path)
    {
    }

    /* operators */
    bool Long::operator==(const Long& other) const
    {
        const auto& otherAron = other.toAronLongDTO();
        if (this->aron->value != otherAron->value)
        {
            return false;
        }
        return true;
    }
    bool Long::operator==(const LongPtr& other) const
    {
        if (!other)
        {
            return false;
        }
        return *this == *other;
    }


    /* static methods */
    LongPtr Long::FromAronLongDTO(const data::dto::AronLongPtr& aron)
    {
        if (!aron)
        {
            return nullptr;
        }
        return std::make_shared<Long>(aron);
    }

    data::dto::AronLongPtr Long::ToAronLongDTO(const LongPtr& navigator)
    {
        return navigator ? navigator->toAronLongDTO() : nullptr;
    }

    /* public member functions */
    data::dto::AronLongPtr Long::toAronLongDTO() const
    {
        return aron;
    }


    /* virtual implementations */
    void Long::fromString(const std::string& setter)
    {
        setValue(std::stol(setter));
    }

    std::string Long::getShortName() const
    {
        return "Long";
    }
    std::string Long::getFullName() const
    {
        return "armarx::aron::data::Long";
    }

    bool Long::fullfillsType(const type::VariantPtr& type) const
    {
        if (!type) return true;
        return type->getDescriptor() == type::Descriptor::LONG;
    }

    type::VariantPtr Long::recalculateType() const
    {
        return std::make_shared<type::Long>(getPath());
    }
}
