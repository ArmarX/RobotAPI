/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "String.h"

namespace armarx::aron::data
{
    /* constructors */
    String::String(const data::dto::AronStringPtr& o, const Path& path) :
        detail::PrimitiveVariant<data::dto::AronString, std::string, String>::PrimitiveVariant(o, data::Descriptor::STRING, path)
    {
    }

    String::String(const Path& path) :
        detail::PrimitiveVariant<data::dto::AronString, std::string, String>::PrimitiveVariant(data::Descriptor::STRING, path)
    {
    }

    String::String(const std::string& d, const Path& path) :
        detail::PrimitiveVariant<data::dto::AronString, std::string, String>::PrimitiveVariant(d, data::Descriptor::STRING, path)
    {
    }

    /* operators */
    bool String::operator==(const String& other) const
    {
        const auto& otherAron = other.toAronStringDTO();
        if (this->aron->value != otherAron->value)
        {
            return false;
        }
        return true;
    }
    bool String::operator==(const StringPtr& other) const
    {
        if (!other)
        {
            return false;
        }
        return *this == *other;
    }


    /* static methods */
    StringPtr String::FromAronStringDTO(const data::dto::AronStringPtr& aron)
    {
        if (!aron)
        {
            return nullptr;
        }
        return std::make_shared<String>(aron);
    }

    data::dto::AronStringPtr String::ToAronStringDTO(const StringPtr& navigator)
    {
        return navigator ? navigator->toAronStringDTO() : nullptr;
    }

    /* public member functions */
    data::dto::AronStringPtr String::toAronStringDTO() const
    {
        return aron;
    }


    /* virtual implementations */
    void String::fromString(const std::string& setter)
    {
        setValue(setter);
    }

    std::string String::getShortName() const
    {
        return "String";
    }
    std::string String::getFullName() const
    {
        return "armarx::aron::data::String";
    }

    bool String::fullfillsType(const type::VariantPtr& type) const
    {
        if (!type) return true;
        return type->getDescriptor() == type::Descriptor::STRING;
    }

    type::VariantPtr String::recalculateType() const
    {
        return std::make_shared<type::String>(getPath());
    }
}
