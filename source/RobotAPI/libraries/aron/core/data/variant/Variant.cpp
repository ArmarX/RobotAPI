/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Variant.h"

// ArmarX
#include "../../Exception.h"
#include "Factory.h"
#include "container/Dict.h"


namespace armarx::aron::data
{
    // static data members
    const VariantFactoryPtr Variant::FACTORY = VariantFactoryPtr(new VariantFactory());

    // static methods
    VariantPtr Variant::FromAronDTO(const data::dto::GenericDataPtr& a, const Path& path)
    {
        return FACTORY->create(a, path);
    }

    std::vector<VariantPtr> Variant::FromAronDTO(const std::vector<data::dto::GenericDataPtr>& a, const Path& path)
    {
        std::vector<VariantPtr> ret;
        for (const auto& aron : a)
        {
            ret.push_back(Variant::FromAronDTO(aron, path));
        }
        return ret;
    }

    std::vector<data::dto::GenericDataPtr> Variant::ToAronDTO(const std::vector<VariantPtr>& a)
    {
        std::vector<data::dto::GenericDataPtr> ret;
        for (const auto& v : a)
        {
            if (v)
            {
                ret.push_back(v->toAronDTO());
            }
            else
            {
                ret.push_back(nullptr);
            }
        }
        return ret;
    }
}
