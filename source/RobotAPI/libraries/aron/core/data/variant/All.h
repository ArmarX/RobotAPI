#pragma once

#include "container/All.h"
#include "complex/All.h"
#include "primitive/All.h"

/**
 * A convenience header to include all aron files (full include, not forward declared)
 */
namespace armarx::aron::data
{

}
