/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <cstddef>
#include <functional>
#include <memory>
#include <map>
#include <numeric>
#include <vector>

// Base class
#include "../detail/ComplexVariant.h"

// ArmarX
#include "../container/Dict.h"

// Types
#include "../../../type/variant/ndarray/All.h"


namespace armarx::aron::data
{
    class NDArray;
    using NDArrayPtr = std::shared_ptr<NDArray>;

    class NDArray :
        public detail::ComplexVariant<data::dto::NDArray, NDArray>
    {
    public:
        // constructors
        NDArray(const Path& path = Path());
        NDArray(const data::dto::NDArrayPtr&, const Path& path = Path());
        NDArray(const std::vector<int>&, const std::string&, const std::vector<unsigned char>&, const Path& path = Path());

        // operators
        virtual bool operator==(const NDArray&) const override;
        bool operator==(const NDArrayPtr&) const override;

        // static methods
        static PointerType FromNDArrayDTO(const data::dto::NDArrayPtr& aron);
        static data::dto::NDArrayPtr ToNDArrayDTO(const PointerType& navigator);

        /// Return dimensions in a readable string such as "(2, 3, 4)".
        static std::string DimensionsToString(const std::vector<int>& dimensions);

        // public member functions
        DictPtr getAsDict() const;

        unsigned char* getData() const;
        void setData(unsigned int, const unsigned char*);

        std::vector<unsigned char> getDataAsVector() const;

        std::vector<int> getShape() const;
        void setShape(const std::vector<int>&);
        void addToShape(int);

        std::string getType() const;
        void setType(const std::string&);

        data::dto::NDArrayPtr toNDArrayDTO() const;

        // virtual implementations
        virtual VariantPtr clone() const override
        {
            NDArrayPtr ret(new NDArray(getShape(), getType(), getDataAsVector(), getPath()));
            return ret;
        }

        virtual std::string getShortName() const override;
        virtual std::string getFullName() const override;

        virtual type::VariantPtr recalculateType() const override;
        virtual bool fullfillsType(const type::VariantPtr&) const override;
    };
}

namespace armarx::aron
{
    template<typename... _Args>
    aron::data::NDArrayPtr make_ndarray(_Args&&... args)
    {
        return std::make_shared<aron::data::NDArray>(args...);
    }
}
