/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "NDArray.h"

// Simox
#include <SimoxUtility/algorithm/string.h>

// ArmarX
#include <RobotAPI/libraries/aron/core/data/variant/Factory.h>
#include <RobotAPI/libraries/aron/core/type/variant/ndarray/All.h>

namespace armarx::aron::data
{
    // constructors
    NDArray::NDArray(const Path& path) :
        detail::ComplexVariant<data::dto::NDArray, NDArray>::ComplexVariant(data::Descriptor::NDARRAY, path)
    {

    }

    NDArray::NDArray(const data::dto::NDArrayPtr& o, const Path& path) :
        detail::ComplexVariant<data::dto::NDArray, NDArray>::ComplexVariant(o, data::Descriptor::NDARRAY, path)
    {
    }

    NDArray::NDArray(const std::vector<int>& dim, const std::string& t, const std::vector<unsigned char>& data, const Path& path) :
        NDArray(data::dto::NDArrayPtr(new data::dto::NDArray(aron::VERSION, dim, t, data)), path)
    {
    }

    // operators
    bool NDArray::operator==(const NDArray& other) const
    {
        const auto otherAron = other.toNDArrayDTO();
        if (aron->shape != otherAron->shape)
        {
            return false;
        }
        if (aron->type != otherAron->type)
        {
            return false;
        }
        // performs memcmp
        if (aron->data != otherAron->data)
        {
            return false;
        }
        return true;
    }
    bool NDArray::operator==(const NDArrayPtr& other) const
    {
        if (!other)
        {
            return false;
        }
        return *this == *other;
    }

    // static methods
    NDArrayPtr NDArray::FromNDArrayDTO(const data::dto::NDArrayPtr& aron)
    {
        if (!aron)
        {
            return nullptr;
        }
        return std::make_shared<NDArray>(aron);
    }

    data::dto::NDArrayPtr NDArray::ToNDArrayDTO(const NDArrayPtr& variant)
    {
        return variant ? variant->toNDArrayDTO() : nullptr;
    }

    // public member functions
    DictPtr NDArray::getAsDict() const
    {
        auto dict = std::make_shared<Dict>();
        auto copy_this = FromAronDTO(toAronDTO(), getPath());
        dict->addElement("data", copy_this);
        return dict;
    }

    unsigned char* NDArray::getData() const
    {
        return aron->data.data();
    }

    void NDArray::setData(unsigned int elements, const unsigned char* src)
    {
        aron->data = std::vector<unsigned char>(elements);
        std::memcpy(aron->data.data(), src, elements);
    }

    std::vector<unsigned char> NDArray::getDataAsVector() const
    {
        return aron->data;
    }

    std::vector<int> NDArray::getShape() const
    {
        return aron->shape;
    }

    void NDArray::setShape(const std::vector<int>& d)
    {
        aron->shape = d;
    }

    void NDArray::addToShape(int i)
    {
        aron->shape.push_back(i);
    }

    std::string NDArray::getType() const
    {
        return aron->type;
    }

    void NDArray::setType(const std::string& t)
    {
        if (t.empty())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "The type cannot be empty", getPath());
        }

        aron->type = t;
    }

    data::dto::NDArrayPtr NDArray::toNDArrayDTO() const
    {
        return aron;
    }

    // virtual implementations
    std::string NDArray::getShortName() const
    {
        return "NDArray";
    }

    std::string NDArray::getFullName() const
    {
        return "armarx::aron::data::NDArray<" + simox::alg::to_string(aron->shape, ", ") + ", " + aron->type + ">";
    }

    type::VariantPtr NDArray::recalculateType() const
    {
        throw error::NotImplementedYetException(__PRETTY_FUNCTION__);
    }

    bool NDArray::fullfillsType(const type::VariantPtr& type) const
    {
        if (!type) return true;

        type::Descriptor typeDesc = type->getDescriptor();
        switch (typeDesc)
        {
        case type::Descriptor::MATRIX:
        {
            auto casted = type::Matrix::DynamicCastAndCheck(type);
            return (aron->shape.size() == 3 &&  aron->shape[0] == casted->getRows() && aron->shape[1] == casted->getCols());
        }
        case type::Descriptor::QUATERNION:
        {
            auto casted = type::Quaternion::DynamicCastAndCheck(type);
            return (aron->shape.size() == 3 && aron->shape[0] == 1 && aron->shape[1] == 4);
        }
        case type::Descriptor::POINTCLOUD:
        {
            auto casted = type::PointCloud::DynamicCastAndCheck(type);
            return (aron->shape.size() == 3);
        }
        case type::Descriptor::IMAGE:
        {
            auto casted = type::Image::DynamicCastAndCheck(type);
            return (aron->shape.size() == 3);
        }
        case type::Descriptor::NDARRAY:
        {
            auto casted = type::NDArray::DynamicCastAndCheck(type);
            return (aron->shape.size() == (unsigned int) casted->getNumberDimensions());
        }
        default:
            return false;
        }
    }

    std::string NDArray::DimensionsToString(const std::vector<int>& dimensions)
    {
        std::stringstream ss;
        ss << "(" << simox::alg::join(simox::alg::multi_to_string(dimensions), ", ") << ")";
        return ss.str();
    }

}  // namespace armarx::aron::datanavigator
