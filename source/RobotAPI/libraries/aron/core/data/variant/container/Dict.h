/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <map>
#include <utility>

// Base class
#include "../detail/ContainerVariant.h"

// ArmarX
#include "../../../type/variant/container/Dict.h"
#include "../../../type/variant/container/Object.h"
#include "../../../type/variant/any/AnyObject.h"

namespace armarx::aron::data
{
    class Dict;
    typedef std::shared_ptr<Dict> DictPtr;

    class Dict :
        public detail::ContainerVariant<data::dto::Dict, Dict>
    {
    public:
        // constructors
        Dict(const Path& path = Path());
        Dict(const data::dto::DictPtr&, const Path& path = Path());
        Dict(const std::map<std::string, VariantPtr>&, const Path& path = Path());

        // operators
        bool operator==(const Dict&) const override;
        bool operator==(const DictPtr&) const override;
        VariantPtr operator[](const std::string&) const;
        Dict& operator=(const Dict&);

        static PointerType FromAronDictDTO(const data::dto::DictPtr& aron);
        static data::dto::DictPtr ToAronDictDTO(const PointerType& navigator);

        // public member functions
        data::dto::DictPtr toAronDictDTO() const;
        std::vector<std::string> getAllKeys() const;
        std::string getAllKeysAsString() const;

        void addElement(const std::string& key, const VariantPtr&);
        bool hasElement(const std::string&) const;
        void setElement(const std::string&, const VariantPtr&);
        VariantPtr getElement(const std::string&) const;
        std::map<std::string, VariantPtr> getElements() const;

        VariantPtr at(const std::string&) const;
        void removeElement(const std::string& key);
        void clear();

        // virtual implementations        
        VariantPtr clone() const override
        {
            DictPtr ret(new Dict(getPath()));
            for (const auto& [key, val] : getElements())
            {
                ret->addElement(key, val->clone());
            }
            return ret;
        }

        std::string getShortName() const override;
        std::string getFullName() const override;
        std::vector<VariantPtr> getChildren() const override;
        size_t childrenSize() const override;

        type::VariantPtr recalculateType() const override;
        bool fullfillsType(const type::VariantPtr&) const override;

        VariantPtr navigateAbsolute(const Path& path) const override;

    private:
        // members
        std::map<std::string, VariantPtr> childrenNavigators;
    };
}

namespace armarx::aron
{
    template<typename... _Args>
    aron::data::DictPtr make_dict(_Args&&... args)
    {
        return std::make_shared<aron::data::Dict>(args...);
    }
}
