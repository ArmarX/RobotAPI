/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "List.h"

// ArmarX
#include <ArmarXCore/core/exceptions/Exception.h>
#include <RobotAPI/libraries/aron/core/data/variant/Factory.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/List.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/Tuple.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/Pair.h>

namespace armarx::aron::data
{
    // constructors
    List::List(const Path& path) :
        detail::ContainerVariant<data::dto::List, List>::ContainerVariant(data::Descriptor::LIST, path)
    {
    }

    List::List(const data::dto::ListPtr& l, const Path& path) :
        detail::ContainerVariant<data::dto::List, List>::ContainerVariant(l, data::Descriptor::LIST, path)
    {
        unsigned int i = 0;
        for (const auto& dataPtr : l->elements)
        {
            childrenNavigators.push_back(FromAronDTO(dataPtr, path.withIndex(i++)));
        }
    }

    List::List(const std::vector<VariantPtr>& n, const Path& path) :
        List(path)
    {
        for (const auto& dataPtr : n)
        {
            addElement(dataPtr);
        }
    }

    // operators
    bool List::operator==(const List& other) const
    {
        unsigned int i = 0;
        for (const auto& nav : childrenNavigators)
        {
            if (!other.hasElement(i))
            {
                return false;
            }
            if (!nav)
            {
                if (!other.getElement(i))
                {
                    return false;
                }
            }
            if (!(*nav == other.getElement(i)))
            {
                return false;
            }
            ++i;
        }
        return true;
    }
    bool List::operator==(const ListPtr& other) const
    {
        if (!other)
        {
            return false;
        }
        return *this == *other;
    }

    // static methods
    ListPtr List::FromAronListDTO(const data::dto::ListPtr& aron)
    {
        if (!aron)
        {
            return nullptr;
        }
        return std::make_shared<List>(aron);
    }

    data::dto::ListPtr List::ToAronListDTO(const ListPtr& navigator)
    {
        return navigator ? navigator->toAronListDTO() : nullptr;
    }

    // public member functions
    DictPtr List::getAsDict() const
    {
        auto dict = std::make_shared<Dict>();
        auto copy_this = FromAronDTO(toAronDTO(), getPath());
        dict->addElement("data", copy_this);
        return dict;
    }

    void List::addElement(const VariantPtr& n)
    {
        setElement(aron->elements.size(), n);
    }

    void List::setElement(unsigned int i, const VariantPtr& n)
    {
        if (i > aron->elements.size())
        {
            error::AronException(__PRETTY_FUNCTION__, "Cannot set a listelement at index " + std::to_string(i) + " because this list has size " + std::to_string(aron->elements.size()));
        }

        if (i == aron->elements.size())
        {
            childrenNavigators.push_back(n);
            if (n)
            {
                aron->elements.push_back(n->toAronDTO());
            }
            else
            {
                aron->elements.push_back(nullptr);
            }
        }
        else
        {
            childrenNavigators[i] = n;
            if (n)
            {
                aron->elements[i] = (n->toAronDTO());
            }
            else
            {
                aron->elements[i] = nullptr;
            }
        }
    }

    bool List::hasElement(unsigned int i) const
    {
        return i < childrenNavigators.size();
    }

    VariantPtr List::getElement(unsigned int i) const
    {
        if (i >= childrenNavigators.size())
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The index is out of bounds (size = " + std::to_string(childrenNavigators.size()) + ")", std::to_string(i), getPath());
        }
        return childrenNavigators[i];
    }

    std::vector<VariantPtr> List::getElements() const
    {
        return childrenNavigators;
    }

    void List::removeElement(unsigned int i)
    {
        // Use with care since this function will not work in a loop with increasing indexes
        i = std::min((unsigned int) childrenNavigators.size()-1, i);
        childrenNavigators.erase(childrenNavigators.begin() + i);
        aron->elements.erase(aron->elements.begin() + i);
    }

    void List::clear()
    {
        childrenNavigators.clear();
        aron->elements.clear();
    }

    data::dto::ListPtr List::toAronListDTO() const
    {
        return aron;
    }

    // virtual implementations
    std::string List::getShortName() const
    {
        return "List";
    }

    std::string List::getFullName() const
    {
        return "armarx::aron::data::List";
    }

    // TODO
    type::VariantPtr List::recalculateType() const
    {
        throw error::NotImplementedYetException(__PRETTY_FUNCTION__);
    }

    bool List::fullfillsType(const type::VariantPtr& type) const
    {
        if (!type) return true;

        type::Descriptor typeDesc = type->getDescriptor();
        switch (typeDesc)
        {
            case type::Descriptor::LIST:
            {
                auto listTypeNav = type::List::DynamicCastAndCheck(type);
                for (const auto& nav : childrenNavigators)
                {
                    if (!nav && !listTypeNav->getAcceptedType())
                    {
                        return false;
                    }
                    if (!nav->fullfillsType(listTypeNav->getAcceptedType()))
                    {
                        return false;
                    }
                }
                return true;
            }
            case type::Descriptor::TUPLE:
            {
                auto tupleTypeNav = type::Tuple::DynamicCastAndCheck(type);
                unsigned int i = 0;
                for (const auto& nav : childrenNavigators)
                {
                    if (!nav && !tupleTypeNav->getAcceptedType(i))
                    {
                        return false;
                    }
                    if (!nav->fullfillsType(tupleTypeNav->getAcceptedType(i)))
                    {
                        return false;
                    }
                }
                return true;
            }
            case type::Descriptor::PAIR:
            {
                auto pairTypeNav = type::Pair::DynamicCastAndCheck(type);
                if (childrenSize() != 2)
                {
                    return false;
                }
                if (!childrenNavigators[0] && !pairTypeNav->getFirstAcceptedType())
                {
                    return false;
                }
                if (!childrenNavigators[1] && !pairTypeNav->getSecondAcceptedType())
                {
                    return false;
                }
                return childrenNavigators[0]->fullfillsType(pairTypeNav->getFirstAcceptedType()) && childrenNavigators[1]->fullfillsType(pairTypeNav->getSecondAcceptedType());
            }
            default:
                return false;
        }
    }

    std::vector<VariantPtr> List::getChildren() const
    {
        return childrenNavigators;
    }

    size_t List::childrenSize() const
    {
        return childrenNavigators.size();
    }

    VariantPtr List::navigateAbsolute(const Path& path) const
    {
        if (!path.hasElement())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Could not navigate without a valid path. The path was empty.");
        }
        unsigned int i = std::stoi(path.getFirstElement());
        if (!hasElement(i))
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Could not find an element of a path.", std::to_string(i), std::to_string(childrenSize()));
        }

        if (path.size() == 1)
        {
            return childrenNavigators.at(i);
        }
        else
        {
            Path next = path.withDetachedFirstElement();
            if (!childrenNavigators.at(i))
            {
                throw error::AronException(__PRETTY_FUNCTION__, "Could not navigate into a NULL member. Seems like the member is optional and not set.", next);
            }
            return childrenNavigators.at(i)->navigateAbsolute(next);
        }
    }
}
