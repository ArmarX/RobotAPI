/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>

// Base class
#include "../detail/SpecializedVariant.h"

// ArmarX
#include "Dict.h"
#include "../../../type/variant/container/List.h"
#include "../../../type/variant/container/Pair.h"
#include "../../../type/variant/container/Tuple.h"

namespace armarx::aron::data
{
    class List;
    typedef std::shared_ptr<List> ListPtr;

    class List :
        public detail::ContainerVariant<data::dto::List, List>
    {
    public:
        // constructors
        List(const Path& path = Path());
        List(const data::dto::ListPtr&, const Path& path = Path());
        List(const std::vector<VariantPtr>&, const Path& path = Path());

        // operators
        bool operator==(const List&) const override;
        bool operator==(const ListPtr&) const override;

        // static methods
        static PointerType FromAronListDTO(const data::dto::ListPtr& aron);
        static data::dto::ListPtr ToAronListDTO(const PointerType& navigator);

        // public member functions
        DictPtr getAsDict() const;

        data::dto::ListPtr toAronListDTO() const;

        void addElement(const VariantPtr&);
        void setElement(unsigned int, const VariantPtr&);
        VariantPtr getElement(unsigned int) const;
        bool hasElement(unsigned int) const;
        std::vector<VariantPtr> getElements() const;

        void removeElement(unsigned int);

        void clear();

        // virtual implementations
        VariantPtr clone() const override
        {
            ListPtr ret(new List(getPath()));
            for (const auto& val : getElements())
            {
                ret->addElement(val->clone());
            }
            return ret;
        }

        std::string getShortName() const override;
        std::string getFullName() const override;
        std::vector<VariantPtr> getChildren() const override;
        size_t childrenSize() const override;

        type::VariantPtr recalculateType() const override;
        bool fullfillsType(const type::VariantPtr&) const override;

        VariantPtr navigateAbsolute(const Path& path) const override;

    private:
        std::vector<VariantPtr> childrenNavigators;
    };
}

namespace armarx::aron
{
    template<typename... _Args>
    aron::data::ListPtr make_list(_Args&&... args)
    {
        return std::make_shared<aron::data::List>(args...);
    }
}
