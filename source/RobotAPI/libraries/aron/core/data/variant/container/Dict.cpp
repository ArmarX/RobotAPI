/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Dict.h"

// ArmarX
#include <RobotAPI/libraries/aron/core/data/variant/Factory.h>

#include <SimoxUtility/algorithm/string/string_conversion.h>

namespace armarx::aron::data
{

    // constructors
    Dict::Dict(const Path& path) :
        detail::ContainerVariant<data::dto::Dict, Dict>::ContainerVariant(data::Descriptor::DICT, path)
    {
    }

    Dict::Dict(const data::dto::DictPtr& o, const Path& path) :
        detail::ContainerVariant<data::dto::Dict, Dict>::ContainerVariant(o, data::Descriptor::DICT, path)
    {
        for (const auto& [key, dataPtr] : this->aron->elements)
        {
            childrenNavigators[key] = Variant::FromAronDTO(dataPtr, path.withElement(key));
        }
    }

    Dict::Dict(const std::map<std::string, VariantPtr>& m, const Path& path) :
        Dict(path)
    {
        for (const auto& [key, dataPtr] : m)
        {
            addElement(key, dataPtr);
        }
    }

    // operators
    bool Dict::operator==(const Dict& other) const
    {
        for (const auto& [key, nav] : childrenNavigators)
        {
            if (not(other.hasElement(key)))
            {
                return false;
            }
            if (!nav)
            {
                return !((bool) other.getElement(key));
            }
            if (not(*nav == *other.getElement(key)))
            {
                return false;
            }
        }
        return true;
    }
    bool Dict::operator==(const DictPtr& other) const
    {
        if (!other)
        {
            return false;
        }
        return *this == *other;
    }
    VariantPtr Dict::operator[](const std::string& s) const
    {
        return getElement(s);
    }
    Dict& Dict::operator=(const Dict& other)
    {
        this->aron = other.aron;
        this->childrenNavigators = other.childrenNavigators;
        return *this;
    }

    // static methods
    DictPtr Dict::FromAronDictDTO(const data::dto::DictPtr& aron)
    {
        if (!aron)
        {
            return nullptr;
        }
        return std::make_shared<Dict>(aron);
    }

    data::dto::DictPtr Dict::ToAronDictDTO(const DictPtr& navigator)
    {
        return navigator ? navigator->toAronDictDTO() : nullptr;
    }

    data::dto::DictPtr Dict::toAronDictDTO() const
    {
        return aron;
    }

    // public member functions
    std::vector<std::string> Dict::getAllKeys() const
    {
        std::vector<std::string> ret;
        for (const auto& [key, _] : childrenNavigators)
        {
            ret.push_back(key);
        }
        return ret;
    }

    std::string Dict::getAllKeysAsString() const
    {
        return simox::alg::to_string(getAllKeys(), ", ");
    }

    void Dict::addElement(const std::string& key, const VariantPtr& data)
    {
        if (hasElement(key))
        {
            throw error::AronException(__PRETTY_FUNCTION__, "The key '"+key+"' already exists in a aron dict.");
        }
        setElement(key, data);
    }

    bool Dict::hasElement(const std::string& key) const
    {
        return childrenNavigators.count(key) > 0;
    }

    VariantPtr Dict::getElement(const std::string& key) const
    {
        auto it = childrenNavigators.find(key);
        if (it == childrenNavigators.end())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Could not find key '" + key + "'. But I found the following keys: [" + simox::alg::join(this->getAllKeys(), ", ") + "]", getPath());
        }
        return it->second;
    }

    std::map<std::string, VariantPtr> Dict::getElements() const
    {
        return childrenNavigators;
    }

    void Dict::setElement(const std::string& key, const VariantPtr& data)
    {
        this->childrenNavigators[key] = data;
        if (data)
        {
            this->aron->elements[key] = data->toAronDTO();
        }
        else
        {
            this->aron->elements[key] = nullptr;
        }
    }

    void Dict::removeElement(const std::string& key)
    {
        childrenNavigators.erase(key);
        aron->elements.erase(key);
    }

    void Dict::clear()
    {
        childrenNavigators.clear();
        aron->elements.clear();
    }

    VariantPtr Dict::at(const std::string& s) const
    {
        return getElement(s);
    }

    // virtual implementations
    std::string Dict::getShortName() const
    {
        return "Dict";
    }

    std::string Dict::getFullName() const
    {
        return "armarx::aron::data::Dict";
    }

    type::VariantPtr Dict::recalculateType() const
    {
        throw error::NotImplementedYetException(__PRETTY_FUNCTION__);
    }

    bool Dict::fullfillsType(const type::VariantPtr& type) const
    {
        if (!type) return true;

        type::Descriptor typeDesc = type->getDescriptor();

        switch (typeDesc)
        {
            case type::Descriptor::OBJECT:
            {
                auto objectTypeNav = type::Object::DynamicCastAndCheck(type);
                for (const auto& [key, nav] : childrenNavigators)
                {
                    if (!objectTypeNav->hasMemberType(key))
                    {
                        return false;
                    }
                    if (!nav || !objectTypeNav->getMemberType(key))
                    {
                        return false;
                    }
                    if (!nav->fullfillsType(objectTypeNav->getMemberType(key)))
                    {
                        return false;
                    }
                }
                return true;
            }
            case type::Descriptor::DICT:
            {
                auto dictTypeNav = type::Dict::DynamicCastAndCheck(type);
                for (const auto& [key, nav] : childrenNavigators)
                {
                    (void) key;
                    if (!nav || !dictTypeNav->getAcceptedType())
                    {
                        return false;
                    }
                    if (!nav->fullfillsType(dictTypeNav->getAcceptedType()))
                    {
                        return false;
                    }
                }
                return true;
            }
            default:
                return false;
        }
    }

    std::vector<VariantPtr> Dict::getChildren() const
    {
        std::vector<VariantPtr> ret(childrenNavigators.size());
        for (const auto& [key, nav] : childrenNavigators)
        {
            ret.push_back(nav);
        }
        return ret;
    }

    size_t Dict::childrenSize() const
    {
        return childrenNavigators.size();
    }

    VariantPtr Dict::navigateAbsolute(const Path& path) const
    {
        if (!path.hasElement())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Could not navigate without a valid path", path);
        }
        std::string el = path.getFirstElement();
        if (!hasElement(el))
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Could not find an element of a path.", el, path);
        }

        if (path.size() == 1)
        {
            return childrenNavigators.at(el);
        }
        else
        {
            Path next = path.withDetachedFirstElement();
            if (!childrenNavigators.at(el))
            {
                throw error::AronException(__PRETTY_FUNCTION__, "Could not navigate into a NULL member. Seems like the member is optional and not set.", next);
            }
            return childrenNavigators.at(el)->navigateAbsolute(next);
        }
    }
}
