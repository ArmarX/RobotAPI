/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL

// Header
#include "Factory.h"

// ArmarX
#include <RobotAPI/libraries/aron/core/data/variant/All.h>

namespace armarx::aron::data
{

    // Access method
    std::unique_ptr<data::Variant> VariantFactory::create(const data::dto::GenericDataPtr& aron, const Path& path) const
    {
        // if aron is null ==> e.g. optional or ptr not set
        if (!aron)
        {
            return nullptr;
        };

        auto descriptor = data::Aron2Descriptor(*aron);
        switch(descriptor)
        {
            case data::Descriptor::LIST: return std::make_unique<data::List>(data::dto::ListPtr::dynamicCast(aron), path);
            case data::Descriptor::DICT: return std::make_unique<data::Dict>(data::dto::DictPtr::dynamicCast(aron), path);
            case data::Descriptor::NDARRAY: return std::make_unique<data::NDArray>(data::dto::NDArrayPtr::dynamicCast(aron), path);
            case data::Descriptor::INT: return std::make_unique<data::Int>(data::dto::AronIntPtr::dynamicCast(aron), path);
            case data::Descriptor::LONG: return std::make_unique<data::Long>(data::dto::AronLongPtr::dynamicCast(aron), path);
            case data::Descriptor::FLOAT: return std::make_unique<data::Float>(data::dto::AronFloatPtr::dynamicCast(aron), path);
            case data::Descriptor::DOUBLE: return std::make_unique<data::Double>(data::dto::AronDoublePtr::dynamicCast(aron), path);
            case data::Descriptor::STRING: return std::make_unique<data::String>(data::dto::AronStringPtr::dynamicCast(aron), path);
            case data::Descriptor::BOOL: return std::make_unique<data::Bool>(data::dto::AronBoolPtr::dynamicCast(aron), path);
            case data::Descriptor::UNKNOWN: break;
        }
        throw error::ValueNotValidException(__PRETTY_FUNCTION__, "", std::to_string((int) descriptor), path);
    }
}
