/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <string>
#include <unordered_map>

// ArmarX
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/core/Path.h>
#include <RobotAPI/libraries/aron/core/Exception.h>
#include <RobotAPI/libraries/aron/core/Descriptor.h>

#include <RobotAPI/libraries/aron/core/type/variant/Variant.h>

namespace armarx::aron::data
{
    class VariantFactory;
    typedef std::unique_ptr<VariantFactory> VariantFactoryPtr;

    class Variant;
    typedef std::shared_ptr<Variant> VariantPtr;

    /**
     * @brief The Variant class. It represents a data object (a variant containing data).
     * Every data variant inherits from this class. It provdes basic methods for cast/conversion and
     * holds a descriptor, specifying the data type.
     *
     * If you have a unknown variant and you want to run a specific method based on the type, we suggest to use the visitor pattern (@see data/visitor/Visitor.h)
     *
     * Note that a data variant differs from a type variant. A data variant contains data (e.g. if you have a list the data object contains the list elements) while
     * a type object holds the static type information (e.g. the accepted type of the list).
     * The elements of a list variant without type information may have different types (e.g. element [0] is an int variant whil element [1] is a string variant)
     *
     * Also note, that while a type variant should always be defined (will not be NULL!), a data object can be NULL.
     * This happens, if a maybe type (e.g. optional) is not set. The underlying ice representation will have a nullptr instead.
     */
    class Variant
    {
    public:
        using PointerType = VariantPtr;

    public:
        // constructors
        Variant() = delete;
        Variant(const data::Descriptor& descriptor, const Path& path) :
            descriptor(descriptor),
            path(path)
        {
        }
        virtual ~Variant() = default;

        // operators
        virtual bool operator==(const Variant& other) const = 0;
        bool operator==(const VariantPtr& other) const
        {
            if (!other)
            {
                return false;
            }

            return *this == *other;
        }

        // static methods
        /// create a variant from a dto object
        static VariantPtr FromAronDTO(const data::dto::GenericDataPtr&, const Path& = Path());

        /// create a list of variants from a list of dto objects
        static std::vector<VariantPtr> FromAronDTO(const std::vector<data::dto::GenericDataPtr>&, const Path& = Path());

        /// return a list of dto objects from a list of variant objects
        static std::vector<data::dto::GenericDataPtr> ToAronDTO(const std::vector<VariantPtr>&);

        /// getter for the descriptor enum
        data::Descriptor getDescriptor() const
        {
            return descriptor;
        }

        /// get the path
        Path getPath() const
        {
            return path;
        }

        /// get the path as string
        std::string pathToString() const
        {
            return path.toString();
        }

        // virtual definitions
        /// get a pointer to a copy of this variant
        virtual VariantPtr clone() const = 0;

        /// get the children of a data variant
        virtual std::vector<VariantPtr> getChildren() const = 0;

        /// get the children size of a data variant
        virtual size_t childrenSize() const = 0;

        /// recalculate the type of a data variant. Please not tha the mapping ist NOT bijective, so the calculated type may be wrong
        virtual type::VariantPtr recalculateType() const = 0;

        /// checks, if the current data variant fullfills the given type
        virtual bool fullfillsType(const type::VariantPtr&) const = 0;

        /// naviate absolute
        virtual VariantPtr navigateAbsolute(const Path& path) const = 0;

        /// navigate relative
        virtual VariantPtr navigateRelative(const Path& path) const = 0;

        /// get a short str representation of this variant
        virtual std::string getShortName() const = 0;

        /// get the full str representation of this variant
        virtual std::string getFullName() const = 0;

        /// convert the variant to the ice representation
        virtual data::dto::GenericDataPtr toAronDTO() const = 0;

    protected:
        const data::Descriptor descriptor;
        const Path path;

    private:
        static const VariantFactoryPtr FACTORY;
    };

    template <class T>
    concept isVariant = std::is_base_of<Variant, T>::value;
}
