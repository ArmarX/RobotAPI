/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "../visitor/Visitor.h"
#include "../rw/Reader.h"
#include "../rw/Writer.h"

namespace armarx::aron::data
{
    // prototypes
    template <class ReaderImplementation, class WriterImplementation, class DerivedT>
    requires isReader<ReaderImplementation> && isWriter<WriterImplementation>
    struct Converter;

    template <class T>
    concept isConverter = std::is_base_of<Converter<typename T::ReaderType, typename T::WriterType, typename T::This>, T>::value;

    template <class ConverterImplementation>
    requires isConverter<ConverterImplementation>
    typename ConverterImplementation::WriterReturnType readAndWrite(typename ConverterImplementation::ReaderInputType& o);

    /// Converter struct providing the needed methods.
    /// WriterImplementation is a writer class, TODO: add concepts
    template <class ReaderImplementation, class WriterImplementation, class DerivedT>
    requires isReader<ReaderImplementation> && isWriter<WriterImplementation>
    struct Converter : virtual public Visitor<typename ReaderImplementation::InputType>
    {
        using WriterType = WriterImplementation;
        using ReaderType = ReaderImplementation;
        using This = DerivedT;
        using WriterReturnType = typename WriterImplementation::ReturnType;
        using ReaderInputType = typename ReaderImplementation::InputType;
        using ReaderInputTypeNonConst = typename ReaderImplementation::InputTypeNonConst;

        ReaderImplementation r;
        WriterImplementation w;
        WriterReturnType last_returned;

        virtual ~Converter() = default;

        data::Descriptor getDescriptor(ReaderInputType& o) final
        {
            return r.getDescriptor(o);
        }

        void visitDict(ReaderInputType& o) final
        {
            std::map<std::string, ReaderInputTypeNonConst> elementsOfInput;
            std::map<std::string, WriterReturnType> elementsReturn;
            Path p;
            r.readDict(o, elementsOfInput, p);
            for (const auto& [key, value] : elementsOfInput)
            {
                auto converted = readAndWrite<DerivedT>(value);
                elementsReturn.insert({key, converted});
            }

            last_returned = w.writeDict(elementsReturn, std::nullopt, p);
        };

        void visitList(ReaderInputType& o) final
        {
            std::vector<ReaderInputTypeNonConst> elementsOfInput;
            std::vector<WriterReturnType> elementsReturn;
            Path p;
            r.readList(o, elementsOfInput, p);
            for (const auto& value : elementsOfInput)
            {
                auto converted = readAndWrite<DerivedT>(value);
                elementsReturn.push_back(converted);
            }
            last_returned = w.writeList(elementsReturn, p);
        };

        void visitNDArray(ReaderInputType& o) final
        {
            std::string type;
            std::vector<int> shape;
            std::vector<unsigned char> data;
            Path p;
            r.readNDArray(o, shape, type, data, p);
            last_returned = w.writeNDArray(shape, type, data.data(), p);
        };

        void visitInt(ReaderInputType& o) final
        {
            int i;
            Path p;
            r.readInt(o, i, p);
            last_returned = w.writeInt(i, p);
        };

        void visitLong(ReaderInputType& o) final
        {
            long i;
            Path p;
            r.readLong(o, i, p);
            last_returned = w.writeLong(i, p);
        };

        void visitFloat(ReaderInputType& o) final
        {
            float i;
            Path p;
            r.readFloat(o, i, p);
            last_returned = w.writeFloat(i, p);
        };

        void visitDouble(ReaderInputType& o) final
        {
            double i;
            Path p;
            r.readDouble(o, i, p);
            last_returned = w.writeDouble(i, p);
        };

        void visitBool(ReaderInputType& o) final
        {
            bool i;
            Path p;
            r.readBool(o, i, p);
            last_returned = w.writeBool(i, p);
        };

        void visitString(ReaderInputType& o) final
        {
            std::string i;
            Path p;
            r.readString(o, i, p);
            last_returned = w.writeString(i, p);
        };

        void visitUnknown(ReaderInputType& o) final
        {
            if (!r.readNull(o))
            {
                throw error::AronException(__PRETTY_FUNCTION__, "A visitor got data but the enum is unknown.");
            }
            w.writeNull();
        };
    };

    /// the function to read from a variant and write to a writer T
    /// returns the returntype of T
    template <class ConverterImplementation>
    requires isConverter<ConverterImplementation>
    typename ConverterImplementation::WriterReturnType readAndWrite(typename ConverterImplementation::ReaderInputType& o)
    {
        ConverterImplementation v;
        data::visit(v, o);
        return v.last_returned;
    }
}
