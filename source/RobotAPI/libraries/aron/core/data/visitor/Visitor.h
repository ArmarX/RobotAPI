/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <string>
#include <vector>

#include "../../Descriptor.h"
#include "../../Exception.h"

namespace armarx::aron::data
{
    /**
     * @see type/visitor/Visitor.h
     */
    template <class T>
    struct VisitorBase
    {
        using Input = T;

        virtual data::Descriptor getDescriptor(Input&) = 0;
        virtual ~VisitorBase() = default;
    };

    /**
     * @see type/visitor/Visitor.h
     */
    template <class T1, class T2>
    struct TypedVisitorBase
    {
        using DataInput = T1;
        using TypeInput = T2;

        virtual type::Descriptor getDescriptor(DataInput&, TypeInput&) = 0;
        virtual ~TypedVisitorBase() = default;
    };

    /**
     * @see type/visitor/Visitor.h
     */
    template <class T>
    struct Visitor : virtual public VisitorBase<T>
    {
        using Input = typename VisitorBase<T>::Input;

        virtual void visitDict(Input&) {};
        virtual void visitList(Input&) {};
        virtual void visitNDArray(Input&) {};
        virtual void visitInt(Input&) {};
        virtual void visitLong(Input&) {};
        virtual void visitFloat(Input&) {};
        virtual void visitDouble(Input&) {};
        virtual void visitBool(Input&) {};
        virtual void visitString(Input&) {};
        virtual void visitUnknown(Input&) { throw error::AronException(__PRETTY_FUNCTION__, "Unknown type in visitor."); }
        virtual ~Visitor() = default;
    };

    /**
     * @see type/visitor/Visitor.h
     */
    template <class T1, class T2>
    struct TypedVisitor : virtual public TypedVisitorBase<T1, T2>
    {
        using DataInput = typename TypedVisitorBase<T1, T2>::DataInput;
        using TypeInput = typename TypedVisitorBase<T1, T2>::TypeInput;

        virtual void visitObject(DataInput&, TypeInput&) {};
        virtual void visitDict(DataInput&, TypeInput&) {};
        virtual void visitPair(DataInput&, TypeInput&) {};
        virtual void visitTuple(DataInput&, TypeInput&) {};
        virtual void visitList(DataInput&, TypeInput&) {};
        virtual void visitMatrix(DataInput&, TypeInput&) {};
        virtual void visitNDArray(DataInput&, TypeInput&) {};
        virtual void visitQuaternion(DataInput&, TypeInput&) {};
        virtual void visitImage(DataInput&, TypeInput&) {};
        virtual void visitPointCloud(DataInput&, TypeInput&) {};
        virtual void visitIntEnum(DataInput&, TypeInput&) {};
        virtual void visitInt(DataInput&, TypeInput&) {};
        virtual void visitLong(DataInput&, TypeInput&) {};
        virtual void visitFloat(DataInput&, TypeInput&) {};
        virtual void visitDouble(DataInput&, TypeInput&) {};
        virtual void visitBool(DataInput&, TypeInput&) {};
        virtual void visitString(DataInput&, TypeInput&) {};
        virtual void visitAnyObject(DataInput&, TypeInput&) {};
        virtual void visitUnknown(DataInput&, TypeInput&) { throw error::AronException(__PRETTY_FUNCTION__, "Unknown type in visitor."); }
        virtual ~TypedVisitor() = default;
    };

    template <class T, class Data>
    concept isVisitor = std::is_base_of<Visitor<Data>, T>::value;

    template <class T, class Data, class Type>
    concept isTypedVisitor = std::is_base_of<TypedVisitor<Data, Type>, T>::value;


    /**
     * @see type/visitor/Visitor.h
     */
    template <class VisitorImplementation>
    requires isVisitor<VisitorImplementation, typename VisitorImplementation::Input>
    void visit(VisitorImplementation& v, typename VisitorImplementation::Input& o)
    {
        auto descriptor = v.getDescriptor(o);
        switch (descriptor)
        {
            case data::Descriptor::LIST:
                return v.visitList(o);
            case data::Descriptor::DICT:
                return v.visitDict(o);
            case data::Descriptor::NDARRAY:
                return v.visitNDArray(o);
            case data::Descriptor::INT:
                return v.visitInt(o);
            case data::Descriptor::LONG:
                return v.visitLong(o);
            case data::Descriptor::FLOAT:
                return v.visitFloat(o);
            case data::Descriptor::DOUBLE:
                return v.visitDouble(o);
            case data::Descriptor::STRING:
                return v.visitString(o);
            case data::Descriptor::BOOL:
                return v.visitBool(o);
            case data::Descriptor::UNKNOWN:
                return v.visitUnknown(o);
        }
    }

    /**
     * @see type/visitor/Visitor.h
     * This implemntation visits a data and type representation simultaneously (based on the type descriptor).
     * Does NOT check if the data and type representation match!
     */
    template <class VisitorImplementation>
    requires isTypedVisitor<VisitorImplementation, typename VisitorImplementation::DataInput, typename VisitorImplementation::TypeInput>
    void visit(VisitorImplementation& v, typename VisitorImplementation::DataInput& o, typename VisitorImplementation::TypeInput& t)
    {
        auto descriptor = v.getDescriptor(o, t);
        switch (descriptor)
        {
        case type::Descriptor::OBJECT:
            return v.visitObject(o, t);
        case type::Descriptor::LIST:
            return v.visitList(o, t);
        case type::Descriptor::DICT:
            return v.visitDict(o, t);
        case type::Descriptor::PAIR:
            return v.visitPair(o, t);
        case type::Descriptor::TUPLE:
            return v.visitTuple(o, t);
        case type::Descriptor::NDARRAY:
            return v.visitNDArray(o, t);
        case type::Descriptor::MATRIX:
            return v.visitMatrix(o, t);
        case type::Descriptor::IMAGE:
            return v.visitImage(o, t);
        case type::Descriptor::POINTCLOUD:
            return v.visitPointCloud(o, t);
        case type::Descriptor::QUATERNION:
            return v.visitQuaternion(o, t);
        case type::Descriptor::INT:
            return v.visitInt(o, t);
        case type::Descriptor::LONG:
            return v.visitLong(o, t);
        case type::Descriptor::FLOAT:
            return v.visitFloat(o, t);
        case type::Descriptor::DOUBLE:
            return v.visitDouble(o, t);
        case type::Descriptor::STRING:
            return v.visitString(o, t);
        case type::Descriptor::BOOL:
            return v.visitBool(o, t);
        case type::Descriptor::ANY_OBJECT:
            return v.visitAnyObject(o, t);
        case type::Descriptor::INT_ENUM:
            return v.visitIntEnum(o, t);
        case type::Descriptor::UNKNOWN:
            return v.visitUnknown(o, t);
        }
    }
}
