/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "VariantVisitor.h"

#include "../../variant/All.h"
#include "../../../type/visitor/variant/VariantVisitor.h"

namespace armarx::aron::data
{
    /****************************************************************************
     * ConstVariantVisitor
     ***************************************************************************/
    data::Descriptor ConstVariantVisitor::GetDescriptor(Input& n)
    {
        if (!n)
        {
            return data::Descriptor::UNKNOWN;
        }
        return n->getDescriptor();
    }

    data::Descriptor ConstVariantVisitor::getDescriptor(Input& n)
    {
        return GetDescriptor(n);
    }

    void ConstVariantVisitor::visitDict(Input& i)
    {
        auto aron = data::Dict::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitList(Input& i)
    {
        auto aron = data::List::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitNDArray(Input& i)
    {
        auto aron = data::NDArray::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitInt(Input& i)
    {
        auto aron = data::Int::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitLong(Input& i)
    {
        auto aron = data::Long::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitFloat(Input& i)
    {
        auto aron = data::Float::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitDouble(Input& i)
    {
        auto aron = data::Double::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitBool(Input& i)
    {
        auto aron = data::Bool::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitString(Input& i)
    {
        auto aron = data::String::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void ConstVariantVisitor::visitAronVariant(const data::DictPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const data::ListPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const data::NDArrayPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const data::IntPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const data::LongPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const data::FloatPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const data::DoublePtr&) {}
    void ConstVariantVisitor::visitAronVariant(const data::BoolPtr&) {}
    void ConstVariantVisitor::visitAronVariant(const data::StringPtr&) {}


    /****************************************************************************
     * ConstTypedVariantVisitor
     ***************************************************************************/
    type::Descriptor ConstTypedVariantVisitor::GetDescriptor(DataInput& i, TypeInput& j)
    {
        auto t_desc = type::ConstVariantVisitor::GetDescriptor(j);
        if (t_desc == type::Descriptor::UNKNOWN)
        {
            auto d_desc = ConstVariantVisitor::GetDescriptor(i);
            t_desc = aron::data::defaultconversion::Data2TypeDescriptor.at(d_desc);
        }
        return t_desc;
    }

    type::Descriptor ConstTypedVariantVisitor::getDescriptor(DataInput& i, TypeInput& n)
    {
        return GetDescriptor(i, n);
    }

    void ConstTypedVariantVisitor::visitObject(DataInput& i, TypeInput& j)
    {
        auto d = data::Dict::DynamicCastAndCheck(i);
        auto t = type::Object::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitDict(DataInput& i, TypeInput& j)
    {
        auto d = data::Dict::DynamicCastAndCheck(i);
        auto t = type::Dict::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitPair(DataInput& i, TypeInput& j)
    {
        auto d = data::List::DynamicCastAndCheck(i);
        auto t = type::Pair::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitTuple(DataInput& i, TypeInput& j)
    {
        auto d = data::List::DynamicCastAndCheck(i);
        auto t = type::Tuple::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitList(DataInput& i, TypeInput& j)
    {
        auto d = data::List::DynamicCastAndCheck(i);
        auto t = type::List::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitMatrix(DataInput& i, TypeInput& j)
    {
        auto d = data::NDArray::DynamicCastAndCheck(i);
        auto t = type::Matrix::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitNDArray(DataInput& i, TypeInput& j)
    {
        auto d = data::NDArray::DynamicCastAndCheck(i);
        auto t = type::NDArray::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitQuaternion(DataInput& i, TypeInput& j)
    {
        auto d = data::NDArray::DynamicCastAndCheck(i);
        auto t = type::Quaternion::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitImage(DataInput& i, TypeInput& j)
    {
        auto d = data::NDArray::DynamicCastAndCheck(i);
        auto t = type::Image::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitPointCloud(DataInput& i, TypeInput& j)
    {
        auto d = data::NDArray::DynamicCastAndCheck(i);
        auto t = type::PointCloud::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitIntEnum(DataInput& i, TypeInput& j)
    {
        auto d = data::Int::DynamicCastAndCheck(i);
        auto t = type::IntEnum::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitInt(DataInput& i, TypeInput& j)
    {
        auto d = data::Int::DynamicCastAndCheck(i);
        auto t = type::Int::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitLong(DataInput& i, TypeInput& j)
    {
        auto d = data::Long::DynamicCastAndCheck(i);
        auto t = type::Long::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitFloat(DataInput& i, TypeInput& j)
    {
        auto d = data::Float::DynamicCastAndCheck(i);
        auto t = type::Float::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitDouble(DataInput& i, TypeInput& j)
    {
        auto d = data::Double::DynamicCastAndCheck(i);
        auto t = type::Double::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitBool(DataInput& i, TypeInput& j)
    {
        auto d = data::Bool::DynamicCastAndCheck(i);
        auto t = type::Bool::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitString(DataInput& i, TypeInput& j)
    {
        auto d = data::String::DynamicCastAndCheck(i);
        auto t = type::String::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void ConstTypedVariantVisitor::visitAronVariant(const data::DictPtr&, const type::ObjectPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::DictPtr&, const type::DictPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::ListPtr&, const type::ListPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::ListPtr&, const type::PairPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::ListPtr&, const type::TuplePtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::NDArrayPtr&, const type::MatrixPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::NDArrayPtr&, const type::NDArrayPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::NDArrayPtr&, const type::QuaternionPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::NDArrayPtr&, const type::PointCloudPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::NDArrayPtr&, const type::ImagePtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::IntPtr&, const type::IntEnumPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::IntPtr&, const type::IntPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::LongPtr&, const type::LongPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::FloatPtr&, const type::FloatPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::DoublePtr&, const type::DoublePtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::BoolPtr&, const type::BoolPtr&) {}
    void ConstTypedVariantVisitor::visitAronVariant(const data::StringPtr&, const type::StringPtr&) {}



    /****************************************************************************
     * RecursiveConstVariantVisitor
     ***************************************************************************/
    data::Descriptor RecursiveConstVariantVisitor::getDescriptor(Input& n)
    {
        return ConstVariantVisitor::GetDescriptor(n);
    }

    RecursiveConstVariantVisitor::MapElements RecursiveConstVariantVisitor::GetDictElements(Input& n)
    {
        auto x = data::Dict::DynamicCastAndCheck(n);
        return x->getElements();
    }

    RecursiveConstVariantVisitor::MapElements RecursiveConstVariantVisitor::getDictElements(Input& n)
    {
        return GetDictElements(n);
    }

    RecursiveConstVariantVisitor::ListElements RecursiveConstVariantVisitor::GetListElements(Input& n)
    {
        auto x = data::List::DynamicCastAndCheck(n);
        return x->getElements();
    }

    RecursiveConstVariantVisitor::ListElements RecursiveConstVariantVisitor::getListElements(Input& n)
    {
        return GetListElements(n);
    }

    void RecursiveConstVariantVisitor::visitDictOnEnter(Input& i)
    {
        auto aron = data::Dict::DynamicCastAndCheck(i);
        visitAronVariantOnEnter(aron);
    }

    void RecursiveConstVariantVisitor::visitDictOnExit(Input& i)
    {
        auto aron = data::Dict::DynamicCastAndCheck(i);
        visitAronVariantOnExit(aron);
    }

    void RecursiveConstVariantVisitor::visitListOnEnter(Input& i)
    {
        auto aron = data::List::DynamicCastAndCheck(i);
        visitAronVariantOnEnter(aron);
    }

    void RecursiveConstVariantVisitor::visitListOnExit(Input& i)
    {
        auto aron = data::List::DynamicCastAndCheck(i);
        visitAronVariantOnExit(aron);
    }

    void RecursiveConstVariantVisitor::visitNDArray(Input& i)
    {
        auto aron = data::NDArray::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveConstVariantVisitor::visitInt(Input& i)
    {
        auto aron = data::Int::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveConstVariantVisitor::visitLong(Input& i)
    {
        auto aron = data::Long::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveConstVariantVisitor::visitFloat(Input& i)
    {
        auto aron = data::Float::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveConstVariantVisitor::visitDouble(Input& i)
    {
        auto aron = data::Double::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveConstVariantVisitor::visitBool(Input& i)
    {
        auto aron = data::Bool::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveConstVariantVisitor::visitString(Input& i)
    {
        auto aron = data::String::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveConstVariantVisitor::visitAronVariantOnEnter(const data::DictPtr&) {}
    void RecursiveConstVariantVisitor::visitAronVariantOnEnter(const data::ListPtr&) {}
    void RecursiveConstVariantVisitor::visitAronVariantOnExit(const data::DictPtr&) {}
    void RecursiveConstVariantVisitor::visitAronVariantOnExit(const data::ListPtr&) {}
    void RecursiveConstVariantVisitor::visitAronVariant(const data::NDArrayPtr&) {}
    void RecursiveConstVariantVisitor::visitAronVariant(const data::IntPtr&) {}
    void RecursiveConstVariantVisitor::visitAronVariant(const data::LongPtr&) {}
    void RecursiveConstVariantVisitor::visitAronVariant(const data::FloatPtr&) {}
    void RecursiveConstVariantVisitor::visitAronVariant(const data::DoublePtr&) {}
    void RecursiveConstVariantVisitor::visitAronVariant(const data::BoolPtr&) {}
    void RecursiveConstVariantVisitor::visitAronVariant(const data::StringPtr&) {}



    /****************************************************************************
     * RecursiveVariantVisitor
     ***************************************************************************/
    data::Descriptor RecursiveVariantVisitor::getDescriptor(Input& n)
    {
        return ConstVariantVisitor::GetDescriptor(n);
    }

    std::map<std::string, RecursiveVariantVisitor::InputNonConst> RecursiveVariantVisitor::getDictElements(Input& n)
    {
        return RecursiveConstVariantVisitor::GetDictElements(n);
    }

    std::vector<RecursiveVariantVisitor::InputNonConst> RecursiveVariantVisitor::getListElements(Input& n)
    {
        return RecursiveConstVariantVisitor::GetListElements(n);
    }

    void RecursiveVariantVisitor::visitDictOnEnter(Input& i)
    {
        auto aron = data::Dict::DynamicCastAndCheck(i);
        visitAronVariantOnEnter(aron);
    }

    void RecursiveVariantVisitor::visitDictOnExit(Input& i)
    {
        auto aron = data::Dict::DynamicCastAndCheck(i);
        visitAronVariantOnExit(aron);
    }

    void RecursiveVariantVisitor::visitListOnEnter(Input& i)
    {
        auto aron = data::List::DynamicCastAndCheck(i);
        visitAronVariantOnEnter(aron);
    }

    void RecursiveVariantVisitor::visitListOnExit(Input& i)
    {
        auto aron = data::List::DynamicCastAndCheck(i);
        visitAronVariantOnExit(aron);
    }

    void RecursiveVariantVisitor::visitNDArray(Input& i)
    {
        auto aron = data::NDArray::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveVariantVisitor::visitInt(Input& i)
    {
        auto aron = data::Int::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveVariantVisitor::visitLong(Input& i)
    {
        auto aron = data::Long::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveVariantVisitor::visitFloat(Input& i)
    {
        auto aron = data::Float::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveVariantVisitor::visitDouble(Input& i)
    {
        auto aron = data::Double::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveVariantVisitor::visitBool(Input& i)
    {
        auto aron = data::Bool::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveVariantVisitor::visitString(Input& i)
    {
        auto aron = data::String::DynamicCastAndCheck(i);
        visitAronVariant(aron);
    }

    void RecursiveVariantVisitor::visitAronVariantOnEnter(data::DictPtr&) {}
    void RecursiveVariantVisitor::visitAronVariantOnEnter(data::ListPtr&) {}
    void RecursiveVariantVisitor::visitAronVariantOnExit(data::DictPtr&) {}
    void RecursiveVariantVisitor::visitAronVariantOnExit(data::ListPtr&) {}
    void RecursiveVariantVisitor::visitAronVariant(data::NDArrayPtr&) {}
    void RecursiveVariantVisitor::visitAronVariant(data::IntPtr&) {}
    void RecursiveVariantVisitor::visitAronVariant(data::LongPtr&) {}
    void RecursiveVariantVisitor::visitAronVariant(data::FloatPtr&) {}
    void RecursiveVariantVisitor::visitAronVariant(data::DoublePtr&) {}
    void RecursiveVariantVisitor::visitAronVariant(data::BoolPtr&) {}
    void RecursiveVariantVisitor::visitAronVariant(data::StringPtr&) {}


    /****************************************************************************
     * RecursiveConstTypedVariantVisitor
     ***************************************************************************/
    type::Descriptor RecursiveConstTypedVariantVisitor::getDescriptor(DataInput& o, TypeInput& n)
    {
        return ConstTypedVariantVisitor::GetDescriptor(o, n);
    }

    RecursiveConstTypedVariantVisitor::MapElements
    RecursiveConstTypedVariantVisitor::GetObjectElements(DataInput& o, TypeInput& t)
    {
        std::map<std::string, std::pair<data::VariantPtr, type::VariantPtr>> ret;
        auto x = data::Dict::DynamicCastAndCheck(o);
        auto y = type::Object::DynamicCastAndCheck(t);

        ARMARX_CHECK_NOT_NULL(y);

        if (x)
        {
            for (const auto& [key, e] : x->getElements())
            {
                auto ct = y->getMemberType(key);
                ret.insert({key, {e, ct}});
            }
        }
        return ret;
    }

    RecursiveConstTypedVariantVisitor::MapElements
    RecursiveConstTypedVariantVisitor::GetObjectElementsWithNullType(DataInput& o, TypeInput& t)
    {
        std::map<std::string, std::pair<aron::data::VariantPtr, aron::type::VariantPtr>> ret;
        auto data = aron::data::Dict::DynamicCastAndCheck(o);
        auto type = aron::type::Object::DynamicCastAndCheck(t);

        if (data)
        {
            for (const auto& [key, e] : data->getElements())
            {
                if (type && type->hasMemberType(key))
                {
                    auto memberType = type->getMemberType(key);
                    ret.insert({key, {e, memberType}});
                }
                else
                {
                    ret.insert({key, {e, nullptr}});
                }
            }
        }
        return ret;
    }

    RecursiveConstTypedVariantVisitor::MapElements
    RecursiveConstTypedVariantVisitor::GetDictElements(DataInput& o, TypeInput& t)
    {
        std::map<std::string, std::pair<data::VariantPtr, type::VariantPtr>> ret;
        auto x = data::Dict::DynamicCastAndCheck(o);
        auto y = type::Dict::DynamicCastAndCheck(t);

        auto ac = y ? y->getAcceptedType() : nullptr;

        if (x)
        {
            for (const auto& [key, e] : x->getElements())
            {
                ret.insert({key, {e, ac}});
            }
        }
        return ret;
    }

    RecursiveConstTypedVariantVisitor::ListElements
    RecursiveConstTypedVariantVisitor::GetListElements(DataInput& o, TypeInput& t)
    {
        std::vector<std::pair<data::VariantPtr, type::VariantPtr>> ret;
        auto x = data::List::DynamicCastAndCheck(o);
        auto y = type::List::DynamicCastAndCheck(t);

        auto ac = y ? y->getAcceptedType() : nullptr;

        if (x)
        {
            for (const auto& e : x->getElements())
            {
                ret.push_back({e, ac});
            }
        }
        return ret;
    }

    RecursiveConstTypedVariantVisitor::PairElements
    RecursiveConstTypedVariantVisitor::GetPairElements(DataInput& o, TypeInput& t)
    {
        auto x = data::List::DynamicCastAndCheck(o);
        auto y = type::Pair::DynamicCastAndCheck(t);

        ARMARX_CHECK_NOT_NULL(y);

        if (x)
        {
            auto e0 = x->getElement(0);
            auto ac0 = y->getFirstAcceptedType();
            auto e1 = x->getElement(1);
            auto ac1 = y->getSecondAcceptedType();
            return {{e0, ac0}, {e1, ac1}};
        }
        return {};
    }

    RecursiveConstTypedVariantVisitor::TupleElements
    RecursiveConstTypedVariantVisitor::GetTupleElements(DataInput& o, TypeInput& t)
    {
        std::vector<std::pair<data::VariantPtr, type::VariantPtr>> ret;
        auto x = data::List::DynamicCastAndCheck(o);
        auto y = type::Tuple::DynamicCastAndCheck(t);

        ARMARX_CHECK_NOT_NULL(y);

        if (x)
        {
            unsigned int i = 0;
            for (const auto& e : x->getElements())
            {
                auto ac = y->getAcceptedType(i++);
                ret.push_back({e, ac});
            }
        }
        return ret;
    }

    RecursiveConstTypedVariantVisitor::MapElements
    RecursiveConstTypedVariantVisitor::getObjectElements(DataInput& o, TypeInput& t)
    {
        return GetObjectElements(o, t);
    }

    RecursiveConstTypedVariantVisitor::MapElements
    RecursiveConstTypedVariantVisitor::getDictElements(DataInput& o, TypeInput& t)
    {
        return GetDictElements(o, t);
    }

    RecursiveConstTypedVariantVisitor::ListElements
    RecursiveConstTypedVariantVisitor::getListElements(DataInput& o, TypeInput& t)
    {
        return GetListElements(o, t);
    }

    RecursiveConstTypedVariantVisitor::PairElements
    RecursiveConstTypedVariantVisitor::getPairElements(DataInput& o, TypeInput& t)
    {
        return GetPairElements(o, t);
    }

    RecursiveConstTypedVariantVisitor::TupleElements
    RecursiveConstTypedVariantVisitor::getTupleElements(DataInput& o, TypeInput& t)
    {
        return GetTupleElements(o, t);
    }

    void RecursiveConstTypedVariantVisitor::visitObjectOnEnter(DataInput& i, TypeInput& j)
    {
        auto d = data::Dict::DynamicCastAndCheck(i);
        auto t = type::Object::DynamicCastAndCheck(j);
        visitAronVariantOnEnter(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitDictOnEnter(DataInput& i, TypeInput& j)
    {
        auto d = data::Dict::DynamicCastAndCheck(i);
        auto t = type::Dict::DynamicCastAndCheck(j);
        visitAronVariantOnEnter(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitPairOnEnter(DataInput& i, TypeInput& j)
    {
        auto d = data::List::DynamicCastAndCheck(i);
        auto t = type::Pair::DynamicCastAndCheck(j);
        visitAronVariantOnEnter(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitTupleOnEnter(DataInput& i, TypeInput& j)
    {
        auto d = data::List::DynamicCastAndCheck(i);
        auto t = type::Tuple::DynamicCastAndCheck(j);
        visitAronVariantOnEnter(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitListOnEnter(DataInput& i, TypeInput& j)
    {
        auto d = data::List::DynamicCastAndCheck(i);
        auto t = type::List::DynamicCastAndCheck(j);
        visitAronVariantOnEnter(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitObjectOnExit(DataInput& i, TypeInput& j)
    {
        auto d = data::Dict::DynamicCastAndCheck(i);
        auto t = type::Object::DynamicCastAndCheck(j);
        visitAronVariantOnExit(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitDictOnExit(DataInput& i, TypeInput& j)
    {
        auto d = data::Dict::DynamicCastAndCheck(i);
        auto t = type::Dict::DynamicCastAndCheck(j);
        visitAronVariantOnExit(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitPairOnExit(DataInput& i, TypeInput& j)
    {
        auto d = data::List::DynamicCastAndCheck(i);
        auto t = type::Pair::DynamicCastAndCheck(j);
        visitAronVariantOnExit(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitTupleOnExit(DataInput& i, TypeInput& j)
    {
        auto d = data::List::DynamicCastAndCheck(i);
        auto t = type::Tuple::DynamicCastAndCheck(j);
        visitAronVariantOnExit(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitListOnExit(DataInput& i, TypeInput& j)
    {
        auto d = data::List::DynamicCastAndCheck(i);
        auto t = type::List::DynamicCastAndCheck(j);
        visitAronVariantOnExit(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitMatrix(DataInput& i, TypeInput& j)
    {
        auto d = data::NDArray::DynamicCastAndCheck(i);
        auto t = type::Matrix::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitNDArray(DataInput& i, TypeInput& j)
    {
        auto d = data::NDArray::DynamicCastAndCheck(i);
        auto t = type::NDArray::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitQuaternion(DataInput& i, TypeInput& j)
    {
        auto d = data::NDArray::DynamicCastAndCheck(i);
        auto t = type::Quaternion::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitImage(DataInput& i, TypeInput& j)
    {
        auto d = data::NDArray::DynamicCastAndCheck(i);
        auto t = type::Image::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitPointCloud(DataInput& i, TypeInput& j)
    {
        auto d = data::NDArray::DynamicCastAndCheck(i);
        auto t = type::PointCloud::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitIntEnum(DataInput& i, TypeInput& j)
    {
        auto d = data::Int::DynamicCastAndCheck(i);
        auto t = type::IntEnum::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitInt(DataInput& i, TypeInput& j)
    {
        auto d = data::Int::DynamicCastAndCheck(i);
        auto t = type::Int::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitLong(DataInput& i, TypeInput& j)
    {
        auto d = data::Long::DynamicCastAndCheck(i);
        auto t = type::Long::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitFloat(DataInput& i, TypeInput& j)
    {
        auto d = data::Float::DynamicCastAndCheck(i);
        auto t = type::Float::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitDouble(DataInput& i, TypeInput& j)
    {
        auto d = data::Double::DynamicCastAndCheck(i);
        auto t = type::Double::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitBool(DataInput& i, TypeInput& j)
    {
        auto d = data::Bool::DynamicCastAndCheck(i);
        auto t = type::Bool::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }

    void RecursiveConstTypedVariantVisitor::visitString(DataInput& i, TypeInput& j)
    {
        auto d = data::String::DynamicCastAndCheck(i);
        auto t = type::String::DynamicCastAndCheck(j);
        visitAronVariant(d, t);
    }


    // see above
    void RecursiveConstTypedVariantVisitor::visitAronVariantOnEnter(const data::DictPtr&, const type::ObjectPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariantOnEnter(const data::DictPtr&, const type::DictPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariantOnEnter(const data::ListPtr&, const type::ListPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariantOnEnter(const data::ListPtr&, const type::PairPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariantOnEnter(const data::ListPtr&, const type::TuplePtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariantOnExit(const data::DictPtr&, const type::ObjectPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariantOnExit(const data::DictPtr&, const type::DictPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariantOnExit(const data::ListPtr&, const type::ListPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariantOnExit(const data::ListPtr&, const type::PairPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariantOnExit(const data::ListPtr&, const type::TuplePtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariant(const data::NDArrayPtr&, const type::MatrixPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariant(const data::NDArrayPtr&, const type::NDArrayPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariant(const data::NDArrayPtr&, const type::QuaternionPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariant(const data::NDArrayPtr&, const type::PointCloudPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariant(const data::NDArrayPtr&, const type::ImagePtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariant(const data::IntPtr&, const type::IntEnumPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariant(const data::IntPtr&, const type::IntPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariant(const data::LongPtr&, const type::LongPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariant(const data::FloatPtr&, const type::FloatPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariant(const data::DoublePtr&, const type::DoublePtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariant(const data::BoolPtr&, const type::BoolPtr&) {}
    void RecursiveConstTypedVariantVisitor::visitAronVariant(const data::StringPtr&, const type::StringPtr&) {}
}
