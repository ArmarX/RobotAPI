/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <string>
#include <vector>

#include "../RecursiveVisitor.h"
#include "../../variant/forward_declarations.h"
#include "../../../type/variant/forward_declarations.h"

namespace armarx::aron::data
{
    /**
     * @see type/visitor/variant/VariantVisitor.h
     */
    struct ConstVariantVisitor : virtual public Visitor<const data::VariantPtr>
    {
        static data::Descriptor GetDescriptor(Input& n);
        data::Descriptor getDescriptor(Input& n) override;
        virtual ~ConstVariantVisitor() = default;

        // Already implemented so that they cast the input and call specilized method below
        // Override if you do not want to use the specialized methods
        void visitDict(Input& i) override;
        void visitList(Input& i) override;
        void visitNDArray(Input& i) override;
        void visitInt(Input& i) override;
        void visitLong(Input& i) override;
        void visitFloat(Input& i) override;
        void visitDouble(Input& i) override;
        void visitBool(Input& i) override;
        void visitString(Input& i) override;

        // Use these if you do not want to cast manually
        virtual void visitAronVariant(const data::DictPtr&);
        virtual void visitAronVariant(const data::ListPtr&);
        virtual void visitAronVariant(const data::NDArrayPtr&);
        virtual void visitAronVariant(const data::IntPtr&);
        virtual void visitAronVariant(const data::LongPtr&);
        virtual void visitAronVariant(const data::FloatPtr&);
        virtual void visitAronVariant(const data::DoublePtr&);
        virtual void visitAronVariant(const data::BoolPtr&);
        virtual void visitAronVariant(const data::StringPtr&);
    };

    /**
     * @see type/visitor/variant/VariantVisitor.h
     */
    struct ConstTypedVariantVisitor : virtual public TypedVisitor<const data::VariantPtr, const type::VariantPtr>
    {
        static type::Descriptor GetDescriptor(DataInput& i, TypeInput& j);
        type::Descriptor getDescriptor(DataInput& i, TypeInput& j) override;
        virtual ~ConstTypedVariantVisitor() = default;

        // see above
        void visitObject(DataInput& i, TypeInput& j) override;
        void visitDict(DataInput&, TypeInput&) override;
        void visitPair(DataInput&, TypeInput&) override;
        void visitTuple(DataInput&, TypeInput&) override;
        void visitList(DataInput&, TypeInput&) override;
        void visitMatrix(DataInput&, TypeInput&) override;
        void visitNDArray(DataInput&, TypeInput&) override;
        void visitQuaternion(DataInput&, TypeInput&) override;
        void visitImage(DataInput&, TypeInput&) override;
        void visitPointCloud(DataInput&, TypeInput&) override;
        void visitIntEnum(DataInput&, TypeInput&) override;
        void visitInt(DataInput&, TypeInput&) override;
        void visitLong(DataInput&, TypeInput&) override;
        void visitFloat(DataInput&, TypeInput&) override;
        void visitDouble(DataInput&, TypeInput&) override;
        void visitBool(DataInput&, TypeInput&) override;
        void visitString(DataInput&, TypeInput&) override;

        // see above
        virtual void visitAronVariant(const data::DictPtr&, const type::ObjectPtr&);
        virtual void visitAronVariant(const data::DictPtr&, const type::DictPtr&);
        virtual void visitAronVariant(const data::ListPtr&, const type::ListPtr&);
        virtual void visitAronVariant(const data::ListPtr&, const type::PairPtr&);
        virtual void visitAronVariant(const data::ListPtr&, const type::TuplePtr&);
        virtual void visitAronVariant(const data::NDArrayPtr&, const type::MatrixPtr&);
        virtual void visitAronVariant(const data::NDArrayPtr&, const type::NDArrayPtr&);
        virtual void visitAronVariant(const data::NDArrayPtr&, const type::QuaternionPtr&);
        virtual void visitAronVariant(const data::NDArrayPtr&, const type::PointCloudPtr&);
        virtual void visitAronVariant(const data::NDArrayPtr&, const type::ImagePtr&);
        virtual void visitAronVariant(const data::IntPtr&, const type::IntEnumPtr&);
        virtual void visitAronVariant(const data::IntPtr&, const type::IntPtr&);
        virtual void visitAronVariant(const data::LongPtr&, const type::LongPtr&);
        virtual void visitAronVariant(const data::FloatPtr&, const type::FloatPtr&);
        virtual void visitAronVariant(const data::DoublePtr&, const type::DoublePtr&);
        virtual void visitAronVariant(const data::BoolPtr&, const type::BoolPtr&);
        virtual void visitAronVariant(const data::StringPtr&, const type::StringPtr&);
    };

    /**
     * @see type/visitor/variant/VariantVisitor.h
     */
    struct RecursiveConstVariantVisitor : virtual public RecursiveVisitor<const data::VariantPtr>
    {
        data::Descriptor getDescriptor(Input& n) override;
        static MapElements GetDictElements(Input& n);
        MapElements getDictElements(Input& n) override;
        static ListElements GetListElements(Input& n);
        ListElements getListElements(Input& n) override;
        virtual ~RecursiveConstVariantVisitor() = default;

        void visitDictOnEnter(Input& i) override;
        void visitDictOnExit(Input& i) override;
        void visitListOnEnter(Input& i) override;
        void visitListOnExit(Input& i) override;
        void visitNDArray(Input& i) override;
        void visitInt(Input& i) override;
        void visitLong(Input& i) override;
        void visitFloat(Input& i) override;
        void visitDouble(Input& i) override;
        void visitBool(Input& i) override;
        void visitString(Input& i) override;

        virtual void visitAronVariantOnEnter(const data::DictPtr&);
        virtual void visitAronVariantOnExit(const data::DictPtr&);
        virtual void visitAronVariantOnEnter(const data::ListPtr&);
        virtual void visitAronVariantOnExit(const data::ListPtr&);
        virtual void visitAronVariant(const data::NDArrayPtr&);
        virtual void visitAronVariant(const data::IntPtr&);
        virtual void visitAronVariant(const data::LongPtr&);
        virtual void visitAronVariant(const data::FloatPtr&);
        virtual void visitAronVariant(const data::DoublePtr&);
        virtual void visitAronVariant(const data::BoolPtr&);
        virtual void visitAronVariant(const data::StringPtr&);
    };

    /**
     * @see type/visitor/variant/VariantVisitor.h
     */
    struct RecursiveVariantVisitor : virtual public RecursiveVisitor<data::VariantPtr>
    {
        data::Descriptor getDescriptor(Input& n) override;
        std::map<std::string, InputNonConst> getDictElements(Input& n) override;
        std::vector<InputNonConst> getListElements(Input& n) override;
        virtual ~RecursiveVariantVisitor() = default;

        void visitDictOnEnter(Input& i) override;
        void visitDictOnExit(Input& i) override;
        void visitListOnEnter(Input& i) override;
        void visitListOnExit(Input& i) override;
        void visitNDArray(Input& i) override;
        void visitInt(Input& i) override;
        void visitLong(Input& i) override;
        void visitFloat(Input& i) override;
        void visitDouble(Input& i) override;
        void visitBool(Input& i) override;
        void visitString(Input& i) override;

        virtual void visitAronVariantOnEnter(data::DictPtr&);
        virtual void visitAronVariantOnExit(data::DictPtr&);
        virtual void visitAronVariantOnEnter(data::ListPtr&);
        virtual void visitAronVariantOnExit(data::ListPtr&);
        virtual void visitAronVariant(data::NDArrayPtr&);
        virtual void visitAronVariant(data::IntPtr&);
        virtual void visitAronVariant(data::LongPtr&);
        virtual void visitAronVariant(data::FloatPtr&);
        virtual void visitAronVariant(data::DoublePtr&);
        virtual void visitAronVariant(data::BoolPtr&);
        virtual void visitAronVariant(data::StringPtr&);
    };

    /**
     * @see type/visitor/variant/VariantVisitor.h
     */
    struct RecursiveConstTypedVariantVisitor : virtual public RecursiveTypedVisitor<const data::VariantPtr, const type::VariantPtr>
    {
        type::Descriptor getDescriptor(DataInput& o, TypeInput& n) override;
        static MapElements GetObjectElements(DataInput& o, TypeInput& t);
        /* This override exists for visitors that need to handle untyped members in the hierarchy.
         * Using it instead of `GetObjectElements` will allow your visitor to visit objects with
         * a nullptr as their type. However, you will have to handle nullptr types in your
         * visitor's methods.
         */
        static MapElements GetObjectElementsWithNullType(DataInput& o, TypeInput& t);
        static MapElements GetDictElements(DataInput& o, TypeInput& t);
        static ListElements GetListElements(DataInput& o, TypeInput& t);
        static PairElements GetPairElements(DataInput& o, TypeInput& t);
        static TupleElements GetTupleElements(DataInput& o, TypeInput& t);

        MapElements getObjectElements(DataInput& o, TypeInput& t) override;
        MapElements getDictElements(DataInput& o, TypeInput& t) override;
        ListElements getListElements(DataInput& o, TypeInput& t) override;
        PairElements getPairElements(DataInput& o, TypeInput& t) override;
        TupleElements getTupleElements(DataInput& o, TypeInput& t) override;
        virtual ~RecursiveConstTypedVariantVisitor() = default;

        // see above
        void visitObjectOnEnter(DataInput&, TypeInput&) override;
        void visitDictOnEnter(DataInput&, TypeInput&) override;
        void visitPairOnEnter(DataInput&, TypeInput&) override;
        void visitTupleOnEnter(DataInput&, TypeInput&) override;
        void visitListOnEnter(DataInput&, TypeInput&) override;
        void visitObjectOnExit(DataInput& i, TypeInput& j) override;
        void visitDictOnExit(DataInput&, TypeInput&) override;
        void visitPairOnExit(DataInput&, TypeInput&) override;
        void visitTupleOnExit(DataInput&, TypeInput&) override;
        void visitListOnExit(DataInput&, TypeInput&) override;
        void visitMatrix(DataInput&, TypeInput&) override;
        void visitNDArray(DataInput&, TypeInput&) override;
        void visitQuaternion(DataInput&, TypeInput&) override;
        void visitImage(DataInput&, TypeInput&) override;
        void visitPointCloud(DataInput&, TypeInput&) override;
        void visitIntEnum(DataInput&, TypeInput&) override;
        void visitInt(DataInput&, TypeInput&) override;
        void visitLong(DataInput&, TypeInput&) override;
        void visitFloat(DataInput&, TypeInput&) override;
        void visitDouble(DataInput&, TypeInput&) override;
        void visitBool(DataInput&, TypeInput&) override;
        void visitString(DataInput&, TypeInput&) override;

        // see above
        virtual void visitAronVariantOnEnter(const data::DictPtr&, const type::ObjectPtr&);
        virtual void visitAronVariantOnEnter(const data::DictPtr&, const type::DictPtr&);
        virtual void visitAronVariantOnEnter(const data::ListPtr&, const type::ListPtr&);
        virtual void visitAronVariantOnEnter(const data::ListPtr&, const type::PairPtr&);
        virtual void visitAronVariantOnEnter(const data::ListPtr&, const type::TuplePtr&);
        virtual void visitAronVariantOnExit(const data::DictPtr&, const type::ObjectPtr&);
        virtual void visitAronVariantOnExit(const data::DictPtr&, const type::DictPtr&);
        virtual void visitAronVariantOnExit(const data::ListPtr&, const type::ListPtr&);
        virtual void visitAronVariantOnExit(const data::ListPtr&, const type::PairPtr&);
        virtual void visitAronVariantOnExit(const data::ListPtr&, const type::TuplePtr&);
        virtual void visitAronVariant(const data::NDArrayPtr&, const type::MatrixPtr&);
        virtual void visitAronVariant(const data::NDArrayPtr&, const type::NDArrayPtr&);
        virtual void visitAronVariant(const data::NDArrayPtr&, const type::QuaternionPtr&);
        virtual void visitAronVariant(const data::NDArrayPtr&, const type::PointCloudPtr&);
        virtual void visitAronVariant(const data::NDArrayPtr&, const type::ImagePtr&);
        virtual void visitAronVariant(const data::IntPtr&, const type::IntEnumPtr&);
        virtual void visitAronVariant(const data::IntPtr&, const type::IntPtr&);
        virtual void visitAronVariant(const data::LongPtr&, const type::LongPtr&);
        virtual void visitAronVariant(const data::FloatPtr&, const type::FloatPtr&);
        virtual void visitAronVariant(const data::DoublePtr&, const type::DoublePtr&);
        virtual void visitAronVariant(const data::BoolPtr&, const type::BoolPtr&);
        virtual void visitAronVariant(const data::StringPtr&, const type::StringPtr&);
    };
}
