/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <type_traits>

#include "../../Descriptor.h"
#include "Visitor.h"

namespace armarx::aron::data
{
    /**
     * @see type/visitor/RecursiveVisitor.h
     */
    template <class T>
    struct RecursiveVisitor : virtual public VisitorBase<T>
    {
        using Input = typename VisitorBase<T>::Input;
        using InputNonConst = typename std::remove_const<Input>::type;

        using MapElements = std::map<std::string, InputNonConst>;
        using ListElements = std::vector<InputNonConst>;

        virtual MapElements getDictElements(Input&) = 0;
        virtual ListElements getListElements(Input&) = 0;

        virtual void visitDictOnEnter(Input& element) {};
        virtual void visitDictOnExit(Input& element) {};
        virtual void visitListOnEnter(Input& element) {};
        virtual void visitListOnExit(Input& element) {};

        virtual void visitNDArray(Input& element) {};
        virtual void visitInt(Input& element) {};
        virtual void visitLong(Input& element) {};
        virtual void visitFloat(Input& element) {};
        virtual void visitDouble(Input& element) {};
        virtual void visitBool(Input& element) {};
        virtual void visitString(Input& element) {};
        virtual void visitUnknown(Input& element) { throw error::AronException(__PRETTY_FUNCTION__, "Unknown type in visitor."); }
        virtual ~RecursiveVisitor() = default;
    };

    /**
     * @see type/visitor/RecursiveVisitor.h
     */
    template <class T1, class T2>
    struct RecursiveTypedVisitor : virtual public TypedVisitorBase<T1, T2>
    {
        using DataInput = typename TypedVisitorBase<T1, T2>::DataInput;
        using TypeInput = typename TypedVisitorBase<T1, T2>::TypeInput;

        using TypeInputNonConst = typename std::remove_const<TypeInput>::type;
        using DataInputNonConst = typename std::remove_const<DataInput>::type;

        using MapElements = std::map<std::string, std::pair<DataInputNonConst, TypeInputNonConst>>;
        using ListElements = std::vector<std::pair<DataInputNonConst, TypeInputNonConst>>;
        using PairElements = std::pair<std::pair<DataInputNonConst, TypeInputNonConst>, std::pair<DataInputNonConst, TypeInputNonConst>>;
        using TupleElements = std::vector<std::pair<DataInputNonConst, TypeInputNonConst>>;

        virtual MapElements getObjectElements(DataInput&, TypeInput&) = 0;
        virtual MapElements getDictElements(DataInput&, TypeInput&) = 0;
        virtual ListElements getListElements(DataInput&, TypeInput&) = 0;
        virtual PairElements getPairElements(DataInput&, TypeInput&) = 0;
        virtual TupleElements getTupleElements(DataInput&, TypeInput&) = 0;

        virtual void visitObjectOnEnter(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitObjectOnExit(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitDictOnEnter(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitDictOnExit(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitPairOnEnter(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitPairOnExit(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitTupleOnEnter(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitTupleOnExit(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitListOnEnter(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitListOnExit(DataInput& elementData, TypeInput& elementType) {};

        virtual void visitMatrix(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitNDArray(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitQuaternion(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitImage(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitPointCloud(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitIntEnum(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitInt(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitLong(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitFloat(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitDouble(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitBool(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitString(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitAnyObject(DataInput& elementData, TypeInput& elementType) {};
        virtual void visitUnknown(DataInput& elementData, TypeInput& elementType) {
            throw error::AronException(__PRETTY_FUNCTION__, "Unknown type in visitor.");
        }
        virtual ~RecursiveTypedVisitor() = default;
    };

    template <class T, class Data>
    concept isRecursiveVisitor = std::is_base_of<RecursiveVisitor<Data>, T>::value;

    template <class T, class Data, class Type>
    concept isRecursiveTypedVisitor = std::is_base_of<RecursiveTypedVisitor<Data, Type>, T>::value;

    /**
     * @see type/visitor/RecursiveVisitor.h
     */
    template <class RecursiveVisitorImplementation>
    requires isRecursiveVisitor<RecursiveVisitorImplementation, typename RecursiveVisitorImplementation::Input>
    void visitRecursive(RecursiveVisitorImplementation& v, typename RecursiveVisitorImplementation::Input& o)
    {
        data::Descriptor descriptor = v.getDescriptor(o);
        switch (descriptor)
        {
            case data::Descriptor::LIST:
            {
                v.visitListOnEnter(o);
                unsigned int i = 0;
                for (auto& value : v.getListElements(o))
                {
                    visitRecursive(v, value);
                    i++;
                }
                v.visitListOnExit(o);
                return;
            }
            case data::Descriptor::DICT:
            {
                v.visitDictOnEnter(o);
                for (auto& [key, value] : v.getDictElements(o))
                {
                    visitRecursive(v, value);
                }
                v.visitDictOnExit(o);
                return;
            }
            case data::Descriptor::NDARRAY:
                return v.visitNDArray(o);
            case data::Descriptor::INT:
                return v.visitInt(o);
            case data::Descriptor::LONG:
                return v.visitLong(o);
            case data::Descriptor::FLOAT:
                return v.visitFloat(o);
            case data::Descriptor::DOUBLE:
                return v.visitDouble(o);
            case data::Descriptor::STRING:
                return v.visitString(o);
            case data::Descriptor::BOOL:
                return v.visitBool(o);
            case data::Descriptor::UNKNOWN:
                return v.visitUnknown(o);
        }
    }

    /**
     * @see type/visitor/RecursiveVisitor.h
     * @see data/visitor/Visitor.h
     */
    template <class RecursiveVisitorImplementation>
    requires isRecursiveTypedVisitor<RecursiveVisitorImplementation, typename RecursiveVisitorImplementation::DataInput, typename RecursiveVisitorImplementation::TypeInput>
    void visitRecursive(RecursiveVisitorImplementation& v, typename RecursiveVisitorImplementation::DataInput& o, typename RecursiveVisitorImplementation::TypeInput& t)
    {
        type::Descriptor descriptor = v.getDescriptor(o, t);
        switch (descriptor)
        {
            case type::Descriptor::LIST:
            {
                v.visitListOnEnter(o, t);
                unsigned int i = 0;
                for (auto& [value, acceptedType] : v.getListElements(o, t))
                {
                    visitRecursive(v, value, acceptedType);
                    i++;
                }
                v.visitListOnExit(o, t);
                return;
            }
            case type::Descriptor::PAIR:
            {
                v.visitPairOnEnter(o, t);
                auto pair = v.getPairElements(o, t);
                auto first = pair.first;
                auto second = pair.second;
                visitRecursive(v, first.first, first.second);
                visitRecursive(v, second.first, second.second);
                v.visitPairOnExit(o, t);
                return;
            }
            case type::Descriptor::TUPLE:
            {
                v.visitTupleOnEnter(o, t);
                unsigned int i = 0;
                for (auto& [value, acceptedType] : v.getTupleElements(o, t))
                {
                    visitRecursive(v, value, acceptedType);
                    i++;
                }
                v.visitTupleOnExit(o, t);
                return;
            }
            case type::Descriptor::DICT:
            {
                    v.visitDictOnEnter(o, t);
                    for (auto& [key, pair] : v.getDictElements(o, t))
                    {
                        visitRecursive(v, pair.first, pair.second);
                    }
                    v.visitDictOnExit(o, t);
                    return;
            }
            case type::Descriptor::OBJECT:
            {
                v.visitObjectOnEnter(o, t);
                for (auto& [key, pair] : v.getObjectElements(o, t))
                {
                    visitRecursive(v, pair.first, pair.second);
                }
                v.visitObjectOnExit(o, t);
                return;
            }
            case type::Descriptor::NDARRAY:
                return v.visitNDArray(o, t);
            case type::Descriptor::MATRIX:
                return v.visitMatrix(o, t);
            case type::Descriptor::IMAGE:
                return v.visitImage(o, t);
            case type::Descriptor::POINTCLOUD:
                return v.visitPointCloud(o, t);
            case type::Descriptor::QUATERNION:
                return v.visitQuaternion(o, t);
            case type::Descriptor::INT:
                return v.visitInt(o, t);
            case type::Descriptor::LONG:
                return v.visitLong(o, t);
            case type::Descriptor::FLOAT:
                return v.visitFloat(o, t);
            case type::Descriptor::DOUBLE:
                return v.visitDouble(o, t);
            case type::Descriptor::STRING:
                return v.visitString(o, t);
            case type::Descriptor::BOOL:
                return v.visitBool(o, t);
            case type::Descriptor::ANY_OBJECT:
                return v.visitAnyObject(o, t);
            case type::Descriptor::INT_ENUM:
                return v.visitIntEnum(o, t);
            case type::Descriptor::UNKNOWN:
                return v.visitUnknown(o, t);
        }
    }
}
