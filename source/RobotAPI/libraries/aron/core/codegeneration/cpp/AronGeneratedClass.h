/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <string>
#include <vector>
#include <map>

// ArmarX
#include <RobotAPI/libraries/aron/core/Exception.h>

#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/data/rw/Reader.h>
#include <RobotAPI/libraries/aron/core/data/rw/Writer.h>
#include <RobotAPI/libraries/aron/core/type/rw/Writer.h>

// ARON
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/core/aron_conversions.h>
#include <RobotAPI/libraries/aron/core/rw.h>


namespace armarx::aron::cpp
{
    class AronGeneratedClass
    {
    public:
        AronGeneratedClass() = default;
        virtual ~AronGeneratedClass() = default;

        /// Reset all member values of this class to default (as stated in the XML). This may mean that maybe types are null or false and images may be created as headers_only
        virtual void resetHard() {};

        /// Reset all member values of this class softly, meaning if a maybe type has a value, we reset only the value (not the full maybe type) and if an image has data (width, height) we keep the data and width and height and only reset teh pixel values
        virtual void resetSoft() {};
    };

    template <class T>
    concept isAronGeneratedClass = std::is_base_of<AronGeneratedClass, T>::value;
}

namespace armarx::aron::codegenerator::cpp
{
    using AronGeneratedClass = aron::cpp::AronGeneratedClass;
}
