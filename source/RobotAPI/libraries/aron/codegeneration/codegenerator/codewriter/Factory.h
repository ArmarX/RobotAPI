/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once
/*
 *TODO Maybe it makes sense to have a unified factory interface for code generation for different languages
 *
// STD/STL
#include <memory>
#include <string>
#include <unordered_map>

// Base Class
#include <RobotAPI/libraries/aron/core/Factory.h>

// ArmarX
#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/core/Concepts.h>
#include <RobotAPI/libraries/aron/core/Path.h>

namespace armarx::aron::codegenerator
{
    template <typename Input, typename Output>
    class SerializerFactory
    {
    public:
        SerializerFactory() = default;

        virtual Output create(const Input&, const Path&) const = 0;
    };
}*/
