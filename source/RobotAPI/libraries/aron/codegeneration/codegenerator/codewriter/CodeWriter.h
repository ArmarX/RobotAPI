/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <map>
#include <vector>
#include <typeinfo>
#include <typeindex>

// ArmarX
#include <ArmarXCore/libraries/cppgen/MetaClass.h>

#include <RobotAPI/libraries/aron/core/type/variant/All.h>

#include <RobotAPI/libraries/aron/codegeneration/codegenerator/helper/WriterInfo.h>
#include <RobotAPI/libraries/aron/codegeneration/codegenerator/helper/ReaderInfo.h>
#include <RobotAPI/libraries/aron/codegeneration/typereader/helper/GenerateTypeInfo.h>
#include <RobotAPI/libraries/aron/codegeneration/typereader/helper/GenerateIntEnumInfo.h>

namespace armarx::aron::codegenerator
{
    class CodeWriter
    {
    public:
        CodeWriter() = delete;
        CodeWriter(const std::string& producerName, const std::vector<std::string>& additionalIncludesFromXMLFile):
            producerName(producerName),
            additionalIncludes(additionalIncludesFromXMLFile)
        {}

        virtual ~CodeWriter() = default;

        virtual void generateTypeObjects(const std::vector<typereader::GenerateObjectInfo>&) = 0;
        virtual void generateTypeIntEnums(const std::vector<typereader::GenerateIntEnumInfo>&) = 0;

        std::vector<MetaClassPtr> getTypeClasses() const
        {
            return typeClasses;
        }

    protected:
        virtual void addSpecificReaderMethods() = 0;
        virtual void addSpecificWriterMethods() = 0;

    protected:
        std::vector<MetaClassPtr> typeClasses;

        std::string producerName;
        std::vector<codegenerator::WriterInfo> dataWriters;
        std::vector<codegenerator::ReaderInfo> dataReaders;
        std::vector<codegenerator::StaticReaderInfo> staticDataReaders;
        std::vector<codegenerator::WriterInfo> initialTypeWriters;
        std::vector<std::string> additionalIncludes;
    };
}
