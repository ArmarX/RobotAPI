/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "IntEnumClass.h"

#include <SimoxUtility/meta/type_name.h>


namespace armarx::aron::codegenerator::cpp::generator
{
    // constructors
    IntEnumClass::IntEnumClass(const type::IntEnum& n) :
        detail::SpecializedGeneratorBase<type::IntEnum, IntEnumClass>(
            n.getEnumName(),
            n.getEnumName(),
            simox::meta::get_type_name<data::dto::NDArray>(),
            simox::meta::get_type_name<type::dto::IntEnum>(),
            n)
    {
        if (type.getMaybe() != type::Maybe::NONE)
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Somehow the maybe flag of a top level int enum declaration is set. This is not valid!", std::to_string((int) type.getMaybe()), type.getPath());
        }
    }

    std::vector<CppFieldPtr> IntEnumClass::getPublicVariableDeclarations(const std::string&) const
    {
        std::vector<CppFieldPtr> fields;
        std::stringstream enum_to_name;
        std::stringstream name_to_enum;
        std::stringstream enum_to_value;
        std::stringstream value_to_enum;

        enum_to_name << "{" << std::endl;
        name_to_enum << "{" << std::endl;
        enum_to_value << "{" << std::endl;
        value_to_enum << "{" << std::endl;
        for (const auto& [key, value] : type.getAcceptedValueMap())
        {
            std::string enumKeyWithNamespace = std::string(IMPL_ENUM) + "::" + key;
            fields.push_back(std::make_shared<CppField>("static constexpr " + std::string(IMPL_ENUM), key + " = " + enumKeyWithNamespace));

            enum_to_name << "\t\t{" << enumKeyWithNamespace << ", \"" << key << "\"}," << std::endl;
            name_to_enum << "\t\t{\"" << key << "\", " << enumKeyWithNamespace << "}," << std::endl;

            enum_to_value << "\t\t{" << enumKeyWithNamespace << ", " << value << "}," << std::endl;
            value_to_enum << "\t\t{" << value << ", " << enumKeyWithNamespace << "}," << std::endl;
        }

        enum_to_name << "\t}";
        name_to_enum << "\t}";
        enum_to_value << "\t}";
        value_to_enum << "\t}";

        fields.push_back(std::make_shared<CppField>("static inline const std::map<" + std::string(IMPL_ENUM) + ", std::string>", "EnumToStringMap", enum_to_name.str(), "Mapping enum values to readable strings"));
        fields.push_back(std::make_shared<CppField>("static inline const std::map<std::string, " + std::string(IMPL_ENUM) + ">", "StringToEnumMap", name_to_enum.str(), "Mapping readable strings to enum values"));
        fields.push_back(std::make_shared<CppField>("static inline const std::map<" + std::string(IMPL_ENUM) + ", int>", "EnumToValueMap", enum_to_value.str(), "Mapping enum values to a int value"));
        fields.push_back(std::make_shared<CppField>("static inline const std::map<int, " + std::string(IMPL_ENUM) + ">", "ValueToEnumMap", value_to_enum.str(), "Mapping int values to a enum"));

        fields.push_back(std::make_shared<CppField>(std::string(IMPL_ENUM), "value", "", "The current value of the enum object"));

        return fields;
    }

    CppBlockPtr IntEnumClass::getResetSoftBlock(const std::string& accessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine("value = {};");
        return block_if_data;
    }

    CppBlockPtr IntEnumClass::getResetHardBlock(const std::string& accessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine("value = {};");
        return block_if_data;
    }

    CppBlockPtr IntEnumClass::getWriteTypeBlock(const std::string&, const std::string&, const Path& p, std::string&) const
    {
        static const std::string INT_ENUM_VALUE_MAP = ARON_VARIABLE_PREFIX + "_str2ValueMap";

        CppBlockPtr b = std::make_shared<CppBlock>();

        std::vector<std::string> map_initializer;
        for (const auto& [key, value] : type.getAcceptedValueMap())
        {
            map_initializer.push_back("{\"" + key + "\", " + std::to_string(value) + "}");
        }
        b->addLine("std::map<std::string, int> " + INT_ENUM_VALUE_MAP + " = {" + simox::alg::to_string(map_initializer, ", ") + "};");
        b->addLine("return " + ARON_WRITER_ACCESSOR + ".writeIntEnum(\"" + type.getEnumName() + "\", " +
                   INT_ENUM_VALUE_MAP + ", " +
                   ARON_MAYBE_TYPE_ACCESSOR + ", " +
                   ARON_PATH_ACCESSOR + ");");
        return b;

        return b;
    }

    CppBlockPtr IntEnumClass::getWriteBlock(const std::string&, const Path& p, std::string&) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();

        block_if_data->addLine("return " + ARON_WRITER_ACCESSOR + ".writePrimitive(EnumToValueMap.at(value), " +
                               ARON_PATH_ACCESSOR + "); // of top level enum " + getInstantiatedCppTypename());
        return block_if_data;
    }

    CppBlockPtr IntEnumClass::getReadBlock(const std::string&, const std::string& variantAccessor) const
    {
        static const std::string INT_ENUM_TMP_VALUE = ARON_VARIABLE_PREFIX + "_tmpValue";

        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine("int " + INT_ENUM_TMP_VALUE + ";");
        block_if_data->addLine("" + ARON_READER_ACCESSOR + ".readPrimitive("+variantAccessor+", "+INT_ENUM_TMP_VALUE+"); // of top level enum " + getInstantiatedCppTypename());
        block_if_data->addLine("value = ValueToEnumMap.at("+INT_ENUM_TMP_VALUE+");");
        return block_if_data;
    }

    CppBlockPtr IntEnumClass::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine("if (not (value == " + otherInstanceAccessor + ".value))");
        block_if_data->addLineAsBlock("return false;");
        return block_if_data;
    }


    CppCtorPtr IntEnumClass::toCopyCtor(const std::string& name) const
    {
        CppCtorPtr c = std::make_shared<CppCtor>(name + "(const " + getFullInstantiatedCppTypename() + "& i)");
        std::vector<std::pair<std::string, std::string>> initList = {{"value", "i.value"}};
        c->addInitListEntries(initList);
        return c;
    }

    CppCtorPtr IntEnumClass::toInnerEnumCtor(const std::string& name) const
    {
        CppCtorPtr c = std::make_shared<CppCtor>(name + "(const " + std::string(IMPL_ENUM) + " e)");
        std::vector<std::pair<std::string, std::string>> initList = {{"value", "e"}};
        c->addInitListEntries(initList);
        return c;
    }

    CppEnumPtr IntEnumClass::toInnerEnumDefinition() const
    {
        CppEnumPtr e = std::make_shared<CppEnum>(std::string(IMPL_ENUM), "The internal enum definition of the enum of this autogenerated class.");
        for (const auto& [key, value] : type.getAcceptedValueMap())
        {
            e->addField(std::make_shared<CppEnumField>(key));
        }
        return e;
    }

    CppMethodPtr IntEnumClass::toIntMethod() const
    {
        std::stringstream doc;
        doc << "@brief int() - Converts the internally stored value to int representation \n";
        doc << "@return - the int representation";

        CppMethodPtr m = CppMethodPtr(new CppMethod("operator int() const", doc.str()));
        CppBlockPtr b = std::make_shared<CppBlock>();
        b->addLine("return EnumToValueMap.at(value);");
        m->setBlock(b);
        return m;
    }

    CppMethodPtr IntEnumClass::toCopyAssignmentMethod() const
    {
        std::stringstream doc;
        doc << "@brief operator=() -  Assignment operator for copy \n";
        doc << "@return - nothing";

        CppMethodPtr m = CppMethodPtr(new CppMethod(getFullInstantiatedCppTypename() + "& operator=(const " + getFullInstantiatedCppTypename() + "& c)", doc.str()));
        CppBlockPtr b = std::make_shared<CppBlock>();
        b->addLine("value = c.value;");
        b->addLine("return *this;");
        m->setBlock(b);
        return m;
    }

    CppMethodPtr IntEnumClass::toEnumAssignmentMethod() const
    {
        std::stringstream doc;
        doc << "@brief operator=() -  Assignment operator for the internally defined enum \n";
        doc << "@return - nothing";

        CppMethodPtr m = CppMethodPtr(new CppMethod(getFullInstantiatedCppTypename() + "& operator=(" + std::string(IMPL_ENUM) + " v)", doc.str()));
        CppBlockPtr b = std::make_shared<CppBlock>();
        b->addLine("value = v;");
        b->addLine("return *this;");
        m->setBlock(b);
        return m;
    }

    CppMethodPtr IntEnumClass::toIntAssignmentMethod() const
    {
        std::stringstream doc;
        doc << "@brief operator=() -  Assignment operator for the internally defined enum \n";
        doc << "@return - nothing";

        CppMethodPtr m = CppMethodPtr(new CppMethod(getFullInstantiatedCppTypename() + "& operator=(int v)", doc.str()));
        CppBlockPtr b = std::make_shared<CppBlock>();
        b->addLine("if (auto it = ValueToEnumMap.find(v); it == ValueToEnumMap.end())");
        CppBlockPtr b2 = std::make_shared<CppBlock>();
        b2->addLine("throw armarx::LocalException(\"The input int is not valid. Could net set the enum to value '\" + std::to_string(v) + \"'\");");
        b->addBlock(b2);
        b->addLine("else");
        CppBlockPtr b3 = std::make_shared<CppBlock>();
        b3->addLine("value = it->second;");
        b->addBlock(b3);
        b->addLine("return *this;");
        m->setBlock(b);
        return m;
    }

    CppMethodPtr IntEnumClass::toToStringMethod() const
    {
        std::stringstream doc;
        doc << "@brief toString() - Converts the internally stored value to string \n";
        doc << "@return - the name of the enum";

        CppMethodPtr m = CppMethodPtr(new CppMethod("std::string toString() const", doc.str()));
        CppBlockPtr b = std::make_shared<CppBlock>();
        b->addLine("return EnumToStringMap.at(value);");
        m->setBlock(b);
        return m;
    }

    CppMethodPtr IntEnumClass::toFromStringMethod() const
    {
        std::stringstream doc;
        doc << "@brief fromString() - sets the internally stored value to the corrsponding enum of the input str \n";
        doc << "@return - nothing";

        CppMethodPtr m = CppMethodPtr(new CppMethod("void fromString(const std::string& str)", doc.str()));
        CppBlockPtr b = std::make_shared<CppBlock>();
        b->addLine("if (auto it = StringToEnumMap.find(str); it == StringToEnumMap.end())");
        CppBlockPtr b2 = std::make_shared<CppBlock>();
        b2->addLine("throw armarx::LocalException(\"The input name is not valid. Could net set the enum to value '\" + str + \"'\");");
        b->addBlock(b2);
        b->addLine("else");
        CppBlockPtr b3 = std::make_shared<CppBlock>();
        b3->addLine("value = it->second;");
        b->addBlock(b3);

        m->setBlock(b);
        return m;
    }
}
