/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "List.h"

#include <SimoxUtility/meta/type_name.h>


namespace armarx::aron::codegenerator::cpp::generator
{
    // constructors
    List::List(const type::List& e) :
        detail::ContainerGenerator<type::List, List>(
            "std::vector<" + FromAronType(*e.getAcceptedType())->getFullInstantiatedCppTypename() + ">",
            "std::vector<" + FromAronType(*e.getAcceptedType())->getFullInstantiatedCppTypename() + ">",
            simox::meta::get_type_name<data::dto::List>(),
            simox::meta::get_type_name<type::dto::List>(), e)
    {
    }


    std::vector<std::string> List::getRequiredIncludes() const
    {
        auto c = FromAronType(*type.getAcceptedType());
        return c->getRequiredIncludes();
    }

    CppBlockPtr List::getResetSoftBlock(const std::string& cppAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine(cppAccessor + nextEl() + "clear();");
        return this->resolveMaybeResetSoftBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr List::getWriteTypeBlock(const std::string& typeAccessor, const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr b = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor;

        auto type_s = FromAronType(*type.getAcceptedType());
        std::string nextVariantAccessor;
        Path nextPath = p.withAcceptedType(true);
        CppBlockPtr b2 = type_s->getWriteTypeBlock(type_s->getInstantiatedCppTypename(), cppAccessor + nextEl() + "accepted_type", nextPath, nextVariantAccessor);
        b->appendBlock(b2);

        b->addLine("auto " + variantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeList(" + nextVariantAccessor + ", " +
                   conversion::Maybe2CppString.at(type.getMaybe()) + ", " +
                   "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + cppAccessor);
        return b;
    }

    CppBlockPtr List::getWriteBlock(const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR+ "_" + escaped_accessor;

        const std::string elementsAccessor = variantAccessor + "_listElements";
        block_if_data->addLine("std::vector<_Aron_T> " + elementsAccessor + ";");

        std::string accessor_iterator = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_it";

        auto type_s = FromAronType(*type.getAcceptedType());
        block_if_data->addLine("for(unsigned int " + accessor_iterator + " = 0; " + accessor_iterator + " < " + cppAccessor + nextEl() + "size(); ++" + accessor_iterator + ")");
        {
            std::string nextVariantAccessor;
            auto for_loop = std::make_shared<CppBlock>();
            Path nextPath = p.withElement("std::to_string(" + accessor_iterator + ")");
            auto child_b = type_s->getWriteBlock(cppAccessor + nextEl() + "at(" + accessor_iterator + ")", nextPath, nextVariantAccessor);
            for_loop->addLine("auto " + nextVariantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeNull();");
            for_loop->appendBlock(child_b);
            for_loop->addLine(elementsAccessor + ".push_back(" + nextVariantAccessor + ");");
            block_if_data->addBlock(for_loop);
        }

        Path path = this->type.getPath();
        block_if_data->addLine(variantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeList(" + elementsAccessor + ", " +
                               "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + cppAccessor);

        return resolveMaybeWriteBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr List::getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        std::string elements_accessor = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_listElements";
        std::string accessor_iterator_value = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_listValue";

        block_if_data->addLine("std::vector<_Aron_TNonConst> " + elements_accessor + ";");
        block_if_data->addLine("" + ARON_READER_ACCESSOR + ".readList(" + variantAccessor + ", " + elements_accessor + ");");
        block_if_data->addLine("for (const auto& " + accessor_iterator_value + " : " + elements_accessor + ")");
        {
            CppBlockPtr for_loop = std::make_shared<CppBlock>();

            auto type_s = FromAronType(*type.getAcceptedType());

            std::string accessor_iterator_tmp = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_listTmp";
            for_loop->addLine(type_s->getFullInstantiatedCppTypename() + " " + accessor_iterator_tmp + ";");
            for_loop->appendBlock(type_s->getReadBlock(accessor_iterator_tmp, accessor_iterator_value));
            for_loop->addLine(cppAccessor + nextEl() + "push_back(" + accessor_iterator_tmp + ");");
            block_if_data->addBlock(for_loop);
        }
        return resolveMaybeReadBlock(block_if_data, cppAccessor, variantAccessor);
    }
}
