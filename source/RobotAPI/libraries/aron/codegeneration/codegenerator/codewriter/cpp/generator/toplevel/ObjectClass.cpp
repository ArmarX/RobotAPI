/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "ObjectClass.h"

#include <SimoxUtility/meta/type_name.h>
#include <SimoxUtility/algorithm/vector.hpp>


namespace armarx::aron::codegenerator::cpp::generator
{
    // constructors
    ObjectClass::ObjectClass(const type::Object& e) :
        detail::SpecializedGeneratorBase<type::Object, ObjectClass>(
            e.getObjectNameWithTemplateInstantiations(), // should be similar to the object name
            e.getObjectNameWithTemplates(),
            simox::meta::get_type_name<data::dto::Dict>(),
            simox::meta::get_type_name<type::dto::AronObject>(), e)
    {
        if (type.getMaybe() != type::Maybe::NONE)
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Somehow the maybe flag of a top level object declaration is set. This is not valid!", std::to_string((int) type.getMaybe()) + " aka " + type::defaultconversion::string::Maybe2String.at(type.getMaybe()), type.getPath());
        }
    }

    std::vector<std::string> ObjectClass::getRequiredIncludes() const
    {
        std::vector<std::string> ret;
        for (const auto& [key, child] : type.getMemberTypes())
        {
            (void) key;
            auto child_s = FromAronType(*child);
            ret = simox::alg::appended(ret, child_s->getRequiredIncludes());
        }
        return ret;
    }

    std::vector<CppFieldPtr> ObjectClass::getPublicVariableDeclarations(const std::string&) const
    {
        std::vector<CppFieldPtr> fields;
        for (const auto& [key, member] : type.getMemberTypes())
        {
            auto member_s = FromAronType(*member);
            std::vector<CppFieldPtr> member_fields = member_s->getPublicVariableDeclarations(key);
            fields.insert(fields.end(), member_fields.begin(), member_fields.end());
        }
        return fields;
    }

    CppBlockPtr ObjectClass::getResetSoftBlock(const std::string& accessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        if (type.getExtends() != nullptr)
        {
            const auto extends_s = FromAronType(*type.getExtends());
            block_if_data->addLine(extends_s->getFullInstantiatedCppTypename() + "::resetSoft();");
        }

        for (const auto& [key, child] : type.getMemberTypes())
        {
            auto child_s = FromAronType(*child);
            CppBlockPtr b2 = child_s->getResetSoftBlock(key);
            block_if_data->appendBlock(b2);
        }
        return block_if_data;
    }

    CppBlockPtr ObjectClass::getResetHardBlock(const std::string& accessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        if (type.getExtends() != nullptr)
        {
            const auto extends_s = FromAronType(*type.getExtends());
            block_if_data->addLine(extends_s->getFullInstantiatedCppTypename() + "::resetHard();");
        }

        for (const auto& [key, child] : type.getMemberTypes())
        {
            const auto child_s = FromAronType(*child);
            CppBlockPtr b2 = child_s->getResetHardBlock(key);
            block_if_data->appendBlock(b2);
        }
        return block_if_data;
    }

    CppBlockPtr ObjectClass::getWriteTypeBlock(const std::string&, const std::string&, const Path& p, std::string&) const
    {
        static const std::string OBJECT_MEMBERS_ACCESSOR = ARON_VARIABLE_PREFIX + "_objectMembers";
        static const std::string OBJECT_EXTENDS_ACCESSOR = ARON_VARIABLE_PREFIX + "_objectExtends";

        CppBlockPtr b = std::make_shared<CppBlock>();
        b->addLine("std::map<std::string, _Aron_T> " + OBJECT_MEMBERS_ACCESSOR + ";");

        if (type.getExtends() != nullptr)
        {
            const auto extends_s = FromAronType(*type.getExtends());
            b->addLine("// get base class of " + this->getFullInstantiatedCppTypename());
            b->addLine("auto " + OBJECT_EXTENDS_ACCESSOR + " = " + extends_s->getFullInstantiatedCppTypename() + "::writeType(" +
                       ARON_WRITER_ACCESSOR + ", " +
                       "{" + simox::alg::join(type.getExtends()->getTemplateInstantiations(), ", ") + "}, " +
                       "::armarx::aron::type::Maybe::NONE);");
        }
        else
        {
            b->addLine("auto " + OBJECT_EXTENDS_ACCESSOR + " = std::nullopt;");
        }

        b->addLine("// members of " + this->getFullInstantiatedCppTypename());
        for (const auto& [key, child] : type.getMemberTypes())
        {
            const auto child_s = FromAronType(*child);
            std::string child_return_variant;
            Path nextPath = p.withElement(key, true);
            CppBlockPtr child_b = child_s->getWriteTypeBlock(child_s->getFullInstantiatedCppTypename(), key, nextPath, child_return_variant);
            b->appendBlock(child_b);
            b->addLine(OBJECT_MEMBERS_ACCESSOR + ".emplace(\"" + key + "\", " + child_return_variant + ");");
        }
        std::vector<std::string> templatesQuoted;
        std::vector<std::string> templateIntantiationsQuoted;
        for (const auto& t : type.getTemplates())
        {
            templatesQuoted.push_back("\"" + t + "\"");
        }
        for (const auto& t : type.getTemplateInstantiations())
        {
            templateIntantiationsQuoted.push_back("\"" + t + "\"");
        }

        b->addLine("return " + ARON_WRITER_ACCESSOR + ".writeObject(\"" + type.getObjectName() + "\", " +
                   "{" + simox::alg::join(templatesQuoted, ", ") + "}, " +
                   ARON_TEMPLATE_INSTANTIATIONS_ACCESSOR + ", " +
                   OBJECT_MEMBERS_ACCESSOR + ", " +
                   OBJECT_EXTENDS_ACCESSOR + ", " +
                   ARON_MAYBE_TYPE_ACCESSOR + ", " +
                   ARON_PATH_ACCESSOR + "); // of top level object " + getInstantiatedCppTypename());;

        return b;
    }

    CppBlockPtr ObjectClass::getWriteBlock(const std::string&, const Path& p, std::string&) const
    {
        static const std::string OBJECT_MEMBERS_ACCESSOR = ARON_VARIABLE_PREFIX + "_objectMembers";
        static const std::string OBJECT_EXTENDS_ACCESSOR = ARON_VARIABLE_PREFIX + "_objectExtends";

        CppBlockPtr block_if_data = std::make_shared<CppBlock>();

        block_if_data->addLine("std::map<std::string, _Aron_T> " + OBJECT_MEMBERS_ACCESSOR + ";");

        block_if_data->addLine("std::optional<_Aron_T> " + OBJECT_EXTENDS_ACCESSOR + ";");
        if (type.getExtends() != nullptr)
        {
            const auto extends_s = FromAronType(*type.getExtends());
            block_if_data->addLine("// write base class of " + this->getFullInstantiatedCppTypename());
            block_if_data->addLine(OBJECT_EXTENDS_ACCESSOR + " = " + extends_s->getFullInstantiatedCppTypename() + "::write(" + ARON_WRITER_ACCESSOR + ");");
        }

        block_if_data->addLine("// members of " + this->getFullInstantiatedCppTypename());
        for (const auto& [key, child] : type.getMemberTypes())
        {
            const auto child_s = FromAronType(*child);
            std::string child_return_variant;

            Path nextPath = p.withElement(key, true);
            CppBlockPtr child_b = child_s->getWriteBlock(key, nextPath, child_return_variant);
            block_if_data->addLine("auto " + child_return_variant + " = " + ARON_WRITER_ACCESSOR + ".writeNull();");
            block_if_data->appendBlock(child_b);
            block_if_data->addLine(OBJECT_MEMBERS_ACCESSOR + ".emplace(\"" + key + "\", " + child_return_variant + ");");
        }

        block_if_data->addLine("return " + ARON_WRITER_ACCESSOR + ".writeDict("+OBJECT_MEMBERS_ACCESSOR+", " +
                               OBJECT_EXTENDS_ACCESSOR + ", "+
                               ARON_PATH_ACCESSOR + "); // of top level object " + getInstantiatedCppTypename());
        return block_if_data;
    }

    CppBlockPtr ObjectClass::getReadBlock(const std::string&, const std::string& variantAccessor) const
    {
        static const std::string OBJECT_MEMBERS_ACCESSOR = ARON_VARIABLE_PREFIX + "_objectMembers";
        static const std::string OBJECT_EXTENDS_ACCESSOR = ARON_VARIABLE_PREFIX + "_objectExtends";

        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine("std::map<std::string, _Aron_TNonConst> " + OBJECT_MEMBERS_ACCESSOR + ";");

        if (type.getExtends() != nullptr)
        {
            const auto extends_s = FromAronType(*type.getExtends());
            block_if_data->addLine(extends_s->getFullInstantiatedCppTypename() + "::read(" + ARON_READER_ACCESSOR + ", "+variantAccessor+");");
        }

        block_if_data->addLine("" + ARON_READER_ACCESSOR + ".readDict("+variantAccessor+", "+OBJECT_MEMBERS_ACCESSOR+"); // of top level object " + getInstantiatedCppTypename());

        for (const auto& [key, child] : type.getMemberTypes())
        {
            const auto child_s = FromAronType(*child);
            const std::string child_variant_accessor = OBJECT_MEMBERS_ACCESSOR + ".at(\"" + key + "\")";
            block_if_data->appendBlock(child_s->getReadBlock(key, child_variant_accessor));
        }
        return block_if_data;
    }

    CppBlockPtr ObjectClass::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        if (type.getExtends() != nullptr)
        {
            const auto extends_s = FromAronType(*type.getExtends());
            block_if_data->addLine("if (not (" + extends_s->getFullInstantiatedCppTypename() + "::operator== (" + otherInstanceAccessor + ")))");
            block_if_data->addLineAsBlock("return false;");
        }
        for (const auto& [key, child] : type.getMemberTypes())
        {
            auto child_s = FromAronType(*child);
            CppBlockPtr b2 = child_s->getEqualsBlock(key, otherInstanceAccessor + "." + key);
            block_if_data->appendBlock(b2);
        }
        return block_if_data;
    }
}

