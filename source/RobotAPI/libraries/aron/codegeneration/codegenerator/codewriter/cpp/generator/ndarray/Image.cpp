/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann (rainer dot kartmann at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Image.h"

#include <SimoxUtility/meta/type_name.h>


namespace armarx::aron::codegenerator::cpp::generator
{
    const std::map<type::image::PixelType, std::tuple<std::string, int, std::string>> PixelType2Cpp =
    {
        { type::image::PixelType::RGB24,   {"CV_8UC3", 3, "::armarx::aron::type::image::PixelType::RGB24"}},
        { type::image::PixelType::DEPTH32, {"CV_32FC1", 4, "::armarx::aron::type::image::PixelType::DEPTH32"}},
    };


    // constructors
    Image::Image(const type::Image& n) :
        detail::NDArrayGenerator<type::Image, Image>(
            "cv::Mat",
            "cv::Mat",
            simox::meta::get_type_name<data::dto::NDArray>(),
            simox::meta::get_type_name<type::dto::Image>(), n)
    {
    }

    std::vector<std::string> Image::getRequiredIncludes() const
    {
        return {"<opencv2/core/core.hpp>"};
    }


    CppBlockPtr Image::getResetHardBlock(const std::string& cppAccessor) const
    {
        CppBlockPtr block_if_data = detail::NDArrayGenerator<type::Image, Image>::getResetHardBlock(cppAccessor);
        block_if_data->addLine(cppAccessor + ".create(0, 0, " + std::get<0>(PixelType2Cpp.at(type.getPixelType())) + ");");
        return this->resolveMaybeResetSoftBlock(block_if_data, cppAccessor);
    }


    CppBlockPtr Image::getResetSoftBlock(const std::string& cppAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine(cppAccessor + ".create(0, 0, " + std::get<0>(PixelType2Cpp.at(type.getPixelType())) + ");");
        return this->resolveMaybeResetSoftBlock(block_if_data, cppAccessor);
    }


    CppBlockPtr Image::getWriteTypeBlock(const std::string& typeAccessor, const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr b = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor;

        b->addLine("auto " + variantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeImage(" + std::get<2>(PixelType2Cpp.at(type.getPixelType())) + ", " +
                   conversion::Maybe2CppString.at(type.getMaybe()) + ", " +
                   "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + cppAccessor);
        return b;
    }


    CppBlockPtr Image::getWriteBlock(const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor;
        const std::string shape_vec = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_imageShape";
        block_if_data->addLine("std::vector<int> " + shape_vec + "(" + cppAccessor + nextEl() + "size.p, " + cppAccessor + nextEl() + "size.p + " + cppAccessor + nextEl() + "dims);");
        block_if_data->addLine(shape_vec+".push_back(" + cppAccessor + nextEl() + "elemSize());");

        block_if_data->addLine(variantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeNDArray(" + shape_vec + ", "+
                               "std::to_string(" + cppAccessor + nextEl() + "type()), "+
                               "reinterpret_cast<const unsigned char*>(" + cppAccessor + nextEl() + "data), "+
                               "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {" + simox::alg::join(p.getPath(), ", ") + "})); // of " + cppAccessor);
        return resolveMaybeWriteBlock(block_if_data, cppAccessor);
    }


    CppBlockPtr Image::getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        std::string type = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor + "_typeAsString";
        std::string dims = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor + "_shape";
        std::string data = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor + "_data";

        block_if_data->addLine("std::string " + type + ";");
        block_if_data->addLine("std::vector<int> " + dims + ";");
        block_if_data->addLine("std::vector<unsigned char> " + data + ";");
        block_if_data->addLine("" + ARON_READER_ACCESSOR + ".readNDArray("+variantAccessor+", "+dims+", "+type+", "+data+"); // of " + cppAccessor);
        block_if_data->addLine(cppAccessor + " = " + getInstantiatedCppTypename() + "(std::vector<int>({" + dims + ".begin(), std::prev(" + dims + ".end())}), std::stoi(" + type + "));");
        block_if_data->addLine("std::memcpy(reinterpret_cast<unsigned char*>(" + cppAccessor + nextEl() + "data), "+data+".data(), "+data+".size());");
        return resolveMaybeReadBlock(block_if_data, cppAccessor, variantAccessor);
    }


    CppBlockPtr Image::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string resolved_accessor = this->resolveMaybeAccessor(accessor);
        std::string other_instance_resolved_accessor = this->resolveMaybeAccessor(otherInstanceAccessor);

        block_if_data->addLine("if (cv::countNonZero(" + resolved_accessor + " != " + other_instance_resolved_accessor + ") != 0)");
        block_if_data->addLineAsBlock("return false;");
        return resolveMaybeEqualsBlock(block_if_data, accessor, otherInstanceAccessor);
    }
}


