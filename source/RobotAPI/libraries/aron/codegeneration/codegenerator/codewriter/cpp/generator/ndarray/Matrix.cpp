/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Matrix.h"

#include <SimoxUtility/meta/type_name.h>


namespace armarx::aron::codegenerator::cpp::generator
{
    const std::map<type::matrix::ElementType, std::tuple<std::string, int, std::string>> ElementType2Cpp =
    {
        {type::matrix::INT16, {"short", 2, "::armarx::aron::type::matrix::INT16"}},
        {type::matrix::INT32, {"int", 4, "::armarx::aron::type::matrix::INT32"}},
        {type::matrix::INT64, {"long", 8, "::armarx::aron::type::matrix::INT64"}},
        {type::matrix::FLOAT32, {"float", 4, "::armarx::aron::type::matrix::FLOAT32"}},
        {type::matrix::FLOAT64, {"double", 8, "::armarx::aron::type::matrix::FLOAT64"}}
    };

    // constructors
    Matrix::Matrix(const type::Matrix& n) :
        detail::NDArrayGenerator<type::Matrix, Matrix>(
            "Eigen::Matrix<" + std::get<0>(ElementType2Cpp.at(n.getElementType())) + ", " + (n.getRows() == -1 ? "Eigen::Dynamic" : std::to_string(n.getRows())) + ", " + (n.getCols() == -1 ? "Eigen::Dynamic" : std::to_string(n.getCols())) + ">",
            "Eigen::Matrix<" + std::get<0>(ElementType2Cpp.at(n.getElementType())) + ", " + (n.getRows() == -1 ? "Eigen::Dynamic" : std::to_string(n.getRows())) + ", " + (n.getCols() == -1 ? "Eigen::Dynamic" : std::to_string(n.getCols())) + ">",
            simox::meta::get_type_name<data::dto::NDArray>(),
            simox::meta::get_type_name<type::dto::Matrix>(), n)
    {
    }

    std::vector<std::string> Matrix::getRequiredIncludes() const
    {
        return {"<Eigen/Core>"};
    }

    CppBlockPtr Matrix::getResetSoftBlock(const std::string& cppAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine(cppAccessor + nextEl() + "setZero();");
        return this->resolveMaybeResetSoftBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr Matrix::getWriteTypeBlock(const std::string& typeAccessor, const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr b = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor;
        b->addLine("auto " + variantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeMatrix((int) " + std::to_string(type.getRows()) + ", " +
                "(int) " + std::to_string(type.getCols()) + ", " +
                std::get<2>(ElementType2Cpp.at(type.getElementType())) + ", " +
                conversion::Maybe2CppString.at(type.getMaybe()) + ", " +
                "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + cppAccessor);

        return b;
    }

    CppBlockPtr Matrix::getWriteBlock(const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor;

        block_if_data->addLine(variantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeNDArray({(int) " + cppAccessor + nextEl() + "rows(), "+
                "(int) " + cppAccessor + nextEl() + "cols(), " +
                std::to_string(std::get<1>(ElementType2Cpp.at(type.getElementType()))) + "}, "+
                "\"" + std::get<0>(ElementType2Cpp.at(type.getElementType())) + "\", "+
                "reinterpret_cast<const unsigned char*>(" + cppAccessor + nextEl() + "data()), " +
                "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {" + simox::alg::join(p.getPath(), ", ") + "})); // of " + cppAccessor);

        return resolveMaybeWriteBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr Matrix::getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        std::string type = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor + "_typeAsString";
        std::string dims = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor + "_shape";
        std::string data = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor + "_data";

        block_if_data->addLine("std::string " + type + ";");
        block_if_data->addLine("std::vector<int> " + dims + ";");
        block_if_data->addLine("std::vector<unsigned char> " + data + ";");
        block_if_data->addLine("" + ARON_READER_ACCESSOR + ".readNDArray("+variantAccessor+", "+dims+", "+type+", "+data+"); // of " + cppAccessor);
        block_if_data->addLine("std::memcpy(reinterpret_cast<unsigned char*>(" + cppAccessor + nextEl() + "data()), "+data+".data(), "+data+".size());");
        return resolveMaybeReadBlock(block_if_data, cppAccessor, variantAccessor);
    }

    CppBlockPtr Matrix::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine("if (not (" + accessor + nextEl() + "isApprox(" + resolveMaybeAccessor(otherInstanceAccessor) + ")))");
        block_if_data->addLineAsBlock("return false;");
        return resolveMaybeEqualsBlock(block_if_data, accessor, otherInstanceAccessor);
    }
}
