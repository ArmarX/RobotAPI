/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <set>
#include <map>
#include <vector>

// Parent class
#include <RobotAPI/libraries/aron/codegeneration/codegenerator/codewriter/CodeWriter.h>

// ArmarX
#include <ArmarXCore/libraries/cppgen/CppMethod.h>
#include <ArmarXCore/libraries/cppgen/CppClass.h>

#include <RobotAPI/libraries/aron/core/type/variant/All.h>
#include <RobotAPI/libraries/aron/codegeneration/codegenerator/codewriter/cpp/generator/toplevel/ObjectClass.h>
#include <RobotAPI/libraries/aron/codegeneration/codegenerator/codewriter/cpp/generator/toplevel/IntEnumClass.h>

namespace armarx::aron::codegenerator::cpp
{
    class Writer :
        virtual public codegenerator::CodeWriter
    {
    public:
        Writer() = delete;
        Writer(const std::string&, const std::vector<std::string>&);

        virtual ~Writer() = default;

        virtual void generateTypeObjects(const std::vector<typereader::GenerateObjectInfo>&) override;
        virtual void generateTypeIntEnums(const std::vector<typereader::GenerateIntEnumInfo>&) override;

    protected:
        virtual void addSpecificWriterMethods() override;
        virtual void addSpecificReaderMethods() override;

        CppClassPtr setupBasicCppClass(const typereader::GenerateInfo& info, const Generator& gen) const;
        void setupMemberFields(CppClassPtr&, const std::map<std::string, std::string>& doc_members, const generator::ObjectClass&) const;
        void setupMemberFields(CppClassPtr&, const std::map<std::string, std::string>& doc_members, const generator::IntEnumClass&) const;

    private:
        static const constexpr char* OWN_TYPE_NAME = "OWN_TYPE_NAME";

    };
}
