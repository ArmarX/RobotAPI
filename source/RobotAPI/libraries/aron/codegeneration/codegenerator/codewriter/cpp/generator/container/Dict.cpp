/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Dict.h"

#include <SimoxUtility/meta/type_name.h>


namespace armarx::aron::codegenerator::cpp::generator
{
    // constructors
    Dict::Dict(const type::Dict& e) :
        detail::ContainerGenerator<type::Dict, Dict>(
            "std::map<std::string, " + FromAronType(*e.getAcceptedType())->getFullInstantiatedCppTypename() + ">",
            "std::map<std::string, " + FromAronType(*e.getAcceptedType())->getFullInstantiatedCppTypename() + ">",
            simox::meta::get_type_name<data::dto::Dict>(),
            simox::meta::get_type_name<type::dto::Dict>(), e)
    {
    }

    // virtual implementations
    std::vector<std::string> Dict::getRequiredIncludes() const
    {
        auto c = FromAronType(*type.getAcceptedType());
        return c->getRequiredIncludes();
    }

    CppBlockPtr Dict::getResetSoftBlock(const std::string& cppAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine(cppAccessor + nextEl() + "clear();");
        return this->resolveMaybeResetSoftBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr Dict::getWriteTypeBlock(const std::string& typeAccessor, const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr b = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR+ "_" + escaped_accessor;

        auto type_s = FromAronType(*type.getAcceptedType());
        std::string nextVariantAccessor;
        Path nextPath = p.withAcceptedType(true);
        b->appendBlock(type_s->getWriteTypeBlock(type_s->getInstantiatedCppTypename(), cppAccessor + nextEl() + "accepted_type", nextPath, nextVariantAccessor));

        b->addLine("auto " + variantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeDict(" + nextVariantAccessor + ", " +
                   conversion::Maybe2CppString.at(type.getMaybe()) + ", " +
                   "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + cppAccessor);

        return b;
    }

    CppBlockPtr Dict::getWriteBlock(const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR+ "_" + escaped_accessor;

        const std::string elementsAccessor = variantAccessor + "_dictElements";
        block_if_data->addLine("std::map<std::string, _Aron_T> " + elementsAccessor + ";");

        std::string accessor_iterator_key = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_key";
        std::string accessor_iterator_val = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_value";
        std::string resolved_accessor = resolveMaybeAccessor(cppAccessor);

        block_if_data->addLine("for (const auto& [" + accessor_iterator_key + ", " + accessor_iterator_val + "] : " + resolved_accessor + ") ");
        {
            auto type_s = FromAronType(*type.getAcceptedType());
            CppBlockPtr for_loop = std::make_shared<CppBlock>();
            std::string nextVariantAccessor;
            Path nextPath = p.withElement(accessor_iterator_key);
            auto child_b = type_s->getWriteBlock(accessor_iterator_val, nextPath, nextVariantAccessor);
            for_loop->addLine("auto " + nextVariantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeNull();");
            for_loop->appendBlock(child_b);
            for_loop->addLine(elementsAccessor + ".emplace(" + accessor_iterator_key + ", " + nextVariantAccessor + ");");
            block_if_data->addBlock(for_loop);
        }

        block_if_data->addLine(variantAccessor + " = " + ARON_WRITER_ACCESSOR +
                               ".writeDict(" + elementsAccessor + ", " +
                               "std::nullopt, " +
                               "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + cppAccessor);

        return resolveMaybeWriteBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr Dict::getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        std::string elements_accessor = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_dictElements";
        std::string accessor_iterator_value = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_dictValue";
        std::string accessor_iterator_key = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_dictKey";

        block_if_data->addLine("std::map<std::string, _Aron_TNonConst> " + elements_accessor + ";");
        block_if_data->addLine("" + ARON_READER_ACCESSOR + ".readDict(" + variantAccessor + ", " + elements_accessor + ");");
        block_if_data->addLine("for (const auto& [" + accessor_iterator_key + ", " + accessor_iterator_value + "] : " + elements_accessor + ")");
        {
            auto type_s = FromAronType(*type.getAcceptedType());
            CppBlockPtr for_loop = std::make_shared<CppBlock>();
            std::string accessor_iterator_tmp = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_dictTmp";
            for_loop->addLine(type_s->getFullInstantiatedCppTypename() + " " + accessor_iterator_tmp +";");
            for_loop->appendBlock(type_s->getReadBlock(accessor_iterator_tmp, accessor_iterator_value));
            for_loop->addLine(cppAccessor + nextEl() + "insert({" + accessor_iterator_key + ", " + accessor_iterator_tmp + "});");
            block_if_data->addBlock(for_loop);
        }
        return resolveMaybeReadBlock(block_if_data, cppAccessor, variantAccessor);
    }
}

