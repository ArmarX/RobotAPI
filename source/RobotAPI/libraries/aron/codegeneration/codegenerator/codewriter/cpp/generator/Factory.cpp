/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL

// Header
#include "Factory.h"

// ArmarX
#include "All.h"
#include "enum/All.h"

namespace armarx::aron::codegenerator::cpp
{
    // Access method
    std::unique_ptr<Generator> GeneratorFactory::create(const type::Variant& n, const Path& path) const
    {
        auto desc = n.getDescriptor();
        switch(desc)
        {
            case type::Descriptor::LIST: return std::make_unique<generator::List>(dynamic_cast<const type::List&>(n));
            case type::Descriptor::DICT: return std::make_unique<generator::Dict>(dynamic_cast<const type::Dict&>(n));
            case type::Descriptor::OBJECT: return std::make_unique<generator::Object>(dynamic_cast<const type::Object&>(n));
            case type::Descriptor::TUPLE: return std::make_unique<generator::Tuple>(dynamic_cast<const type::Tuple&>(n));
            case type::Descriptor::PAIR: return std::make_unique<generator::Pair>(dynamic_cast<const type::Pair&>(n));
            case type::Descriptor::NDARRAY: return std::make_unique<generator::NDArray>(dynamic_cast<const type::NDArray&>(n));
            case type::Descriptor::MATRIX: return std::make_unique<generator::Matrix>(dynamic_cast<const type::Matrix&>(n));
            case type::Descriptor::QUATERNION: return std::make_unique<generator::Quaternion>(dynamic_cast<const type::Quaternion&>(n));
            case type::Descriptor::IMAGE: return std::make_unique<generator::Image>(dynamic_cast<const type::Image&>(n));
            case type::Descriptor::POINTCLOUD: return std::make_unique<generator::PointCloud>(dynamic_cast<const type::PointCloud&>(n));
            case type::Descriptor::INT_ENUM: return std::make_unique<generator::IntEnum>(dynamic_cast<const type::IntEnum&>(n));
            case type::Descriptor::INT: return std::make_unique<generator::Int>(dynamic_cast<const type::Int&>(n));
            case type::Descriptor::LONG: return std::make_unique<generator::Long>(dynamic_cast<const type::Long&>(n));
            case type::Descriptor::FLOAT: return std::make_unique<generator::Float>(dynamic_cast<const type::Float&>(n));
            case type::Descriptor::DOUBLE: return std::make_unique<generator::Double>(dynamic_cast<const type::Double&>(n));
            case type::Descriptor::STRING: return std::make_unique<generator::String>(dynamic_cast<const type::String&>(n));
            case type::Descriptor::BOOL: return std::make_unique<generator::Bool>(dynamic_cast<const type::Bool&>(n));
            case type::Descriptor::ANY_OBJECT: return std::make_unique<generator::AnyObject>(dynamic_cast<const type::AnyObject&>(n));
            case type::Descriptor::UNKNOWN: break;
        }
        throw error::AronEOFException(__PRETTY_FUNCTION__);
    }

}
