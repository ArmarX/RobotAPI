/*
* This file is part of ArmarX.
*
* Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
* Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// STD/STL

#include "Generator.h"
#include "Factory.h"

#include <SimoxUtility/meta/type_name.h>
#include <SimoxUtility/algorithm/string.h>


namespace armarx::aron::codegenerator::cpp
{
    // constantes
    const std::string Generator::ARON_VARIABLE_PREFIX = "aron";

    const std::string Generator::ARON_MAYBE_TYPE_ACCESSOR = Generator::ARON_VARIABLE_PREFIX + "_maybeType";
    const std::string Generator::ARON_READER_ACCESSOR = Generator::ARON_VARIABLE_PREFIX + "_r";
    const std::string Generator::ARON_WRITER_ACCESSOR = Generator::ARON_VARIABLE_PREFIX + "_w";
    const std::string Generator::ARON_TEMPLATE_INSTANTIATIONS_ACCESSOR = Generator::ARON_VARIABLE_PREFIX + "_tmpls";
    const std::string Generator::ARON_VARIANT_RETURN_ACCESSOR = Generator::ARON_VARIABLE_PREFIX + "_variant";
    const std::string Generator::ARON_PATH_ACCESSOR = Generator::ARON_VARIABLE_PREFIX + "_p";

    const SerializerFactoryPtr Generator::FACTORY = SerializerFactoryPtr(new GeneratorFactory());

    // static methods
    std::string Generator::EscapeAccessor(const std::string& accessor)
    {
        const static std::map<std::string, std::string> ESCAPE_ACCESSORS =
        {
            {"->", "_ptr_"},
            {".", "_dot_"},
            {"[", "_lbrC_"},
            {"]", "_rbrC_"},
            {"(", "_lbrR_"},
            {")", "_rbrR_"},
            {"*", "_ast_"}
        };

        std::string escaped_accessor = accessor;
        for (const auto& [key, value] : ESCAPE_ACCESSORS)
        {
            escaped_accessor = simox::alg::replace_all(escaped_accessor, key, value);
        }
        return escaped_accessor;
    }

    std::string Generator::ExtractCppTypename(const type::Variant& n)
    {
        auto cpp = Generator::FromAronType(n);
        ARMARX_CHECK_NOT_NULL(cpp);
        return cpp->getInstantiatedCppTypename();
    }
    std::vector<std::string> Generator::ExtractCppTypenames(const std::vector<type::VariantPtr>& n)
    {
        std::vector<std::string> ret;
        for (const auto& v : n)
        {
            ret.push_back(ExtractCppTypename(*v));
        }
        return ret;
    }

    std::unique_ptr<Generator> Generator::FromAronType(const type::Variant& n)
    {
        return FACTORY->create(n, n.getPath());
    }


    // constructors
    Generator::Generator(const std::string& instantiatedCppTypename, const std::string& classCppTypename, const std::string& aronDataTypename, const std::string& aronTypeTypename) :
        instantiatedCppTypename(instantiatedCppTypename),
        classCppTypename(classCppTypename),
        aronDataTypename(aronDataTypename),
        aronTypeTypename(aronTypeTypename)
    {

    }

    // public methods
    std::string Generator::getInstantiatedCppTypename() const
    {
        return instantiatedCppTypename;
    }

    std::string Generator::getFullInstantiatedCppTypename() const
    {
        switch (getType().getMaybe())
        {
            case type::Maybe::NONE:
                return getInstantiatedCppTypename();
            case type::Maybe::OPTIONAL:
                return "std::optional<" + getInstantiatedCppTypename() + ">";
            case type::Maybe::RAW_PTR:
                return getInstantiatedCppTypename() + "*";
            case type::Maybe::SHARED_PTR:
                return "std::shared_ptr<" + getInstantiatedCppTypename() + ">";
            case type::Maybe::UNIQUE_PTR:
                return "std::unique_ptr<" + getInstantiatedCppTypename() + ">";
            default:
                throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Received unknown maybe enum", std::to_string((int) getType().getMaybe()), getType().getPath());
        }
    }

    std::string Generator::getFullInstantiatedCppTypenameGenerator() const
    {
        switch (getType().getMaybe())
        {
            case type::Maybe::NONE:
                return getInstantiatedCppTypename();
            case type::Maybe::OPTIONAL:
                return "std::make_optional<" + getInstantiatedCppTypename() + ">";
            case type::Maybe::RAW_PTR:
                return getInstantiatedCppTypename() + "*";
            case type::Maybe::SHARED_PTR:
                return "std::make_shared<" + getInstantiatedCppTypename() + ">";
            case type::Maybe::UNIQUE_PTR:
                return "std::make_unique<" + getInstantiatedCppTypename() + ">";
            default:
                throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Received unknown maybe enum", std::to_string((int) getType().getMaybe()), getType().getPath());
        }
    }

    std::string Generator::getClassCppTypename() const
    {
        return classCppTypename;
    }

    std::string Generator::getFullClassCppTypename() const
    {
        switch (getType().getMaybe())
        {
            case type::Maybe::NONE:
                return getClassCppTypename();
            case type::Maybe::OPTIONAL:
                return "std::optional<" + getClassCppTypename() + ">";
            case type::Maybe::RAW_PTR:
                return getClassCppTypename() + "*";
            case type::Maybe::SHARED_PTR:
                return "std::shared_ptr<" + getClassCppTypename() + ">";
            case type::Maybe::UNIQUE_PTR:
                return "std::unique_ptr<" + getClassCppTypename() + ">";
            default:
                throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Received unknown maybe enum", std::to_string((int) getType().getMaybe()), getType().getPath());
        }
    }

    CppCtorPtr Generator::toCtor(const std::string& name) const
    {
        CppCtorPtr c = CppCtorPtr(new CppCtor(name + "()"));
        std::vector<std::pair<std::string, std::string>> initList = this->getCtorInitializers("");
        CppBlockPtr b = std::make_shared<CppBlock>();
        b->addLine("resetHard();");
        b->appendBlock(this->getCtorBlock(""));
        c->addInitListEntries(initList);
        c->setBlock(b);

        return c;
    }

    CppMethodPtr Generator::toResetSoftMethod() const
    {
        std::stringstream doc;
        doc << "@brief resetSoft() - This method resets all member variables with respect to the current parameterization. \n";
        doc << "@return - nothing";

        CppMethodPtr m = CppMethodPtr(new CppMethod("virtual void resetSoft() override", doc.str()));
        CppBlockPtr b = this->getResetSoftBlock("");
        m->setBlock(b);
        return m;
    }

    CppMethodPtr Generator::toResetHardMethod() const
    {
        std::stringstream doc;
        doc << "@brief resetHard() - This method resets member variables according to the XML type description. \n";
        doc << "@return - nothing";

        CppMethodPtr m = CppMethodPtr(new CppMethod("virtual void resetHard() override", doc.str()));
        CppBlockPtr b = this->getResetHardBlock("");
        m->setBlock(b);
        return m;
    }

    CppMethodPtr Generator::toWriteTypeMethod() const
    {
        std::stringstream doc;
        doc << "@brief writeType() - This method returns a new type from the class structure using a type writer implementation. This function is static. \n";
        doc << "@return - the result of the writer implementation";

        CppMethodPtr m = CppMethodPtr(new CppMethod("template<class WriterT>\nstatic typename WriterT::ReturnType writeType(WriterT& " + ARON_WRITER_ACCESSOR + ", std::vector<std::string> " + ARON_TEMPLATE_INSTANTIATIONS_ACCESSOR + " = {}, armarx::aron::type::Maybe "+ ARON_MAYBE_TYPE_ACCESSOR +" = ::armarx::aron::type::Maybe::NONE, const ::armarx::aron::Path& "+ARON_PATH_ACCESSOR+" = ::armarx::aron::Path())", doc.str()));
        CppBlockPtr b = std::make_shared<CppBlock>();
        b->addLine("using _Aron_T [[maybe_unused]] = typename WriterT::ReturnType;");

        std::string dummy;
        b->appendBlock(this->getWriteTypeBlock("", "", Path(), dummy));
        m->setBlock(b);
        return m;
    }

    CppMethodPtr Generator::toWriteMethod() const
    {
        std::stringstream doc;
        doc << "@brief write() - This method returns a new type from the member data types using a data writer implementation. \n";
        doc << "@param w - The writer implementation\n";
        doc << "@return - the result of the writer implementation";

        CppMethodPtr m = CppMethodPtr(new CppMethod("template<class WriterT>\ntypename WriterT::ReturnType write(WriterT& " + ARON_WRITER_ACCESSOR + ", const ::armarx::aron::Path& "+ARON_PATH_ACCESSOR+" = armarx::aron::Path()) const", doc.str()));
        CppBlockPtr b = std::make_shared<CppBlock>();
        b->addLine("using _Aron_T [[maybe_unused]] = typename WriterT::ReturnType;");

        b->addLine("try");
        std::string dummy;
        b->addBlock(this->getWriteBlock("", Path(), dummy));
        b->addLine("catch(const std::exception& " + ARON_VARIABLE_PREFIX + "_e)");
        b->addLineAsBlock("throw ::armarx::aron::error::AronException(__PRETTY_FUNCTION__, std::string(\"An error occured during the write method of an aron generated class. The full error log was:\\n\") + " + ARON_VARIABLE_PREFIX + "_e.what());");

        m->setBlock(b);
        return m;
    }

    CppMethodPtr Generator::toReadMethod() const
    {
        std::stringstream doc;
        doc << "@brief read() - This method sets the struct members to new values given in a data reader implementation. \n";
        doc << "@param r - The reader implementation\n";
        doc << "@return - nothing";

        CppMethodPtr m = CppMethodPtr(new CppMethod("template<class ReaderT>\nvoid read(ReaderT& " + ARON_READER_ACCESSOR + ", typename ReaderT::InputType& input)", doc.str()));
        CppBlockPtr b = std::make_shared<CppBlock>();
        b->addLine("using _Aron_T [[maybe_unused]] = typename ReaderT::InputType;");
        b->addLine("using _Aron_TNonConst [[maybe_unused]] = typename ReaderT::InputTypeNonConst;");

        b->addLine("this->resetSoft();");
        b->addLine("if (" + ARON_READER_ACCESSOR + ".readNull(input))");
        b->addLineAsBlock("throw ::armarx::aron::error::AronException(__PRETTY_FUNCTION__, \"The input to the read method must not be null.\");");

        b->addLine("try");
        b->addBlock(this->getReadBlock("", "input"));
        b->addLine("catch(const std::exception& " + ARON_VARIABLE_PREFIX + "_e)");
        b->addLineAsBlock("throw ::armarx::aron::error::AronException(__PRETTY_FUNCTION__, std::string(\"An error occured during the read method of an aron generated class. The full error log was:\\n\") + " + ARON_VARIABLE_PREFIX + "_e.what());");
        m->setBlock(b);
        return m;
    }

    CppMethodPtr Generator::toSpecializedDataWriterMethod(const WriterInfo& info) const
    {
        std::stringstream doc;
        doc << "@brief " << info.methodName << "() - This method returns a new data from the member data types using a writer implementation. \n";
        doc << "@return - the result of the writer implementation";

        CppMethodPtr m = CppMethodPtr(new CppMethod(info.returnType + " " + info.methodName + "() const", doc.str()));
        m->addLine(info.writerClassType + " writer;");
        m->addLine("return " + info.enforceConversion + "(this->write(writer))" + info.enforceMemberAccess + ";");
        return m;
    }

    CppMethodPtr Generator::toSpecializedDataReaderMethod(const ReaderInfo& info) const
    {
        std::stringstream doc;
        doc << "@brief " << info.methodName << " - This method sets the struct members to new values given in a reader implementation. \n";
        doc << "@return - nothing";

        CppMethodPtr m = CppMethodPtr(new CppMethod("void " + info.methodName + "(const " + info.argumentType + "& input)", doc.str()));
        m->addLine(info.readerClassType + " reader;");
        m->addLine("this->read(reader, " + info.enforceConversion + "(input)" + info.enforceMemberAccess + ");");
        return m;
    }

    CppMethodPtr Generator::toSpecializedStaticDataReaderMethod(const StaticReaderInfo& info) const
    {
        std::stringstream doc;
        doc << "@brief " << info.methodName << "() - This method sets the struct members to new values given in a reader implementation. \n";
        doc << "@return - nothing";

        CppMethodPtr m = CppMethodPtr(new CppMethod("static " + info.returnType + " " + info.methodName + "(const " + info.argumentType + "& input)", doc.str()));
        m->addLine(info.returnType + " t;");
        m->addLine("t.fromAron(input);");
        m->addLine("return t;");
        return m;
    }

    CppMethodPtr Generator::toSpecializedTypeWriterMethod(const WriterInfo& info) const
    {
        std::stringstream doc;
        doc << "@brief " << info.methodName << "() - This method returns a new type from the member data types using a writer implementation. \n";
        doc << "@return - the result of the writer implementation";

        CppMethodPtr m = CppMethodPtr(new CppMethod("static " + info.returnType + " " + info.methodName + "()", doc.str()));
        m->addLine(info.writerClassType + " writer;");
        m->addLine("return " + info.enforceConversion + "(writeType(writer))" + info.enforceMemberAccess + ";");
        return m;
    }

    CppMethodPtr Generator::toEqualsMethod() const
    {
        std::stringstream doc;
        doc << "@brief operator==() - This method checks whether all values equal another instance. \n";
        doc << "@param i - The other instance\n";
        doc << "@return - true, if all members are the same, false otherwise";

        CppMethodPtr m = CppMethodPtr(new CppMethod("bool operator==(const " + this->getFullClassCppTypename() + "& i) const", doc.str()));
        CppBlockPtr b = this->getEqualsBlock("", "i");
        b->addLine("return true;");
        m->setBlock(b);
        return m;
    }

    // defaulted implementations of the blocks
    std::vector<std::string> Generator::getRequiredIncludes() const
    {
        return {};
    }
    std::vector<CppFieldPtr> Generator::getPublicVariableDeclarations(const std::string& name) const
    {
        auto field = std::make_shared<CppField>(this->getFullInstantiatedCppTypename(), name);
        return {field};
    }

    std::vector<std::pair<std::string, std::string>> Generator::getCtorInitializers(const std::string&) const
    {
        return {};
    }

    CppBlockPtr Generator::getCtorBlock(const std::string&) const
    {
        return std::make_shared<CppBlock>();
    }

    CppBlockPtr Generator::getResetHardBlock(const std::string& cppAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine(cppAccessor + " = " + this->getFullInstantiatedCppTypename() + "();");
        return resolveMaybeResetHardBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr Generator::getResetSoftBlock(const std::string& cppAccessor) const
    {
        auto block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine(cppAccessor + " = " + this->getFullInstantiatedCppTypename() + "();");
        return this->resolveMaybeResetSoftBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr Generator::getEqualsBlock(const std::string& accessor, const std::string& otherInstanceAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string resolved_accessor = this->resolveMaybeAccessor(accessor);
        std::string other_instance_resolved_accessor = this->resolveMaybeAccessor(otherInstanceAccessor);

        block_if_data->addLine("if (not (" + resolved_accessor + " == " + other_instance_resolved_accessor + "))");
        block_if_data->addLineAsBlock("return false;");
        return resolveMaybeEqualsBlock(block_if_data, accessor, otherInstanceAccessor);
    }


    // Helper methods
    std::string Generator::resolveMaybeAccessor(const std::string& s) const
    {
        const auto& t = getType();
        if (t.getMaybe() == type::Maybe::OPTIONAL)
        {
            return s + ".value()";
        }
        if (t.getMaybe() == type::Maybe::RAW_PTR || t.getMaybe() == type::Maybe::SHARED_PTR || t.getMaybe() == type::Maybe::UNIQUE_PTR)
        {
            return "*" + s;
        }
        return s;
    }

    std::string Generator::resolveMaybeGenerator(const std::string& s) const
    {
        const auto& t = getType();
        if (t.getMaybe() == type::Maybe::OPTIONAL)
        {
            return s + " = std::make_optional<" + getInstantiatedCppTypename() + ">();";
        }
        if (t.getMaybe() == type::Maybe::RAW_PTR)
        {
            return s + " = new " + getInstantiatedCppTypename() + "();";
        }
        if (t.getMaybe() == type::Maybe::SHARED_PTR)
        {
            return s + " = std::make_shared<" + getInstantiatedCppTypename() + ">();";
        }
        if (t.getMaybe() == type::Maybe::UNIQUE_PTR)
        {
            return s + " = std::make_unique<" + getInstantiatedCppTypename() + ">();";
        }
        return "";
    }

    std::string Generator::nextEl() const
    {
        const auto& type = getType();
        switch (type.getMaybe())
        {
            case type::Maybe::NONE:
                return ".";
            case type::Maybe::OPTIONAL: //[[fallthrough]];
            case type::Maybe::RAW_PTR: //[[fallthrough]];
            case type::Maybe::SHARED_PTR: //[[fallthrough]];
            case type::Maybe::UNIQUE_PTR:
                return "->";
        }
        throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Received unknown maybe enum", std::to_string((int) type.getMaybe()), type.getPath());
    }

    std::string Generator::toPointerAccessor(const std::string& cppAccessor) const
    {
        const auto& type = getType();
        switch (type.getMaybe())
        {
            case type::Maybe::RAW_PTR:
                return cppAccessor;
            case type::Maybe::SHARED_PTR: //[[fallthrough]];
            case type::Maybe::UNIQUE_PTR:
                return cppAccessor + ".get()";

            case type::Maybe::NONE: //[[fallthrough]];
            case type::Maybe::OPTIONAL: break;
        }
        throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Received invalid maybe enum (not a pointer?)", std::to_string((int) type.getMaybe()), type.getPath());
    }

    CppBlockPtr Generator::resolveMaybeResetHardBlock(const CppBlockPtr& block_if_data, const std::string& cppAccessor) const
    {
        const auto& type = getType();
        if (type.getMaybe() == type::Maybe::OPTIONAL)
        {
            CppBlockPtr b = std::make_shared<CppBlock>();
            b->addLine(cppAccessor + " = std::nullopt;");
            return b;
        }
        else if (type.getMaybe() == type::Maybe::RAW_PTR || type.getMaybe() == type::Maybe::SHARED_PTR || type.getMaybe() == type::Maybe::UNIQUE_PTR)
        {
            CppBlockPtr b = std::make_shared<CppBlock>();
            b->addLine(cppAccessor + " = nullptr;");
            return b;
        }
        return block_if_data;
    }

    CppBlockPtr Generator::resolveMaybeResetSoftBlock(const CppBlockPtr& block_if_data, const std::string& cppAccessor) const
    {
        const auto& type = getType();
        if (type.getMaybe() == type::Maybe::OPTIONAL || type.getMaybe() == type::Maybe::RAW_PTR || type.getMaybe() == type::Maybe::SHARED_PTR || type.getMaybe() == type::Maybe::UNIQUE_PTR)
        {
            CppBlockPtr b = std::make_shared<CppBlock>();
            b->addLine("if (" + cppAccessor + ") // if " + cppAccessor + " contains data");
            b->addBlock(block_if_data);
            return b;
        }
        return block_if_data;
    }

    CppBlockPtr Generator::resolveMaybeWriteBlock(const CppBlockPtr& block_if_data, const std::string& cppAccessor) const
    {
        const auto& type = getType();
        if (type.getMaybe() == type::Maybe::OPTIONAL || type.getMaybe() == type::Maybe::RAW_PTR || type.getMaybe() == type::Maybe::SHARED_PTR || type.getMaybe() == type::Maybe::UNIQUE_PTR)
        {
            CppBlockPtr b = std::make_shared<CppBlock>();
            b->addLine("if (" + cppAccessor + ") // if " + cppAccessor + " contains data");
            b->addBlock(block_if_data);
            return b;
        }
        return block_if_data;
    }

    CppBlockPtr Generator::resolveMaybeReadBlock(const CppBlockPtr& block_if_data, const std::string& cppAccessor, const std::string& variantAccessor) const
    {
        const auto& type = getType();
        if (type.getMaybe() == type::Maybe::OPTIONAL || type.getMaybe() == type::Maybe::RAW_PTR || type.getMaybe() == type::Maybe::SHARED_PTR || type.getMaybe() == type::Maybe::UNIQUE_PTR)
        {
            CppBlockPtr b = std::make_shared<CppBlock>();
            b->addLine("if (not (" + ARON_READER_ACCESSOR + ".readNull(" + variantAccessor + "))) // if aron contains data");
            {
                CppBlockPtr ifb = std::make_shared<CppBlock>();
                ifb->addLine(resolveMaybeGenerator(cppAccessor));
                ifb->appendBlock(block_if_data);
                b->addBlock(ifb);
            }
            return b;
        }
        return block_if_data;
    }

    CppBlockPtr Generator::resolveMaybeEqualsBlock(const CppBlockPtr& block_if_data, const std::string& accessor, const std::string& otherInstanceAccessor) const
    {
        const auto& type = getType();
        if (type.getMaybe() == type::Maybe::OPTIONAL || type.getMaybe() == type::Maybe::RAW_PTR || type.getMaybe() == type::Maybe::SHARED_PTR || type.getMaybe() == type::Maybe::UNIQUE_PTR)
        {
            CppBlockPtr b = std::make_shared<CppBlock>();
            b->addLine("if (not ((bool) " + accessor + " == (bool) " + otherInstanceAccessor + ")) // check if both contain data");
            b->addLineAsBlock("return false;");
            b->addLine("if ((bool) " + accessor + " && (bool) " + otherInstanceAccessor + ")");
            b->addBlock(block_if_data);
            return b;
        }
        return block_if_data;
    }
}


