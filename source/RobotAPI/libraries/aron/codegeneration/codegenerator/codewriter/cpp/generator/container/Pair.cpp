/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Pair.h"

#include <SimoxUtility/meta/type_name.h>
#include <SimoxUtility/algorithm/vector.hpp>

namespace armarx::aron::codegenerator::cpp::generator
{
    Pair::Pair(const type::Pair& e) :
        detail::ContainerGenerator<type::Pair, Pair>(
            "std::pair<" + ExtractCppTypename(*e.getFirstAcceptedType()) + ", " + ExtractCppTypename(*e.getSecondAcceptedType()) + ">",
            "std::pair<" + ExtractCppTypename(*e.getFirstAcceptedType()) + ", " + ExtractCppTypename(*e.getSecondAcceptedType()) + ">",
            simox::meta::get_type_name<data::dto::List>(),
            simox::meta::get_type_name<type::dto::Pair>(), e)
    {
    }


    std::vector<std::string> Pair::getRequiredIncludes() const
    {
        auto child_s1 = FromAronType(*type.getFirstAcceptedType());
        auto child_s2 = FromAronType(*type.getSecondAcceptedType());

        auto l1 = child_s1->getRequiredIncludes();
        auto l2 = child_s2->getRequiredIncludes();

        return simox::alg::appended(l1, l2);
    }

    CppBlockPtr Pair::getResetSoftBlock(const std::string& cppAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string resolved_accessor = resolveMaybeAccessor(cppAccessor);

        auto child_s1 = FromAronType(*type.getFirstAcceptedType());
        CppBlockPtr b21 = child_s1->getResetSoftBlock(cppAccessor + nextEl() + "first");
        block_if_data->appendBlock(b21);

        auto child_s2 = FromAronType(*type.getSecondAcceptedType());
        CppBlockPtr b22 = child_s2->getResetSoftBlock(cppAccessor + nextEl() + "second");
        block_if_data->appendBlock(b22);
        return this->resolveMaybeResetSoftBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr Pair::getWriteTypeBlock(const std::string& typeAccessor, const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR+ "_" + escaped_accessor;

        auto child_s1 = FromAronType(*type.getFirstAcceptedType());
        std::string accessor_iterator1 = cppAccessor + nextEl() + "first";
        std::string firstVariantAccessor;
        Path firstPath = p.withAcceptedTypeIndex(0);
        CppBlockPtr b21 = child_s1->getWriteTypeBlock(child_s1->getInstantiatedCppTypename(), accessor_iterator1, firstPath, firstVariantAccessor);
        block_if_data->appendBlock(b21);

        auto child_s2 = FromAronType(*type.getSecondAcceptedType());
        std::string accessor_iterator2 = cppAccessor + nextEl() + "second";
        std::string secondVariantAccessor;
        Path secondPath = p.withAcceptedTypeIndex(1);
        CppBlockPtr b22 = child_s2->getWriteTypeBlock(child_s2->getInstantiatedCppTypename(), accessor_iterator2, secondPath, secondVariantAccessor);
        block_if_data->appendBlock(b22);

        block_if_data->addLine("auto " + variantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writePair(" + firstVariantAccessor + ", " +
                               secondVariantAccessor + ", " +
                               conversion::Maybe2CppString.at(type.getMaybe()) + ", " +
                               "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + cppAccessor);
        return block_if_data;
    }

    CppBlockPtr Pair::getWriteBlock(const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR+ "_" + escaped_accessor;

        auto child_s1 = FromAronType(*type.getFirstAcceptedType());
        std::string accessor_iterator1 = cppAccessor + nextEl() + "first";
        std::string firstVariantAccessor;
        Path firstPath = p.withElement("\"0\"");
        CppBlockPtr b21 = child_s1->getWriteBlock(accessor_iterator1, firstPath, firstVariantAccessor);
        block_if_data->addLine("auto " + firstVariantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeNull();");
        block_if_data->appendBlock(b21);

        auto child_s2 = FromAronType(*type.getSecondAcceptedType());
        std::string accessor_iterator2 = cppAccessor + nextEl() + "second";
        std::string secondVariantAccessor;
        Path secondPath = p.withElement("\"1\"");
        CppBlockPtr b22 = child_s2->getWriteBlock(accessor_iterator2, secondPath, secondVariantAccessor);
        block_if_data->addLine("auto " + secondVariantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeNull();");
        block_if_data->appendBlock(b22);

        block_if_data->addLine(variantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writePair("+firstVariantAccessor+", "+
                               secondVariantAccessor+", " +
                               "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {" + simox::alg::join(p.getPath(), ", ") + "})); // of " + cppAccessor);
        return resolveMaybeWriteBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr Pair::getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        std::string elements_accessor = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_pairElements";

        block_if_data->addLine("std::vector<_Aron_TNonConst> " + elements_accessor + ";");
        block_if_data->addLine("" + ARON_READER_ACCESSOR + ".readList("+elements_accessor+"); // of " + cppAccessor);

        auto child_s1 = FromAronType(*type.getFirstAcceptedType());
        CppBlockPtr b21 = child_s1->getReadBlock(cppAccessor + nextEl() + "first", elements_accessor+"[0]");
        block_if_data->appendBlock(b21);

        auto child_s2 = FromAronType(*type.getSecondAcceptedType());
        CppBlockPtr b22 = child_s2->getReadBlock(cppAccessor + nextEl() + "second", elements_accessor+"[1]");
        block_if_data->appendBlock(b22);

        return resolveMaybeReadBlock(block_if_data, cppAccessor, variantAccessor);
    }
}

