/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "AnyObject.h"

#include <SimoxUtility/meta/type_name.h>


namespace armarx::aron::codegenerator::cpp::generator
{
    /* constructors */
    AnyObject::AnyObject(const type::AnyObject& e) :
        detail::AnyGenerator<type::AnyObject, AnyObject>(
            "armarx::aron::data::Dict",
            "armarx::aron::data::Dict",
            simox::meta::get_type_name<data::dto::Dict>(),
            simox::meta::get_type_name<type::dto::AnyObject>(),
            e)
    {
        // if the type is not known, we only accept shared ptr for unspecified aron dicts
        if (type.getMaybe() != type::Maybe::SHARED_PTR)
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "At the moment the unknown any object type must be a shared_ptr! ", std::to_string((int) type.getMaybe()) + " aka " + type::defaultconversion::string::Maybe2String.at(type.getMaybe()), type.getPath());
        }

    }

    /* virtual implementations */

    std::vector<std::string> AnyObject::getRequiredIncludes() const
    {
        std::vector<std::string> required_includes = {"<RobotAPI/libraries/aron/core/data/converter/variant/VariantConverter.h>"};
        return required_includes;
    }

    CppBlockPtr AnyObject::getWriteBlock(const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        auto block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = this->EscapeAccessor(cppAccessor);
        variantAccessor = Generator::ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor;

        block_if_data->addLine(variantAccessor + " = ::armarx::aron::data::readAndWrite<::armarx::aron::data::FromVariantConverter<WriterT>>(" + cppAccessor + "); // of " + cppAccessor);

        return block_if_data;
    }

    CppBlockPtr AnyObject::getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const
    {
        auto block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = this->EscapeAccessor(cppAccessor);

        block_if_data->addLine(cppAccessor + " = ::armarx::aron::data::Dict::DynamicCastAndCheck(::armarx::aron::data::readAndWrite<::armarx::aron::data::ToVariantConverter<ReaderT>>(" + variantAccessor + ")); // of " + cppAccessor);

        return block_if_data;
    }

    CppBlockPtr AnyObject::getWriteTypeBlock(const std::string& typeAccessor, const std::string& accessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr b = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(accessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor;

        b->addLine("auto " + variantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeAnyObject(" + conversion::Maybe2CppString.at(type.getMaybe()) + ", " +
                   "::armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + typeAccessor);
        return b;
    }
}
