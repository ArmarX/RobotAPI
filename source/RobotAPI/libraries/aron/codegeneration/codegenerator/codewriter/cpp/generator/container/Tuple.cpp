/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Tuple.h"

#include <SimoxUtility/meta/type_name.h>
#include <SimoxUtility/algorithm/vector.hpp>

namespace armarx::aron::codegenerator::cpp::generator
{
    Tuple::Tuple(const type::Tuple& e) :
        detail::ContainerGenerator<type::Tuple, Tuple>(
            "std::tuple<" + simox::alg::join(ExtractCppTypenames(e.getAcceptedTypes()), ", ") + ">",
            "std::tuple<" + simox::alg::join(ExtractCppTypenames(e.getAcceptedTypes()), ", ") + ">",
            simox::meta::get_type_name<data::dto::List>(),
            simox::meta::get_type_name<type::dto::Tuple>(), e)
    {
    }


    std::vector<std::string> Tuple::getRequiredIncludes() const
    {
        std::vector<std::string> ret;
        for (const auto& child : type.getAcceptedTypes())
        {
            auto child_s = FromAronType(*child);
            ret = simox::alg::appended(ret, child_s->getRequiredIncludes());
        }
        return ret;
    }

    CppBlockPtr Tuple::getResetSoftBlock(const std::string& cppAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string resolved_accessor = resolveMaybeAccessor(cppAccessor);

        unsigned int i = 0;
        for (const auto& child : type.getAcceptedTypes())
        {
            auto child_s = FromAronType(*child);
            CppBlockPtr b2 = child_s->getResetSoftBlock("std::get<" + std::to_string(i++) + ">(" + resolved_accessor + ")");
            block_if_data->appendBlock(b2);
        }
        return this->resolveMaybeResetSoftBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr Tuple::getWriteTypeBlock(const std::string& typeAccessor, const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string resolved_accessor = resolveMaybeAccessor(cppAccessor);
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR+ "_" + escaped_accessor;

        const std::string acceptedTypesAccessor = variantAccessor + "_tupleAcceptedTypes";
        block_if_data->addLine("std::vector<_Aron_T> " + acceptedTypesAccessor + ";");

        unsigned int i = 0;
        for (const auto& type : type.getAcceptedTypes())
        {
            std::string accessor_iterator = "std::get<" + std::to_string(i) + ">("+resolved_accessor+");";
            auto type_s = FromAronType(*type);
            std::string nextVariantAccessor;
            Path nextPath = p.withAcceptedTypeIndex(i++);
            CppBlockPtr b2 = type_s->getWriteTypeBlock(type_s->getInstantiatedCppTypename(), accessor_iterator, nextPath, nextVariantAccessor);
            block_if_data->appendBlock(b2);
            block_if_data->addLine(acceptedTypesAccessor + ".push_back(" + nextVariantAccessor + ");");
        }
        block_if_data->addLine("auto " + variantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeTuple(" + acceptedTypesAccessor + ", " +
                               conversion::Maybe2CppString.at(type.getMaybe()) + ", " +
                               "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + cppAccessor);
        return block_if_data;
    }

    CppBlockPtr Tuple::getWriteBlock(const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string resolved_accessor = resolveMaybeAccessor(cppAccessor);
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR+ "_" + escaped_accessor;

        const std::string elementsAccessor = variantAccessor + "_tupleElements";
        block_if_data->addLine("std::vector<_Aron_T> " + elementsAccessor + ";");

        unsigned int i = 0;
        for (const auto& type : type.getAcceptedTypes())
        {
            std::string accessor_iterator = "std::get<" + std::to_string(i) + ">("+resolved_accessor+");";
            auto type_s = FromAronType(*type);
            std::string nextVariantAccessor;
            Path nextPath = p.withElement("\"" + std::to_string(i++) + "\"");
            CppBlockPtr b2 = type_s->getWriteBlock(accessor_iterator, nextPath, nextVariantAccessor);
            block_if_data->addLine("auto " + nextVariantAccessor + " = " + ARON_WRITER_ACCESSOR + ".writeNull();");
            block_if_data->appendBlock(b2);
            block_if_data->addLine(elementsAccessor + ".push_back(" + nextVariantAccessor + ");");
        }

        block_if_data->addLine(variantAccessor+ " = " + ARON_WRITER_ACCESSOR + ".writeTuple("+elementsAccessor+", "+
                               "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {" + simox::alg::join(p.getPath(), ", ") + "})); // of " + cppAccessor);
        return resolveMaybeWriteBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr Tuple::getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        std::string resolved_accessor = resolveMaybeAccessor(cppAccessor);
        std::string elements_accessor = ARON_VARIABLE_PREFIX + "_" + escaped_accessor + "_tupleElements";

        block_if_data->addLine("std::vector<_Aron_TNonConst> " + elements_accessor + ";");
        block_if_data->addLine("" + ARON_READER_ACCESSOR + ".readList("+elements_accessor+"); // of " + cppAccessor);

        unsigned int i = 0;
        for (const auto& type : type.getAcceptedTypes())
        {
            auto type_s = FromAronType(*type);
            CppBlockPtr b2 = type_s->getReadBlock("std::get<" + std::to_string(i) + ">(" + resolved_accessor + ")", elements_accessor+"[" + std::to_string(i) + "]");
            block_if_data->appendBlock(b2);
            i++;
        }
        return resolveMaybeReadBlock(block_if_data, cppAccessor, variantAccessor);
    }
}

