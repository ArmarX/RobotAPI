/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/aron/codegeneration/codegenerator/codewriter/cpp/generator/detail/SpecializedGenerator.h>
#include <RobotAPI/libraries/aron/core/type/variant/enum/IntEnum.h>

#include <map>
#include <utility>  // std::pair


namespace armarx::aron::codegenerator::cpp::generator
{
    class IntEnumClass :
        public detail::SpecializedGeneratorBase<type::IntEnum, IntEnumClass>
    {
    public:
        // constructors
        IntEnumClass(const type::IntEnum&);
        virtual ~IntEnumClass() = default;

        // virtual implementations
        std::vector<CppFieldPtr> getPublicVariableDeclarations(const std::string&) const final;
        CppBlockPtr getResetHardBlock(const std::string& cppAccessor) const final;
        CppBlockPtr getResetSoftBlock(const std::string& cppAccessor) const final;
        CppBlockPtr getWriteTypeBlock(const std::string& typeAccessor, const std::string& cppAccessor, const Path&, std::string& variantAccessor) const final;
        CppBlockPtr getWriteBlock(const std::string& cppAccessor, const Path&, std::string& variantAccessor) const final;
        CppBlockPtr getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const final;
        CppBlockPtr getEqualsBlock(const std::string&, const std::string&) const final;

        // TODO: Move some of those methods to upper class for enums (if we want to support multiple enums)
        CppCtorPtr toCopyCtor(const std::string&) const;
        CppCtorPtr toInnerEnumCtor(const std::string&) const;
        CppEnumPtr toInnerEnumDefinition() const;
        CppMethodPtr toIntMethod() const;
        CppMethodPtr toCopyAssignmentMethod() const;
        CppMethodPtr toEnumAssignmentMethod() const;
        CppMethodPtr toIntAssignmentMethod() const;
        CppMethodPtr toToStringMethod() const;
        CppMethodPtr toFromStringMethod() const;

    private:
        // Members
        static const std::map<std::string, std::pair<std::string, int>> ACCEPTED_TYPES;
        static constexpr const char* IMPL_ENUM = "ImplEnum";
    };
}
