/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "IntEnum.h"

#include <SimoxUtility/meta/type_name.h>


namespace armarx::aron::codegenerator::cpp::generator
{
    // constructors
    IntEnum::IntEnum(const type::IntEnum& e) :
        detail::SpecializedGeneratorBase<type::IntEnum, IntEnum>(
            e.getEnumName(),
            e.getEnumName(),
            simox::meta::get_type_name<data::dto::NDArray>(),
            simox::meta::get_type_name<type::dto::IntEnum>(), e)
    {
    }

    CppBlockPtr IntEnum::getResetSoftBlock(const std::string& accessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine(accessor + nextEl() + "resetSoft();");
        return this->resolveMaybeResetSoftBlock(block_if_data, accessor);
    }

    CppBlockPtr IntEnum::getResetHardBlock(const std::string& accessor) const
    {
        CppBlockPtr b = std::make_shared<CppBlock>();
        if (type.getMaybe() != type::Maybe::NONE)
        {
            b->addLine(accessor + " = {};");
        }
        else
        {
            b->addLine(accessor + nextEl() + "resetHard();");
        }
        return this->resolveMaybeResetHardBlock(b, accessor);
    }

    CppBlockPtr IntEnum::getWriteTypeBlock(const std::string& typeAccessor, const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr b = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor;

        b->addLine("auto "+variantAccessor+" = " +getInstantiatedCppTypename() + "::writeType(" + ARON_WRITER_ACCESSOR + ", " +
                   "{}, " +
                   conversion::Maybe2CppString.at(type.getMaybe()) + ", " +
                   "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + cppAccessor);
        return b;
    }

    CppBlockPtr IntEnum::getWriteBlock(const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor;

        block_if_data->addLine(variantAccessor+" = " + cppAccessor + nextEl() + "write(" + ARON_WRITER_ACCESSOR + ", "+
                               "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + cppAccessor);

        return resolveMaybeWriteBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr IntEnum::getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        if (const auto reset = resolveMaybeGenerator(cppAccessor); !reset.empty())
        {
            block_if_data->addLine(reset);
        }
        block_if_data->addLine(cppAccessor + nextEl() + "read(" + ARON_READER_ACCESSOR + ", " + variantAccessor + ");");
        return resolveMaybeReadBlock(block_if_data, cppAccessor, variantAccessor);
    }
}
