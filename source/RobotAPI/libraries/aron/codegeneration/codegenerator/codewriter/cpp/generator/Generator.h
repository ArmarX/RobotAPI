/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/aron.h>
#include <RobotAPI/libraries/aron/core/Exception.h>
#include <RobotAPI/libraries/aron/core/type/variant/Variant.h>
#include <RobotAPI/libraries/aron/codegeneration/codegenerator/helper/ReaderInfo.h>
#include <RobotAPI/libraries/aron/codegeneration/codegenerator/helper/WriterInfo.h>

#include <ArmarXCore/libraries/cppgen/CppBlock.h>
#include <ArmarXCore/libraries/cppgen/CppField.h>
#include <ArmarXCore/libraries/cppgen/CppCtor.h>
#include <ArmarXCore/libraries/cppgen/CppMethod.h>
#include <ArmarXCore/libraries/cppgen/CppClass.h>

#include <memory>
#include <map>
#include <string>
#include <vector>


namespace armarx::aron::codegenerator::cpp
{

    namespace conversion
    {
        const std::map<type::Maybe, std::string> Maybe2CppString =
        {
            {type::Maybe::NONE, "::armarx::aron::type::Maybe::NONE"},
            {type::Maybe::OPTIONAL, "::armarx::aron::type::Maybe::OPTIONAL"},
            {type::Maybe::RAW_PTR, "::armarx::aron::type::Maybe::RAW_PTR"},
            {type::Maybe::SHARED_PTR, "::armarx::aron::type::Maybe::SHARED_PTR"},
            {type::Maybe::UNIQUE_PTR, "::armarx::aron::type::Maybe::UNIQUE_PTR"}
        };
    }

    class GeneratorFactory;
    typedef std::shared_ptr<GeneratorFactory> SerializerFactoryPtr;

    class Generator;
    typedef std::shared_ptr<Generator> GeneratorPtr;

    class Generator
    {
    public:
        using PointerType = GeneratorPtr;

    public:
        // constructors
        Generator() = delete;
        Generator(const std::string& instantiatedCppTypename /* used for templates, e.g. vector<string> */, const std::string& classCppTypename /* the raw typename, e.g. vector<T> */, const std::string& aronDataTypename, const std::string& aronTypeTypename);
        virtual ~Generator() = default;

        // public member methods
        std::string getInstantiatedCppTypename() const;
        std::string getFullInstantiatedCppTypename() const;
        std::string getFullInstantiatedCppTypenameGenerator() const;

        std::string getClassCppTypename() const;
        std::string getFullClassCppTypename() const;

        CppMethodPtr toSpecializedDataWriterMethod(const WriterInfo& info) const;
        CppMethodPtr toSpecializedDataReaderMethod(const ReaderInfo& info) const;
        CppMethodPtr toSpecializedStaticDataReaderMethod(const StaticReaderInfo& info) const;
        CppMethodPtr toSpecializedTypeWriterMethod(const WriterInfo& info) const;

        // virtual override definitions
        virtual std::vector<std::string> getRequiredIncludes() const;
        virtual std::vector<CppFieldPtr> getPublicVariableDeclarations(const std::string&) const;

        CppCtorPtr toCtor(const std::string&) const;
        virtual std::vector<std::pair<std::string, std::string>> getCtorInitializers(const std::string&) const;
        virtual CppBlockPtr getCtorBlock(const std::string&) const;

        CppMethodPtr toResetSoftMethod() const;
        virtual CppBlockPtr getResetSoftBlock(const std::string& cppAccessor) const;

        CppMethodPtr toResetHardMethod() const;
        virtual CppBlockPtr getResetHardBlock(const std::string& cppAccessor) const;

        CppMethodPtr toWriteTypeMethod() const;
        virtual CppBlockPtr getWriteTypeBlock(const std::string& typeAccessor, const std::string& cppAccessor, const Path&, std::string& variantAccessor) const = 0;

        CppMethodPtr toWriteMethod() const;
        virtual CppBlockPtr getWriteBlock(const std::string& cppAccessor, const Path&, std::string& variantAccessor) const = 0;

        CppMethodPtr toReadMethod() const;
        virtual CppBlockPtr getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const = 0;

        CppMethodPtr toEqualsMethod() const;
        virtual CppBlockPtr getEqualsBlock(const std::string& cppAccessorThis, const std::string& cppAccessorOther) const;

        virtual const type::Variant& getType() const = 0;

        // static methods
        static std::string EscapeAccessor(const std::string&);

        static std::string ExtractCppTypename(const type::Variant&);
        static std::vector<std::string> ExtractCppTypenames(const std::vector<type::VariantPtr>&);

        static std::unique_ptr<Generator> FromAronType(const type::Variant&);

    protected:
        std::string nextEl() const;
        std::string toPointerAccessor(const std::string&) const;

        std::string resolveMaybeAccessor(const std::string&) const;
        std::string resolveMaybeGenerator(const std::string&) const;

        CppBlockPtr resolveMaybeResetHardBlock(const CppBlockPtr&, const std::string&) const;
        CppBlockPtr resolveMaybeResetSoftBlock(const CppBlockPtr&, const std::string&) const;
        CppBlockPtr resolveMaybeWriteBlock(const CppBlockPtr&, const std::string&) const;
        CppBlockPtr resolveMaybeReadBlock(const CppBlockPtr&, const std::string&, const std::string&) const;
        CppBlockPtr resolveMaybeEqualsBlock(const CppBlockPtr&, const std::string&, const std::string&) const;

    protected:
        static const std::string ARON_VARIABLE_PREFIX;

        static const std::string ARON_MAYBE_TYPE_ACCESSOR;
        static const std::string ARON_PATH_ACCESSOR;
        static const std::string ARON_READER_ACCESSOR;
        static const std::string ARON_WRITER_ACCESSOR;
        static const std::string ARON_TEMPLATE_INSTANTIATIONS_ACCESSOR;
        static const std::string ARON_VARIANT_RETURN_ACCESSOR;

    private:
        static const SerializerFactoryPtr FACTORY;

        std::string instantiatedCppTypename;
        std::string classCppTypename;
        std::string aronDataTypename;
        std::string aronTypeTypename;
    };
}
