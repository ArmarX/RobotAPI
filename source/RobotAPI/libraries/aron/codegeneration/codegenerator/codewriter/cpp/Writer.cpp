/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


// Simox
#include <SimoxUtility/algorithm/string.h>

// Header
#include "Writer.h"

// ArmarX

namespace armarx::aron::codegenerator::cpp
{
    Writer::Writer(const std::string& producerName, const std::vector<std::string>& additionalIncludesFromXMLFile) :
        CodeWriter(producerName, additionalIncludesFromXMLFile)
    {
        addSpecificWriterMethods();
        addSpecificReaderMethods();
    }

    void Writer::addSpecificWriterMethods()
    {
        // The toAron Serializer is visible by default
        {
            codegenerator::WriterInfo toAron;
            toAron.methodName = "toAron";
            toAron.returnType = "armarx::aron::data::DictPtr";
            toAron.writerClassType = "armarx::aron::data::writer::VariantWriter";
            toAron.include = "<RobotAPI/libraries/aron/core/data/rw/writer/variant/VariantWriter.h>";
            toAron.enforceConversion = "armarx::aron::data::Dict::DynamicCastAndCheck";
            dataWriters.push_back(toAron);
        }

        {
            codegenerator::WriterInfo toAronDTO;
            toAronDTO.methodName = "toAronDTO";
            toAronDTO.returnType = "armarx::aron::data::dto::DictPtr";
            toAronDTO.writerClassType = "armarx::aron::data::writer::VariantWriter";
            toAronDTO.include = "<RobotAPI/libraries/aron/core/data/rw/writer/variant/VariantWriter.h>";
            toAronDTO.enforceConversion = "armarx::aron::data::Dict::DynamicCastAndCheck";
            toAronDTO.enforceMemberAccess = "->toAronDictDTO()";
            dataWriters.push_back(toAronDTO);
        }

        {
            // The toAron Serializer is visible by default
            codegenerator::WriterInfo ToAronType;
            ToAronType.methodName = "ToAronType";
            ToAronType.returnType = "armarx::aron::type::ObjectPtr";
            ToAronType.writerClassType = "armarx::aron::type::writer::VariantWriter";
            ToAronType.include = "<RobotAPI/libraries/aron/core/type/rw/writer/variant/VariantWriter.h>";
            ToAronType.enforceConversion = "armarx::aron::type::Object::DynamicCastAndCheck";
            initialTypeWriters.push_back(ToAronType);
        }

        {
            // The toAron Serializer is visible by default
            codegenerator::WriterInfo ToAronTypeDTO;
            ToAronTypeDTO.methodName = "ToAronTypeDTO";
            ToAronTypeDTO.returnType = "armarx::aron::type::dto::AronObjectPtr";
            ToAronTypeDTO.writerClassType = "armarx::aron::type::writer::VariantWriter";
            ToAronTypeDTO.include = "<RobotAPI/libraries/aron/core/type/rw/writer/variant/VariantWriter.h>";
            ToAronTypeDTO.enforceConversion = "armarx::aron::type::Object::DynamicCastAndCheck";
            ToAronTypeDTO.enforceMemberAccess = "->toAronObjectDTO()";
            initialTypeWriters.push_back(ToAronTypeDTO);
        }
    }

    void Writer::addSpecificReaderMethods()
    {
        // The static FromAron Deserializer
        {
            codegenerator::StaticReaderInfo fromAron;
            fromAron.methodName = "FromAron";
            fromAron.argumentType = "armarx::aron::data::DictPtr";
            fromAron.returnType = OWN_TYPE_NAME;
            staticDataReaders.push_back(fromAron);
        }

        {
            codegenerator::StaticReaderInfo fromAronDTO;
            fromAronDTO.methodName = "FromAron";
            fromAronDTO.argumentType = "armarx::aron::data::dto::DictPtr";
            fromAronDTO.returnType = OWN_TYPE_NAME;
            staticDataReaders.push_back(fromAronDTO);
        }


        // The fromAron Deserializer is visible by default
        {
            codegenerator::ReaderInfo fromAron;
            fromAron.methodName = "fromAron";
            fromAron.argumentType = "armarx::aron::data::DictPtr";
            fromAron.readerClassType = "armarx::aron::data::reader::VariantReader";
            fromAron.include = "<RobotAPI/libraries/aron/core/data/rw/reader/variant/VariantReader.h>";
            dataReaders.push_back(fromAron);
        }

        {
            codegenerator::ReaderInfo fromAronDTO;
            fromAronDTO.methodName = "fromAron";
            fromAronDTO.argumentType = "armarx::aron::data::dto::DictPtr";
            fromAronDTO.readerClassType = "armarx::aron::data::reader::VariantReader";
            fromAronDTO.include = "<RobotAPI/libraries/aron/core/data/rw/reader/variant/VariantReader.h>";
            fromAronDTO.enforceConversion = "std::make_shared<armarx::aron::data::Dict>";
            dataReaders.push_back(fromAronDTO);
        }
    }


    void Writer::generateTypeObjects(const std::vector<typereader::GenerateObjectInfo>& generateObjects)
    {
        for (const auto& publicGenerateObjectType : generateObjects)
        {
            const auto type = publicGenerateObjectType.correspondingType;

            //std::cout << "Generating: " << nav->getName() << std::endl;

            // Convert to Object type and create class object
            ARMARX_CHECK_NOT_NULL(type);

            generator::ObjectClass generator(*type);
            CppClassPtr c = setupBasicCppClass(publicGenerateObjectType, generator);
            setupMemberFields(c, publicGenerateObjectType.doc_members, generator);

            // check for templates
            if (!type->getTemplates().empty())
            {
                std::vector<std::string> tmpl;
                std::vector<std::string> tmpl_requires;
                for (auto& t : type->getTemplates())
                {
                    tmpl_requires.push_back("armarx::aron::cpp::isAronGeneratedClass<" + t + ">");
                    tmpl.push_back("class " + t);
                }
                std::string templateDef = "template <" + simox::alg::join(tmpl, ", ") + ">";
                std::string requiresDef = "requires " + simox::alg::join(tmpl_requires, " && ");
                c->setTemplates(templateDef + "\n" + requiresDef);
            }

            // check for inheritance
            if (type->getExtends() != nullptr)
            {
                generator::ObjectClass extendsSerializer(*type->getExtends());
                c->addInherit("public " + extendsSerializer.getFullInstantiatedCppTypename());
            }
            else
            {
                c->addInherit("public armarx::aron::cpp::AronGeneratedClass");
            }

            // Writermethods
            for (codegenerator::WriterInfo info : dataWriters)
            {
                if (info.returnType == OWN_TYPE_NAME)
                {
                    info.returnType = generator.getFullClassCppTypename();
                }

                if (!info.include.empty())
                {
                    c->addInclude(info.include);
                }
                CppMethodPtr convert = generator.toSpecializedDataWriterMethod(info);
                c->addMethod(convert);
            }

            // Add methods to set the member variables
            // also resolve the original class name if the return type is set to special
            for (codegenerator::StaticReaderInfo info : staticDataReaders)
            {
                if (info.returnType == OWN_TYPE_NAME)
                {
                    info.returnType = generator.getFullClassCppTypename();
                }
                if (info.argumentType == OWN_TYPE_NAME)
                {
                    info.argumentType = generator.getFullClassCppTypename();
                }

                CppMethodPtr convert = generator.toSpecializedStaticDataReaderMethod(info);
                c->addMethod(convert);
            }

            // Add methods to set the member variables
            for (codegenerator::ReaderInfo info : dataReaders)
            {
                if (info.argumentType == OWN_TYPE_NAME)
                {
                    info.argumentType = generator.getFullClassCppTypename();
                }

                if (!info.include.empty())
                {
                    c->addInclude(info.include);
                }
                CppMethodPtr convert = generator.toSpecializedDataReaderMethod(info);
                c->addMethod(convert);
            }

            // Typewritermethods
            for (codegenerator::WriterInfo info : initialTypeWriters)
            {
                if (info.returnType == OWN_TYPE_NAME)
                {
                    info.returnType = generator.getFullClassCppTypename();
                }

                if (!info.include.empty())
                {
                    c->addInclude(info.include);
                }
                CppMethodPtr convert = generator.toSpecializedTypeWriterMethod(info);
                c->addMethod(convert);
            }

            typeClasses.push_back(c);
        }
    }

    void Writer::generateTypeIntEnums(const std::vector<typereader::GenerateIntEnumInfo>& generateIntEnums)
    {
        for (const auto& publicGenerateIntEnumType : generateIntEnums)
        {
            const auto& nav = publicGenerateIntEnumType.correspondingType;
            ARMARX_CHECK_NOT_NULL(nav);

            generator::IntEnumClass generator(*nav);
            CppClassPtr c = setupBasicCppClass(publicGenerateIntEnumType, generator);
            setupMemberFields(c, publicGenerateIntEnumType.doc_values, generator);

            c->addInherit("public armarx::aron::cpp::AronGeneratedClass");

            // ctor
            c->addCtor(generator.toCopyCtor(c->getName()));
            c->addCtor(generator.toInnerEnumCtor(c->getName()));

            // Specific methods
            CppEnumPtr enumrepresentation = generator.toInnerEnumDefinition();
            c->addInnerEnum(enumrepresentation);

            CppMethodPtr toString = generator.toToStringMethod();
            c->addMethod(toString);

            CppMethodPtr fromString = generator.toFromStringMethod();
            c->addMethod(fromString);

            CppMethodPtr intConversion = generator.toIntMethod();
            c->addMethod(intConversion);

            CppMethodPtr enumAssignment = generator.toEnumAssignmentMethod();
            c->addMethod(enumAssignment);

            CppMethodPtr enumAssignment2 = generator.toCopyAssignmentMethod();
            c->addMethod(enumAssignment2);

            CppMethodPtr enumAssignment3 = generator.toIntAssignmentMethod();
            c->addMethod(enumAssignment3);

            typeClasses.push_back(c);
        }
    }

    CppClassPtr Writer::setupBasicCppClass(const typereader::GenerateInfo& info, const Generator& gen) const
    {
        const auto& type = gen.getType();
        if (type.getMaybe() != type::Maybe::NONE)
        {
            // Should not happen since we check the maybe flag on creation of the generator. However, better to double check
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Somehow the maybe flag of a top level object declaration is set. This is not valid!", std::to_string((int) type.getMaybe()) + " aka " + type::defaultconversion::string::Maybe2String.at(type.getMaybe()), type.getPath());
        }

        const std::string classCppTypename = gen.getClassCppTypename();

        std::vector<std::string> namespaces = info.getNamespaces();
        std::string rawObjectName = info.getNameWithoutNamespace();

        CppClassPtr c = std::make_shared<CppClass>(namespaces, rawObjectName);
        // Add includes and guard
        std::string classDoc = "******************************************\n"
                               "* AUTOGENERATED CLASS. Please do NOT edit.\n"
                               "******************************************\n";

        if (!info.doc_author.empty() or !info.doc_brief.empty())
        {
            classDoc += (info.doc_brief.empty() ? "" : " * @brief " + info.doc_brief + "\n");
            classDoc += (info.doc_author.empty() ? "" : " * @author " + info.doc_author + "\n");
        }

        c->addClassDoc(classDoc);
        c->setPragmaOnceIncludeGuard(true);

        // add generator includes (e.g. coming from dto types)
        for (const auto& s : gen.getRequiredIncludes())
        {
            if (!s.empty())
            {
                c->addInclude(s);
            }
        }

        // add predefined includes from generator app (e.g. coming from aron includes)
        for (const std::string& s : additionalIncludes)
        {
            if (!s.empty())
            {
                c->addInclude(simox::alg::replace_last(s, ".aron.generated.codesuffix", ".aron.generated.h"));
            }
        }

        // add aron includes
        c->addInclude("<RobotAPI/libraries/aron/core/aron_conversions.h>");
        c->addInclude("<RobotAPI/libraries/aron/core/rw.h>");
        c->addInclude("<RobotAPI/libraries/aron/core/codegeneration/cpp/AronGeneratedClass.h>");

        // ctor
        c->addCtor(gen.toCtor(c->getName()));

        // Generic methods
        //std::cout << "Generate equals method" << std::endl;
        CppMethodPtr equals = gen.toEqualsMethod();
        c->addMethod(equals);

        //std::cout << "Generate reset method" << std::endl;
        CppMethodPtr resetHard = gen.toResetHardMethod();
        c->addMethod(resetHard);

        //std::cout << "Generate init method" << std::endl;
        CppMethodPtr resetSoft = gen.toResetSoftMethod();
        c->addMethod(resetSoft);

        //std::cout << "Generate writeInit method" << std::endl;
        CppMethodPtr writeType = gen.toWriteTypeMethod();
        c->addMethod(writeType);

        //std::cout << "Generate write method" << std::endl;
        CppMethodPtr write = gen.toWriteMethod();
        c->addMethod(write);

        //std::cout << "Generate read method" << std::endl;
        CppMethodPtr read = gen.toReadMethod();
        c->addMethod(read);
        return c;
    }

    void Writer::setupMemberFields(CppClassPtr& c, const std::map<std::string, std::string>& doc_members, const generator::ObjectClass& o) const
    {
        auto publicFields = o.getPublicVariableDeclarations("");
        for (const auto& f : publicFields)
        {
            if (auto it = doc_members.find(f->getName()); it != doc_members.end())
            {
                f->setDoc("@brief " + it->second);
            }
            c->addPublicField(f);
        }
    }

    void Writer::setupMemberFields(CppClassPtr& c, const std::map<std::string, std::string>& doc_members, const generator::IntEnumClass& o) const
    {
        auto publicFields = o.getPublicVariableDeclarations("");
        for (const auto& f : publicFields)
        {
            if (auto it = doc_members.find(f->getName()); it != doc_members.end())
            {
                f->setDoc("@brief " + it->second);
            }
            c->addPublicField(f);
        }
    }
}





