/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/aron/codegeneration/codegenerator/codewriter/cpp/generator/detail/SpecializedGenerator.h>
#include <RobotAPI/libraries/aron/core/type/variant/enum/IntEnum.h>

#include <map>
#include <string>


namespace armarx::aron::codegenerator::cpp::generator
{
    class IntEnum;
    typedef std::shared_ptr<IntEnum> IntEnumSerializerPtr;

    class IntEnum :
        public detail::SpecializedGeneratorBase<type::IntEnum, IntEnum>
    {
    public:
        // constructors
        IntEnum(const type::IntEnum&);
        virtual ~IntEnum() = default;

        // virtual implementations
        CppBlockPtr getResetHardBlock(const std::string& cppAccessor) const final;
        CppBlockPtr getResetSoftBlock(const std::string& cppAccessor) const final;
        CppBlockPtr getWriteTypeBlock(const std::string& typeAccessor, const std::string& cppAccessor, const Path&, std::string& variantAccessor) const final;
        CppBlockPtr getWriteBlock(const std::string& cppAccessor, const Path&, std::string& variantAccessor) const final;
        CppBlockPtr getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const final;
    };
}
