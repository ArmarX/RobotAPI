/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "Object.h"

#include <SimoxUtility/meta/type_name.h>
#include <SimoxUtility/algorithm/vector.hpp>

namespace armarx::aron::codegenerator::cpp::generator
{
    const std::map<std::string, DTOObjectReplacement> DTO_REPLACEMENTS =
    {
        { "armarx::arondto::DateTime", {"armarx::arondto::DateTime", "armarx::arondto::DateTime", "armarx::core::time::DateTime", "armarx::core::time::DateTime",
            {"<RobotAPI/libraries/aron/common/rw/time.h>"},
            {}}},
        { "armarx::arondto::Duration", {"armarx::arondto::Duration", "armarx::arondto::Duration", "armarx::core::time::Duration", "armarx::core::time::Duration",
            {"<RobotAPI/libraries/aron/common/rw/time.h>"},
            {"armarx::arondto::Frequency", "armarx::arondto::DateTime"}}},
        { "armarx::arondto::FramedPosition", {"armarx::arondto::FramedPosition", "armarx::arondto::FramedPosition", "armarx::FramedPosition", "armarx::FramedPosition",
            {"<RobotAPI/libraries/aron/common/rw/framed.h>"},
            {}}},
        { "armarx::arondto::FramedOrientation", {"armarx::arondto::FramedOrientation", "armarx::arondto::FramedOrientation", "armarx::FramedOrientation", "armarx::FramedOrientation",
            {"<RobotAPI/libraries/aron/common/rw/framed.h>"},
            {}}},
        { "armarx::arondto::FramedPose", {"armarx::arondto::FramedPose", "armarx::arondto::FramedPose", "armarx::FramedPose", "armarx::FramedPose",
            {"<RobotAPI/libraries/aron/common/rw/framed.h>"},
            {}}}
    };

    bool checkForAllowedReplacement(const type::Object& t)
    {
        std::string name = t.getObjectNameWithTemplates();
        if (DTO_REPLACEMENTS.count(name) > 0)
        {
            auto x = DTO_REPLACEMENTS.at(name);
            bool b = std::find(x.disallowedBases.begin(), x.disallowedBases.end(), t.getPath().getRootIdentifier()) == x.disallowedBases.end();
            return b;
        }
        return true;
    }

    std::string checkForInstantiationTypenameReplacement(const type::Object& t)
    {
        std::string name = t.getObjectNameWithTemplates();
        if (checkForAllowedReplacement(t) && DTO_REPLACEMENTS.count(name) > 0)
        {
            auto x = DTO_REPLACEMENTS.at(name);
            return x.replacedInstantiatedTypename;
        }
        return t.getObjectNameWithTemplateInstantiations();
    }

    std::string checkForClassNameReplacement(const type::Object& t)
    {
        std::string name = t.getObjectNameWithTemplates();
        if (checkForAllowedReplacement(t) && DTO_REPLACEMENTS.count(name) > 0)
        {
            auto x = DTO_REPLACEMENTS.at(name);
            return x.replacedTypename;
        }
        return name;
    }

    std::vector<std::string> checkForAdditionalIncludes(const type::Object& t)
    {
        std::string name = t.getObjectNameWithTemplates();
        if (checkForAllowedReplacement(t) && DTO_REPLACEMENTS.count(name) > 0)
        {
            auto x = DTO_REPLACEMENTS.at(name);
            return x.additionalIncludes;
        }
        return {};
    }

    // constructors
    Object::Object(const type::Object& e) :
        detail::ContainerGenerator<type::Object, Object>(
            checkForInstantiationTypenameReplacement(e),
            checkForClassNameReplacement(e),
            simox::meta::get_type_name<data::dto::Dict>(),
            simox::meta::get_type_name<type::dto::AronObject>(), e)
    {
    }


    std::vector<std::string> Object::getRequiredIncludes() const
    {
        std::vector<std::string> ret;

        // check for includes of members
        for (const auto& [key, child] : type.getMemberTypes())
        {
            (void) key;
            auto child_s = FromAronType(*child);
            ret = simox::alg::appended(ret, child_s->getRequiredIncludes());
        }

        // also check for additional includes from dto replacements
        ret = simox::alg::appended(ret, checkForAdditionalIncludes(type));

        return ret;
    }

    CppBlockPtr Object::getWriteTypeBlock(const std::string& typeAccessor, const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr b = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor;

        std::vector<std::string> templatesQuoted;
        for (const auto& t : type.getTemplateInstantiations())
        {
            templatesQuoted.push_back("\"" + t + "\"");
        }

        b->addLine("auto " + variantAccessor + " = " + type.getObjectNameWithTemplateInstantiations() + "::writeType(" + ARON_WRITER_ACCESSOR + ", " +
                   "{" + simox::alg::join(templatesQuoted, ", ") + "}, " +
                   conversion::Maybe2CppString.at(type.getMaybe()) + ", " +
                   "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + cppAccessor);
        return b;
    }

    CppBlockPtr Object::getWriteBlock(const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        std::string resolved_accessor = resolveMaybeAccessor(cppAccessor);
        variantAccessor = ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor;

        // if is dto replacement
        block_if_data->addLine("armarx::aron::write(" + ARON_WRITER_ACCESSOR + ", " + resolved_accessor + ", " + variantAccessor + ", " + "armarx::aron::Path("+ARON_PATH_ACCESSOR+", {"+simox::alg::join(p.getPath(), ", ")+"})); // of " + cppAccessor);

        return resolveMaybeWriteBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr Object::getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        std::string escaped_accessor = EscapeAccessor(cppAccessor);
        std::string resolved_accessor = resolveMaybeAccessor(cppAccessor);

        if (const auto reset = resolveMaybeGenerator(cppAccessor); !reset.empty())
        {
            block_if_data->addLine(reset);
        }

        block_if_data->addLine("armarx::aron::read(" + ARON_READER_ACCESSOR + ", " + variantAccessor + ", " + resolved_accessor + "); // of " + cppAccessor);

        return resolveMaybeReadBlock(block_if_data, cppAccessor, variantAccessor);
    }
}

