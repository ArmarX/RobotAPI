/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/aron/codegeneration/codegenerator/codewriter/cpp/generator/detail/ContainerGenerator.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>


namespace armarx::aron::codegenerator::cpp::generator
{
    struct DTOObjectReplacement
    {
        std::string originalTypeName;
        std::string originalInstantiatedTypename;
        std::string replacedTypename;
        std::string replacedInstantiatedTypename;

        std::vector<std::string> additionalIncludes; // additional includes for the replaced type and aron conversions
        std::vector<std::string> disallowedBases; // disallow replacement if the used in a specific class
    };

    class Object :
        public detail::ContainerGenerator<type::Object, Object>
    {
    public:
        // constructors
        Object(const type::Object&);
        virtual ~Object() = default;

        // virtual implementations
        std::vector<std::string> getRequiredIncludes() const final;
        CppBlockPtr getWriteTypeBlock(const std::string& typeAccessor, const std::string& cppAccessor, const Path&, std::string& variantAccessor) const final;
        CppBlockPtr getWriteBlock(const std::string& cppAccessor, const Path&, std::string& variantAccessor) const final;
        CppBlockPtr getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const final;
    };
}
