/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// Header
#include "NDArray.h"

#include <SimoxUtility/meta/type_name.h>


namespace armarx::aron::codegenerator::cpp::generator
{
    // constructors
    NDArray::NDArray(const type::NDArray& n) :
        detail::NDArrayGenerator<type::NDArray, NDArray>(
            "NDArray",
            "NDArray",
            simox::meta::get_type_name<data::dto::NDArray>(),
            simox::meta::get_type_name<type::dto::NDArray>(), n)
    {
    }

    CppBlockPtr NDArray::getResetSoftBlock(const std::string& cppAccessor) const
    {
        CppBlockPtr block_if_data = std::make_shared<CppBlock>();
        block_if_data->addLine(cppAccessor + " = " + this->getFullInstantiatedCppTypename() + "();");
        return resolveMaybeResetSoftBlock(block_if_data, cppAccessor);
    }

    CppBlockPtr NDArray::getWriteTypeBlock(const std::string&, const std::string&, const Path& p, std::string&) const
    {
        CppBlockPtr b = std::make_shared<CppBlock>();
        return b;
    }

    CppBlockPtr NDArray::getWriteBlock(const std::string& cppAccessor, const Path& p, std::string&) const
    {
        CppBlockPtr b = std::make_shared<CppBlock>();
        return b;
    }

    CppBlockPtr NDArray::getReadBlock(const std::string& cppAccessor, const std::string&) const
    {
        CppBlockPtr b = std::make_shared<CppBlock>();
        return b;
    }
}

