/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "SpecializedGenerator.h"

#include <string>


namespace armarx::aron::codegenerator::cpp::generator::detail
{
    template<typename typeT, typename DerivedT>
    class PrimitiveGenerator :
        public SpecializedGeneratorBase<typeT, DerivedT>
    {
    public:
        using SpecializedGeneratorBase<typeT, DerivedT>::SpecializedGeneratorBase;
        virtual ~PrimitiveGenerator() = default;

        CppBlockPtr getWriteBlock(const std::string& cppAccessor, const Path& p, std::string& variantAccessor) const override
        {
            auto block_if_data = std::make_shared<CppBlock>();
            std::string resolved_accessor = this->resolveMaybeAccessor(cppAccessor);
            std::string escaped_accessor = this->EscapeAccessor(cppAccessor);
            variantAccessor = Generator::ARON_VARIANT_RETURN_ACCESSOR + "_" + escaped_accessor;

            block_if_data->addLine(variantAccessor + " = " + this->ARON_WRITER_ACCESSOR + ".writePrimitive(" + resolved_accessor + ", "+
                                   "armarx::aron::Path("+this->ARON_PATH_ACCESSOR+", {" + simox::alg::join(p.getPath(), ", ") + "})); // of " + cppAccessor);

            return this->resolveMaybeWriteBlock(block_if_data, cppAccessor);
        }

        CppBlockPtr getReadBlock(const std::string& cppAccessor, const std::string& variantAccessor) const override
        {
            auto block_if_data = std::make_shared<CppBlock>();
            std::string resolved_accessor = this->resolveMaybeAccessor(cppAccessor);
            std::string escaped_accessor = this->EscapeAccessor(cppAccessor);

            block_if_data->addLine("" + this->ARON_READER_ACCESSOR + ".readPrimitive("+variantAccessor+", " + resolved_accessor + "); // of " + cppAccessor);
            return this->resolveMaybeReadBlock(block_if_data, cppAccessor, variantAccessor);
        }
    };
}
