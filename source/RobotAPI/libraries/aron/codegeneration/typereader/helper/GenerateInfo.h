/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <string>
#include <vector>

#include <SimoxUtility/algorithm/string.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

namespace armarx::aron::typereader
{
    /// A top-level struct for type-generation information
    struct GenerateInfo
    {
        std::string typeName;
        std::string definedIn;
        std::string doc_brief;
        std::string doc_author;


        std::string getNameWithoutNamespace() const
        {
            std::vector<std::string> split = simox::alg::split(typeName, "::");
            return split[split.size() -1];
        }

        std::vector<std::string> getTemplates() const
        {
            auto first = typeName.find("<");
            if (first == std::string::npos)
            {
                return {};
            }

            auto last = typeName.find(">");
            ARMARX_CHECK_NOT_EQUAL(last, std::string::npos);

            return simox::alg::split(typeName.substr(first,last-first), ",");
        }

        std::vector<std::string> getNamespaces() const
        {
            std::vector<std::string> split = simox::alg::split(typeName, "::");
            std::vector<std::string> namespaces(split);
            namespaces.pop_back();
            return namespaces;
        }
    };
}
