/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL


// Header
#include "Reader.h"
#include <sstream>

// Simox
#include <SimoxUtility/algorithm/string/string_tools.h>
#include <SimoxUtility/algorithm/vector.hpp>

// ArmarX
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <RobotAPI/libraries/aron/codegeneration/typereader/xml/Data.h>
#include <RobotAPI/libraries/aron/core/type/variant/Factory.h>


namespace armarx::aron::typereader::xml
{
    namespace fs = std::filesystem;

    namespace
    {
        /// Resolve a relative package path
        std::optional<fs::path> resolveRelativePackagePath(const fs::path& path, const std::vector<fs::path>& includePaths)
        {
            // new behavior: using provided include paths
            for (const auto& includePath : includePaths)
            {
                fs::path absPath = includePath / path;
                if (fs::is_regular_file(absPath))
                {
                    // path is valid
                    return absPath;
                }
            }

            // legacy behavior: using cmake package finder paths
            const std::string package = *path.begin();
            armarx::CMakePackageFinder finder(package);
            if (finder.packageFound())
            {
                for (const std::string& includePath : finder.getIncludePathList())
                {
                    fs::path absPath = includePath / path;
                    if (fs::is_regular_file(absPath))
                    {
                        // path is valid
                        return absPath;
                    }
                }
                return std::nullopt;
            }

            return std::nullopt;
        }
    }

    void Reader::parseFile(const std::string& _filename, const std::vector<std::filesystem::path>& includePaths)
    {
        std::string filename = _filename;

        // Handle C++ style includes like "<path/to/file>".
        if (!filename.empty() && filename.front() == '<' && filename.back() == '>')
        {
            filename = filename.substr(1, filename.size() - 2);
        }
        parseFile(std::filesystem::path(filename), includePaths);
    }

    void Reader::parseFile(const std::filesystem::path& _file, const std::vector<std::filesystem::path>& includePaths)
    {
        fs::path file = _file;
        if (not std::filesystem::exists(file))
        {
            // check if file is package path
            auto p = resolveRelativePackagePath(file, includePaths);
            if (not p)
            {
                throw error::AronException(__PRETTY_FUNCTION__, "Could not find the file " + file.string() + ". Tried include paths: " + simox::alg::to_string(includePaths));
            }
            file = *p;
        }

        RapidXmlReaderPtr reader = RapidXmlReader::FromFile(file.string());
        parse(reader, file, includePaths);
    }

    // private method reading nodes
    void Reader::parse(const RapidXmlReaderPtr& reader, const std::filesystem::path& filePath, const std::vector<std::filesystem::path>& includePaths)
    {
        RapidXmlReaderNode root = reader->getRoot();

        // Check Header
        util::EnforceTagName(root, constantes::TYPE_DEFINITION_TAG);

        util::EnforceChildSizeSmallerEqual(root, 4);

        auto includes = util::GetFirstNodeWithTag(root, constantes::INCLUDES_TAG);
        auto generate_types = util::GetFirstNodeWithTag(root, constantes::GENERATE_TYPES_TAG);

        // 1. Check code includes
        auto codeincludes = util::GetFirstNodeWithTag(root, constantes::CODE_INCLUDES_TAG);
        auto aronincludes = util::GetFirstNodeWithTag(root, constantes::ARON_INCLUDES_TAG);
        if (codeincludes.has_value())
        {
            for (const auto& include : (*codeincludes).nodes())
            {
                auto i = readCodeInclude(include, filePath.parent_path(), includePaths);
                if (not i.empty()) this->systemIncludes.push_back(i);
            }
        }

        // 2. Check aron includes
        if (aronincludes.has_value())
        {
            for (const auto& include : (*aronincludes).nodes())
            {
                auto i = readAronInclude(include, filePath.parent_path(), includePaths);
                if (not i.empty()) this->aronIncludes.push_back(i);
            }
        }

        // 3. Check general includes and try to deduce info
        if (includes.has_value())
        {
            for (const auto& include : (*includes).nodes())
            {
                if (util::HasTagName(include, constantes::SYSTEM_INCLUDE_TAG)) // if its a system include tag then we know that it must be a code include
                {
                    auto i = readCodeInclude(include, filePath.parent_path(), includePaths);
                    if (not i.empty()) this->systemIncludes.push_back(i);
                }
                else
                {
                    // case that the file can be either an aron or code file. We need to deduce the type based on the given information
                    std::string what = "";

                    if (util::HasTagName(include, constantes::PACKAGE_PATH_TAG)) // can be both
                    {
                        what = util::GetAttribute(include, constantes::PATH_ATTRIBUTE_NAME);
                    }

                    if (util::HasTagName(include, constantes::INCLUDE_TAG)) // can be both
                    {
                        what = util::GetAttribute(include, constantes::INCLUDE_ATTRIBUTE_NAME);
                    }

                    if (not what.empty() && std::filesystem::path(what) != filePath) // did we found something?
                    {
                        if (simox::alg::ends_with(what, ARON_FILE_SUFFIX))
                        {
                            auto i = readAronInclude(include, filePath.parent_path(), includePaths);
                            if (not i.empty()) this->aronIncludes.push_back(i);
                        }
                        else // we believe that this is a code include since it is not an xml file
                        {
                            auto i = readCodeInclude(include, filePath.parent_path(), includePaths);
                            if (not i.empty()) this->systemIncludes.push_back(i);
                        }
                    }
                }
            }
        }

        // 2. Check used tags of GenerateTypes for special defaulted includes
        if (generate_types.has_value())
        {
            for (const auto& generateType : (*generate_types).nodes())
            {
                if (util::HasTagName(generateType, constantes::OBJECT_TAG))
                {
                    for (const auto& additionalInclude : getAdditionalIncludesFromReplacements(generateType, filePath.parent_path()))
                    {
                        RapidXmlReaderPtr reader = RapidXmlReader::FromXmlString("<PackagePath package=\""+additionalInclude.first+"\" path=\""+additionalInclude.second+"\"/>");
                        auto node = reader->getRoot();

                        auto i = this->readAronInclude(node, filePath, includePaths);
                        if (not i.empty() && std::find(this->aronIncludes.begin(), this->aronIncludes.end(), i) == this->aronIncludes.end())
                        {
                            this->aronIncludes.push_back(i);
                        }
                    }
                }
            }
        }

        // 3. Check GenerateTypes
        if (generate_types.has_value())
        {
            for (const auto& generateType : (*generate_types).nodes())
            {
                if (util::HasTagName(generateType, constantes::OBJECT_TAG))
                {
                    const auto nav = readGenerateObject(generateType);
                    generateObjects.push_back(factory.allGeneratedPublicObjects.at(nav->getObjectName()));
                    continue;
                }

                if (util::HasTagName(generateType, constantes::INT_ENUM_TAG))
                {
                    const auto nav = readGenerateIntEnum(generateType);
                    generateIntEnums.push_back(factory.allGeneratedPublicIntEnums.at(nav->getEnumName()));
                    continue;
                }
                throw error::ValueNotValidException("XMLReader", "parse", "Could not find a valid tag inside generatetypes", generateType.name());
            }
        }
        else
        {
            throw error::AronException(__PRETTY_FUNCTION__, "No generate types found in aron xml '" + filePath.string() + "'.");
        }
    }

    std::pair<std::string, std::string> Reader::readPackagePathInclude(const RapidXmlReaderNode& node, const std::filesystem::path&, const std::vector<std::filesystem::path>& includePaths)
    {
        util::EnforceTagName(node, constantes::PACKAGE_PATH_TAG);
        std::string package = util::GetAttribute(node, constantes::PACKAGE_ATTRIBUTE_NAME);
        std::string path = util::GetAttribute(node, constantes::PATH_ATTRIBUTE_NAME);
        util::EnforceChildSize(node, 0);

        package = simox::alg::replace_all(package, "<", "");
        package = simox::alg::replace_all(package, ">", "");
        path = simox::alg::replace_all(path, "<", "");
        path = simox::alg::replace_all(path, ">", "");

        const std::filesystem::path includepath(package + "/" + path);
        if (std::optional<fs::path> resolvedPackagePath = resolveRelativePackagePath(includepath, includePaths); resolvedPackagePath.has_value())
        {
            return {package + "/" + path, "<" + package + "/" + path + ">"};
        }

        throw error::AronException(__PRETTY_FUNCTION__, "Could not find an file `" + includepath.string() + "`. Search paths are: " + simox::alg::to_string(includePaths));
    }

    std::pair<std::string, std::string> Reader::readInclude(const RapidXmlReaderNode& node, const std::filesystem::path& filepath, const std::vector<std::filesystem::path>& includePaths)
    {
        util::EnforceTagName(node, constantes::INCLUDE_TAG);
        std::string value = util::GetAttribute(node, constantes::INCLUDE_ATTRIBUTE_NAME);
        util::EnforceChildSize(node, 0);

        value = simox::alg::replace_all(value, "<", "");
        value = simox::alg::replace_all(value, ">", "");

        return {value, "<" + value + ">"};
    }

    std::pair<std::string, std::string> Reader::readSystemInclude(const RapidXmlReaderNode& node, const std::filesystem::path&, const std::vector<std::filesystem::path>& includePaths)
    {
        util::EnforceTagName(node, constantes::SYSTEM_INCLUDE_TAG);
        std::string value = util::GetAttribute(node, constantes::INCLUDE_ATTRIBUTE_NAME);
        util::EnforceChildSize(node, 0);

        value = simox::alg::replace_all(value, "<", "");
        value = simox::alg::replace_all(value, ">", "");

        return {"", "<" + value + ">"};
    }

    std::string Reader::readCodeInclude(const RapidXmlReaderNode& node, const std::filesystem::path& filepath, const std::vector<std::filesystem::path>& includePaths)
    {
        if (util::HasTagName(node, constantes::PACKAGE_PATH_TAG))
        {
            return readPackagePathInclude(node, filepath, includePaths).second;
        }
        if (util::HasTagName(node, constantes::INCLUDE_TAG))
        {
            return readInclude(node, filepath, includePaths).second;
        }
        if (util::HasTagName(node, constantes::SYSTEM_INCLUDE_TAG))
        {
            return readSystemInclude(node, filepath, includePaths).second;
        }

        throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Could not parse a code include. The tag is wrong.", node.name());
    }

    std::string Reader::readAronInclude(const RapidXmlReaderNode& node, const std::filesystem::path& filepath, const std::vector<std::filesystem::path>& includePaths)
    {
        std::string include;
        std::string include_unescaped;
        if (util::HasTagName(node, constantes::PACKAGE_PATH_TAG))
        {
            auto p = readPackagePathInclude(node, filepath, includePaths);
            include_unescaped = p.first;
            include = p.second;
        }
        if (util::HasTagName(node, constantes::INCLUDE_TAG))
        {
            auto p = readInclude(node, filepath, includePaths);
            include_unescaped = p.first;
            include = p.second;
        }

        if (not include.empty()) // we found something
        {
            // autoinclude the code file (suffix will be replaced by correct language)
            std::string codeinclude = simox::alg::replace_last(include, ARON_FILE_SUFFIX, CODE_FILE_SUFFIX);
            this->systemIncludes.push_back(codeinclude);

            // parse parent xml file and add objects to alreday known
            Reader anotherReader;
            anotherReader.parseFile(include_unescaped, includePaths);
            for (const auto& previouslyKnown : anotherReader.factory.allPreviouslyKnownPublicTypes)
            {
                factory.allPreviouslyKnownPublicTypes.push_back(previouslyKnown);
            }
            for (const auto& knownObjectInfo : anotherReader.factory.allGeneratedPublicObjects)
            {
                factory.allPreviouslyKnownPublicTypes.push_back(knownObjectInfo.first);
            }
            for (const auto& knownIntEnumInfo : anotherReader.factory.allGeneratedPublicIntEnums)
            {
                factory.allPreviouslyKnownPublicTypes.push_back(knownIntEnumInfo.first);
            }

            return include;
        }

        throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Could not parse an aron include. The tag is wrong.", node.name());
    }

    std::vector<std::pair<std::string, std::string>> Reader::getAdditionalIncludesFromReplacements(const RapidXmlReaderNode& node, const std::filesystem::path& filePath)
    {
        std::vector<std::pair<std::string, std::string>> ret;
        for (const auto& repl : constantes::REPLACEMENTS)
        {
            if (not repl.second.second.first.empty() && not repl.second.second.second.empty() && util::HasTagName(node, repl.first))
            {
                // we found a string that will be replaced so we might need to add a default include
                ret.push_back(repl.second.second);
                break;
            }
        }

        if (not ret.empty()) // children are not allowed for dto replaced nodes!
        {
            util::EnforceChildSize(node, 0);
        }

        for (const auto& n : node.nodes()) // only valid if ret is empty
        {
            auto v = getAdditionalIncludesFromReplacements(n, filePath);
            ret = simox::alg::appended(ret, v);
        }

        return ret;
    }

    type::ObjectPtr Reader::readGenerateObject(const RapidXmlReaderNode& node)
    {
        util::EnforceTagName(node, constantes::OBJECT_TAG);
        return type::Object::DynamicCastAndCheck(factory.create(node, Path()));
    }

    type::IntEnumPtr Reader::readGenerateIntEnum(const RapidXmlReaderNode& node)
    {
        util::EnforceTagName(node, constantes::INT_ENUM_TAG);
        return type::IntEnum::DynamicCastAndCheck(factory.create(node, Path()));
    }
}
