/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <map>
#include <vector>
#include <stack>

// ArmarX
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <RobotAPI/libraries/aron/codegeneration/typereader/helper/GenerateTypeInfo.h>
#include <RobotAPI/libraries/aron/codegeneration/typereader/helper/GenerateIntEnumInfo.h>
#include <RobotAPI/libraries/aron/core/type/variant/Variant.h>


namespace armarx::aron::typereader::xml
{
    /**
     * @brief The ReaderFactory class. It takes a xml-node and generates a type object representing the description in the xml node
     */
    class ReaderFactory
    {
    public:
        ReaderFactory() = default;

        /// the creation methods to return the types. Basically does a switch-case over the xml tag names converted to a descriptor object.
        type::VariantPtr create(const RapidXmlReaderNode&, const Path&);

    private:

        /// check, whether a given name corresponds to an already created object name.
        type::VariantPtr findExistingObject(const std::string& n, const Path& path) const;

        /// check, if the type should be a maybe type
        type::Maybe getMaybe(const RapidXmlReaderNode&) const;

        /// check, if the member name matches the requirements
        void checkObjectMemberName(const std::string&) const;


        type::VariantPtr createObject(const RapidXmlReaderNode& node, const Path& path);
        type::VariantPtr createList(const RapidXmlReaderNode& node, const Path& path);
        type::VariantPtr createDict(const RapidXmlReaderNode& node, const Path& path);
        type::VariantPtr createTuple(const RapidXmlReaderNode& node, const Path& path);
        type::VariantPtr createPair(const RapidXmlReaderNode& node, const Path& path);
        type::VariantPtr createIntEnum(const RapidXmlReaderNode& node, const Path& path);

        type::VariantPtr createNDArray(const RapidXmlReaderNode& node, const Path& path) const;
        type::VariantPtr createMatrix(const RapidXmlReaderNode& node, const Path& path) const;
        type::VariantPtr createQuaternion(const RapidXmlReaderNode& node, const Path& path) const;
        type::VariantPtr createImage(const RapidXmlReaderNode& node, const Path& path) const;
        type::VariantPtr createPointCloud(const RapidXmlReaderNode& node, const Path& path) const;
        type::VariantPtr createPosition(const RapidXmlReaderNode& node, const Path& path) const;
        type::VariantPtr createOrientation(const RapidXmlReaderNode& node, const Path& path) const;
        type::VariantPtr createPose(const RapidXmlReaderNode& node, const Path& path) const;

        type::VariantPtr createInt(const RapidXmlReaderNode& node, const Path& path) const;
        type::VariantPtr createLong(const RapidXmlReaderNode& node, const Path& path) const;
        type::VariantPtr createFloat(const RapidXmlReaderNode& node, const Path& path) const;
        type::VariantPtr createDouble(const RapidXmlReaderNode& node, const Path& path) const;
        type::VariantPtr createString(const RapidXmlReaderNode& node, const Path& path) const;
        type::VariantPtr createBool(const RapidXmlReaderNode& node, const Path& path) const;

        type::VariantPtr createAnyObject(const RapidXmlReaderNode& node, const Path& path) const;

    public:
        /// static map of all generated objects. Since this factory may be called recursively, it must be static
        std::map<std::string, typereader::GenerateObjectInfo> allGeneratedPublicObjects;

        /// same for int enums
        std::map<std::string, typereader::GenerateIntEnumInfo> allGeneratedPublicIntEnums;

        /// previously known types
        std::vector<std::string> allPreviouslyKnownPublicTypes;

    private:
        std::vector<std::string> allPreviouslyKnownPrivateTypes;
    };
}
