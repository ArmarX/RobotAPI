/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <filesystem>
#include <memory>
#include <optional>


// Base Class
#include <RobotAPI/libraries/aron/codegeneration/typereader/Reader.h>

// ArmarX
#include <SimoxUtility/xml.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/aron/codegeneration/typereader/xml/Factory.h>

namespace armarx::aron::typereader::xml
{
    /**
     * @brief The Reader class. It reads a xml-file and returns a type object and codegeneration information (such as additional includes etc)
     */
    class Reader :
        virtual public typereader::Reader<std::string>
    {
    public:
        Reader() = default;

        void parseFile(const std::string& filename, const std::vector<std::filesystem::path>& includePaths = {}) override;
        void parseFile(const std::filesystem::path& file, const std::vector<std::filesystem::path>& includePaths = {}) override;

    private:
        void parse(const RapidXmlReaderPtr& node, const std::filesystem::path& filePath, const std::vector<std::filesystem::path>& includePaths);

        // replacement management
        std::vector<std::pair<std::string, std::string>> getAdditionalIncludesFromReplacements(const RapidXmlReaderNode& node, const std::filesystem::path& filePath);

        // parse includes
        std::pair<std::string, std::string> readPackagePathInclude(const RapidXmlReaderNode& node, const std::filesystem::path& filename, const std::vector<std::filesystem::path>& includePaths);
        std::pair<std::string, std::string> readInclude(const RapidXmlReaderNode& node, const std::filesystem::path& filename, const std::vector<std::filesystem::path>& includePaths);
        std::pair<std::string, std::string> readSystemInclude(const RapidXmlReaderNode& node, const std::filesystem::path& filename, const std::vector<std::filesystem::path>& includePaths);

        std::string readCodeInclude(const RapidXmlReaderNode& node, const std::filesystem::path& filename, const std::vector<std::filesystem::path>& includePaths);
        std::string readAronInclude(const RapidXmlReaderNode& node, const std::filesystem::path& filename, const std::vector<std::filesystem::path>& includePaths);

        // parse top-level objects
        type::ObjectPtr readGenerateObject(const RapidXmlReaderNode& node);
        type::IntEnumPtr readGenerateIntEnum(const RapidXmlReaderNode& node);

    public:
        static const constexpr char* CODE_FILE_SUFFIX = ".aron.generated.codesuffix";
        static const constexpr char* ARON_FILE_SUFFIX = ".xml";

    private:
        ReaderFactory factory;
    };
}
