/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <map>
#include <optional>

// ArmarX
#include <SimoxUtility/xml.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

/**
 * All constantes for the aron XML parser, in addition to some utility functions wrapping around the armarxcore-xml-parser
 */
namespace armarx::aron::typereader::xml
{
    namespace constantes
    {
        // Definition tags
        const std::string TYPE_DEFINITION_TAG = "arontypedefinition";

        const std::string CODE_INCLUDES_TAG = "codeincludes";
        const std::string ARON_INCLUDES_TAG = "aronincludes";
        const std::string INCLUDES_TAG = "includes";
        const std::string SYSTEM_INCLUDE_TAG = "systeminclude";
        const std::string PACKAGE_PATH_TAG = "packagepath";
        const std::string INCLUDE_TAG = "include";
        const std::string GENERATE_TYPES_TAG = "generatetypes";

        // Attribute names
        const std::string ARON_ATTRIBUTE_NAME = "aron";
        const std::string CODE_ATTRIBUTE_NAME = "code";
        const std::string METHOD_ATTRIBUTE_NAME = "method";
        const std::string RETURN_ATTRIBUTE_NAME = "return";
        const std::string ARGUMENT_TYPE_ATTRIBUTE_NAME = "argumenttype";
        const std::string INCLUDE_ATTRIBUTE_NAME = "include";
        const std::string READER_ATTRIBUTE_NAME = "reader";
        const std::string WRITER_ATTRIBUTE_NAME = "writer";
        const std::string EXTENDS_ATTRIBUTE_NAME = "extends";
        const std::string NAME_ATTRIBUTE_NAME = "name";
        const std::string VALUE_ATTRIBUTE_NAME = "value";
        const std::string TEMPLATE_ATTRIBUTE_NAME = "template";
        const std::string KEY_ATTRIBUTE_NAME = "key";
        const std::string TYPE_ATTRIBUTE_NAME = "type";
        const std::string WIDTH_ATTRIBUTE_NAME = "width";
        const std::string HEIGHT_ATTRIBUTE_NAME = "height";
        const std::string ROWS_ATTRIBUTE_NAME = "rows";
        const std::string COLS_ATTRIBUTE_NAME = "cols";
        const std::string DIMENSIONS_ATTRIBUTE_NAME = "dimensions";
        const std::string SHAPE_ATTRIBUTE_NAME = "shape";
        const std::string OPTIONAL_ATTRIBUTE_NAME = "optional";
        const std::string RAW_PTR_ATTRIBUTE_NAME = "raw_ptr";
        const std::string SHARED_PTR_ATTRIBUTE_NAME = "shared_ptr";
        const std::string UNIQUE_PTR_ATTRIBUTE_NAME = "unique_ptr";
        const std::string PACKAGE_ATTRIBUTE_NAME = "package";
        const std::string DOC_BRIEF_ATTRIBUTE_NAME = "doc-brief";
        const std::string DOC_AUTHOR_ATTRIBUTE_NAME = "doc-author";
        const std::string DOC_PARAM_ATTRIBUTE_NAME = "doc-param";
        const std::string PATH_ATTRIBUTE_NAME = "path";

        // Second level tags. Only important if in specific top level tag
        const std::string OBJECT_CHILD_TAG = "objectchild";
        const std::string ENUM_VALUE_TAG = "enumvalue";

        // Top Level type tags
        const std::string LIST_TAG = "list";
        const std::string DICT_TAG = "dict";
        const std::string OBJECT_TAG = "object";
        const std::string PAIR_TAG = "pair";
        const std::string TUPLE_TAG = "tuple";
        const std::string INT_ENUM_TAG = "intenum";
        const std::string NDARRAY_TAG = "ndarray";
        const std::string MATRIX_TAG = "matrix";
        const std::string QUATERNION_TAG = "quaternion";
        const std::string IMAGE_TAG = "image";
        const std::string POINT_CLOUD_TAG = "pointcloud";
        const std::string INT_TAG = "int";
        const std::string LONG_TAG = "long";
        const std::string FLOAT_TAG = "float";
        const std::string DOUBLE_TAG = "double";
        const std::string STRING_TAG = "string";
        const std::string BOOL_TAG = "bool";
        const std::string ANY_OBJECT_TAG = "anyobject";

        // others
        const std::vector<std::string> WHATEVER_VALUES = {"?"};

        // Replacements ({tagName, {replacementsTag, additionalAronDTOXMLIncludePackagePath}})
        const std::map<std::string, std::pair<std::string, std::pair<std::string, std::string>>> REPLACEMENTS =
        {
            {"position",    {"<matrix rows='3' cols='1' type='float32' />", {}}},
            {"pose",        {"<matrix rows='4' cols='4' type='float32' />", {}}},
            {"orientation", {"<quaternion type='float32' />",               {}}},

            // You can also add replacements for arondtos here!
            // structure: {xml-identifier, {replacement, auto-include}}
            {"datetime",            {"<armarx::arondto::DateTime />",             {"RobotAPI", "libraries/aron/common/aron/time.xml"}}},
            {"time",                {"<armarx::arondto::DateTime />",             {"RobotAPI", "libraries/aron/common/aron/time.xml"}}},
            {"duration",            {"<armarx::arondto::Duration />",             {"RobotAPI", "libraries/aron/common/aron/time.xml"}}},
            {"framedposition",      {"<armarx::arondto::FramedPosition />",       {"RobotAPI", "libraries/aron/common/aron/framed.xml"}}},
            {"framedorientation",   {"<armarx::arondto::FramedOrientation />",    {"RobotAPI", "libraries/aron/common/aron/framed.xml"}}},
            {"framedpose",          {"<armarx::arondto::FramedPose />",           {"RobotAPI", "libraries/aron/common/aron/framed.xml"}}}
        };
    }


    namespace util
    {
        std::optional<RapidXmlReaderNode> GetFirstNodeWithTag(const RapidXmlReaderNode& node, const std::string& name);

        void EnforceAttribute(const RapidXmlReaderNode& node, const std::string& att);
        bool HasAttribute(const RapidXmlReaderNode& node, const std::string& att);
        std::string GetAttribute(const RapidXmlReaderNode& node, const std::string& att);
        std::string GetAttributeWithDefault(const RapidXmlReaderNode& node, const std::string& att, const std::string& def);
        bool AttributeIsTrue(const RapidXmlReaderNode& node, const std::string& att);

        bool HasTagName(const RapidXmlReaderNode& node, const std::string& name);
        void EnforceTagName(const RapidXmlReaderNode& node, const std::string& name);
        void EnforceTagNames(const RapidXmlReaderNode& node, const std::vector<std::string>& names);
        std::string GetTagName(const RapidXmlReaderNode& node);

        void EnforceChildSizeSmaller(const RapidXmlReaderNode& node, const size_t size);
        void EnforceChildSizeSmallerEqual(const RapidXmlReaderNode& node, const size_t size);
        void EnforceChildSize(const RapidXmlReaderNode& node, const size_t size);
        void EnforceChildSizeGreaterEqual(const RapidXmlReaderNode& node, const size_t size);
        void EnforceChildSizeGreater(const RapidXmlReaderNode& node, const size_t size);
    }
}
