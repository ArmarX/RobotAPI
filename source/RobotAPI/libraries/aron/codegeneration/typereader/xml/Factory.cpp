/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

// STD/STL

// Simox
#include <SimoxUtility/algorithm/string.h>

// Header
#include "Factory.h"

// ArmarX
#include <RobotAPI/libraries/aron/core/type/variant/All.h>
#include "Data.h"

namespace armarx::aron::typereader::xml
{

    type::VariantPtr ReaderFactory::create(const RapidXmlReaderNode& node, const Path& path)
    {
        static const std::map<std::string, type::Descriptor> String2Descriptor =
        {
            {constantes::LIST_TAG, type::Descriptor::LIST},
            {constantes::OBJECT_TAG, type::Descriptor::OBJECT},
            {constantes::TUPLE_TAG, type::Descriptor::TUPLE},
            {constantes::PAIR_TAG, type::Descriptor::PAIR},
            {constantes::DICT_TAG, type::Descriptor::DICT},
            {constantes::NDARRAY_TAG, type::Descriptor::NDARRAY},
            {constantes::MATRIX_TAG, type::Descriptor::MATRIX},
            {constantes::QUATERNION_TAG, type::Descriptor::QUATERNION},
            {constantes::POINT_CLOUD_TAG, type::Descriptor::POINTCLOUD},
            {constantes::IMAGE_TAG, type::Descriptor::IMAGE},
            {constantes::INT_ENUM_TAG, type::Descriptor::INT_ENUM},
            {constantes::INT_TAG, type::Descriptor::INT},
            {constantes::LONG_TAG, type::Descriptor::LONG},
            {constantes::FLOAT_TAG, type::Descriptor::FLOAT},
            {constantes::DOUBLE_TAG, type::Descriptor::DOUBLE},
            {constantes::STRING_TAG, type::Descriptor::STRING},
            {constantes::BOOL_TAG, type::Descriptor::BOOL},
            {constantes::ANY_OBJECT_TAG, type::Descriptor::ANY_OBJECT}
        };

        RapidXmlReaderNode nodeToUse = node;

        // check for replacement
        if (auto it = constantes::REPLACEMENTS.find(simox::alg::to_lower(nodeToUse.name())); it != constantes::REPLACEMENTS.end())
        {
            // We make the system believe there is another string in the xml
            RapidXmlReaderPtr reader = RapidXmlReader::FromXmlString(it->second.first);
            nodeToUse = reader->getRoot();
        }

        auto it = String2Descriptor.find(simox::alg::to_lower(nodeToUse.name()));
        auto descriptor = (it == String2Descriptor.end() ? type::Descriptor::UNKNOWN : it->second);

        switch(descriptor)
        {
            case type::Descriptor::LIST: return createList(nodeToUse, path);
            case type::Descriptor::DICT: return createDict(nodeToUse, path);
            case type::Descriptor::OBJECT: return createObject(nodeToUse, path);
            case type::Descriptor::TUPLE: return createTuple(nodeToUse, path);
            case type::Descriptor::PAIR: return createPair(nodeToUse, path);
            case type::Descriptor::NDARRAY: return createNDArray(nodeToUse, path);
            case type::Descriptor::MATRIX: return createMatrix(nodeToUse, path);
            case type::Descriptor::QUATERNION: return createQuaternion(nodeToUse, path);
            case type::Descriptor::IMAGE: return createImage(nodeToUse, path);
            case type::Descriptor::POINTCLOUD: return createPointCloud(nodeToUse, path);
            case type::Descriptor::INT_ENUM: return createIntEnum(nodeToUse, path);
            case type::Descriptor::INT: return createInt(nodeToUse, path);
            case type::Descriptor::LONG: return createLong(nodeToUse, path);
            case type::Descriptor::FLOAT: return createFloat(nodeToUse, path);
            case type::Descriptor::DOUBLE: return createDouble(nodeToUse, path);
            case type::Descriptor::STRING: return createString(nodeToUse, path);
            case type::Descriptor::BOOL: return createBool(nodeToUse, path);
            case type::Descriptor::ANY_OBJECT: return createAnyObject(nodeToUse, path);
            case type::Descriptor::UNKNOWN: return findExistingObject(nodeToUse.name(), path);
        }

        throw aron::error::AronEOFException(__PRETTY_FUNCTION__);
    }

    type::VariantPtr ReaderFactory::findExistingObject(const std::string& name, const Path& path) const
    {
        const auto public_intenum_it = allGeneratedPublicIntEnums.find(name);
        if (public_intenum_it != allGeneratedPublicIntEnums.end())
        {
            // copy the navigator
            auto v = type::Variant::FromAronDTO(*public_intenum_it->second.correspondingType->toAronDTO(), path);
            return v;
        }

        const auto public_obj_it = allGeneratedPublicObjects.find(name);
        if (public_obj_it != allGeneratedPublicObjects.end())
        {
            // copy the navigator and set instantiation template args
            auto v = type::Variant::FromAronDTO(*public_obj_it->second.correspondingType->toAronDTO(), path);
            return v;
        }

        const auto public_known_it = std::find(allPreviouslyKnownPublicTypes.begin(), allPreviouslyKnownPublicTypes.end(), name);
        if(public_known_it != allPreviouslyKnownPublicTypes.end())
        {
            // create an empty navigator
            auto v = std::make_shared<aron::type::Object>(*public_known_it, std::vector<std::string>(), std::vector<std::string>(), std::map<std::string, type::VariantPtr>(), path);
            return v;
        }

        // only works during generation process (not for top-level). Used for templates.
        const auto private_known_it = std::find(allPreviouslyKnownPrivateTypes.begin(), allPreviouslyKnownPrivateTypes.end(), name);
        if(private_known_it != allPreviouslyKnownPrivateTypes.end())
        {
            // create an empty navigator
            auto v = std::make_shared<aron::type::Object>(*private_known_it, std::vector<std::string>(), std::vector<std::string>(), std::map<std::string, type::VariantPtr>(), path);
            return v;
        }

        throw error::ValueNotValidException(__PRETTY_FUNCTION__, "Cannot find a valid object.", name);
    }

    type::Maybe ReaderFactory::getMaybe(const RapidXmlReaderNode& n) const
    {
        if (util::AttributeIsTrue(n, constantes::OPTIONAL_ATTRIBUTE_NAME))
        {
            return type::Maybe::OPTIONAL;
        }
        if (util::AttributeIsTrue(n, constantes::RAW_PTR_ATTRIBUTE_NAME))
        {
            return type::Maybe::RAW_PTR;
        }
        if (util::AttributeIsTrue(n, constantes::SHARED_PTR_ATTRIBUTE_NAME))
        {
            return type::Maybe::SHARED_PTR;
        }
        if (util::AttributeIsTrue(n, constantes::UNIQUE_PTR_ATTRIBUTE_NAME))
        {
            return type::Maybe::UNIQUE_PTR;
        }
        return type::Maybe::NONE;
    }

    void ReaderFactory::checkObjectMemberName(const std::string& s) const
    {
        if (simox::alg::starts_with(s, "_") || simox::alg::ends_with(s, "_"))
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "You used an invalid membername - '_' as starting or ending char is not allowed.", s);
        }

        if (simox::alg::starts_with(s, "aron"))
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "You used an invalid membername - The prefix 'aron' is used for codegeneration.", s);
        }
    }

    type::VariantPtr ReaderFactory::createObject(const RapidXmlReaderNode& node, const Path& path)
    {
        if (path.hasElement())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Having an inner class is not supported anymore since Aron Version 'beta 0.2.3'. Please move the inner class definition to the <" + constantes::GENERATE_TYPES_TAG + ">-tag.");
        }
        // ensured we are toplevel!

        const std::string name = util::GetAttribute(node, constantes::NAME_ATTRIBUTE_NAME);
        const std::string extends = util::GetAttributeWithDefault(node, constantes::EXTENDS_ATTRIBUTE_NAME, "");
        auto templates = simox::alg::split(util::GetAttributeWithDefault(node, constantes::TEMPLATE_ATTRIBUTE_NAME, ""), ",");

        auto newObjectInfo = typereader::GenerateObjectInfo();
        newObjectInfo.typeName = name;
        newObjectInfo.doc_brief = util::GetAttributeWithDefault(node, constantes::DOC_BRIEF_ATTRIBUTE_NAME, "");
        newObjectInfo.doc_author = util::GetAttributeWithDefault(node, constantes::DOC_AUTHOR_ATTRIBUTE_NAME, "");

        // reset private known types
        allPreviouslyKnownPrivateTypes = templates;

        // set objPath
        aron::Path objPath = path; // copy, just in case there are special things set
        objPath.setRootIdentifier(name); // set the root identifier to the obj name

        std::map<std::string, type::VariantPtr> members;
        for (const RapidXmlReaderNode& objectChild : node.nodes())
        {
            util::EnforceTagName(objectChild, constantes::OBJECT_CHILD_TAG);
            util::EnforceChildSize(objectChild, 1);

            util::EnforceAttribute(objectChild, constantes::KEY_ATTRIBUTE_NAME);
            const std::string key = util::GetAttribute(objectChild, constantes::KEY_ATTRIBUTE_NAME);

            checkObjectMemberName(key);

            if (util::HasAttribute(objectChild, constantes::DOC_BRIEF_ATTRIBUTE_NAME))
            {
                newObjectInfo.doc_members.insert({key, util::GetAttribute(objectChild, constantes::DOC_BRIEF_ATTRIBUTE_NAME)});
            }

            std::vector<RapidXmlReaderNode> children = objectChild.nodes();

            auto maybe = getMaybe(children[0]);
            type::VariantPtr childNavigator = create(children[0], objPath.withElement(key));

            if (childNavigator->getDescriptor() == aron::type::Descriptor::OBJECT)
            {
                // check if template args are present
                std::vector<std::string> templates = simox::alg::split(util::GetAttributeWithDefault(children[0], constantes::TEMPLATE_ATTRIBUTE_NAME, ""), ",");
                auto obj = aron::type::Object::DynamicCastAndCheck(childNavigator);
                for (const auto& t : templates)
                {
                    obj->addTemplateInstantiation(t);
                }
            }

            childNavigator->setMaybe(maybe);
            members.insert({key, childNavigator});
        }

        // set the new object
        auto aronObjectType = std::make_shared<type::Object>(name, templates, std::vector<std::string>(), members, objPath);

        if (extends != "")
        {
            auto parentObj = type::Object::DynamicCastAndCheck(findExistingObject(extends, Path()));
            aronObjectType->setExtends(parentObj);
        }

        newObjectInfo.correspondingType = aronObjectType;
        allGeneratedPublicObjects.emplace(newObjectInfo.typeName, newObjectInfo);

        allPreviouslyKnownPrivateTypes.clear();
        return aronObjectType;
    }

    type::VariantPtr ReaderFactory::createList(const RapidXmlReaderNode &node, const Path &path)
    {
        util::EnforceChildSize(node, 1);

        std::vector<RapidXmlReaderNode> c = node.nodes();
        const RapidXmlReaderNode typeNode = c[0];
        type::VariantPtr type = create(typeNode, path.withAcceptedType());
        type->setMaybe(getMaybe(typeNode));

        auto o = std::make_shared<type::List>(type, path);
        return o;
    }

    type::VariantPtr ReaderFactory::createDict(const RapidXmlReaderNode &node, const Path &path)
    {
        util::EnforceChildSize(node, 1);

        std::vector<RapidXmlReaderNode> c = node.nodes();
        const RapidXmlReaderNode typeNode = c[0];
        type::VariantPtr type = create(typeNode, path.withAcceptedType());
        type->setMaybe(getMaybe(typeNode));

        auto o = std::make_shared<type::Dict>(type, path);
        return o;
    }

    type::VariantPtr ReaderFactory::createTuple(const RapidXmlReaderNode& node, const Path& path)
    {
        util::EnforceChildSizeGreater(node, 1);

        unsigned int i = 0;
        std::vector<RapidXmlReaderNode> c = node.nodes();
        std::vector<type::VariantPtr> elementTypes;
        for (const RapidXmlReaderNode& tupleTypeDeclarationNode : c)
        {
            util::EnforceChildSize(tupleTypeDeclarationNode, 1);

            std::vector<RapidXmlReaderNode> typeNodeChildren = tupleTypeDeclarationNode.nodes();
            const RapidXmlReaderNode typeNode = typeNodeChildren[0];

            type::VariantPtr type = create(typeNode, path.withAcceptedTypeIndex(i++));
            type->setMaybe(getMaybe(typeNode));

            elementTypes.push_back(type);
        }

        auto o = std::make_shared<type::Tuple>(elementTypes, path);
        return o;
    }

    type::VariantPtr ReaderFactory::createPair(const RapidXmlReaderNode& node, const Path& path)
    {
        util::EnforceChildSize(node, 2);

        std::vector<RapidXmlReaderNode> c = node.nodes();
        const RapidXmlReaderNode type1Node = c[0];

        type::VariantPtr type1 = create(type1Node, path.withAcceptedTypeIndex(0));
        type1->setMaybe(getMaybe(type1Node));

        const RapidXmlReaderNode type2Node = c[1];
        type::VariantPtr type2 = create(type2Node, path.withAcceptedTypeIndex(1));
        type2->setMaybe(getMaybe(type2Node));

        auto o = std::make_shared<type::Pair>(type1, type2, path);
        return o;
    }

    type::VariantPtr ReaderFactory::createNDArray(const RapidXmlReaderNode& node, const Path& path) const
    {
        static const std::map<std::string, type::ndarray::ElementType> String2NDArrayType =
        {
            {"int8", type::ndarray::ElementType::INT8},
            {"int16", type::ndarray::ElementType::INT16},
            {"int32", type::ndarray::ElementType::INT32},
            {"uint8", type::ndarray::ElementType::UINT8},
            {"uint16", type::ndarray::ElementType::UINT16},
            {"uint32", type::ndarray::ElementType::UINT32},
            {"float32", type::ndarray::ElementType::FLOAT32},
            {"float64", type::ndarray::ElementType::FLOAT64}
        };

        return nullptr;
    }

    type::VariantPtr ReaderFactory::createMatrix(const RapidXmlReaderNode &node, const Path &path) const
    {
        static const std::map<std::string, type::matrix::ElementType> String2MatrixType =
        {
            {"int16", type::matrix::ElementType::INT16},
            {"int32", type::matrix::ElementType::INT32},
            {"int64", type::matrix::ElementType::INT64},
            {"float32", type::matrix::ElementType::FLOAT32},
            {"float64", type::matrix::ElementType::FLOAT64}
        };

        auto o = std::make_shared<type::Matrix>(path);
        util::EnforceChildSize(node, 0);
        util::EnforceAttribute(node, constantes::TYPE_ATTRIBUTE_NAME);

        auto rows_str = util::GetAttributeWithDefault(node, constantes::ROWS_ATTRIBUTE_NAME, "4");
        auto cols_str = util::GetAttributeWithDefault(node, constantes::COLS_ATTRIBUTE_NAME, "4");

        if (std::find(constantes::WHATEVER_VALUES.begin(), constantes::WHATEVER_VALUES.end(), simox::alg::to_lower(rows_str)) != constantes::WHATEVER_VALUES.end())
        {
            rows_str = "-1";
        }
        if (std::find(constantes::WHATEVER_VALUES.begin(), constantes::WHATEVER_VALUES.end(), simox::alg::to_lower(cols_str)) != constantes::WHATEVER_VALUES.end())
        {
            cols_str = "-1";
        }

        const int rows = std::stoi(rows_str);
        const int cols = std::stoi(cols_str);
        std::string type = util::GetAttributeWithDefault(node, constantes::TYPE_ATTRIBUTE_NAME, "???");

        o->setRows(rows);
        o->setCols(cols);
        try
        {
            o->setElementType(String2MatrixType.at(simox::alg::to_lower(type)));
        }
        catch (std::out_of_range& e)
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The type tag is not valid", type);
        }
        return o;
    }

    type::VariantPtr ReaderFactory::createQuaternion(const RapidXmlReaderNode &node, const Path &path) const
    {
        static const std::map<std::string, type::quaternion::ElementType> String2QuaternionType =
        {
            {"float32", type::quaternion::ElementType::FLOAT32},
            {"float64", type::quaternion::ElementType::FLOAT64}
        };

        auto o = std::make_shared<type::Quaternion>(path);
        util::EnforceChildSize(node, 0);
        util::EnforceAttribute(node, constantes::TYPE_ATTRIBUTE_NAME);

        std::string type = util::GetAttributeWithDefault(node, constantes::TYPE_ATTRIBUTE_NAME, "???");

        try
        {
            o->setElementType(String2QuaternionType.at(simox::alg::to_lower(type)));
        }
        catch (std::out_of_range& e)
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The type tag is not valid", type);
        }
        return o;
    }


    type::VariantPtr ReaderFactory::createImage(const RapidXmlReaderNode &node, const Path &path) const
    {
        static const std::map<std::string, type::image::PixelType> String2PixelType =
        {
            {"rgb24", type::image::PixelType::RGB24},
            {"depth32", type::image::PixelType::DEPTH32}
        };

        auto o = std::make_shared<type::Image>(path);
        util::EnforceChildSize(node, 0);
        util::EnforceAttribute(node, constantes::TYPE_ATTRIBUTE_NAME);

        std::string type = util::GetAttributeWithDefault(node, constantes::TYPE_ATTRIBUTE_NAME, "???");

        try
        {
            o->setPixelType(String2PixelType.at(simox::alg::to_lower(type)));
        }
        catch (std::out_of_range& e)
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The type tag is not valid", type);
        }
        return o;
    }

    type::VariantPtr ReaderFactory::createPointCloud(const RapidXmlReaderNode &node, const Path &path) const
    {
        static const std::map<std::string, type::pointcloud::VoxelType> String2VoxelType =
        {
            {"pointxyz", type::pointcloud::VoxelType::POINT_XYZ},
            {"pointxyzi", type::pointcloud::VoxelType::POINT_XYZI},
            {"pointxyzl", type::pointcloud::VoxelType::POINT_XYZL},
            {"pointxyzrgb", type::pointcloud::VoxelType::POINT_XYZRGB},
            {"pointxyzrgbl", type::pointcloud::VoxelType::POINT_XYZRGBL},
            {"pointxyzrgba", type::pointcloud::VoxelType::POINT_XYZRGBA},
            {"pointxyzhsv", type::pointcloud::VoxelType::POINT_XYZHSV}
        };

        auto o = std::make_shared<type::PointCloud>(path);
        util::EnforceChildSize(node, 0);
        util::EnforceAttribute(node, constantes::TYPE_ATTRIBUTE_NAME);

        std::string type = util::GetAttributeWithDefault(node, constantes::TYPE_ATTRIBUTE_NAME, "???");
        try
        {
            o->setVoxelType(String2VoxelType.at(simox::alg::to_lower(type)));
        }
        catch (std::out_of_range& e)
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The type tag is not valid", type);
        }
        return o;
    }

    type::VariantPtr ReaderFactory::createIntEnum(const RapidXmlReaderNode& node, const Path& path)
    {
        if (path.hasElement())
        {
            throw error::AronException(__PRETTY_FUNCTION__, "Having an inner int-enum is not supported anymore since Aron Version 'beta 0.2.3'. Please move the inner int-enum definition to the <" + constantes::GENERATE_TYPES_TAG + ">-tag.");
        }
        // ensured we are top-level!

        const std::string name = util::GetAttribute(node, constantes::NAME_ATTRIBUTE_NAME);

        auto newEnumInfo = typereader::GenerateIntEnumInfo();
        newEnumInfo.typeName = name;

        allPreviouslyKnownPrivateTypes.clear();

        std::map<std::string, int> acceptedValues;
        for (const RapidXmlReaderNode& valueChild : node.nodes())
        {
            util::EnforceTagName(valueChild, constantes::ENUM_VALUE_TAG);
            util::EnforceChildSize(valueChild, 0);

            const std::string key = util::GetAttribute(valueChild, constantes::KEY_ATTRIBUTE_NAME);

            if (util::HasAttribute(valueChild, constantes::DOC_BRIEF_ATTRIBUTE_NAME))
            {
                newEnumInfo.doc_values.insert({key, util::GetAttribute(valueChild, constantes::DOC_BRIEF_ATTRIBUTE_NAME)});
            }

            const std::string value = util::GetAttribute(valueChild, constantes::VALUE_ATTRIBUTE_NAME);

            acceptedValues.emplace(key, std::stoi(value));
        }

        // create the int enum
        aron::Path enumPath = path; // copy, just in case there are special things set
        enumPath.setRootIdentifier(name); // set the root identifier to the obj name
        auto o = std::make_shared<type::IntEnum>(name, acceptedValues, enumPath);
        newEnumInfo.correspondingType = o;

        allGeneratedPublicIntEnums.emplace(newEnumInfo.typeName, newEnumInfo);

        allPreviouslyKnownPrivateTypes.clear();
        return o;
    }

    type::VariantPtr ReaderFactory::createInt(const RapidXmlReaderNode &node, const Path &path) const
    {
        return std::make_shared<type::Int>(path);
    }

    type::VariantPtr ReaderFactory::createLong(const RapidXmlReaderNode &node, const Path &path) const
    {
        return std::make_shared<type::Long>(path);
    }

    type::VariantPtr ReaderFactory::createFloat(const RapidXmlReaderNode &node, const Path &path) const
    {
        return std::make_shared<type::Float>(path);
    }

    type::VariantPtr ReaderFactory::createDouble(const RapidXmlReaderNode &node, const Path &path) const
    {
        return std::make_shared<type::Double>(path);
    }

    type::VariantPtr ReaderFactory::createString(const RapidXmlReaderNode &node, const Path &path) const
    {
        return std::make_shared<type::String>(path);
    }

    type::VariantPtr ReaderFactory::createBool(const RapidXmlReaderNode &node, const Path &path) const
    {
        return std::make_shared<type::Bool>(path);
    }

    type::VariantPtr ReaderFactory::createAnyObject(const RapidXmlReaderNode& node, const Path& path) const
    {
        return std::make_shared<type::AnyObject>(path);
    }
}
