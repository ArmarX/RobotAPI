/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Data.h"

#include <SimoxUtility/algorithm/string/string_tools.h>
#include <RobotAPI/libraries/aron/core/Exception.h>

namespace armarx::aron::typereader::xml
{

    std::optional<RapidXmlReaderNode> util::GetFirstNodeWithTag(const RapidXmlReaderNode& node, const std::string& name)
    {
        for (const auto& n : node.nodes())
        {
            if (HasTagName(n, name))
            {
                return n;
            }
        }

        return std::nullopt;
    }

    void util::EnforceAttribute(const RapidXmlReaderNode& node, const std::string& att)
    {
        if (!HasAttribute(node, att))
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "A <" + node.name() + ">-tag does not have the '"+att+"' attribute", simox::alg::to_string(node.get_all_attributes()));
        }
    }

    bool util::HasAttribute(const RapidXmlReaderNode& node, const std::string& att)
    {
        return node.has_attribute(att.c_str());
    }

    std::string util::GetAttribute(const RapidXmlReaderNode& node, const std::string& att)
    {
        EnforceAttribute(node, att);
        return node.attribute_value(att.c_str());
    }

    std::string util::GetAttributeWithDefault(const armarx::RapidXmlReaderNode& node, const std::string& att, const std::string& def)
    {
        if (!(HasAttribute(node, att)))
        {
            return def;
        }
        return node.attribute_value(att.c_str());
    }

    bool util::AttributeIsTrue(const armarx::RapidXmlReaderNode& node, const std::string& att)
    {
        if (HasAttribute(node, att))
        {
            std::string v = simox::alg::to_lower(node.attribute_value(att.c_str()));
            if (v == "1" or v == "true" or v == "yes" or v == "ja" or v == "")
            {
                return true;
            }
        }
        return false;
    }

    bool util::HasTagName(const armarx::RapidXmlReaderNode& node, const std::string& name)
    {
        return (simox::alg::to_lower(name) == simox::alg::to_lower(node.name()));
    }

    void util::EnforceTagName(const armarx::RapidXmlReaderNode& node, const std::string& name)
    {
        if (!(HasTagName(node, name)))
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The node has the wrong tag. Expected: " + name, node.name());
        }
    }

    void util::EnforceTagNames(const RapidXmlReaderNode& node, const std::vector<std::string>& names)
    {
        for (const auto& name : names)
        {
            if (HasTagName(node, name))
            {
                return;
            }
        }
        throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The node has the wrong tag. Expected one of: " + simox::alg::to_string(names, ", "), node.name());
    }

    std::string util::GetTagName(const armarx::RapidXmlReaderNode& node)
    {
        return simox::alg::to_lower(node.name());
    }

    void util::EnforceChildSizeSmaller(const RapidXmlReaderNode& node, const size_t size)
    {
        const size_t s = node.nodes().size();
        if (s >= size)
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The node <" + node.name() + "> has the wrong number of children", std::to_string(s), std::to_string(size));
        }
    }

    void util::EnforceChildSizeSmallerEqual(const RapidXmlReaderNode& node, const size_t size)
    {
        const size_t s = node.nodes().size();
        if (s > size)
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The node <" + node.name() + "> has the wrong number of children", std::to_string(s), std::to_string(size));
        }
    }

    void util::EnforceChildSize(const armarx::RapidXmlReaderNode& node, const size_t size)
    {
        const size_t s = node.nodes().size();
        if (s != size)
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The node <" + node.name() + "> has the wrong number of children", std::to_string(s), std::to_string(size));
        }
    }

    void util::EnforceChildSizeGreaterEqual(const RapidXmlReaderNode& node, const size_t size)
    {
        const size_t s = node.nodes().size();
        if (s < size)
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The node <" + node.name() + "> has the wrong number of children", std::to_string(s), std::to_string(size));
        }
    }

    void util::EnforceChildSizeGreater(const RapidXmlReaderNode& node, const size_t size)
    {
        const size_t s = node.nodes().size();
        if (s <= size)
        {
            throw error::ValueNotValidException(__PRETTY_FUNCTION__, "The node <" + node.name() + "> has the wrong number of children", std::to_string(s), std::to_string(size));
        }
    }

}
