/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <memory>
#include <filesystem>

// ArmarX
#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/aron/codegeneration/codegenerator/helper/ReaderInfo.h>
#include <RobotAPI/libraries/aron/codegeneration/codegenerator/helper/WriterInfo.h>
#include <RobotAPI/libraries/aron/codegeneration/typereader/helper/GenerateTypeInfo.h>
#include <RobotAPI/libraries/aron/codegeneration/typereader/helper/GenerateIntEnumInfo.h>

namespace armarx::aron::typereader
{
    /**
     * @brief The basic reader class, defining methods for reading an aron description file and returning an aron type object, representing the description
     * It contains basic functions, to read in arbitrary files and to store the type generation information
     */
    template <typename Input>
    class Reader
    {
    public:
        Reader() = default;

        /// parse a filename
        virtual void parseFile(const std::string& filename, const std::vector<std::filesystem::path>& includePaths) = 0;

        /// path a file given by std::filesystem
        virtual void parseFile(const std::filesystem::path& file, const std::vector<std::filesystem::path>& includePaths) = 0;

        std::vector<std::string> getCodeIncludes() const
        {
            return systemIncludes;
        }
        std::vector<std::string> getAronIncludes() const
        {
            return aronIncludes;
        }
        std::vector<codegenerator::WriterInfo> getWriters() const
        {
            return writers;
        }
        std::vector<codegenerator::ReaderInfo> getReaders() const
        {
            return readers;
        }
        std::vector<typereader::GenerateObjectInfo> getGenerateObjects() const
        {
            return generateObjects;
        }
        std::vector<typereader::GenerateIntEnumInfo> getGenerateIntEnums() const
        {
            return generateIntEnums;
        }

    protected:
        std::vector<std::string> alreadyParsedXMLFiles;

        std::vector<std::string> systemIncludes;
        std::vector<std::string> aronIncludes;

        std::vector<codegenerator::ReaderInfo> readers;
        std::vector<codegenerator::WriterInfo> writers;

        std::vector<typereader::GenerateIntEnumInfo> generateIntEnums;
        std::vector<typereader::GenerateObjectInfo> generateObjects;
    };
}
