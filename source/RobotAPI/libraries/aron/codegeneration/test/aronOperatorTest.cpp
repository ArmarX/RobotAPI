/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::aron
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::aron

#define ARMARX_BOOST_TEST

// STD/STL
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <numeric>

// Test
#include <RobotAPI/Test.h>

// ArmarX
#include <ArmarXCore/libraries/cppgen/CppMethod.h>
#include <ArmarXCore/libraries/cppgen/CppClass.h>
#include <RobotAPI/libraries/aron/core/Exception.h>

// Aron
#include <RobotAPI/libraries/aron/core/data/variant/All.h>

using namespace armarx;
using namespace aron;

BOOST_AUTO_TEST_CASE(AronAssignmentTest)
{
    std::cout << "Aron assignment test" << std::endl;
    float f = 5.0;
    double d = 42.0;
    int i = 666;
    long l = 123;
    std::string s = "Hello World";
    bool b = true;

    aron::data::Float fn = f;
    aron::data::Double dn = d;
    aron::data::Int in = i;
    aron::data::Long ln = l;
    aron::data::String sn = s;
    aron::data::Bool bn = b;

    BOOST_CHECK_EQUAL((float) fn == f, true);
    BOOST_CHECK_EQUAL((double) dn == d, true);
    BOOST_CHECK_EQUAL((int) in == i, true);
    BOOST_CHECK_EQUAL((long) ln == l, true);
    BOOST_CHECK_EQUAL((std::string) sn == s, true);
    BOOST_CHECK_EQUAL((bool) bn == b, true);
}

BOOST_AUTO_TEST_CASE(AronEqualsTest)
{
    std::cout << "Aron equals test" << std::endl;
    aron::data::Float fn = 5.0;
    aron::data::Float fn_equals = 5.0;
    aron::data::Float fn_unequals = 6.0;

    BOOST_CHECK_EQUAL(fn == fn_equals, true);
    BOOST_CHECK_EQUAL(fn == fn_unequals, false);

    auto fn_ptr_equals = std::make_shared<aron::data::Float>(5.0);
    auto fn_ptr_unequals = std::make_shared<aron::data::Float>(42.0);

    BOOST_CHECK_EQUAL(fn == fn_ptr_equals, true);
    BOOST_CHECK_EQUAL(fn == fn_ptr_unequals, false);
}

// TODO more tests (fabian.peller)

