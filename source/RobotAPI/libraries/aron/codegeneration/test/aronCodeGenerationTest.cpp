/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::aron
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::aron

#define ARMARX_BOOST_TEST

// STD/STL
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <numeric>

// Test
#include <RobotAPI/Test.h>

// ArmarX
#include <ArmarXCore/libraries/cppgen/CppMethod.h>
#include <ArmarXCore/libraries/cppgen/CppClass.h>
#include <RobotAPI/libraries/aron/core/Exception.h>

// Generated File
#include <RobotAPI/libraries/aron/codegeneration/test/aron/TemplateTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/AnyTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/ListTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/DictTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/PrimitiveTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/ObjectTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/ImageTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/MatrixTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/QuaternionTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/PointCloudTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/PositionTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/OrientationTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/PoseTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/EnumTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/OptionalTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/DtoTest.aron.generated.h>

using namespace armarx;
using namespace aron;


namespace std
{
    ostream& operator<<(ostream& os, const type_info& i)
    {
        return os << "'" << i.name() << "'";
    }
}


BOOST_AUTO_TEST_CASE(AronCodeGenerationAnyTest)
{
    std::cout << "Running Code Gen Any test" << std::endl;
    AnyTest p;
    p.any_object = std::make_shared<aron::data::Dict>(aron::Path({"any_object"}));
    p.any_object->addElement("hello_world", std::make_shared<aron::data::Int>(5));
    BOOST_CHECK_EQUAL(p.any_object->childrenSize(), 1);

    auto int_var = aron::data::Int::DynamicCastAndCheck(p.any_object->getElement("hello_world"));
    BOOST_CHECK_EQUAL(int_var->getValue(), 5);

    auto aron = p.toAron();
    auto any_object_member = aron->getElement("any_object");
    auto any_object_member_casted = aron::data::Dict::DynamicCastAndCheck(any_object_member);
    auto any_object_int_var = aron::data::Int::DynamicCastAndCheck(any_object_member_casted->getElement("hello_world"));
    BOOST_CHECK_EQUAL(any_object_int_var->getValue(), 5);

    AnyTest p2;
    p2.fromAron(aron);
    auto int_var2 = aron::data::Int::DynamicCastAndCheck(p2.any_object->getElement("hello_world"));
    BOOST_CHECK_EQUAL(int_var2->getValue(), 5);
}


BOOST_AUTO_TEST_CASE(AronCodeGenerationTemplateTest)
{
    std::cout << "Running Code Gen Tmpl test" << std::endl;
    TemplateTest2<WithoutTemplateTest, WithoutTemplateTest> t;
    t.da_bar.da_string = "da_bar";
    t.da_foo.da_string = "da_foo";
    t.da_tmpl.da_blarg.da_string = "da_tmpl.da_blarg";
    t.da_int = 1234;

    auto aron = t.toAron();
    auto t_dabar = aron::data::Dict::DynamicCastAndCheck(aron->getElement("da_bar"));
    auto t_dafoo = aron::data::Dict::DynamicCastAndCheck(aron->getElement("da_foo"));
    auto t_daimpl = aron::data::Dict::DynamicCastAndCheck(aron->getElement("da_tmpl"));
    auto t_daint = aron::data::Int::DynamicCastAndCheck(aron->getElement("da_int"));
    BOOST_CHECK_EQUAL(t_daint->getValue(), 1234);

    auto t_dabar_dastring = aron::data::String::DynamicCastAndCheck(t_dabar->getElement("da_string"));
    BOOST_CHECK_EQUAL(t_dabar_dastring->getValue(), "da_bar");
    auto t_dafoo_dastring = aron::data::String::DynamicCastAndCheck(t_dafoo->getElement("da_string"));
    BOOST_CHECK_EQUAL(t_dafoo_dastring->getValue(), "da_foo");
    auto t_daimpl_dablarg = aron::data::Dict::DynamicCastAndCheck(t_daimpl->getElement("da_blarg"));

    auto t_daimpl_dablarg_dastring = aron::data::String::DynamicCastAndCheck(t_daimpl_dablarg->getElement("da_string"));
    BOOST_CHECK_EQUAL(t_daimpl_dablarg_dastring->getValue(), "da_tmpl.da_blarg");

    auto aronT = TemplateTest2<WithoutTemplateTest, WithoutTemplateTest>::ToAronType();
    BOOST_CHECK_EQUAL(aronT->getObjectName(), "armarx::TemplateTest2");
    BOOST_CHECK_EQUAL(aronT->getTemplates(), std::vector<std::string>({"Foo", "Bar"}));
    BOOST_CHECK_EQUAL(aronT->getTemplateInstantiations(), std::vector<std::string>{});


    auto tt_dabar = aron::type::Object::DynamicCastAndCheck(aronT->getMemberType("da_bar"));
    auto tt_dafoo = aron::type::Object::DynamicCastAndCheck(aronT->getMemberType("da_foo"));
    auto tt_datmpl = aron::type::Object::DynamicCastAndCheck(aronT->getMemberType("da_tmpl"));
    BOOST_CHECK_EQUAL(tt_dabar->getObjectName(), "armarx::WithoutTemplateTest");
    BOOST_CHECK_EQUAL(tt_dafoo->getObjectName(), "armarx::WithoutTemplateTest");
    BOOST_CHECK_EQUAL(tt_datmpl->getObjectName(), "armarx::TemplateTest1");

    BOOST_CHECK_EQUAL(tt_dabar->getTemplates(), std::vector<std::string>{});
    BOOST_CHECK_EQUAL(tt_dafoo->getTemplates(), std::vector<std::string>{});
    BOOST_CHECK_EQUAL(tt_datmpl->getTemplates(), std::vector<std::string>({"Blarg"}));

    BOOST_CHECK_EQUAL(tt_dabar->getTemplateInstantiations(), std::vector<std::string>{});
    BOOST_CHECK_EQUAL(tt_dafoo->getTemplateInstantiations(), std::vector<std::string>{});
    BOOST_CHECK_EQUAL(tt_datmpl->getTemplateInstantiations(), std::vector<std::string>{"armarx::WithoutTemplateTest"});

    auto tt_daimpl_dablarg =  aron::type::Object::DynamicCastAndCheck(tt_datmpl->getMemberType("da_blarg"));
    BOOST_CHECK_EQUAL(tt_daimpl_dablarg->getObjectName(), "armarx::WithoutTemplateTest");
    BOOST_CHECK_EQUAL(tt_daimpl_dablarg->getTemplates(), std::vector<std::string>{});
    BOOST_CHECK_EQUAL(tt_daimpl_dablarg->getTemplateInstantiations(), std::vector<std::string>{});
}


BOOST_AUTO_TEST_CASE(AronCodeGenerationListTest)
{
    std::cout << "Running Code Gen List test" << std::endl;
    ListTest p_tmp;
    ListTest p = p_tmp; // test assignment
    BOOST_CHECK(p_tmp == p);

    BOOST_CHECK_EQUAL(typeid(p.intList), typeid(std::vector<int>));
    BOOST_CHECK_EQUAL(typeid(p.longList), typeid(std::vector<long>));
    BOOST_CHECK_EQUAL(typeid(p.floatList), typeid(std::vector<float>));
    BOOST_CHECK_EQUAL(typeid(p.doubleList), typeid(std::vector<double>));
    BOOST_CHECK_EQUAL(typeid(p.stringList), typeid(std::vector<std::string>));
    BOOST_CHECK_EQUAL(typeid(p.boolList), typeid(std::vector<bool>));
    //BOOST_CHECK_EQUAL(typeid(p.objectList), typeid(std::vector<ListTest::ListClass>));

    BOOST_CHECK_EQUAL(p.intList.size(), 0);
    BOOST_CHECK_EQUAL(p.longList.size(), 0);
    BOOST_CHECK_EQUAL(p.floatList.size(), 0);
    BOOST_CHECK_EQUAL(p.doubleList.size(), 0);
    BOOST_CHECK_EQUAL(p.stringList.size(), 0);
    BOOST_CHECK_EQUAL(p.boolList.size(), 0);
    BOOST_CHECK_EQUAL(p.objectList.size(), 0);
}


BOOST_AUTO_TEST_CASE(AronCodeGenerationDictTest)
{
    std::cout << "Running Code Gen Dict test" << std::endl;
    DictTest p_tmp;
    DictTest p = p_tmp; // test assignment
    BOOST_CHECK(p_tmp == p);

    BOOST_CHECK_EQUAL(p.dict.size(), 0);
}


BOOST_AUTO_TEST_CASE(AronCodeGenerationEigenMatrixTest)
{
    std::cout << "Running Code Gen EigenMatrix test" << std::endl;
    EigenMatrixTest p_tmp;
    EigenMatrixTest p = p_tmp; // test assignment
    BOOST_CHECK(p_tmp == p);

    BOOST_CHECK_EQUAL(p.the_short_eigen_matrix.rows(), 5);
    BOOST_CHECK_EQUAL(p.the_short_eigen_matrix.cols(), 7);
    BOOST_CHECK_EQUAL(p.the_int_eigen_matrix.rows(), 7);
    BOOST_CHECK_EQUAL(p.the_int_eigen_matrix.cols(), 7);
    BOOST_CHECK_EQUAL(p.the_long_eigen_matrix.rows(), 7);
    BOOST_CHECK_EQUAL(p.the_long_eigen_matrix.cols(), 5);
    BOOST_CHECK_EQUAL(p.the_float_eigen_matrix.rows(), 1);
    BOOST_CHECK_EQUAL(p.the_float_eigen_matrix.cols(), 9);
    BOOST_CHECK_EQUAL(p.the_double_eigen_matrix.rows(), 5);
    BOOST_CHECK_EQUAL(p.the_double_eigen_matrix.cols(), 1);
}


BOOST_AUTO_TEST_CASE(AronCodeGenerationEigenQuaternionTest)
{
    std::cout << "Running Code Gen EigenQuaternion test" << std::endl;
    EigenQuaternionTest p_tmp;
    EigenQuaternionTest p = p_tmp; // test assignment
    BOOST_CHECK(p_tmp == p);

    //BOOST_CHECK_EQUAL(p.the_double_eigen_matrix.w(), 0);
    //BOOST_CHECK_EQUAL(p.the_double_eigen_matrix.x(), 0);
    //BOOST_CHECK_EQUAL(p.the_double_eigen_matrix.y(), 0);
    //BOOST_CHECK_EQUAL(p.the_double_eigen_matrix.z(), 0);
}


BOOST_AUTO_TEST_CASE(AronCodeGenerationEigenPositionTest)
{
    std::cout << "Running Code Gen EigenPosition test" << std::endl;
    PositionTest p_tmp;
    PositionTest p = p_tmp; // test assignment
    BOOST_CHECK(p_tmp == p);

    BOOST_CHECK_EQUAL(p.position.rows(), 3);
    BOOST_CHECK_EQUAL(p.position.cols(), 1);
}


BOOST_AUTO_TEST_CASE(AronCodeGenerationEigenPoseTest)
{
    std::cout << "Running Code Gen EigenPose test" << std::endl;
    PoseTest p_tmp;
    PoseTest p = p_tmp; // test assignment
    BOOST_CHECK(p_tmp == p);

    BOOST_CHECK_EQUAL(p.pose.rows(), 4);
    BOOST_CHECK_EQUAL(p.pose.cols(), 4);
}


BOOST_AUTO_TEST_CASE(AronCodeGenerationIntEnumTest)
{
    std::cout << "Running Code Gen IntEnum test" << std::endl;
    TheObjectThatUsesTheIntEnum p_tmp;
    TheObjectThatUsesTheIntEnum p = p_tmp; // test assignment
    BOOST_CHECK(p_tmp == p);

    BOOST_CHECK(p.the_int_enum.value == TheIntEnum::INT_ENUM_VALUE_0);
}


BOOST_AUTO_TEST_CASE(AronCodeGenerationPrimitiveTest)
{
    std::cout << "Running Code Gen Primitive test" << std::endl;
    PrimitiveTest p_tmp;
    PrimitiveTest p = p_tmp; // test assignment
    BOOST_CHECK(p_tmp == p);

    BOOST_CHECK_EQUAL(p.the_bool, false);
    BOOST_CHECK_EQUAL(p.the_double, 0.0);
    BOOST_CHECK_EQUAL(p.the_float, 0.0);
    BOOST_CHECK_EQUAL(p.the_int, 0);
    BOOST_CHECK_EQUAL(p.the_long, 0);
    BOOST_CHECK_EQUAL(p.the_string, "");
}


BOOST_AUTO_TEST_CASE(AronCodeGenerationOptionalTest)
{
    std::cout << "Running Code Gen Optional test" << std::endl;
    OptionalTest p_tmp;
    OptionalTest p = p_tmp; // test assignment
    BOOST_CHECK(p_tmp == p);

    BOOST_CHECK_EQUAL(typeid(p.some_dict), typeid(std::optional<std::map<std::string, float>>));
    BOOST_CHECK_EQUAL(typeid(p.some_dict_with_optional_type), typeid(std::map<std::string, std::optional<float>>));
    BOOST_CHECK_EQUAL(typeid(p.some_eigen_matrix), typeid(std::optional<Eigen::Matrix<long, 25, 10>>));
    BOOST_CHECK_EQUAL(typeid(p.some_float), typeid(std::optional<float>));
    BOOST_CHECK_EQUAL(typeid(p.some_list), typeid(std::optional<std::vector<double>>));
    BOOST_CHECK_EQUAL(typeid(p.some_list_with_optional_list), typeid(std::optional<std::vector<std::optional<std::vector<std::optional<float>>>>>));
    BOOST_CHECK_EQUAL(typeid(p.some_list_with_optional_type), typeid(std::vector<std::optional<double>>));
    BOOST_CHECK_EQUAL(typeid(p.some_obj), typeid(std::optional<armarx::OptionalTestElement>));
    BOOST_CHECK_EQUAL(typeid(p.some_string), typeid(std::optional<std::string>));

    auto aronType = p.ToAronType();
    BOOST_CHECK_EQUAL(aronType->getMemberType("some_float")->getMaybe(), aron::type::Maybe::OPTIONAL);

    BOOST_CHECK(not p.some_float.has_value());
    auto aron = p.toAron();
    BOOST_CHECK(aron->getElement("some_float") == nullptr);

    p.some_float = 5.0f;
    BOOST_CHECK(p.some_float.has_value());
    aron = p.toAron();
    BOOST_CHECK(aron->getElement("some_float"));
    BOOST_CHECK(*aron->getElement("some_float") == armarx::aron::data::Float(5.0f));
}
