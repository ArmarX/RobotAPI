/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::aron
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::aron

#define ARMARX_BOOST_TEST


// Generated File - include them first to check whether their includes are complete.
#include <RobotAPI/libraries/aron/core/test/aron/ListTest.aron.generated.h>
#include <RobotAPI/libraries/aron/core/test/aron/DictTest.aron.generated.h>
#include <RobotAPI/libraries/aron/core/test/aron/PrimitiveTest.aron.generated.h>
#include <RobotAPI/libraries/aron/core/test/aron/ObjectTest.aron.generated.h>
#include <RobotAPI/libraries/aron/core/test/aron/ImageTest.aron.generated.h>
#include <RobotAPI/libraries/aron/core/test/aron/MatrixTest.aron.generated.h>
#include <RobotAPI/libraries/aron/core/test/aron/QuaternionTest.aron.generated.h>
#include <RobotAPI/libraries/aron/core/test/aron/PointCloudTest.aron.generated.h>
#include <RobotAPI/libraries/aron/core/test/aron/PositionTest.aron.generated.h>
#include <RobotAPI/libraries/aron/core/test/aron/OrientationTest.aron.generated.h>
#include <RobotAPI/libraries/aron/core/test/aron/PoseTest.aron.generated.h>
#include <RobotAPI/libraries/aron/core/test/aron/EnumTest.aron.generated.h>
#include <RobotAPI/libraries/aron/core/test/aron/OptionalTest.aron.generated.h>

// Aron
#include <RobotAPI/libraries/aron/core/test/Randomizer.h>

#include <RobotAPI/libraries/aron/core/data/rw/reader/variant/VariantReader.h>
#include <RobotAPI/libraries/aron/core/data/rw/reader/nlohmannJSON/NlohmannJSONReader.h>

#include <RobotAPI/libraries/aron/core/data/rw/writer/variant/VariantWriter.h>
#include <RobotAPI/libraries/aron/core/data/rw/writer/nlohmannJSON/NlohmannJSONWriter.h>

#include <RobotAPI/libraries/aron/core/type/rw/reader/variant/VariantReader.h>
#include <RobotAPI/libraries/aron/core/type/rw/reader/nlohmannJSON/NlohmannJSONReader.h>

#include <RobotAPI/libraries/aron/core/type/rw/writer/variant/VariantWriter.h>
#include <RobotAPI/libraries/aron/core/type/rw/writer/nlohmannJSON/NlohmannJSONWriter.h>

#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

// ArmarX
#include <ArmarXCore/libraries/cppgen/CppMethod.h>
#include <ArmarXCore/libraries/cppgen/CppClass.h>
#include <RobotAPI/libraries/aron/core/Exception.h>
#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>


// IVT
#include <Image/ByteImage.h>

// Eigen
#include <Eigen/Core>

// OpenCV
#include <opencv2/core/core.hpp>

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


// STD/STL
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <numeric>


// Test
#include <RobotAPI/Test.h>


// Files without code generation
//#include "aronDataWithoutCodeGeneration.h"


using namespace armarx;
using namespace aron;


template <typename T>
void test_ToAronType(T& in, T& out)
{
    nlohmann::json in_type_json;
    nlohmann::json out_type_json;

    BOOST_TEST_CONTEXT("getting in type")
    {
        type::ObjectPtr in_type_nav = in.ToAronType();

        type::dto::AronObjectPtr in_type = in_type_nav->toAronObjectDTO();
        BOOST_CHECK(in_type);

        armarx::aron::type::writer::NlohmannJSONWriter json_writer_in;
        in_type_json = in.writeType(json_writer_in);

        BOOST_CHECK(in_type_json.is_object());
    }

    BOOST_TEST_CONTEXT("getting out type")
    {
        type::ObjectPtr out_type_nav = out.ToAronType();

        type::dto::AronObjectPtr out_type = out_type_nav->toAronObjectDTO();
        BOOST_CHECK(out_type);

        armarx::aron::type::writer::NlohmannJSONWriter json_writer_out;
        out_type_json = out.writeType(json_writer_out);
        BOOST_CHECK(out_type_json.is_object());
    }

    BOOST_CHECK_EQUAL(out_type_json.dump(2), in_type_json.dump(2));
}




template <typename T>
void test_toAron(T& in, T& out)
{
    Randomizer r;

    aron::data::DictPtr in_aron_nav = in.toAron();
    BOOST_TEST_CONTEXT("initialize in aron randomly")
    {
        type::ObjectPtr in_type_nav = in.ToAronType();
        r.initializeRandomly(in_aron_nav, *in_type_nav);
    }

    BOOST_TEST_INFO("getting in aron");
    aron::data::dto::DictPtr in_aron = in_aron_nav->toAronDictDTO();
    BOOST_CHECK(in_aron);

    //BOOST_TEST_MESSAGE("in_aron_nav: \n" << converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(in_aron_nav).dump(2));
    //BOOST_TEST_MESSAGE("out.toAron(): \n" << converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(out.toAron()).dump(2));

    aron::data::DictPtr out_aron_nav;
    BOOST_TEST_CONTEXT("setting out aron from in aron")
    {
        out.fromAron(in_aron_nav);

        BOOST_TEST_INFO("getting out aron");
        out_aron_nav = out.toAron();
        BOOST_CHECK(*in_aron_nav == *out_aron_nav);

        aron::data::dto::DictPtr out_aron = out_aron_nav->toAronDictDTO();
        BOOST_CHECK(out_aron);
    }

    BOOST_TEST_CONTEXT("setting in aron from out aron and check for equality")
    {
        in.fromAron(out_aron_nav);
        BOOST_CHECK(in == out);

        aron::data::DictPtr in_aron_nav_again = in.toAron();
        BOOST_TEST_MESSAGE("in_aron_nav: \n" << converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(in_aron_nav).dump(2));
        BOOST_TEST_MESSAGE("in_aron_nav_again: \n" << converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(in_aron_nav_again).dump(2));

        BOOST_CHECK(*in_aron_nav == *in_aron_nav_again);
    }
}

#if 0
template <typename T>
void test_toJson(T& in, T& out)
{
    Randomizer r;

    data::DictPtr in_aron_nav = in.toAron();
    {
        type::ObjectPtr in_type_nav = in.ToAronType();
        r.initializeRandomly(in_aron_nav, *in_type_nav);
    }

    data::dto::DictPtr in_aron = in_aron_nav->toAronDictPtr();
    in.fromAron(in_aron_nav);

    nlohmann::json in_aron_json;
    std::string in_aron_json_str;
    BOOST_TEST_CONTEXT("check JSON export of in and visitor implementation for equality")
    {
        in_aron_json = in.toJSON();
        in_aron_json_str = in_aron_json.dump(4);

        nlohmann::json direct_in_aron_json = converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(in_aron_nav);
        std::string direct_in_aron_json_str = direct_in_aron_json.dump(4);

        BOOST_TEST_MESSAGE("in toJSON: \n" << in_aron_json_str);
        BOOST_TEST_MESSAGE("in JSON Converter: \n" << direct_in_aron_json_str);
        BOOST_CHECK(in_aron_json_str == direct_in_aron_json_str);
    }

    nlohmann::json out_aron_json;
    std::string out_aron_json_str;
    BOOST_TEST_CONTEXT("setting out from in json and check JSON export of out and visitor implementation for equality")
    {
        out.fromJSON(in_aron_json);
        data::DictPtr out_aron_nav = out.toAron();
        out_aron_json = out.toJSON();
        out_aron_json_str = out_aron_json.dump(4);

        nlohmann::json direct_out_aron_json = converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(out_aron_nav);
        std::string direct_out_aron_json_str = direct_out_aron_json.dump(4);

        BOOST_TEST_MESSAGE("out toJSON: \n" << out_aron_json_str);
        BOOST_TEST_MESSAGE("out JSON Converter: \n" << direct_out_aron_json_str);
    }


    BOOST_TEST_CONTEXT("checking in and out json for equality")
    {
        BOOST_TEST_MESSAGE("in_aron_json_str: \n" << in_aron_json_str);
        BOOST_TEST_MESSAGE("out_aron_json_str: \n" << out_aron_json_str);
        BOOST_CHECK(in_aron_json_str == out_aron_json_str);
    }
}
#endif


template <typename T>
void runTestWithInstances(T& in, T& out)
{
    // assumes not nullptrs as in and out. If you have a maybe type then make sure that it is set properly
    test_ToAronType(in, out);
    test_toAron(in, out);
#if 0
    test_toJson(in, out);
#endif
}


BOOST_AUTO_TEST_CASE(test_List)
{
    BOOST_TEST_MESSAGE("Running List test");
    ListTest l;
    ListTest l2;
    runTestWithInstances<ListTest>(l, l2);
}

BOOST_AUTO_TEST_CASE(test_Dict)
{
    BOOST_TEST_MESSAGE("Running Dict test");
    DictTest d;
    DictTest d2;
    runTestWithInstances<DictTest>(d, d2);
}

BOOST_AUTO_TEST_CASE(test_Primitive)
{
    BOOST_TEST_MESSAGE("Running Primitive test");
    PrimitiveTest p;
    PrimitiveTest p2;
    runTestWithInstances<PrimitiveTest>(p, p2);
}

BOOST_AUTO_TEST_CASE(test_Object)
{
    BOOST_TEST_MESSAGE("Running Object test");
    ObjectTest1 o1;
    ObjectTest1 o12;
    runTestWithInstances<ObjectTest1>(o1, o12);

    ObjectTest2 o2;
    ObjectTest2 o22;
    runTestWithInstances<ObjectTest2>(o2, o22);
}

BOOST_AUTO_TEST_CASE(test_EigenMatrix)
{
    // Eigen may cause problems with equality checks. Sometimes even with dimension ~25
    BOOST_TEST_MESSAGE("Running EigenMatrix test");
    EigenMatrixTest em;
    EigenMatrixTest em2;
    runTestWithInstances<EigenMatrixTest>(em, em2);
}

BOOST_AUTO_TEST_CASE(test_Image)
{
    BOOST_TEST_MESSAGE("Running Image test");
    ImageTest image;
    ImageTest image2;

    image.the_rgb24_image.create(3, 4, image.the_rgb24_image.type());
    image.the_depth32_image.create(3, 4, image.the_depth32_image.type());

    // Could be used for ndarray test
    //image.the_2d_opencv_matrix = cv::Mat(2, 2, CV_64F);
    //image.the_3d_opencv_matrix = cv::Mat(std::vector<int>({2, 2, 2}), CV_32F);
    //image.the_4d_opencv_matrix = cv::Mat(std::vector<int>({2, 2, 2, 2}), CV_16S);
    //image.the_5d_opencv_matrix = cv::Mat(std::vector<int>({2, 2, 2, 2, 2}), CV_8U);

    runTestWithInstances<ImageTest>(image, image2);
}

BOOST_AUTO_TEST_CASE(test_PointCloud)
{
    BOOST_TEST_MESSAGE("Running PointCloud test");
    PointCloudTest pc;
    PointCloudTest pc2;

    pc.the_xyzrgb_pcl_pointcloud = pcl::PointCloud<pcl::PointXYZRGB>(5, 5);
    pc.the_xyzrgba_pcl_pointcloud = pcl::PointCloud<pcl::PointXYZRGBA>(7, 7);

    pc2.the_xyzrgb_pcl_pointcloud = pcl::PointCloud<pcl::PointXYZRGB>(4, 4);
    pc2.the_xyzrgba_pcl_pointcloud = pcl::PointCloud<pcl::PointXYZRGBA>(3, 3);

    runTestWithInstances<PointCloudTest>(pc, pc2);
}

BOOST_AUTO_TEST_CASE(test_EigenQuaternion)
{
    BOOST_TEST_MESSAGE("Running EigenQuaternion test");
    EigenQuaternionTest eq;
    EigenQuaternionTest eq2;
    runTestWithInstances<EigenQuaternionTest>(eq, eq2);
}

BOOST_AUTO_TEST_CASE(test_Position)
{
    BOOST_TEST_MESSAGE("Running Position test");
    PositionTest pc;
    PositionTest pc2;
    runTestWithInstances<PositionTest>(pc, pc2);
}

BOOST_AUTO_TEST_CASE(test_Orientation)
{
    BOOST_TEST_MESSAGE("Running Orientation test");
    OrientationTest pc;
    OrientationTest pc2;
    runTestWithInstances<OrientationTest>(pc, pc2);
}

BOOST_AUTO_TEST_CASE(test_Pose)
{
    BOOST_TEST_MESSAGE("Running Pose test");
    PoseTest pc;
    PoseTest pc2;
    runTestWithInstances<PoseTest>(pc, pc2);
}

BOOST_AUTO_TEST_CASE(test_Optional)
{
    BOOST_TEST_MESSAGE("Running Optional test");
    OptionalTest pc;
    OptionalTest pc2;
    runTestWithInstances<OptionalTest>(pc, pc2);
}


BOOST_AUTO_TEST_CASE(test_Enum)
{
    BOOST_TEST_MESSAGE("Running Optional test");
    TheIntEnum value = TheIntEnum::INT_ENUM_VALUE_2;
    BOOST_CHECK_EQUAL(value.toString(), "INT_ENUM_VALUE_2");
}
