/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::aron
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::aron

#define ARMARX_BOOST_TEST

// STD/STL
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <numeric>

// Test
#include <RobotAPI/Test.h>

// ArmarX
#include <ArmarXCore/libraries/cppgen/CppMethod.h>
#include <ArmarXCore/libraries/cppgen/CppClass.h>
#include <RobotAPI/libraries/aron/core/Exception.h>

// Generated File
#include <RobotAPI/libraries/aron/codegeneration/test/aron/NaturalIKTest.aron.generated.h>

using namespace armarx;
using namespace aron;

BOOST_AUTO_TEST_CASE(AronNavigateTest)
{
    std::cout << "Running navigate test" << std::endl;
    NaturalIKResult k;
    NaturalIKResult k2;

    // test Path
    data::DictPtr aron = k.toAron();
    Path path = aron->getPath(); // should be empty since aron is top level object
    Path memberReached = path.withElement("reached");
    Path memberJointValues = path.withElement("jointValues");
    Path indexJointValues0 = memberJointValues.withElement("0");
    Path indexJointValues1 = memberJointValues.withElement("1");

    std::string root = path.getRootIdentifier();

    BOOST_CHECK_EQUAL(path.toString(), root);
    BOOST_CHECK_EQUAL(path.size(), 0);

    BOOST_CHECK_EQUAL(memberReached.toString(), root+"->reached");
    BOOST_CHECK_EQUAL(memberReached.size(), 1);

    BOOST_CHECK_EQUAL(memberJointValues.toString(), root+"->jointValues");
    BOOST_CHECK_EQUAL(memberJointValues.size(), 1);

    BOOST_CHECK_EQUAL(indexJointValues0.toString(), root+"->jointValues->0");
    BOOST_CHECK_EQUAL(indexJointValues0.size(), 2);

    BOOST_CHECK_EQUAL(indexJointValues1.toString(), root+"->jointValues->1");
    BOOST_CHECK_EQUAL(indexJointValues1.size(), 2);

    data::BoolPtr reached = data::Bool::DynamicCastAndCheck(aron->navigateAbsolute(memberReached));
    data::ListPtr jointValues = data::List::DynamicCastAndCheck(aron->navigateAbsolute(memberJointValues));

    if (jointValues->childrenSize() > 0)
    {
        data::FloatPtr el = data::Float::DynamicCastAndCheck(aron->navigateAbsolute(indexJointValues0));
    }
    if (jointValues->childrenSize() > 1)
    {
        data::FloatPtr el = data::Float::DynamicCastAndCheck(aron->navigateAbsolute(indexJointValues1));
    }

    Path diff = indexJointValues1.getWithoutPrefix(indexJointValues0);
    BOOST_CHECK_EQUAL(diff.toString(), root+"->1");
    BOOST_CHECK_EQUAL(diff.size(), 1);
}
