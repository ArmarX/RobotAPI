/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::aron
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::aron

#define ARMARX_BOOST_TEST

// STD/STL
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <numeric>

// Test
#include <RobotAPI/Test.h>

// ArmarX
#include <ArmarXCore/libraries/cppgen/CppMethod.h>
#include <ArmarXCore/libraries/cppgen/CppClass.h>
#include <RobotAPI/libraries/aron/core/Exception.h>

// Aron
#include <RobotAPI/libraries/aron/core/data/variant/All.h>

// Generated File
#include <RobotAPI/libraries/aron/codegeneration/test/aron/BaseClassTest.aron.generated.h>
#include <RobotAPI/libraries/aron/codegeneration/test/aron/DerivedClassTest.aron.generated.h>

using namespace armarx;
using namespace aron;

BOOST_AUTO_TEST_CASE(AronExtendsTest)
{
    std::cout << "Aron extends test" << std::endl;
    BaseClassTest base;
    auto baseType = base.ToAronType();

    DerivedClassTest derived;
    auto derivedType = derived.ToAronType();

    for (const auto& [key, value] : baseType->getMemberTypes())
    {
        BOOST_CHECK_EQUAL(derivedType->hasMemberType(key), true);
        //BOOST_CHECK_EQUAL(value == derivedType->getMemberType(key), true);
    }

    derived.base_class_member1 = false;
    derived.base_class_member2 = {1.0, 2.5, 3.8};
    derived.base_class_member3.inner_class_member = true;

    auto derivedAron = derived.toAron();
    //BOOST_CHECK_EQUAL(derivedAron->fullfillsType(baseType), true);

    BaseClassTest& casted = static_cast<BaseClassTest&>(derived);
    BOOST_CHECK_EQUAL(casted.base_class_member1 == derived.base_class_member1, true);
    BOOST_CHECK_EQUAL(casted.base_class_member2 == derived.base_class_member2, true);
    BOOST_CHECK_EQUAL(casted.base_class_member3.inner_class_member == derived.base_class_member3.inner_class_member, true);

    DerivedClassTest& casted_back = dynamic_cast<DerivedClassTest&>(casted);
    BOOST_CHECK_EQUAL(casted_back == derived, true);
}

