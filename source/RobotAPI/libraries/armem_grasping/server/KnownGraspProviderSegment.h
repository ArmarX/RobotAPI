# pragma once

#include <RobotAPI/libraries/armem/server/segment/SpecializedSegment.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>
#include <RobotAPI/libraries/armem_grasping/aron/KnownGraspCandidate.aron.generated.h>

namespace armarx::armem::grasping::segment
{
    class KnownGraspProviderSegment : public armem::server::segment::SpecializedProviderSegment
    {
        using Base = armem::server::segment::SpecializedProviderSegment;

    public:
        KnownGraspProviderSegment(armem::server::MemoryToIceAdapter& iceMemory);

        void init() override;

    private:
        void loadMemory();
        std::optional<arondto::KnownGraspInfo> knownGraspInfoFromObjectInfo(const ObjectInfo&);

    public:
        static const constexpr char* CORE_SEGMENT_NAME = "KnownGraspCandidate";
        static const constexpr char* PROVIDER_SEGMENT_NAME = "PriorKnowledgeData";
    };
}
