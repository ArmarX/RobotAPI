/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem_grasping
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


namespace armarx
{
    /**
    * @defgroup Library-armem_grasping armem_grasping
    * @ingroup RobotAPI
    *
    * A library redirecting to GraspingUtility to make the ArMem-related
    * classes there easier to find.
    *
    * @class armem_grasping
    * @ingroup Library-armem_grasping
    * @brief Brief description of class armem_grasping.
    *
    * Detailed description of class armem_grasping.
    */
    class armem_grasping
    {
    public:

    };

}
