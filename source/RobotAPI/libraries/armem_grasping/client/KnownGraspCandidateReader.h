/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <optional>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Reader.h>

#include <RobotAPI/libraries/armem_grasping/aron/KnownGraspCandidate.aron.generated.h>


namespace armarx::armem::grasping::known_grasps
{
    class Reader
    {
    public:
        Reader(armem::client::MemoryNameSystem& memoryNameSystem);
        virtual ~Reader() = default;

        void registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def);
        void connect();

        std::optional<armem::grasping::arondto::KnownGraspInfo> queryKnownGraspInfoByEntityName(const std::string&, const armem::Time&);
        std::optional<armem::grasping::arondto::KnownGraspInfo> queryKnownGraspInfo(const armem::wm::Memory& memory, const armem::Time&);

    private:

        struct Properties
        {
            std::string memoryName                  = "Grasp";
            std::string coreSegmentName             = "KnownGraspCandidate";
        } properties;

        const std::string propertyPrefix = "mem.grasping.knowngrasps.";

        armem::client::MemoryNameSystem& memoryNameSystem;
        armem::client::Reader memoryReader;
        mutable std::mutex memoryWriterMutex;
    };


}  // namespace armarx::armem::attachment
