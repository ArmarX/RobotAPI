#pragma once

// STD/STL
#include <string>
#include <filesystem>
#include <optional>

// Base Class
#include <ArmarXCore/core/logging/Logging.h>

// ArmarX
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::priorknowledge::core
{

    struct PackageFileLocation
    {
        /// Name of the ArmarX package.
        std::string package;

        /// Relative to the package's data directory.
        std::filesystem::path relativePath;

        /// The absolute path (in the host's file system).
        std::filesystem::path absolutePath;
    };

    template <class IDType>
    class FinderInfoBase
    {
    public:
        FinderInfoBase(const std::string& packageName, const std::filesystem::path& absPackageDataDir, const std::filesystem::path& relPackageDataPath, const IDType& id) :
            packageName(packageName),
            absPackageDataDir(absPackageDataDir),
            relPackageDataPath(relPackageDataPath),
            id(id)
        {}

        std::string getPackageName() const
        {
            return packageName;
        }

        std::filesystem::path getRelativePath() const
        {
            return relPackageDataPath;
        }

        std::filesystem::path getAbsolutePackagePath() const
        {
            checkAbsolutePathIsValid();
            return absPackageDataDir;
        }

        virtual std::filesystem::path getFullPath() const
        {
            checkAbsolutePathIsValid();
            return absPackageDataDir / packageName / relPackageDataPath;
        }

        IDType getID() const
        {
            return id;
        }

    protected:
        void checkAbsolutePathIsValid() const
        {
            if (!std::filesystem::exists(absPackageDataDir))
            {
                ARMARX_ERROR << "Could not resolve absolute path for package '" << packageName << "'.";
            }
        }


    protected:
        bool logError = true;

    private:
        std::string packageName;
        std::filesystem::path absPackageDataDir;
        std::filesystem::path relPackageDataPath;
        IDType id;
    };

    template <class IDType, class DatasetType>
    class DatasetFinderInfoBase : public FinderInfoBase<IDType>
    {

    public:
        using Base = FinderInfoBase<IDType>;

        DatasetFinderInfoBase(const std::string& packageName, const std::filesystem::path& absPackageDataDir, const std::filesystem::path& relPackageDataPath, const std::filesystem::path& relDatasetPath,  const DatasetType& dataset, const IDType& id) :
            Base(packageName, absPackageDataDir, relPackageDataPath, id),
            dataset(dataset),
            relDatasetPath(relDatasetPath)
        {}

        virtual std::filesystem::path getFullPath() const override
        {
            return Base::getFullPath() / relDatasetPath;
        }

        DatasetType getDataset() const
        {
            return dataset;
        }

    private:
        DatasetType dataset;
        std::filesystem::path relDatasetPath;
    };
}
