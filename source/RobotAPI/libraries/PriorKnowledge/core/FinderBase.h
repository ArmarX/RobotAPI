#pragma once

// STD/STL
#include <string>
#include <filesystem>
#include <optional>

// Base Class
#include <ArmarXCore/core/logging/Logging.h>

// ArmarX
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>


namespace armarx::priorknowledge::core
{
    template <class IDType, class FinderInfoType>
    class FinderBase : public Logging
    {
    public:
        FinderBase() = delete;
        FinderBase(const std::string& packageName, const std::filesystem::path& relDir) :
            packageName(packageName),
            relPackageDataPath(relDir)
        {
            CMakePackageFinder packageFinder(packageName);
            absPackageDataDir = packageFinder.getDataDir();
            if (absPackageDataDir.empty())
            {
                ARMARX_WARNING << "Could not find package '" << packageName << "'.";
                throw LocalException() << "Could not find package '" << packageName << "'.";
            }
            else
            {
                ARMARX_INFO << "PriorKnowledgeFinder root directory: " << absPackageDataDir;

                // make sure this data path is available => e.g. for findArticulatedObjects
                armarx::ArmarXDataPath::addDataPaths(std::vector<std::string> {absPackageDataDir});
            }
        }

        FinderBase(FinderBase&&)                 = default;
        FinderBase(const FinderBase&)            = default;
        FinderBase& operator=(FinderBase&&)      = default;
        FinderBase& operator=(const FinderBase&) = default;

        void setRelativePath(const std::filesystem::path& p)
        {
            relPackageDataPath = p;
        }

        std::string getPackageName() const
        {
            return packageName;
        }

        std::filesystem::path getRelativePath() const
        {
            return relPackageDataPath;
        }

        std::filesystem::path getAbsolutePackagePath() const
        {
            checkAbsolutePathIsValid();
            return absPackageDataDir;
        }

        std::filesystem::path getFullPath() const
        {
            checkAbsolutePathIsValid();
            return absPackageDataDir / packageName / relPackageDataPath;
        }

        virtual bool checkAll() const = 0;
        virtual bool check(const IDType& id) const = 0;
        virtual std::optional<FinderInfoType> find(const IDType& id) const = 0;
        virtual std::vector<FinderInfoType> findAll() const = 0;

    protected:
        void checkAbsolutePathIsValid() const
        {
            if (!std::filesystem::exists(absPackageDataDir))
            {
                ARMARX_ERROR << "PriorKnowledgeFinder is not initialized yet. Could not resolve absolute path for package '" << packageName << "'.";
            }
        }

    protected:

    private:
        std::string packageName;
        std::filesystem::path relPackageDataPath;
        std::filesystem::path absPackageDataDir;
    };


    template <class IDType, class DatasetType, class FinderInfoType> // TODO: concept DatasetFinderInfoType
    class DatasetFinderBase : public FinderBase<IDType, FinderInfoType>
    {

    public:
        using Base = FinderBase<IDType, FinderInfoType>;

        DatasetFinderBase(const std::string& packageName, const std::filesystem::path& relDir) :
            Base(packageName, relDir)
        {}

        std::filesystem::path getFullPath(const std::filesystem::path& relDatasetPath) const
        {
            return Base::getFullPath() / relDatasetPath;
        }

        virtual bool checkAll(const DatasetType& dataset) const = 0;
        virtual bool check(const DatasetType& dataset, const IDType& id) const = 0;
        virtual std::optional<FinderInfoType> find(const DatasetType& dataset, const IDType& id) const = 0;
        virtual std::vector<FinderInfoType> findAll(const DatasetType& dataset) const = 0;
    };
}
