#include <RobotAPI/libraries/PriorKnowledge/core/FinderBase.h>
#include <RobotAPI/libraries/PriorKnowledge/core/FinderInfoBase.h>


namespace armarx::priorknowledge::motions
{
    class MotionFinderInfo : public core::DatasetFinderInfoBase<std::string, std::string>
    {
        using Base = core::DatasetFinderInfoBase<std::string, std::string>;

    public:
        MotionFinderInfo(const std::string& packageName,
                         const std::filesystem::path& absPackageDataDir,
                         const std::filesystem::path& relPackageDataPath,
                         const std::filesystem::path& relDatasetPath,
                         const std::string& dataset,
                         const std::string& id) :
            Base(packageName, absPackageDataDir, relPackageDataPath, relDatasetPath, dataset, id)
        {
        }
    };

    class MotionFinder : public core::DatasetFinderBase<std::string, std::string, MotionFinderInfo>
    {
        using Base = core::DatasetFinderBase<std::string, std::string, MotionFinderInfo>;

    public:
        MotionFinder(const std::string& packageName, const std::filesystem::path& relDir) :
            Base(packageName, relDir)
        {}

        bool checkAll() const;
        bool check(const std::string& id) const;
        std::optional<MotionFinderInfo> find(const std::string& id) const;
        std::vector<MotionFinderInfo> findAll() const;

        bool checkAll(const std::string& dataset) const;
        bool check(const std::string& dataset, const std::string& id) const;
        std::optional<MotionFinderInfo> find(const std::string& dataset, const std::string& id) const;
        std::vector<MotionFinderInfo> findAll(const std::string& dataset) const;

    private:
        std::vector<std::string> DATASET_FOLDERS_BLACKLIST = {};
        std::vector<std::string> MOTION_ID_FOLDERS_BLACKLIST = {"script"};
    };

}
