// Simox
#include <SimoxUtility/algorithm/vector.hpp>

// BaseClass
#include "MotionFinder.h"

// ArmarX

namespace armarx::priorknowledge::motions
{
    bool MotionFinder::checkAll() const
    {
        return true;
    }

    bool MotionFinder::check(const std::string &id) const
    {
        return true;
    }

    std::optional<MotionFinderInfo> MotionFinder::find(const std::string &id) const
    {
        const std::filesystem::path absPath = Base::Base::getFullPath();
        if (std::filesystem::is_regular_file(absPath))
        {
            ARMARX_WARNING << "The entered path is leading to a file!";
            return std::nullopt;
        }

        for (const auto& d : std::filesystem::directory_iterator(absPath))
        {
            if (!d.is_directory())
            {
                ARMARX_WARNING << "Found invalid path: " << d.path();
                continue;
            }
            std::string k = d.path().filename();
            if (simox::alg::contains(DATASET_FOLDERS_BLACKLIST, k))
            {
                continue;
            }

            if (const auto op = this->find(k, id); op.has_value())
            {
                return op;
            }
        }
        return std::nullopt;
    }

    std::vector<MotionFinderInfo> MotionFinder::findAll() const
    {
        const std::filesystem::path absPath = Base::Base::getFullPath();
        if (std::filesystem::is_regular_file(absPath))
        {
            ARMARX_WARNING << "The entered path is leading to a file!";
            return {};
        }

        std::vector<MotionFinderInfo> ret;
        for (const auto& d : std::filesystem::directory_iterator(absPath))
        {
            if (!d.is_directory())
            {
                ARMARX_WARNING << "Found invalid path: " << d.path();
                continue;
            }
            std::string k = d.path().filename();
            if (simox::alg::contains(DATASET_FOLDERS_BLACKLIST, k))
            {
                continue;
            }

            auto motionsForDataset = this->findAll(k);
            simox::alg::append(ret, motionsForDataset);
        }
        return ret;
    }

    bool MotionFinder::checkAll(const std::string &dataset) const
    {
        return true;
    }

    bool MotionFinder::check(const std::string &dataset, const std::string &id) const
    {
        return true;
    }

    std::optional<MotionFinderInfo> MotionFinder::find(const std::string &dataset, const std::string &id) const
    {
        const std::filesystem::path absPathToMotion = this->getFullPath(dataset) / id;
        if (std::filesystem::is_regular_file(absPathToMotion))
        {
            ARMARX_WARNING << "The entered path is leading to a file!";
            return std::nullopt;
        }

        return std::make_optional(MotionFinderInfo(this->getPackageName(), this->getAbsolutePackagePath(), this->getRelativePath(), dataset, dataset, id));
    }

    std::vector<MotionFinderInfo> MotionFinder::findAll(const std::string &dataset) const
    {
        const std::filesystem::path absPathToDataset = this->getFullPath(dataset);
        if (std::filesystem::is_regular_file(absPathToDataset))
        {
            ARMARX_WARNING << "The entered path is leading to a file!";
            return {};
        }

        std::vector<MotionFinderInfo> ret;
        for (const auto& d : std::filesystem::directory_iterator(absPathToDataset))
        {
            if (!d.is_directory())
            {
                ARMARX_WARNING << "Found invalid path: " << d.path();
                continue;
            }
            std::string k = d.path().filename();
            if (simox::alg::contains(MOTION_ID_FOLDERS_BLACKLIST, k))
            {
                continue;
            }

            if(auto op = this->find(dataset, k); op.has_value())
            {
                ret.emplace_back(op.value());
            }
        }
        return ret;
    }

}
