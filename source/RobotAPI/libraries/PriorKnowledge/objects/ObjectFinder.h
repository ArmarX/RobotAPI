#include <RobotAPI/libraries/PriorKnowledge/core/FinderBase.h>
#include <RobotAPI/libraries/PriorKnowledge/core/FinderInfoBase.h>

// TODO!!!
namespace armarx::priorknowledge::objects
{
    class ObjectFinderInfo : public core::DatasetFinderInfoBase<std::string, std::string>
    {
        using Base = core::DatasetFinderInfoBase<std::string, std::string>;

    public:
        ObjectFinderInfo(const std::string& packageName,
                         const std::filesystem::path& absPackageDataDir,
                         const std::filesystem::path& relPackageDataPath,
                         const std::filesystem::path& relDatasetPath,
                         const std::string& dataset,
                         const std::string& id) :
            Base(packageName, absPackageDataDir, relPackageDataPath, relDatasetPath, dataset, id)
        {
        }
    };

    class ObjectFinder : public core::DatasetFinderBase<std::string, std::string, ObjectFinderInfo>
    {
        using Base = core::DatasetFinderBase<std::string, std::string, ObjectFinderInfo>;

    public:
        ObjectFinder(const std::string& packageName, const std::filesystem::path& relDir) :
            Base(packageName, relDir)
        {}

        std::vector<ObjectFinderInfo> findAll() const;

        std::vector<ObjectFinderInfo> findAll(const std::string& dataset) const;

    private:
        std::vector<std::string> DATASET_FOLDERS_BLACKLIST = {};
        std::vector<std::string> ID_FOLDERS_BLACKLIST = {"script"};
    };

}
