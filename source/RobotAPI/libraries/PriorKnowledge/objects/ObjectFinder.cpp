// Simox
#include <SimoxUtility/algorithm/vector.hpp>

// BaseClass
#include "ObjectFinder.h"

// ArmarX

namespace armarx::priorknowledge::objects
{
    std::vector<ObjectFinderInfo> ObjectFinder::findAll() const
    {
        const std::filesystem::path absPath = Base::Base::getFullPath();
        if (std::filesystem::is_regular_file(absPath))
        {
            ARMARX_WARNING << "The entered path is leading to a file!";
            return {};
        }

        std::vector<ObjectFinderInfo> ret;
        for (const auto& d : std::filesystem::directory_iterator(absPath))
        {
            if (!d.is_directory())
            {
                ARMARX_WARNING << "Found invalid path: " << d.path();
                continue;
            }
            std::string k = d.path().filename();
            if (simox::alg::contains(DATASET_FOLDERS_BLACKLIST, k))
            {
                continue;
            }

            auto motionsForDataset = this->findAll(k);
            simox::alg::append(ret, motionsForDataset);
        }
        return ret;
    }

    std::vector<ObjectFinderInfo> ObjectFinder::findAll(const std::string &dataset) const
    {
        const std::filesystem::path absPathToDataset = this->getFullPath(dataset);
        if (std::filesystem::is_regular_file(absPathToDataset))
        {
            ARMARX_WARNING << "The entered path is leading to a file!";
            return {};
        }

        std::vector<ObjectFinderInfo> ret;
        for (const auto& d : std::filesystem::directory_iterator(absPathToDataset))
        {
            if (!d.is_directory())
            {
                ARMARX_WARNING << "Found invalid path: " << d.path();
                continue;
            }
            std::string k = d.path().filename();
            if (simox::alg::contains(ID_FOLDERS_BLACKLIST, k))
            {
                continue;
            }

            if(auto op = this->find(dataset, k); op.has_value())
            {
                ret.emplace_back(op.value());
            }
        }
        return ret;
    }

}
