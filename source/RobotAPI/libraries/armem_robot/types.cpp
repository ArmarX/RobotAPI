#include "types.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::armem::robot
{

    std::ostream& operator<<(std::ostream &os, const RobotDescription &rhs)
    {
        os << "RobotDescription { name: '" << rhs.name << "', xml: '" << rhs.xml << "' }";
        return os;
    }


    std::string Robot::name() const
    {
        ARMARX_CHECK_NOT_EMPTY(description.name) << "The robot name must be set!";
        ARMARX_CHECK_NOT_EMPTY(instance) << "The robot instance name must be provided!";

        return description.name + "/" + instance;
    }
  
}
