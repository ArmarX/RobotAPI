#pragma once


#include "RobotAPI/libraries/armem/core/forward_declarations.h"
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem_robot/types.h>

namespace armarx::armem::robot
{
    class ReaderInterface
    {
    public:
        virtual ~ReaderInterface() = default;

        virtual bool synchronize(Robot& obj, const armem::Time& timestamp) = 0;

        virtual Robot get(const RobotDescription& description, const armem::Time& timestamp) = 0;
        virtual std::optional<Robot> get(const std::string& name, const armem::Time& timestamp) = 0;
    };

    class WriterInterface
    {
    public:
        virtual ~WriterInterface() = default;

        // virtual bool store(const Robot& obj) = 0;

        virtual bool storeDescription(const RobotDescription& description,
                                      const armem::Time& timestamp = armem::Time::Invalid()) = 0;

        virtual bool storeState(const robot::RobotState& state,
                                const std::string& robotTypeName,
                                const std::string& robotName,
                                const std::string& robotRootNodeName) = 0;
    };

} // namespace armarx::armem::robot
