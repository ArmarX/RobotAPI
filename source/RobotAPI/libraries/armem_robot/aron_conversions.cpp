#include "aron_conversions.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/libraries/aron/common/aron_conversions.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
#include <RobotAPI/libraries/ArmarXObjects/aron_conversions.h>


namespace armarx::armem::robot
{

    /* Robot */

    void fromAron(const arondto::Robot& dto, Robot& bo)
    {
        // fromAron(dto.description, bo.description);
        fromAron(dto.state, bo.config);
    }

    void toAron(arondto::Robot& dto, const Robot& bo)
    {
        // toAron(dto.description, bo.description);
        toAron(dto.state, bo.config);
    }

    /* RobotDescription */

    void fromAron(const arondto::RobotDescription& dto, RobotDescription& bo)
    {
        aron::fromAron(dto.name, bo.name);
        fromAron(dto.xml, bo.xml);
    }

    void toAron(arondto::RobotDescription& dto, const RobotDescription& bo)
    {
        aron::toAron(dto.name, bo.name);
        toAron(dto.xml, bo.xml);
    }

    /* RobotState */

    void fromAron(const arondto::RobotState& dto, RobotState& bo)
    {
        aron::fromAron(dto.timestamp, bo.timestamp);
        aron::fromAron(dto.globalPose, bo.globalPose.matrix());
        aron::fromAron(dto.jointMap, bo.jointMap);

    }

    void toAron(arondto::RobotState& dto, const RobotState& bo)
    {
        aron::toAron(dto.timestamp, bo.timestamp);
        aron::toAron(dto.globalPose, bo.globalPose.matrix());
        aron::toAron(dto.jointMap, bo.jointMap);

    }

}  // namespace armarx::armem::robot


namespace armarx::armem
{

    void robot::fromAron(const arondto::ObjectClass& dto, RobotDescription& bo)
    {
        bo.name = aron::fromAron<armarx::ObjectID>(dto.id).str();
        fromAron(dto.articulatedSimoxXmlPath, bo.xml);
    }

    void robot::toAron(arondto::ObjectClass& dto, const RobotDescription& bo)
    {
        toAron(dto.id, ObjectID(bo.name));
        toAron(dto.articulatedSimoxXmlPath, bo.xml);
    }


    void robot::fromAron(const arondto::ObjectInstance& dto, RobotState& bo)
    {
        fromAron(dto.pose, bo);
    }

    void robot::toAron(arondto::ObjectInstance& dto, const RobotState& bo)
    {
        toAron(dto.pose, bo);
    }


    void robot::fromAron(const objpose::arondto::ObjectPose& dto, RobotState& bo)
    {
        aron::fromAron(dto.timestamp, bo.timestamp);
        aron::fromAron(dto.objectPoseGlobal, bo.globalPose);
        aron::fromAron(dto.objectJointValues, bo.jointMap);

    }

    void robot::toAron(objpose::arondto::ObjectPose& dto, const RobotState& bo)
    {
        aron::toAron(dto.timestamp, bo.timestamp);
        aron::toAron(dto.objectPoseGlobal, bo.globalPose.matrix());
        aron::toAron(dto.objectJointValues, bo.jointMap);

    }
    

}  // namespace armarx::armem
