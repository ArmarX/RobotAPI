#pragma once

#include <RobotAPI/libraries/armem_objects/aron/ObjectClass.aron.generated.h>
#include <RobotAPI/libraries/armem_objects/aron/ObjectInstance.aron.generated.h>
#include <RobotAPI/libraries/ArmarXObjects/aron/ObjectPose.aron.generated.h>

#include <RobotAPI/libraries/armem_robot/types.h>
#include <RobotAPI/libraries/armem_robot/aron/RobotDescription.aron.generated.h>
#include <RobotAPI/libraries/armem_robot/aron/RobotState.aron.generated.h>
#include <RobotAPI/libraries/armem_robot/aron/Robot.aron.generated.h>


namespace armarx::armem::robot
{
    // TODO move the following
    void fromAron(const long& dto, IceUtil::Time& time);
    void toAron(long& dto, const IceUtil::Time& time);
    // end TODO

    void fromAron(const arondto::Robot& dto, Robot& bo);
    void toAron(arondto::Robot& dto, const Robot& bo);

    void fromAron(const arondto::RobotDescription& dto, RobotDescription& bo);
    void toAron(arondto::RobotDescription& dto, const RobotDescription& bo);

    void fromAron(const arondto::RobotState& dto, RobotState& bo);
    void toAron(arondto::RobotState& dto, const RobotState& bo);

    void fromAron(const arondto::ObjectClass& dto, RobotDescription& bo);
    void toAron(arondto::ObjectClass& dto, const RobotDescription& bo);

    void fromAron(const arondto::ObjectInstance& dto, RobotState& bo);
    void toAron(arondto::ObjectInstance& dto, const RobotState& bo);

    void fromAron(const objpose::arondto::ObjectPose& dto, RobotState& bo);
    void toAron(objpose::arondto::ObjectPose& dto, const RobotState& bo);

}  // namespace armarx::armem::robot
