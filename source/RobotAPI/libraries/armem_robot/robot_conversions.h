#pragma once

#include <optional>

#include "types.h"

namespace armarx::armem::wm
{
    class EntityInstance;
}

namespace armarx::armem::robot
{
    std::optional<RobotDescription> convertRobotDescription(const armem::wm::EntityInstance& instance);
    std::optional<RobotState> convertRobotState(const armem::wm::EntityInstance& instance);
} // namespace armarx::armem::articulated_object
