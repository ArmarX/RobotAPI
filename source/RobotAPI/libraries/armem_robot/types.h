#pragma once

#include <filesystem>
#include <map>
#include <vector>

#include <Eigen/Geometry>

#include <ArmarXCore/core/PackagePath.h>
#include <ArmarXCore/core/time/DateTime.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>


namespace armarx::armem::robot
{
    struct RobotDescription
    {
        // DateTime timestamp;

        std::string name;
        PackagePath xml{"", std::filesystem::path("")};
    };


    struct Twist
    {
        Eigen::Vector3f linear;
        Eigen::Vector3f angular;
    };

    struct PlatformState
    {
        Twist twist;
    };

    struct ForceTorque
    {
        Eigen::Vector3f force;
        Eigen::Vector3f torque;
    };

    struct RobotState
    {
        using JointMap = std::map<std::string, float>;
        using Pose = Eigen::Affine3f;

        DateTime timestamp;

        Pose globalPose;
        JointMap jointMap;
    };


    struct Robot
    {
        RobotDescription description;
        std::string instance;

        RobotState config;

        DateTime timestamp;

        std::string name() const;
    };

    using Robots = std::vector<Robot>;
    using RobotDescriptions = std::vector<RobotDescription>;
    using RobotStates = std::vector<RobotState>;

    std::ostream& operator<<(std::ostream &os, const RobotDescription &rhs);

}  // namespace armarx::armem::robot
