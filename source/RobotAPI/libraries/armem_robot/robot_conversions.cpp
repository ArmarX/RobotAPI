#include "robot_conversions.h"

#include <filesystem>

#include <RobotAPI/libraries/aron/common/aron_conversions.h>

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/aron_conversions.h>

#include <RobotAPI/libraries/armem_robot/aron/RobotDescription.aron.generated.h>
#include <RobotAPI/libraries/armem_robot/aron_conversions.h>

#include <ArmarXCore/core/logging/Logging.h>


namespace fs = ::std::filesystem;

namespace armarx::armem::robot
{

    std::optional<RobotDescription> convertRobotDescription(const armem::wm::EntityInstance& instance)
    {
        arondto::RobotDescription aronRobotDescription;
        try
        {
            aronRobotDescription.fromAron(instance.data());
        }
        catch (...)
        {
            ARMARX_WARNING << "Conversion to RobotDescription failed!";
            return std::nullopt;
        }

        RobotDescription robotDescription
        {
            .name = "",
            .xml = ::armarx::PackagePath("", fs::path("")) // initialize empty, no default c'tor
        };

        fromAron(aronRobotDescription, robotDescription);

        return robotDescription;
    }


    std::optional<RobotState> convertRobotState(const armem::wm::EntityInstance& instance)
    {
        arondto::Robot aronRobot;
        try
        {
            aronRobot.fromAron(instance.data());
        }
        catch (...)
        {
            ARMARX_WARNING << "Conversion to RobotState failed!";
            return std::nullopt;
        }

        RobotState robotState;
        fromAron(aronRobot.state, robotState);

        return robotState;
    }

}  // namespace armarx::armem::robot
