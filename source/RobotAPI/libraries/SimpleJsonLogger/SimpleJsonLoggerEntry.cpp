/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SimpleJsonLoggerEntry.h"
#include <IceUtil/Time.h>

namespace armarx
{
    SimpleJsonLoggerEntry::SimpleJsonLoggerEntry()
        : obj(new JsonObject)
    {
    }

    void SimpleJsonLoggerEntry::AddAsArr(const std::string& key, const Eigen::Vector3f& vec)
    {
        obj->add(key, ToArr((Eigen::VectorXf)vec));
    }

    void SimpleJsonLoggerEntry::AddAsArr(const std::string& key, const Eigen::VectorXf& vec)
    {
        obj->add(key, ToArr(vec));
    }

    void SimpleJsonLoggerEntry::AddAsObj(const std::string& key, const Eigen::Vector3f& vec)
    {
        obj->add(key, ToObj(vec));
    }

    void SimpleJsonLoggerEntry::AddAsArr(const std::string& key, const Eigen::Matrix4f& mat)
    {
        obj->add(key, ToArr(mat));
    }

    void SimpleJsonLoggerEntry::AddMatrix(const std::string& key, const Eigen::MatrixXf& mat)
    {
        obj->add(key, MatrixToArr(mat));
    }

    void SimpleJsonLoggerEntry::Add(const std::string& key, const std::string& value)
    {
        obj->add(key, JsonValue::Create(value));
    }

    void SimpleJsonLoggerEntry::Add(const std::string& key, float value)
    {
        obj->add(key, JsonValue::Create(value));
    }

    void SimpleJsonLoggerEntry::Add(const std::string& key, const std::vector<float>& value)
    {
        obj->add(key, ToArr(value));
    }

    void SimpleJsonLoggerEntry::Add(const std::string& key, const std::map<std::string, float>& value)
    {
        obj->add(key, ToObj(value));
    }


    JsonArrayPtr SimpleJsonLoggerEntry::ToArr(const Eigen::VectorXf& vec)
    {
        JsonArrayPtr arr(new JsonArray);
        for (int i = 0; i < vec.rows(); i++)
        {
            arr->add(JsonValue::Create(vec(i)));
        }
        return arr;
    }

    JsonArrayPtr SimpleJsonLoggerEntry::ToArr(const std::vector<float>& vec)
    {
        JsonArrayPtr arr(new JsonArray);
        for (float v : vec)
        {
            arr->add(JsonValue::Create(v));
        }
        return arr;
    }

    JsonObjectPtr SimpleJsonLoggerEntry::ToObj(Eigen::Vector3f vec)
    {
        JsonObjectPtr obj(new JsonObject);
        obj->add("x", JsonValue::Create(vec(0)));
        obj->add("y", JsonValue::Create(vec(1)));
        obj->add("z", JsonValue::Create(vec(2)));
        return obj;
    }

    JsonObjectPtr SimpleJsonLoggerEntry::ToObj(const std::map<std::string, float>& value)
    {
        JsonObjectPtr obj(new JsonObject);
        for (const auto& pair : value)
        {
            obj->add(pair.first, JsonValue::Create(pair.second));
        }
        return obj;
    }

    JsonArrayPtr SimpleJsonLoggerEntry::ToArr(Eigen::Matrix4f mat)
    {
        JsonArrayPtr arr(new JsonArray);
        JsonArrayPtr row1(new JsonArray);
        JsonArrayPtr row2(new JsonArray);
        JsonArrayPtr row3(new JsonArray);
        JsonArrayPtr row4(new JsonArray);

        row1->add(JsonValue::Create(mat(0, 0)));
        row1->add(JsonValue::Create(mat(0, 1)));
        row1->add(JsonValue::Create(mat(0, 2)));
        row1->add(JsonValue::Create(mat(0, 3)));

        row2->add(JsonValue::Create(mat(1, 0)));
        row2->add(JsonValue::Create(mat(1, 1)));
        row2->add(JsonValue::Create(mat(1, 2)));
        row2->add(JsonValue::Create(mat(1, 3)));

        row3->add(JsonValue::Create(mat(2, 0)));
        row3->add(JsonValue::Create(mat(2, 1)));
        row3->add(JsonValue::Create(mat(2, 2)));
        row3->add(JsonValue::Create(mat(2, 3)));

        row4->add(JsonValue::Create(mat(3, 0)));
        row4->add(JsonValue::Create(mat(3, 1)));
        row4->add(JsonValue::Create(mat(3, 2)));
        row4->add(JsonValue::Create(mat(3, 3)));

        arr->add(row1);
        arr->add(row2);
        arr->add(row3);
        arr->add(row4);

        return arr;
    }

    JsonArrayPtr SimpleJsonLoggerEntry::MatrixToArr(Eigen::MatrixXf mat)
    {
        JsonArrayPtr arr(new JsonArray);
        for (int i = 0; i < mat.rows(); i++)
        {
            JsonArrayPtr row(new JsonArray);
            for (int j = 0; j < mat.cols(); j++)
            {
                row->add(JsonValue::Create(mat(i, j)));
            }
            arr->add(row);
        }
        return arr;
    }

    void SimpleJsonLoggerEntry::AddTimestamp()
    {
        IceUtil::Time now = IceUtil::Time::now();
        obj->add("timestamp", JsonValue::Create(now.toDateTime()));
    }
}
