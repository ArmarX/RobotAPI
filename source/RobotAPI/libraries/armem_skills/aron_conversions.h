#pragma once

#include <ArmarXCore/interface/core/Profiler.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>

#include <RobotAPI/libraries/armem_skills/aron/Statechart.aron.generated.h>

namespace armarx::armem
{
    void fromAron(const armarx::skills::arondto::Statechart::StateType& dto, armarx::eStateType& bo);
    void toAron(armarx::skills::arondto::Statechart::StateType& dto, const armarx::eStateType& bo);

    void fromAron(const armarx::skills::arondto::Statechart::ParameterMap& dto, armarx::StateParameterMap& bo);
    void toAron(armarx::skills::arondto::Statechart::ParameterMap& dto, const armarx::StateParameterMap& bo);

    void fromAron(const armarx::skills::arondto::Statechart::Transition& dto, armarx::ProfilerStatechartTransitionWithParameters& bo);
    void toAron(armarx::skills::arondto::Statechart::Transition& dto, const armarx::ProfilerStatechartTransitionWithParameters& bo);
}
