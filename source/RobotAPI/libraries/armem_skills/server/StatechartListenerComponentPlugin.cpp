#include "StatechartListenerComponentPlugin.h"

#include <ArmarXCore/core/Component.h>

namespace armarx::plugins
{
    void StatechartListenerComponentPlugin::preOnInitComponent()
    {}

    void StatechartListenerComponentPlugin::preOnConnectComponent()
    {}

    void StatechartListenerComponentPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {}
}


namespace armarx
{
    StatechartListenerComponentPluginUser::StatechartListenerComponentPluginUser()
    {
        addPlugin(plugin);
    }
}
