#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>
#include <ArmarXCore/core/ManagedIceObject.h>

#include <RobotAPI/interface/skills/StatechartListenerInterface.h>

namespace armarx::plugins
{
    class StatechartListenerComponentPlugin : public ComponentPlugin
    {
    public:
        using ComponentPlugin::ComponentPlugin;

        void preOnInitComponent() override;

        void preOnConnectComponent() override;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

    };
}

namespace armarx
{
    class StatechartListenerComponentPluginUser :
            virtual public ManagedIceObject,
            virtual public dti::StatechartListenerInterface
    {
    public:
        StatechartListenerComponentPluginUser();

        void reportNetworkTraffic(const std::string&, const std::string&, Ice::Int, Ice::Int, const Ice::Current&) override {}
        void reportEvent(const ProfilerEvent&, const Ice::Current&) override {}
        void reportStatechartTransition(const ProfilerStatechartTransition& event, const Ice::Current&) override {}
        void reportStatechartInputParameters(const ProfilerStatechartParameters& event, const Ice::Current&) override {}
        void reportStatechartLocalParameters(const ProfilerStatechartParameters& event, const Ice::Current&) override {}
        void reportStatechartOutputParameters(const ProfilerStatechartParameters&, const Ice::Current&) override {}
        void reportProcessCpuUsage(const ProfilerProcessCpuUsage&, const Ice::Current&) override {}
        void reportProcessMemoryUsage(const ProfilerProcessMemoryUsage&, const Ice::Current&) override {}

        void reportEventList(const ProfilerEventList& events, const Ice::Current&) override {}
        void reportStatechartTransitionList(const ProfilerStatechartTransitionList&, const Ice::Current&) override {}
        void reportStatechartInputParametersList(const ProfilerStatechartParametersList& data, const Ice::Current&) override {}
        void reportStatechartLocalParametersList(const ProfilerStatechartParametersList&, const Ice::Current&) override {}
        void reportStatechartOutputParametersList(const ProfilerStatechartParametersList&, const Ice::Current&) override {}
        void reportProcessCpuUsageList(const ProfilerProcessCpuUsageList&, const Ice::Current&) override {}
        void reportProcessMemoryUsageList(const ProfilerProcessMemoryUsageList&, const Ice::Current&) override {}

    private:
        armarx::plugins::StatechartListenerComponentPlugin* plugin = nullptr;
    };
}
