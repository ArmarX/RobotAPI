#pragma once

// Base Class
#include <RobotAPI/libraries/armem/server/segment/SpecializedSegment.h>

// ArmarX
#include <ArmarXCore/interface/core/Profiler.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <RobotAPI/interface/skills/SkillManagerInterface.h>
#include <RobotAPI/interface/skills/SkillProviderInterface.h>

namespace armarx::skills::segment
{
    class SkillExecutionRequestCoreSegment :
        public armem::server::segment::SpecializedCoreSegment
    {
        using Base = armem::server::segment::SpecializedCoreSegment;

    public:
        static constexpr const char* CoreSegmentName = "SkillExecutionRequest";

        SkillExecutionRequestCoreSegment(armem::server::MemoryToIceAdapter& iceMemory);

        void defineProperties(PropertyDefinitionsPtr defs, const std::string &prefix);
        void init();

        skills::manager::dto::SkillExecutionRequest convertCommit(const aron::data::dto::DictPtr& commitData);

        void addSkillExecutionRequest(const skills::manager::dto::SkillExecutionRequest& info);
    };
}
