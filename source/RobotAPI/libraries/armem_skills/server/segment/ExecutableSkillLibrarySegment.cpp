#include "ExecutableSkillLibrarySegment.h"

#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>
#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

#include <RobotAPI/libraries/armem_skills/aron/Skill.aron.generated.h>

namespace armarx::skills::segment
{
    ExecutableSkillLibraryCoreSegment::ExecutableSkillLibraryCoreSegment(armem::server::MemoryToIceAdapter& iceMemory):
        Base(iceMemory, CoreSegmentName, skills::arondto::SkillDescription::ToAronType())
    {
    }

    void ExecutableSkillLibraryCoreSegment::defineProperties(PropertyDefinitionsPtr defs, const std::string &prefix)
    {
        // No properties! (meaning no name and no max size)
    }

    void ExecutableSkillLibraryCoreSegment::init()
    {
        Base::init();
    }

    void ExecutableSkillLibraryCoreSegment::addSkillProvider(const skills::manager::dto::ProviderInfo& info)
    {
        // add skills
        auto skills = info.providedSkills;

        auto provId = id().withProviderSegmentName(info.providerName);

        for (const auto& [key, desc] : skills)
        {
            armarx::skills::arondto::SkillDescription skillDescription;
            skillDescription.skillName = desc.skillName;
            skillDescription.description = desc.description;
            skillDescription.iceInfo = info.provider->ice_toString();
            skillDescription.robots = desc.robots;
            skillDescription.timeoutMs = desc.timeoutMs;

            if (desc.acceptedType)
            {
                auto t = aron::type::Object::FromAronObjectDTO(desc.acceptedType);
                skillDescription.acceptedType = aron::converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(t).dump(2);
            }

            armem::Commit commit;
            auto& entityUpdate = commit.add();
            entityUpdate.confidence = 1.0;
            entityUpdate.timeCreated = armem::Time::Now();
            entityUpdate.instancesData = {skillDescription.toAron()};
            entityUpdate.entityID = provId.withEntityName(skillDescription.skillName);

            // Commit data to memory and notify
            iceMemory.commit(commit);
        }
    }

    void ExecutableSkillLibraryCoreSegment::removeSkillProvider(const std::string& providerName)
    {
        skills.erase(providerName);

        // TODO also add info about removed provider to memory?
    }
}
