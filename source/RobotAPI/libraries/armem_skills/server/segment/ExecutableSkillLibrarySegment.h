#pragma once

// Base Class
#include <RobotAPI/libraries/armem/server/segment/SpecializedSegment.h>

// ArmarX
#include <ArmarXCore/interface/core/Profiler.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <RobotAPI/interface/skills/SkillManagerInterface.h>
#include <RobotAPI/interface/skills/SkillProviderInterface.h>

#include <RobotAPI/libraries/armem_skills/aron/Skill.aron.generated.h>

namespace armarx::skills::segment
{
    class ExecutableSkillLibraryCoreSegment :
        public armem::server::segment::SpecializedCoreSegment
    {
        using Base = armem::server::segment::SpecializedCoreSegment;

    public:
        static constexpr const char* CoreSegmentName = "ExecutableSkill";

        ExecutableSkillLibraryCoreSegment(armem::server::MemoryToIceAdapter& iceMemory);

        void defineProperties(PropertyDefinitionsPtr defs, const std::string &prefix);
        void init();

        void addSkillProvider(const skills::manager::dto::ProviderInfo& info);
        void removeSkillProvider(const std::string& providerName);

        size_t size() const;

    private:
        std::map<std::string, std::map<std::string, skills::manager::dto::ProviderInfo>> skills;
    };
}
