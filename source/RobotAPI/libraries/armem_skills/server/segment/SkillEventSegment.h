#pragma once

#include <thread>

// Base Class
#include <RobotAPI/libraries/armem/server/segment/SpecializedSegment.h>

// ArmarX
#include <RobotAPI/interface/skills/SkillManagerInterface.h>
#include <RobotAPI/interface/skills/SkillProviderInterface.h>

#include <RobotAPI/libraries/armem_skills/aron/Skill.aron.generated.h>

namespace armarx::skills::segment
{
    class SkillEventCoreSegment :
        public armem::server::segment::SpecializedCoreSegment
    {
        using Base = armem::server::segment::SpecializedCoreSegment;

    public:
        static constexpr const char* CoreSegmentName = "SkillEvent";

        SkillEventCoreSegment(armem::server::MemoryToIceAdapter& iceMemory);

        void defineProperties(PropertyDefinitionsPtr defs, const std::string &prefix);
        void init();

        void addSkillUpdateEvent(const skills::provider::dto::SkillStatusUpdate& update);

    private:
    };
}
