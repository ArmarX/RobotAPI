#include "SkillEventSegment.h"

#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>
#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

#include <RobotAPI/libraries/armem_skills/aron/Skill.aron.generated.h>

namespace armarx::skills::segment
{

    SkillEventCoreSegment::SkillEventCoreSegment(armem::server::MemoryToIceAdapter& iceMemory):
        Base(iceMemory, CoreSegmentName, armarx::skills::arondto::SkillExecutionRequest::ToAronType())
    {
    }

    void SkillEventCoreSegment::defineProperties(PropertyDefinitionsPtr defs, const std::string &prefix)
    {
        // No properties! (meaning no name and no max size)
    }

    void SkillEventCoreSegment::init()
    {
        Base::init();
    }

    void SkillEventCoreSegment::addSkillUpdateEvent(const skills::provider::dto::SkillStatusUpdate& update)
    {
        // add update for skill to memory
        static std::map<armarx::skills::provider::dto::Execution::Status, std::string> ExecutionStatus2String = {
            {armarx::skills::provider::dto::Execution::Status::Idle, "Idle"},
            {armarx::skills::provider::dto::Execution::Status::Scheduled, "Scheduled"},
            {armarx::skills::provider::dto::Execution::Status::Running, "Running"},
            {armarx::skills::provider::dto::Execution::Status::Aborted, "Aborted"},
            {armarx::skills::provider::dto::Execution::Status::Failed, "Failed"},
            {armarx::skills::provider::dto::Execution::Status::Succeeded, "Succeeded"}
        };

        // create commit about new update
        armarx::skills::arondto::SkillExecutionEvent event;
        event.providerName = update.header.skillId.providerName;
        event.skillName = update.header.skillId.skillName;
        event.status = ExecutionStatus2String.at(update.header.status);
        event.params = aron::data::Dict::FromAronDictDTO(update.header.usedParams);
        event.data = aron::data::Dict::FromAronDictDTO(update.data);

        armem::MemoryID commitId = id();
        commitId.providerSegmentName = event.providerName;
        commitId.entityName = event.skillName;

        auto aron = event.toAron();

        armem::Commit comm;
        auto& entityUpdate = comm.add();
        entityUpdate.confidence = 1.0;
        entityUpdate.timeCreated = armem::Time::Now();
        entityUpdate.instancesData = { aron };
        entityUpdate.entityID = commitId;

        iceMemory.commit(comm);
    }
}
