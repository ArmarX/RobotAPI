#pragma once

// Base Class
#include <RobotAPI/libraries/armem/server/segment/SpecializedSegment.h>

// ArmarX
#include <ArmarXCore/interface/core/Profiler.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>


namespace armarx::skills::segment
{
    class StatechartListenerProviderSegment :
        public armem::server::segment::SpecializedProviderSegment
    {
        using Base = armem::server::segment::SpecializedProviderSegment;

    public:
        StatechartListenerProviderSegment(armem::server::MemoryToIceAdapter& iceMemory);

        void defineProperties(PropertyDefinitionsPtr defs, const std::string &prefix);
        void init();

        void reportStatechartTransitionWithParameters(const ProfilerStatechartTransitionWithParameters&);
        void reportStatechartTransitionWithParametersList(const ProfilerStatechartTransitionWithParametersList&);

    private:
        std::string getStatechartName(std::string stateName);

    private:
        struct Properties
        {
            // Statechart transition logging
            std::string statechartCoreSegmentName = "Statechart";
            std::string statechartTransitionsProviderSegmentName = "Transitions";
            std::string statechartTransitionsTopicName = "StateReportingTopic";
        };
        Properties p;
    };
}
