#include "StatechartListenerSegment.h"

#include <RobotAPI/libraries/armem_skills/aron/Statechart.aron.generated.h>
#include <RobotAPI/libraries/armem_skills/aron_conversions.h>

namespace armarx::skills::segment
{
    StatechartListenerProviderSegment::StatechartListenerProviderSegment(armem::server::MemoryToIceAdapter& iceMemory):
        Base(iceMemory, "StatechartListener", "Transitions", arondto::Statechart::Transition::ToAronType())
    {
    }

    void StatechartListenerProviderSegment::defineProperties(PropertyDefinitionsPtr defs, const std::string &prefix)
    {
        // Statechart Logging
        defs->optional(p.statechartCoreSegmentName, "StatechartCoreSegmentName", "Name of the core segment for statecharts.");
        defs->optional(p.statechartTransitionsProviderSegmentName, "TransitionsProviderSegmentName", "Name of the provider segment for statechart transitions.");
        defs->optional(p.statechartTransitionsTopicName, "tpc.sub.ProfilerListener", "Name of the ProfilerListenerInterface topics to subscribe.");
    }

    void StatechartListenerProviderSegment::init()
    {
        Base::init();
    }

    void StatechartListenerProviderSegment::reportStatechartTransitionWithParameters(const ProfilerStatechartTransitionWithParameters& t)
    {
        const std::string& entityName = getStatechartName(t.targetStateIdentifier);
        armem::Time transitionTime = armem::Time(armem::Duration::MicroSeconds(t.timestamp));

        armem::EntityUpdate update;
        update.entityID = segmentPtr->id().withEntityName(entityName);

        update.timeCreated = transitionTime;
        skills::arondto::Statechart::Transition data;
        armem::toAron(data, t);
        update.instancesData.push_back(data.toAron());

        try
        {
            segmentPtr->update(update);
        }
        catch (const armem::error::ArMemError& e)
        {
            ARMARX_WARNING << e.what();
        }
    }

    void StatechartListenerProviderSegment::reportStatechartTransitionWithParametersList(const ProfilerStatechartTransitionWithParametersList& transitions)
    {
        for (const auto& t : transitions)
        {
            reportStatechartTransitionWithParameters(t);
        }
    }

    std::string StatechartListenerProviderSegment::getStatechartName(std::string stateName)
    {
        const std::string delimiter = "->";
        const int maxLevels = 2;

        size_t pos;
        int levels = 0;
        std::string statechartName;
        while ((pos = stateName.find(delimiter)) != std::string::npos && levels < maxLevels)
        {
            if (levels != 0)
            {
                statechartName += delimiter;
            }
            statechartName += stateName.substr(0, pos);
            stateName.erase(0, pos + delimiter.length());
            levels++;
        }

        return statechartName;
    }
}
