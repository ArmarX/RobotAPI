#include "SkillExecutionRequestSegment.h"

#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>
#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

#include <RobotAPI/libraries/armem_skills/aron/Skill.aron.generated.h>


namespace armarx::skills::segment
{

    SkillExecutionRequestCoreSegment::SkillExecutionRequestCoreSegment(armem::server::MemoryToIceAdapter& iceMemory):
        Base(iceMemory, CoreSegmentName, skills::arondto::SkillExecutionRequest::ToAronType())
    {
    }

    void SkillExecutionRequestCoreSegment::defineProperties(PropertyDefinitionsPtr defs, const std::string &prefix)
    {
        // No properties! (meaning no name and no max size)
    }

    void SkillExecutionRequestCoreSegment::init()
    {
        Base::init();
    }


    skills::manager::dto::SkillExecutionRequest SkillExecutionRequestCoreSegment::convertCommit(const aron::data::dto::DictPtr& commitData)
    {
        // convert ice commitData to aron
        auto commitDataAron = std::make_shared<aron::data::Dict>(commitData);

        // we got a skill execution request
        skills::arondto::SkillExecutionRequest request;
        request.fromAron(commitDataAron);

        skills::manager::dto::SkillExecutionRequest info;
        info.skillId = {request.providerName, request.skillName};
        info.params = request.params->toAronDictDTO();
        return info;
    }


    void SkillExecutionRequestCoreSegment::addSkillExecutionRequest(const skills::manager::dto::SkillExecutionRequest& info)
    {
        // override directly execution to add a request to the memory
        armem::Commit comm;

        skills::arondto::SkillExecutionRequest request;
        request.executorName = info.executorName;
        request.providerName = info.skillId.providerName;
        request.skillName = info.skillId.skillName;
        request.params = aron::data::Dict::FromAronDictDTO(info.params);

        auto aron = request.toAron();

        {
            auto& entityUpdate = comm.add();

            armem::MemoryID skillExecutionMemID = id();
            skillExecutionMemID.providerSegmentName = request.providerName;
            skillExecutionMemID.entityName = request.skillName;

            entityUpdate.entityID = skillExecutionMemID;
            entityUpdate.instancesData = { aron };
            entityUpdate.confidence = 1.0;
            entityUpdate.timeCreated = armem::Time::Now();
        }

        {
            auto& entityUpdate = comm.add();

            armem::MemoryID skillExecutionMemID = id();
            skillExecutionMemID.providerSegmentName = "All Skill Execution Requests";
            skillExecutionMemID.entityName = "All Skill Execution Requests";

            entityUpdate.entityID = skillExecutionMemID;
            entityUpdate.instancesData = { aron };
            entityUpdate.confidence = 1.0;
            entityUpdate.timeCreated = armem::Time::Now();
        }


        iceMemory.commit(comm);
    }
}
