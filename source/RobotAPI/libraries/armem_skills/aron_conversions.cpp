#include "aron_conversions.h"

namespace armarx::armem
{
    std::map<armarx::eStateType, armarx::skills::arondto::Statechart::StateType> toAronStateTypeMap =
    {
        {eNormalState, (armarx::skills::arondto::Statechart::StateType) armarx::skills::arondto::Statechart::StateType::NORMAL},
        {eFinalState, (armarx::skills::arondto::Statechart::StateType) armarx::skills::arondto::Statechart::StateType::FINAL},
        {eRemoteState, (armarx::skills::arondto::Statechart::StateType) armarx::skills::arondto::Statechart::StateType::REMOTE},
        {eDynamicRemoteState, (armarx::skills::arondto::Statechart::StateType) armarx::skills::arondto::Statechart::StateType::DYNAMIC_REMOTE},
        {eUndefined, (armarx::skills::arondto::Statechart::StateType) armarx::skills::arondto::Statechart::StateType::UNDEFINED},
    };

    std::map<armarx::skills::arondto::Statechart::StateType, armarx::eStateType> fromAronStateTypeMap =
    {
        {(armarx::skills::arondto::Statechart::StateType) armarx::skills::arondto::Statechart::StateType::NORMAL, eNormalState},
        {(armarx::skills::arondto::Statechart::StateType) armarx::skills::arondto::Statechart::StateType::FINAL, eFinalState},
        {(armarx::skills::arondto::Statechart::StateType) armarx::skills::arondto::Statechart::StateType::REMOTE, eRemoteState},
        {(armarx::skills::arondto::Statechart::StateType) armarx::skills::arondto::Statechart::StateType::DYNAMIC_REMOTE, eDynamicRemoteState},
        {(armarx::skills::arondto::Statechart::StateType) armarx::skills::arondto::Statechart::StateType::UNDEFINED, eUndefined},
    };

    void fromAron(const skills::arondto::Statechart::StateType& dto, eStateType& bo)
    {
        if (fromAronStateTypeMap.find(dto) != fromAronStateTypeMap.end())
        {
            bo = fromAronStateTypeMap[dto];
        }
        else
        {
            bo = eStateType::eUndefined;
        }
    }

    void toAron(skills::arondto::Statechart::StateType& dto, const eStateType& bo)
    {
        if (toAronStateTypeMap.find(bo) != toAronStateTypeMap.end())
        {
            dto.value = toAronStateTypeMap[bo].value;
        }
        else
        {
            dto.value = skills::arondto::Statechart::StateType::UNDEFINED;
        }
    }

    void fromAron(const skills::arondto::Statechart::ParameterMap& dto, StateParameterMap& bo)
    {
        // todo: implement
        //        for (auto const& [key, val] : dto.parameters)
        //        {
        // fromAron(val, ...)
        //            bo.insert(key, val);
        //        }
    }

    void toAron(skills::arondto::Statechart::ParameterMap& dto, const StateParameterMap& bo)
    {
        for (auto const& [key, val] : bo)
        {
            dto.parameters[key] = val->value->toString();
        }
    }

    void fromAron(const skills::arondto::Statechart::Transition& dto, ProfilerStatechartTransitionWithParameters& bo)
    {
        bo.processId = dto.processId;
        bo.sourceStateIdentifier = dto.sourceStateIdentifier;
        bo.targetStateIdentifier = dto.targetStateIdentifier;
        bo.eventName = dto.eventName;
        fromAron(dto.targetStateType, bo.targetStateType);
        fromAron(dto.inputParameters, bo.inputParameters);
        fromAron(dto.localParameters, bo.localParameters);
        fromAron(dto.outputParameters, bo.outputParameters);
    }

    void toAron(skills::arondto::Statechart::Transition& dto, const ProfilerStatechartTransitionWithParameters& bo)
    {
        dto.processId = bo.processId;
        dto.sourceStateIdentifier = bo.sourceStateIdentifier;
        dto.targetStateIdentifier = bo.targetStateIdentifier;
        dto.eventName = bo.eventName;
        toAron(dto.targetStateType, bo.targetStateType);
        toAron(dto.inputParameters, bo.inputParameters);
        toAron(dto.localParameters, bo.localParameters);
        toAron(dto.outputParameters, bo.outputParameters);
    }
}
