/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::armem

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include <RobotAPI/libraries/armem/core/wm/ice_conversions.h>
#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>

#include <iostream>
#include <SimoxUtility/algorithm/get_map_keys_values.h>


namespace armem = armarx::armem;
namespace aron = armarx::aron;


BOOST_AUTO_TEST_CASE(test_entity)
{
    armem::wm::Entity entity("entity");

    std::vector<armem::Time> expectedTimestamps;

    armem::wm::EntitySnapshot snapshot;
    snapshot.time() = armem::Time(armem::Duration::MilliSeconds(100));
    expectedTimestamps.push_back(snapshot.time());
    entity.addSnapshot(snapshot);

    snapshot.time() = armem::Time(armem::Duration::MilliSeconds(300));
    expectedTimestamps.push_back(snapshot.time());
    entity.addSnapshot(snapshot);

    armem::data::EntityPtr ice;
    armarx::toIce(ice, entity);

    BOOST_CHECK_EQUAL(ice->id.memoryName, entity.id().memoryName);
    BOOST_CHECK_EQUAL(ice->id.coreSegmentName, entity.id().coreSegmentName);
    BOOST_CHECK_EQUAL(ice->id.providerSegmentName, entity.id().providerSegmentName);
    BOOST_CHECK_EQUAL(ice->id.entityName, entity.id().entityName);

    BOOST_CHECK_EQUAL(ice->history.size(), entity.size());

    armem::wm::Entity entityOut;
    armarx::fromIce(ice, entityOut);

    BOOST_CHECK_EQUAL(entityOut.size(), entity.size());

    std::vector<armem::Time> timestamps = entityOut.getTimestamps();
    BOOST_CHECK_EQUAL_COLLECTIONS(timestamps.begin(), timestamps.end(), expectedTimestamps.begin(), expectedTimestamps.end());
}

