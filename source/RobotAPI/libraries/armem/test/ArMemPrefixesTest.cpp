/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::armem
 * @author     phesch ( ulila at student dot kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::armem

#define ARMARX_BOOST_TEST

#include <iostream>

#include <RobotAPI/Test.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/container_maps.h>


namespace armem = armarx::armem;

BOOST_AUTO_TEST_CASE(test_MemoryID_prefixes)
{
    std::map<armem::MemoryID, int> idMap;
    std::map<armem::MemoryID, std::vector<int>> idListMap;
    armem::MemoryID empty;
    armem::MemoryID complete("mem", "coreSeg", "provSeg", "entity", armarx::DateTime::Now(), 0);

    BOOST_TEST_CONTEXT(VAROUT(idMap))
    {
        BOOST_CHECK(armem::findMostSpecificEntryContainingID(idMap, empty) == idMap.end());
        BOOST_CHECK(armem::findMostSpecificEntryContainingID(idMap, complete) == idMap.end());
        BOOST_CHECK(armem::accumulateEntriesContainingID(idMap, empty).empty());
        BOOST_CHECK(armem::accumulateEntriesContainingID(idMap, complete).empty());

        BOOST_CHECK(armem::accumulateEntriesContainingID(idListMap, empty).empty());
        BOOST_CHECK(armem::accumulateEntriesContainingID(idListMap, complete).empty());
    }

    idMap[armem::MemoryID()] = 0;
    idMap.emplace("mem", 1);
    idMap.emplace("mem/coreSeg", 2);
    idMap.emplace("mem/coreSeg/provSeg", 3);
    idMap.emplace("mem/otherSeg/provSeg", 10);

    BOOST_TEST_CONTEXT(VAROUT(idMap))
    {
        auto it = armem::findMostSpecificEntryContainingID(idMap, empty);
        BOOST_CHECK(it != idMap.end());
        BOOST_CHECK_EQUAL(it->second, 0);

        it = armem::findMostSpecificEntryContainingID(idMap, complete.getCoreSegmentID());
        BOOST_CHECK(it != idMap.end());
        BOOST_CHECK_EQUAL(it->second, 2);

        it = armem::findMostSpecificEntryContainingID(idMap, complete);
        BOOST_CHECK(it != idMap.end());
        BOOST_CHECK_EQUAL(it->second, 3);

        BOOST_CHECK((armem::accumulateEntriesContainingID(idMap, empty) == std::vector<int>{0}));
        BOOST_CHECK((armem::accumulateEntriesContainingID(idMap, complete.getCoreSegmentID()) ==
                     std::vector<int>{2, 1, 0}));
        BOOST_CHECK(
            (armem::accumulateEntriesContainingID(idMap, complete) == std::vector<int>{3, 2, 1, 0}));
    }

    idListMap.emplace("mem", std::vector<int>{1, 2});
    idListMap.emplace("mem/coreSeg", std::vector<int>{3, 4, 5});
    idListMap.emplace("mem/coreSeg/provSeg", std::vector<int>{6, 7, 8});
    idListMap.emplace("mem/otherSeg/provSeg", std::vector<int>{9, 10, 11});

    BOOST_TEST_CONTEXT(VAROUT(idListMap))
    {
        BOOST_CHECK((armem::accumulateEntriesContainingID(idListMap, empty).empty()));
        BOOST_CHECK((armem::accumulateEntriesContainingID(idListMap, complete.getCoreSegmentID()) ==
                     std::vector<int>{3, 4, 5, 1, 2}));
        BOOST_CHECK((armem::accumulateEntriesContainingID(idListMap, complete) ==
                     std::vector<int>{6, 7, 8, 3, 4, 5, 1, 2}));
    }
}
