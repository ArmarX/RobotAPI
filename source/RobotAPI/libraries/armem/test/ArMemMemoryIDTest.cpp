/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::armem

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/error.h>

#include <iostream>


namespace armem = armarx::armem;


BOOST_AUTO_TEST_CASE(test_MemoryID_contains)
{
    armem::MemoryID general, specific;

    // Both empty.
    BOOST_TEST_CONTEXT(VAROUT(general) << " | " << VAROUT(specific))
    {
        BOOST_CHECK(armem::contains(general, specific));
    }

    // Diverging.
    general.memoryName = "a";
    specific.memoryName = "b";

    BOOST_TEST_CONTEXT(VAROUT(general) << " | " << VAROUT(specific))
    {
        BOOST_CHECK(not armem::contains(general, specific));
        BOOST_CHECK(not armem::contains(specific, general));
    }

    // Identical.
    specific.memoryName = "a";

    BOOST_TEST_CONTEXT(VAROUT(general) << " | " << VAROUT(specific))
    {
        BOOST_CHECK(armem::contains(general, specific));
        BOOST_CHECK(armem::contains(specific, general));
    }

    // general contains specific
    specific.coreSegmentName = "c";

    BOOST_TEST_CONTEXT(VAROUT(general) << " | " << VAROUT(specific))
    {
        BOOST_CHECK(armem::contains(general, specific));
        BOOST_CHECK(not armem::contains(specific, general));
    }

    // general contains specific
    specific.providerSegmentName = "d";

    BOOST_TEST_CONTEXT(VAROUT(general) << " | " << VAROUT(specific))
    {
        BOOST_CHECK(armem::contains(general, specific));
        BOOST_CHECK(not armem::contains(specific, general));
    }

    // Not well-defined ID - throw an exception.
    specific.coreSegmentName.clear();

    BOOST_TEST_CONTEXT(VAROUT(general) << " | " << VAROUT(specific))
    {
        BOOST_CHECK_THROW(armem::contains(general, specific), armem::error::InvalidMemoryID);
        BOOST_CHECK_THROW(armem::contains(specific, general), armem::error::InvalidMemoryID);
    }
}



BOOST_AUTO_TEST_CASE(test_MemoryID_from_to_string)
{
    armem::MemoryID idIn {"Memory/Core/Prov/entity/2810381/2"};

    BOOST_CHECK_EQUAL(idIn.memoryName, "Memory");
    BOOST_CHECK_EQUAL(idIn.coreSegmentName, "Core");
    BOOST_CHECK_EQUAL(idIn.providerSegmentName, "Prov");
    BOOST_CHECK_EQUAL(idIn.entityName, "entity");
    BOOST_CHECK_EQUAL(idIn.timestamp, armem::Time(armem::Duration::MicroSeconds(2810381)));
    BOOST_CHECK_EQUAL(idIn.instanceIndex, 2);


    BOOST_TEST_CONTEXT(VAROUT(idIn.str()))
    {
        armem::MemoryID idOut(idIn.str());
        BOOST_CHECK_EQUAL(idOut, idIn);
    }

    idIn.entityName = "KIT/Amicelli/0";  // Like an ObjectID
    BOOST_CHECK_EQUAL(idIn.entityName, "KIT/Amicelli/0");

    BOOST_TEST_CONTEXT(VAROUT(idIn.str()))
    {
        armem::MemoryID idOut(idIn.str());
        BOOST_CHECK_EQUAL(idOut.entityName, "KIT/Amicelli/0");
        BOOST_CHECK_EQUAL(idOut, idIn);
    }

    idIn = armem::MemoryID {"InThe\\/Mid/AtTheEnd\\//\\/AtTheStart/YCB\\/sugar\\/-1//2"};
    BOOST_CHECK_EQUAL(idIn.memoryName, "InThe/Mid");
    BOOST_CHECK_EQUAL(idIn.coreSegmentName, "AtTheEnd/");
    BOOST_CHECK_EQUAL(idIn.providerSegmentName, "/AtTheStart");
    BOOST_CHECK_EQUAL(idIn.entityName, "YCB/sugar/-1");
    BOOST_CHECK_EQUAL(idIn.timestamp, armem::Time::Invalid());
    BOOST_CHECK_EQUAL(idIn.instanceIndex, 2);

    BOOST_TEST_CONTEXT(VAROUT(idIn.str()))
    {
        armem::MemoryID idOut(idIn.str());
        BOOST_CHECK_EQUAL(idOut, idIn);
    }
}


BOOST_AUTO_TEST_CASE(test_MemoryID_copy_move_ctors_ops)
{
    const armem::MemoryID id("A/B/C/123/1"), moved("////1");  // int is not moved
    {
        const armem::MemoryID out(id);
        BOOST_CHECK_EQUAL(out, id);
    }
    {
        armem::MemoryID out;
        out = id;
        BOOST_CHECK_EQUAL(out, id);
    }
    {
        armem::MemoryID in = id;
        const armem::MemoryID out(std::move(in));
        BOOST_CHECK_EQUAL(in, moved);
        BOOST_CHECK_EQUAL(out, id);
    }
    {
        armem::MemoryID in = id;
        armem::MemoryID out;
        out = std::move(in);
        BOOST_CHECK_EQUAL(in, moved);
        BOOST_CHECK_EQUAL(out, id);
    }
}
