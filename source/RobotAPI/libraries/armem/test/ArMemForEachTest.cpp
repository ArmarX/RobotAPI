/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::armem

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include <RobotAPI/libraries/armem/core/Commit.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/operations.h>

#include <iostream>
#include <set>


namespace armem = armarx::armem;


template <class ValueT>
static
std::vector<ValueT> toVector(const std::set<ValueT>& set)
{
    return {set.begin(), set.end()};
}



BOOST_AUTO_TEST_CASE(test_forEach)
{
    using namespace armarx::armem;

    const MemoryID mid("memory");
    const std::set<MemoryID> emptySet;  // For comparison.

    wm::Memory memory(mid);
    std::set<MemoryID> cids, pids, eids, sids, iids;

    for (size_t c = 0; c <= 2; ++c)
    {
        const MemoryID cid = mid.withCoreSegmentName("core_" + std::to_string(c));
        cids.insert(cid);
        wm::CoreSegment& coreSeg = memory.addCoreSegment(cid.coreSegmentName);
        for (size_t p = 0; p <= c; ++p)
        {
            const MemoryID pid = cid.withProviderSegmentName("prov_" + std::to_string(p));
            pids.insert(pid);
            for (size_t e = 0; e <= p; ++e)
            {
                const MemoryID eid = pid.withEntityName("entity_" + std::to_string(e));
                eids.insert(eid);
                for (size_t s = 0; s <= e; ++s)
                {
                    const MemoryID sid = eid.withTimestamp(Time(Duration::MicroSeconds(int(s))));
                    sids.insert(sid);

                    EntityUpdate update;
                    update.entityID = eid;
                    update.timeCreated = sid.timestamp;
                    for (size_t i = 0; i <= s; ++i)
                    {
                        const MemoryID iid = sid.withInstanceIndex(int(i));
                        iids.insert(iid);
                        update.instancesData.emplace_back();
                    }
                    auto r = memory.update(update);

                    BOOST_CHECK(coreSeg.hasProviderSegment(pid.providerSegmentName));
                }
            }

            BOOST_CHECK_EQUAL(coreSeg.size(), p + 1);
        }
        BOOST_CHECK_EQUAL(memory.size(), c + 1);
    }

    BOOST_CHECK_GT(iids.size(), 0);
    BOOST_CHECK_GT(sids.size(), 0);
    BOOST_CHECK_GT(eids.size(), 0);
    BOOST_CHECK_GT(pids.size(), 0);
    BOOST_CHECK_GT(cids.size(), 0);

    BOOST_TEST_MESSAGE("Memory: \n" << armem::print(memory));

    memory.forEachInstance([&](const wm::EntityInstance & i) -> bool
    {
        BOOST_TEST_CONTEXT(wm::EntityInstance::getLevelName() << " " << i.id())
        {
            BOOST_TEST_INFO(toVector(iids));
            BOOST_CHECK_EQUAL(iids.count(i.id()), 1);
            iids.erase(i.id());
        }
        return true;
    });
    BOOST_TEST_CONTEXT(toVector(iids))
    {
        BOOST_CHECK_EQUAL(iids.size(), 0);
    }

    memory.forEachSnapshot([&](const wm::EntitySnapshot & s) -> bool
    {
        BOOST_TEST_CONTEXT(wm::EntitySnapshot::getLevelName() << " " << s.id())
        {
            BOOST_TEST_INFO(toVector(sids));
            BOOST_CHECK_EQUAL(sids.count(s.id()), 1);
            sids.erase(s.id());
        }
        return true;
    });
    BOOST_TEST_INFO(toVector(sids));
    BOOST_CHECK_EQUAL(sids.size(), 0);

    memory.forEachEntity([&](const wm::Entity & e) -> bool
    {
        BOOST_TEST_CONTEXT(wm::Entity::getLevelName() << " " << e.id())
        {
            BOOST_TEST_INFO(toVector(eids));
            BOOST_CHECK_EQUAL(eids.count(e.id()), 1);
            eids.erase(e.id());
        }
        return true;
    });
    BOOST_TEST_INFO(toVector(eids));
    BOOST_CHECK_EQUAL(eids.size(), 0);

    memory.forEachProviderSegment([&](const wm::ProviderSegment & p)  // -> bool
    {
        BOOST_TEST_CONTEXT(wm::ProviderSegment::getLevelName() << " " << p.id())
        {
            BOOST_TEST_INFO(toVector(pids));
            BOOST_CHECK_EQUAL(pids.count(p.id()), 1);
            pids.erase(p.id());
        }
        return true;
    });
    BOOST_TEST_INFO(toVector(pids));
    BOOST_CHECK_EQUAL(pids.size(), 0);

    memory.forEachCoreSegment([&](const wm::CoreSegment & c)  // -> bool
    {
        BOOST_TEST_CONTEXT(wm::CoreSegment::getLevelName() << " " << c.id())
        {
            BOOST_TEST_INFO(toVector(cids));
            BOOST_CHECK_EQUAL(cids.count(c.id()), 1);
            cids.erase(c.id());
        }
        return true;
    });
    BOOST_TEST_INFO(toVector(cids));
    BOOST_CHECK_EQUAL(cids.size(), 0);
}


BOOST_AUTO_TEST_CASE(test_forEach_non_bool_func)
{
    // Check whether this compiles + runs without break.

    armem::wm::Entity entity;
    entity.addSnapshot(armem::Time(armem::Duration::MicroSeconds(500)));
    entity.addSnapshot(armem::Time(armem::Duration::MicroSeconds(1500)));

    int i = 0;
    entity.forEachSnapshot([&i](const armem::wm::EntitySnapshot&) -> void
    {
        ++i;
    });
    BOOST_CHECK_EQUAL(i, 2);
}



BOOST_AUTO_TEST_CASE(test_forEachInstanceIn)
{
    armem::wm::EntitySnapshot snapshot(armem::Time::Now());
    snapshot.addInstance();
    snapshot.addInstance();

    int i = 0;
    auto count = [&i](const auto&) -> void
    {
        ++i;
    };

    {
        auto test = [&](auto&& snapshot)
        {
            armem::MemoryID id;

            // Count any.
            i = 0;
            snapshot.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 2);

            // Just index 0.
            id.instanceIndex = 0;
            i = 0;
            snapshot.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 1);

            // Just index 1.
            id.instanceIndex = 1;
            i = 0;
            snapshot.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 1);

            // Just index 2, but does not exist.
            id.instanceIndex = 2;
            i = 0;
            snapshot.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 0);
        };
        test(snapshot);
        test(const_cast<const armem::wm::EntitySnapshot&>(snapshot));
    }


    armem::wm::Entity entity("entity");
    entity.addSnapshot(snapshot);
    entity.addSnapshot(snapshot.time() + armem::Duration::Seconds(1)).addInstance();

    {
        auto test = [&](auto&& entity)
        {
            armem::MemoryID id;

            // Count any.
            i = 0;
            entity.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 3);

            // Count only one snapshot (with 2 instances).
            id.timestamp = snapshot.time();
            i = 0;
            entity.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 2);

            // Count only one snapshot and only one instance.
            id.instanceIndex = 1;
            i = 0;
            entity.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 1);

            // Count non-existing snapshot.
            id = {};
            id.timestamp = snapshot.time() - armem::Duration::Seconds(1);
            i = 0;
            entity.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 0);
        };
        test(entity);
        test(const_cast<const armem::wm::Entity&>(entity));
    }


    armem::wm::ProviderSegment provSeg("provider");
    provSeg.addEntity(entity);
    provSeg.addEntity(entity.name() + " 2")
            .addSnapshot(armem::Time::Now())
            .addInstance();

    {
        auto test = [&](auto&& provSeg)
        {
            armem::MemoryID id;

            // Count all (2+1) + 1 instances.
            i = 0;
            provSeg.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 4);

            // Count only one entity (with 2+1 instances).
            id.entityName = entity.name();
            i = 0;
            provSeg.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 3);

            // Count only one entity and one snapshot with 2 instances.
            id.timestamp = snapshot.time();
            i = 0;
            provSeg.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 2);

            // Count only one entity and one snapshot, 1 instance.
            id.instanceIndex = 1;
            i = 0;
            provSeg.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 1);

            // Count non-existing entity.
            id = {};
            id.entityName = "non existing";
            i = 0;
            provSeg.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 0);
        };
        test(provSeg);
        test(const_cast<const armem::wm::ProviderSegment&>(provSeg));
    }


    armem::wm::CoreSegment coreSeg("core");
    coreSeg.addProviderSegment(provSeg);
    coreSeg.addProviderSegment("other prov segment")
            .addEntity("other entity")
            .addSnapshot(armem::Time::Now())
            .addInstance();

    {
        auto test = [&](auto&& coreSeg)
        {
            armem::MemoryID id;

            // Count all ( (2+1) + 1) + 1 instances.
            i = 0;
            coreSeg.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 5);

            // Count only one provider segment (with (2+1) + 1 instances).
            id.providerSegmentName = provSeg.name();
            i = 0;
            coreSeg.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 4);

            id.entityName = entity.name();
            i = 0;
            coreSeg.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 3);

            id.timestamp = snapshot.time();
            i = 0;
            coreSeg.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 2);

            id.instanceIndex = 1;
            i = 0;
            coreSeg.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 1);

            // Count non-existing provider segment.
            id = {};
            id.providerSegmentName = "non existing";
            i = 0;
            coreSeg.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 0);
        };
        test(coreSeg);
        test(const_cast<const armem::wm::CoreSegment&>(coreSeg));
    }


    armem::wm::Memory memory("memory");
    memory.addCoreSegment(coreSeg);
    memory.addCoreSegment("other memory")
            .addProviderSegment("other provider segment")
            .addEntity("other entity")
            .addSnapshot(armem::Time::Now())
            .addInstance();

    {
        auto test = [&](auto&& memory)
        {
            armem::MemoryID id;

            // Count all ( ( (2+1) + 1) + 1 ) + 1 instances.
            i = 0;
            memory.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 6);

            // Count only one provider segment (with (2+1) + 1 instances).
            id.coreSegmentName = coreSeg.name();
            i = 0;
            memory.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 5);

            id.providerSegmentName = provSeg.name();
            i = 0;
            memory.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 4);

            id.entityName = entity.name();
            i = 0;
            memory.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 3);

            id.timestamp = snapshot.time();
            i = 0;
            memory.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 2);

            id.instanceIndex = 1;
            i = 0;
            memory.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 1);

            // Count non-existing provider segment.
            id = {};
            id.coreSegmentName = "non existing";
            i = 0;
            memory.forEachInstanceIn(id, count);
            BOOST_CHECK_EQUAL(i, 0);
        };
        test(memory);
        test(const_cast<const armem::wm::Memory&>(memory));
    }
}
