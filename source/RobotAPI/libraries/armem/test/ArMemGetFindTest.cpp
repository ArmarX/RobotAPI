/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::armem

#define ARMARX_BOOST_TEST

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/operations.h>

#include <RobotAPI/Test.h>

#include <iostream>


namespace armem = armarx::armem;
namespace wm = armarx::armem::wm;
namespace aron = armarx::aron;


namespace ArMemGetFindTest
{
    struct Fixture
    {
        wm::EntitySnapshot snapshot;
        wm::Entity entity;
        wm::ProviderSegment provSeg;
        wm::CoreSegment coreSeg;
        wm::Memory memory;

        armem::MemoryID instanceID;
        armem::MemoryID snapshotID;
        armem::MemoryID entityID;
        armem::MemoryID provSegID;
        armem::MemoryID coreSegID;
        armem::MemoryID memoryID;

        Fixture()
        {
            {
                snapshot.time() = armem::Time(armem::Duration::MicroSeconds(1000));
                snapshot.addInstance();
            }
            {
                entity.name() = "entity";
                entity.addSnapshot(snapshot);
            }
            {
                provSeg.name() = "provider segment";
                provSeg.addEntity(entity);
            }
            {
                coreSeg.name() = "core segment";
                coreSeg.addProviderSegment(provSeg);
            }
            {
                memory.name() = "memory";
                memory.addCoreSegment(coreSeg);
            }

            memoryID = memory.id();
            coreSegID = memoryID.withCoreSegmentName(coreSeg.name());
            provSegID = coreSegID.withProviderSegmentName(provSeg.name());
            entityID = provSegID.withEntityName(entity.name());
            snapshotID = entityID.withTimestamp(snapshot.time());
            instanceID = snapshotID.withInstanceIndex(0);
        }
        ~Fixture()
        {
        }


        template <class ParentT>
        void test_get_find_instance_by_id(ParentT&& parent)
        {
            _test_get_find_instance_by_id(const_cast<const ParentT&>(parent));
            _test_get_find_instance_by_id(const_cast<ParentT&>(parent));
        }
        template <class ParentT>
        void _test_get_find_instance_by_id(ParentT&& parent)
        {
            BOOST_TEST_CONTEXT("Parent: " << armem::print(parent))
            {
                BOOST_CHECK_EQUAL(parent.hasInstance(snapshotID.withInstanceIndex(0)), true);
                BOOST_CHECK_EQUAL(parent.hasInstance(snapshotID.withInstanceIndex(1)), false);

                BOOST_CHECK_NE(parent.findInstance(snapshotID.withInstanceIndex(0)), nullptr);
                BOOST_CHECK_EQUAL(parent.findInstance(snapshotID.withInstanceIndex(1)), nullptr);

                BOOST_CHECK_NO_THROW(parent.getInstance(snapshotID.withInstanceIndex(0)));
                BOOST_CHECK_THROW(parent.getInstance(snapshotID.withInstanceIndex(1)), armem::error::MissingEntry);
            }
        }

        template <class ParentT>
        void test_get_find_snapshot_by_id(ParentT&& parent)
        {
            _test_get_find_snapshot_by_id(const_cast<const ParentT&>(parent));
            _test_get_find_snapshot_by_id(const_cast<ParentT&>(parent));
        }
        template <class ParentT>
        void _test_get_find_snapshot_by_id(ParentT&& parent)
        {
            BOOST_TEST_CONTEXT("Parent: " << armem::print(parent))
            {
                BOOST_CHECK_EQUAL(parent.hasSnapshot(entityID.withTimestamp(armem::Time(armem::Duration::MicroSeconds(1000)))), true);
                BOOST_CHECK_EQUAL(parent.hasSnapshot(entityID.withTimestamp(armem::Time(armem::Duration::MicroSeconds(2000)))), false);

                BOOST_CHECK_NE(parent.findSnapshot(entityID.withTimestamp(armem::Time(armem::Duration::MicroSeconds(1000)))), nullptr);
                BOOST_CHECK_EQUAL(parent.findSnapshot(entityID.withTimestamp(armem::Time(armem::Duration::MicroSeconds(2000)))), nullptr);

                BOOST_CHECK_NO_THROW(parent.getSnapshot(entityID.withTimestamp(armem::Time(armem::Duration::MicroSeconds(1000)))));
                BOOST_CHECK_THROW(parent.getSnapshot(entityID.withTimestamp(armem::Time(armem::Duration::MicroSeconds(2000)))), armem::error::MissingEntry);
            }
        }


        template <class ParentT>
        void test_get_find_entity_by_id(ParentT&& parent)
        {
            _test_get_find_entity_by_id(const_cast<const ParentT&>(parent));
            _test_get_find_entity_by_id(const_cast<ParentT&>(parent));
        }
        template <class ParentT>
        void _test_get_find_entity_by_id(ParentT&& parent)
        {
            BOOST_TEST_CONTEXT("Parent: " << armem::print(parent))
            {
                BOOST_CHECK_EQUAL(parent.hasEntity(provSegID.withEntityName("entity")), true);
                BOOST_CHECK_EQUAL(parent.hasEntity(provSegID.withEntityName("other entity")), false);

                BOOST_CHECK_NE(parent.findEntity(provSegID.withEntityName("entity")), nullptr);
                BOOST_CHECK_EQUAL(parent.findEntity(provSegID.withEntityName("other entity")), nullptr);

                BOOST_CHECK_NO_THROW(parent.getEntity(provSegID.withEntityName("entity")));
                BOOST_CHECK_THROW(parent.getEntity(provSegID.withEntityName("other entity")), armem::error::MissingEntry);
            }
        }

        template <class ParentT>
        void test_get_find_provider_segment_by_id(ParentT&& parent)
        {
            _test_get_find_provider_segment_by_id(const_cast<const ParentT&>(parent));
            _test_get_find_provider_segment_by_id(const_cast<ParentT&>(parent));
        }
        template <class ParentT>
        void _test_get_find_provider_segment_by_id(ParentT&& parent)
        {
            BOOST_TEST_CONTEXT("Parent: " << armem::print(parent))
            {
                BOOST_CHECK_EQUAL(parent.hasProviderSegment(provSegID.withProviderSegmentName("provider segment")), true);
                BOOST_CHECK_EQUAL(parent.hasProviderSegment(provSegID.withProviderSegmentName("other provider segment")), false);

                BOOST_CHECK_NE(parent.findProviderSegment(provSegID.withProviderSegmentName("provider segment")), nullptr);
                BOOST_CHECK_EQUAL(parent.findProviderSegment(provSegID.withProviderSegmentName("other provider segment")), nullptr);

                BOOST_CHECK_NO_THROW(parent.getProviderSegment(provSegID.withProviderSegmentName("provider segment")));
                BOOST_CHECK_THROW(parent.getProviderSegment(provSegID.withProviderSegmentName("other provider segment")), armem::error::MissingEntry);
            }
        }

        template <class ParentT>
        void test_get_find_core_segment_by_id(ParentT&& parent)
        {
            _test_get_find_core_segment_by_id(const_cast<const ParentT&>(parent));
            _test_get_find_core_segment_by_id(const_cast<ParentT&>(parent));
        }
        template <class ParentT>
        void _test_get_find_core_segment_by_id(ParentT&& parent)
        {
            BOOST_TEST_CONTEXT("Parent: " << armem::print(parent))
            {
                BOOST_CHECK_EQUAL(parent.hasCoreSegment(provSegID.withCoreSegmentName("core segment")), true);
                BOOST_CHECK_EQUAL(parent.hasCoreSegment(provSegID.withCoreSegmentName("other core segment")), false);

                BOOST_CHECK_NE(parent.findCoreSegment(provSegID.withCoreSegmentName("core segment")), nullptr);
                BOOST_CHECK_EQUAL(parent.findCoreSegment(provSegID.withCoreSegmentName("other core segment")), nullptr);

                BOOST_CHECK_NO_THROW(parent.getCoreSegment(provSegID.withCoreSegmentName("core segment")));
                BOOST_CHECK_THROW(parent.getCoreSegment(provSegID.withCoreSegmentName("other core segment")), armem::error::MissingEntry);
            }
        }
    };
}


BOOST_FIXTURE_TEST_SUITE(ArMemGetFindTest, Fixture)



BOOST_AUTO_TEST_CASE(test_snapshot_get_find_instance_by_key)
{
    BOOST_CHECK_EQUAL(snapshot.hasInstance(0), true);
    BOOST_CHECK_EQUAL(snapshot.hasInstance(1), false);

    BOOST_CHECK_NE(snapshot.findInstance(0), nullptr);
    BOOST_CHECK_EQUAL(snapshot.findInstance(1), nullptr);

    BOOST_CHECK_NO_THROW(snapshot.getInstance(0));
    BOOST_CHECK_THROW(snapshot.getInstance(1), armem::error::MissingEntry);
}


BOOST_AUTO_TEST_CASE(test_entity_get_find_snapshot_by_key)
{
    BOOST_CHECK_EQUAL(entity.hasSnapshot(armem::Time(armem::Duration::MicroSeconds(1000))), true);
    BOOST_CHECK_EQUAL(entity.hasSnapshot(armem::Time(armem::Duration::MicroSeconds(2000))), false);

    BOOST_CHECK_NE(entity.findSnapshot(armem::Time(armem::Duration::MicroSeconds(1000))), nullptr);
    BOOST_CHECK_EQUAL(entity.findSnapshot(armem::Time(armem::Duration::MicroSeconds(2000))), nullptr);

    BOOST_CHECK_NO_THROW(entity.getSnapshot(armem::Time(armem::Duration::MicroSeconds(1000))));
    BOOST_CHECK_THROW(entity.getSnapshot(armem::Time(armem::Duration::MicroSeconds(2000))), armem::error::MissingEntry);
}


BOOST_AUTO_TEST_CASE(test_provider_segment_get_find_entity_by_key)
{
    BOOST_CHECK_EQUAL(provSeg.hasEntity("entity"), true);
    BOOST_CHECK_EQUAL(provSeg.hasEntity("other entity"), false);

    BOOST_CHECK_NE(provSeg.findEntity("entity"), nullptr);
    BOOST_CHECK_EQUAL(provSeg.findEntity("other entity"), nullptr);

    BOOST_CHECK_NO_THROW(provSeg.getEntity("entity"));
    BOOST_CHECK_THROW(provSeg.getEntity("other entity"), armem::error::MissingEntry);
}


BOOST_AUTO_TEST_CASE(test_core_segment_get_find_provider_segment_by_key)
{
    BOOST_CHECK_EQUAL(coreSeg.hasProviderSegment("provider segment"), true);
    BOOST_CHECK_EQUAL(coreSeg.hasProviderSegment("other provider segment"), false);

    BOOST_CHECK_NE(coreSeg.findProviderSegment("provider segment"), nullptr);
    BOOST_CHECK_EQUAL(coreSeg.findProviderSegment("other provider segment"), nullptr);

    BOOST_CHECK_NO_THROW(coreSeg.getProviderSegment("provider segment"));
    BOOST_CHECK_THROW(coreSeg.getProviderSegment("other provider segment"), armem::error::MissingEntry);
}


BOOST_AUTO_TEST_CASE(test_memory_get_find_core_segment_by_key)
{
    BOOST_CHECK_EQUAL(memory.hasCoreSegment("core segment"), true);
    BOOST_CHECK_EQUAL(memory.hasCoreSegment("other core segment"), false);

    BOOST_CHECK_NE(memory.findCoreSegment("core segment"), nullptr);
    BOOST_CHECK_EQUAL(memory.findCoreSegment("other core segment"), nullptr);

    BOOST_CHECK_NO_THROW(memory.getCoreSegment("core segment"));
    BOOST_CHECK_THROW(memory.getCoreSegment("other core segment"), armem::error::MissingEntry);
}



BOOST_AUTO_TEST_CASE(test_snapshot_get_find_instance_by_id)
{
    test_get_find_instance_by_id(snapshot);
}
BOOST_AUTO_TEST_CASE(test_entity_get_find_instance_by_id)
{
    test_get_find_instance_by_id(entity);
}
BOOST_AUTO_TEST_CASE(test_provider_segment_get_find_instance_by_id)
{
    test_get_find_instance_by_id(provSeg);
}
BOOST_AUTO_TEST_CASE(test_core_segment_get_find_instance_by_id)
{
    test_get_find_instance_by_id(coreSeg);
}
BOOST_AUTO_TEST_CASE(test_memory_get_find_instance_by_id)
{
    test_get_find_instance_by_id(memory);
}


BOOST_AUTO_TEST_CASE(test_entity_get_find_snapshot_by_id)
{
    test_get_find_snapshot_by_id(entity);
}
BOOST_AUTO_TEST_CASE(test_provider_segment_get_find_snapshot_by_id)
{
    test_get_find_snapshot_by_id(provSeg);
}
BOOST_AUTO_TEST_CASE(test_core_segment_get_find_snapshot_by_id)
{
    test_get_find_snapshot_by_id(coreSeg);
}
BOOST_AUTO_TEST_CASE(test_memory_get_find_snapshot_by_id)
{
    test_get_find_snapshot_by_id(memory);
}


BOOST_AUTO_TEST_CASE(test_provider_segment_get_find_entity_by_id)
{
    test_get_find_entity_by_id(provSeg);
}
BOOST_AUTO_TEST_CASE(test_core_segment_get_find_entity_by_id)
{
    test_get_find_entity_by_id(coreSeg);
}
BOOST_AUTO_TEST_CASE(test_memory_get_find_entity_by_id)
{
    test_get_find_entity_by_id(memory);
}


BOOST_AUTO_TEST_CASE(test_core_segment_get_find_provider_segment_by_id)
{
    test_get_find_provider_segment_by_id(coreSeg);
}
BOOST_AUTO_TEST_CASE(test_memory_get_find_provider_segment_by_id)
{
    test_get_find_provider_segment_by_id(memory);
}

BOOST_AUTO_TEST_CASE(test_memory_get_find_core_segment_by_id)
{
    test_get_find_core_segment_by_id(memory);
}



BOOST_AUTO_TEST_SUITE_END()
