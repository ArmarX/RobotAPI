/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::armem

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>

#include <ArmarXCore/core/time/ice_conversions.h>

#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/client/query/query_fns.h>

#include <iostream>


namespace armem = armarx::armem;
namespace query = armarx::armem::query::data;


namespace ArMemQueryBuilderTest
{
    struct Fixture
    {
        Fixture()
        {
        }
        ~Fixture()
        {
        }
    };
}


BOOST_FIXTURE_TEST_SUITE(ArMemQueryBuilderTest, ArMemQueryBuilderTest::Fixture)



BOOST_AUTO_TEST_CASE(test_all_all)
{
    armem::client::query::Builder qb;
    qb
    .coreSegments().all()
    .providerSegments().all()
    .entities().all()
    .snapshots().all();

    query::MemoryQuerySeq memoryQueries = qb.buildMemoryQueries();

    BOOST_REQUIRE_EQUAL(memoryQueries.size(), 1);
    query::MemoryQueryPtr memoryQuery = memoryQueries.front();
    BOOST_CHECK(query::memory::AllPtr::dynamicCast(memoryQuery));

    BOOST_REQUIRE_EQUAL(memoryQuery->coreSegmentQueries.size(), 1);
    query::CoreSegmentQueryPtr coreSegQuery = memoryQuery->coreSegmentQueries.front();
    BOOST_CHECK(query::core::AllPtr::dynamicCast(coreSegQuery));

    BOOST_REQUIRE_EQUAL(coreSegQuery->providerSegmentQueries.size(), 1);
    query::ProviderSegmentQueryPtr provSegQuery = coreSegQuery->providerSegmentQueries.front();
    BOOST_CHECK(query::provider::AllPtr::dynamicCast(provSegQuery));

    BOOST_REQUIRE_EQUAL(provSegQuery->entityQueries.size(), 1);
    query::EntityQueryPtr entityQuery = provSegQuery->entityQueries.front();
    BOOST_CHECK(query::entity::AllPtr::dynamicCast(entityQuery));
}


BOOST_AUTO_TEST_CASE(test_mixed)
{
    using namespace armem::client::query_fns;

    std::vector<std::string> entityNames = {"one", "two"};

    armem::client::query::Builder qb(armem::query::DataMode::WithData);
    qb
    .coreSegments(withName("core"))
    .providerSegments(withName("provider"))
    .entities(withName(entityNames.at(0)),
              withName(entityNames.at(1)))
    .snapshots(latest(),
               atTime(armem::Time(armem::Duration::MilliSeconds(3000))),
               indexRange(5, -10),
               timeRange(armem::Time(armem::Duration::MilliSeconds(1000)),
                         armem::Time(armem::Duration::MilliSeconds(2000))))
    ;


    armem::client::QueryInput input = qb.buildQueryInput();
    const query::MemoryQuerySeq& memoryQueries = input.memoryQueries;

    BOOST_REQUIRE_EQUAL(memoryQueries.size(), 1);
    query::MemoryQueryPtr memoryQuery = memoryQueries.front();
    {
        query::memory::SinglePtr single = query::memory::SinglePtr::dynamicCast(memoryQuery);
        BOOST_REQUIRE(single);
        BOOST_CHECK_EQUAL(single->coreSegmentName, "core");
    }

    BOOST_REQUIRE_EQUAL(memoryQuery->coreSegmentQueries.size(), 1);
    query::CoreSegmentQueryPtr coreSegQuery = memoryQuery->coreSegmentQueries.front();
    {
        query::core::SinglePtr single = query::core::SinglePtr::dynamicCast(coreSegQuery);
        BOOST_REQUIRE(single);
        BOOST_CHECK_EQUAL(single->providerSegmentName, "provider");
    }


    BOOST_REQUIRE_EQUAL(coreSegQuery->providerSegmentQueries.size(), entityNames.size());
    for (size_t i = 0; i < coreSegQuery->providerSegmentQueries.size(); ++i)
    {
        const query::ProviderSegmentQueryPtr& provSegQuery = coreSegQuery->providerSegmentQueries[i];
        {
            query::provider::SinglePtr single = query::provider::SinglePtr::dynamicCast(provSegQuery);
            BOOST_REQUIRE(single);
            BOOST_CHECK_EQUAL(single->entityName, entityNames.at(i));
        }

        BOOST_CHECK_EQUAL(provSegQuery->entityQueries.size(), 4);
        auto it = provSegQuery->entityQueries.begin();
        {
            auto latest = query::entity::SinglePtr::dynamicCast(*it);
            BOOST_REQUIRE(latest);
            BOOST_CHECK_EQUAL(latest->timestamp.timeSinceEpoch.microSeconds, armem::Time::Invalid().toMicroSecondsSinceEpoch());
            ++it;
        }
        {
            auto atTime = query::entity::SinglePtr::dynamicCast(*it);
            BOOST_REQUIRE(atTime);
            BOOST_CHECK_EQUAL(atTime->timestamp, armarx::toIce<armem::dto::Time>(armem::Time(armem::Duration::MilliSeconds(3000))));
            ++it;
        }
        {
            auto indexRange = query::entity::IndexRangePtr::dynamicCast(*it);
            BOOST_REQUIRE(indexRange);
            BOOST_CHECK_EQUAL(indexRange->first, 5);
            BOOST_CHECK_EQUAL(indexRange->last, -10);
            ++it;
        }
        {
            auto timeRange = query::entity::TimeRangePtr::dynamicCast(*it);
            BOOST_REQUIRE(timeRange);
            BOOST_CHECK_EQUAL(timeRange->minTimestamp, armarx::toIce<armem::dto::Time>(armem::Time(armem::Duration::MilliSeconds(1000))));
            BOOST_CHECK_EQUAL(timeRange->maxTimestamp, armarx::toIce<armem::dto::Time>(armem::Time(armem::Duration::MilliSeconds(2000))));
            ++it;
        }
    }
}



BOOST_AUTO_TEST_CASE(test_branched_hierarchy)
{
    armem::client::query::Builder qb;

    // Common root tree
    armem::client::query::ProviderSegmentSelector& provider = qb
            .coreSegments().withName("core")
            .providerSegments().withName("provider");

    // 1st entity branch
    provider
    .entities().withName("first")
    .snapshots().all();

    // 2nd entity branch
    provider
    .entities().withNamesMatching(".*_regex")
    .snapshots().latest();


    BOOST_TEST_MESSAGE(std::numeric_limits<long>::min());
    armem::client::QueryInput input = qb.buildQueryInput();
    const query::ProviderSegmentQuerySeq& provQueries = input.memoryQueries.front()->coreSegmentQueries.front()->providerSegmentQueries;

    BOOST_CHECK_EQUAL(provQueries.size(), 2);
    auto it = provQueries.begin();
    {
        auto single = query::provider::SinglePtr::dynamicCast(*it);
        BOOST_REQUIRE(single);
        single->entityName = "first";

        BOOST_CHECK_EQUAL(single->entityQueries.size(), 1);
        auto snapshotAll = query::entity::AllPtr::dynamicCast(single->entityQueries.at(0));
        BOOST_CHECK(snapshotAll);

        ++it;
    }
    {
        auto regex = query::provider::RegexPtr::dynamicCast(*it);
        BOOST_REQUIRE(regex);
        regex->entityNameRegex = ".*_regex";

        BOOST_CHECK_EQUAL(regex->entityQueries.size(), 1);
        auto snapshotLatest = query::entity::SinglePtr::dynamicCast(regex->entityQueries.at(0));
        BOOST_CHECK(snapshotLatest);
        BOOST_CHECK_EQUAL(snapshotLatest->timestamp.timeSinceEpoch.microSeconds, armem::Time::Invalid().toMicroSecondsSinceEpoch());

        ++it;
    }
}


BOOST_AUTO_TEST_SUITE_END()
