#pragma once

#include <RobotAPI/libraries/armem/core/Time.h>

#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>
#include <RobotAPI/interface/armem/server/ActionsInterface.h>
#include <RobotAPI/interface/armem/server/PredictingMemoryInterface.h>
#include <RobotAPI/interface/armem/server/ReadingMemoryInterface.h>
#include <RobotAPI/interface/armem/server/WritingMemoryInterface.h>

#include <ArmarXCore/core/logging/Logging.h>

#include <map>
#include <string>


namespace armarx::armem::mns
{

    /**
     * @brief A registry for memory servers.
     */
    class Registry : armarx::Logging
    {
    public:

        Registry(const std::string& logTag = "MemoryNameSystem Registry");


        /// Indicates whether a server entry for that name exists.
        bool hasServer(const std::string& memoryName) const;


        /**
         * @brief Register a new memory server or update an existing entry.
         *
         * Causes threads waiting in `waitForMemory()` to resume if the respective
         * memory server was added.
         */
        virtual dto::RegisterServerResult registerServer(const dto::RegisterServerInput& input);
        /**
         * @brief Remove a server entry.
         */
        dto::RemoveServerResult removeServer(const dto::RemoveServerInput& input);


        /**
         * @brief Gets a server entry, if it is available.
         */
        dto::ResolveServerResult resolveServer(const dto::ResolveServerInput& input);

        dto::GetAllRegisteredServersResult getAllRegisteredServers();


    public:

        /// Information about a memory entry.
        struct ServerInfo
        {
            std::string name;
            mns::dto::MemoryServerInterfaces server;
            Time timeRegistered;
        };

        /// The registered memories.
        std::map<std::string, ServerInfo> servers;

    };

}
