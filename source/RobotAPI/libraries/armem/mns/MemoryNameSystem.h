#pragma once

#include "Registry.h"

#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/LightweightRemoteGuiComponentPlugin.h>

#include <map>
#include <string>


namespace armarx::armem::mns
{

    class MemoryNameSystem : public Registry
    {
    public:

        using WaitForServerFuturePtr = AMD_MemoryNameSystemInterface_waitForServerPtr;


    public:

        /**
         * @brief Store the call in a container for later response.
         */
        void waitForServer_async(
            const AMD_MemoryNameSystemInterface_waitForServerPtr& future,
            const dto::WaitForServerInput& input);

        void waitForServer_processOnce();

        dto::RegisterServerResult registerServer(const dto::RegisterServerInput& input) override;


        /// Builds a RemoteGui grid containing information about registered memories.
        armarx::RemoteGui::Client::GridLayout RemoteGui_buildInfoGrid();


    public:

        /// Queued calls to `waitForServer`.
        std::map<std::string, std::vector<WaitForServerFuturePtr>> waitForServerFutures;

    };

}
