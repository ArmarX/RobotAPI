#include "MemoryNameSystem.h"


namespace armarx::armem::mns
{

    // dto::WaitForServerResult MemoryNameSystem::waitForServer(const dto::WaitForServerInput& input)
    void MemoryNameSystem::waitForServer_async(
        const AMD_MemoryNameSystemInterface_waitForServerPtr& future,
        const dto::WaitForServerInput& input)
    {
        waitForServerFutures[input.name].push_back(future);
        waitForServer_processOnce();
    }


    void MemoryNameSystem::waitForServer_processOnce()
    {
        for (auto it = waitForServerFutures.begin(); it != waitForServerFutures.end();)
        {
            auto& [name, futures] = *it;
            if (auto st = servers.find(name); st != servers.end())
            {
                ServerInfo& info = st->second;

                dto::WaitForServerResult result;
                result.success = true;
                result.server = info.server;

                // Send responses and remove entry.
                for (auto& future : futures)
                {
                    future->ice_response(result);
                }
                it = waitForServerFutures.erase(it);
            }
            else
            {
                ++it;  // Skip.
            }
        }
    }
    
    dto::RegisterServerResult MemoryNameSystem::registerServer(const dto::RegisterServerInput& input) 
    {
        const auto result = Registry::registerServer(input);
        waitForServer_processOnce();

        return result;
    }


    armarx::RemoteGui::Client::GridLayout MemoryNameSystem::RemoteGui_buildInfoGrid()
    {
        using namespace armarx::RemoteGui::Client;

        GridLayout grid;

        int row = 0;
        grid.add(Label("Memory Name"), {row, 0})
        .add(Label("Component Name"), {row, 1})
        .add(Label("R/W/P/A"), {row, 2})
        .add(Label("Registration Time"), {row, 3})
        ;
        row++;

        for (const auto& [name, info] : servers)
        {
            ARMARX_CHECK_EQUAL(name, info.name);
            std::string componentName = "";
            std::string mode = "";
            if (info.server.reading)
            {
                componentName = info.server.reading->ice_getIdentity().name;
                mode += "R";
            }
            if (info.server.writing)
            {
                componentName = info.server.writing->ice_getIdentity().name;
                mode += "W";
            }
            if (info.server.prediction)
            {
                componentName = info.server.prediction->ice_getIdentity().name;
                mode += "P";
            }
            if (info.server.actions)
            {
                componentName = info.server.actions->ice_getIdentity().name;
                mode += "A";
            }

            int col = 0;
            grid.add(Label(name), {row, col});
            ++col;

            grid.add(Label(componentName), {row, col});
            ++col;

            grid.add(Label(mode), {row, col});
            ++col;

            grid.add(Label(armem::toDateTimeMilliSeconds(info.timeRegistered, 0)), {row, col});
            ++col;

            ++row;
        }

        return grid;
    }

}
