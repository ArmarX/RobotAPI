#include "Registry.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/interface/armem/server/ActionsInterface.h>


namespace armarx::armem::mns
{

    Registry::Registry(const std::string& logTag)
    {
        armarx::Logging::setTag(logTag);
    }


    bool Registry::hasServer(const std::string& memoryName) const
    {
        return servers.count(memoryName) > 0;
    }


    dto::RegisterServerResult Registry::registerServer(const dto::RegisterServerInput& input)
    {
        dto::RegisterServerResult result;

        if (not(input.server.reading or input.server.writing))
        {
            result.success = false;
            std::stringstream ss;
            ss << "Could not register the memory server '" << input.name << "'."
               << "\nGiven proxies are null."
               << "\nIf you want to remove a memory server, use `removeServer()`.";
            result.errorMessage = ss.str();
            return result;
        }

        auto it = servers.find(input.name);
        if (it == servers.end())
        {
            it = servers.emplace(input.name, ServerInfo{}).first;
        }
        else if (not input.existOk)
        {
            result.success = false;
            std::stringstream ss;
            ss << "Could not register the memory '" << input.name << "'."
               << "\nMemory '" << input.name << "' is already registered. "
               << "\nIf this is ok, set 'existOk' to true when registering the memory.";
            result.errorMessage = ss.str();
            return result;
        }

        ServerInfo& info = it->second;
        info.name = input.name;
        info.server = input.server;
        info.timeRegistered = armem::Time::Now();
        ARMARX_DEBUG << "Registered memory '" << info.name << "'.";

        result.success = true;
        return result;
    }


    dto::RemoveServerResult Registry::removeServer(const dto::RemoveServerInput& input)
    {
        dto::RemoveServerResult result;

        if (auto it = servers.find(input.name); it != servers.end())
        {
            result.success = true;
            servers.erase(it);

            ARMARX_DEBUG << "Removed memory '" << input.name << "'.";
        }
        else if (!input.notExistOk)
        {
            result.success = false;
            std::stringstream ss;
            ss << "Could not remove the memory '" << input.name << "."
               << "\nMemory '" << input.name << "' is not registered. "
               << "\nIf this is ok, set 'notExistOk' to true when removing the memory.";
            result.errorMessage = ss.str();
        }

        return result;
    }


    template <class Prx>
    bool isAvailable(const Prx& proxy)
    {
        if (proxy)
        {
            try
            {
                proxy->ice_ping();
                return true;
            }
            catch (const Ice::Exception&)
            {
            }
        }
        return false;
    }


    dto::GetAllRegisteredServersResult
    Registry::getAllRegisteredServers()
    {
        dto::GetAllRegisteredServersResult result;
        result.success = true;
        result.errorMessage = "";

        for (const auto& [name, info] : servers)
        {
            if (isAvailable(info.server.reading) or isAvailable(info.server.writing))
            {
                result.servers[name] = info.server;
            }
        }

        return result;
    }


    dto::ResolveServerResult Registry::resolveServer(const dto::ResolveServerInput& input)
    {
        dto::ResolveServerResult result;
        try
        {
            ServerInfo& info = servers.at(input.name);

            result.success = true;
            result.server = info.server;

            ARMARX_DEBUG << "Resolved memory name '" << input.name << "'.";
        }
        catch (const std::out_of_range&)
        {
            result.success = false;
            std::stringstream ss;
            ss << "Could not resolve the memory name '" << input.name << "'."
               << "\nServer '" << input.name << "' is not registered.";
            result.errorMessage = ss.str();
        }

        return result;
    }

}
