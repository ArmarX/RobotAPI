#include "PluginUser.h"
#include "Plugin.h"


namespace armarx::armem::mns::plugins
{

    PluginUser::PluginUser()
    {
        addPlugin(plugin);
    }


    dto::RegisterServerResult PluginUser::registerServer(const dto::RegisterServerInput& input, const Ice::Current&)
    {
        std::scoped_lock lock(mnsMutex);
        dto::RegisterServerResult result = plugin->mns.registerServer(input);
        return result;
    }


    dto::RemoveServerResult PluginUser::removeServer(const dto::RemoveServerInput& input, const Ice::Current&)
    {
        std::scoped_lock lock(mnsMutex);
        dto::RemoveServerResult result = plugin->mns.removeServer(input);
        return result;
    }


    dto::GetAllRegisteredServersResult
    PluginUser::getAllRegisteredServers(const Ice::Current&)
    {
        std::scoped_lock lock(mnsMutex);
        dto::GetAllRegisteredServersResult result = plugin->mns.getAllRegisteredServers();
        return result;
    }


    dto::ResolveServerResult PluginUser::resolveServer(const dto::ResolveServerInput& input, const Ice::Current&)
    {
        std::scoped_lock lock(mnsMutex);
        dto::ResolveServerResult result = plugin->mns.resolveServer(input);
        return result;
    }


    // dto::WaitForServerResult PluginUser::waitForServer(const dto::WaitForServerInput& input, const Ice::Current&)
    void PluginUser::waitForServer_async(
        const AMD_MemoryNameSystemInterface_waitForServerPtr& future,
        const dto::WaitForServerInput& input,
        const Ice::Current&)
    {
        std::scoped_lock lock(mnsMutex);
        plugin->mns.waitForServer_async(future, input);
    }


    MemoryNameSystem& PluginUser::mns()
    {
        return plugin->mns;
    }


    const MemoryNameSystem& PluginUser::mns() const
    {
        return plugin->mns;
    }

}
