#pragma once

#include <RobotAPI/libraries/armem/mns/MemoryNameSystem.h>

#include <ArmarXCore/core/ComponentPlugin.h>


namespace armarx::armem::mns::plugins
{

    class Plugin : public armarx::ComponentPlugin
    {
    public:

        using armarx::ComponentPlugin::ComponentPlugin;


    public:

        MemoryNameSystem mns;

    };

}
