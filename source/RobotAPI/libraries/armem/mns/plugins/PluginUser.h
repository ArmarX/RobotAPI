#pragma once

#include <RobotAPI/libraries/armem/mns/MemoryNameSystem.h>

#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>

#include <ArmarXCore/core/ManagedIceObject.h>

#include <mutex>


namespace armarx::armem::mns::plugins
{
    class Plugin;


    class PluginUser :
        virtual public ManagedIceObject
        , virtual public mns::MemoryNameSystemInterface
    {
    public:

        PluginUser();

        // mns::MemoryNameSystemInterface interface
    public:

        dto::RegisterServerResult registerServer(const dto::RegisterServerInput& input, const Ice::Current& = Ice::emptyCurrent) override;
        dto::RemoveServerResult removeServer(const dto::RemoveServerInput& input, const Ice::Current& = Ice::emptyCurrent) override;

        dto::GetAllRegisteredServersResult getAllRegisteredServers(const Ice::Current&) override;

        dto::ResolveServerResult resolveServer(const dto::ResolveServerInput& input, const Ice::Current& = Ice::emptyCurrent) override;

        // Uses Asynchronous Method Dispatch (AMD)
        void waitForServer_async(
            const AMD_MemoryNameSystemInterface_waitForServerPtr& future,
            const dto::WaitForServerInput& input,
            const Ice::Current& = Ice::emptyCurrent) override;


    protected:

        std::mutex mnsMutex;
        MemoryNameSystem& mns();
        const MemoryNameSystem& mns() const;


    private:

        plugins::Plugin* plugin = nullptr;

    };

}
