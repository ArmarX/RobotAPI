#pragma once

#include <vector>

#include <RobotAPI/interface/aron.h>

#include "../core/MemoryID.h"
#include "../core/Time.h"


namespace armarx::armem::detail
{
    struct SuccessHeader
    {
        operator bool() const
        {
            return success;
        }

        bool success;
        std::string errorMessage;
    };


    template <class Ice>
    void toIce(Ice& ice, const SuccessHeader& header)
    {
        ice.success = header.success;
        ice.errorMessage = header.errorMessage;
    }
    template <class Ice>
    void fromIce(const Ice& ice, SuccessHeader& header)
    {
        header.success = ice.success;
        header.errorMessage = ice.errorMessage;
    }
}
