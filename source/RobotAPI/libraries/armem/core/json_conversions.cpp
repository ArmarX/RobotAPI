#include "json_conversions.h"

#include <RobotAPI/libraries/armem/core/MemoryID.h>


void armarx::armem::to_json(nlohmann::json& j, const MemoryID& bo)
{
    j["memoryName"] = bo.memoryName;
    j["coreSegmentName"] = bo.coreSegmentName;
    j["providerSegmentName"] = bo.providerSegmentName;
    j["entityName"] = bo.entityName;
    j["timestamp_usec"] = bo.timestamp.toMicroSecondsSinceEpoch();
    j["timestamp_datetime"] = toDateTimeMilliSeconds(bo.timestamp);
    j["instanceIndex"] = bo.instanceIndex;
}

void armarx::armem::from_json(const nlohmann::json& j, MemoryID& bo)
{
    j.at("memoryName").get_to(bo.memoryName);
    j.at("coreSegmentName").get_to(bo.coreSegmentName);
    j.at("providerSegmentName").get_to(bo.providerSegmentName);
    j.at("entityName").get_to(bo.entityName);
    bo.timestamp = Time(Duration::MicroSeconds(j.at("timestamp_usec").get<int64_t>()));
    // j.at("timestamp_datetime").get_to(toDateTimeMilliSeconds(bo.timestamp));
    j.at("instanceIndex").get_to(bo.instanceIndex);
}
