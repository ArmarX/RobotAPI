#include "operations.h"

#include <RobotAPI/libraries/armem/core/Time.h>

#include <SimoxUtility/algorithm/string/string_tools.h>


namespace armarx
{

    std::vector<aron::data::DictPtr>
    armem::getAronData(const wm::EntitySnapshot& snapshot)
    {
        std::vector<aron::data::DictPtr> result;
        snapshot.forEachChild([&result](const wm::EntityInstance & instance)
        {
            result.push_back(instance.data());
            return true;
        });
        return result;
    }


    armem::EntityUpdate armem::toEntityUpdate(const wm::EntitySnapshot& snapshot)
    {
        EntityUpdate up;
        up.entityID = snapshot.id().getEntityID();
        up.timeCreated = snapshot.time();
        up.instancesData = getAronData(snapshot);
        return up;
    }


    template <class DataT>
    static std::string makeLine(int depth, const DataT& data, std::optional<size_t> size = std::nullopt)
    {
        std::stringstream ss;
        while (depth > 1)
        {
            ss << "|  ";
            depth--;
        }
        if (depth > 0)
        {
            ss << "+- ";
        }
        ss << simox::alg::capitalize_words(DataT::getLevelName());
        ss << " '" << simox::alg::capitalize_words(data.getKeyString()) << "'";
        if (size.has_value())
        {
            ss << " (size " << size.value() << ")";
        }
        ss << "\n";
        return ss.str();
    }


    template <class DataT>
    static std::string _print(const DataT& data, int maxDepth, int depth)
    {
        std::stringstream ss;
        ss << makeLine(depth, data, data.size());
        if (maxDepth < 0 || maxDepth > 0)
        {
            data.forEachChild([&ss, maxDepth, depth](const auto& instance)
            {
                ss << armem::print(instance, maxDepth - 1, depth + 1);
            });
        }
        return ss.str();
    }

    std::string armem::print(const wm::Memory& data, int maxDepth, int depth)
    {
        return _print(data, maxDepth, depth);
    }
    std::string armem::print(const wm::CoreSegment& data, int maxDepth, int depth)
    {
        return _print(data, maxDepth, depth);
    }
    std::string armem::print(const wm::ProviderSegment& data, int maxDepth, int depth)
    {
        return _print(data, maxDepth, depth);
    }
    std::string armem::print(const wm::Entity& data, int maxDepth, int depth)
    {
        return _print(data, maxDepth, depth);
    }
    std::string armem::print(const wm::EntitySnapshot& data, int maxDepth, int depth)
    {
        return _print(data, maxDepth, depth);
    }

    std::string armem::print(const wm::EntityInstance& data, int maxDepth, int depth)
    {
        (void) maxDepth;
        std::stringstream ss;
        ss << makeLine(depth, data);
        return ss.str();
    }
}
