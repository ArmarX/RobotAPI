#pragma once

#include "wm/memory_definitions.h"

#include <string>


/*
 * Functions performing some "advanced" operations on the memory
 * data structure, which are using their public API but should not
 * be part of it.
 */

namespace armarx::armem
{

    std::vector<aron::data::DictPtr>
    getAronData(const wm::EntitySnapshot& snapshot);


    EntityUpdate
    toEntityUpdate(const wm::EntitySnapshot& snapshot);


    template <class ContainerT>
    Commit
    toCommit(const ContainerT& container)
    {
        Commit commit;
        container.forEachSnapshot([&commit](const wm::EntitySnapshot & snapshot)
        {
            commit.add(toEntityUpdate(snapshot));
            return true;
        });
        return commit;
    }


    template <class ContainerT>
    const typename ContainerT::EntityInstanceT*
    findFirstInstance(const ContainerT& container)
    {
        const typename ContainerT::EntityInstanceT* instance = nullptr;
        container.forEachInstance([&instance](const wm::EntityInstance & i)
        {
            instance = &i;
            return false;
        });
        return instance;
    }


    template <class ContainerT>
    std::vector<MemoryID> getEntityIDs(const ContainerT& container)
    {
        std::vector<armem::MemoryID> entityIDs;

        container.forEachEntity([&entityIDs](const auto& entity)
        {
            entityIDs.push_back(entity.id());
        });

        return entityIDs;
    }


    std::string print(const wm::Memory& data, int maxDepth = -1, int depth = 0);
    std::string print(const wm::CoreSegment& data, int maxDepth = -1, int depth = 0);
    std::string print(const wm::ProviderSegment& data, int maxDepth = -1, int depth = 0);
    std::string print(const wm::Entity& data, int maxDepth = -1, int depth = 0);
    std::string print(const wm::EntitySnapshot& data, int maxDepth = -1, int depth = 0);
    std::string print(const wm::EntityInstance& data, int maxDepth = -1, int depth = 0);

}

