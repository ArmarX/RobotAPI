#pragma once

#include <map>
#include <string>

#include "ProviderSegmentBase.h"
#include "detail/AronTyped.h"
#include "detail/MemoryContainerBase.h"
#include "detail/iteration_mixins.h"
#include "detail/lookup_mixins.h"
#include "detail/Predictive.h"

#include <ArmarXCore/core/logging/Logging.h>

namespace armarx::armem::base
{

    /**
     * @brief Data of a core segment containing multiple provider segments.
     */
    template <class _ProviderSegmentT, class _Derived>
    class CoreSegmentBase :
        public detail::MemoryContainerBase<std::map<std::string, _ProviderSegmentT>, _Derived>
        , public detail::AronTyped
        , public detail::PredictiveContainer<_Derived>
        , public detail::ForEachEntityInstanceMixin<_Derived>
        , public detail::ForEachEntitySnapshotMixin<_Derived>
        , public detail::ForEachEntityMixin<_Derived>
        , public detail::GetFindInstanceMixin<_Derived>
        , public detail::GetFindSnapshotMixin<_Derived>
        , public detail::GetFindEntityMixin<_Derived>
    {
        using Base = detail::MemoryContainerBase<std::map<std::string, _ProviderSegmentT>, _Derived>;

    public:

        using typename Base::DerivedT;
        using typename Base::ContainerT;

        using ProviderSegmentT = _ProviderSegmentT;
        using EntityT = typename ProviderSegmentT::EntityT;
        using EntitySnapshotT = typename EntityT::EntitySnapshotT;
        using EntityInstanceT = typename EntitySnapshotT::EntityInstanceT;

        using ChildT = ProviderSegmentT;


        struct UpdateResult
        {
            armarx::armem::UpdateType coreSegmentUpdateType;
            armarx::armem::UpdateType providerSegmentUpdateType;
            armarx::armem::UpdateType entityUpdateType;
            MemoryID id;
            std::vector<EntitySnapshotT> removedSnapshots;

            UpdateResult() = default;
            UpdateResult(const typename ProviderSegmentT::UpdateResult& c) :
                providerSegmentUpdateType(c.providerSegmentUpdateType),
                entityUpdateType(c.entityUpdateType),
                id(c.id),
                removedSnapshots(c.removedSnapshots)
            {}
        };


    public:

        CoreSegmentBase()
        {
        }
        explicit CoreSegmentBase(const std::string& name,
                                 aron::type::ObjectPtr aronType = nullptr,
                                 const std::vector<PredictionEngine>& predictionEngines = {}) :
            CoreSegmentBase(name, MemoryID(), aronType, predictionEngines)
        {
        }
        explicit CoreSegmentBase(const std::string& name,
                                 const MemoryID& parentID,
                                 aron::type::ObjectPtr aronType = nullptr,
                                 const std::vector<PredictionEngine>& predictionEngines = {}) :
            CoreSegmentBase(parentID.withCoreSegmentName(name), aronType, predictionEngines)
        {
        }
        explicit CoreSegmentBase(const MemoryID& id,
                                 aron::type::ObjectPtr aronType = nullptr,
                                 const std::vector<PredictionEngine>& predictionEngines = {}) :
            Base(id), AronTyped(aronType), detail::PredictiveContainer<_Derived>(predictionEngines)
        {
        }

        CoreSegmentBase(const CoreSegmentBase& other) = default;
        CoreSegmentBase(CoreSegmentBase&& other) = default;
        CoreSegmentBase& operator=(const CoreSegmentBase& other) = default;
        CoreSegmentBase& operator=(CoreSegmentBase&& other) = default;


        // READ ACCESS

        // Get key
        inline std::string& name()
        {
            return this->id().coreSegmentName;
        }
        inline const std::string& name() const
        {
            return this->id().coreSegmentName;
        }


        // Has child by key
        bool hasProviderSegment(const std::string& name) const
        {
            return this->findProviderSegment(name) != nullptr;
        }
        // Has child by memory ID
        bool hasProviderSegment(const MemoryID& providerSegmentID) const
        {
            return this->findProviderSegment(providerSegmentID) != nullptr;
        }


        // Find child by key
        ProviderSegmentT* findProviderSegment(const std::string& name)
        {
            return detail::findChildByKey(name, this->_container);
        }
        const ProviderSegmentT* findProviderSegment(const std::string& name) const
        {
            return detail::findChildByKey(name, this->_container);
        }

        // Get child by key
        ProviderSegmentT& getProviderSegment(const std::string& name)
        {
            return detail::getChildByKey(name, this->_container, *this);
        }
        const ProviderSegmentT& getProviderSegment(const std::string& name) const
        {
            return detail::getChildByKey(name, this->_container, *this);
        }

        // Find child by MemoryID
        ProviderSegmentT* findProviderSegment(const MemoryID& providerSegmentID)
        {
            detail::checkHasProviderSegmentName(providerSegmentID);
            return this->findProviderSegment(providerSegmentID.providerSegmentName);
        }
        const ProviderSegmentT* findProviderSegment(const MemoryID& providerSegmentID) const
        {
            detail::checkHasProviderSegmentName(providerSegmentID);
            return this->findProviderSegment(providerSegmentID.providerSegmentName);
        }

        // Get child by MemoryID
        ProviderSegmentT& getProviderSegment(const MemoryID& providerSegmentID)
        {
            detail::checkHasProviderSegmentName(providerSegmentID);
            return this->getProviderSegment(providerSegmentID.providerSegmentName);
        }
        const ProviderSegmentT& getProviderSegment(const MemoryID& providerSegmentID) const
        {
            detail::checkHasProviderSegmentName(providerSegmentID);
            return this->getProviderSegment(providerSegmentID.providerSegmentName);
        }

        // get/findInstance are provided by GetFindInstanceMixin
        // get/findSnapshot are provided by GetFindSnapshotMixin
        // get/findEntity are provided by GetFindEntityMixin
        using detail::GetFindEntityMixin<DerivedT>::hasEntity;
        using detail::GetFindEntityMixin<DerivedT>::findEntity;

        // Search all provider segments for the first matching entity.

        bool hasEntity(const std::string& entityName) const
        {
            return this->findEntity(entityName) != nullptr;
        }
        EntityT* findEntity(const std::string& entityName)
        {
            return _findEntity(*this, entityName);
        }
        const EntityT* findEntity(const std::string& entityName) const
        {
            return _findEntity(*this, entityName);
        }


        // ITERATION

        /**
         * @param func Function like: bool process(ProviderSegmentT& provSeg)
         */
        template <class ProviderSegmentFunctionT>
        bool forEachProviderSegment(ProviderSegmentFunctionT&& func)
        {
            return this->forEachChild(func);
        }
        /**
         * @param func Function like: bool process(const ProviderSegmentT& provSeg)
         */
        template <class ProviderSegmentFunctionT>
        bool forEachProviderSegment(ProviderSegmentFunctionT&& func) const
        {
            return this->forEachChild(func);
        }

        // forEachEntity() is provided by ForEachEntityMixin.
        // forEachSnapshot() is provided by ForEachEntitySnapshotMixin.
        // forEachInstance() is provided by ForEachEntityInstanceMixin.


        /**
         * @param func Function like void process(EntityInstanceT& instance)>
         */
        template <class InstanceFunctionT>
        bool forEachInstanceIn(const MemoryID& id, InstanceFunctionT&& func)
        {
            return detail::forEachInstanceIn(
                        id, func, *this,
                        id.hasProviderSegmentName(),
                        id.hasProviderSegmentName() ? this->findProviderSegment(id.providerSegmentName) : nullptr);
        }
        /**
         * @param func Function like void process(EntityInstanceT& instance)>
         */
        template <class InstanceFunctionT>
        bool forEachInstanceIn(const MemoryID& id, InstanceFunctionT&& func) const
        {
            return detail::forEachInstanceIn(
                        id, func, *this,
                        id.hasProviderSegmentName(),
                        id.hasProviderSegmentName() ? this->findProviderSegment(id.providerSegmentName) : nullptr);
        }


        // Get child keys
        std::vector<std::string> getProviderSegmentNames() const
        {
            return simox::alg::get_keys(this->_container);
        }


        // MODIFICATION

        /**
         * @brief Updates an entity's history.
         *
         * Missing entity entries are added before updating.
         */
        UpdateResult update(const EntityUpdate& update)
        {
            this->_checkContainerName(update.entityID.coreSegmentName, this->name());

            auto [inserted, provSeg] = _addProviderSegmentIfMissing(update.entityID.providerSegmentName);


            // Update entry.
            UpdateResult ret(provSeg->update(update));
            if (inserted)
            {
                ret.coreSegmentUpdateType = UpdateType::InsertedNew;
            }
            else
            {
                ret.coreSegmentUpdateType = UpdateType::UpdatedExisting;
            }
            return ret;
        }


        template <class OtherDerivedT>
        void append(const OtherDerivedT& other)
        {
            other.forEachProviderSegment([this](const auto& provSeg)
            {
                auto it = this->_container.find(provSeg.name());
                if (it == this->_container.end())
                {
                    it = this->_container.emplace(provSeg.name(), this->id().withProviderSegmentName(provSeg.name())).first;
                }
                it->second.append(provSeg);
                return true;
            });
        }

        /**
         * @brief Add an empty provider segment with the given name,
         *        optional provider segment type and prediction engines.
         * @param name The segment name.
         * @param providerSegmentType The provider type. If nullptr, the core segment type is used.
         * @param predictionEngines The prediction engines supported by the provider segment (optional).
         * @return The added provider segment.
         */
        ProviderSegmentT&
        addProviderSegment(const std::string& name,
                           aron::type::ObjectPtr providerSegmentType = nullptr,
                           const std::vector<PredictionEngine>& predictionEngines = {})
        {
            aron::type::ObjectPtr type = providerSegmentType ? providerSegmentType : this->aronType();
            return this->_derived().addProviderSegment(name, name, type, predictionEngines);
        }

        /// Copy and insert a provider segment.
        ProviderSegmentT& addProviderSegment(const ProviderSegmentT& providerSegment)
        {
            return this->_derived().addProviderSegment(providerSegment.name(), ProviderSegmentT(providerSegment));
        }

        /// Move and insert a provider segment.
        ProviderSegmentT& addProviderSegment(ProviderSegmentT&& providerSegment)
        {
            const std::string name = providerSegment.name();  // Copy before move.
            return this->_derived().addProviderSegment(name, std::move(providerSegment));
        }

        /// Insert a provider segment in-place.
        template <class ...Args>
        ProviderSegmentT& addProviderSegment(const std::string& name, Args... args)
        {
            ChildT& child = this->template _addChild<ChildT>(name, args...);
            child.id() = this->id().withProviderSegmentName(name);
            return child;
        }


        // MISC

        bool equalsDeep(const DerivedT& other) const
        {
            //std::cout << "CoreSegment::equalsDeep" << std::endl;
            if (this->size() != other.size())
            {
                return false;
            }
            for (const auto& [key, provider] : this->_container)
            {
                if (not other.hasProviderSegment(key))
                {
                    return false;
                }
                if (not provider.equalsDeep(other.getProviderSegment(key)))
                {
                    return false;
                }
            }
            return true;
        }

        static std::string getLevelName()
        {
            return "core segment";
        }

        std::string getKeyString() const
        {
            return this->name();
        }


    protected:

        template <class ParentT>
        static
        auto*
        _findEntity(ParentT&& parent, const std::string& entityName)
        {
            decltype(parent.findEntity(entityName)) result = nullptr;
            parent.forEachProviderSegment([&result, &entityName](auto & provSeg)
            {
                result = provSeg.findEntity(entityName);
                return result == nullptr;  // Keep going if null, break if not null.
            });
            return result;
        }


        std::pair<bool, ProviderSegmentT*>
        _addProviderSegmentIfMissing(const std::string& providerSegmentName)
        {
            ProviderSegmentT* provSeg;

            auto it = this->_container.find(providerSegmentName);
            if (it == this->_container.end())
            {
                if (_addMissingProviderSegmentDuringUpdate)
                {
                    // Insert into map.
                    provSeg = &addProviderSegment(providerSegmentName);
                    return {true, provSeg};
                }
                else
                {
                    throw error::MissingEntry::create<ProviderSegmentT>(providerSegmentName, *this);
                }
            }
            else
            {
                provSeg = &it->second;
                return {false, provSeg};
            }
        }


    private:

        bool _addMissingProviderSegmentDuringUpdate = true;

    };

}
