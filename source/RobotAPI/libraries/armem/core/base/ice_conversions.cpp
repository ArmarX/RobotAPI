#include "ice_conversions.h"

#include <ArmarXCore/core/time/ice_conversions.h>
#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>

#include <RobotAPI/libraries/armem/core/ice_conversions.h>


namespace armarx::armem::base
{

    void detail::toIceItem(data::detail::MemoryItem& ice, const armem::base::detail::MemoryItem& item)
    {
        toIce(ice.id, item.id());
    }
    void detail::fromIceItem(const data::detail::MemoryItem& ice, armem::base::detail::MemoryItem& item)
    {
        fromIce(ice.id, item.id());
    }


    void detail::toIce(aron::data::dto::DictPtr& ice, const aron::data::DictPtr& data)
    {
        ice = data ? data->toAronDictDTO() : nullptr;
    }
    void detail::fromIce(const aron::data::dto::DictPtr& ice, aron::data::DictPtr& data)
    {
        if (ice)
        {
            data = aron::data::Dict::FromAronDictDTO(ice);
        }
        else
        {
            data = nullptr;
        };
    }

    void detail::toIce(aron::type::dto::GenericTypePtr& ice, const aron::type::ObjectPtr& bo)
    {
        ice = bo ? bo->toAronDTO() : nullptr;
    }
    void detail::fromIce(const aron::type::dto::GenericTypePtr& ice, aron::type::ObjectPtr& bo)
    {
        bo = ice
             ? aron::type::Object::DynamicCastAndCheck(aron::type::Variant::FromAronDTO(*ice))
             : nullptr;
    }

}
namespace armarx::armem
{

    void base::toIce(data::EntityInstanceMetadata& ice, const EntityInstanceMetadata& metadata)
    {
        ice.confidence = metadata.confidence;
        toIce(ice.timeArrived, metadata.timeArrived);
        toIce(ice.timeCreated, metadata.timeCreated);
        toIce(ice.timeSent, metadata.timeSent);
    }
    void base::fromIce(const data::EntityInstanceMetadata& ice, EntityInstanceMetadata& metadata)
    {
        metadata.confidence = ice.confidence;
        fromIce(ice.timeArrived, metadata.timeArrived);
        fromIce(ice.timeCreated, metadata.timeCreated);
        fromIce(ice.timeSent, metadata.timeSent);
    }


    void base::toIce(data::EntityInstanceMetadataPtr& ice, const EntityInstanceMetadata& metadata)
    {
        armarx::toIce<data::EntityInstanceMetadata>(ice, metadata);
    }
    void base::fromIce(const data::EntityInstanceMetadataPtr& ice, EntityInstanceMetadata& metadata)
    {
        armarx::fromIce<data::EntityInstanceMetadata>(ice, metadata);
    }

}
