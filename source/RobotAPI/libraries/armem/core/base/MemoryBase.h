#pragma once

#include <map>
#include <string>

#include "CoreSegmentBase.h"
#include "detail/MemoryContainerBase.h"
#include "detail/iteration_mixins.h"
#include "detail/lookup_mixins.h"
#include "detail/Predictive.h"


namespace armarx::armem::base
{

    /**
     * @brief Data of a memory consisting of multiple core segments.
     */
    template <class _CoreSegmentT, class _Derived>
    class MemoryBase :
        public detail::MemoryContainerBase<std::map<std::string, _CoreSegmentT>, _Derived>
        , public detail::PredictiveContainer<_Derived>
        , public detail::ForEachEntityInstanceMixin<_Derived>
        , public detail::ForEachEntitySnapshotMixin<_Derived>
        , public detail::ForEachEntityMixin<_Derived>
        , public detail::ForEachProviderSegmentMixin<_Derived>
        , public detail::GetFindInstanceMixin<_Derived>
        , public detail::GetFindSnapshotMixin<_Derived>
        , public detail::GetFindEntityMixin<_Derived>
        , public detail::GetFindProviderSegmentMixin<_Derived>
    {
        using Base = detail::MemoryContainerBase<std::map<std::string, _CoreSegmentT>, _Derived>;

    public:

        using typename Base::DerivedT;
        using typename Base::ContainerT;

        using CoreSegmentT = _CoreSegmentT;
        using ProviderSegmentT = typename CoreSegmentT::ProviderSegmentT;
        using EntityT = typename ProviderSegmentT::EntityT;
        using EntitySnapshotT = typename EntityT::EntitySnapshotT;
        using EntityInstanceT = typename EntitySnapshotT::EntityInstanceT;

        using ChildT = CoreSegmentT;


        struct UpdateResult
        {
            armarx::armem::UpdateType memoryUpdateType;
            armarx::armem::UpdateType coreSegmentUpdateType;
            armarx::armem::UpdateType providerSegmentUpdateType;
            armarx::armem::UpdateType entityUpdateType;
            MemoryID id;
            std::vector<EntitySnapshotT> removedSnapshots;

            UpdateResult() = default;
            UpdateResult(const typename CoreSegmentT::UpdateResult& c) :
                coreSegmentUpdateType(c.coreSegmentUpdateType),
                providerSegmentUpdateType(c.providerSegmentUpdateType),
                entityUpdateType(c.entityUpdateType),
                id(c.id),
                removedSnapshots(c.removedSnapshots)
            {}
        };

    public:

        MemoryBase()
        {
        }
        explicit MemoryBase(const std::string& name,
                            const std::vector<PredictionEngine>& predictionEngines = {}) :
            MemoryBase(MemoryID().withMemoryName(name), predictionEngines)
        {
        }
        explicit MemoryBase(const MemoryID& id,
                            const std::vector<PredictionEngine>& predictionEngines = {}) :
            Base(id), detail::PredictiveContainer<_Derived>(predictionEngines)
        {
        }

        MemoryBase(const MemoryBase& other) = default;
        MemoryBase(MemoryBase&& other) = default;
        MemoryBase& operator=(const MemoryBase& other) = default;
        MemoryBase& operator=(MemoryBase&& other) = default;


        // READ ACCESS

        // Get key
        inline std::string& name()
        {
            return this->id().memoryName;
        }
        inline const std::string& name() const
        {
            return this->id().memoryName;
        }


        // Has child by key
        bool hasCoreSegment(const std::string& name) const
        {
            return this->findCoreSegment(name) != nullptr;
        }
        // Has child by MemoryID
        bool hasCoreSegment(const MemoryID& coreSegmentID) const
        {
            return this->findCoreSegment(coreSegmentID) != nullptr;
        }

        // Find child by key
        CoreSegmentT* findCoreSegment(const std::string& name)
        {
            return detail::findChildByKey(name, this->_container);
        }
        const CoreSegmentT* findCoreSegment(const std::string& name) const
        {
            return detail::findChildByKey(name, this->_container);
        }

        // Get child by key
        CoreSegmentT& getCoreSegment(const std::string& name)
        {
            return detail::getChildByKey(name, this->_container, *this);
        }
        const CoreSegmentT& getCoreSegment(const std::string& name) const
        {
            return detail::getChildByKey(name, this->_container, *this);
        }

        // Find child by MemoryID
        CoreSegmentT* findCoreSegment(const MemoryID& coreSegmentID)
        {
            detail::checkHasCoreSegmentName(coreSegmentID);
            return this->findCoreSegment(coreSegmentID.coreSegmentName);
        }
        const CoreSegmentT* findCoreSegment(const MemoryID& coreSegmentID) const
        {
            detail::checkHasCoreSegmentName(coreSegmentID);
            return this->findCoreSegment(coreSegmentID.coreSegmentName);
        }

        // Get child by MemoryID
        CoreSegmentT& getCoreSegment(const MemoryID& coreSegmentID)
        {
            detail::checkHasCoreSegmentName(coreSegmentID);
            return this->getCoreSegment(coreSegmentID.coreSegmentName);
        }
        const CoreSegmentT& getCoreSegment(const MemoryID& coreSegmentID) const
        {
            detail::checkHasCoreSegmentName(coreSegmentID);
            return this->getCoreSegment(coreSegmentID.coreSegmentName);
        }

        // get/findInstance are provided by GetFindInstanceMixin
        // get/findSnapshot are provided by GetFindSnapshotMixin
        // get/findEntity are provided by GetFindEntityMixin
        // get/findProviderSegment are provided by GetFindProviderSegmentMixin



        // ITERATION

        /**
         * @param func Function like: bool process(CoreSegmentT& coreSeg)
         */
        template <class CoreSegmentFunctionT>
        bool forEachCoreSegment(CoreSegmentFunctionT&& func)
        {
            return this->forEachChild(func);
        }
        /**
         * @param func Function like: bool process(const CoreSegmentT& coreSeg)
         */
        template <class CoreSegmentFunctionT>
        bool forEachCoreSegment(CoreSegmentFunctionT&& func) const
        {
            return this->forEachChild(func);
        }

        // forEachProviderSegment() is provided by ForEachProviderSegmentMixin.
        // forEachEntity() is provided by ForEachEntityMixin.
        // forEachSnapshot() is provided by ForEachEntitySnapshotMixin.
        // forEachInstance() is provided by ForEachEntityInstanceMixin.


        /**
         * @param func Function like void process(EntityInstanceT& instance)>
         */
        template <class InstanceFunctionT>
        bool forEachInstanceIn(const MemoryID& id, InstanceFunctionT&& func)
        {
            return detail::forEachInstanceIn(
                        id, func, *this,
                        id.hasCoreSegmentName(),
                        id.hasCoreSegmentName() ? this->findCoreSegment(id.coreSegmentName) : nullptr);
        }
        /**
         * @param func Function like void process(EntityInstanceT& instance)>
         */
        template <class InstanceFunctionT>
        bool forEachInstanceIn(const MemoryID& id, InstanceFunctionT&& func) const
        {
            return detail::forEachInstanceIn(
                        id, func, *this,
                        id.hasCoreSegmentName(),
                        id.hasCoreSegmentName() ? this->findCoreSegment(id.coreSegmentName) : nullptr);
        }


        std::vector<std::string> getCoreSegmentNames() const
        {
            return simox::alg::get_keys(this->_container);
        }


        // MODIFICATION

        /**
         * @brief Add an empty core segment with the given name, type and prediction engines.
         * @param name The core segment name.
         * @param coreSegmentType The core segment type (optional).
         * @param predictionEngines The prediction engines supported by the core segment (optional).
         * @return The added core segment.
         */
        CoreSegmentT&
        addCoreSegment(const std::string& name,
                       aron::type::ObjectPtr coreSegmentType = nullptr,
                       const std::vector<PredictionEngine>& predictionEngines = {})
        {
            return this->_derived().addCoreSegment(name, name, coreSegmentType, predictionEngines);
        }

        /// Copy and insert a core segment.
        CoreSegmentT& addCoreSegment(const CoreSegmentT& coreSegment)
        {
            return this->_derived().addCoreSegment(coreSegment.name(), CoreSegmentT(coreSegment));
        }

        /// Move and insert a core segment.
        CoreSegmentT& addCoreSegment(CoreSegmentT&& coreSegment)
        {
            const std::string name = coreSegment.name();  // Copy before move.
            return this->_derived().addCoreSegment(name, std::move(coreSegment));
        }

        /// Move and insert a core segment.
        template <class ...Args>
        CoreSegmentT& addCoreSegment(const std::string& name, Args... args)
        {
            CoreSegmentT& child = this->template _addChild<ChildT>(name, args...);
            child.id() = this->id().withCoreSegmentName(name);
            return child;
        }


        /**
         * @brief Store all updates in `commit`.
         * @param commit The commit.
         * @return The resulting memory IDs.
         */
        std::vector<UpdateResult> update(const Commit& commit)
        {
            std::vector<UpdateResult> results;
            for (const EntityUpdate& update : commit.updates)
            {
                results.push_back(this->update(update));
            }
            return results;
        }

        /**
         * @brief Store the given update.
         * @param update The update.
         * @return The resulting entity snapshot's ID.
         */
        UpdateResult update(const EntityUpdate& update)
        {
            this->_checkContainerName(update.entityID.memoryName, this->name());

            auto [inserted, coreSeg] = _addCoreSegmentIfMissing(update.entityID.coreSegmentName);

            // Update entry.
            UpdateResult ret(coreSeg->update(update));
            if (inserted)
            {
                ret.memoryUpdateType = UpdateType::InsertedNew;
            }
            else
            {
                ret.memoryUpdateType = UpdateType::UpdatedExisting;
            }
            return ret;
        }


        /**
         * @brief Merge another memory into this one. Append all data
         * @param m The other memory
         */
        template <class OtherDerivedT>
        void append(const OtherDerivedT& other)
        {
            other.forEachCoreSegment([this](const auto& coreSeg)
            {
                auto it = this->_container.find(coreSeg.name());
                if (it == this->_container.end())
                {
                    it = this->_container.emplace(coreSeg.name(), this->id().withCoreSegmentName(coreSeg.name())).first;
                }
                it->second.append(coreSeg);
                return true;
            });
        }

        bool equalsDeep(const MemoryBase& other) const
        {
            //std::cout << "Memory::equalsDeep" << std::endl;
            if (this->size() != other.size())
            {
                return false;
            }
            for (const auto& [key, core] : this->_container)
            {
                if (not other.hasCoreSegment(key))
                {
                    return false;
                }
                if (not core.equalsDeep(other.getCoreSegment(key)))
                {
                    return false;
                }
            }
            return true;
        }

        static std::string getLevelName()
        {
            return "memory";
        }

        std::string getKeyString() const
        {
            return this->name();
        }


    protected:

        std::pair<bool, CoreSegmentT*> _addCoreSegmentIfMissing(const std::string& coreSegmentName)
        {
            CoreSegmentT* coreSeg = nullptr;

            auto it = this->_container.find(coreSegmentName);
            if (it == this->_container.end())
            {
                if (_addMissingCoreSegmentDuringUpdate)
                {
                    // Insert into map.
                    coreSeg = &addCoreSegment(coreSegmentName);
                    return {true, coreSeg};
                }
                else
                {
                    throw error::MissingEntry::create<CoreSegmentT>(coreSegmentName, *this);
                }
            }
            else
            {
                coreSeg = &it->second;
                return {false, coreSeg};
            }
        }


    public:

        // ToDo: Remove from base structure - this should be specific to the server versions.
        // If necessary at all.
        bool _addMissingCoreSegmentDuringUpdate = false;

    };
}
