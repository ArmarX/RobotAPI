#pragma once

#include <RobotAPI/libraries/aron/core/type/variant/forward_declarations.h>


namespace armarx::armem::base::detail
{

    /**
     * @brief Something with a specific ARON type.
     */
    class AronTyped
    {
    public:

        explicit AronTyped(aron::type::ObjectPtr aronType = nullptr);


        bool hasAronType() const;
        aron::type::ObjectPtr& aronType();
        aron::type::ObjectPtr aronType() const;


    protected:

        /// The expected Aron type. May be nullptr, in which case no type information is available.
        aron::type::ObjectPtr _aronType;

    };


}
