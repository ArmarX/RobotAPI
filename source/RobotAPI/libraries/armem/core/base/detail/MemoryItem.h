#pragma once

#include <string>

#include <RobotAPI/libraries/armem/core/MemoryID.h>


namespace armarx::armem::base::detail
{

    /**
     * @brief Base class of memory classes on different levels.
     */
    class MemoryItem
    {
    public:

        MemoryItem();
        explicit MemoryItem(const MemoryID& id);

        MemoryItem(const MemoryItem& other) = default;
        MemoryItem(MemoryItem&& other) = default;
        MemoryItem& operator=(const MemoryItem& other) = default;
        MemoryItem& operator=(MemoryItem&& other) = default;


        inline MemoryID& id()
        {
            return _id;
        }
        inline const MemoryID& id() const
        {
            return _id;
        }


    protected:

        // Protected so we get a compile error if someone tries to destruct a MemoryItem
        // instead of a derived type.
        ~MemoryItem();


    protected:

        MemoryID _id;

    };

}
