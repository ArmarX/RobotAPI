#pragma once


namespace armarx::armem::base::detail
{

    template <class DerivedT, class ThisT>
    DerivedT&
    derived(ThisT* t)
    {
        return static_cast<DerivedT&>(*t);
    }


    template <class DerivedT, class ThisT>
    const DerivedT&
    derived(const ThisT* t)
    {
        return static_cast<const DerivedT&>(*t);
    }

}
