#include "lookup_mixins.h"

#include <RobotAPI/libraries/armem/core/error/ArMemError.h>


namespace armarx::armem::base
{

    void detail::checkHasInstanceIndex(const MemoryID& instanceID)
    {
        if (not instanceID.hasInstanceIndex())
        {
            throw armem::error::InvalidMemoryID(instanceID, "Instance ID has no instance index.");
        }
    }


    void detail::checkHasTimestamp(const MemoryID& snapshotID)
    {
        if (not snapshotID.hasTimestamp())
        {
            throw armem::error::InvalidMemoryID(snapshotID, "Snapshot ID has no timestamp.");
        }
    }


    void detail::checkHasEntityName(const MemoryID& entityID)
    {
        if (not entityID.hasEntityName())
        {
            throw armem::error::InvalidMemoryID(entityID, "Entity ID has no entity name.");
        }
    }


    void detail::checkHasProviderSegmentName(const MemoryID& providerSegmentID)
    {
        if (not providerSegmentID.hasProviderSegmentName())
        {
            throw armem::error::InvalidMemoryID(providerSegmentID, "Provider segment ID has no provider segment name.");
        }
    }


    void detail::checkHasCoreSegmentName(const MemoryID& coreSegmentID)
    {
        if (not coreSegmentID.hasCoreSegmentName())
        {
            throw armem::error::InvalidMemoryID(coreSegmentID, "Core segment ID has no core segment name.");
        }
    }


    void detail::checkHasMemoryName(const MemoryID& memoryID)
    {
        if (not memoryID.hasMemoryName())
        {
            throw armem::error::InvalidMemoryID(memoryID, "Memory ID has no memory name.");
        }
    }


}
