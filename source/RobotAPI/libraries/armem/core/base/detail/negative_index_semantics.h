#pragma once

#include <stddef.h>


namespace armarx::armem::base::detail
{

    size_t negativeIndexSemantics(long index, size_t size);

}
