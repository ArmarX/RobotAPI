#pragma once

#include "derived.h"

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/error/ArMemError.h>


namespace armarx::armem::base::detail
{

    void checkHasInstanceIndex(const MemoryID& instanceID);
    void checkHasTimestamp(const MemoryID& snapshotID);
    void checkHasEntityName(const MemoryID& entityID);
    void checkHasProviderSegmentName(const MemoryID& providerSegmentID);
    void checkHasCoreSegmentName(const MemoryID& coreSegmentID);
    void checkHasMemoryName(const MemoryID& memory);


    template <class KeyT, class ContainerT>
    auto* findChildByKey(const KeyT& key, ContainerT&& container)
    {
        auto it = container.find(key);
        return it != container.end() ? &it->second : nullptr;
    }

    template <class KeyT, class ContainerT, class ParentT, class KeyStringFn>
    auto&
    getChildByKey(
        const KeyT& key,
        ContainerT&& container,
        const ParentT& parent,
        KeyStringFn&& keyStringFn)
    {
        if (auto* child = findChildByKey(key, container))
        {
            return *child;
        }
        else
        {
            throw armem::error::MissingEntry::create<typename ParentT::ChildT>(keyStringFn(key), parent);
        }
    }
    template <class KeyT, class ContainerT, class ParentT>
    auto&
    getChildByKey(
        const KeyT& key,
        ContainerT&& container,
        const ParentT& parent)
    {
        return getChildByKey(key, container, parent, [](const KeyT & key)
        {
            return key;
        });
    }


    template <class DerivedT>
    struct GetFindInstanceMixin
    {
        // Relies on this->find/getSnapshot()

        bool hasInstance(const MemoryID& instanceID) const
        {
            return derived<DerivedT>(this).findInstance(instanceID) != nullptr;
        }

        /**
         * @brief Find an entity instance.
         * @param id The instance ID.
         * @return The instance or nullptr if it is missing.
         */
        auto*
        findInstance(const MemoryID& instanceID)
        {
            auto* snapshot = derived<DerivedT>(this).findSnapshot(instanceID);
            return snapshot ? snapshot->findInstance(instanceID) : nullptr;
        }
        const auto*
        findInstance(const MemoryID& instanceID) const
        {
            const auto* snapshot = derived<DerivedT>(this).findSnapshot(instanceID);
            return snapshot ? snapshot->findInstance(instanceID) : nullptr;
        }

        /**
         * @brief Retrieve an entity instance.
         * @param id The instance ID.
         * @return The instance if it is found.
         * @throw `armem::error::ArMemError` if it is missing.
         */
        auto&
        getInstance(const MemoryID& instanceID)
        {
            return derived<DerivedT>(this).getSnapshot(instanceID).getInstance(instanceID);
        }
        const auto&
        getInstance(const MemoryID& instanceID) const
        {
            return derived<DerivedT>(this).getSnapshot(instanceID).getInstance(instanceID);
        }
    };


    template <class DerivedT>
    struct GetFindSnapshotMixin
    {
        // Relies on this->find/getEntity()

        bool hasSnapshot(const MemoryID& snapshotID) const
        {
            return derived<DerivedT>(this).findSnapshot(snapshotID) != nullptr;
        }

        /**
         * @brief Find an entity snapshot.
         * @param id The snapshot ID.
         * @return The snapshot or nullptr if it is missing.
         */
        auto*
        findSnapshot(const MemoryID& snapshotID)
        {
            auto* entity = derived<DerivedT>(this).findEntity(snapshotID);
            return entity ? entity->findSnapshot(snapshotID) : nullptr;
        }
        const auto*
        findSnapshot(const MemoryID& snapshotID) const
        {
            auto* entity = derived<DerivedT>(this).findEntity(snapshotID);
            return entity ? entity->findSnapshot(snapshotID) : nullptr;
        }

        /**
         * @brief Retrieve an entity snapshot.
         * @param id The snapshot ID.
         * @return The snapshot if it is found.
         * @throw `armem::error::ArMemError` if it is missing.
         */
        auto&
        getSnapshot(const MemoryID& snapshotID)
        {
            return derived<DerivedT>(this).getEntity(snapshotID).getSnapshot(snapshotID);
        }
        const auto&
        getSnapshot(const MemoryID& snapshotID) const
        {
            return derived<DerivedT>(this).getEntity(snapshotID).getSnapshot(snapshotID);
        }


        // More elaborate cases

        auto* findLatestSnapshot(const MemoryID& entityID)
        {
            auto* entity = derived<DerivedT>(this).findEntity(entityID);
            return entity ? entity->findLatestSnapshot() : nullptr;
        }
        const auto* findLatestSnapshot(const MemoryID& entityID) const
        {
            auto* entity = derived<DerivedT>(this).findEntity(entityID);
            return entity ? entity->findLatestSnapshot() : nullptr;
        }

        auto* findLatestInstance(const MemoryID& entityID, int instanceIndex = 0)
        {
            auto* snapshot = derived<DerivedT>(this).findLatestSnapshot(entityID);
            return snapshot ? snapshot->findInstance(instanceIndex) : nullptr;
        }
        const auto* findLatestInstance(const MemoryID& entityID, int instanceIndex = 0) const
        {
            auto* snapshot = derived<DerivedT>(this).findLatestSnapshot(entityID);
            return snapshot ? snapshot->findInstance(instanceIndex) : nullptr;
        }

    };



    template <class DerivedT>
    struct GetFindEntityMixin
    {
        // Relies on this->find/getProviderSegment()

        bool hasEntity(const MemoryID& entityID) const
        {
            return derived<DerivedT>(this).findEntity(entityID) != nullptr;
        }

        /**
         * @brief Find an entity.
         * @param id The entity ID.
         * @return The entity or nullptr if it is missing.
         */
        auto*
        findEntity(const MemoryID& entityID)
        {
            auto* provSeg = derived<DerivedT>(this).findProviderSegment(entityID);
            return provSeg ? provSeg->findEntity(entityID) : nullptr;
        }
        const auto*
        findEntity(const MemoryID& entityID) const
        {
            auto* provSeg = derived<DerivedT>(this).findProviderSegment(entityID);
            return provSeg ? provSeg->findEntity(entityID) : nullptr;
        }

        /**
         * @brief Retrieve an entity.
         * @param id The entity ID.
         * @return The entity if it is found.
         * @throw `armem::error::ArMemError` if it is missing.
         */
        auto&
        getEntity(const MemoryID& entityID)
        {
            return derived<DerivedT>(this).getProviderSegment(entityID).getEntity(entityID);
        }
        const auto&
        getEntity(const MemoryID& entityID) const
        {
            return derived<DerivedT>(this).getProviderSegment(entityID).getEntity(entityID);
        }
    };



    template <class DerivedT>
    struct GetFindProviderSegmentMixin
    {
        // Relies on this->find/getCoreSegment()

        bool hasProviderSegment(const MemoryID& providerSegmentID) const
        {
            return derived<DerivedT>(this).findProviderSegment(providerSegmentID) != nullptr;
        }

        /**
         * @brief Retrieve a provider segment.
         * @param id The provider segment ID.
         * @return The provider segment if it is found or nullptr if it missing.
         */
        auto*
        findProviderSegment(const MemoryID& providerSegmentID)
        {
            auto* provSeg = derived<DerivedT>(this).findCoreSegment(providerSegmentID);
            return provSeg ? provSeg->findProviderSegment(providerSegmentID) : nullptr;
        }
        const auto*
        findProviderSegment(const MemoryID& providerSegmentID) const
        {
            auto* provSeg = derived<DerivedT>(this).findCoreSegment(providerSegmentID);
            return provSeg ? provSeg->findProviderSegment(providerSegmentID) : nullptr;
        }

        /**
         * @brief Retrieve a provider segment.
         * @param id The provider segment ID.
         * @return The provider segment if it is found.
         * @throw `armem::error::ArMemError` if it is missing.
         */
        auto&
        getProviderSegment(const MemoryID& providerSegmentID)
        {
            return derived<DerivedT>(this).getCoreSegment(providerSegmentID).getProviderSegment(providerSegmentID);
        }
        const auto&
        getProviderSegment(const MemoryID& providerSegmentID) const
        {
            return derived<DerivedT>(this).getCoreSegment(providerSegmentID).getProviderSegment(providerSegmentID);
        }

    };

}
