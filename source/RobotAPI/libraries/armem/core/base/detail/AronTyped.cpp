#include "AronTyped.h"

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>


namespace armarx::armem::base::detail
{

    AronTyped::AronTyped(aron::type::ObjectPtr aronType) :
        _aronType(aronType)
    {}

    bool AronTyped::hasAronType() const
    {
        return _aronType != nullptr;
    }

    aron::type::ObjectPtr& AronTyped::aronType()
    {
        return _aronType;
    }

    aron::type::ObjectPtr AronTyped::aronType() const
    {
        return _aronType;
    }


}
