/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::armem::core::base::detail
 * @author     phesch ( phesch at student dot kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/Prediction.h>

#include "derived.h"


namespace armarx::armem::base::detail
{

    /**
     * @brief Something that supports a set of prediction engines.
     */
    template <class DerivedT>
    class Predictive
    {
    public:

        explicit Predictive(const std::vector<PredictionEngine>& engines = {}) :
            _predictionEngines(engines)
        {
        }

        const std::vector<PredictionEngine>&
        predictionEngines() const
        {
            return _predictionEngines;
        }

        void addPredictionEngine(const PredictionEngine& engine)
        {
            _predictionEngines.push_back(engine);
        }

        void setPredictionEngines(const std::vector<PredictionEngine>& engines)
        {
            this->_predictionEngines = engines;
        }

        std::map<MemoryID, std::vector<PredictionEngine>>
        getAllPredictionEngines() const
        {
            std::map<MemoryID, std::vector<PredictionEngine>> engines;

            // Add own engines.
            const auto& derivedThis = derived<DerivedT>(this);
            const auto& ownEngines = derivedThis.predictionEngines();
            if (not ownEngines.empty())
            {
                engines.emplace(derivedThis.id(), derivedThis.predictionEngines());
            }

            return engines;
        }


    private:

        std::vector<PredictionEngine> _predictionEngines;

    };


    /**
     * @brief Something that supports a set of prediction engines.
     */
    template <class DerivedT>
    class PredictiveContainer : public Predictive<DerivedT>
    {
    public:

        using Predictive<DerivedT>::Predictive;


        std::map<MemoryID, std::vector<PredictionEngine>>
        getAllPredictionEngines() const
        {
            std::map<MemoryID, std::vector<PredictionEngine>> engines;

            // Collect engines of children.
            const auto& derivedThis = derived<DerivedT>(this);
            derivedThis.forEachChild(
                [&engines](const auto& child)
                {
                    const auto& childMap = child.getAllPredictionEngines();
                    engines.insert(childMap.begin(), childMap.end());
                });

            // Add own engines.
            const auto& ownEngines = derivedThis.predictionEngines();
            if (not ownEngines.empty())
            {
                engines.emplace(derivedThis.id(), derivedThis.predictionEngines());
            }

            return engines;
        }

    };


} // namespace armarx::armem::base::detail
