#pragma once

#include <functional>
#include <type_traits>

#include <RobotAPI/libraries/armem/core/MemoryID.h>


namespace
{
    template<typename F, typename Ret, typename A, typename... Rest>
    A helper(Ret (F::*)(A, Rest...));

    template<typename F, typename Ret, typename A, typename... Rest>
    A helper(Ret (F::*)(A, Rest...) const);

    // volatile or lvalue/rvalue *this not required for lambdas (phew)

    template <typename FuncT>
    struct first_argument
    {
        using type = decltype( helper(&FuncT::operator()) );
    };

    template <typename FuncT>
    using first_argument_t = typename first_argument<FuncT>::type;
}

namespace armarx::armem::base::detail
{

    // Helper functions to implement the forEach*() method at the current level.

    // Handle functions with different return type.
    template <class FunctionT, class ChildT>
    bool call(FunctionT&& func, ChildT&& child)
    {
        if constexpr(std::is_same_v<decltype(func(child)), bool>)
        {
            if (!func(child))
            {
                return false;
            }
            return true;
        }
        else
        {
            func(child);
            return true;
        }
    }


    // Single-valued containers.
    template <class ContainerT, class FunctionT>
    bool forEachChildSingle(ContainerT& container, FunctionT&& func)
    {
        for (auto& child : container)
        {
            if (not call(func, child))
            {
                return false;
            }
        }
        return true;
    }


    // Pair-valued containers.
    template <class ContainerT, class FunctionT>
    bool forEachChildPair(ContainerT& container, FunctionT&& func)
    {
        for (auto& [_, child] : container)
        {
            if (not call(func, child))
            {
                return false;
            }
        }
        return true;
    }


    // see: https://en.cppreference.com/w/cpp/types/void_t

    // primary template handles types that have no nested ::type member:
    template< class, class = void >
    struct has_mapped_type : std::false_type { };

    // specialization recognizes types that do have a nested ::type member:
    template< class T >
    struct has_mapped_type<T, std::void_t<typename T::mapped_type>> : std::true_type { };


    template <class ContainerT, class FunctionT>
    bool forEachChild(ContainerT& container, FunctionT&& func)
    {
        if constexpr(has_mapped_type<ContainerT>::value)
        {
            return forEachChildPair(container, func);
        }
        else
        {
            return forEachChildSingle(container, func);
        }
    }


    template <class FunctionT, class ParentT, class ChildT>
    bool forEachInstanceIn(
            const MemoryID& id,
            FunctionT&& func,
            ParentT& parent,
            bool single,
            ChildT* child
            )
    {
        auto childFn = [&id,&func](auto& child)
        {
            return child.forEachInstanceIn(id, func);
        };
        if (single)
        {
            return child ? childFn(*child) : true;
        }
        else
        {
            return parent.forEachChild(childFn);
        }
    }



    // We use auto instead of, e.g. DerivedT::EntitySnapshotT,
    // as we cannot use the typedef before DerivedT was completely defined.

    template <class DerivedT>
    struct ForEachEntityInstanceMixin
    {
        // not: using EntitySnapshotT = typename DerivedT::EntitySnapshotT;

        /**
         * @param func Function like: bool process(EntityInstanceT& instance)>
         */
        template <class InstanceFunctionT>
        bool forEachInstance(InstanceFunctionT&& func)
        {
            return static_cast<DerivedT*>(this)->forEachSnapshot(
                       [&func](auto & snapshot) -> bool
            {
                return snapshot.forEachInstance(func);
            });
        }

        /**
         * @param func Function like: bool process(const EntityInstanceT& instance)
         */
        template <class InstanceFunctionT>
        bool forEachInstance(InstanceFunctionT&& func) const
        {
            return static_cast<const DerivedT*>(this)->forEachSnapshot(
                       [&func](const auto & snapshot) -> bool
            {
                return snapshot.forEachInstance(func);
            });
        }

        /**
         * Call `func` on the data of each instances converted to Aron DTO class.
         *
         * @code
         * ().forEachEntityInstanceAs([](const my::arondto::CoolData& data)
         * {
         *      ...
         * });
         * @endcode
         *
         * The Aron DTO type is deduced from the passed function's first argument.
         *
         * @param func Function like: `bool process(const my::arondto::CoolData& data)`
         */
        template <class AronDtoFunctionT>
        bool forEachInstanceAs(AronDtoFunctionT&& func) const
        {
            return static_cast<const DerivedT*>(this)->forEachInstance(
                       [&func](const auto & instance) -> bool
            {
                using AronDtoT = typename std::remove_reference_t<first_argument_t<AronDtoFunctionT>>;
                return func(instance.template dataAs<AronDtoT>());
            });
        }

    };


    template <class DerivedT>
    struct ForEachEntitySnapshotMixin
    {
        /**
         * @param func Function like: bool process(EntitySnapshotT& snapshot)>
         */
        template <class SnapshotFunctionT>
        bool forEachSnapshot(SnapshotFunctionT&& func)
        {
            return static_cast<DerivedT*>(this)->forEachEntity(
                       [&func](auto & entity) -> bool
            {
                return entity.forEachSnapshot(func);
            });
        }

        /**
         * @param func Function like: bool process(const EntitySnapshotT& snapshot)
         */
        template <class SnapshotFunctionT>
        bool forEachSnapshot(SnapshotFunctionT&& func) const
        {
            return static_cast<const DerivedT*>(this)->forEachEntity(
                       [&func](const auto & entity) -> bool
            {
                return entity.forEachSnapshot(func);
            });
        }
    };


    template <class DerivedT>
    struct ForEachEntityMixin
    {
        /**
         * @param func Function like: bool process(EntityT& entity)>
         */
        template <class FunctionT>
        bool forEachEntity(FunctionT&& func)
        {
            return static_cast<DerivedT*>(this)->forEachProviderSegment(
                       [&func](auto & providerSegment) -> bool
            {
                return providerSegment.forEachEntity(func);
            });
        }

        /**
         * @param func Function like: bool process(const EntityT& entity)
         */
        template <class FunctionT>
        bool forEachEntity(FunctionT&& func) const
        {
            return static_cast<const DerivedT*>(this)->forEachProviderSegment(
                       [&func](const auto & providerSegment) -> bool
            {
                return providerSegment.forEachEntity(func);
            });
        }
    };


    template <class DerivedT>
    struct ForEachProviderSegmentMixin
    {
        /**
         * @param func Function like: bool process(ProviderSegmentT& providerSegment)>
         */
        template <class FunctionT>
        bool forEachProviderSegment(FunctionT&& func)
        {
            return static_cast<DerivedT*>(this)->forEachCoreSegment(
                       [&func](auto & coreSegment) -> bool
            {
                return coreSegment.forEachProviderSegment(func);
            });
        }

        /**
         * @param func Function like: bool process(const ProviderSegmentT& providerSegment)
         */
        template <class FunctionT>
        bool forEachProviderSegment(FunctionT&& func) const
        {
            return static_cast<const DerivedT*>(this)->forEachCoreSegment(
                       [&func](const auto & coreSegment) -> bool
            {
                return coreSegment.forEachProviderSegment(func);
            });
        }
    };

}
