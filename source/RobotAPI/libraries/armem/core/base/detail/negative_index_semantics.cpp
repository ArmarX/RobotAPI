#include "negative_index_semantics.h"

#include <algorithm>


size_t armarx::armem::base::detail::negativeIndexSemantics(long index, size_t size)
{
    const size_t max = size > 0 ? size - 1 : 0;
    if (index >= 0)
    {
        return std::clamp<size_t>(static_cast<size_t>(index), 0, max);
    }
    else
    {
        return static_cast<size_t>(std::clamp<long>(static_cast<long>(size) + index, 0, static_cast<long>(max)));
    }
}
