#include "MemoryItem.h"


namespace armarx::armem::base::detail
{

    MemoryItem::MemoryItem()
    {
    }


    MemoryItem::MemoryItem(const MemoryID& id) :
        _id(id)
    {
    }


    MemoryItem::~MemoryItem()
    {
    }

}
