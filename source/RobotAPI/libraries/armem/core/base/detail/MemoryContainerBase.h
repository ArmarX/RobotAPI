#pragma once

#include <RobotAPI/libraries/armem/core/error/ArMemError.h>

#include "MemoryItem.h"
#include "iteration_mixins.h"


namespace armarx::armem::base::detail
{
    /**
     * @class Provides default implmentations of `MemoryContainer`, as well as
     * iterators (which requires a template).
     */
    template <class _ContainerT, class _DerivedT>
    class MemoryContainerBase :
        public MemoryItem
    {
        using Base = MemoryItem;

    public:

        using DerivedT = _DerivedT;
        using ContainerT = _ContainerT;


    public:

        MemoryContainerBase()
        {}
        explicit MemoryContainerBase(const MemoryID& id) :
            MemoryItem(id)
        {
        }

        MemoryContainerBase(const MemoryContainerBase& other) = default;
        MemoryContainerBase(MemoryContainerBase&& other) = default;
        MemoryContainerBase& operator=(const MemoryContainerBase& other) = default;
        MemoryContainerBase& operator=(MemoryContainerBase&& other) = default;


        // Container methods

        bool empty() const
        {
            return _container.empty();
        }
        std::size_t size() const
        {
            return _container.size();
        }
        void clear()
        {
            return _container.clear();
        }


        // ITERATION

        /**
         * @param func Function like: bool process(ChildT& provSeg)
         */
        template <class ChildFunctionT>
        bool forEachChild(ChildFunctionT&& func)
        {
            return base::detail::forEachChild(this->_container, func);
        }
        /**
         * @param func Function like: bool process(const ChildT& provSeg)
         */
        template <class ChildFunctionT>
        bool forEachChild(ChildFunctionT&& func) const
        {
            return base::detail::forEachChild(this->_container, func);
        }


        [[deprecated("Direct container access is deprecated. Use forEach*() instead.")]]
        typename ContainerT::const_iterator begin() const
        {
            return _container.begin();
        }
        [[deprecated("Direct container access is deprecated. Use forEach*() instead.")]]
        typename ContainerT::iterator begin()
        {
            return _container.begin();
        }
        [[deprecated("Direct container access is deprecated. Use forEach*() instead.")]]
        typename ContainerT::const_iterator end() const
        {
            return _container.end();
        }
        [[deprecated("Direct container access is deprecated. Use forEach*() instead.")]]
        typename ContainerT::iterator end()
        {
            return _container.end();
        }


    protected:

        const ContainerT& container() const
        {
            return _container;
        }
        ContainerT& container()
        {
            return _container;
        }

        DerivedT& _derived()
        {
            return static_cast<DerivedT&>(*this);
        }
        const DerivedT& _derived() const
        {
            return static_cast<DerivedT&>(*this);
        }


        /**
         * @throw `armem::error::ContainerNameMismatch` if `gottenName` does not match `actualName`.
         */
        void _checkContainerName(const std::string& gottenName, const std::string& actualName,
                                 bool emptyOk = true) const
        {
            if (!((emptyOk && gottenName.empty()) || gottenName == actualName))
            {
                throw armem::error::ContainerNameMismatch(
                    gottenName, DerivedT::getLevelName(), actualName);
            }
        }


        template <class ChildT, class KeyT, class ...ChildArgs>
        ChildT& _addChild(const KeyT& key, ChildArgs... childArgs)
        {
            auto [it, inserted] = this->_container.try_emplace(key, childArgs...);
            if (not inserted)
            {
                throw armem::error::ContainerEntryAlreadyExists(
                    ChildT::getLevelName(), key, DerivedT::getLevelName(), this->_derived().name());
            }
            return it->second;
        }


    protected:

        mutable ContainerT _container;

    };

}
