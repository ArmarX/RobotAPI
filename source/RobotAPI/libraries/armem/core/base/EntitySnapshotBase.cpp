#include "EntitySnapshotBase.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


void armarx::armem::base::detail::throwIfNotEqual(const Time& ownTime, const Time& updateTime)
{
    ARMARX_CHECK_EQUAL(ownTime, updateTime)
            << "Cannot update a snapshot to an update with another timestamp. \n"
            << "Note: A snapshot's timestamp must not be changed after construction.";
}
