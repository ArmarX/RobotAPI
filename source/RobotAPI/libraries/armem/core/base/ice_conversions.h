#pragma once

#include "EntityInstanceBase.h"
#include "EntitySnapshotBase.h"
#include "EntityBase.h"
#include "ProviderSegmentBase.h"
#include "CoreSegmentBase.h"
#include "MemoryBase.h"

#include <RobotAPI/libraries/armem/core/ice_conversions.h>
#include <RobotAPI/libraries/aron/core/type/variant/forward_declarations.h>

#include <RobotAPI/interface/armem/memory.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>
#include <ArmarXCore/core/time/ice_conversions.h>


namespace armarx::armem::base::detail
{
    void toIceItem(data::detail::MemoryItem& ice, const MemoryItem& item);
    void fromIceItem(const data::detail::MemoryItem& ice, MemoryItem& item);

    void toIce(aron::data::dto::DictPtr& ice, const aron::data::DictPtr& bo);
    void fromIce(const aron::data::dto::DictPtr& ice, aron::data::DictPtr& bo);

    void toIce(aron::type::dto::GenericTypePtr& ice, const aron::type::ObjectPtr& bo);
    void fromIce(const aron::type::dto::GenericTypePtr& ice, aron::type::ObjectPtr& bo);
}
namespace armarx::armem::base
{
    void toIce(data::EntityInstanceMetadata& ice, const EntityInstanceMetadata& metadata);
    void fromIce(const data::EntityInstanceMetadata& ice, EntityInstanceMetadata& metadata);

    void toIce(data::EntityInstanceMetadataPtr& ice, const EntityInstanceMetadata& metadata);
    void fromIce(const data::EntityInstanceMetadataPtr& ice, EntityInstanceMetadata& metadata);


    template <class ...Args>
    void toIce(data::EntityInstance& ice, const EntityInstanceBase<Args...>& data)
    {
        detail::toIceItem(ice, data);

        // detail::toIce(std::ref(ice.data).get(), data.data());
        detail::toIce(ice.data, data.data());
        toIce(ice.metadata, data.metadata());
    }
    template <class ...Args>
    void fromIce(const data::EntityInstance& ice, EntityInstanceBase<Args...>& data)
    {
        detail::fromIceItem(ice, data);

        detail::fromIce(ice.data, data.data());
        fromIce(ice.metadata, data.metadata());
    }

    template <class ...Args>
    void toIce(data::EntitySnapshot& ice, const EntitySnapshotBase<Args...>& snapshot)
    {
        detail::toIceItem(ice, snapshot);

        ice.instances.clear();
        snapshot.forEachInstance([&ice](const auto & instance)
        {
            armarx::toIce(ice.instances.emplace_back(), instance);
        });
    }
    template <class ...Args>
    void fromIce(const data::EntitySnapshot& ice, EntitySnapshotBase<Args...>& snapshot)
    {
        detail::fromIceItem(ice, snapshot);

        snapshot.clear();
        for (const data::EntityInstancePtr& iceInstance : ice.instances)
        {
            snapshot.addInstance(armarx::fromIce<typename EntitySnapshotBase<Args...>::EntityInstanceT>(iceInstance));
        }
    }

    template <class ...Args>
    void toIce(data::Entity& ice, const EntityBase<Args...>& entity)
    {
        detail::toIceItem(ice, entity);

        ice.history.clear();
        entity.forEachSnapshot([&ice](const auto & snapshot)
        {
            armarx::toIce(ice.history[armarx::toIce<dto::Time>(snapshot.time())], snapshot);
        });
    }
    template <class ...Args>
    void fromIce(const data::Entity& ice, EntityBase<Args...>& entity)
    {
        detail::fromIceItem(ice, entity);

        entity.clear();
        for (const auto& [key, snapshot] : ice.history)
        {
            entity.addSnapshot(armarx::fromIce<typename EntityBase<Args...>::EntitySnapshotT>(snapshot));
        }
    }


    template <class ...Args>
    void toIce(data::ProviderSegment& ice, const ProviderSegmentBase<Args...>& providerSegment)
    {
        detail::toIceItem(ice, providerSegment);

        detail::toIce(ice.aronType, providerSegment.aronType());
        ARMARX_CHECK(!providerSegment.aronType() || ice.aronType);

        ice.entities.clear();
        providerSegment.forEachEntity([&ice](const auto & entity)
        {
            armarx::toIce(ice.entities[entity.name()], entity);
        });
    }
    template <class ...Args>
    void fromIce(const data::ProviderSegment& ice, ProviderSegmentBase<Args...>& providerSegment)
    {
        detail::fromIceItem(ice, providerSegment);

        detail::fromIce(ice.aronType, providerSegment.aronType());
        ARMARX_CHECK(!providerSegment.aronType() || ice.aronType);

        providerSegment.clear();
        for (const auto& [key, entity] : ice.entities)
        {
            providerSegment.addEntity(
                armarx::fromIce<typename ProviderSegmentBase<Args...>::EntityT>(entity));
        }
    }

    template <class ...Args>
    void toIce(data::CoreSegment& ice, const CoreSegmentBase<Args...>& coreSegment)
    {
        detail::toIceItem(ice, coreSegment);

        detail::toIce(ice.aronType, coreSegment.aronType());
        ARMARX_CHECK(!coreSegment.aronType() || ice.aronType);

        ice.providerSegments.clear();
        coreSegment.forEachProviderSegment([&ice](const auto & providerSegment)
        {
            armarx::toIce(ice.providerSegments[providerSegment.name()], providerSegment);
        });
    }
    template <class ...Args>
    void fromIce(const data::CoreSegment& ice, CoreSegmentBase<Args...>& coreSegment)
    {
        detail::fromIceItem(ice, coreSegment);

        detail::fromIce(ice.aronType, coreSegment.aronType());
        ARMARX_CHECK(!coreSegment.aronType() || ice.aronType);

        // fromIce(ice.providerSegments, coreSegment.providerSegments());
        coreSegment.clear();
        for (const auto& [key, providerSegment] : ice.providerSegments)
        {
            coreSegment.addProviderSegment(
                armarx::fromIce<typename CoreSegmentBase<Args...>::ProviderSegmentT>(providerSegment));
        }
    }

    template <class ...Args>
    void toIce(data::Memory& ice, const MemoryBase<Args...>& memory)
    {
        base::detail::toIceItem(ice, memory);

        ice.coreSegments.clear();
        memory.forEachCoreSegment([&ice](const auto & coreSegment)
        {
            armarx::toIce(ice.coreSegments[coreSegment.name()], coreSegment);
        });
    }
    template <class ...Args>
    void fromIce(const data::Memory& ice, MemoryBase<Args...>& memory)
    {
        base::detail::fromIceItem(ice, memory);

        memory.clear();
        for (const auto& [key, coreSegment] : ice.coreSegments)
        {
            // We can avoid using CoreSegment's copy constructor this way:
            armarx::fromIce(coreSegment, memory.addCoreSegment(coreSegment->id.coreSegmentName));
        }
    }

}
