#include "EntityInstanceBase.h"


namespace armarx::armem::base
{
    bool EntityInstanceMetadata::operator==(const EntityInstanceMetadata& other) const
    {
        return timeCreated == other.timeCreated
               && timeSent == other.timeSent
               && timeArrived == other.timeArrived
               && std::abs(confidence - other.confidence) < 1e-6f;
    }
}


std::ostream& armarx::armem::base::operator<<(std::ostream& os, const EntityInstanceMetadata& d)
{
    os << "EntityInstanceMetadata: "
       << "\n- t_create =   \t" << armem::toStringMicroSeconds(d.timeCreated) << " us"
       << "\n- t_sent =     \t" << armem::toStringMicroSeconds(d.timeSent) << " us"
       << "\n- t_arrived =  \t" << armem::toStringMicroSeconds(d.timeArrived) << " us"
       << "\n- confidence = \t" << d.confidence << " us"
       ;
    return os;
}
