#pragma once

#include <map>
#include <string>

#include <RobotAPI/libraries/armem/core/Prediction.h>

#include "EntityBase.h"
#include "detail/AronTyped.h"
#include "detail/Predictive.h"
#include "detail/MemoryContainerBase.h"
#include "detail/iteration_mixins.h"
#include "detail/lookup_mixins.h"


namespace armarx::armem::base
{

    /**
     * @brief Data of a provider segment containing multiple entities.
     */
    template <class _EntityT, class _Derived>
    class ProviderSegmentBase :
        public detail::MemoryContainerBase<std::map<std::string, _EntityT>, _Derived>
        , public detail::AronTyped
        , public detail::Predictive<_Derived>
        , public detail::ForEachEntityInstanceMixin<_Derived>
        , public detail::ForEachEntitySnapshotMixin<_Derived>
        , public detail::GetFindInstanceMixin<_Derived>
        , public detail::GetFindSnapshotMixin<_Derived>
    {
        using Base = detail::MemoryContainerBase<std::map<std::string, _EntityT>, _Derived>;

    public:

        using typename Base::DerivedT;
        using typename Base::ContainerT;

        using EntityT = _EntityT;
        using EntitySnapshotT = typename EntityT::EntitySnapshotT;
        using EntityInstanceT = typename EntitySnapshotT::EntityInstanceT;

        using ChildT = EntityT;

        struct UpdateResult
        {
            armarx::armem::UpdateType providerSegmentUpdateType;
            armarx::armem::UpdateType entityUpdateType;
            MemoryID id;
            std::vector<EntitySnapshotT> removedSnapshots;

            UpdateResult() = default;
            UpdateResult(const typename EntityT::UpdateResult& c) :
                entityUpdateType(c.entityUpdateType),
                id(c.id),
                removedSnapshots(c.removedSnapshots)
            {}
        };


    public:

        ProviderSegmentBase()
        {
        }

        explicit ProviderSegmentBase(
            const std::string& name,
            aron::type::ObjectPtr aronType = nullptr,
            const std::vector<PredictionEngine>& predictionEngines = {}) :
            ProviderSegmentBase(name, MemoryID(), aronType, predictionEngines)
        {
        }
        explicit ProviderSegmentBase(
            const std::string& name,
            const MemoryID parentID,
            aron::type::ObjectPtr aronType = nullptr,
            const std::vector<PredictionEngine>& predictionEngines = {}) :
            ProviderSegmentBase(parentID.withProviderSegmentName(name), aronType, predictionEngines)
        {
        }
        explicit ProviderSegmentBase(
            const MemoryID id,
            aron::type::ObjectPtr aronType = nullptr,
            const std::vector<PredictionEngine>& predictionEngines = {}) :
            Base(id), AronTyped(aronType), detail::Predictive<_Derived>(predictionEngines)
        {
        }

        ProviderSegmentBase(const ProviderSegmentBase& other) = default;
        ProviderSegmentBase(ProviderSegmentBase&& other) = default;
        ProviderSegmentBase& operator=(const ProviderSegmentBase& other) = default;
        ProviderSegmentBase& operator=(ProviderSegmentBase&& other) = default;


        // READ ACCESS

        // Get key
        inline std::string& name()
        {
            return this->id().providerSegmentName;
        }
        inline const std::string& name() const
        {
            return this->id().providerSegmentName;
        }


        // Has child by key
        bool hasEntity(const std::string& name) const
        {
            return this->findEntity(name) != nullptr;
        }
        // Has child by ID
        bool hasEntity(const MemoryID& entityID) const
        {
            return this->findEntity(entityID) != nullptr;
        }


        // Find child by key
        EntityT* findEntity(const std::string& name)
        {
            return detail::findChildByKey(name, this->_container);
        }
        const EntityT* findEntity(const std::string& name) const
        {
            return detail::findChildByKey(name, this->_container);
        }

        // Get child by key
        EntityT& getEntity(const std::string& name)
        {
            return detail::getChildByKey(name, this->_container, *this);
        }
        const EntityT& getEntity(const std::string& name) const
        {
            return detail::getChildByKey(name, this->_container, *this);
        }

        // Find child by MemoryID
        EntityT* findEntity(const MemoryID& entityID)
        {
            detail::checkHasEntityName(entityID);
            return this->findEntity(entityID.entityName);
        }
        const EntityT* findEntity(const MemoryID& entityID) const
        {
            detail::checkHasEntityName(entityID);
            return this->findEntity(entityID.entityName);
        }

        // Get child by MemoryID
        EntityT& getEntity(const MemoryID& entityID)
        {
            detail::checkHasEntityName(entityID);
            return this->getEntity(entityID.entityName);
        }
        const EntityT& getEntity(const MemoryID& entityID) const
        {
            detail::checkHasEntityName(entityID);
            return this->getEntity(entityID.entityName);
        }

        // get/findInstance are provided by GetFindInstanceMixin
        // get/findSnapshot are provided by GetFindSnapshotMixin


        // ITERATION

        // All functors are taken as universal reference (F&&).

        /**
         * @param func Function like: bool process(EntityT& entity)
         */
        template <class EntityFunctionT>
        bool forEachEntity(EntityFunctionT&& func)
        {
            return this->forEachChild(func);
        }
        /**
         * @param func Function like: bool process(const EntityT& entity)
         */
        template <class EntityFunctionT>
        bool forEachEntity(EntityFunctionT&& func) const
        {
            return this->forEachChild(func);
        }

        // forEachSnapshot() is provided by ForEachEntitySnapshotMixin
        // forEachInstance() is provided by ForEachEntityInstanceMixin

        /**
         * @param func Function like void process(EntityInstanceT& instance)>
         */
        template <class InstanceFunctionT>
        bool forEachInstanceIn(const MemoryID& id, InstanceFunctionT&& func)
        {
            return detail::forEachInstanceIn(
                        id, func, *this,
                        id.hasEntityName(),
                        id.hasEntityName() ? this->findEntity(id.entityName) : nullptr);
        }
        /**
         * @param func Function like void process(EntityInstanceT& instance)>
         */
        template <class InstanceFunctionT>
        bool forEachInstanceIn(const MemoryID& id, InstanceFunctionT&& func) const
        {
            return detail::forEachInstanceIn(
                        id, func, *this,
                        id.hasEntityName(),
                        id.hasEntityName() ? this->findEntity(id.entityName) : nullptr);
        }


        // Get child keys
        std::vector<std::string> getEntityNames() const
        {
            return simox::alg::get_keys(this->_container);
        }


        // MODIFICATION

        /**
         * @brief Updates an entity's history.
         *
         * Missing entity entries are added before updating.
         */
        UpdateResult update(const EntityUpdate& update)
        {
            this->_checkContainerName(update.entityID.providerSegmentName, this->name());

            EntityT* entity;
            UpdateType updateType = UpdateType::InsertedNew;

            auto it = this->_container.find(update.entityID.entityName);
            if (it == this->_container.end())
            {
                // Add entity entry.
                entity = &addEntity(update.entityID.entityName);
                updateType = UpdateType::InsertedNew;
            }
            else
            {
                entity = &it->second;
                updateType = UpdateType::UpdatedExisting;
            }
            // Update entity.
            UpdateResult ret(entity->update(update));
            ret.providerSegmentUpdateType = updateType;
            return ret;
        }


        template <class OtherDerivedT>
        void append(const OtherDerivedT& other)
        {
            other.forEachEntity([this](const auto& entity)
            {
                auto it = this->_container.find(entity.name());
                if (it == this->_container.end())
                {
                    it = this->_container.emplace(entity.name(), this->id().withEntityName(entity.name())).first;
                }
                it->second.append(entity);
                return true;
            });
        }


        /// Add an empty entity with the given name.
        EntityT& addEntity(const std::string& name)
        {
            return this->_derived().addEntity(name, name);
        }

        /// Copy and insert an entity.
        EntityT& addEntity(const EntityT& entity)
        {
            return this->_derived().addEntity(entity.name(), EntityT(entity));
        }

        /// Move and insert an entity.
        EntityT& addEntity(EntityT&& entity)
        {
            const std::string name = entity.name();  // Copy before move.
            return this->_derived().addEntity(name, std::move(entity));
        }

        /// Insert an entity in-place.
        template <class ...Args>
        EntityT& addEntity(const std::string& name, Args... args)
        {
            ChildT& child = this->template _addChild<ChildT>(name, args...);
            child.id() = this->id().withEntityName(name);
            return child;
        }


        // MISC

        bool equalsDeep(const DerivedT& other) const
        {
            if (this->size() != other.size())
            {
                return false;
            }
            for (const auto& [key, value] : this->_container)
            {
                if (not other.hasEntity(key))
                {
                    return false;
                }

                if (not value.equalsDeep(other.getEntity(key)))
                {
                    return false;
                }
            }
            return true;
        }


        static std::string getLevelName()
        {
            return "provider segment";
        }

        std::string getKeyString() const
        {
            return this->name();
        }

    };

}
