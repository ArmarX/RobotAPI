#pragma once

#include <RobotAPI/libraries/armem/core/Commit.h>
#include <RobotAPI/libraries/armem/core/Time.h>

#include "detail/MemoryItem.h"


namespace armarx::armem::base
{

    /**
     * @brief Default data of an entity instance (empty).
     */
    struct NoData
    {
    };


    /**
     * @brief Metadata of an entity instance.
     */
    struct EntityInstanceMetadata
    {
        /// Time when this value was created.
        Time timeCreated;
        /// Time when this value was sent to the memory.
        Time timeSent;
        /// Time when this value has arrived at the memory.
        Time timeArrived;

        /// An optional confidence, may be used for things like decay.
        float confidence = 1.0;


        bool operator==(const EntityInstanceMetadata& other) const;
        inline bool operator!=(const EntityInstanceMetadata& other) const
        {
            return !(*this == other);
        }
    };

    std::ostream& operator<<(std::ostream& os, const EntityInstanceMetadata& rhs);



    /**
     * @brief Data of a single entity instance.
     */
    template <class _DataT = NoData, class _MetadataT = EntityInstanceMetadata>
    class EntityInstanceBase :
        public detail::MemoryItem
    {
        using Base = detail::MemoryItem;

    public:

        using MetadataT = _MetadataT;
        using DataT = _DataT;


        EntityInstanceBase()
        {
        }
        explicit EntityInstanceBase(int index, const MemoryID& parentID = {}) :
            EntityInstanceBase(parentID.withInstanceIndex(index))
        {
        }
        explicit EntityInstanceBase(const MemoryID& id) :
            Base(id)
        {
        }


        // Key
        inline int& index()
        {
            return id().instanceIndex;
        }
        inline int index() const
        {
            return id().instanceIndex;
        }


        // Data

        EntityInstanceMetadata& metadata()
        {
            return _metadata;
        }
        const EntityInstanceMetadata& metadata() const
        {
            return _metadata;
        }

        const DataT& data() const
        {
            return _data;
        }

        DataT& data()
        {
            return _data;
        }

        /**
         * @brief Get the data converted to a generated Aron DTO class.
         */
        template <class AronDtoT>
        AronDtoT dataAs() const
        {
            return AronDtoT::FromAron(_data);
        }


        // Misc

        static std::string getLevelName()
        {
            return "entity instance";
        }

        std::string getKeyString() const
        {
            return std::to_string(index());
        }


    protected:

        /// The metadata.
        MetadataT _metadata;

        /// The data. May be nullptr.
        DataT _data;

    };

}
