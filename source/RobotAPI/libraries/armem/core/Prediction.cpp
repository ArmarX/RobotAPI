/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::armem::client
 * @author     phesch ( ulila at student dot kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Prediction.h"

#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>

#include <RobotAPI/libraries/armem/core/ice_conversions.h>

namespace armarx::armem
{
    PredictionEngine
    PredictionEngine::fromIce(const armem::prediction::data::PredictionEngine& ice)
    {
        return armarx::fromIce<PredictionEngine>(ice);
    }

    armem::prediction::data::PredictionEngine
    PredictionEngine::toIce() const
    {
        return armarx::toIce<armem::prediction::data::PredictionEngine>(*this);
    }

    PredictionSettings
    PredictionSettings::fromIce(const armem::prediction::data::PredictionSettings& ice)
    {
        return armarx::fromIce<PredictionSettings>(ice);
    }

    armem::prediction::data::PredictionSettings
    PredictionSettings::toIce() const
    {
        return armarx::toIce<armem::prediction::data::PredictionSettings>(*this);
    }

    PredictionRequest
    PredictionRequest::fromIce(const armem::prediction::data::PredictionRequest& ice)
    {
        return armarx::fromIce<PredictionRequest>(ice);
    }

    armem::prediction::data::PredictionRequest
    PredictionRequest::toIce() const
    {
        return armarx::toIce<armem::prediction::data::PredictionRequest>(*this);
    }

    PredictionResult
    PredictionResult::fromIce(const armem::prediction::data::PredictionResult& ice)
    {
        return armarx::fromIce<PredictionResult>(ice);
    }

    armem::prediction::data::PredictionResult
    PredictionResult::toIce() const
    {
        return armarx::toIce<armem::prediction::data::PredictionResult>(*this);
    }

    void
    toIce(armem::prediction::data::PredictionEngine& ice, const PredictionEngine& engine)
    {
        ice.engineID = engine.engineID;
    }

    void
    fromIce(const armem::prediction::data::PredictionEngine& ice, PredictionEngine& engine)
    {
        engine.engineID = ice.engineID;
    }

    void
    toIce(armem::prediction::data::PredictionSettings& ice, const PredictionSettings& settings)
    {
        ice.predictionEngineID = settings.predictionEngineID;
    }

    void
    fromIce(const armem::prediction::data::PredictionSettings& ice, PredictionSettings& settings)
    {
        settings.predictionEngineID = ice.predictionEngineID;
    }

    void
    toIce(armem::prediction::data::PredictionRequest& ice, const PredictionRequest& request)
    {
        toIce(ice.snapshotID, request.snapshotID);
        toIce(ice.settings, request.predictionSettings);
    }

    void
    fromIce(const armem::prediction::data::PredictionRequest& ice, PredictionRequest& request)
    {
        fromIce(ice.snapshotID, request.snapshotID);
        fromIce(ice.settings, request.predictionSettings);
    }

    void
    toIce(armem::prediction::data::PredictionResult& ice, const PredictionResult& result)
    {
        ice.success = result.success;
        ice.errorMessage = result.errorMessage;
        toIce(ice.snapshotID, result.snapshotID);
        if (result.prediction)
        {
            ice.prediction = result.prediction->toAronDictDTO();
        }
        else
        {
            ice.prediction = nullptr;
        }
    }

    void
    fromIce(const armem::prediction::data::PredictionResult& ice, PredictionResult& result)
    {
        result.success = ice.success;
        result.errorMessage = ice.errorMessage;
        fromIce(ice.snapshotID, result.snapshotID);
        result.prediction = aron::data::Dict::FromAronDictDTO(ice.prediction);
    }

} // namespace armarx::armem
