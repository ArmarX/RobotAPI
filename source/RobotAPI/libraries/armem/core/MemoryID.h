#pragma once

#include <functional>  // for std::hash
#include <string>
#include <vector>

#include "Time.h"


namespace armarx::armem
{

    /**
     * @brief A memory ID.
     *
     * A memory ID is an index into the hierarchical memory structure.
     * It specifies the keys for the different levels, starting from the
     * memory name and ending at the instance index.
     *
     * A memory ID need not be complete, e.g. it may specify only the memory
     * and core segment names (thus representing a core segment ID).
     * A memory ID that fully identifies a level starting from the memory is
     * called well-defined.
     * @see `isWellDefined()`
     *
     * Memory IDs can be encoded in strings using a delimiter:
     * - Structure: "MemoryName/CoreSegmentName/ProviderSegmentName/EntityName/Timestamp/InstanceIndex"
     * - Example:   "Vision/RGBImages/Primesense/image/1245321323/0"
     * @see `str()`
     *
     * If an ID does not specify the lower levels, these parts can be omitted.
     * Thus, an entity ID could look like:
     * - Structure: "MemoryName/CoreSegmentName/ProviderSegmentName/EntityName"
     * - Example:   "Vision/RGBImages/Primesense/image"
     *
     * If a name contains a "/", it will be escaped:
     * - Example:   "Vision/RGBImages/Primesense/my\/entity\/with\/slashes"
     *
     * Memory IDs may be not well-defined. This can occur e.g. when preparing
     * an entity instance ID which is still pending the timestamp.
     * It could look like (note the missing timestamp):
     * - Example:   "Vision/RGBImages/Primesense/image//0"
     *
     * These IDs are still valid and can be handled (encoded as string etc.).
     * However, some operations may not be well-defined for non-well-defined IDs
     * (such as `contains()`).
     */
    class MemoryID
    {
    public:

        std::string memoryName = "";
        std::string coreSegmentName = "";
        std::string providerSegmentName = "";
        std::string entityName = "";
        Time timestamp = Time::Invalid();
        int instanceIndex = -1;


    public:

        /// Construct a default (empty) memory ID.
        MemoryID();
        /// (Re-)Construct a memory ID from a string representation as returned by `str()`.
        explicit MemoryID(const std::string& string);

        MemoryID(const std::string& memoryName,
                 const std::string& coreSegmentName,
                 const std::string& providerSegmentName = "",
                 const std::string& entityName = "",
                 Time timestamp = Time::Invalid(),
                 int instanceIndex = -1);


        /**
         * @brief Indicate whether this ID is well-defined.
         *
         * A well-defined ID has no specified level after a non-specified level (i.e., no gaps).
         *
         * Well-defined examples:
         * - "" (empty, but well-defined)
         * - "Memory" (a memory ID)
         * - "Memory/Core" (a core segment ID)
         * - "Memory/Core/Provider" (a provider segment ID)
         *
         * Non-well-defined examples:
         * - "Memory//Provider" (no core segment name)
         * - "/Core" (no memory name)
         * - "Mem/Core/Prov/entity//0" (no timestamp)
         * - "///entity//0" (no memory, core segment and provider segment names)
         *
         * @return True if `*this` is a well-defined memory ID.
         */
        bool isWellDefined() const;


        // Checks whether a specific level is specified.

        bool hasMemoryName() const
        {
            return not memoryName.empty();
        }
        bool hasCoreSegmentName() const
        {
            return not coreSegmentName.empty();
        }
        bool hasProviderSegmentName() const
        {
            return not providerSegmentName.empty();
        }
        bool hasEntityName() const
        {
            return not entityName.empty();
        }
        bool hasTimestamp() const
        {
            return timestamp.isValid();
        }
        void clearTimestamp()
        {
            timestamp = Time::Invalid();
        }
        bool hasInstanceIndex() const
        {
            return instanceIndex >= 0;
        }
        void clearInstanceIndex()
        {
            instanceIndex = -1;
        }


        // Slice getters: Get upper part of the ID.

        MemoryID getMemoryID() const;
        MemoryID getCoreSegmentID() const;
        MemoryID getProviderSegmentID() const;
        MemoryID getEntityID() const;
        MemoryID getEntitySnapshotID() const;
        MemoryID getEntityInstanceID() const;

        // Slice getter: remove the last set name
        MemoryID removeLeafItem() const;


        // Slice setters: Set part of the ID.

        void setMemoryID(const MemoryID& id);
        void setCoreSegmentID(const MemoryID& id);
        void setProviderSegmentID(const MemoryID& id);
        void setEntityID(const MemoryID& id);
        void setEntitySnapshotID(const MemoryID& id);
        void setEntityInstanceID(const MemoryID& id);

        // Return a modified copy.

        MemoryID withMemoryName(const std::string& name) const;
        MemoryID withCoreSegmentName(const std::string& name) const;
        MemoryID withProviderSegmentName(const std::string& name) const;
        MemoryID withEntityName(const std::string& name) const;
        MemoryID withTimestamp(Time time) const;
        MemoryID withInstanceIndex(int index) const;


        // String conversion

        /**
         * @brief Get a string representation of this memory ID.
         *
         * Items are separated by a delimiter. If `escapeDelimiter` is true,
         * delimiters occuring inside names are escaped with backward slashes.
         * This allows to reconstruct the memory ID from the result of `str()`
         * in these cases.
         *
         * @param escapeDelimiter If true, escape delimiters inside names
         * @return A string representation of this MemoryID.
         */
        std::string str(bool escapeDelimiters = true) const;

        /// Get the timestamp as string.
        std::string timestampStr() const;
        /// Get the instance index as string.
        std::string instanceIndexStr() const;

        /// Alias for constructor from string.
        static MemoryID fromString(const std::string& string);
        /// Reconstruct a timestamp from a string as returned by `timestampStr()`.
        static Time timestampFromStr(const std::string& timestamp);
        /// Reconstruct an instance index from a string as returned by `instanceIndexStr()`.
        static int instanceIndexFromStr(const std::string& index);


        /// Get all levels as strings.
        std::vector<std::string> getAllItems(bool escapeDelimiters = false) const;
        /// Get the levels from root to first not defined level (excluding).
        std::vector<std::string> getItems(bool escapeDelimiters = false) const;


        // Other utility.

        /// Indicate whether this ID has a gap such as in 'Memory//MyProvider' (no core segment name).
        bool hasGap() const;
        /// Get the lowest defined level (or empty string if there is none).
        std::string getLeafItem() const;


        // Operators

        bool operator ==(const MemoryID& other) const;
        inline bool operator !=(const MemoryID& other) const
        {
            return not (*this == other);
        }

        bool operator< (const MemoryID& rhs) const;
        inline bool operator> (const MemoryID& rhs) const
        {
            return rhs < (*this);
        }
        inline bool operator<=(const MemoryID& rhs) const
        {
            return not operator> (rhs);
        }
        inline bool operator>=(const MemoryID& rhs) const
        {
            return not operator< (rhs);
        }

        friend std::ostream& operator<<(std::ostream& os, const MemoryID id);


    private:

        static long long parseInteger(const std::string& string, const std::string& semanticName);
        static std::string escapeDelimiter(const std::string& name);
        static std::string escape(const std::string& name, bool escapeDelimiters);

        static const std::string delimiter;


        // Do not allow specifying the delimiter from outside.
        std::string str(const std::string& delimiter, bool escapeDelimiter) const;

    };


    /**
     * @brief Indicates whether `general` is "less specific" than, or equal to, `specific`,
     * i.e. `general` "contains" `specific`.
     *
     * A memory ID A is said to be less specific than B, if B the same values as A
     * for all levels specified in A, but potentially also specifies the lower levels.
     *
     * Examples:
     * - "" contains ""
     * - "m" contains "m" and "m/c", but not "n" and "n/c"
     * - "m/c" contains "m/c" and "m/c/p", but not "m/d" and "m/c/q"
     *
     * If a memory ID has a gap (`@see MemoryID::hasGap()`), such as "m//p",
     * the levels after the gap are ignored.
     * - "m//p" contains "m", "m/c" and "m/c/p".
     *
     * @param general The less specific memory ID
     * @param specific The more specific memory ID
     * @return True if `general` is less specific than `specific`.
     */
    bool contains(const MemoryID& general, const MemoryID& specific);

}  // namespace armarx::armem


namespace std
{

    template <>
    struct hash<armarx::armem::MemoryID>
    {
        std::size_t operator()(const armarx::armem::MemoryID& id) const
        {
            const std::string sid = id.str();
            return std::hash<string>()(sid);
        }
    };

}

