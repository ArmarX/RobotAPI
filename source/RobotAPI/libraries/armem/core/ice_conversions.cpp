#include "ice_conversions.h"

#include <ArmarXCore/core/time/ice_conversions.h>
#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>

#include <RobotAPI/interface/armem/actions.h>
#include <RobotAPI/interface/armem/commit.h>
#include <RobotAPI/interface/armem/memory.h>

#include "actions.h"
#include "Commit.h"
#include "MemoryID.h"
#include "Time.h"


namespace armarx
{

    void armem::toIce(data::MemoryID& ice, const MemoryID& id)
    {
        ice.memoryName = id.memoryName;
        ice.coreSegmentName = id.coreSegmentName;
        ice.providerSegmentName = id.providerSegmentName;
        ice.entityName = id.entityName;
        toIce(ice.timestamp, id.timestamp);
        ice.instanceIndex = id.instanceIndex;
    }

    void armem::fromIce(const data::MemoryID& ice, MemoryID& id)
    {
        id.memoryName = ice.memoryName;
        id.coreSegmentName = ice.coreSegmentName;
        id.providerSegmentName = ice.providerSegmentName;
        id.entityName = ice.entityName;
        fromIce(ice.timestamp, id.timestamp);
        id.instanceIndex = ice.instanceIndex;
    }

    void armem::fromIce(const data::Commit& ice, Commit& commit)
    {
        commit.updates.clear();
        for (const auto& ice_update : ice.updates)
        {
            EntityUpdate& update = commit.updates.emplace_back();
            fromIce(ice_update, update);
        }
    }

    void armem::toIce(data::Commit& ice, const Commit& commit)
    {
        ice.updates.clear();
        for (const auto& update : commit.updates)
        {
            data::EntityUpdate& ice_update = ice.updates.emplace_back();
            toIce(ice_update, update);
        }
    }

    void armem::fromIce(const data::CommitResult& ice, CommitResult& result)
    {
        result.results.clear();
        for (const auto& ice_res : ice.results)
        {
            EntityUpdateResult& res = result.results.emplace_back();
            fromIce(ice_res, res);
        }
    }

    void armem::toIce(data::CommitResult& ice, const CommitResult& result)
    {
        ice.results.clear();
        for (const auto& res : result.results)
        {
            data::EntityUpdateResult& ice_res = ice.results.emplace_back();
            toIce(ice_res, res);
        }
    }

    void armem::fromIce(const data::EntityUpdate& ice, EntityUpdate& update)
    {
        fromIce(ice.entityID, update.entityID);

        update.instancesData.clear();
        update.instancesData.reserve(ice.instancesData.size());
        std::transform(ice.instancesData.begin(), ice.instancesData.end(), std::back_inserter(update.instancesData),
                       aron::data::Dict::FromAronDictDTO);

        fromIce(ice.timeCreated, update.timeCreated);

        update.confidence = ice.confidence;
        fromIce(ice.timeSent, update.timeSent);
    }

    void armem::toIce(data::EntityUpdate& ice, const EntityUpdate& update)
    {
        toIce(ice.entityID, update.entityID);

        ice.instancesData.clear();
        ice.instancesData.reserve(update.instancesData.size());
        std::transform(update.instancesData.begin(), update.instancesData.end(), std::back_inserter(ice.instancesData),
                       aron::data::Dict::ToAronDictDTO);

        toIce(ice.timeCreated, update.timeCreated);

        ice.confidence = update.confidence;
        toIce(ice.timeSent, update.timeSent);
    }

    void armem::fromIce(const data::EntityUpdateResult& ice, EntityUpdateResult& result)
    {
        result.success = ice.success;
        fromIce(ice.snapshotID, result.snapshotID);
        fromIce(ice.timeArrived, result.timeArrived);
        result.errorMessage = ice.errorMessage;
    }

    void armem::toIce(data::EntityUpdateResult& ice, const EntityUpdateResult& result)
    {
        ice.success = result.success;
        toIce(ice.snapshotID, result.snapshotID);
        toIce(ice.timeArrived, result.timeArrived);
        ice.errorMessage = result.errorMessage;
    }

    void armem::fromIce(const data::Commit& ice, Commit& commit, Time timeArrived)
    {
        commit.updates.clear();
        for (const auto& ice_update : ice.updates)
        {
            EntityUpdate& update = commit.updates.emplace_back(EntityUpdate());
            fromIce(ice_update, update, timeArrived);
        }
    }

    void armem::fromIce(const data::EntityUpdate& ice, EntityUpdate& update, Time timeArrived)
    {
        fromIce(ice, update);
        update.timeArrived = timeArrived;
    }

    void armem::fromIce(const actions::data::MenuPtr& ice, actions::Menu& menu)
    {
        menu = actions::Menu::fromIce(ice);
    }

    void armem::toIce(actions::data::MenuPtr& ice, const actions::Menu& menu)
    {
        ice = menu.toIce();
    }
}

