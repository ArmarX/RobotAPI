#pragma once

#include <map>

#include <boost/container/flat_map.hpp>


namespace armarx::armem
{
    // std::map <-> boost::container::flat_map

    template <class IceKeyT, class IceValueT, class CppKeyT, class CppValueT>
    void toIce(std::map<IceKeyT, IceValueT>& iceMap,
               const boost::container::flat_map<CppKeyT, CppValueT>& cppMap)
    {
        iceMap.clear();
        for (const auto& [key, value] : cppMap)
        {
            iceMap.emplace(toIce<IceKeyT>(key), toIce<IceValueT>(value));
        }
    }

    template <class IceKeyT, class IceValueT, class CppKeyT, class CppValueT>
    void fromIce(const std::map<IceKeyT, IceValueT>& iceMap,
                 boost::container::flat_map<CppKeyT, CppValueT>& cppMap)
    {
        cppMap.clear();
        for (const auto& [key, value] : iceMap)
        {
            cppMap.emplace(fromIce<CppKeyT>(key), fromIce<CppValueT>(value));
        }
    }

}
