#include "QueryTarget.h"

namespace armarx::armem::query
{
    void toIce(armem::query::data::QueryTarget::QueryTargetEnum& ice, const QueryTarget bo)
    {
        switch(bo)
        {
            case QueryTarget::WM: ice = armem::query::data::QueryTarget::WM; break;
            case QueryTarget::WM_LTM: ice = armem::query::data::QueryTarget::WM_LTM; break;
        }
    }
    void fromIce(const armem::query::data::QueryTarget::QueryTargetEnum ice, QueryTarget& bo)
    {
        switch(ice)
        {
            case armem::query::data::QueryTarget::WM: bo = QueryTarget::WM; break;
            case armem::query::data::QueryTarget::WM_LTM: bo = QueryTarget::WM_LTM; break;
        }
    }
}
