#pragma once


namespace armarx::armem::query
{

    enum class DataMode
    {
        NoData,     ///< Just get the structure, but no ARON data.
        WithData,   ///< Get structure and ARON data.
    };

    DataMode boolToDataMode(bool withData);
}
