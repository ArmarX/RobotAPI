#pragma once

#include <RobotAPI/interface/armem/query.h>

namespace armarx::armem::query
{

    enum class QueryTarget
    {
        WM,
        WM_LTM
    };
    void toIce(armem::query::data::QueryTarget::QueryTargetEnum& ice, const QueryTarget bo);
    void fromIce(const armem::query::data::QueryTarget::QueryTargetEnum ice, QueryTarget& bo);
}
