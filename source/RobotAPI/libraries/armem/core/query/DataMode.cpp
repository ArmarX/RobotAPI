#include "DataMode.h"

namespace armarx::armem::query
{
    DataMode boolToDataMode(bool withData)
    {
        return withData ? DataMode::WithData : DataMode::NoData;
    }
}
