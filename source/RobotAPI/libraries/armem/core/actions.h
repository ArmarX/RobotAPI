#pragma once

#include <vector>

#include <RobotAPI/interface/armem/actions.h>

namespace armarx::armem::actions
{
    // Business objects to make working with the Armem
    // action interface easier. To see an example usage,
    // check the ExampleMemory.

    struct MenuEntry
    {
        MenuEntry(const std::string& id,
                  const std::string& text,
                  const std::vector<MenuEntry>& entries = {});

        MenuEntry& add(const std::string& id,
                       const std::string& text,
                       const std::vector<MenuEntry>& entries = {});

        data::MenuEntryPtr toIce() const;

        static MenuEntry fromIce(const data::MenuEntryPtr& ice);

    public:
        std::string id;
        std::string text;
        std::vector<MenuEntry> entries;
    };


    struct Action : public MenuEntry
    {
        Action(const std::string& id, const std::string& text);
    };

    struct SubMenu : public MenuEntry
    {
        SubMenu(const std::string& id,
                const std::string& text,
                const std::vector<MenuEntry>& entries = {});

        MenuEntry& add(const std::string& id,
                       const std::string& text,
                       const std::vector<MenuEntry>& entries = {});
    };


    class Menu
    {

    public:
        Menu(std::initializer_list<MenuEntry> entries);

        Menu(const std::vector<MenuEntry>& entries = {});

        MenuEntry& add(const std::string& id,
                       const std::string& text,
                       const std::vector<MenuEntry>& entries = {});

        data::MenuPtr toIce() const;

        static Menu fromIce(const data::MenuPtr& ice);

    public:
        std::vector<MenuEntry> entries;
    };

    using data::ExecuteActionInputSeq;
    using data::ExecuteActionOutputSeq;
    using data::GetActionsInputSeq;
    using data::GetActionsOutputSeq;
    using data::ActionPath;

} // namespace armarx::armem::actions
