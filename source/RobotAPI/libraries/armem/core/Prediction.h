/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::armem::client
 * @author     phesch ( ulila at student dot kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/interface/armem/prediction.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>


namespace armarx::armem
{

    struct PredictionEngine
    {
        std::string engineID;

        static PredictionEngine fromIce(const armem::prediction::data::PredictionEngine& ice);
        armem::prediction::data::PredictionEngine toIce() const;
    };

    struct PredictionSettings
    {
        std::string predictionEngineID;

        static PredictionSettings fromIce(const armem::prediction::data::PredictionSettings& ice);
        armem::prediction::data::PredictionSettings toIce() const;
    };

    struct PredictionRequest
    {
        armem::MemoryID snapshotID;
        PredictionSettings predictionSettings;

        static PredictionRequest fromIce(const armem::prediction::data::PredictionRequest& ice);
        armem::prediction::data::PredictionRequest toIce() const;
    };

    struct PredictionResult
    {
        bool success;
        std::string errorMessage;

        armem::MemoryID snapshotID;
        aron::data::DictPtr prediction;

        static PredictionResult fromIce(const armem::prediction::data::PredictionResult& ice);
        armem::prediction::data::PredictionResult toIce() const;
    };


    void toIce(armem::prediction::data::PredictionEngine& ice,
               const PredictionEngine& engine);
    void fromIce(const armem::prediction::data::PredictionEngine& ice,
                 PredictionEngine& engine);

    void toIce(armem::prediction::data::PredictionSettings& ice,
               const PredictionSettings& settings);
    void fromIce(const armem::prediction::data::PredictionSettings& ice,
                 PredictionSettings& settings);

    void toIce(armem::prediction::data::PredictionRequest& ice, const PredictionRequest& request);
    void fromIce(const armem::prediction::data::PredictionRequest& ice,
                 PredictionRequest& request);

    void toIce(armem::prediction::data::PredictionResult& ice, const PredictionResult& result);
    void fromIce(const armem::prediction::data::PredictionResult& ice, PredictionResult& result);

} // namespace armarx::armem
