#pragma once

#include <ArmarXCore/core/ice_conversions.h>

#define DEPRECATION_TO_ICE "This function is deprecated. Use armarx::toIce() from <ArmarXCore/core/ice_conversions.h> instead."
#define DEPRECATION_FROM_ICE "This function is deprecated. Use armarx::fromIce() from <ArmarXCore/core/ice_conversions.h> instead."


namespace armarx::armem
{

    // Same type
    template <class T>
    [[deprecated(DEPRECATION_TO_ICE)]]
    void toIce(T& ice, const T& cpp)
    {
        ice = cpp;
    }
    template <class T>
    [[deprecated(DEPRECATION_FROM_ICE)]]
    void fromIce(const T& ice, T& cpp)
    {
        cpp = ice;
    }


    // Ice Handle
    template <class IceT, class CppT>
    [[deprecated(DEPRECATION_TO_ICE)]]
    void toIce(::IceInternal::Handle<IceT>& ice, const CppT& cpp)
    {
        ice = new IceT();
        toIce(*ice, cpp);
    }
    template <class IceT, class CppT>
    [[deprecated(DEPRECATION_FROM_ICE)]]
    void fromIce(const ::IceInternal::Handle<IceT>& ice, CppT& cpp)
    {
        if (ice)
        {
            fromIce(*ice, cpp);
        }
    }


    // General return version
    template <class IceT, class CppT>
    [[deprecated(DEPRECATION_TO_ICE)]]
    IceT toIce(const CppT& cpp)
    {
        IceT ice;
        toIce(ice, cpp);
        return ice;
    }
    template <class CppT, class IceT>
    [[deprecated(DEPRECATION_FROM_ICE)]]
    CppT fromIce(const IceT& ice)
    {
        CppT cpp;
        fromIce(ice, cpp);
        return cpp;
    }

    // std::unique_ptr

    template <class IceT, class CppT>
    [[deprecated(DEPRECATION_TO_ICE)]]
    void toIce(IceT& ice, const std::unique_ptr<CppT>& cppPointer)
    {
        if (cppPointer)
        {
            toIce(ice, *cppPointer);
        }
    }
    template <class IceT, class CppT>
    [[deprecated(DEPRECATION_FROM_ICE)]]
    void fromIce(const IceT& ice, std::unique_ptr<CppT>& cppPointer)
    {
        cppPointer = std::make_unique<CppT>();
        fromIce(ice, *cppPointer);
    }


    // Ice Handle <-> std::unique_ptr

    template <class IceT, class CppT>
    [[deprecated(DEPRECATION_TO_ICE)]]
    void toIce(::IceInternal::Handle<IceT>& ice, const std::unique_ptr<CppT>& cppPointer)
    {
        if (cppPointer)
        {
            ice = new IceT();
            toIce(*ice, *cppPointer);
        }
        else
        {
            ice = nullptr;
        }
    }
    template <class IceT, class CppT>
    [[deprecated(DEPRECATION_FROM_ICE)]]
    void fromIce(const ::IceInternal::Handle<IceT>& ice, std::unique_ptr<CppT>& cppPointer)
    {
        if (ice)
        {
            cppPointer = std::make_unique<CppT>();
            fromIce(*ice, *cppPointer);
        }
        else
        {
            cppPointer = nullptr;
        }
    }


    // std::vector

    template <class IceT, class CppT>
    [[deprecated(DEPRECATION_TO_ICE)]]
    void toIce(std::vector<IceT>& ices, const std::vector<CppT>& cpps)
    {
        ices.clear();
        ices.reserve(cpps.size());
        for (const auto& cpp : cpps)
        {
            toIce(ices.emplace_back(), cpp);
        }
    }
    template <class IceT, class CppT>
    [[deprecated(DEPRECATION_FROM_ICE)]]
    void fromIce(const std::vector<IceT>& ices, std::vector<CppT>& cpps)
    {
        cpps.clear();
        cpps.reserve(ices.size());
        for (const auto& ice : ices)
        {
            fromIce(ice, cpps.emplace_back());
        }
    }

    template <class IceT, class CppT>
    [[deprecated(DEPRECATION_TO_ICE)]]
    std::vector<IceT> toIce(const std::vector<CppT>& cpps)
    {
        std::vector<IceT> ices;
        toIce(ices, cpps);
        return ices;
    }


    // std::map

    template <class IceKeyT, class IceValueT, class CppKeyT, class CppValueT>
    [[deprecated(DEPRECATION_TO_ICE)]]
    void toIce(std::map<IceKeyT, IceValueT>& iceMap,
               const std::map<CppKeyT, CppValueT>& cppMap)
    {
        iceMap.clear();
        for (const auto& [key, value] : cppMap)
        {
            iceMap.emplace(toIce<IceKeyT>(key), toIce<IceValueT>(value));
        }
    }
    template <class IceKeyT, class IceValueT, class CppKeyT, class CppValueT>
    [[deprecated(DEPRECATION_FROM_ICE)]]
    void fromIce(const std::map<IceKeyT, IceValueT>& iceMap,
                 std::map<CppKeyT, CppValueT>& cppMap)
    {
        cppMap.clear();
        for (const auto& [key, value] : iceMap)
        {
            cppMap.emplace(fromIce<CppKeyT>(key), fromIce<CppValueT>(value));
        }
    }

    template <class IceKeyT, class IceValueT, class CppKeyT, class CppValueT>
    [[deprecated(DEPRECATION_TO_ICE)]]
    std::map<IceKeyT, IceValueT> toIce(const std::map<CppKeyT, CppValueT>& cppMap)
    {
        std::map<IceKeyT, IceValueT> iceMap;
        toIce(iceMap, cppMap);
        return iceMap;
    }
}
