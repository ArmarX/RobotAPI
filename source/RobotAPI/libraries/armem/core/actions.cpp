#include "actions.h"

#include <RobotAPI/interface/armem/actions.h>

namespace armarx::armem::actions
{

    MenuEntry::MenuEntry(const std::string& id,
                         const std::string& text,
                         const std::vector<MenuEntry>& entries) :
        id(id), text(text), entries(entries)
    {
    }

    MenuEntry&
    MenuEntry::add(const std::string& id,
                   const std::string& text,
                   const std::vector<MenuEntry>& entries)
    {
        return this->entries.emplace_back(id, text, entries);
    }

    data::MenuEntryPtr
    MenuEntry::toIce() const // NOLINT (recursive call chain)
    {
        if (entries.empty())
        {
            return new data::Action{this->id, this->text};
        }

        data::SubMenuPtr ice = new data::SubMenu{this->id, this->text, {}};
        for (const MenuEntry& entry : entries)
        {
            ice->entries.push_back(entry.toIce());
        }
        return ice;
    }

    MenuEntry
    MenuEntry::fromIce(const data::MenuEntryPtr& ice) // NOLINT (recursive call chain)
    {
        if (ice->ice_isA(data::SubMenu::ice_staticId()))
        {
            auto ptr = IceUtil::Handle<data::SubMenu>::dynamicCast(ice);
            MenuEntry subMenu{ptr->id, ptr->text, {}};
            for (const auto& entry : ptr->entries)
            {
                subMenu.entries.push_back(MenuEntry::fromIce(entry));
            }
            return subMenu;
        }

        return MenuEntry{ice->id, ice->text, {}};
    }

    Action::Action(const std::string& id, const std::string& text) : MenuEntry(id, text)
    {
    }

    SubMenu::SubMenu(const std::string& id,
                     const std::string& text,
                     const std::vector<MenuEntry>& entries) :
        MenuEntry(id, text, entries)
    {
    }

    MenuEntry&
    SubMenu::add(const std::string& id,
                 const std::string& text,
                 const std::vector<MenuEntry>& entries)
    {
        return this->entries.emplace_back(id, text, entries);
    }


    Menu::Menu(std::initializer_list<MenuEntry> entries) : entries(entries)
    {
    }

    Menu::Menu(const std::vector<MenuEntry>& entries) : entries(entries)
    {
    }


    MenuEntry&
    Menu::add(const std::string& id, const std::string& text, const std::vector<MenuEntry>& entries)
    {
        return this->entries.emplace_back(id, text, entries);
    }

    data::MenuPtr
    Menu::toIce() const
    {
        data::MenuPtr ice{new data::Menu};
        for (const MenuEntry& entry : entries)
        {
            ice->entries.push_back(entry.toIce());
        }
        return ice;
    }

    Menu
    Menu::fromIce(const data::MenuPtr& ice)
    {
        Menu menu;
        for (const auto& entry : ice->entries)
        {
            menu.entries.push_back(MenuEntry::fromIce(entry));
        }
        return menu;
    }
} // namespace armarx::armem::actions
