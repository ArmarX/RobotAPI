#include "MemoryID_operators.h"

#include "MemoryID.h"


std::ostream& armarx::armem::operator<<(std::ostream& os, const std::vector<MemoryID>& rhs)
{
    os << "std::vector<MemoryID> with " << rhs.size() << " entries:";
    for (size_t i = 0; i < rhs.size(); ++i)
    {
        os << "\n\t[" << i << "] " << rhs[i];
    }
    return os;
}


bool armarx::armem::compareTimestamp(const MemoryID& lhs, const MemoryID& rhs)
{
    return lhs.timestamp < rhs.timestamp;
}


bool armarx::armem::compareTimestampDecreasing(const MemoryID& lhs, const MemoryID& rhs)
{
    return lhs.timestamp > rhs.timestamp;
}
