#pragma once

#include <SimoxUtility/json/json.hpp>


namespace armarx::armem
{
    class MemoryID;

    void to_json(nlohmann::json& j, const MemoryID& bo);
    void from_json(const nlohmann::json& j, MemoryID& bo);
}
