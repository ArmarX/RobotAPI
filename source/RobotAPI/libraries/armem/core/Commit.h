#pragma once

#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>

#include <RobotAPI/libraries/aron/core/data/variant/forward_declarations.h>

#include <memory>
#include <vector>


namespace armarx::armem
{

    /**
     * @brief The type of an update
     */
    enum class UpdateType
    {
        UpdatedExisting,
        InsertedNew
    };

    /**
     * @brief An update of an entity for a specific point in time.
     */
    struct EntityUpdate
    {
        /// The entity's ID.
        MemoryID entityID;

        /// The entity data.
        std::vector<aron::data::DictPtr> instancesData;

        /**
         * @brief Time when this entity update was created (e.g. time of image recording).
         * This is the key of the entity's history.
         */
        Time timeCreated = Time::Invalid();


        // OPTIONAL

        /// An optional confidence, may be used for things like decay.
        float confidence = 1.0;


        // OPTIONAL

        /**
         * @brief Time when this update was sent to the memory server.
         *
         * Set automatically when sending the commit.
         */
        Time timeSent = Time::Invalid();

        /**
         * @brief Time when this update arrived at the memory server.
         *
         * Set by memory server on arrival.
         */
        Time timeArrived = Time::Invalid();


        friend std::ostream& operator<<(std::ostream& os, const EntityUpdate& rhs);
    };


    /**
     * @brief Result of an `EntityUpdate`.
     */
    struct EntityUpdateResult
    {
        bool success = false;

        MemoryID snapshotID;
        Time timeArrived = Time::Invalid();

        std::string errorMessage;

        friend std::ostream& operator<<(std::ostream& os, const EntityUpdateResult& rhs);
    };



    /**
     * @brief A bundle of updates to be sent to the memory.
     */
    struct Commit
    {
        /**
         * @brief The entity updates.
         *
         * May contain updates of multiple entities at different
         * points in time.
         */
        std::vector<EntityUpdate> updates;

        EntityUpdate& add();
        EntityUpdate& add(const EntityUpdate& update);
        void add(const std::vector<EntityUpdate>& updates);
        void append(const Commit& c);

        friend std::ostream& operator<<(std::ostream& os, const Commit& rhs);
    };

    /**
     * @brief Result of a `Commit`.
     */
    struct CommitResult
    {
        std::vector<EntityUpdateResult> results;

        bool allSuccess() const;
        std::vector<std::string> allErrorMessages() const;

        friend std::ostream& operator<<(std::ostream& os, const CommitResult& rhs);
    };


}
