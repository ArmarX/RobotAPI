#include "Commit.h"

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SimoxUtility/algorithm/apply.hpp>


namespace armarx::armem
{

    std::ostream& operator<<(std::ostream& os, const EntityUpdate& rhs)
    {
        return os << "Entity update: "
               << "\n- success:    \t" << rhs.entityID
               << "\n- timestamp:  \t" << toDateTimeMilliSeconds(rhs.timeCreated)
               << "\n- #instances: \t" << rhs.instancesData.size()
               << "\n"
               ;
    }

    std::ostream& operator<<(std::ostream& os, const EntityUpdateResult& rhs)
    {
        return os << "Entity update result: "
               << "\n- success:       \t" << (rhs.success ? "true" : "false")
               << "\n- snapshotID:    \t" << rhs.snapshotID
               << "\n- time arrived:  \t" << toDateTimeMilliSeconds(rhs.timeArrived)
               << "\n- error message: \t" << rhs.errorMessage
               << "\n"
               ;
    }

    std::ostream& operator<<(std::ostream& os, const Commit& rhs)
    {
        os << "Commit with " << rhs.updates.size() << " entity update";
        if (rhs.updates.size() > 1)
        {
            os << "s";
        }
        os << ": \n";
        for (size_t i = 0; i < rhs.updates.size(); ++i)
        {
            os << "[" << i << "] " << rhs.updates[i];
        }
        return os;
    }
    std::ostream& operator<<(std::ostream& os, const CommitResult& rhs)
    {
        os << "Commit results of " << rhs.results.size() << " entity update";
        if (rhs.results.size() > 1)
        {
            os << "s";
        }
        os << ": \n";
        for (size_t i = 0; i < rhs.results.size(); ++i)
        {
            os << "[" << i << "] " << rhs.results[i];
        }
        return os;
    }


    bool CommitResult::allSuccess() const
    {
        return std::find_if(results.begin(), results.end(), [](const EntityUpdateResult & r)
        {
            return !r.success;
        }) == results.end();
    }

    std::vector<std::string> CommitResult::allErrorMessages() const
    {
        return simox::alg::apply(results, [](const EntityUpdateResult & res)
        {
            return res.errorMessage;
        });
    }


    EntityUpdate& Commit::add()
    {
        return updates.emplace_back();
    }

    EntityUpdate& Commit::add(const EntityUpdate& update)
    {
        return updates.emplace_back(update);
    }

    void Commit::add(const std::vector<EntityUpdate>& updates)
    {
        for (const auto& update : updates)
        {
            add(update);
        }
    }

    void Commit::append(const Commit& c)
    {
        add(c.updates);
    }

}
