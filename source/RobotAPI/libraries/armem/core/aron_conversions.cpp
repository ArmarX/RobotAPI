#include "aron_conversions.h"

#include <RobotAPI/libraries/aron/common/aron_conversions.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/aron/MemoryID.aron.generated.h>


void armarx::armem::fromAron(const arondto::MemoryID& dto, MemoryID& bo)
{
    aron::fromAron(dto.memoryName, bo.memoryName);
    aron::fromAron(dto.coreSegmentName, bo.coreSegmentName);
    aron::fromAron(dto.providerSegmentName, bo.providerSegmentName);
    aron::fromAron(dto.entityName, bo.entityName);
    aron::fromAron(dto.timestamp, bo.timestamp);
    aron::fromAron(dto.instanceIndex, bo.instanceIndex);

}

void armarx::armem::toAron(arondto::MemoryID& dto, const MemoryID& bo)
{
    aron::toAron(dto.memoryName, bo.memoryName);
    aron::toAron(dto.coreSegmentName, bo.coreSegmentName);
    aron::toAron(dto.providerSegmentName, bo.providerSegmentName);
    aron::toAron(dto.entityName, bo.entityName);
    aron::toAron(dto.timestamp, bo.timestamp);
    aron::toAron(dto.instanceIndex, bo.instanceIndex);

}

std::ostream& armarx::armem::arondto::operator<<(std::ostream& os, const MemoryID& rhs)
{
    armem::MemoryID bo;
    fromAron(rhs, bo);
    return os << bo;
}
