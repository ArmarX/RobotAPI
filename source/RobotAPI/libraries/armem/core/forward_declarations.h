#pragma once

#include <ArmarXCore/core/time/forward_declarations.h>


namespace IceInternal
{
    template<typename T> class Handle;
}

namespace armarx::armem
{
    using Time = armarx::core::time::DateTime;
    using Duration = armarx::core::time::Duration;

    class MemoryID;
    class Commit;
    class EntityUpdate;
    class CommitResult;
    class EntityUpdateResult;
}
namespace armarx::armem::dto
{
    using Time = armarx::core::time::dto::DateTime;
    using Duration = armarx::core::time::dto::Duration;
}

namespace armarx::armem::arondto
{
    class MemoryID;
}

namespace armarx::armem::base
{
    struct NoData;
    struct EntityInstanceMetadata;

    template <class _DataT, class _MetadataT>
    class EntityInstanceBase;
    template <class _EntityInstanceT, class _Derived>
    class EntitySnapshotBase;
    template <class _EntitySnapshotT, class _Derived>
    class EntityBase;
    template <class _EntityT, class _Derived>
    class ProviderSegmentBase;
    template <class _ProviderSegmentT, class _Derived>
    class CoreSegmentBase;
    template <class _CoreSegmentT, class _Derived>
    class MemoryBase;
}

namespace armarx::armem::wm
{
    class EntityInstance;
    class EntitySnapshot;
    class Entity;
    class ProviderSegment;
    class CoreSegment;
    class Memory;
}

namespace armarx::armem::server::wm
{
    using EntityInstance = armem::wm::EntityInstance;
    using EntitySnapshot = armem::wm::EntitySnapshot;
    class Entity;
    class ProviderSegment;
    class CoreSegment;
    class Memory;
}

namespace armarx::armem::data
{
    struct MemoryID;
    struct Commit;
    struct CommitResult;
    struct EntityUpdate;
    struct EntityUpdateResult;
}

namespace armarx::armem::actions
{
    class Menu;
}

namespace armarx::armem::actions::data
{
    class Menu;
    using MenuPtr = ::IceInternal::Handle<Menu>;
}
