#include "mns.h"

#include <sstream>

#include "../MemoryID.h"


namespace armarx::armem::error
{

    MemoryNameSystemQueryFailed::MemoryNameSystemQueryFailed(const std::string& function, const std::string& errorMessage) :
        ArMemError(makeMsg(function, errorMessage))
    {
    }

    std::string MemoryNameSystemQueryFailed::makeMsg(const std::string& function, const std::string& errorMessage)
    {
        std::stringstream ss;
        ss << "Failed to call '" << function << "' on the memory name system.\n";
        if (not errorMessage.empty())
        {
            ss << "\n" << errorMessage;
        }
        return ss.str();
    }



    CouldNotResolveMemoryServer::CouldNotResolveMemoryServer(const MemoryID& memoryID, const std::string& errorMessage) :
        ArMemError(makeMsg(memoryID, errorMessage))
    {
    }

    std::string CouldNotResolveMemoryServer::makeMsg(const MemoryID& memoryID, const std::string& errorMessage)
    {
        std::stringstream ss;
        ss << "Could not resolve the memory name '" << memoryID << "'."
           << "\nMemory server for '" << memoryID << "' is not registered.";
        if (not errorMessage.empty())
        {
            ss << "\n" << errorMessage;
        }
        return ss.str();
    }


    ServerRegistrationOrRemovalFailed::ServerRegistrationOrRemovalFailed(const std::string& verb, const MemoryID& memoryID, const std::string& errorMessage) :
        ArMemError(makeMsg(verb, memoryID, errorMessage))
    {

    }

    std::string ServerRegistrationOrRemovalFailed::makeMsg(const std::string& verb, const MemoryID& memoryID, const std::string& errorMessage)
    {
        std::stringstream ss;
        ss << "Failed to " << verb << " memory server for '" << memoryID << "' in the Memory Name System (MNS).";
        if (not errorMessage.empty())
        {
            ss << "\n" << errorMessage;
        }
        return ss.str();
    }

}
