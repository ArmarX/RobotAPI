#pragma once

#include <stdexcept>

#include <SimoxUtility/meta/type_name.h>


namespace armarx::armem
{
    class MemoryID;
}

namespace armarx::armem::error
{

    /**
     * @brief Base class for all exceptions thrown by the armem library.
     */
    class ArMemError : public std::runtime_error
    {
    public:

        ArMemError(const std::string& msg);

    };


    /**
     * @brief Indicates that an argument was invalid.
     */
    class InvalidArgument : public ArMemError
    {
    public:

        InvalidArgument(const std::string& argument, const std::string& function,
                        const std::string& message);

        static std::string makeMsg(const std::string& argument, const std::string& function,
                                   const std::string& message);

    };



    /**
     * @brief Indicates that a name in a given ID does not match a container's own name.
     */
    class ContainerNameMismatch : public ArMemError
    {
    public:

        ContainerNameMismatch(const std::string& gottenName,
                              const std::string& containerTerm, const std::string& containerName);

        static std::string makeMsg(const std::string& gottenName,
                                   const std::string& containerTerm, const std::string& containerName);

    };


    /**
     * @brief Indicates that a name in a given ID does not match a container's own name.
     */
    class ContainerEntryAlreadyExists : public ArMemError
    {
    public:

        ContainerEntryAlreadyExists(const std::string& existingTerm, const std::string& existingName,
                                    const std::string& ownTerm, const std::string& ownName);

        static std::string makeMsg(const std::string& existingTerm, const std::string& existingName,
                                   const std::string& ownTerm, const std::string& ownName);

    };


    /**
     * @brief Indicates that a container did not have an entry under a given name.
     */
    class MissingEntry : public ArMemError
    {
    public:

        template <class MissingT, class ContainerT>
        static MissingEntry create(const std::string& missingKey, const ContainerT& container)
        {
            return MissingEntry(MissingT::getLevelName(), missingKey,
                                ContainerT::getLevelName(), container.getKeyString(), container.size());
        }


        MissingEntry(const std::string& missingTerm, const std::string& missingName,
                     const std::string& containerTerm, const std::string& containerName,
                     size_t containerSize);

        static std::string makeMsg(const std::string& missingTerm, const std::string& missingName,
                                   const std::string& containerTerm, const std::string& containerName,
                                   size_t size);
    };


    /**
     * @brief Indicates that a container did have an entry, but the entry's data was
     * null when trying to access it.
     */
    class MissingData : public ArMemError
    {
    public:
        MissingData(const std::string& missingTerm, const std::string& missingName,
                    const std::string& ownTerm, const std::string& ownName);

        static std::string makeMsg(const std::string& missingTerm, const std::string& missingName,
                                   const std::string& ownTerm, const std::string& ownName);
    };


    /**
     * @brief Indicates that a string could not be parsed as integer.
     */
    class ParseIntegerError : public ArMemError
    {
    public:
        ParseIntegerError(std::string string, std::string semanticName);

        static std::string makeMsg(std::string string, std::string semanticName);
    };



    /**
     * @brief Indicates that a memory ID is invalid, e.g. does not contain necessary information.
     */
    class InvalidMemoryID : public ArMemError
    {
    public:

        InvalidMemoryID(const MemoryID& id, const std::string& message);

        static std::string makeMsg(const MemoryID& id, const std::string& message);

    };


    /**
     * @brief Indicates that an entity's history was queried, but is empty.
     */
    class EntityHistoryEmpty : public ArMemError
    {
    public:

        EntityHistoryEmpty(const std::string& entityName, const std::string& message = "");

        static std::string makeMsg(const std::string& entityName, const std::string& message = "");

    };


    /**
     * @brief Indicates that an entity's history was queried, but is empty.
     */
    class UnknownQueryType : public ArMemError
    {
    public:

        template <class QueryType>
        UnknownQueryType(const std::string& term, const QueryType& query) :
            UnknownQueryType(term, simox::meta::get_type_name(query))
        {
        }
        UnknownQueryType(const std::string& term, const std::string& typeName);

        static std::string makeMsg(const std::string& term, const std::string& typeName);

    };

    /**
     * @brief Indicates that a query resulted in an Error.
     */
    class QueryFailed : public ArMemError
    {
    public:

        QueryFailed(const std::string& memory, const std::string& message = "");

        static std::string makeMsg(const std::string& memory, const std::string& message = "");

    };


    /**
     * @brief Indicates that something went wrong when accessing the filesystem.
     */
    class IOError : public ArMemError
    {
    public:

        IOError(const std::string& path, const std::string& message = "");

        static std::string makeMsg(const std::string& path, const std::string& message = "");


        std::string path;

    };

    /**
     * @brief Indicates that a proxy required for an operation wasn't usable.
     */
    class ProxyNotSet : public ArMemError
    {
    public:
        ProxyNotSet(const std::string& proxyName, const std::string& message = "");

        static std::string makeMsg(const std::string& proxyName, const std::string& message = "");
    };

} // namespace armarx::armem::error
