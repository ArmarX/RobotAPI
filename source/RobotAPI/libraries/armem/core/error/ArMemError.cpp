#include "ArMemError.h"

#include <sstream>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include "../MemoryID.h"


namespace armarx::armem::error
{


    ArMemError::ArMemError(const std::string& msg) : std::runtime_error(msg)
    {
    }


    InvalidArgument::InvalidArgument(const std::string& argument,
                                     const std::string& function,
                                     const std::string& message) :
        ArMemError(makeMsg(argument, function, message))
    {
    }

    std::string
    InvalidArgument::makeMsg(const std::string& argument,
                             const std::string& function,
                             const std::string& message)
    {
        std::stringstream ss;
        ss << "Invalid value for argument '" << argument << "' in function " << function << "()";
        if (message.empty())
        {
            ss << ".";
        }
        else
        {
            ss << ":\n" << message;
        }
        return ss.str();
    }


    ContainerNameMismatch::ContainerNameMismatch(const std::string& gottenName,
                                                 const std::string& ownTerm,
                                                 const std::string& containerName) :
        ArMemError(makeMsg(gottenName, ownTerm, containerName))
    {
    }

    std::string
    ContainerNameMismatch::makeMsg(const std::string& gottenName,
                                   const std::string& containerTerm,
                                   const std::string& containerName)
    {
        std::stringstream ss;
        ss << "Name '" << gottenName << "' does not match name of " << containerTerm << " '"
           << containerName << "'.";
        return ss.str();
    }


    ContainerEntryAlreadyExists::ContainerEntryAlreadyExists(const std::string& existingTerm,
                                                             const std::string& existingName,
                                                             const std::string& ownTerm,
                                                             const std::string& ownName) :
        ArMemError(makeMsg(existingTerm, existingName, ownTerm, ownName))
    {
    }

    std::string
    ContainerEntryAlreadyExists::makeMsg(const std::string& existingTerm,
                                         const std::string& existingName,
                                         const std::string& ownTerm,
                                         const std::string& ownName)
    {
        std::stringstream ss;
        ss << simox::alg::capitalize_words(existingTerm) << " with name '" << existingName << "' "
           << "already exists in " << ownTerm << " '" << ownName << "'.";
        return ss.str();
    }


    MissingEntry::MissingEntry(const std::string& missingTerm,
                               const std::string& missingName,
                               const std::string& containerTerm,
                               const std::string& containerName,
                               size_t size) :
        ArMemError(makeMsg(missingTerm, missingName, containerTerm, containerName, size))
    {
    }

    std::string
    MissingEntry::makeMsg(const std::string& missingTerm,
                          const std::string& missingName,
                          const std::string& containerTerm,
                          const std::string& containerName,
                          size_t size)
    {
        std::stringstream ss;
        ss << "No " << missingTerm << " with name '" << missingName << "' "
           << "in " << containerTerm << " '" << containerName << "' (with size " << size << ").";
        return ss.str();
    }


    MissingData::MissingData(const std::string& missingTerm,
                             const std::string& missingName,
                             const std::string& ownTerm,
                             const std::string& ownName) :
        ArMemError(makeMsg(missingTerm, missingName, ownTerm, ownName))
    {
    }

    std::string
    MissingData::makeMsg(const std::string& missingTerm,
                         const std::string& missingName,
                         const std::string& ownTerm,
                         const std::string& ownName)
    {
        std::stringstream ss;
        ss << "No " << missingTerm << " data at '" << missingName << "' "
           << "in " << ownTerm << " '" << ownName << "'.";
        return ss.str();
    }


    ParseIntegerError::ParseIntegerError(std::string string, std::string semanticName) :
        ArMemError(makeMsg(string, semanticName))
    {
    }

    std::string
    ParseIntegerError::makeMsg(std::string string, std::string semanticName)
    {
        std::stringstream ss;
        ss << "Failed to parse " << semanticName << " '" << string << "' as integer.";
        return ss.str();
    }


    InvalidMemoryID::InvalidMemoryID(const MemoryID& id, const std::string& message) :
        ArMemError(makeMsg(id, message))
    {
    }

    std::string
    InvalidMemoryID::makeMsg(const MemoryID& id, const std::string& message)
    {
        std::stringstream ss;
        ss << "Invalid memory ID " << id << ": " << message;
        return ss.str();
    }


    EntityHistoryEmpty::EntityHistoryEmpty(const std::string& entityName,
                                           const std::string& message) :
        ArMemError(makeMsg(entityName, message))
    {
    }

    std::string
    EntityHistoryEmpty::makeMsg(const std::string& entityName, const std::string& message)
    {
        std::stringstream ss;
        ss << "History of entity '" << entityName << "' is empty";
        if (message.size() > 0)
        {
            ss << " " << message;
        }
        else
        {
            ss << ".";
        }
        return ss.str();
    }


    UnknownQueryType::UnknownQueryType(const std::string& term, const std::string& typeName) :
        ArMemError(makeMsg(term, typeName))
    {
    }

    std::string
    UnknownQueryType::makeMsg(const std::string& term, const std::string& typeName)
    {
        std::stringstream ss;
        ss << "Unknown " << term << " query type '" << typeName << "'.";
        return ss.str();
    }

    QueryFailed::QueryFailed(const std::string& memory, const std::string& message) :
        ArMemError(makeMsg(memory, message))
    {
    }

    std::string QueryFailed::makeMsg(const std::string& memory, const std::string& message)
    {
        std::stringstream ss;
        ss << "Query from memory " << memory << " failed with message: " << message;
        return ss.str();
    }

    IOError::IOError(const std::string& path, const std::string& message) :
        ArMemError(makeMsg(path, message)), path(path)
    {
    }


    std::string
    IOError::makeMsg(const std::string& path, const std::string& message)
    {
        std::stringstream ss;
        ss << "IOError on path \"" << path << "\".";
        if (message.size() > 0)
        {
            ss << "\n" << message;
        }
        return ss.str();
    }

    ProxyNotSet::ProxyNotSet(const std::string& proxyName, const std::string& message) :
        ArMemError(makeMsg(proxyName, message))
    {
    }

    std::string
    ProxyNotSet::makeMsg(const std::string& proxyName, const std::string& message)
    {
        std::stringstream sstream;
        sstream << "Proxy \"" << proxyName << "\" not set.";
        if (!message.empty())
        {
            sstream << "\n" << message;
        }
        return sstream.str();
    }

} // namespace armarx::armem::error
