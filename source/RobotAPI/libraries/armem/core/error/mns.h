#pragma once

#include "ArMemError.h"


namespace armarx::armem::error
{

    /**
     * @brief Indicates that a query to the Memory Name System failed.
     */
    class MemoryNameSystemQueryFailed : public ArMemError
    {
    public:

        MemoryNameSystemQueryFailed(const std::string& function, const std::string& errorMessage = "");

        static std::string makeMsg(const std::string& function, const std::string& errorMessage = "");

    };


    /**
     * @brief Indicates that a query to the Memory Name System failed.
     */
    class CouldNotResolveMemoryServer : public ArMemError
    {
    public:

        CouldNotResolveMemoryServer(const MemoryID& memoryID, const std::string& errorMessage = "");

        static std::string makeMsg(const MemoryID& memoryID, const std::string& errorMessage = "");

    };


    /**
     * @brief Indicates that a query to the Memory Name System failed.
     */
    class ServerRegistrationOrRemovalFailed : public ArMemError
    {
    public:

        ServerRegistrationOrRemovalFailed(const std::string& verb, const MemoryID& memoryID, const std::string& errorMessage = "");

        static std::string makeMsg(const std::string& verb, const MemoryID& memoryID, const std::string& errorMessage = "");

    };


}

