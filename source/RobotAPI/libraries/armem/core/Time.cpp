#include "Time.h"

#include <cmath>
#include <iomanip>


namespace armarx
{


    std::string armem::toStringMilliSeconds(const Time& time, int decimals)
    {
        std::stringstream ss;
        ss << time.toMicroSecondsSinceEpoch() / 1000;
        if (decimals > 0)
        {
            int div = int(std::pow(10, 3 - decimals));
            ss << "." << std::setfill('0') << std::setw(decimals)
               << (time.toMicroSecondsSinceEpoch() % 1000) / div;
        }
        ss << " ms";
        return ss.str();
    }


    std::string armem::toStringMicroSeconds(const Time& time)
    {
        static const char* mu = "\u03BC";
        std::stringstream ss;
        ss << time.toMicroSecondsSinceEpoch() << " " << mu << "s";
        return ss.str();
    }


    std::string armem::toDateTimeMilliSeconds(const Time& time, int decimals)
    {
        std::stringstream ss;
        ss << time.toString("%Y-%m-%d %H:%M:%S");
        if (decimals > 0)
        {
            int div = int(std::pow(10, 6 - decimals));
            ss << "." << std::setfill('0') << std::setw(decimals)
               << (time.toMicroSecondsSinceEpoch() % int(1e6)) / div;
        }

        return ss.str();
    }


    armem::Time armem::timeFromStringMicroSeconds(const std::string& microSeconds)
    {
        return Time(Duration::MicroSeconds(std::stol(microSeconds)), ClockType::Virtual);
    }

}



