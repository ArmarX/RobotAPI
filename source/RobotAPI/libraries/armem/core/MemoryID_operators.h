#pragma once

// #include "MemoryID.h"

#include <vector>
#include <ostream>


namespace armarx::armem
{
    class MemoryID;

    std::ostream& operator<<(std::ostream& os, const std::vector<MemoryID>& rhs);

    /// lhs.timestamp < rhs.timstamp
    bool compareTimestamp(const MemoryID& lhs, const MemoryID& rhs);
    /// lhs.timestamp > rhs.timstamp
    bool compareTimestampDecreasing(const MemoryID& lhs, const MemoryID& rhs);

}


