#include "memory_definitions.h"

#include "error.h"

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <map>
#include <vector>



namespace armarx::armem::wm
{

    bool EntityInstance::equalsDeep(const EntityInstance& other) const
    {
        if (_data and other.data())
        {
            return id() == other.id()
                   && _metadata == other.metadata()
                   && *_data == *other.data();
        }
        if (_data or other.data())
        {
            return false;
        }
        return id() == other.id() && _metadata == other.metadata();
    }


    void EntityInstance::update(const EntityUpdate& update)
    {
        ARMARX_CHECK_FITS_SIZE(this->index(), update.instancesData.size());

        this->data() = update.instancesData.at(size_t(this->index()));

        this->_metadata.confidence = update.confidence;

        this->_metadata.timeCreated = update.timeCreated;
        this->_metadata.timeSent = update.timeSent;
        this->_metadata.timeArrived = update.timeArrived;
    }

}
