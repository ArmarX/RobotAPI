#pragma once

#include <RobotAPI/libraries/aron/core/data/variant/forward_declarations.h>

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>


namespace armarx::armem
{
    /// convert from metadata and data aron instance
    void from_aron(const aron::data::DictPtr& metadata, const aron::data::DictPtr& data, wm::EntityInstance&);

    /// convert to metadata and aron instance
    void to_aron(aron::data::DictPtr& metadata, aron::data::DictPtr& data, const wm::EntityInstance&);
}
