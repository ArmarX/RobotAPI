#pragma once

#include <RobotAPI/interface/armem/commit.h>
#include <RobotAPI/interface/armem/memory.h>

#include "memory_definitions.h"


namespace armarx::armem::wm
{

    void toIce(data::EntityInstance& ice, const EntityInstance& data);
    void fromIce(const data::EntityInstance& ice, EntityInstance& data);


    void toIce(data::EntitySnapshot& ice, const EntitySnapshot& snapshot);
    void fromIce(const data::EntitySnapshot& ice, EntitySnapshot& snapshot);

    void toIce(data::Entity& ice, const Entity& entity);
    void fromIce(const data::Entity& ice, Entity& entity);


    void toIce(data::ProviderSegment& ice, const ProviderSegment& providerSegment);
    void fromIce(const data::ProviderSegment& ice, ProviderSegment& providerSegment);

    void toIce(data::CoreSegment& ice, const CoreSegment& coreSegment);
    void fromIce(const data::CoreSegment& ice, CoreSegment& coreSegment);

    void toIce(data::Memory& ice, const Memory& memory);
    void fromIce(const data::Memory& ice, Memory& memory);
}

// Must be included after the prototypes. Otherwise the compiler cannot find the correct methods in ice_coversion_templates.h
#include <RobotAPI/libraries/armem/core/ice_conversions.h>
