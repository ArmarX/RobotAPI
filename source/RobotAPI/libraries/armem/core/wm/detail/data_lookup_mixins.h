#pragma once

#include <RobotAPI/libraries/armem/core/forward_declarations.h>
#include <RobotAPI/libraries/armem/core/base/detail/derived.h>

#include <RobotAPI/libraries/aron/core/data/variant/forward_declarations.h>

#include <optional>


namespace armarx::armem::wm::detail
{
    using base::detail::derived;


    template <class AronDtoT>
    std::optional<AronDtoT>
    getInstanceDataAs(aron::data::DictPtr data)
    {
        if (data)
        {
            AronDtoT aron;
            aron.fromAron(data);
            return aron;
        }
        else
        {
            return std::nullopt;
        }
    }



    template <class DerivedT>
    struct FindInstanceDataMixinForSnapshot
    {

        aron::data::DictPtr
        findInstanceData(int instanceIndex = 0) const
        {
            const auto* instance = derived<DerivedT>(this).findInstance(instanceIndex);
            return instance ? instance->data() : nullptr;
        }


        template <class AronDtoT>
        std::optional<AronDtoT>
        findInstanceDataAs(int instanceIndex = 0) const
        {
            return getInstanceDataAs<AronDtoT>(derived<DerivedT>(this).findInstanceData(instanceIndex));
        }

    };



    template <class DerivedT>
    struct FindInstanceDataMixinForEntity
    {

        aron::data::DictPtr
        findLatestInstanceData(int instanceIndex = 0) const
        {
            const auto* instance = derived<DerivedT>(this).findLatestInstance(instanceIndex);
            return instance ? instance->data() : nullptr;
        }


        template <class AronDtoT>
        std::optional<AronDtoT>
        findLatestInstanceDataAs(int instanceIndex = 0) const
        {
            return getInstanceDataAs<AronDtoT>(derived<DerivedT>(this).findLatestInstanceData(instanceIndex));
        }

    };



    template <class DerivedT>
    struct FindInstanceDataMixin
    {

        aron::data::DictPtr
        findLatestInstanceData(const MemoryID& entityID, int instanceIndex = 0) const
        {
            const auto* instance = derived<DerivedT>(this).findLatestInstance(entityID, instanceIndex);
            return instance ? instance->data() : nullptr;
        }

        template <class AronDtoT>
        std::optional<AronDtoT>
        findLatestInstanceDataAs(const MemoryID& entityID, int instanceIndex = 0) const
        {
            return getInstanceDataAs<AronDtoT>(derived<DerivedT>(this).findLatestInstanceData(entityID, instanceIndex));
        }

    };

}
