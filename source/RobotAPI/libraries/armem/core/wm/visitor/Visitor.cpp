#include "Visitor.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/error.h>


namespace armarx::armem::wm
{

    Visitor::Visitor()
    {
    }

    Visitor::~Visitor()
    {
    }

    bool Visitor::applyTo(Memory& memory)
    {
        visitEnter(memory);
        bool cont = memory.forEachCoreSegment([this](CoreSegment & coreSeg)
        {
            return applyTo(coreSeg);
        });
        visitExit(memory);
        return cont;
    }

    bool Visitor::applyTo(CoreSegment& coreSegment)
    {
        visitEnter(coreSegment);
        bool cont = coreSegment.forEachProviderSegment([this](ProviderSegment & provSeg)
        {
            return applyTo(provSeg);
        });
        visitExit(coreSegment);
        return cont;
    }

    bool Visitor::applyTo(ProviderSegment& providerSegment)
    {
        visitEnter(providerSegment);
        bool cont = providerSegment.forEachEntity([this](Entity & entity)
        {
            return applyTo(entity);
        });
        visitExit(providerSegment);
        return cont;
    }

    bool Visitor::applyTo(Entity& entity)
    {
        visitEnter(entity);
        bool cont = entity.forEachSnapshot([this](EntitySnapshot & snapshot)
        {
            return applyTo(snapshot);
        });
        visitExit(entity);
        return cont;
    }

    bool Visitor::applyTo(EntitySnapshot& snapshot)
    {
        visitEnter(snapshot);
        bool cont = snapshot.forEachInstance([this](EntityInstance & instance)
        {
            return applyTo(instance);
        });
        visitExit(snapshot);
        return cont;
    }

    bool Visitor::applyTo(EntityInstance& instance)
    {
        return visit(instance);
    }


    bool Visitor::applyTo(const Memory& memory)
    {
        visitEnter(memory);
        bool cont = memory.forEachCoreSegment([this](const CoreSegment & coreSeg)
        {
            return applyTo(coreSeg);
        });
        visitExit(memory);
        return cont;
    }

    bool Visitor::applyTo(const CoreSegment& coreSegment)
    {
        visitEnter(coreSegment);
        bool cont = coreSegment.forEachProviderSegment([this](const ProviderSegment & provSeg)
        {
            return applyTo(provSeg);
        });
        visitExit(coreSegment);
        return cont;
    }

    bool Visitor::applyTo(const ProviderSegment& providerSegment)
    {
        visitEnter(providerSegment);
        bool cont = providerSegment.forEachEntity([this](const Entity & entity)
        {
            return applyTo(entity);
        });
        visitExit(providerSegment);
        return cont;
    }

    bool Visitor::applyTo(const Entity& entity)
    {
        visitEnter(entity);
        bool cont = entity.forEachSnapshot([this](const EntitySnapshot & snapshot)
        {
            return applyTo(snapshot);
        });
        visitExit(entity);
        return cont;
    }

    bool Visitor::applyTo(const EntitySnapshot& snapshot)
    {
        visitEnter(snapshot);
        bool cont = snapshot.forEachInstance([this](const EntityInstance & instance)
        {
            return applyTo(instance);
        });
        visitExit(snapshot);
        return cont;
    }

    bool Visitor::applyTo(const EntityInstance& instance)
    {
        return visit(instance);
    }

}
