#pragma once


namespace armarx::armem::wm
{
    class Memory;
    class CoreSegment;
    class ProviderSegment;
    class Entity;
    class EntitySnapshot;
    class EntityInstance;


    /**
     * @brief A visitor for the hierarchical memory data structure.
     */
    class Visitor
    {

    public:

        Visitor();
        virtual ~Visitor();

        bool applyTo(Memory& memory);
        bool applyTo(CoreSegment& coreSegment);
        bool applyTo(ProviderSegment& providerSegment);
        bool applyTo(Entity& entity);
        bool applyTo(EntitySnapshot& snapshot);
        bool applyTo(EntityInstance& instance);


        virtual bool visitEnter(Memory& memory)
        {
            return visitEnter(const_cast<const Memory&>(memory));
        }
        virtual bool visitEnter(CoreSegment& coreSegment)
        {
            return visitEnter(const_cast<const CoreSegment&>(coreSegment));
        }
        virtual bool visitEnter(ProviderSegment& providerSegment)
        {
            return visitEnter(const_cast<const ProviderSegment&>(providerSegment));
        }
        virtual bool visitEnter(Entity& entity)
        {
            return visitEnter(const_cast<const Entity&>(entity));
        }
        virtual bool visitEnter(EntitySnapshot& snapshot)
        {
            return visitEnter(const_cast<const EntitySnapshot&>(snapshot));
        }

        virtual bool visitExit(Memory& memory)
        {
            return visitExit(const_cast<const Memory&>(memory));
        }
        virtual bool visitExit(CoreSegment& coreSegment)
        {
            return visitExit(const_cast<const CoreSegment&>(coreSegment));
        }
        virtual bool visitExit(ProviderSegment& providerSegment)
        {
            return visitExit(const_cast<const ProviderSegment&>(providerSegment));
        }
        virtual bool visitExit(Entity& entity)
        {
            return visitExit(const_cast<const Entity&>(entity));
        }
        virtual bool visitExit(EntitySnapshot& snapshot)
        {
            return visitExit(const_cast<const EntitySnapshot&>(snapshot));
        }

        virtual bool visit(EntityInstance& instance)
        {
            return visit(const_cast<const EntityInstance&>(instance));
        }



        // Const versions

        bool applyTo(const Memory& memory);
        bool applyTo(const CoreSegment& coreSegment);
        bool applyTo(const ProviderSegment& providerSegment);
        bool applyTo(const Entity& entity);
        bool applyTo(const EntitySnapshot& snapshot);
        bool applyTo(const EntityInstance& instance);


        virtual bool visitEnter(const Memory& memory)
        {
            (void) memory;
            return true;
        }
        virtual bool visitEnter(const CoreSegment& coreSegment)
        {
            (void) coreSegment;
            return true;
        }
        virtual bool visitEnter(const ProviderSegment& providerSegment)
        {
            (void) providerSegment;
            return true;
        }
        virtual bool visitEnter(const Entity& entity)
        {
            (void) entity;
            return true;
        }
        virtual bool visitEnter(const EntitySnapshot& snapshot)
        {
            (void) snapshot;
            return true;
        }

        virtual bool visitExit(const Memory& memory)
        {
            (void) memory;
            return true;
        }
        virtual bool visitExit(const CoreSegment& coreSegment)
        {
            (void) coreSegment;
            return true;
        }
        virtual bool visitExit(const ProviderSegment& providerSegment)
        {
            (void) providerSegment;
            return true;
        }
        virtual bool visitExit(const Entity& entity)
        {
            (void) entity;
            return true;
        }
        virtual bool visitExit(const EntitySnapshot& snapshot)
        {
            (void) snapshot;
            return true;
        }

        virtual bool visit(const EntityInstance& instance)
        {
            (void) instance;
            return true;
        }

    };

}
