#pragma once

#include <functional>

#include "Visitor.h"


namespace armarx::armem::wm
{

    /**
     * @brief A `Visitor` which can be parametrized by `std::function`
     * instead of inheriting and overriding.
     */
    class FunctionalVisitor : public Visitor
    {
    public:

        FunctionalVisitor();
        virtual ~FunctionalVisitor() override;


        bool visitEnter(Memory& memory) override
        {
            return memoryFn ? memoryFn(memory) : Visitor::visitEnter(memory);
        }
        bool visitEnter(CoreSegment& coreSegment) override
        {
            return coreSegmentFn ? coreSegmentFn(coreSegment) : Visitor::visitEnter(coreSegment);
        }
        bool visitEnter(ProviderSegment& providerSegment) override
        {
            return providerSegmentFn ? providerSegmentFn(providerSegment) : Visitor::visitEnter(providerSegment);
        }
        bool visitEnter(Entity& entity) override
        {
            return entityFn ? entityFn(entity) : Visitor::visitEnter(entity);
        }
        bool visitEnter(EntitySnapshot& snapshot) override
        {
            return snapshotFn ? snapshotFn(snapshot) : Visitor::visitEnter(snapshot);
        }

        bool visit(EntityInstance& instance) override
        {
            return instanceFn ? instanceFn(instance) : Visitor::visit(instance);
        }



        // Const versions


        bool visitEnter(const Memory& memory) override
        {
            return memoryConstFn ? memoryConstFn(memory) : Visitor::visitEnter(memory);
        }
        bool visitEnter(const CoreSegment& coreSegment) override
        {
            return coreSegmentConstFn ? coreSegmentConstFn(coreSegment) : Visitor::visitEnter(coreSegment);
        }
        bool visitEnter(const ProviderSegment& providerSegment) override
        {
            return providerSegmentConstFn ? providerSegmentConstFn(providerSegment) : Visitor::visitEnter(providerSegment);
        }
        bool visitEnter(const Entity& entity) override
        {
            return entityConstFn ? entityConstFn(entity) : Visitor::visitEnter(entity);
        }
        bool visitEnter(const EntitySnapshot& snapshot) override
        {
            return snapshotConstFn ? snapshotConstFn(snapshot) : Visitor::visitEnter(snapshot);
        }

        bool visit(const EntityInstance& instance) override
        {
            return instanceConstFn ? instanceConstFn(instance) : Visitor::visit(instance);
        }


        std::function<bool(Memory& memory)> memoryFn;
        std::function<bool(const Memory& memory)> memoryConstFn;

        std::function<bool(CoreSegment& coreSegment)> coreSegmentFn;
        std::function<bool(const CoreSegment& coreSegment)> coreSegmentConstFn;

        std::function<bool(ProviderSegment& providerSegment)> providerSegmentFn;
        std::function<bool(const ProviderSegment& providerSegment)> providerSegmentConstFn;

        std::function<bool(Entity& entity)> entityFn;
        std::function<bool(const Entity& entity)> entityConstFn;

        std::function<bool(EntitySnapshot& snapshot)> snapshotFn;
        std::function<bool(const EntitySnapshot& snapshot)> snapshotConstFn;

        std::function<bool(EntityInstance& instance)> instanceFn;
        std::function<bool(const EntityInstance& instance)> instanceConstFn;

    };

}
