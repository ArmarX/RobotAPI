#include "aron_conversions.h"

#include <RobotAPI/libraries/aron/core/data/variant/All.h>


namespace armarx::armem
{
    constexpr const char* DATA_WRAPPER_TIME_STORED_FIELD     = "__WRITER_METADATA__TIME_STORED";
    constexpr const char* DATA_WRAPPER_TIME_CREATED_FIELD    = "__ENTITY_METADATA__TIME_CREATED";
    constexpr const char* DATA_WRAPPER_TIME_SENT_FIELD       = "__ENTITY_METADATA__TIME_SENT";
    constexpr const char* DATA_WRAPPER_TIME_ARRIVED_FIELD    = "__ENTITY_METADATA__TIME_ARRIVED";
    constexpr const char* DATA_WRAPPER_CONFIDENCE_FIELD      = "__ENTITY_METADATA__CONFIDENCE";
}


void armarx::armem::from_aron(const aron::data::DictPtr& metadata, const aron::data::DictPtr& data, wm::EntityInstance& e)
{
    ARMARX_CHECK_NOT_NULL(metadata);
    // Todo: What if data is null?

    wm::EntityInstanceMetadata& m = e.metadata();

    e.data() = data;

    auto timeCreated = aron::data::Long::DynamicCastAndCheck(metadata->getElement(DATA_WRAPPER_TIME_CREATED_FIELD));
    m.timeCreated = Time(Duration::MicroSeconds(timeCreated->toAronLongDTO()->value));

    auto timeSent = aron::data::Long::DynamicCastAndCheck(metadata->getElement(DATA_WRAPPER_TIME_SENT_FIELD));
    m.timeSent = Time(Duration::MicroSeconds(timeSent->toAronLongDTO()->value));

    auto timeArrived = aron::data::Long::DynamicCastAndCheck(metadata->getElement(DATA_WRAPPER_TIME_ARRIVED_FIELD));
    m.timeArrived = Time(Duration::MicroSeconds(timeArrived->toAronLongDTO()->value));

    auto confidence = aron::data::Double::DynamicCastAndCheck(metadata->getElement(DATA_WRAPPER_CONFIDENCE_FIELD));
    m.confidence = static_cast<float>(confidence->toAronDoubleDTO()->value);
}


void armarx::armem::to_aron(aron::data::DictPtr& metadata, aron::data::DictPtr& data, const wm::EntityInstance& e)
{
    data = e.data();
    metadata = std::make_shared<aron::data::Dict>();

    auto timeWrapped = std::make_shared<aron::data::Long>();
    timeWrapped->setValue(Time::Now().toMicroSecondsSinceEpoch());
    metadata->addElement(DATA_WRAPPER_TIME_STORED_FIELD, timeWrapped);

    const wm::EntityInstanceMetadata& m = e.metadata();
    auto timeCreated = std::make_shared<aron::data::Long>();
    timeCreated->setValue(m.timeCreated.toMicroSecondsSinceEpoch());
    metadata->addElement(DATA_WRAPPER_TIME_CREATED_FIELD, timeCreated);

    auto timeSent = std::make_shared<aron::data::Long>();
    timeSent->setValue(m.timeSent.toMicroSecondsSinceEpoch());
    metadata->addElement(DATA_WRAPPER_TIME_SENT_FIELD, timeSent);

    auto timeArrived = std::make_shared<aron::data::Long>();
    timeArrived->setValue(m.timeArrived.toMicroSecondsSinceEpoch());
    metadata->addElement(DATA_WRAPPER_TIME_ARRIVED_FIELD, timeArrived);

    auto confidence = std::make_shared<aron::data::Double>();
    confidence->setValue(static_cast<double>(m.confidence));
    metadata->addElement(DATA_WRAPPER_CONFIDENCE_FIELD, confidence);
}
