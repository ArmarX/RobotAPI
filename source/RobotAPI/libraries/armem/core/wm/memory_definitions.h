#pragma once

#include "detail/data_lookup_mixins.h"

#include <RobotAPI/libraries/armem/core/base/EntityInstanceBase.h>
#include <RobotAPI/libraries/armem/core/base/EntitySnapshotBase.h>
#include <RobotAPI/libraries/armem/core/base/EntityBase.h>
#include <RobotAPI/libraries/armem/core/base/ProviderSegmentBase.h>
#include <RobotAPI/libraries/armem/core/base/CoreSegmentBase.h>
#include <RobotAPI/libraries/armem/core/base/MemoryBase.h>

#include <RobotAPI/libraries/aron/core/data/variant/forward_declarations.h>


namespace armarx::armem::wm
{
    using EntityInstanceMetadata = base::EntityInstanceMetadata;
    using EntityInstanceData = armarx::aron::data::Dict;
    using EntityInstanceDataPtr = armarx::aron::data::DictPtr;


    /// @see base::EntityInstanceBase
    class EntityInstance :
        public base::EntityInstanceBase<EntityInstanceDataPtr, EntityInstanceMetadata>
    {
        using Base = base::EntityInstanceBase<EntityInstanceDataPtr, EntityInstanceMetadata>;

    public:

        using Base::EntityInstanceBase;


        /**
         * @brief Fill `*this` with the update's values.
         * @param update The update.
         * @param index The instances index.
         */
        void update(const EntityUpdate& update);

        bool equalsDeep(const EntityInstance& other) const;

    };



    /// @see base::EntitySnapshotBase
    class EntitySnapshot :
        public base::EntitySnapshotBase<EntityInstance, EntitySnapshot>
        , public detail::FindInstanceDataMixinForSnapshot<EntitySnapshot>
    {
    public:

        using base::EntitySnapshotBase<EntityInstance, EntitySnapshot>::EntitySnapshotBase;

    };


    /// @see base::EntityBase
    class Entity :
        public base::EntityBase<EntitySnapshot, Entity>
        , public detail::FindInstanceDataMixinForEntity<Entity>
    {
    public:

        using base::EntityBase<EntitySnapshot, Entity>::EntityBase;

    };



    /// @see base::ProviderSegmentBase
    class ProviderSegment :
        public base::ProviderSegmentBase<Entity, ProviderSegment>
        , public detail::FindInstanceDataMixin<ProviderSegment>
    {
        using Base = base::ProviderSegmentBase<Entity, ProviderSegment>;

    public:

        using Base::ProviderSegmentBase;

    };



    /// @see base::CoreSegmentBase
    class CoreSegment :
        public base::CoreSegmentBase<ProviderSegment, CoreSegment>
        , public detail::FindInstanceDataMixin<CoreSegment>
    {
        using Base = base::CoreSegmentBase<ProviderSegment, CoreSegment>;

    public:

        using Base::CoreSegmentBase;

    };



    /// @see base::MemoryBase
    class Memory :
        public base::MemoryBase<CoreSegment, Memory>
        , public detail::FindInstanceDataMixin<Memory>
    {
        using Base = base::MemoryBase<CoreSegment, Memory>;

    public:

        using Base::MemoryBase;

    };

}

