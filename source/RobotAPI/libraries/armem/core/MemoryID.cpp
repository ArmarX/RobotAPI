#include "MemoryID.h"

#include "error/ArMemError.h"

#include <SimoxUtility/algorithm/advanced.h>
#include <SimoxUtility/algorithm/string/string_tools.h>

#include <boost/algorithm/string.hpp>

#include <forward_list>


namespace armarx::armem
{

    // This is constant, not a variable.
    const std::string MemoryID::delimiter = "/";


    MemoryID::MemoryID()
    {
    }

    MemoryID::MemoryID(const std::string& string)
    {
        std::forward_list<std::string> items;
        boost::split(items, string, boost::is_any_of(delimiter));

        // Handle escaped /'s
        for (auto it = items.begin(); it != items.end(); ++it)
        {
            while (it->size() > 0 && it->back() == '\\')
            {
                // The / causing the split was escaped. Merge the items together.
                auto next = simox::alg::advanced(it, 1);
                if (next != items.end())
                {
                    // "it\" + "next" => "it/next"
                    *it = it->substr(0, it->size() - 1) + delimiter + *next;
                    items.erase_after(it); // Invalidates `next`, but not `it`.
                }
            }
        }

        auto it = items.begin();
        if (it == items.end())
        {
            return;
        }
        this->memoryName = *it;

        if (++it == items.end())
        {
            return;
        }
        this->coreSegmentName = *it;

        if (++it == items.end())
        {
            return;
        }
        this->providerSegmentName = *it;

        if (++it == items.end())
        {
            return;
        }
        this->entityName = *it;

        if (++it == items.end())
        {
            return;
        }
        this->timestamp = timestampFromStr(*it);

        if (++it == items.end())
        {
            return;
        }
        this->instanceIndex = instanceIndexFromStr(*it);
    }


    MemoryID::MemoryID(
        const std::string& memoryName,
        const std::string& coreSegmentName,
        const std::string& providerSegmentName,
        const std::string& entityName,
        Time timestamp,
        int instanceIndex) :
        memoryName(memoryName),
        coreSegmentName(coreSegmentName),
        providerSegmentName(providerSegmentName),
        entityName(entityName),
        timestamp(timestamp),
        instanceIndex(instanceIndex)
    {
    }


    std::string MemoryID::str(bool escapeDelimiters) const
    {
        return str(delimiter, escapeDelimiters);
    }

    std::string MemoryID::str(const std::string& delimiter, bool escapeDelimiter) const
    {
        std::vector<std::string> items = getAllItems(escapeDelimiter);
        while (items.size() > 0 && items.back().empty())
        {
            items.pop_back();
        }
        return simox::alg::join(items, delimiter, false, false);
    }

    std::string MemoryID::getLeafItem() const
    {
        std::vector<std::string> items = getAllItems();
        for (auto it = items.rbegin(); it != items.rend(); ++it)
        {
            if (!it->empty())
            {
                return *it;
            }
        }
        return "";
    }

    bool MemoryID::hasGap() const
    {
        bool emptyFound = false;
        for (const std::string& item : getAllItems())
        {
            if (item.empty())
            {
                emptyFound = true;
            }
            else if (emptyFound)
            {
                // Found a non-empty item after an empty item.
                return true;
            }
        }
        return false;
    }

    bool MemoryID::isWellDefined() const
    {
        return !hasGap();
    }


    MemoryID MemoryID::fromString(const std::string& string)
    {
        return MemoryID(string);
    }

    std::vector<std::string> MemoryID::getItems(bool escapeDelimiters) const
    {
        std::vector<std::string> items;

        items.push_back(escape(memoryName, escapeDelimiters));

        if (!hasCoreSegmentName())
        {
            return items;
        }
        items.push_back(escape(coreSegmentName, escapeDelimiters));

        if (!hasProviderSegmentName())
        {
            return items;
        }
        items.push_back(escape(providerSegmentName, escapeDelimiters));

        if (!hasEntityName())
        {
            return items;
        }
        items.push_back(escape(entityName, escapeDelimiters));

        if (!hasTimestamp())
        {
            return items;
        }
        items.push_back(timestampStr());

        if (!hasInstanceIndex())
        {
            return items;
        }
        items.push_back(instanceIndexStr());

        return items;
    }

    std::vector<std::string> MemoryID::getAllItems(bool escapeDelimiters) const
    {
        return
        {
            escape(memoryName, escapeDelimiters), escape(coreSegmentName, escapeDelimiters),
            escape(providerSegmentName, escapeDelimiters), escape(entityName, escapeDelimiters),
            timestampStr(), instanceIndexStr()
        };
    }

    MemoryID MemoryID::getMemoryID() const
    {
        MemoryID id;
        id.memoryName = memoryName;
        return id;
    }

    MemoryID MemoryID::getCoreSegmentID() const
    {
        MemoryID id = getMemoryID();
        id.coreSegmentName = coreSegmentName;
        return id;
    }

    MemoryID MemoryID::getProviderSegmentID() const
    {
        MemoryID id = getCoreSegmentID();
        id.providerSegmentName = providerSegmentName;
        return id;
    }

    MemoryID MemoryID::getEntityID() const
    {
        MemoryID id = getProviderSegmentID();
        id.entityName = entityName;
        return id;
    }

    MemoryID MemoryID::getEntitySnapshotID() const
    {
        MemoryID id = getEntityID();
        id.timestamp = timestamp;
        return id;
    }

    MemoryID MemoryID::getEntityInstanceID() const
    {
        MemoryID id = getEntitySnapshotID();
        id.instanceIndex = instanceIndex;
        return id;
    }

    MemoryID MemoryID::removeLeafItem() const
    {
        if (instanceIndex != -1)
        {
            return getEntitySnapshotID();
        }
        if (timestamp.isValid())
        {
            return getEntityID();
        }
        if (!entityName.empty())
        {
            return getProviderSegmentID();
        }
        if (!providerSegmentName.empty())
        {
            return getCoreSegmentID();
        }
        if (!coreSegmentName.empty())
        {
            return getMemoryID();
        }
        return {};  // return empty if already empty. Client needs to check (Avoids using optional as additional include)
    }

    void MemoryID::setMemoryID(const MemoryID& id)
    {
        memoryName = id.memoryName;
    }

    void MemoryID::setCoreSegmentID(const MemoryID& id)
    {
        setMemoryID(id);
        coreSegmentName = id.coreSegmentName;
    }

    void MemoryID::setProviderSegmentID(const MemoryID& id)
    {
        setCoreSegmentID(id);
        providerSegmentName = id.providerSegmentName;
    }

    void MemoryID::setEntityID(const MemoryID& id)
    {
        setProviderSegmentID(id);
        entityName = id.entityName;
    }

    void MemoryID::setEntitySnapshotID(const MemoryID& id)
    {
        setEntityID(id);
        timestamp = id.timestamp;
    }

    void MemoryID::setEntityInstanceID(const MemoryID& id)
    {
        setEntitySnapshotID(id);
        instanceIndex = id.instanceIndex;
    }

    MemoryID MemoryID::withMemoryName(const std::string& name) const
    {
        MemoryID id = *this;
        id.memoryName = name;
        return id;
    }

    MemoryID MemoryID::withCoreSegmentName(const std::string& name) const
    {
        MemoryID id = *this;
        id.coreSegmentName = name;
        return id;
    }

    MemoryID MemoryID::withProviderSegmentName(const std::string& name) const
    {
        MemoryID id = *this;
        id.providerSegmentName = name;
        return id;
    }

    MemoryID MemoryID::withEntityName(const std::string& name) const
    {
        MemoryID id = *this;
        id.entityName = name;
        return id;
    }

    MemoryID MemoryID::withTimestamp(Time time) const
    {
        MemoryID id = *this;
        id.timestamp = time;
        return id;
    }

    MemoryID MemoryID::withInstanceIndex(int index) const
    {
        MemoryID id = *this;
        id.instanceIndex = index;
        return id;
    }


    std::string MemoryID::timestampStr() const
    {
        return hasTimestamp() ? std::to_string(timestamp.toMicroSecondsSinceEpoch()) : "";
    }

    std::string MemoryID::instanceIndexStr() const
    {
        return hasInstanceIndex() ? std::to_string(instanceIndex) : "";
    }

    Time MemoryID::timestampFromStr(const std::string& string)
    {
        return Time(Duration::MicroSeconds(parseInteger(string, "timestamp")));
    }

    int MemoryID::instanceIndexFromStr(const std::string& string)
    {
        return int(parseInteger(string, "instance index"));
    }

    bool MemoryID::operator==(const MemoryID& other) const
    {
        return memoryName == other.memoryName
               and coreSegmentName == other.coreSegmentName
               and providerSegmentName == other.providerSegmentName
               and entityName == other.entityName
               and timestamp == other.timestamp
               and instanceIndex == other.instanceIndex;
    }

    bool MemoryID::operator<(const MemoryID& rhs) const
    {
        int c = memoryName.compare(rhs.memoryName);
        if (c != 0)
        {
            return c < 0;
        }
        // Equal memory name
        c = coreSegmentName.compare(rhs.coreSegmentName);
        if (c != 0)
        {
            return c < 0;
        }
        // Equal core segment ID
        c = providerSegmentName.compare(rhs.providerSegmentName);
        if (c != 0)
        {
            return c < 0;
        }
        // Equal provider segment ID
        c = entityName.compare(rhs.entityName);
        if (c != 0)
        {
            return c < 0;
        }
        // Equal entity ID
        if (timestamp != rhs.timestamp)
        {
            return timestamp < rhs.timestamp;
        }
        // Equal entity snapshot ID
        return instanceIndex < rhs.instanceIndex;
    }

    long long MemoryID::parseInteger(const std::string& string, const std::string& semanticName)
    {
        if (string.empty())
        {
            return std::numeric_limits<long>::min();
        }
        try
        {
            return std::stoll(string);
        }
        catch (const std::invalid_argument&)
        {
            throw error::ParseIntegerError(string, semanticName);
        }
        catch (const std::out_of_range&)
        {
            throw error::ParseIntegerError(string, semanticName);
        }
    }

    std::string MemoryID::escapeDelimiter(const std::string& name)
    {
        return simox::alg::replace_all(name, delimiter, "\\" + delimiter);
    }

    std::string MemoryID::escape(const std::string& name, bool escapeDelimiters)
    {
        if (escapeDelimiters)
        {
            return escapeDelimiter(name);
        }
        else
        {
            return name;
        }
    }

    std::ostream& operator<<(std::ostream& os, const MemoryID id)
    {
        return os << "'" << id.str() << "'";
    }


    bool contains(const MemoryID& general, const MemoryID& specific)
    {
        if (!general.isWellDefined())
        {
            std::stringstream ss;
            ss << "\nGeneral ID " << general << " is not well-defined, which is required for `" << __FUNCTION__ << "()`.";
            throw error::InvalidMemoryID(general, ss.str());
        }
        if (!specific.isWellDefined())
        {
            std::stringstream ss;
            ss << "\nSpecific ID " << specific << " is not well-defined, which is required for `" << __FUNCTION__ << "()`.";
            throw error::InvalidMemoryID(specific, ss.str());
        }

        if (general.memoryName.empty())
        {
            return true;
        }
        else if (general.memoryName != specific.memoryName)
        {
            return false;
        }

        if (general.coreSegmentName.empty())
        {
            return true;
        }
        else if (general.coreSegmentName != specific.coreSegmentName)
        {
            return false;
        }

        if (general.providerSegmentName.empty())
        {
            return true;
        }
        else if (general.providerSegmentName != specific.providerSegmentName)
        {
            return false;
        }

        if (general.entityName.empty())
        {
            return true;
        }
        else if (general.entityName != specific.entityName)
        {
            return false;
        }

        if (general.timestamp.isInvalid())
        {
            return true;
        }
        else if (general.timestamp != specific.timestamp)
        {
            return false;
        }

        if (general.instanceIndex < 0)
        {
            return true;
        }
        else if (general.instanceIndex != specific.instanceIndex)
        {
            return false;
        }

        return true;
    }

}
