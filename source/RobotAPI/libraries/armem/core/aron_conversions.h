#pragma once

#include <ostream>

#include "forward_declarations.h"


namespace armarx::armem
{
    // Implemented in <RobotAPI/libraries/aron/common/aron_conversions/armarx.h>
    // void fromAron(const IceUtil::Time& dto, Time& bo);
    // void toAron(IceUtil::Time& dto, const Time& bo);

    void fromAron(const arondto::MemoryID& dto, MemoryID& bo);
    void toAron(arondto::MemoryID& dto, const MemoryID& bo);
}
namespace armarx::armem::arondto
{
    std::ostream& operator<<(std::ostream& os, const MemoryID& rhs);
}
