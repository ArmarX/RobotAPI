#pragma once

#include <string>

#include <ArmarXCore/core/time/DateTime.h>
#include <ArmarXCore/core/time/Duration.h>

#include "forward_declarations.h"


namespace armarx::armem
{

    /**
     * @brief Returns `time` as e.g. "123456789.012 ms".
     * @param decimals How many sub-millisecond decimals to include.
     */
    std::string toStringMilliSeconds(const Time& time, int decimals = 3);

    /**
     * @brief Returns `time` as e.g. "123456789012 `mu`s".
     * The output string contains the actual greek letter `mu`.
     */
    std::string toStringMicroSeconds(const Time& time);

    /**
     * @brief Returns `time`as e.g. "2020-11-16 17:01:54.123456".
     * @param decimals How many sub-second decimals to include.
     */
    std::string toDateTimeMilliSeconds(const Time& time, int decimals = 6);


    /**
     * @brief Get a `Time` from the microseconds as text.
     */
    Time timeFromStringMicroSeconds(const std::string& microSeconds);

}  // namespace armarx::armem
