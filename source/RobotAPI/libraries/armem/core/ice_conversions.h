#pragma once

#include "forward_declarations.h"


namespace armarx::armem
{

    void fromIce(const data::MemoryID& ice, MemoryID& id);
    void toIce(data::MemoryID& ice, const MemoryID& id);


    void fromIce(const data::Commit& ice, Commit& commit);
    void toIce(data::Commit& ice, const Commit& commit);

    void fromIce(const data::CommitResult& ice, CommitResult& result);
    void toIce(data::CommitResult& ice, const CommitResult& result);


    void fromIce(const data::EntityUpdate& ice, EntityUpdate& update);
    void toIce(data::EntityUpdate& ice, const EntityUpdate& update);

    void fromIce(const data::EntityUpdateResult& ice, EntityUpdateResult& result);
    void toIce(data::EntityUpdateResult& ice, const EntityUpdateResult& result);


    // Server-side conversion with specified arrival time.
    void fromIce(const data::Commit& ice, Commit& commit, Time timeArrived);
    void fromIce(const data::EntityUpdate& ice, EntityUpdate& update, Time timeArrived);

    void fromIce(const actions::data::MenuPtr& ice, actions::Menu& menu);
    void toIce(actions::data::MenuPtr& ice, const actions::Menu& menu);

}

