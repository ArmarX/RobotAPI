#include "util.h"

#include <RobotAPI/libraries/armem/client.h>
#include <RobotAPI/libraries/armem/core/error/mns.h>

namespace armarx::armem
{

    std::optional<armarx::armem::wm::Memory>
    resolveID(armarx::armem::client::MemoryNameSystem& mns, const armarx::armem::MemoryID& memoryID)
    {
        armarx::armem::client::Reader reader;
        try
        {
            reader = mns.getReader(memoryID);
        }
        catch (const armarx::armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_WARNING << armarx::armem::error::CouldNotResolveMemoryServer::makeMsg(memoryID);
            return {};
        }

        const armarx::armem::ClientQueryResult result = reader.queryMemoryIDs({memoryID});
        if (result.success)
        {
            return result.memory;
        }
        ARMARX_WARNING << "Could not query memory for ID " << memoryID << ", "
                       << result.errorMessage;
        return {};
    }


    std::optional<std::pair<armarx::aron::data::DictPtr, armarx::aron::type::ObjectPtr>>
    extractDataAndType(const armarx::armem::wm::Memory& memory,
                       const armarx::armem::MemoryID& memoryID)
    {
        const auto* instance = memory.findLatestInstance(memoryID);
        if (instance == nullptr)
        {
            return {};
        }

        armarx::aron::data::DictPtr aronData = instance->data();
        armarx::aron::type::ObjectPtr aronType;
        const auto* providerSegment = memory.findProviderSegment(memoryID);
        if (providerSegment == nullptr)
        {
            return {};
        }

        if (!providerSegment->hasAronType())
        {
            const auto* coreSegment = memory.findCoreSegment(memoryID);
            if (coreSegment == nullptr || !coreSegment->hasAronType())
            {
                return {};
            }
            aronType = coreSegment->aronType();
        }
        else
        {
            aronType = providerSegment->aronType();
        }

        return {{aronData, aronType}};
    }
} // namespace armarx::armem
