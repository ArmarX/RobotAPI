#pragma once

#include "client/MemoryNameSystem.h"
#include "client/plugins.h"
#include "client/Query.h"
#include "client/query/Builder.h"
#include "client/query/query_fns.h"
#include "client/Reader.h"
#include "client/Writer.h"


namespace armarx::armem
{

    using ClientReader = client::Reader;
    using ClientWriter = client::Writer;

    using ClientQueryInput = client::QueryInput;
    using ClientQueryResult = client::QueryResult;
    using ClientQueryBuilder = client::query::Builder;
    namespace client_query_fns = client::query_fns;

}
