#include "SpecializedCoreSegment.h"

#include <ArmarXCore/core/application/properties/PluginAll.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>


namespace armarx::armem::server::segment
{

    SpecializedCoreSegment::SpecializedCoreSegment(
            armem::server::MemoryToIceAdapter& iceMemory,
            const std::string& defaultCoreSegmentName,
            aron::type::ObjectPtr coreSegmentAronType,
            int defaultMaxHistorySize,
            const std::vector<PredictionEngine>& predictionEngines) :
        Base(iceMemory),
        aronType(coreSegmentAronType),
        predictionEngines(predictionEngines),
        properties({defaultCoreSegmentName, defaultMaxHistorySize})
    {
        Logging::setTag("armarx::armem::SpecializedCoreSegment");
    }


    SpecializedCoreSegment::~SpecializedCoreSegment()
    {
    }


    void SpecializedCoreSegment::defineProperties(
        armarx::PropertyDefinitionsPtr defs,
        const std::string& prefix)
    {
        ARMARX_CHECK_NOT_NULL(defs);

        defs->optional(properties.segmentName,
                       prefix + "seg.CoreSegmentName",
                       "Name of the " + properties.segmentName + " core segment.");

        defs->optional(properties.maxHistorySize,
                       prefix + "seg.CoreMaxHistorySize",
                       "Maximal size of the " + properties.segmentName + " entity histories (-1 for infinite).");
    }


    void SpecializedCoreSegment::init()
    {
        ARMARX_CHECK_NOT_NULL(iceMemory.workingMemory);
        Logging::setTag(properties.segmentName + " Core Segment");

        if (iceMemory.workingMemory->hasCoreSegment(properties.segmentName))
        {
            segmentPtr = &iceMemory.workingMemory->getCoreSegment(properties.segmentName);
        }
        else
        {
            ARMARX_INFO << "Adding core segment '" << properties.segmentName << "'";
            segmentPtr = &iceMemory.workingMemory->addCoreSegment(properties.segmentName);

            segmentPtr->aronType() = aronType;
            segmentPtr->setMaxHistorySize(properties.maxHistorySize);
            segmentPtr->setPredictionEngines(predictionEngines);
        }
    }

    void SpecializedCoreSegment::setDefaultCoreSegmentName(const std::string& coreSegmentName)
    {
        this->properties.segmentName = coreSegmentName;
    }


    void SpecializedCoreSegment::setDefaultMaxHistorySize(int64_t maxHistorySize)
    {
        this->properties.maxHistorySize = maxHistorySize;
    }


    void SpecializedCoreSegment::setAronType(aron::type::ObjectPtr aronType)
    {
        this->aronType = aronType;
    }

    void
    SpecializedCoreSegment::setPredictionEngines(
        const std::vector<PredictionEngine>& predictionEngines)
    {
        this->predictionEngines = predictionEngines;
    }

    wm::CoreSegment& SpecializedCoreSegment::getCoreSegment()
    {
        ARMARX_CHECK_NOT_NULL(segmentPtr);
        return *segmentPtr;
    }


    const wm::CoreSegment& SpecializedCoreSegment::getCoreSegment() const
    {
        ARMARX_CHECK_NOT_NULL(segmentPtr);
        return *segmentPtr;
    }
}
