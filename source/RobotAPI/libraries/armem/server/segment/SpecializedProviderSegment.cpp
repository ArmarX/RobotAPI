#include "SpecializedProviderSegment.h"

#include <ArmarXCore/core/application/properties/PluginAll.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>


namespace armarx::armem::server::segment
{
    SpecializedProviderSegment::SpecializedProviderSegment(
        armem::server::MemoryToIceAdapter& iceMemory,
        const std::string& defaultProviderSegmentName,
        const std::string& defaultCoreSegmentName,
        aron::type::ObjectPtr providerSegmentAronType,
        aron::type::ObjectPtr coreSegmentAronType,
        int defaultMaxHistorySize,
        const std::vector<PredictionEngine>& providerSegmentPredictionEngines,
        const std::vector<PredictionEngine>& coreSegmentPredictionEngines) :
        Base(iceMemory),
        aronType(providerSegmentAronType),
        predictionEngines(providerSegmentPredictionEngines),
        coreSegment(iceMemory,
                    defaultCoreSegmentName,
                    coreSegmentAronType,
                    -1,
                    providerSegmentPredictionEngines),
        properties({defaultProviderSegmentName, defaultMaxHistorySize})
    {
        Logging::setTag("armarx::armem::SpecializedProviderSegment");
    }


    SpecializedProviderSegment::~SpecializedProviderSegment()
    {
    }


    void SpecializedProviderSegment::defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix)
    {
        ARMARX_CHECK_NOT_NULL(defs);

        coreSegment.defineProperties(defs, prefix);

        defs->optional(properties.segmentName,
                       prefix + "seg.ProviderSegmentName",
                       "Name of the " + properties.segmentName + " provider segment.");

        defs->optional(properties.maxHistorySize,
                       prefix + "seg.ProviderMaxHistorySize",
                       "Maximal size of the " + properties.segmentName + " entity histories (-1 for infinite).");
    }


    void SpecializedProviderSegment::init()
    {
        ARMARX_CHECK_NOT_NULL(iceMemory.workingMemory);

        coreSegment.init();

        Logging::setTag(properties.segmentName + " Provider Segment");

        if (coreSegment.segmentPtr->hasProviderSegment(properties.segmentName))
        {
            segmentPtr = &coreSegment.segmentPtr->getProviderSegment(properties.segmentName);
        }
        else
        {
            ARMARX_INFO << "Adding provider segment '" << properties.segmentName << "'";
            segmentPtr = &coreSegment.segmentPtr->addProviderSegment(properties.segmentName);

            segmentPtr->aronType() = aronType;
            segmentPtr->setMaxHistorySize(properties.maxHistorySize);
            segmentPtr->setPredictionEngines(predictionEngines);
        }
    }

    void SpecializedProviderSegment::setDefaultProviderSegmentName(const std::string& providerSegmentName)
    {
        this->properties.segmentName = providerSegmentName;
    }


    void SpecializedProviderSegment::setDefaultMaxHistorySize(int64_t maxHistorySize)
    {
        this->properties.maxHistorySize = maxHistorySize;
    }


    void SpecializedProviderSegment::setAronType(aron::type::ObjectPtr aronType)
    {
        this->aronType = aronType;
    }

    void
    SpecializedProviderSegment::setPredictionEngines(
        const std::vector<PredictionEngine>& predictionEngines)
    {
        this->predictionEngines = predictionEngines;
    }

    wm::ProviderSegment& SpecializedProviderSegment::getProviderSegment()
    {
        ARMARX_CHECK_NOT_NULL(segmentPtr);
        return *segmentPtr;
    }


    const wm::ProviderSegment& SpecializedProviderSegment::getProviderSegment() const
    {
        ARMARX_CHECK_NOT_NULL(segmentPtr);
        return *segmentPtr;
    }

}
