#pragma once

#include <RobotAPI/libraries/armem/server/forward_declarations.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

#include <RobotAPI/libraries/aron/core/type/variant/forward_declarations.h>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/application/properties/forward_declarations.h>

#include <string>

#include "SpecializedCoreSegment.h"

namespace armarx::armem::server::segment
{
        /**
         * @brief A base class for provider segments
         */
        class SpecializedProviderSegment : public detail::SegmentBase<server::wm::ProviderSegment>
        {
            using Base = detail::SegmentBase<server::wm::ProviderSegment>;

        public:

            SpecializedProviderSegment(
                MemoryToIceAdapter& iceMemory,
                const std::string& defaultProviderSegmentName = "",
                const std::string& defaultCoreSegmentName = "",
                aron::type::ObjectPtr providerSegmentAronType = nullptr,
                aron::type::ObjectPtr coreSegmentAronType = nullptr,
                int defaultMaxHistorySize = -1,
                const std::vector<PredictionEngine>& providerSegmentPredictionEngines = {},
                const std::vector<PredictionEngine>& coreSegmentPredictionEngines = {});

            virtual ~SpecializedProviderSegment() override;

            virtual void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "") override;
            virtual void init() override;

            void setDefaultProviderSegmentName(const std::string& providerSegmentName);
            void setDefaultMaxHistorySize(int64_t maxHistorySize);
            void setAronType(aron::type::ObjectPtr aronType);
            void
            setPredictionEngines(const std::vector<PredictionEngine>& predictionEngines);

            wm::ProviderSegment& getProviderSegment();
            const wm::ProviderSegment& getProviderSegment() const;


        public:

            aron::type::ObjectPtr aronType;
            std::vector<PredictionEngine> predictionEngines;
            SpecializedCoreSegment coreSegment;

            struct Properties
            {
                std::string segmentName;
                int64_t maxHistorySize;
            };
            Properties properties;
        };
}
