#pragma once

#include <RobotAPI/libraries/armem/server/forward_declarations.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

#include <RobotAPI/libraries/aron/core/type/variant/forward_declarations.h>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/application/properties/forward_declarations.h>

#include <string>


namespace armarx::armem::server::segment
{
    namespace detail
    {
        /**
         * @brief A base class for memory servers to manage their segments.
         * A segment can inherit this base class and add segment specific code.
         * TODO (fabian.peller): add concept to only accept coresegments, providersegments or entitysegments
         */
        template <class SegmentType>
        class SegmentBase : public armarx::Logging
        {
        public:

            SegmentBase() = delete;
            SegmentBase(MemoryToIceAdapter& iceMemory) :
                iceMemory(iceMemory)
            {
                Logging::setTag("armarx::armem::Segment");
            }

            MemoryID& id()
            {
                ARMARX_CHECK_NOT_NULL(segmentPtr);
                return segmentPtr->id();
            }

            const MemoryID& id() const
            {
                ARMARX_CHECK_NOT_NULL(segmentPtr);
                return segmentPtr->id();
            }

            virtual ~SegmentBase() = default;

            virtual void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "") = 0;
            virtual void init() = 0;

        public:

            SegmentType* segmentPtr;


        protected:

            // Memory connection
            MemoryToIceAdapter& iceMemory;
        };
    }
}
