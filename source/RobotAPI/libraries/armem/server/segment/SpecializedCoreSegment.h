#pragma once

#include <RobotAPI/libraries/armem/server/forward_declarations.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

#include <RobotAPI/libraries/aron/core/type/variant/forward_declarations.h>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/application/properties/forward_declarations.h>

#include <string>

#include "detail/SpecializedSegment.h"

namespace armarx::armem::server::segment
{
        /**
         * @brief A base class for core segments
         */
        class SpecializedCoreSegment : public detail::SegmentBase<server::wm::CoreSegment>
        {
            using Base = detail::SegmentBase<server::wm::CoreSegment>;

        public:

            SpecializedCoreSegment(
                MemoryToIceAdapter& iceMemory,
                const std::string& defaultCoreSegmentName = "",
                aron::type::ObjectPtr coreSegmentAronType = nullptr,
                int defaultMaxHistorySize = -1,
                const std::vector<PredictionEngine>& predictionEngines = {});

            virtual ~SpecializedCoreSegment() override;

            virtual void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "") override;
            virtual void init() override;

            template <class FunctionT>
            auto doLocked(FunctionT&& function) const
            {
                return segmentPtr->doLocked(function);
            }

            void setDefaultCoreSegmentName(const std::string& coreSegmentName);
            void setDefaultMaxHistorySize(int64_t maxHistorySize);
            void setAronType(aron::type::ObjectPtr aronType);
            void
            setPredictionEngines(const std::vector<PredictionEngine>& predictionEngines);

            wm::CoreSegment& getCoreSegment();
            const wm::CoreSegment& getCoreSegment() const;


        public:

            aron::type::ObjectPtr aronType;
            std::vector<PredictionEngine> predictionEngines;

            struct Properties
            {
                std::string segmentName;
                int64_t maxHistorySize;
            };
            Properties properties;

        };
}
