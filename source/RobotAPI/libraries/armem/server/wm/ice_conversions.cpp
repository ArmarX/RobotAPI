#include "ice_conversions.h"

#include <RobotAPI/libraries/armem/core/base/ice_conversions.h>


namespace armarx::armem::server
{

    void wm::toIce(data::EntityInstance& ice, const EntityInstance& data)
    {
        base::toIce(ice, data);
    }
    void wm::fromIce(const data::EntityInstance& ice, EntityInstance& data)
    {
        base::fromIce(ice, data);
    }


    void wm::toIce(data::EntitySnapshot& ice, const EntitySnapshot& snapshot)
    {
        base::toIce(ice, snapshot);
    }
    void wm::fromIce(const data::EntitySnapshot& ice, EntitySnapshot& snapshot)
    {
        base::fromIce(ice, snapshot);
    }

    void wm::toIce(data::Entity& ice, const Entity& entity)
    {
        base::toIce(ice, entity);
    }
    void wm::fromIce(const data::Entity& ice, Entity& entity)
    {
        base::fromIce(ice, entity);
    }

    void wm::toIce(data::ProviderSegment& ice, const ProviderSegment& providerSegment)
    {
        base::toIce(ice, providerSegment);
    }
    void wm::fromIce(const data::ProviderSegment& ice, ProviderSegment& providerSegment)
    {
        base::fromIce(ice, providerSegment);
    }

    void wm::toIce(data::CoreSegment& ice, const CoreSegment& coreSegment)
    {
        base::toIce(ice, coreSegment);
    }
    void wm::fromIce(const data::CoreSegment& ice, CoreSegment& coreSegment)
    {
        base::fromIce(ice, coreSegment);
    }

    void wm::toIce(data::Memory& ice, const Memory& memory)
    {
        base::toIce(ice, memory);
    }
    void wm::fromIce(const data::Memory& ice, Memory& memory)
    {
        base::fromIce(ice, memory);
    }

}
