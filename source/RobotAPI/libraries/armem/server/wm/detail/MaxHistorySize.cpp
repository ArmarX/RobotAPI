#include "MaxHistorySize.h"


namespace armarx::armem::server::wm::detail
{
    void MaxHistorySize::setMaxHistorySize(long maxSize)
    {
        this->_maxHistorySize = maxSize;
    }

    long MaxHistorySize::getMaxHistorySize() const
    {
        return _maxHistorySize;
    }
}
