#pragma once

namespace armarx::armem::server::wm::detail
{
    // TODO: Replace by ConstrainedHistorySize (not only max entries, e.g. delete oldest / delete least accessed / ...)

    class MaxHistorySize
    {
    public:

        /**
         * @brief Set the maximum number of snapshots to be contained in an entity.
         * Affected entities are to be update right away.
         */
        void setMaxHistorySize(long maxSize);

        long getMaxHistorySize() const;


    protected:

        /**
         * @brief Maximum size of entity histories.
         *
         * If negative, the size of `history` is not limited.
         *
         * @see Entity::maxHstorySize
         */
        long _maxHistorySize = -1;

    };



    template <class DerivedT>
    class MaxHistorySizeParent : public MaxHistorySize
    {
    public:

        /**
         * @brief Sets the maximum history size of entities in this container.
         * This affects all current entities as well as new ones.
         *
         * @see MaxHistorySize::setMaxHistorySize()
         */
        void setMaxHistorySize(long maxSize)
        {
            MaxHistorySize::setMaxHistorySize(maxSize);
            static_cast<DerivedT&>(*this).forEachChild([maxSize](auto & child)
            {
                child.setMaxHistorySize(maxSize);
            });
        }

    };
}
