#pragma once

#include "detail/MaxHistorySize.h"
#include "detail/Prediction.h"

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/base/EntityInstanceBase.h>
#include <RobotAPI/libraries/armem/core/base/EntitySnapshotBase.h>
#include <RobotAPI/libraries/armem/core/base/EntityBase.h>
#include <RobotAPI/libraries/armem/core/base/ProviderSegmentBase.h>
#include <RobotAPI/libraries/armem/core/base/CoreSegmentBase.h>
#include <RobotAPI/libraries/armem/core/base/MemoryBase.h>

#include <RobotAPI/libraries/armem/core/wm/detail/data_lookup_mixins.h>

#include <mutex>


namespace armarx::armem::server::wm
{
    using EntityInstanceMetadata = base::EntityInstanceMetadata;
    using EntityInstanceData = armarx::aron::data::Dict;
    using EntityInstanceDataPtr = armarx::aron::data::DictPtr;

    using EntityInstance = armem::wm::EntityInstance;
    using EntitySnapshot = armem::wm::EntitySnapshot;


    /// @see base::EntityBase
    class Entity :
        public base::EntityBase<EntitySnapshot, Entity>
        , public detail::MaxHistorySize
        , public armem::wm::detail::FindInstanceDataMixinForEntity<Entity>
    {
    public:

        using base::EntityBase<EntitySnapshot, Entity>::EntityBase;


        /**
         * @brief Sets the maximum history size.
         *
         * The current history is truncated if necessary.
         */
        void setMaxHistorySize(long maxSize);

        UpdateResult update(const EntityUpdate& update);


    protected:

        /// If maximum size is set, ensure `history`'s is not higher.
        std::vector<EntitySnapshotT> truncate();

    };



    /// @see base::ProviderSegmentBase
    class ProviderSegment :
        public base::ProviderSegmentBase<Entity, ProviderSegment>
        , public detail::MaxHistorySizeParent<ProviderSegment>
        , public armem::wm::detail::FindInstanceDataMixin<ProviderSegment>
        , public armem::server::wm::detail::Prediction<ProviderSegment>
    {
    public:

        using base::ProviderSegmentBase<Entity, ProviderSegment>::ProviderSegmentBase;


        using ProviderSegmentBase::addEntity;

        template <class ...Args>
        Entity& addEntity(const std::string& name, Args... args)
        {
            Entity& added = ProviderSegmentBase::addEntity(name, args...);
            added.setMaxHistorySize(this->getMaxHistorySize());
            return added;
        }

    };



    /// @brief base::CoreSegmentBase
    class CoreSegment :
        public base::CoreSegmentBase<ProviderSegment, CoreSegment>
        , public detail::MaxHistorySizeParent<CoreSegment>
        , public armem::wm::detail::FindInstanceDataMixin<CoreSegment>
        , public armem::server::wm::detail::PredictionContainer<CoreSegment>
    {
        using Base = base::CoreSegmentBase<ProviderSegment, CoreSegment>;

    public:

        using Base::CoreSegmentBase;

        /// @see base::CoreSegmentBase::addProviderSegment()
        using CoreSegmentBase::addProviderSegment;
        template <class ...Args>
        ProviderSegment& addProviderSegment(const std::string& name, Args... args)
        {
            ProviderSegmentT& added = CoreSegmentBase::addProviderSegment(name, args...);
            added.setMaxHistorySize(this->getMaxHistorySize());
            return added;
        }


        // Locking interface

        template <class FunctionT>
        auto doLocked(FunctionT&& function) const
        {
            std::scoped_lock lock(_mutex);
            return function();
        }


    private:

        mutable std::mutex _mutex;

    };



    /// @see base::MemoryBase
    class Memory :
        public base::MemoryBase<CoreSegment, Memory>
        , public armem::wm::detail::FindInstanceDataMixin<Memory>
        , public armem::server::wm::detail::PredictionContainer<Memory>
    {
        using Base = base::MemoryBase<CoreSegment, Memory>;

    public:

        using Base::MemoryBase;


        /**
         * @brief Perform the commit, locking the core segments.
         *
         * Groups the commits by core segment, and updates each core segment
         * in a batch, locking the core segment.
         */
        std::vector<Base::UpdateResult> updateLocking(const Commit& commit);

        /**
         * @brief Update the memory, locking the updated core segment.
         */
        Base::UpdateResult updateLocking(const EntityUpdate& update);

    };

}

