
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore ${LIB_NAME})

armarx_add_test(ArMemLTMTest ArMemLTMTest.cpp "${LIBS}")
armarx_add_test(ArMemLTMBenchmark ArMemLTMBenchmark.cpp "${LIBS}")
armarx_add_test(ArMemMemoryTest ArMemMemoryTest.cpp "${LIBS}")
armarx_add_test(ArMemQueryTest ArMemQueryTest.cpp "${LIBS}")
