/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::armem

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/operations.h>

#include <iostream>
#include <SimoxUtility/meta/type_name.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>


namespace armem = armarx::armem;
namespace aron = armarx::aron;
namespace wm = armarx::armem::wm;


BOOST_AUTO_TEST_CASE(test_time_to_string)
{
    // 111111: seconds, 345: milliseconds, 789: microseconds
    armem::Time time { armem::Duration::MicroSeconds(111111345789) };

    BOOST_CHECK_EQUAL(armem::toStringMilliSeconds(time), "111111345.789 ms");
    BOOST_CHECK_EQUAL(armem::toStringMilliSeconds(time, 0), "111111345 ms");
    BOOST_CHECK_EQUAL(armem::toStringMilliSeconds(time, 1), "111111345.7 ms");
    BOOST_CHECK_EQUAL(armem::toStringMilliSeconds(time, 2), "111111345.78 ms");
    BOOST_CHECK_EQUAL(armem::toStringMilliSeconds(time, 3), "111111345.789 ms");

    BOOST_CHECK_EQUAL(armem::toStringMicroSeconds(time), "111111345789 " "\u03BC" "s");

    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time), "1970-01-02 07:51:51.345789");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time, 0), "1970-01-02 07:51:51");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time, 3), "1970-01-02 07:51:51.345");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time, 6), "1970-01-02 07:51:51.345789");

    // 111111: seconds, 000: milliseconds, 789: microseconds
    time = armem::Time(armem::Duration::MicroSeconds(111111000789));
    BOOST_CHECK_EQUAL(armem::toStringMilliSeconds(time), "111111000.789 ms");
    BOOST_CHECK_EQUAL(armem::toStringMicroSeconds(time), "111111000789 " "\u03BC" "s");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time), "1970-01-02 07:51:51.000789");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time, 0), "1970-01-02 07:51:51");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time, 3), "1970-01-02 07:51:51.000");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time, 6), "1970-01-02 07:51:51.000789");

    // 111111: seconds, 345: milliseconds, 000: microseconds
    time = armem::Time(armem::Duration::MicroSeconds(111111345000));
    BOOST_CHECK_EQUAL(armem::toStringMilliSeconds(time), "111111345.000 ms");
    BOOST_CHECK_EQUAL(armem::toStringMicroSeconds(time), "111111345000 " "\u03BC" "s");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time), "1970-01-02 07:51:51.345000");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time, 0), "1970-01-02 07:51:51");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time, 3), "1970-01-02 07:51:51.345");
    BOOST_CHECK_EQUAL(armem::toDateTimeMilliSeconds(time, 6), "1970-01-02 07:51:51.345000");
}


// Public interface tests


namespace ArMemMemoryTest
{
    struct APITestFixture
    {
        wm::EntityInstance instance { 0 };
        wm::EntitySnapshot snapshot { armem::Time(armem::Duration::MicroSeconds(1000)) };
        wm::Entity entity { "entity" };
        wm::ProviderSegment provSeg { "provider" };
        wm::CoreSegment coreSeg { "core" };
        wm::Memory memory { "memory" };


        void test_add_instance(wm::EntityInstance& added, const wm::EntitySnapshot& parent)
        {
            BOOST_TEST_CONTEXT("Added: " << armem::print(added) << "\n Parent: " << armem::print(parent))
            {
                const int index = 0;
                BOOST_CHECK_EQUAL(added.index(), index);
                BOOST_CHECK_EQUAL(parent.size(), 1);
                BOOST_CHECK(parent.hasInstance(index));
                BOOST_CHECK_EQUAL(&parent.getInstance(index), &added);
            }
        }
        void test_add_snapshot(wm::EntitySnapshot& added, const wm::Entity& parent)
        {
            BOOST_TEST_CONTEXT("Added: " << armem::print(added) << "\n Parent: " << armem::print(parent))
            {
                const armem::Time time = armem::Time(armem::Duration::MicroSeconds(1000));
                BOOST_CHECK_EQUAL(added.time(), time);
                BOOST_CHECK_EQUAL(parent.size(), 1);
                BOOST_CHECK(parent.hasSnapshot(time));
                BOOST_CHECK_EQUAL(&parent.getSnapshot(time), &added);
            }
        }
        void test_add_entity(wm::Entity& added, const wm::ProviderSegment& parent)
        {
            BOOST_TEST_CONTEXT("Added: " << armem::print(added) << "\n Parent: " << armem::print(parent))
            {
                const std::string name = "entity";
                BOOST_CHECK_EQUAL(added.name(), name);
                BOOST_CHECK_EQUAL(parent.size(), 1);
                BOOST_CHECK(parent.hasEntity(name));
                BOOST_CHECK_EQUAL(&parent.getEntity(name), &added);
            }
        }
        void test_add_provider_segment(wm::ProviderSegment& added, const wm::CoreSegment& parent)
        {
            BOOST_TEST_CONTEXT("Added: " << armem::print(added) << "\n Parent: " << armem::print(parent))
            {
                const std::string name = "provider";
                BOOST_CHECK_EQUAL(added.name(), name);
                BOOST_CHECK_EQUAL(parent.size(), 1);
                BOOST_CHECK(parent.hasProviderSegment(name));
                BOOST_CHECK_EQUAL(&parent.getProviderSegment(name), &added);
            }
        }
        void test_add_core_segment(wm::CoreSegment& added, const wm::Memory& parent)
        {
            BOOST_TEST_CONTEXT("Added: " << armem::print(added) << "\n Parent: " << armem::print(parent))
            {
                const std::string name = "core";
                BOOST_CHECK_EQUAL(added.name(), name);
                BOOST_CHECK_EQUAL(parent.size(), 1);
                BOOST_CHECK(parent.hasCoreSegment(name));
                BOOST_CHECK_EQUAL(&parent.getCoreSegment(name), &added);
            }
        }
    };
}



BOOST_FIXTURE_TEST_SUITE(APITest, ArMemMemoryTest::APITestFixture)


BOOST_AUTO_TEST_CASE(test_add_instance_no_args)
{
    test_add_instance(snapshot.addInstance(), snapshot);
}
BOOST_AUTO_TEST_CASE(test_add_instance_copy)
{
    test_add_instance(snapshot.addInstance(instance), snapshot);
}
BOOST_AUTO_TEST_CASE(test_add_instance_move)
{
    test_add_instance(snapshot.addInstance(std::move(instance)), snapshot);
}

BOOST_AUTO_TEST_CASE(test_add_snapshot_time)
{
    test_add_snapshot(entity.addSnapshot(armem::Time(armem::Duration::MicroSeconds(1000))), entity);
}
BOOST_AUTO_TEST_CASE(test_add_snapshot_copy)
{
    test_add_snapshot(entity.addSnapshot(snapshot), entity);
}
BOOST_AUTO_TEST_CASE(test_add_snapshot_move)
{
    test_add_snapshot(entity.addSnapshot(std::move(snapshot)), entity);
}

BOOST_AUTO_TEST_CASE(test_add_entity_name)
{
    test_add_entity(provSeg.addEntity("entity"), provSeg);
}
BOOST_AUTO_TEST_CASE(test_add_entity_copy)
{
    test_add_entity(provSeg.addEntity(entity), provSeg);
}
BOOST_AUTO_TEST_CASE(test_add_entity_move)
{
    test_add_entity(provSeg.addEntity(std::move(entity)), provSeg);
}

BOOST_AUTO_TEST_CASE(test_add_provider_segment_name)
{
    test_add_provider_segment(coreSeg.addProviderSegment("provider"), coreSeg);
}
BOOST_AUTO_TEST_CASE(test_add_provider_segment_copy)
{
    test_add_provider_segment(coreSeg.addProviderSegment(provSeg), coreSeg);
}
BOOST_AUTO_TEST_CASE(test_add_provider_segment_move)
{
    test_add_provider_segment(coreSeg.addProviderSegment(std::move(provSeg)), coreSeg);
}

BOOST_AUTO_TEST_CASE(test_add_core_segment_name)
{
    test_add_core_segment(memory.addCoreSegment("core"), memory);
}
BOOST_AUTO_TEST_CASE(test_add_core_segment_copy)
{
    test_add_core_segment(memory.addCoreSegment(coreSeg), memory);
}
BOOST_AUTO_TEST_CASE(test_add_core_segment_move)
{
    test_add_core_segment(memory.addCoreSegment(std::move(coreSeg)), memory);
}


BOOST_AUTO_TEST_SUITE_END()





// COPY/MOVE CTOR/OP TESTS


namespace ArMemMemoryTest
{
    struct TestMemoryItem : public armem::base::detail::MemoryItem
    {
        using MemoryItem::MemoryItem;
        using MemoryItem::operator=;

        std::string getKeyString() const
        {
            return "";
        }
        std::string getLevelName() const
        {
            return "";
        }
    };
    struct MemoryItemCtorOpTestFixture
    {
        const armem::MemoryID id {"A/B/C/123/1"};
        const armem::MemoryID moved {"////1"};  // int is not moved
        TestMemoryItem item { id };

        MemoryItemCtorOpTestFixture()
        {
            BOOST_CHECK_EQUAL(item.id(), id);
        }
    };
}


BOOST_FIXTURE_TEST_SUITE(MemoryItemTest, ArMemMemoryTest::MemoryItemCtorOpTestFixture)


BOOST_AUTO_TEST_CASE(test_copy_ctor)
{
    const ArMemMemoryTest::TestMemoryItem out(item);
    BOOST_CHECK_EQUAL(item.id(), id);
    BOOST_CHECK_EQUAL(out.id(), id);
}
BOOST_AUTO_TEST_CASE(test_copy_op)
{
    ArMemMemoryTest::TestMemoryItem out;
    out = item;
    BOOST_CHECK_EQUAL(item.id(), id);
    BOOST_CHECK_EQUAL(out.id(), id);
}
BOOST_AUTO_TEST_CASE(test_move_ctor)
{
    ArMemMemoryTest::TestMemoryItem in = item;
    const ArMemMemoryTest::TestMemoryItem out(std::move(in));
    BOOST_CHECK_EQUAL(in.id(), moved);
    BOOST_CHECK_EQUAL(out.id(), id);
}
BOOST_AUTO_TEST_CASE(test_move_op)
{
    ArMemMemoryTest::TestMemoryItem in = item;
    ArMemMemoryTest::TestMemoryItem out;
    out = std::move(in);
    BOOST_CHECK_EQUAL(in.id(), moved);
    BOOST_CHECK_EQUAL(out.id(), id);
}


BOOST_AUTO_TEST_SUITE_END()



namespace ArMemMemoryTest
{
    struct TestMemoryContainer : public armem::base::detail::MemoryContainerBase<std::vector<int>, TestMemoryContainer>
    {
        using MemoryContainerBase::MemoryContainerBase;
        using MemoryContainerBase::operator=;

        std::string getKeyString() const
        {
            return "";
        }
        std::string getLevelName() const
        {
            return "";
        }

        void fill()
        {
            _container = std::vector<int> { -1, 2, -3 };
        }
    };
    struct MemoryContainerCtorOpTestFixture
    {
        const armem::MemoryID id {"M/C/P/E/123/1"};
        const armem::MemoryID moved {"////123/1"};  // Time and int are just copied, not moved
        TestMemoryContainer cont {id};

        MemoryContainerCtorOpTestFixture()
        {
            cont.fill();
            BOOST_CHECK_EQUAL(cont.id(), id);
            BOOST_CHECK_EQUAL(cont.size(), 3);
        }
    };

}

BOOST_FIXTURE_TEST_SUITE(MemoryContainerTest, ArMemMemoryTest::MemoryContainerCtorOpTestFixture)

BOOST_AUTO_TEST_CASE(test_copy_ctor)
{
    const ArMemMemoryTest::TestMemoryContainer out(cont);
    BOOST_CHECK_EQUAL(cont.id(), id);
    BOOST_CHECK_EQUAL(cont.size(), 3);
    BOOST_CHECK_EQUAL(out.id(), id);
    BOOST_CHECK_EQUAL(out.size(), 3);
}
BOOST_AUTO_TEST_CASE(test_copy_op)
{
    ArMemMemoryTest::TestMemoryContainer out;
    out = cont;
    BOOST_CHECK_EQUAL(cont.id(), id);
    BOOST_CHECK_EQUAL(cont.size(), 3);
    BOOST_CHECK_EQUAL(out.id(), id);
    BOOST_CHECK_EQUAL(out.size(), 3);
}
BOOST_AUTO_TEST_CASE(test_move_ctor)
{
    ArMemMemoryTest::TestMemoryContainer in = cont;
    const ArMemMemoryTest::TestMemoryContainer out(std::move(in));
    BOOST_CHECK_EQUAL(in.id(), moved);
    BOOST_CHECK_EQUAL(in.size(), 0);
    BOOST_CHECK_EQUAL(out.id(), id);
    BOOST_CHECK_EQUAL(out.size(), 3);
}
BOOST_AUTO_TEST_CASE(test_move_op)
{
    ArMemMemoryTest::TestMemoryContainer in = cont;
    ArMemMemoryTest::TestMemoryContainer out;
    out = std::move(in);
    BOOST_CHECK_EQUAL(in.id(), moved);
    BOOST_CHECK_EQUAL(in.size(), 0);
    BOOST_CHECK_EQUAL(out.id(), id);
    BOOST_CHECK_EQUAL(out.size(), 3);
}

BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_CASE(test_key_ctors)
{
    wm::EntityInstance instance(10);
    BOOST_CHECK_EQUAL(instance.index(), 10);

    wm::EntitySnapshot snapshot(armem::Time(armem::Duration::MilliSeconds(100)));
    BOOST_CHECK_EQUAL(snapshot.time(), armem::Time(armem::Duration::MilliSeconds(100)));

    wm::Entity entity("entity");
    BOOST_CHECK_EQUAL(entity.name(), "entity");

    wm::ProviderSegment provSeg("provSeg");
    BOOST_CHECK_EQUAL(provSeg.name(), "provSeg");

    wm::CoreSegment coreSeg("coreSeg");
    BOOST_CHECK_EQUAL(coreSeg.name(), "coreSeg");

    wm::Memory memory("memory");
    BOOST_CHECK_EQUAL(memory.name(), "memory");
}



template <class T>
struct CustomChecks
{
    static void checkEqual(const T& lhs, const T& rhs)
    {
        (void) lhs, (void) rhs;
    }
    static void checkMoved(const T& moved)
    {
        (void) moved;
    }
};


template <class T>
void checkEqual_d_ltm(const T& lhs, const T& rhs)
{
    BOOST_CHECK_EQUAL(lhs.path, rhs.path);
}
template <class T>
void checkMoved_d_ltm(const T& moved)
{
    BOOST_CHECK_EQUAL(moved.path, nullptr);
}

template <>
struct CustomChecks<wm::EntityInstance>
{
    static void checkEqual(const wm::EntityInstance& lhs, const wm::EntityInstance& rhs)
    {
        BOOST_CHECK_EQUAL(lhs.metadata(), rhs.metadata());
        BOOST_CHECK_EQUAL(lhs.data(), rhs.data());
    }
    static void checkMoved(const wm::EntityInstance& moved)
    {
        BOOST_CHECK_EQUAL(moved.data(), nullptr);
    }
};
/*template <>
struct CustomChecks<armem::d_ltm::EntityInstance>
{
    static void checkEqual(const armem::d_ltm::EntityInstance& lhs, const armem::d_ltm::EntityInstance& rhs)
    {
        checkEqual_d_ltm(lhs, rhs);
    }
    static void checkMoved(const armem::d_ltm::EntityInstance& moved)
    {
        checkMoved_d_ltm(moved);
    }
};
template <>
struct CustomChecks<armem::d_ltm::EntitySnapshot>
{
    static void checkEqual(const armem::d_ltm::EntitySnapshot& lhs, const armem::d_ltm::EntitySnapshot& rhs)
    {
        checkEqual_d_ltm(lhs, rhs);
    }
    static void checkMoved(const armem::d_ltm::EntitySnapshot& moved)
    {
        checkMoved_d_ltm(moved);
    }
};
template <>
struct CustomChecks<armem::d_ltm::Entity>
{
    static void checkEqual(const armem::d_ltm::Entity& lhs, const armem::d_ltm::Entity& rhs)
    {
        checkEqual_d_ltm(lhs, rhs);
    }
    static void checkMoved(const armem::d_ltm::Entity& moved)
    {
        checkMoved_d_ltm(moved);
    }
};
template <>
struct CustomChecks<armem::d_ltm::ProviderSegment>
{
    static void checkEqual(const armem::d_ltm::ProviderSegment& lhs, const armem::d_ltm::ProviderSegment& rhs)
    {
        checkEqual_d_ltm(lhs, rhs);
    }
    static void checkMoved(const armem::d_ltm::ProviderSegment& moved)
    {
        checkMoved_d_ltm(moved);
    }
};
template <>
struct CustomChecks<armem::d_ltm::CoreSegment>
{
    static void checkEqual(const armem::d_ltm::CoreSegment& lhs, const armem::d_ltm::CoreSegment& rhs)
    {
        checkEqual_d_ltm(lhs, rhs);
    }
    static void checkMoved(const armem::d_ltm::CoreSegment& moved)
    {
        checkMoved_d_ltm(moved);
    }
};
template <>
struct CustomChecks<armem::d_ltm::Memory>
{
    static void checkEqual(const armem::d_ltm::Memory& lhs, const armem::d_ltm::Memory& rhs)
    {
        checkEqual_d_ltm(lhs, rhs);
    }
    static void checkMoved(const armem::d_ltm::Memory& moved)
    {
        checkMoved_d_ltm(moved);
    }
};*/


struct CopyMoveCtorsOpsTestBase
{
    const armem::MemoryID id {"M", "C", "P", "E", armem::Time(armem::Duration::MicroSeconds(123000)), 1};
    //const armem::MemoryID idMoved {"", "", "", "", armem::Time(armem::Duration::MicroSeconds(123000), 1};
    armem::MemoryID idMoved = id;

    std::string typeName;

    CopyMoveCtorsOpsTestBase(const std::string& typeName) :
        typeName(typeName)
    {
        armem::MemoryID copy = std::move(idMoved);
        (void) copy;
    }
    virtual ~CopyMoveCtorsOpsTestBase()
    {
    }


    void test()
    {
        BOOST_TEST_CONTEXT("Type " << typeName)
        {
            reset();
            BOOST_TEST_CONTEXT("copy ctor")
            {
                testCopyCtor();
            }
            reset();
            BOOST_TEST_CONTEXT("copy op")
            {
                testCopyOp();
            }
            reset();
            BOOST_TEST_CONTEXT("move ctor")
            {
                testMoveCtor();
            }
            reset();
            BOOST_TEST_CONTEXT("move op")
            {
                testMoveOp();
            }
        }
    }

    virtual void reset()
    {
    }

    virtual void testCopyCtor() = 0;
    virtual void testCopyOp() = 0;
    virtual void testMoveCtor() = 0;
    virtual void testMoveOp() = 0;
};



template <class T>
struct InstanceCopyMoveCtorsOpsTest : public CopyMoveCtorsOpsTestBase
{
    T in;

    InstanceCopyMoveCtorsOpsTest() :
        CopyMoveCtorsOpsTestBase(simox::meta::get_type_name<T>())
    {
    }
    virtual ~InstanceCopyMoveCtorsOpsTest() override = default;

    void reset() override
    {
        in = T {id};
        BOOST_CHECK_EQUAL(in.id(), id);
    }

    void testCopyCtor() override
    {
        T out { in };

        BOOST_CHECK_EQUAL(out.id(), id);

        CustomChecks<T>::checkEqual(out, in);
    }
    void testCopyOp() override
    {
        T out;
        out = in;

        BOOST_CHECK_EQUAL(out.id(), id);

        CustomChecks<T>::checkEqual(out, in);
    }
    void testMoveCtor() override
    {
        T copy { in };
        T out { std::move(in) };

        BOOST_CHECK_EQUAL(in.id(), idMoved);
        BOOST_CHECK_EQUAL(out.id(), id);

        CustomChecks<T>::checkEqual(out, copy);
        CustomChecks<T>::checkMoved(in);
    }
    void testMoveOp() override
    {
        T copy { in };
        T out;
        out = std::move(in);

        BOOST_CHECK_EQUAL(in.id(), idMoved);
        BOOST_CHECK_EQUAL(out.id(), id);

        CustomChecks<T>::checkEqual(out, copy);
        CustomChecks<T>::checkMoved(in);
    }
};


template <class T>
struct CopyMoveCtorsOpsTest : public CopyMoveCtorsOpsTestBase
{
    T in;

    CopyMoveCtorsOpsTest() : CopyMoveCtorsOpsTestBase(simox::meta::get_type_name<T>())
    {
    }
    virtual ~CopyMoveCtorsOpsTest() override = default;

    void reset() override
    {
        in = T {id};
        if constexpr(std::is_same_v<T, wm::Memory>)
        {
            in.addCoreSegment("C");
        }
        {
            armem::EntityUpdate update;
            update.entityID = armem::MemoryID("M", "C", "P", "E", armem::Time(armem::Duration::MicroSeconds(123000)), 0);
            update.timeCreated = update.entityID.timestamp;
            update.instancesData.emplace_back();
            in.update(update);
        }

        BOOST_CHECK_EQUAL(in.id(), id);
        BOOST_CHECK_EQUAL(in.size(), 1);

        if constexpr(std::is_base_of_v <armarx::armem::base::detail::AronTyped, T>)
        {

            in.aronType() = std::make_shared<aron::type::Object>("some_object");
            BOOST_CHECK(in.aronType());
        }
    }

    void testCopyCtor() override
    {
        T out { in };
        BOOST_CHECK_EQUAL(in.id(), id);
        BOOST_CHECK_EQUAL(in.size(), 1);
        BOOST_CHECK_EQUAL(out.id(), id);
        BOOST_CHECK_EQUAL(out.size(), 1);

        if constexpr(std::is_base_of_v <armarx::armem::base::detail::AronTyped, T>)
        {
            BOOST_CHECK(in.aronType());
            BOOST_CHECK(out.aronType());
            BOOST_CHECK_EQUAL(in.aronType(), out.aronType());
        }

        CustomChecks<T>::checkEqual(out, in);
    }
    void testCopyOp() override
    {
        T out;
        out = in;
        BOOST_CHECK_EQUAL(in.id(), id);
        BOOST_CHECK_EQUAL(in.size(), 1);
        BOOST_CHECK_EQUAL(out.id(), id);
        BOOST_CHECK_EQUAL(out.size(), 1);

        if constexpr(std::is_base_of_v <armarx::armem::base::detail::AronTyped, T>)
        {
            BOOST_CHECK(in.aronType());
            BOOST_CHECK(out.aronType());
            BOOST_CHECK_EQUAL(in.aronType(), out.aronType());
        }

        CustomChecks<T>::checkEqual(out, in);
    }
    void testMoveCtor() override
    {
        T out { std::move(in) };

        BOOST_CHECK_EQUAL(in.id(), idMoved);
        BOOST_CHECK_EQUAL(in.size(), 0);
        BOOST_CHECK_EQUAL(out.id(), id);
        BOOST_CHECK_EQUAL(out.size(), 1);

        if constexpr(std::is_base_of_v <armarx::armem::base::detail::AronTyped, T>)
        {
            BOOST_CHECK(out.aronType());
            BOOST_CHECK_EQUAL(in.aronType(), nullptr);
        }

        CustomChecks<T>::checkMoved(in);
    }
    void testMoveOp() override
    {
        T out;
        out = std::move(in);

        BOOST_CHECK_EQUAL(in.id(), idMoved);
        BOOST_CHECK_EQUAL(in.size(), 0);
        BOOST_CHECK_EQUAL(out.id(), id);
        BOOST_CHECK_EQUAL(out.size(), 1);

        if constexpr(std::is_base_of_v <armarx::armem::base::detail::AronTyped, T>)
        {
            BOOST_CHECK(out.aronType());
            BOOST_CHECK_EQUAL(in.aronType(), nullptr);
        }

        CustomChecks<T>::checkMoved(in);
    }
};



BOOST_AUTO_TEST_CASE(test_copy_move_ctors_ops)
{
    {
        InstanceCopyMoveCtorsOpsTest<wm::EntityInstance>().test();
        CopyMoveCtorsOpsTest<wm::EntitySnapshot>().test();
        CopyMoveCtorsOpsTest<wm::Entity>().test();
        CopyMoveCtorsOpsTest<wm::ProviderSegment>().test();
        CopyMoveCtorsOpsTest<wm::CoreSegment>().test();
        CopyMoveCtorsOpsTest<wm::Memory>().test();
    }
    /*{
        InstanceCopyMoveCtorsOpsTest<armem::ltm::EntityInstance>().test();
        CopyMoveCtorsOpsTest<armem::ltm::EntitySnapshot>().test();
        CopyMoveCtorsOpsTest<armem::ltm::Entity>().test();
        CopyMoveCtorsOpsTest<armem::ltm::ProviderSegment>().test();
        CopyMoveCtorsOpsTest<armem::ltm::CoreSegment>().test();
        CopyMoveCtorsOpsTest<armem::ltm::Memory>().test();
    }*/
    /*{
        InstanceCopyMoveCtorsOpsTest<armem::d_ltm::EntityInstance>().test();
        CopyMoveCtorsOpsTest<armem::d_ltm::EntitySnapshot>().test();
        CopyMoveCtorsOpsTest<armem::d_ltm::Entity>().test();
        CopyMoveCtorsOpsTest<armem::d_ltm::ProviderSegment>().test();
        CopyMoveCtorsOpsTest<armem::d_ltm::CoreSegment>().test();
        CopyMoveCtorsOpsTest<armem::d_ltm::Memory>().test();
    }*/
}



BOOST_AUTO_TEST_CASE(test_segment_setup)
{
    armem::EntityUpdate update;

    wm::Memory memory("Memory");
    BOOST_CHECK_EQUAL(memory.name(), "Memory");
    {
        update.entityID = armem::MemoryID::fromString("OtherMemory/SomeSegment");
        BOOST_CHECK_THROW(memory.update(update), armem::error::ContainerNameMismatch);
        update.entityID = armem::MemoryID::fromString("Memory/MissingSegment");
        BOOST_CHECK_THROW(memory.update(update), armem::error::MissingEntry);
    }

    wm::CoreSegment& coreSegment = memory.addCoreSegment("ImageRGB");
    BOOST_CHECK_EQUAL(coreSegment.name(), "ImageRGB");
    BOOST_CHECK(memory.hasCoreSegment(coreSegment.name()));
    {
        update.entityID = armem::MemoryID::fromString("Memory/OtherCoreSegment");
        BOOST_CHECK_THROW(coreSegment.update(update), armem::error::ContainerNameMismatch);
        // This adds the segment now (changed default behavior)
        //update.entityID = armem::MemoryID::fromString("Memory/ImageRGB/MissingProvider");
        //BOOST_CHECK_THROW(coreSegment.update(update), armem::error::MissingEntry);
    }

    wm::ProviderSegment& providerSegment = coreSegment.addProviderSegment("SomeRGBImageProvider");
    BOOST_CHECK_EQUAL(providerSegment.name(), "SomeRGBImageProvider");
    BOOST_CHECK(coreSegment.hasProviderSegment(providerSegment.name()));
    {
        update.entityID = armem::MemoryID::fromString("Memory/ImageRGB/OtherRGBImageProvider");
        BOOST_CHECK_THROW(providerSegment.update(update), armem::error::ContainerNameMismatch);
    }


    // A successful update.

    update.entityID = armem::MemoryID::fromString("Memory/ImageRGB/SomeRGBImageProvider/image");
    update.instancesData =
    {
        std::make_shared<aron::data::Dict>(),
        std::make_shared<aron::data::Dict>()
    };
    update.timeCreated = armem::Time(armem::Duration::MilliSeconds(1000));
    BOOST_CHECK_NO_THROW(providerSegment.update(update));

    BOOST_CHECK_EQUAL(providerSegment.size(), 1);
    BOOST_CHECK(providerSegment.hasEntity("image"));
    BOOST_CHECK(!providerSegment.hasEntity("other_image"));

    wm::Entity& entity = providerSegment.getEntity("image");
    BOOST_CHECK_EQUAL(entity.name(), "image");
    BOOST_CHECK_EQUAL(entity.size(), 1);
    BOOST_CHECK(entity.hasSnapshot(update.timeCreated));

    wm::EntitySnapshot& entitySnapshot = entity.getSnapshot(update.timeCreated);
    BOOST_CHECK_EQUAL(entitySnapshot.size(), update.instancesData.size());


    // Another update (on memory).

    update.instancesData = { std::make_shared<aron::data::Dict>() };
    update.timeCreated = armem::Time(armem::Duration::MilliSeconds(2000));
    memory.update(update);
    BOOST_CHECK_EQUAL(entity.size(), 2);
    BOOST_CHECK(entity.hasSnapshot(update.timeCreated));
    BOOST_CHECK_EQUAL(entity.getSnapshot(update.timeCreated).size(), update.instancesData.size());


    // A third update (on entity).
    update.instancesData = { std::make_shared<aron::data::Dict>() };
    update.timeCreated = armem::Time(armem::Duration::MilliSeconds(3000));
    entity.update(update);
    BOOST_CHECK_EQUAL(entity.size(), 3);

}



BOOST_AUTO_TEST_CASE(test_history_size_in_entity)
{
    armem::server::wm::Entity entity("entity");

    armem::EntityUpdate update;
    update.entityID.entityName = entity.name();

    // With unlimited history.
    update.timeCreated = armem::Time(armem::Duration::MilliSeconds(1000));
    entity.update(update);
    update.timeCreated = armem::Time(armem::Duration::MilliSeconds(2000));
    entity.update(update);
    update.timeCreated = armem::Time(armem::Duration::MilliSeconds(3000));
    entity.update(update);
    BOOST_CHECK_EQUAL(entity.size(), 3);

    // Now with maximum history size.
    entity.setMaxHistorySize(2);
    BOOST_CHECK_EQUAL(entity.size(), 2);
    BOOST_CHECK(not entity.hasSnapshot(armem::Time(armem::Duration::MilliSeconds(1000))));
    BOOST_CHECK(entity.hasSnapshot(armem::Time(armem::Duration::MilliSeconds(2000))));
    BOOST_CHECK(entity.hasSnapshot(armem::Time(armem::Duration::MilliSeconds(3000))));


    update.timeCreated = armem::Time(armem::Duration::MilliSeconds(4000));
    entity.update(update);
    BOOST_CHECK_EQUAL(entity.size(), 2);
    BOOST_CHECK(not entity.hasSnapshot(armem::Time(armem::Duration::MilliSeconds(2000))));
    BOOST_CHECK(entity.hasSnapshot(armem::Time(armem::Duration::MilliSeconds(3000))));
    BOOST_CHECK(entity.hasSnapshot(armem::Time(armem::Duration::MilliSeconds(4000))));

    // Disable maximum history size.
    entity.setMaxHistorySize(-1);

    update.timeCreated = armem::Time(armem::Duration::MilliSeconds(5000));
    entity.update(update);
    BOOST_CHECK_EQUAL(entity.size(), 3);
    BOOST_CHECK(entity.hasSnapshot(armem::Time(armem::Duration::MilliSeconds(3000))));
    BOOST_CHECK(entity.hasSnapshot(armem::Time(armem::Duration::MilliSeconds(4000))));
    BOOST_CHECK(entity.hasSnapshot(armem::Time(armem::Duration::MilliSeconds(5000))));
}


BOOST_AUTO_TEST_CASE(test_history_size_in_provider_segment)
{
    armem::server::wm::ProviderSegment providerSegment("SomeRGBImageProvider");

    armem::EntityUpdate update;
    update.entityID.providerSegmentName = providerSegment.name();

    std::vector<std::string> entityNames = { "A", "B" };

    // Fill entities and histories with unlimited size.
    for (const std::string& name : entityNames)
    {
        update.entityID.entityName = name;

        update.timeCreated = armem::Time(armem::Duration::MilliSeconds(1000));
        providerSegment.update(update);
        update.timeCreated = armem::Time(armem::Duration::MilliSeconds(2000));
        providerSegment.update(update);
        update.timeCreated = armem::Time(armem::Duration::MilliSeconds(3000));
        providerSegment.update(update);
    }
    update.entityID.entityName = entityNames.back();
    update.timeCreated = armem::Time(armem::Duration::MilliSeconds(4000));
    providerSegment.update(update);

    BOOST_CHECK_EQUAL(providerSegment.getEntity("A").size(), 3);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("B").size(), 4);


    // Employ maximum history size.
    providerSegment.setMaxHistorySize(3);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("A").size(), 3);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("B").size(), 3);

    providerSegment.setMaxHistorySize(2);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("A").size(), 2);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("B").size(), 2);

    providerSegment.setMaxHistorySize(3);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("A").size(), 2);
    BOOST_CHECK_EQUAL(providerSegment.getEntity("B").size(), 2);

    // Add new entity.
    providerSegment.setMaxHistorySize(2);

    update.entityID.entityName = "C";
    update.timeCreated = armem::Time(armem::Duration::MilliSeconds(1000));
    providerSegment.update(update);
    update.timeCreated = armem::Time(armem::Duration::MilliSeconds(2000));
    providerSegment.update(update);
    update.timeCreated = armem::Time(armem::Duration::MilliSeconds(3000));
    providerSegment.update(update);

    // Check correctly inherited history size.
    BOOST_CHECK_EQUAL(providerSegment.getEntity("C").getMaxHistorySize(), 2);
    // Check actual history size.
    BOOST_CHECK_EQUAL(providerSegment.getEntity("C").size(), 2);

    // Remove maximum.
    providerSegment.setMaxHistorySize(-1);

    entityNames.push_back("C");
    for (const std::string& name : entityNames)
    {
        update.entityID.entityName = name;
        update.timeCreated = armem::Time(armem::Duration::MilliSeconds(5000));
        providerSegment.update(update);
        BOOST_CHECK_EQUAL(providerSegment.getEntity(name).getMaxHistorySize(), -1);
        BOOST_CHECK_EQUAL(providerSegment.getEntity(name).size(), 3);
    }
}
