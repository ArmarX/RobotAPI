/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::armem

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include <RobotAPI/libraries/armem/server/ltm/disk/Memory.h>
#include <RobotAPI/libraries/armem/core/error.h>

#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/type/variant/All.h>

#include <ArmarXCore/core/time/StopWatch.h>


#include <filesystem>
#include <iostream>

namespace armem = armarx::armem;
namespace aron = armarx::aron;
namespace fs = std::filesystem;

namespace ArMemLTMBenchmark
{
    struct Fixture
    {
        fs::path storagePath = "/tmp/MemoryExport";

        Fixture()
        {
            clearStoragePath();
        }
        ~Fixture()
        {
            //clearStoragePath();
        }

        void clearStoragePath()
        {
            if (fs::exists(storagePath))
            {
                fs::remove_all(storagePath);
            }
            BOOST_TEST_INFO("Storage path: " << storagePath);
            BOOST_REQUIRE(!fs::exists(storagePath));
        }

        void storeElementNTimes(const std::string& memoryName, const aron::data::DictPtr& dict, int waitingTimeMs, int n)
        {
            armem::server::ltm::disk::Memory ltm(storagePath, memoryName);

            armem::wm::Memory wm(memoryName);
            auto& core = wm.addCoreSegment("CoreS");
            auto& prov = core.addProviderSegment("ProvS");
            auto& en = prov.addEntity("EntityS");

            for (int i = 0; i < n; ++i)
            {
                std::cout << "Store instance " << i << " of memory " << memoryName << std::endl;

                en.clear();
                auto& snap = en.addSnapshot(armem::Time::Now());
                auto& ins = snap.addInstance();
                auto cloned = aron::data::Dict::DynamicCastAndCheck(dict->clone());
                ins.data() = cloned;

                ltm.store(wm);
                ltm.storeBuffer();

                usleep(waitingTimeMs * 1000.0);
            }
        }
    };
}

BOOST_FIXTURE_TEST_SUITE(ArMemLTMBenchmark, Fixture)


BOOST_AUTO_TEST_CASE(test_memory_export__single_image_benchmark)
{
    auto data = std::make_shared<aron::data::Dict>();

    std::vector<int> dimensions = {720, 1280, 3};
    std::string type = "16";
    std::vector<unsigned char> d(720*1280*3, 255);
    data->addElement("image", std::make_shared<aron::data::NDArray>(dimensions, type, d));

    storeElementNTimes("SingleImageBenchmark", data, 100, 2);
}

BOOST_AUTO_TEST_SUITE_END()

