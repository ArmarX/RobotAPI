/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::armem

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include <RobotAPI/libraries/armem/core/wm/ice_conversions.h>
#include <RobotAPI/libraries/armem/core/error.h>

#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/type/variant/All.h>


//#include "../core/io/diskWriter/NlohmannJSON/NlohmannJSONDiskWriter.h"

#include <filesystem>
#include <iostream>

namespace armem = armarx::armem;
namespace aron = armarx::aron;
namespace fs = std::filesystem;

namespace ArMemLTMTest
{
    struct Fixture
    {
        fs::path storagePath = "/tmp/TestMemoryExport";

        Fixture()
        {
            clearStoragePath();
            assureStoragePath();
        }
        ~Fixture()
        {
            //clearStoragePath();
        }

        void assureStoragePath()
        {
            if (!fs::is_directory(storagePath))
            {
                fs::create_directory(storagePath);
            }
            BOOST_TEST_INFO("Storage path: " << storagePath);
            BOOST_REQUIRE(fs::exists(storagePath) && fs::is_directory(storagePath));
        }

        void clearStoragePath()
        {
            if (fs::exists(storagePath))
            {
                fs::remove_all(storagePath);
            }
            BOOST_TEST_INFO("Storage path: " << storagePath);
            BOOST_REQUIRE(!fs::exists(storagePath));
        }


        static armem::wm::Memory setupMemoryWithType(
            const std::string& memoryName,
            const aron::type::ObjectPtr& t1,
            const aron::type::ObjectPtr& t2,
            unsigned int numSnapshots,
            unsigned int numInstances)
        {
            /*aron::Randomizer r;

            armem::wm::Memory memory(memoryName);
            BOOST_CHECK_EQUAL(memory.name(), memoryName);

            armem::wm::CoreSegment& coreSegment = memory.addCoreSegment("TestCoreSegment");
            BOOST_CHECK_EQUAL(coreSegment.name(), "TestCoreSegment");
            BOOST_CHECK(memory.hasCoreSegment(coreSegment.name()));

            armem::wm::ProviderSegment& providerSegment = coreSegment.addProviderSegment("TestProvider");
            BOOST_CHECK_EQUAL(providerSegment.name(), "TestProvider");
            BOOST_CHECK(coreSegment.hasProviderSegment(providerSegment.name()));

            //coreSegment.aronType() = t1;
            //providerSegment.aronType() = t2;

            aron::type::ObjectPtr t = t2 != nullptr ? t2 : t1;

            for (unsigned int i = 0; i < numSnapshots; ++i)
            {
                armem::EntityUpdate update;
                std::vector<aron::data::DictPtr> q;

                for (unsigned int j = 0; j < numInstances; ++j)
                {
                    aron::data::DictPtr m = aron::data::Dict::DynamicCastAndCheck(r.generateEmptyAronDataFromType(t));
                    r.initializeRandomly(m, t);
                    q.push_back(m);
                }
                update.entityID = armem::MemoryID::fromString(memoryName + "/TestCoreSegment/TestProvider/TestEntity");
                update.instancesData = q;
                update.timeCreated = armem::Time::Now();
                BOOST_CHECK_NO_THROW(providerSegment.update(update));
                BOOST_CHECK_EQUAL(providerSegment.size(), 1);
            }
            BOOST_CHECK(providerSegment.hasEntity("TestEntity"));

            return memory;*/
            return {};
        }

        template <class TypeNavigatorT>
        aron::type::ObjectPtr makeType(const std::string& memberPrefix, int numMembers = 4)
        {
            /*aron::type::ObjectPtr t = std::make_shared<aron::type::Object>(aron::Path());
            t->setObjectName("TestObjectType1");

            for (int i = 0; i < numMembers; ++i)
            {
                t->addMemberType(memberPrefix + std::to_string(i + 1), std::make_shared<TypeNavigatorT>());
            }

            return t;*/
            return nullptr;
        }

        template <class TypeNavigatorT>
        void run(const std::string& memoryName, const std::string& memberNamePrefix)
        {
            /*aron::typenavigator::ObjectNavigatorPtr t = makeType<TypeNavigatorT>(memberNamePrefix);
            armem::Memory memory = setupMemoryWithType(memoryName, t, nullptr, 15, 1);

            // create reader and writer
            armem::io::DiskWriterPtr w = std::make_shared<armem::io::NlohmannJSONDiskWriter>();

            // export memory
            BOOST_REQUIRE(fs::exists(storagePath));
            armem::io::DiskMemory dMem(memory);
            std::string mfs_str = dMem.toString();

            armem::io::DiskMemory dMem2();
            std::string mfs2_str = dMem2.toString();

            //std::cout << "MFS1: " << std::endl;
            //std::cout << mfs_str << std::endl;
            //std::cout << "MFS2: " << std::endl;
            //std::cout << mfs2_str << std::endl;
            BOOST_CHECK_EQUAL(mfs_str == mfs2_str, true);

            armem::Memory memory2 = dMem2.toMemory();
            memory2.name() = memoryName;

            BOOST_CHECK_EQUAL(memory.equalsDeep(memory2), true);*/
        }
    };
}

BOOST_FIXTURE_TEST_SUITE(ArMemLTMTest, Fixture)


BOOST_AUTO_TEST_CASE(test_memory_export__easy_int_setup)
{
    run<aron::type::Int>("TestMemory_IntSetup", "theInt");
}

BOOST_AUTO_TEST_CASE(test_memory_export__easy_long_setup)
{
    run<aron::type::Long>("TestMemory_LongSetup", "theLong");
}

BOOST_AUTO_TEST_CASE(test_memory_export__easy_float_setup)
{
    run<aron::type::Float>("TestMemory_FloatSetup", "theFloat");
}

BOOST_AUTO_TEST_CASE(test_memory_export__easy_double_setup)
{
    run<aron::type::Double>("TestMemory_DoubleSetup", "theDouble");
}

BOOST_AUTO_TEST_CASE(test_memory_export__easy_string_setup)
{
    run<aron::type::String>("TestMemory_StringSetup", "theString");
}

BOOST_AUTO_TEST_CASE(test_memory_export__easy_bool_setup)
{
    run<aron::type::Bool>("TestMemory_BoolSetup", "theBool");
}

/*
BOOST_AUTO_TEST_CASE(test_memory_export__easy_rainer_setup)
{
    std::string memoryName = "TestMemory_RainerSetup";

    aron::typenavigator::ObjectNavigatorPtr t(new aron::typenavigator::ObjectNavigator(aron::Path()));
    t->setObjectName("TestRainerType1");

    aron::typenavigator::DictNavigatorPtr tm1(new aron::typenavigator::DictNavigator(aron::Path()));
    aron::typenavigator::FloatNavigatorPtr tm1m1(new aron::typenavigator::FloatNavigator(aron::Path()));
    tm1->setAcceptedType(tm1m1);
    aron::typenavigator::StringNavigatorPtr tm2(new aron::typenavigator::StringNavigator(aron::Path()));
    aron::typenavigator::DictNavigatorPtr tm3(new aron::typenavigator::DictNavigator(aron::Path()));
    aron::typenavigator::StringNavigatorPtr tm3m1(new aron::typenavigator::StringNavigator(aron::Path()));
    tm3->setAcceptedType(tm3m1);
    aron::typenavigator::StringNavigatorPtr tm4(new aron::typenavigator::StringNavigator(aron::Path()));
    t->addMemberType("float_params", tm1);
    t->addMemberType("name", tm2);
    //t->addMemberType("string_params", tm3);
    t->addMemberType("type", tm4);

    armem::Memory memory = setupMemoryWithType(memoryName, t, nullptr, 15, 1);

    // export memory
    std::string storagePath = "/tmp/MemoryExport"; // important! without tailing /
    std::filesystem::remove_all(storagePath);
    armem::io::FileSystemMemoryManager mfs(storagePath, true);
    mfs.writeOnDisk(memory);

    std::string mfs_str = mfs.toString();

    armem::io::FileSystemMemoryManager mfs2(storagePath);
    mfs2.update();

    std::string mfs2_str = mfs2.toString();

    //std::cout << "MFS1: " << std::endl;
    //std::cout << mfs_str << std::endl;
    //std::cout << "MFS2: " << std::endl;
    //std::cout << mfs2_str << std::endl;
    BOOST_CHECK_EQUAL(mfs_str == mfs2_str, true);

    armem::Memory memory2 = mfs.readMemoryFromDisk(memoryName);
    BOOST_CHECK_EQUAL(memory.equalsDeep(memory2), true);
}*/


BOOST_AUTO_TEST_SUITE_END()

