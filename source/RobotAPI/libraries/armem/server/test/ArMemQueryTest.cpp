/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::armem

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include <RobotAPI/interface/armem/query.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/base/detail/negative_index_semantics.h>
#include <RobotAPI/libraries/armem/server/query_proc/wm/wm.h>

#include <ArmarXCore/core/exceptions/LocalException.h>

#include <SimoxUtility/algorithm/get_map_keys_values.h>
#include <SimoxUtility/algorithm/string.h>

#include <iostream>


namespace armem = armarx::armem;
namespace aron = armarx::aron;
namespace query = armarx::armem::query::data;
using EntityQueryProcessor = armarx::armem::server::query_proc::wm::EntityQueryProcessor;


namespace ArMemQueryTest
{

    class UnknownEntityQuery : public query::EntityQuery
    {
    };

    struct Fixture
    {
        armem::wm::Entity entity;

        armem::server::query_proc::wm::EntityQueryProcessor processor;

        std::vector<armem::wm::Entity> results;


        Fixture()
        {
            entity = armem::wm::Entity("entity");

            armem::wm::EntitySnapshot snapshot;
            snapshot.time() = armem::Time(armem::Duration::MicroSeconds(1000));
            entity.addSnapshot(snapshot);
            snapshot.time() = armem::Time(armem::Duration::MicroSeconds(2000));
            entity.addSnapshot(snapshot);
            snapshot.time() = armem::Time(armem::Duration::MicroSeconds(3000));
            entity.addSnapshot(snapshot);
            snapshot.time() = armem::Time(armem::Duration::MicroSeconds(4000));
            entity.addSnapshot(snapshot);
            snapshot.time() = armem::Time(armem::Duration::MicroSeconds(5000));
            entity.addSnapshot(snapshot);
        }
        ~Fixture()
        {
        }


        template <class QueryT>
        void addResults(QueryT query = {})
        {
            // Test whether we get the same result when we pass the concrete type
            // (static resolution) and when we pass the base type (dynamic resolution).
            results.emplace_back(processor.process(query, entity));
            results.emplace_back(processor.process(static_cast<query::EntityQuery const&>(query), entity));
        }
    };

}


BOOST_FIXTURE_TEST_SUITE(ArMemQueryTest, ArMemQueryTest::Fixture)



BOOST_AUTO_TEST_CASE(test_entity_UnknownQueryType)
{
    BOOST_CHECK_THROW(processor.process(UnknownEntityQuery(), entity), armem::error::UnknownQueryType);
    std::string msg;
    try
    {
        processor.process(UnknownEntityQuery(), entity);
    }
    catch (const armem::error::UnknownQueryType& e)
    {
        msg = e.what();
    }
    BOOST_TEST_MESSAGE(msg);
    BOOST_CHECK(simox::alg::contains(msg, "UnknownEntityQuery"));
}



BOOST_AUTO_TEST_CASE(test_entity_Single_latest)
{
    addResults(query::entity::Single());
    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());
        BOOST_CHECK_EQUAL(result.size(), 1);
        const armem::wm::EntitySnapshot* first = result.findFirstSnapshotAfterOrAt(armem::Time(armem::Duration::MicroSeconds(0)));
        BOOST_REQUIRE_NE(first, nullptr);
        BOOST_CHECK_EQUAL(first->time(), armem::Time(armem::Duration::MicroSeconds(5000)));
    }
}

static armem::dto::Time
t_usec(long usec)
{
    armem::dto::Time t;
    t.timeSinceEpoch.microSeconds = usec;
    return t;
}
static armem::dto::Duration
d_usec(long usec)
{
    armem::dto::Duration d;
    d.microSeconds = usec;
    return d;
}

BOOST_AUTO_TEST_CASE(test_entity_Single_existing)
{
    addResults(query::entity::Single(t_usec(3000)));
    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());
        BOOST_REQUIRE_EQUAL(result.size(), 1);
        BOOST_CHECK_EQUAL(result.getFirstSnapshot().time(), armem::Time(armem::Duration::MicroSeconds(3000)));
    }
}


BOOST_AUTO_TEST_CASE(test_entity_Single_non_existing)
{
    addResults(query::entity::Single(t_usec(3500)));
    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());
        BOOST_CHECK_EQUAL(result.size(), 0);
    }
}


BOOST_AUTO_TEST_CASE(test_entity_All)
{
    addResults(query::entity::All());
    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());
        BOOST_CHECK_EQUAL(result.size(), entity.size());
        for (armem::Time time : entity.getTimestamps())
        {
            BOOST_REQUIRE(result.hasSnapshot(time));
            const armem::wm::EntitySnapshot& rs = result.getSnapshot(time);
            const armem::wm::EntitySnapshot& es = entity.getSnapshot(time);
            BOOST_CHECK_EQUAL(rs.time(), es.time());
            BOOST_CHECK_NE(&rs, &es);
        }
    }
}



BOOST_AUTO_TEST_CASE(test_entity_TimeRange_slice)
{
    addResults(query::entity::TimeRange(t_usec(1500), t_usec(3500)));
    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());

        BOOST_CHECK_EQUAL(result.size(), 2);

        std::vector<armem::Time> times = result.getTimestamps();
        std::vector<armem::Time> expected
        {
            armem::Time(armem::Duration::MicroSeconds(2000)), armem::Time(armem::Duration::MicroSeconds(3000))
        };
        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }
}


BOOST_AUTO_TEST_CASE(test_entity_TimeRange_exact)
{
    addResults(query::entity::TimeRange(t_usec(2000), t_usec(4000)));
    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());

        BOOST_CHECK_EQUAL(result.size(), 3);

        std::vector<armem::Time> times = result.getTimestamps();
        std::vector<armem::Time> expected
        {
            armem::Time(armem::Duration::MicroSeconds(2000)),
            armem::Time(armem::Duration::MicroSeconds(3000)),
            armem::Time(armem::Duration::MicroSeconds(4000))
        };
        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }
}

BOOST_AUTO_TEST_CASE(test_entity_TimeRange_all)
{
    addResults(query::entity::TimeRange(t_usec(0), t_usec(10000)));
    addResults(query::entity::TimeRange(t_usec(-1), t_usec(-1)));
    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());
        BOOST_CHECK_EQUAL(result.size(), entity.size());

        const std::vector<armem::Time> times = result.getTimestamps();
        const std::vector<armem::Time> expected = entity.getTimestamps();
        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }
}


BOOST_AUTO_TEST_CASE(test_entity_TimeRange_empty)
{
    addResults(query::entity::TimeRange(t_usec(2400), t_usec(2600)));
    addResults(query::entity::TimeRange(t_usec(6000), t_usec(1000)));
    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());

        BOOST_CHECK_EQUAL(result.size(), 0);
    }
}


BOOST_AUTO_TEST_CASE(test_entity_TimeRange_from_start)
{
    addResults(query::entity::TimeRange(t_usec(-1), t_usec(2500)));
    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());

        BOOST_CHECK_EQUAL(result.size(), 2);

        const std::vector<armem::Time> times = result.getTimestamps();
        std::vector<armem::Time> expected
        {
            armem::Time(armem::Duration::MicroSeconds(1000)),
            armem::Time(armem::Duration::MicroSeconds(2000))
        };
        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }
}


BOOST_AUTO_TEST_CASE(test_entity_TimeRange_to_end)
{
    addResults(query::entity::TimeRange(t_usec(2500), t_usec(-1)));
    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());
        BOOST_CHECK_EQUAL(result.size(), 3);

        const std::vector<armem::Time> times = result.getTimestamps();
        std::vector<armem::Time> expected
        {
            armem::Time(armem::Duration::MicroSeconds(3000)),
            armem::Time(armem::Duration::MicroSeconds(4000)),
            armem::Time(armem::Duration::MicroSeconds(5000))
        };
        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }
}


/* BeforeTime */

BOOST_AUTO_TEST_CASE(test_entity_BeforeTime_1)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::BeforeTime(t_usec(3500), 1));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        const std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE_EQUAL(times.size(), 1);

        BOOST_CHECK_EQUAL(times.front(), armem::Time(armem::Duration::MicroSeconds(3000)));
    }
}

BOOST_AUTO_TEST_CASE(test_entity_BeforeTime_2)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::BeforeTime(t_usec(3500), 2));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        const std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE_EQUAL(times.size(), 2);

        std::vector<armem::Time> expected
        {
            armem::Time(armem::Duration::MicroSeconds(2000)),
            armem::Time(armem::Duration::MicroSeconds(3000))
        };

        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }

}



/* BeforeOrAtTime */

BOOST_AUTO_TEST_CASE(test_entity_BeforeOrAtTime_before)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::BeforeOrAtTime(t_usec(3500)));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE_EQUAL(times.size(), 1);

        BOOST_REQUIRE_EQUAL(times.front(), armem::Time(armem::Duration::MicroSeconds(3000)));
    }
}

BOOST_AUTO_TEST_CASE(test_entity_BeforeOrAtTime_at)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::BeforeOrAtTime(t_usec(3000)));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE_EQUAL(times.size(), 1);

        BOOST_REQUIRE_EQUAL(times.front(),  armem::Time(armem::Duration::MicroSeconds(3000)));
    }
}

BOOST_AUTO_TEST_CASE(test_entity_BeforeOrAtTime_lookup_past)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::BeforeOrAtTime(t_usec(1)));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE(times.empty());
    }
}

/* TimeApprox */

/**
 * @brief Lookup between elements. No range specified.
 *
 * Desired behavior: Return elements before and after timestamp.
 */
BOOST_AUTO_TEST_CASE(test_entity_TimeApprox_no_limit)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::TimeApprox(t_usec(3500), d_usec(-1)));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE_EQUAL(times.size(), 2);

        std::vector<armem::Time> expected
        {
            armem::Time(armem::Duration::MicroSeconds(3000)),
            armem::Time(armem::Duration::MicroSeconds(4000))
        };

        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }

}

/**
 * @brief Lookup between elements. Range is OK.
 *
 * Desired behavior: Return elements before and after timestamp.
 */
BOOST_AUTO_TEST_CASE(test_entity_TimeApprox_limit_600)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::TimeApprox(t_usec(3500), d_usec(600)));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE_EQUAL(times.size(), 2);

        std::vector<armem::Time> expected
        {
            armem::Time(armem::Duration::MicroSeconds(3000)),
            armem::Time(armem::Duration::MicroSeconds(4000))
        };

        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }

}

/**
 * @brief Lookup between elements. Range is too small.
 *
 * Desired behavior: Return empty list.
 */
BOOST_AUTO_TEST_CASE(test_entity_TimeApprox_limit_too_small)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::TimeApprox(t_usec(3500), d_usec(100)));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE_EQUAL(times.size(), 0);
    }

}

/**
 * @brief Lookup between elements. Only next element is in range.
 *
 * Desired behavior: Return only element after query timestamp.
 */
BOOST_AUTO_TEST_CASE(test_entity_TimeApprox_limit_only_next)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::TimeApprox(t_usec(3700), d_usec(400)));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE_EQUAL(times.size(), 1);

        std::vector<armem::Time> expected
        {
            armem::Time(armem::Duration::MicroSeconds(4000))
        };

        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }
}

/**
 * @brief Lookup between elements. Only previous element is in range.
 *
 * Desired behavior: Return only element before query timestamp.
 */
BOOST_AUTO_TEST_CASE(test_entity_TimeApprox_limit_only_previous)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::TimeApprox(t_usec(3300), d_usec(400)));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE_EQUAL(times.size(), 1);

        std::vector<armem::Time> expected
        {
            armem::Time(armem::Duration::MicroSeconds(3000))
        };

        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }
}

/**
 * @brief Lookup with perfect match.
 *
 * Desired behavior: Return only element matching timestamp exactly.
 */
BOOST_AUTO_TEST_CASE(test_entity_TimeApprox_perfect_match)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::TimeApprox(t_usec(3000), d_usec(-1)));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE_EQUAL(times.size(), 1);

        std::vector<armem::Time> expected
        {
            armem::Time(armem::Duration::MicroSeconds(3000))
        };

        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }
}

/**
 * @brief Invalid lookup into the past.
 *
 * Desired behavior: Return empty list.
 */
BOOST_AUTO_TEST_CASE(test_entity_TimeApprox_lookup_past)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::TimeApprox(t_usec(1), d_usec(1)));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE(times.empty());
    }
}

/**
 * @brief Invalid lookup into the future.
 *
 * Desired behavior: Return empty list.
 */
BOOST_AUTO_TEST_CASE(test_entity_TimeApprox_lookup_future)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::TimeApprox(t_usec(10'000), d_usec(1)));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE(times.empty());
    }
}

/**
 * @brief Lookup into the future, but still considered valid as time range is not set.
 *
 * Desired behavior: Return most recent element in history.
 */
BOOST_AUTO_TEST_CASE(test_entity_TimeApprox_lookup_future_valid)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::TimeApprox(t_usec(10'000), d_usec(-1)));
    BOOST_REQUIRE_EQUAL(results.size(), 2);

    for (const auto& result : results)
    {
        std::vector<armem::Time> times = result.getTimestamps();
        BOOST_REQUIRE_EQUAL(times.size(), 1);

        std::vector<armem::Time> expected
        {
            armem::Time(armem::Duration::MicroSeconds(5'000))
        };

        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }
}

BOOST_AUTO_TEST_CASE(test_entity_TimeApprox_lookup_invalid_timestamp)
{
    BOOST_REQUIRE_THROW(addResults(query::entity::TimeApprox(t_usec(-1), d_usec(1))), ::armarx::LocalException);
}



BOOST_AUTO_TEST_CASE(test_negative_index_semantics)
{
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(0, 0), 0);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(0, 1), 0);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(0, 10), 0);

    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(1, 1), 0);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(5, 1), 0);

    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(1, 5), 1);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(5, 5), 4);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(5, 6), 5);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(5, 10), 5);

    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(-1, 0), 0);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(-5, 0), 0);

    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(-1, 1), 0);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(-5, 1), 0);

    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(-1, 2), 1);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(-5, 2), 0);

    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(-1, 5), 4);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(-5, 5), 0);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(-5, 6), 1);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(-5, 10), 5);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(-10, 10), 0);
    BOOST_CHECK_EQUAL(armem::base::detail::negativeIndexSemantics(-20, 10), 0);
}


BOOST_AUTO_TEST_CASE(test_entity_IndexRange_all_default)
{
    addResults(query::entity::IndexRange());
    addResults(query::entity::IndexRange(0, -1));
    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());

        BOOST_CHECK_EQUAL(result.size(), entity.size());

        const std::vector<armem::Time> times = result.getTimestamps();
        const std::vector<armem::Time> expected = entity.getTimestamps();
        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }

}


BOOST_AUTO_TEST_CASE(test_entity_IndexRange_slice)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::IndexRange(1,  3));  // => [1, 2, 3]
    addResults(query::entity::IndexRange(1, -2));  // 5 - 2 = 3
    addResults(query::entity::IndexRange(-4,  3));  // 5 - 4 = 1
    addResults(query::entity::IndexRange(-4, -2));
    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());

        BOOST_CHECK_EQUAL(result.size(), 3);

        std::vector<armem::Time> times = result.getTimestamps();
        std::vector<armem::Time> expected
        {
            armem::Time(armem::Duration::MicroSeconds(2000)),
            armem::Time(armem::Duration::MicroSeconds(3000)),
            armem::Time(armem::Duration::MicroSeconds(4000))
        };
        BOOST_CHECK_EQUAL_COLLECTIONS(times.begin(), times.end(), expected.begin(), expected.end());
    }
}


BOOST_AUTO_TEST_CASE(test_entity_IndexRange_empty_range)
{
    BOOST_REQUIRE_EQUAL(entity.size(), 5);
    addResults(query::entity::IndexRange(1,  0));
    addResults(query::entity::IndexRange(2,  1));
    addResults(query::entity::IndexRange(5,  3));
    addResults(query::entity::IndexRange(4, -3));   // 5-3 = 2
    addResults(query::entity::IndexRange(3, -3));
    addResults(query::entity::IndexRange(1, -5));

    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());

        BOOST_CHECK_EQUAL(result.size(), 0);
    }
}


BOOST_AUTO_TEST_CASE(test_entity_IndexRange_empty_entity)
{
    entity.clear();
    BOOST_REQUIRE_EQUAL(entity.size(), 0);
    addResults(query::entity::IndexRange(0,  0));
    addResults(query::entity::IndexRange(0, 10));
    addResults(query::entity::IndexRange(-10, -1));
    addResults(query::entity::IndexRange(2,  5));
    addResults(query::entity::IndexRange(3, -3));
    addResults(query::entity::IndexRange(-1, 10));

    BOOST_REQUIRE_GT(results.size(), 0);

    for (const armem::wm::Entity& result : results)
    {
        BOOST_CHECK_EQUAL(result.name(), entity.name());
        BOOST_CHECK_EQUAL(result.size(), 0);
    }
}


BOOST_AUTO_TEST_SUITE_END()
