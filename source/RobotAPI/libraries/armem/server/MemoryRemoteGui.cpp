#include "MemoryRemoteGui.h"

#include "RemoteGuiAronDataVisitor.h"

#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SimoxUtility/meta/type_name.h>

#include <mutex>


namespace armarx::armem::server
{

    template <class ...Args>
    MemoryRemoteGui::GroupBox MemoryRemoteGui::_makeGroupBox(const armem::base::MemoryBase<Args...>& memory) const
    {
        GroupBox group;
        group.setLabel(makeGroupLabel("Memory", memory.name(), memory.size()));

        if (memory.empty())
        {
            group.addChild(Label(makeNoItemsMessage("core segments")));
        }
        memory.forEachCoreSegment([this, &group](const auto & coreSegment)
        {
            group.addChild(this->makeGroupBox(coreSegment));
        });
        return group;
    }


    static std::string getTypeString(const armem::base::detail::AronTyped& typed)
    {
        std::stringstream type;
        if (typed.aronType())
        {
            type << " (" << typed.aronType()->getFullName() << ")";
        }
        else
        {
            type << " (no Aron type)";
        }
        return type.str();
    }

    template <class ...Args>
    MemoryRemoteGui::GroupBox MemoryRemoteGui::_makeGroupBox(const armem::base::CoreSegmentBase<Args...>& coreSegment) const
    {
        GroupBox group;
        group.setLabel(makeGroupLabel("Core Segment", coreSegment.name(), coreSegment.size())
                       + getTypeString(coreSegment));

        if (coreSegment.empty())
        {
            group.addChild(Label(makeNoItemsMessage("provider segments")));
        }
        coreSegment.forEachProviderSegment([this, &group](const auto & providerSegment)
        {
            group.addChild(this->makeGroupBox(providerSegment));
        });
        return group;
    }


    template <class ...Args>
    MemoryRemoteGui::GroupBox MemoryRemoteGui::_makeGroupBox(const armem::base::ProviderSegmentBase<Args...>& providerSegment) const
    {
        GroupBox group;
        group.setLabel(makeGroupLabel("Provider Segment", providerSegment.name(), providerSegment.size())
                       + getTypeString(providerSegment));

        if (providerSegment.empty())
        {
            group.addChild(Label(makeNoItemsMessage("entities")));
        }
        providerSegment.forEachEntity([this, &group](const auto & entity)
        {
            group.addChild(this->makeGroupBox(entity));
        });
        return group;
    }


    template <class ...Args>
    MemoryRemoteGui::GroupBox MemoryRemoteGui::_makeGroupBox(const armem::base::EntityBase<Args...>& entity) const
    {
        GroupBox group;
        group.setLabel(makeGroupLabel("Entity", entity.name(), entity.size()));

        if (entity.empty())
        {
            group.addChild(Label(makeNoItemsMessage("snapshots")));
        }

        auto addChild = [this, &group](const armem::wm::EntitySnapshot & snapshot)
        {
            group.addChild(makeGroupBox(snapshot));
        };

        if (int(entity.size()) <= maxHistorySize)
        {
            entity.forEachSnapshot(addChild);
        }
        else
        {
            const int margin = 2;
            entity.forEachSnapshotInIndexRange(0, margin, addChild);
            entity.forEachSnapshotInIndexRange(-margin, -1, addChild);
        }
        group.setCollapsed(true);

        return group;
    }


    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const armem::server::wm::Memory& memory) const
    {
        return this->_makeGroupBox(memory);
    }

    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const armem::wm::Memory& memory) const
    {
        return this->_makeGroupBox(memory);
    }


    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const armem::server::wm::CoreSegment& coreSegment) const
    {
        return coreSegment.doLocked([this, &coreSegment]()
        {
            return this->_makeGroupBox(coreSegment);
        });
    }


    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const armem::wm::CoreSegment& coreSegment) const
    {
        return this->_makeGroupBox(coreSegment);
    }


    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const armem::server::wm::ProviderSegment& providerSegment) const
    {
        return this->_makeGroupBox(providerSegment);
    }


    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const armem::wm::ProviderSegment& providerSegment) const
    {
        return this->_makeGroupBox(providerSegment);
    }


    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const armem::wm::Entity& entity) const
    {
        return this->_makeGroupBox(entity);
    }


    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const armem::server::wm::Entity& entity) const
    {
        return this->_makeGroupBox(entity);
    }


    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const armem::wm::EntitySnapshot& snapshot) const
    {
        GroupBox group;
        group.setLabel(makeGroupLabel("t", armem::toDateTimeMilliSeconds(snapshot.time()),
                                      snapshot.size(), " = ", ""));

        if (snapshot.empty())
        {
            group.addChild(Label(makeNoItemsMessage("instances")));
        }
        snapshot.forEachInstance([this, &group](const armem::wm::EntityInstance & instance)
        {
            group.addChild(makeGroupBox(instance));
            return true;
        });
        group.setCollapsed(true);

        return group;
    }


    MemoryRemoteGui::GroupBox MemoryRemoteGui::makeGroupBox(const armem::wm::EntityInstance& instance) const
    {
        GroupBox group;

        if (instance.data())
        {
            RemoteGuiAronDataVisitor v;
            aron::data::visitRecursive(v, instance.data());
            group = v.result;
        }
        else
        {
            group.addChild(Label("(No data.)"));
        }

        std::stringstream ss;
        ss << "Instance #" << instance.index();
        group.setLabel(ss.str());
        group.setCollapsed(true);

        return group;
    }


    std::string MemoryRemoteGui::makeGroupLabel(
        const std::string& term, const std::string& name, size_t size,
        const std::string& namePrefix, const std::string& nameSuffix) const
    {
        std::stringstream ss;
        ss << term << namePrefix << name << nameSuffix << " (" << size << ")";
        return ss.str();
    }


    std::string MemoryRemoteGui::makeNoItemsMessage(const std::string& term) const
    {
        std::stringstream ss;
        ss << "(no " << term << ")";
        return ss.str();
    }




}
