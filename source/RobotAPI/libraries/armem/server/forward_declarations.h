#pragma once

#include <RobotAPI/libraries/armem/core/forward_declarations.h>


namespace armarx::armem::server
{
    class MemoryToIceAdapter;
}
namespace armarx::armem::server::wm
{
    using EntityInstance = armem::wm::EntityInstance;
    using EntitySnapshot = armem::wm::EntitySnapshot;
    class Entity;
    class ProviderSegment;
    class CoreSegment;
    class Memory;
}
namespace armarx::armem::server::ltm::mongodb
{
    class Memory;
}
namespace armarx::armem::server::ltm::disk
{
    class Memory;
}
