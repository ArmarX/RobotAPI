#pragma once

#include <filesystem>

// Base Class
#include "../base/detail/EntityBase.h"
#include "detail/DiskStorage.h"

#include "EntitySnapshot.h"

namespace armarx::armem::server::ltm::disk
{
    /// @brief A memory storing data in mongodb (needs 'armarx memory start' to start the mongod instance)
    class Entity :
            public EntityBase<EntitySnapshot>,
            public DiskMemoryItem
    {
    public:
        Entity(const std::filesystem::path& parentPath, const MemoryID& id, const std::shared_ptr<Processors>& p, const DiskMemoryItem::MemoryEncodingMode mode, const unsigned long e);

        bool forEachSnapshot(std::function<void(EntitySnapshot&)>&& func) const override;
        bool forEachSnapshotInIndexRange(long first, long last, std::function<void(EntitySnapshot&)>&& func) const override;
        bool forEachSnapshotInTimeRange(const Time& min, const Time& max, std::function<void(EntitySnapshot&)>&& func) const override;
        bool forEachSnapshotBeforeOrAt(const Time& time, std::function<void(EntitySnapshot&)>&& func) const override;
        bool forEachSnapshotBefore(const Time& time, std::function<void(EntitySnapshot&)>&& func) const override;

        std::shared_ptr<EntitySnapshot> findSnapshot(const Time&) const override;
        std::shared_ptr<EntitySnapshot> findLatestSnapshot() const override;
        std::shared_ptr<EntitySnapshot> findLatestSnapshotBefore(const Time& time) const override;
        std::shared_ptr<EntitySnapshot> findLatestSnapshotBeforeOrAt(const Time& time) const override;
        std::shared_ptr<EntitySnapshot> findFirstSnapshotAfter(const Time& time) const override;
        std::shared_ptr<EntitySnapshot> findFirstSnapshotAfterOrAt(const Time& time) const override;

    protected:
        void _loadAllReferences(armem::wm::Entity&) override;
        void _resolve(armem::wm::Entity&) override;
        void _store(const armem::wm::Entity&) override;

    private:
        MemoryEncodingMode currentMode;
        unsigned long currentExport;
    };

} // namespace armarx::armem::server::ltm::disk
