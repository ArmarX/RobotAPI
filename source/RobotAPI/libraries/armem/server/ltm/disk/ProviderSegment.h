#pragma once

#include <filesystem>

// Base Class
#include "../base/detail/ProviderSegmentBase.h"
#include "detail/DiskStorage.h"

#include "Entity.h"

namespace armarx::armem::server::ltm::disk
{
    class ProviderSegment :
            public ProviderSegmentBase<Entity>,
            public DiskMemoryItem
    {
    public:
        ProviderSegment(const std::filesystem::path& parentPath, const MemoryID& id, const std::shared_ptr<Processors>& p, const DiskMemoryItem::MemoryEncodingMode mode, const unsigned long e);

        bool forEachEntity(std::function<void(Entity&)>&& func) const override;

        std::shared_ptr<Entity> findEntity(const std::string&) const override;

    protected:
        void _loadAllReferences(armem::wm::ProviderSegment&) override;
        void _resolve(armem::wm::ProviderSegment&) override;
        void _store(const armem::wm::ProviderSegment&) override;
        void _storeType(const armem::wm::ProviderSegment&) override;

    private:
        MemoryEncodingMode currentMode;
        unsigned long currentExport;
    };

} // namespace armarx::armem::server::ltm::disk
