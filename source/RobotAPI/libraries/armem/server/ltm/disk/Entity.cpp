// Header
#include "Entity.h"

// ArmarX
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/armem/core/base/detail/negative_index_semantics.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

#include "../base/filter/frequencyFilter/FrequencyFilter.h"


namespace armarx::armem::server::ltm::disk
{
    Entity::Entity(const std::filesystem::path& p, const MemoryID& id, const std::shared_ptr<Processors>& filters, const DiskMemoryItem::MemoryEncodingMode mode, const unsigned long e) :
        EntityBase(id, filters),
        DiskMemoryItem(p, EscapeSegmentName(id.memoryName), std::filesystem::path(EscapeSegmentName(id.coreSegmentName)) / EscapeSegmentName(id.providerSegmentName) / EscapeSegmentName(id.entityName)),
        currentMode(mode),
        currentExport(e)
    {
    }

    bool Entity::forEachSnapshot(std::function<void(EntitySnapshot&)>&& func) const
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return false;
        }

        for (const auto& hName : util::getAllDirectories(mPath, relPath))
        {
            if (!util::isNumber(hName))
            {
                ARMARX_WARNING << "Found a non-timestamp folder inside an entity '" << id().str() << "' with name '" << hName << "'. " <<
                                  "Ignoring this folder, however this is a bad situation.";
                continue;
            }

            // check if this is already a microsec folder (legacy export support)
            //if (std::stol(secName) > 1647524607 /* the time in us the new export was implemented */)
            //{
            //    EntitySnapshot c(memoryParentPath, id().withTimestamp(timeFromStringMicroSeconds(secName)), processors, currentMode, currentExport);
            //    func(c);
            //    continue;
            //}

            auto hRelPath = relPath / hName;
            for (const auto& secName : util::getAllDirectories(mPath, hRelPath))
            {
                if (!util::isNumber(secName))
                {
                    ARMARX_WARNING << "Found a non-timestamp folder inside an entity '" << id().str() << "' with name '" << secName << "'. " <<
                                      "Ignoring this folder, however this is a bad situation.";
                    continue;
                }

                auto secRelPath = hRelPath / secName;
                for (const auto& usecName : util::getAllDirectories(mPath, secRelPath))
                {
                    if (!util::isNumber(usecName))
                    {
                        ARMARX_WARNING << "Found a non-timestamp folder inside an entity '" << id().str() << "' with name '" << usecName << "'. " <<
                                          "Ignoring this folder, however this is a bad situation.";
                        continue;
                    }

                    EntitySnapshot c(memoryParentPath, id().withTimestamp(timeFromStringMicroSeconds(usecName)), processors, currentMode, currentExport);
                    func(c);
                }
            }
        }
        return true;
    }

    bool Entity::forEachSnapshotInIndexRange(long first, long last, std::function<void(EntitySnapshot&)>&& func) const
    {
        ARMARX_WARNING << "PLEASE NOTE THAT QUERYING THE LTM INDEX WISE MAY BE BUGGY BECAUSE THE FILESYSTEM ITERATOR IS UNSORTED!";

        if (first < 0 or last < 0)
        {
            // We need to know what the size of the memory is... May be slow
            unsigned long size = 0;
            auto f = [&](EntitySnapshot& e)
            {
                size++;
            };
            forEachSnapshot(std::move(f));

            first = armarx::armem::base::detail::negativeIndexSemantics(first, size);
            last = armarx::armem::base::detail::negativeIndexSemantics(last, size);
        }

        long checked = 0;
        auto f = [&](EntitySnapshot& e)
        {
            checked++;
            if (checked >= first && checked <= last)
            {
                func(e);
            }
        };

        return forEachSnapshot(std::move(f));
    }

    bool Entity::forEachSnapshotInTimeRange(const Time& min, const Time& max, std::function<void(EntitySnapshot&)>&& func) const
    {
        auto f = [&](EntitySnapshot& e)
        {
            auto ts = e.id().timestamp;
            if (ts >= min && ts <= max)
            {
                func(e);
            }
        };

        return forEachSnapshot(std::move(f));
    }

    bool Entity::forEachSnapshotBeforeOrAt(const Time& time, std::function<void(EntitySnapshot&)>&& func) const
    {
        auto f = [&](EntitySnapshot& e)
        {
            auto ts = e.id().timestamp;
            if (ts <= time)
            {
                func(e);
            }
        };

        return forEachSnapshot(std::move(f));
    }

    bool Entity::forEachSnapshotBefore(const Time& time, std::function<void(EntitySnapshot&)>&& func) const
    {
        auto f = [&](EntitySnapshot& e)
        {
            auto ts = e.id().timestamp;
            if (ts < time)
            {
                func(e);
            }
        };

        return forEachSnapshot(std::move(f));
    }

    std::shared_ptr<EntitySnapshot> Entity::findSnapshot(const Time& n) const
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return nullptr;
        }

        auto c = std::make_shared<EntitySnapshot>(memoryParentPath, id().withTimestamp(n), processors, currentMode, currentExport);
        if (!util::checkIfFolderExists(mPath, c->getRelativePathForMode(currentMode)))
        {
            return nullptr;
        }

        return c;
    }

    std::shared_ptr<EntitySnapshot> Entity::findLatestSnapshot() const
    {
        Time bestMatch = Time::Invalid();
        auto f = [&](EntitySnapshot& e) {
            auto ts = e.id().timestamp;
            if (ts > bestMatch)
            {
                bestMatch = ts;
            }
        };

        forEachSnapshot(std::move(f));

        if (bestMatch == Time::Invalid())
        {
            return nullptr;
        }

        return std::make_shared<EntitySnapshot>(memoryParentPath, id().withTimestamp(bestMatch), processors, currentMode, currentExport);
    }

    std::shared_ptr<EntitySnapshot> Entity::findLatestSnapshotBefore(const Time& time) const
    {
        Time bestMatch = Time::Invalid();
        auto f = [&](EntitySnapshot& e) {
            auto ts = e.id().timestamp;
            if (ts < time && ts > bestMatch)
            {
                bestMatch = ts;
            }
        };

        forEachSnapshot(std::move(f));

        if (bestMatch == Time::Invalid())
        {
            return nullptr;
        }

        return std::make_shared<EntitySnapshot>(memoryParentPath, id().withTimestamp(bestMatch), processors, currentMode, currentExport);
    }

    std::shared_ptr<EntitySnapshot> Entity::findLatestSnapshotBeforeOrAt(const Time& time) const
    {
        Time bestMatch = Time::Invalid();
        auto f = [&](EntitySnapshot& e) {
            auto ts = e.id().timestamp;
            if (ts <= time && ts > bestMatch)
            {
                bestMatch = ts;
            }
        };

        forEachSnapshot(std::move(f));

        if (bestMatch == Time::Invalid())
        {
            return nullptr;
        }

        return std::make_shared<EntitySnapshot>(memoryParentPath, id().withTimestamp(bestMatch), processors, currentMode, currentExport);
    }

    std::shared_ptr<EntitySnapshot> Entity::findFirstSnapshotAfter(const Time& time) const
    {
        Time bestMatch { Duration::MicroSeconds(std::numeric_limits<long>::max()) };
        auto f = [&](EntitySnapshot& e)
        {
            auto ts = e.id().timestamp;
            if (ts > time && ts < bestMatch)
            {
                bestMatch = ts;
            }
        };

        forEachSnapshot(std::move(f));

        if (bestMatch == Time(Duration::MicroSeconds(std::numeric_limits<long>::max())))
        {
            return nullptr;
        }

        return std::make_shared<EntitySnapshot>(memoryParentPath, id().withTimestamp(bestMatch), processors, currentMode, currentExport);
    }

    std::shared_ptr<EntitySnapshot> Entity::findFirstSnapshotAfterOrAt(const Time& time) const
    {
        Time bestMatch { Duration::MicroSeconds(std::numeric_limits<long>::max()) };
        auto f = [&](EntitySnapshot& e)
        {
            auto ts = e.id().timestamp;
            if (ts >= time && ts < bestMatch)
            {
                bestMatch = ts;
            }
        };

        forEachSnapshot(std::move(f));

        if (bestMatch == Time(Duration::MicroSeconds(std::numeric_limits<long>::max())))
        {
            return nullptr;
        }

        return std::make_shared<EntitySnapshot>(memoryParentPath, id().withTimestamp(bestMatch), processors, currentMode, currentExport);
    }

    void Entity::_loadAllReferences(armem::wm::Entity& e)
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return;
        }

        e.id() = id();

        forEachSnapshot([&e](auto& x)
        {
            if (!e.hasSnapshot(x.id().timestamp)) // we only load the references if the snapshot is not existant
            {
                armem::wm::EntitySnapshot s;
                x.loadAllReferences(s);
                e.addSnapshot(s);
            }
        });
    }

    void Entity::_resolve(armem::wm::Entity& p)
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return;
        }

        p.forEachSnapshot([&](auto& e)
        {
            EntitySnapshot c(memoryParentPath, id().withTimestamp(e.id().timestamp), processors, currentMode, currentExport);
            if (util::checkIfFolderExists(mPath, c.getRelativePathForMode(currentMode)))
            {
                c.resolve(e);
            }
        });
    }

    void Entity::_store(const armem::wm::Entity& c)
    {
        if (id().entityName.empty())
        {
            ARMARX_WARNING << "During storage of segment '" << c.id().str() << "' I noticed that the corresponding LTM has no id set. " <<
                              "I set the id of the LTM to the same name, however this should not happen!";
            id().entityName = c.id().entityName;
        }

        auto defaultMode = MemoryEncodingMode::FILESYSTEM;

        auto defaultMPath = getMemoryBasePathForMode(defaultMode, 0);
        auto defaultRelPath = getRelativePathForMode(defaultMode);
        if (!util::checkIfFolderExists(defaultMPath, defaultRelPath))
        {
            ARMARX_WARNING << "The segment folder for segment '"+id().str()+"'was not created. I will create the folder by myself, however it seems like there is a bug in the ltm pipeline.";
            util::ensureFolderExists(defaultMPath, defaultRelPath, true);
        }

        c.forEachSnapshot([&](const auto& snap)
        {
            auto currentMaxExport = currentExport;
            auto encodingModeOfPast = currentMode;

            for (unsigned long i = 0; i < currentMaxExport; ++i)
            {
                MemoryEncodingMode mode = i == 0 ? defaultMode : encodingModeOfPast;
                auto mPath = getMemoryBasePathForMode(mode, i);

                EntitySnapshot c(memoryParentPath, id().withTimestamp(snap.id().timestamp), processors, mode, i);
                if (util::checkIfFolderExists(mPath, c.getRelativePathForMode(mode)))
                {
                    ARMARX_INFO << deactivateSpam() << "Ignoring to put an EntitiySnapshot into the LTM because the timestamp already existed (we assume snapshots are const and do not change outside the ltm).";
                    return;
                }
            }

            for (auto& f : processors->snapFilters)
            {
                if (f->enabled && !f->accept(snap))
                {
                    ARMARX_INFO << deactivateSpam() << "Ignoring to put an EntitiySnapshot into the LTM because it got filtered.";
                    return;
                }
            }

            EntitySnapshot c(memoryParentPath, id().withTimestamp(snap.id().timestamp), processors, encodingModeOfPast, currentMaxExport);
            util::ensureFolderExists(defaultMPath, c.getRelativePathForMode(defaultMode));

            statistics.recordedSnapshots++;

            c.store(snap);
        });
    }
}
