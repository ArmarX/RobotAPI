// Header
#include "EntitySnapshot.h"

// STD / STL
#include <iostream>
#include <fstream>

// ArmarX
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>

namespace armarx::armem::server::ltm::disk
{
    EntitySnapshot::EntitySnapshot(const std::filesystem::path& p, const MemoryID& id, const std::shared_ptr<Processors>& filters, const DiskMemoryItem::MemoryEncodingMode mode, const unsigned long e) :
        EntitySnapshotBase(id, filters),
        DiskMemoryItem(p, EscapeSegmentName(id.memoryName),
                       std::filesystem::path(EscapeSegmentName(id.coreSegmentName))
                       / EscapeSegmentName(id.providerSegmentName)
                       / EscapeSegmentName(id.entityName)
                       / std::to_string(id.timestamp.toSecondsSinceEpoch() / 3600 /* hours */)
                       / std::to_string(id.timestamp.toSecondsSinceEpoch()) / id.timestampStr()),
        currentMode(mode),
        currentExport(e)
    {
    }

    void EntitySnapshot::_loadAllReferences(armem::wm::EntitySnapshot& e) const
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return;
        }

        e.id() = id();

        for (unsigned int i = 0; i < 1000; ++i) // 1000 is max size for instances in a single timestamp
        {
            std::filesystem::path relIPath = relPath / std::to_string(i) / "";
            if (!util::checkIfFolderExists(mPath, relIPath))
            {
                break;
            }

            // add instance. Do not set data, since we only return references
            e.addInstance();
        }
    }

    void EntitySnapshot::_resolve(armem::wm::EntitySnapshot& e) const
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return;
        }

        auto& dictConverter = processors->defaultObjectConverter;

        // Get data from disk
        for (unsigned int i = 0; i < e.size(); ++i)
        {
            std::filesystem::path relIPath = relPath / std::to_string(i) / "";
            if (util::checkIfFolderExists(mPath, relIPath))
            {
                std::string dataFilename = (constantes::DATA_FILENAME + dictConverter.suffix);
                std::string metadataFilename = (constantes::METADATA_FILENAME + dictConverter.suffix);
                std::filesystem::path relDataPath = relIPath / dataFilename;
                std::filesystem::path relMetadataPath = relIPath / metadataFilename;

                auto& ins = e.getInstance(i);
                aron::data::DictPtr datadict = nullptr;
                aron::data::DictPtr metadatadict = nullptr;

                // get list of all files. This ensures that we only have to call fs::exists once for each file
                auto allFilesInIndexFolder = util::getAllFiles(mPath, relIPath);

                if (std::find(allFilesInIndexFolder.begin(), allFilesInIndexFolder.end(), dataFilename) != allFilesInIndexFolder.end())
                {
                    auto datafilecontent = util::readDataFromFile(mPath, relDataPath);
                    auto dataaron = dictConverter.convert(datafilecontent, "");
                    datadict = aron::data::Dict::DynamicCastAndCheck(dataaron);

                    // check for special members
                    for (const auto& [key, m] : datadict->getElements())
                    {
                        for (auto& [t, f] : processors->converters)
                        {
                            for (const auto& filename : allFilesInIndexFolder) // iterate over all files and search for matching ones
                            {
                                if (simox::alg::starts_with(filename, key) and simox::alg::ends_with(filename, f->suffix))
                                {
                                    std::filesystem::path relMemberPath = relIPath / filename;
                                    std::string mode = simox::alg::remove_suffix(simox::alg::remove_prefix(filename, key), f->suffix);

                                    auto memberfilecontent = util::readDataFromFile(mPath, relMemberPath);
                                    auto memberaron = f->convert(memberfilecontent, mode);
                                    datadict->setElement(key, memberaron);
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    ARMARX_ERROR << "Could not find the data file '" << relDataPath.string() << "'. Continuing without data.";
                }

                if (std::find(allFilesInIndexFolder.begin(), allFilesInIndexFolder.end(), metadataFilename) != allFilesInIndexFolder.end())
                {
                    auto metadatafilecontent = util::readDataFromFile(mPath, relMetadataPath);
                    auto metadataaron = dictConverter.convert(metadatafilecontent, "");
                    metadatadict = aron::data::Dict::DynamicCastAndCheck(metadataaron);
                }
                else
                {
                    ARMARX_ERROR << "Could not find the metadata file '" << relMetadataPath.string() << "'. Continuing without metadata.";
                }

                from_aron(metadatadict, datadict, ins);
            }
            else
            {
                ARMARX_WARNING << "Could not find the index segment folder for segment '" << e.id().str() << "'.";
            }
        }
    }

    void EntitySnapshot::_store(const armem::wm::EntitySnapshot& e)
    {
        //auto currentMaxExport = currentExport;
        //auto encodingModeOfPast = currentMode;

        if (id().timestampStr().empty())
        {
            ARMARX_WARNING << "During storage of segment '" << e.id().str() << "' I noticed that the corresponding LTM has no id set. " <<
                              "I set the id of the LTM to the same name, however this should not happen!";
            id().timestamp = e.id().timestamp;
        }

        auto defaultMode = MemoryEncodingMode::FILESYSTEM;

        auto& dictConverter = processors->defaultObjectConverter;

        auto defaultMPath = getMemoryBasePathForMode(defaultMode, 0);
        auto defaultRelPath = getRelativePathForMode(defaultMode);
        if (!util::checkIfFolderExists(defaultMPath, defaultRelPath))
        {
            ARMARX_WARNING << "The segment folder for segment '"+id().str()+"'was not created. I will create the folder by myself, however it seems like there is a bug in the ltm pipeline.";
            util::ensureFolderExists(defaultMPath, defaultRelPath, true);
        }

        for (unsigned int i = 0; i < e.size(); ++i)
        {
            std::filesystem::path defaultRelIPath = defaultRelPath / std::to_string(i) / "";

            // For performance reasons we skip to check whether the index folder already exists somewhere.
            // We already check if the ts exists on entity level.

            if (!util::checkIfFolderExists(defaultMPath, defaultRelIPath))
            {
                util::ensureFolderExists(defaultMPath, defaultRelIPath);

                std::filesystem::path relDataPath = defaultRelIPath / (constantes::DATA_FILENAME + dictConverter.suffix);
                std::filesystem::path relMetadataPath = defaultRelIPath / (constantes::METADATA_FILENAME + dictConverter.suffix);

                auto& ins = e.getInstance(i);

                // data
                auto dataAron = std::make_shared<aron::data::Dict>();
                auto metadataAron = std::make_shared<aron::data::Dict>();
                to_aron(metadataAron, dataAron, ins);

                // check special members for special extractions
                for (auto& x : processors->extractors)
                {
                    if (!x->enabled) continue;

                    const auto& t = x->identifier;

                    Converter* conv = nullptr; // find suitable converter
                    for (const auto& [ct, c] : processors->converters)
                    {
                        if (!c->enabled) continue;
                        if (t != ct) continue;
                        conv = c;
                    }

                    if (conv)
                    {
                        auto dataExt = x->extract(dataAron);

                        for (const auto& [memberName, var] : dataExt.extraction)
                        {
                            ARMARX_CHECK_NOT_NULL(var);

                            auto [memberDataVec, memberDataModeSuffix] = conv->convert(var);
                            std::filesystem::path relMemberPath = defaultRelIPath / (memberName + memberDataModeSuffix + conv->suffix);

                            util::writeDataToFileRepeated(defaultMPath, relMemberPath, memberDataVec);
                        }

                        dataAron = dataExt.dataWithoutExtraction;
                    }
                    // else we could not convert the extracted data so it makes no sense to extract it at all...
                }

                // convert dict and metadata
                auto [dataVec, dataVecModeSuffix] = dictConverter.convert(dataAron);
                auto [metadataVec, metadataVecModeSuffix] = dictConverter.convert(metadataAron);
                ARMARX_CHECK_EMPTY(dataVecModeSuffix);
                ARMARX_CHECK_EMPTY(metadataVecModeSuffix);

                statistics.recordedInstances++;

                util::writeDataToFileRepeated(defaultMPath, relDataPath, dataVec);
                util::writeDataToFileRepeated(defaultMPath, relMetadataPath, metadataVec);
            }
            // Ignore if the full index already exists. Actually this should not happen since the existence of the ts folder is checked on entity level
        }
    }
}
