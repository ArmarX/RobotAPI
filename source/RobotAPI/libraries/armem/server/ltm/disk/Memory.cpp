#include "Memory.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

namespace armarx::armem::server::ltm::disk
{
    void Memory::configure(const nlohmann::json& json)
    {
        Base::configure(json);
        if (json.find("Memory.memoryParentPath") != json.end())
        {
            memoryParentPath = std::filesystem::path(json.at("Memory.memoryParentPath"));
        }
        if (json.find("Memory.sizeToCompressDataInMegaBytes") != json.end())
        {
            sizeToCompressDataInMegaBytes = json.at("Memory.sizeToCompressDataInMegaBytes");
        }
    }

    Memory::Memory() :
        Memory(std::filesystem::path("/tmp/MemoryExport"), "Test")
    {
    }

    Memory::Memory(const std::filesystem::path& p, const std::string& memoryName /* UNESCAPED */, const MemoryEncodingMode defaultEncodingMode) :
        Base(MemoryID(memoryName, "")),
        DiskMemoryItem(p, EscapeSegmentName(memoryName), ""),
        defaultExportEncodingMode(defaultEncodingMode)
    {
    }

    void Memory::setMemoryID(const MemoryID &id)
    {
        Base::setMemoryID(id);
        escapedMemoryName = EscapeSegmentName(id.memoryName);
    }
    void Memory::setMemoryID(const MemoryID& id, const std::string& componentNameFallback)
    {
        MemoryID _id = id;
        if (id.memoryName.empty())
        {
            ARMARX_WARNING << "ATTENTION: A memory id passed to some LTM is empty. Either you used the wrong setter (always use setMemoryName(..) before onInitComponent()) or you emptied the scenario parameter. Using a the component name as fallback.";
            _id.memoryName = componentNameFallback;
        }

        setMemoryID(_id);
    }

    void Memory::init()
    {
        Base::init();

        // Discover how many exports already exists
        for (unsigned long i = 1; i < 10000; ++i)
        {
            std::filesystem::path exportPath = getMemoryBasePathForMode(defaultExportEncodingMode, i);
            if (!std::filesystem::exists(exportPath))
            {
                break;
            }
            maxExportIndex = i;
        }
    }

    bool Memory::forEachCoreSegment(std::function<void(CoreSegment&)>&& func) const
    {
        for (unsigned long i = 0; i <= maxExportIndex; ++i)
        {
            MemoryEncodingMode mode = i == 0 ? MemoryEncodingMode::FILESYSTEM : defaultExportEncodingMode;
            auto mPath = getMemoryBasePathForMode(mode, i);
            if (util::checkIfBasePathExists(mPath))
            {
                for (const auto& subdirName : util::getAllDirectories(mPath, getRelativePathForMode(mode)))
                {
                    std::string segmentName = UnescapeSegmentName(subdirName);
                    CoreSegment c(memoryParentPath, id().withCoreSegmentName(segmentName), processors, mode, i);
                    func(c);
                }
            }
        }

        return true;
    }

    std::shared_ptr<CoreSegment> Memory::findCoreSegment(const std::string& coreSegmentName) const
    {
        for (unsigned long i = 0; i <= maxExportIndex; ++i)
        {
            MemoryEncodingMode mode = i == 0 ? MemoryEncodingMode::FILESYSTEM : defaultExportEncodingMode;

            auto mPath = getMemoryBasePathForMode(mode, i);
            if (!util::checkIfBasePathExists(mPath))
            {
                continue;
            }

            auto c = std::make_shared<CoreSegment>(memoryParentPath, id().withCoreSegmentName(coreSegmentName), processors, mode, i);
            if (!util::checkIfFolderExists(mPath, c->getRelativePathForMode(mode)))
            {
                continue;
            }

            return c;
        }
        return nullptr;
    }

    void Memory::_loadAllReferences(armem::wm::Memory& m)
    {
        m.id() = id();

        forEachCoreSegment([&m](auto& x) {
            armem::wm::CoreSegment s;
            x.loadAllReferences(s);
            m.addCoreSegment(s);
        });
    }

    void Memory::_resolve(armem::wm::Memory& m)
    {
        std::lock_guard l(ltm_mutex); // we cannot load a memory multiple times simultaneously

        for (unsigned long i = 0; i <= maxExportIndex; ++i)
        {
            MemoryEncodingMode mode = i == 0 ? MemoryEncodingMode::FILESYSTEM : defaultExportEncodingMode;

            auto mPath = getMemoryBasePathForMode(mode, i);
            if (!util::checkIfBasePathExists(mPath))
            {
                continue;
            }

            m.forEachCoreSegment([&](auto& e)
            {
                CoreSegment c(memoryParentPath, id().withCoreSegmentName(e.id().coreSegmentName), processors, mode, i);
                if (util::checkIfFolderExists(mPath, c.getRelativePathForMode(mode)))
                {
                    c.resolve(e);
                }
            });
        }
    }

    void Memory::_directlyStore(const armem::wm::Memory& memory)
    {
        std::lock_guard l(ltm_mutex); // we cannot store a memory multiple times simultaneously

        MemoryEncodingMode defaultMode = MemoryEncodingMode::FILESYSTEM;
        MemoryEncodingMode encodeModeOfPast = defaultExportEncodingMode;
        // Storage will always be in filesystem mode!
        // Somehow, minizip was not able to write data to images. It always created a folder named xyz.png without any data in it...
        // Another problem is that storing data directly in compressed format will require a lot of time when the compressed file is big (>20MB)

        if (id().memoryName.empty())
        {
            ARMARX_WARNING << "During storage of memory '" << memory.id().str() << "' I noticed that the corresponding LTM has no id set. " <<
                              "I set the id of the LTM to the same name, however this should not happen!";
            setMemoryID(memory.id());
        }

        auto defaultMPath = getMemoryBasePathForMode(defaultMode, 0);

        // Check if we have to compress the memory!
        // See above mentioned issues with directly compressing the data. Therefore we store data in plain text and compress from time to time
        // using system calls. Also increase the index of all old exports
        auto size = filesystem::util::getSizeOfDirectory(defaultMPath);
        //std::cout << "Current maxExportIndex is: " << maxExportIndex << std::endl;
        if (size >= (sizeToCompressDataInMegaBytes * 1024 * 1024))
        {
            ARMARX_INFO << "Compressen of memory " + id().memoryName + " needed because the size of last export is " + std::to_string(size / 1024.f / 1024.f) + " (>= " + std::to_string(sizeToCompressDataInMegaBytes) + ")";

            // increase index of old memories
            for (unsigned long i = maxExportIndex; i >= 1; --i)
            {
                ARMARX_INFO << "Increasing the index of old compressed memory " + id().memoryName + " (" + std::to_string(i) + " to " + std::to_string(i+1) + ")";
                auto exportPath = getMemoryBasePathForMode(encodeModeOfPast, i);
                auto newExportPath = getMemoryBasePathForMode(encodeModeOfPast, i+1);
                std::string moveCommand = "mv " + exportPath.string() + " " + newExportPath.string();
                //std::cout << "Exec command: " << moveCommand << std::endl;
                int ret = system(moveCommand.c_str());
                (void) ret;
            }

            // zip away current export
            ARMARX_INFO << "Compressing the last export of " + id().memoryName;
            auto newExportPath = getMemoryBasePathForMode(encodeModeOfPast, 1); // 1 will be the new export
            std::string zipCommand = "cd " + memoryParentPath.string() + " && zip -r " + newExportPath.string() + " " + escapedMemoryName;
            //std::cout << "Exec command: " << zipCommand << std::endl;
            int ret = system(zipCommand.c_str());
            (void) ret;

            // remove unzipped memory export
            ARMARX_INFO << "Removing the last export of " + id().memoryName;
            std::string rmCommand = "rm -r " + defaultMPath.string();
            ret = system(rmCommand.c_str());
            (void) ret;

            maxExportIndex++;
        }

        util::ensureBasePathExists(defaultMPath, true); // create if not exists

        memory.forEachCoreSegment([&](const auto& core)
        {
            CoreSegment c(memoryParentPath, id().withCoreSegmentName(core.id().coreSegmentName), processors, encodeModeOfPast, 0 /* how far to look back in past on enity level. For full lookup use maxExportIndex. */);
            util::ensureFolderExists(defaultMPath, c.getRelativePathForMode(defaultMode), true); // create subfolder

            statistics.recordedCoreSegments++;

            // 1. store type
            c.storeType(core);

            // 2. store data
            c.store(core);
        });
    }
}
