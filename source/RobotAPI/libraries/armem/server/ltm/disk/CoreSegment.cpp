#include "CoreSegment.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>


namespace armarx::armem::server::ltm::disk
{
    CoreSegment::CoreSegment(const std::filesystem::path& p, const MemoryID& id, const std::shared_ptr<Processors>& filters, const DiskMemoryItem::MemoryEncodingMode mode, const unsigned long e) :
        CoreSegmentBase(id, filters),
        DiskMemoryItem(p, EscapeSegmentName(id.memoryName), std::filesystem::path(EscapeSegmentName(id.coreSegmentName))),
        currentMode(mode),
        currentExport(e)
    {
    }

    bool CoreSegment::forEachProviderSegment(std::function<void(ProviderSegment&)>&& func) const
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return false;
        }

        for (const auto& subdirName : util::getAllDirectories(mPath, relPath))
        {
            std::string segmentName = UnescapeSegmentName(subdirName);
            ProviderSegment c(memoryParentPath, id().withProviderSegmentName(segmentName), processors, currentMode, currentExport);
            func(c);
        }
        return true;
    }

    std::shared_ptr<ProviderSegment> CoreSegment::findProviderSegment(const std::string& providerSegmentName) const
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return nullptr;
        }

        auto c = std::make_shared<ProviderSegment>(memoryParentPath, id().withProviderSegmentName(providerSegmentName), processors, currentMode, currentExport);
        if (!util::checkIfFolderExists(mPath, c->getRelativePathForMode(currentMode)))
        {
            return nullptr;
        }

        return c;
    }

    void CoreSegment::_loadAllReferences(armem::wm::CoreSegment& e)
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return;
        }

        e.id() = id();

        auto& conv = processors->defaultTypeConverter;
        auto relTPath = relPath / (constantes::TYPE_FILENAME + conv.suffix);

        if (util::checkIfFileExists(mPath, relTPath))
        {
            auto typeFileContent = util::readDataFromFile(mPath, relTPath);
            auto typeAron = conv.convert(typeFileContent, "");
            e.aronType() = aron::type::Object::DynamicCastAndCheck(typeAron);
        }

        forEachProviderSegment([&e](auto& x) {
            armem::wm::ProviderSegment s;
            x.loadAllReferences(s);
            e.addProviderSegment(s);
        });
    }

    void CoreSegment::_resolve(armem::wm::CoreSegment& c)
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return;
        }

        c.forEachProviderSegment([&](auto& e)
        {
            ProviderSegment c(memoryParentPath, id().withProviderSegmentName(e.id().providerSegmentName), processors, currentMode, currentExport);
            if (util::checkIfFolderExists(mPath, c.getRelativePathForMode(currentMode)))
            {
                c.resolve(e);
            }
        });
    }

    void CoreSegment::_store(const armem::wm::CoreSegment& c)
    {
        auto currentMaxExport = currentExport;
        auto encodingModeOfPast = currentMode;

        if (id().coreSegmentName.empty())
        {
            ARMARX_WARNING << "During storage of segment '" << c.id().str() << "' I noticed that the corresponding LTM has no id set. " <<
                              "I set the id of the LTM to the same name, however this should not happen!";
            id().coreSegmentName = c.id().coreSegmentName;
        }

        auto defaultMode = MemoryEncodingMode::FILESYSTEM;

        auto defaultMPath = getMemoryBasePathForMode(defaultMode, 0);
        auto defaultRelPath = getRelativePathForMode(defaultMode);
        if (!util::checkIfFolderExists(defaultMPath, defaultRelPath))
        {
            ARMARX_WARNING << "The segment folder for segment '"+id().str()+"'was not created. I will create the folder by myself, however it seems like there is a bug in the ltm pipeline.";
            util::ensureFolderExists(defaultMPath, defaultRelPath, true);
        }

        c.forEachProviderSegment([&](const auto& prov)
        {
            ProviderSegment c(memoryParentPath, id().withProviderSegmentName(prov.id().providerSegmentName), processors, encodingModeOfPast, currentMaxExport);
            util::ensureFolderExists(defaultMPath, c.getRelativePathForMode(defaultMode), true);

            statistics.recordedProviderSegments++;

            c.storeType(prov);
            c.store(prov);
        });
    }

    void CoreSegment::_storeType(const armem::wm::CoreSegment& c)
    {
        if (id().coreSegmentName.empty())
        {
            ARMARX_WARNING << "During storage of segment '" << c.id().str() << "' I noticed that the corresponding LTM has no id set. " <<
                              "I set the id of the LTM to the same name, however this should not happen!";
            id().coreSegmentName = c.id().coreSegmentName;
        }

        auto defaultMode = MemoryEncodingMode::FILESYSTEM;

        auto defaultMPath = getMemoryBasePathForMode(defaultMode, 0);
        auto defaultRelPath = getRelativePathForMode(defaultMode);
        if (!util::checkIfFolderExists(defaultMPath, defaultRelPath))
        {
            ARMARX_WARNING << "The segment folder for segment '"+id().str()+"'was not created. I will create the folder by myself, however it seems like there is a bug in the ltm pipeline.";
            util::ensureFolderExists(defaultMPath, defaultRelPath, true);
        }

        if(c.hasAronType())
        {
            auto& conv = processors->defaultTypeConverter;

            auto [vec, modeSuffix] = conv.convert(c.aronType());
            ARMARX_CHECK_EMPTY(modeSuffix);
            std::filesystem::path relTypePath = defaultRelPath / (constantes::TYPE_FILENAME + conv.suffix);
            util::writeDataToFileRepeated(defaultMPath, relTypePath, vec);
        }
    }
}
