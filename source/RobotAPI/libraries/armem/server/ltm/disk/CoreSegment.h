#pragma once

#include <filesystem>

// Base Class
#include "../base/detail/CoreSegmentBase.h"
#include "detail/DiskStorage.h"

#include "ProviderSegment.h"

namespace armarx::armem::server::ltm::disk
{
    class CoreSegment :
            public CoreSegmentBase<ProviderSegment>,
            public DiskMemoryItem
    {
    public:

        CoreSegment(const std::filesystem::path& parentPath, const MemoryID& id, const std::shared_ptr<Processors>& p, const DiskMemoryItem::MemoryEncodingMode mode, const unsigned long e);

        bool forEachProviderSegment(std::function<void(ProviderSegment&)>&& func) const override;

        std::shared_ptr<ProviderSegment> findProviderSegment(const std::string&) const override;

    protected:
        void _loadAllReferences(armem::wm::CoreSegment&) override;
        void _resolve(armem::wm::CoreSegment&) override;
        void _store(const armem::wm::CoreSegment&) override;
        void _storeType(const armem::wm::CoreSegment&) override;

    private:
        MemoryEncodingMode currentMode;
        unsigned long currentExport;
    };

} // namespace armarx::armem::server::ltm::disk
