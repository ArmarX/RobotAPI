// Header
#include "ProviderSegment.h"

// ArmarX
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>


namespace armarx::armem::server::ltm::disk
{
    ProviderSegment::ProviderSegment(const std::filesystem::path& p, const MemoryID& id, const std::shared_ptr<Processors>& filters, const DiskMemoryItem::MemoryEncodingMode mode, const unsigned long e) :
        ProviderSegmentBase(id, filters),
        DiskMemoryItem(p, EscapeSegmentName(id.memoryName), std::filesystem::path(EscapeSegmentName(id.coreSegmentName)) / EscapeSegmentName(id.providerSegmentName)),
        currentMode(mode),
        currentExport(e)
    {
    }

    bool ProviderSegment::forEachEntity(std::function<void(Entity&)>&& func) const
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return false;
        }

        for (const auto& subdirName : util::getAllDirectories(mPath, relPath))
        {
            std::string segmentName = UnescapeSegmentName(subdirName);
            Entity c(memoryParentPath, id().withEntityName(subdirName), processors, currentMode, currentExport);
            func(c);
        }
        return true;
    }

    std::shared_ptr<Entity> ProviderSegment::findEntity(const std::string& entityName) const
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return nullptr;
        }

        auto c = std::make_shared<Entity>(memoryParentPath, id().withEntityName(entityName), processors, currentMode, currentExport);
        if (!util::checkIfFolderExists(mPath, c->getRelativePathForMode(currentMode)))
        {
            return nullptr;
        }

        return c;
    }

    void ProviderSegment::_loadAllReferences(armem::wm::ProviderSegment& e)
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return;
        }

        e.id() = id();

        auto& conv = processors->defaultTypeConverter;
        auto relTPath = relPath / (constantes::TYPE_FILENAME + conv.suffix);

        if (util::checkIfFileExists(mPath, relTPath))
        {
            auto typeFileContent = util::readDataFromFile(mPath, relTPath);
            auto typeAron = conv.convert(typeFileContent, "");
            e.aronType() = aron::type::Object::DynamicCastAndCheck(typeAron);
        }

        forEachEntity([&e](auto& x) {
            armem::wm::Entity s;
            x.loadAllReferences(s);
            e.addEntity(s);
        });
    }

    void ProviderSegment::_resolve(armem::wm::ProviderSegment& p)
    {
        auto mPath = getMemoryBasePathForMode(currentMode, currentExport);
        auto relPath = getRelativePathForMode(currentMode);
        if (!util::checkIfBasePathExists(mPath) || !util::checkIfFolderExists(mPath, relPath))
        {
            return;
        }

        p.forEachEntity([&](auto& e)
        {
            Entity c(memoryParentPath, id().withEntityName(e.id().entityName), processors, currentMode, currentExport);
            if (util::checkIfFolderExists(mPath, c.getRelativePathForMode(currentMode)))
            {
                c.resolve(e);
            }
        });
    }

    void ProviderSegment::_store(const armem::wm::ProviderSegment& p)
    {
        auto currentMaxExport = currentExport;
        auto encodingModeOfPast = currentMode;

        if (id().providerSegmentName.empty())
        {
            ARMARX_WARNING << "During storage of segment '" << p.id().str() << "' I noticed that the corresponding LTM has no id set. " <<
                              "I set the id of the LTM to the same name, however this should not happen!";
            id().providerSegmentName = p.id().providerSegmentName;
        }

        auto defaultMode = MemoryEncodingMode::FILESYSTEM;

        auto defaultMPath = getMemoryBasePathForMode(defaultMode, 0);
        auto defaultRelPath = getRelativePathForMode(defaultMode);
        if (!util::checkIfFolderExists(defaultMPath, defaultRelPath))
        {
            ARMARX_WARNING << "The segment folder for segment '"+id().str()+"'was not created. I will create the folder by myself, however it seems like there is a bug in the ltm pipeline.";
            util::ensureFolderExists(defaultMPath, defaultRelPath, true);
        }

        p.forEachEntity([&](const auto& e)
        {
            Entity c(memoryParentPath, id().withEntityName(e.id().entityName), processors, encodingModeOfPast, currentMaxExport);
            util::ensureFolderExists(defaultMPath, c.getRelativePathForMode(defaultMode), true);

            statistics.recordedEntities++;

            c.store(e);
        });
    }

    void ProviderSegment::_storeType(const armem::wm::ProviderSegment& p)
    {
        if (id().providerSegmentName.empty())
        {
            ARMARX_WARNING << "During storage of segment '" << p.id().str() << "' I noticed that the corresponding LTM has no id set. " <<
                              "I set the id of the LTM to the same name, however this should not happen!";
            id().providerSegmentName = p.id().providerSegmentName;
        }

        auto defaultMode = MemoryEncodingMode::FILESYSTEM;

        auto defaultMPath = getMemoryBasePathForMode(defaultMode, 0);
        auto defaultRelPath = getRelativePathForMode(defaultMode);
        if (!util::checkIfFolderExists(defaultMPath, defaultRelPath))
        {
            ARMARX_WARNING << "The segment folder for segment '"+id().str()+"'was not created. I will create the folder by myself, however it seems like there is a bug in the ltm pipeline.";
            util::ensureFolderExists(defaultMPath, defaultRelPath, true);
        }

        if(p.hasAronType())
        {
            auto& conv = processors->defaultTypeConverter;

            auto [vec, modeSuffix] = conv.convert(p.aronType());
            ARMARX_CHECK_EMPTY(modeSuffix);
            std::filesystem::path relTypePath = defaultRelPath / (constantes::TYPE_FILENAME + conv.suffix);
            util::writeDataToFileRepeated(defaultMPath, relTypePath, vec);
        }
    }
}
