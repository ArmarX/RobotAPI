#pragma once

#include <filesystem>

// Base Class
#include <RobotAPI/libraries/armem/server/ltm/base/detail/BufferedMemoryBase.h>
#include "detail/DiskStorage.h"

// Segmnet Type
#include "CoreSegment.h"


namespace armarx::armem::server::ltm::disk
{
    /// @brief A memory storing data in mongodb (needs 'armarx memory start' to start the mongod instance)
    class Memory :
            public BufferedMemoryBase<CoreSegment>,
            public DiskMemoryItem
    {
    public:
        using Base = BufferedMemoryBase<CoreSegment>;

        Memory();
        Memory(const std::filesystem::path&, const std::string&, const MemoryEncodingMode defaultEncodingMode = MemoryEncodingMode::MINIZIP);

        void init() final;
        void setMemoryID(const MemoryID& id) final;
        void setMemoryID(const MemoryID& id, const std::string& fallback);

        void configure(const nlohmann::json& config) final;

        bool forEachCoreSegment(std::function<void(CoreSegment&)>&& func) const final;
        std::shared_ptr<CoreSegment> findCoreSegment(const std::string& coreSegmentName) const final;

    protected:
        void _loadAllReferences(armem::wm::Memory&) final;
        void _resolve(armem::wm::Memory&) final;
        void _directlyStore(const armem::wm::Memory&) final;

    private:
        MemoryEncodingMode defaultExportEncodingMode = MemoryEncodingMode::MINIZIP;
        unsigned long maxExportIndex = 0;
        unsigned long sizeToCompressDataInMegaBytes = long(1) * 1024; // 1GB

    public:
        static const int DEPTH_TO_DATA_FILES = 7; // from memory folder = 1 (cseg) + 1 (pseg) + 1 (ent) + 3 (snap) + 1 (inst)
    };
} // namespace armarx::armem::server::ltm::disk
