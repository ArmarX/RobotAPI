// Header
#include "DiskStorage.h"

// Simox
#include <SimoxUtility/algorithm/string.h>

// ArmarX
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/LocalException.h>

namespace armarx::armem::server::ltm::disk
{
    //const std::string DiskMemoryItem::Prefix = "_";
    //const std::string DiskMemoryItem::PrefixEscaped = "__";

    const std::string DiskMemoryItem::MEMORY_EXPORT_SUFFIX = "_";
    const std::map<std::string, std::string> DiskMemoryItem::EscapeTable = {
        {"/", "|"}
    };

    DiskMemoryItem::DiskMemoryItem(const std::filesystem::path& memoryParentPath, const std::string& memoryName, const std::filesystem::path relativePath) :
        memoryParentPath(memoryParentPath),
        escapedMemoryName(memoryName),
        relativeSegmentPath(relativePath)
    {
    }

    std::filesystem::path DiskMemoryItem::getMemoryBasePathForMode(const MemoryEncodingMode m, const unsigned long e) const
    {
        std::string escapedMemoryNameSuffixed = escapedMemoryName;
        if (e > 0)
        {
            escapedMemoryNameSuffixed = escapedMemoryName + MEMORY_EXPORT_SUFFIX + std::to_string(e);
        }

        switch(m)
        {
        case MemoryEncodingMode::FILESYSTEM:
            return memoryParentPath / escapedMemoryNameSuffixed;
        case MemoryEncodingMode::MINIZIP:
            return memoryParentPath / (escapedMemoryNameSuffixed + minizip::util::MINIZIP_SUFFIX);
        }
        return "";
    }

    std::filesystem::path DiskMemoryItem::getRelativePathForMode(const MemoryEncodingMode m) const
    {
        switch(m)  // ensure a tailing "/"
        {
        case MemoryEncodingMode::FILESYSTEM:
            return relativeSegmentPath / "";
        case MemoryEncodingMode::MINIZIP:
            return std::filesystem::path(escapedMemoryName) / relativeSegmentPath / "";
        }
        return "";
    }

    std::string DiskMemoryItem::EscapeSegmentName(const std::string& segmentName)
    {
        std::string ret = segmentName;
        //simox::alg::replace_all(ret, Prefix, PrefixEscaped);
        for (const auto& [s, r] : EscapeTable)
        {
            ret = simox::alg::replace_all(ret, s, r);
        }
        return ret;
    }

    std::string DiskMemoryItem::UnescapeSegmentName(const std::string& escapedName)
    {
        std::string ret = escapedName;
        for (const auto& [s, r] : EscapeTable) // Here we assume that noone uses the replaced char usually in the segment name... TODO
        {
            ret = simox::alg::replace_all(ret, r, s);
        }
        return ret;
    }
}
