#pragma once

#include "../../../../../core/error.h"

#include "filesystem_util.h"
#include "minizip_util.h"

namespace armarx::armem::server::ltm::disk
{
    namespace constantes
    {
        const std::string TYPE_FILENAME = "type.aron";
        const std::string DATA_FILENAME = "data.aron";
        const std::string METADATA_FILENAME = "metadata.aron";
    }

    namespace util
    {
        // check whether a string is a number (e.g. timestamp folders)
        bool isNumber(const std::string& s);

        bool checkIfBasePathExists(const std::filesystem::path& mPath);

        void ensureBasePathExists(const std::filesystem::path& mPath, bool createIfNotExistent = true);

        bool checkIfFolderExists(const std::filesystem::path& mPath, const std::filesystem::path& p);

        void ensureFolderExists(const std::filesystem::path& mPath, const std::filesystem::path& p, bool createIfNotExistent = true);

        bool checkIfFileExists(const std::filesystem::path& mPath, const std::filesystem::path& p);

        void ensureFileExists(const std::filesystem::path& mPath, const std::filesystem::path& p);

        void writeDataToFile(const std::filesystem::path& mPath, const std::filesystem::path& p, const std::vector<unsigned char>& data);

        void writeDataToFileRepeated(const std::filesystem::path& mPath, const std::filesystem::path& p, const std::vector<unsigned char>& data, const unsigned int maxTries = 100, const unsigned int sleepTimeMs = 10);

        std::vector<unsigned char> readDataFromFile(const std::filesystem::path& mPath, const std::filesystem::path& p);

        std::vector<std::string> getAllDirectories(const std::filesystem::path& mPath, const std::filesystem::path& p);

        std::vector<std::string> getAllFiles(const std::filesystem::path& mPath, const std::filesystem::path& p);
    }
} // namespace armarx::armem::server::ltm::disk
