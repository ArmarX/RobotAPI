#pragma once

#include <filesystem>
#include <map>
#include <string>

// util
#include "util/util.h"

namespace armarx::armem::server::ltm::disk
{
    class DiskMemoryItem
    {
    public:
        enum class MemoryEncodingMode
        {
            FILESYSTEM = 0,
            MINIZIP = 1
        };

        DiskMemoryItem() = default;
        DiskMemoryItem(const std::filesystem::path& memoryParentPath, const std::string& escapedMemoryName, const std::filesystem::path relativePath);
        virtual ~DiskMemoryItem() = default;

        std::filesystem::path getMemoryBasePathForMode(const MemoryEncodingMode m, const unsigned long e) const;
        std::filesystem::path getRelativePathForMode(const MemoryEncodingMode m) const;

    protected:
        static std::string EscapeSegmentName(const std::string& segmentName);
        static std::string UnescapeSegmentName(const std::string& escapedName);

    protected:
        //static const std::string PREFIX;
        //static const std::string PREFIX_ESCAPED;
        static const std::string MEMORY_EXPORT_SUFFIX;
        static const std::map<std::string, std::string> EscapeTable;

        std::filesystem::path memoryParentPath;
        std::string escapedMemoryName;
        std::filesystem::path relativeSegmentPath;
    };
} // namespace armarx::armem::server::ltm::disk
