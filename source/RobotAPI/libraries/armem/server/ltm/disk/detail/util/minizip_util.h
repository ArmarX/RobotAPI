#pragma once

// STD / STL
#include <filesystem>
#include <iostream>

#include <minizip/zip.h>
#include <minizip/unzip.h>

#include <SimoxUtility/algorithm/string.h>

#include "../../../../../core/error.h"

namespace armarx::armem::server::ltm::disk
{
    namespace minizip::util
    {
        const std::string MINIZIP_SUFFIX = ".zip";
        const int MINIZIP_CASE_SENSITIVITY = 1;

        bool checkZipFile(const std::filesystem::path& pathToZip);

        zipFile ensureZipFile(const std::filesystem::path& pathToZip, bool createIfNotExistent = true);

        unzFile getUnzFile(const std::filesystem::path& pathToZip);

        bool checkIfElementInZipExists(const std::filesystem::path& pathToZip, const std::filesystem::path& p);

        void ensureElementInZipExists(const std::filesystem::path& pathToZip, const std::filesystem::path& p, bool createIfNotExistent = true);

        void ensureFolderInZipExists(const std::filesystem::path& pathToZip, const std::filesystem::path& p, bool createIfNotExistent = true);

        void ensureFileInZipExists(const std::filesystem::path& pathToZip, const std::filesystem::path& p);

        bool checkIfFolderInZipExists(const std::filesystem::path& pathToZip, const std::filesystem::path& p);

        bool checkIfFileInZipExists(const std::filesystem::path& pathToZip, const std::filesystem::path& p);

        void writeDataInFileInZipFile(const std::filesystem::path& pathToZip, const std::filesystem::path& p, const std::vector<unsigned char>& data);

        std::vector<unsigned char> readDataFromFileInZipFile(const std::filesystem::path& pathToZip, const std::filesystem::path& p);

        std::vector<std::string> getAllDirectoriesInZipFile(const std::filesystem::path& pathToZip, const std::filesystem::path p);

        std::vector<std::string> getAllFilesInZipFile(const std::filesystem::path& pathToZip, const std::filesystem::path p);
    }
} // namespace armarx::armem::server::ltm::disk
