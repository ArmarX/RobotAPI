#include "minizip_util.h"

namespace armarx::armem::server::ltm::disk
{
    namespace minizip::util
    {
        bool checkZipFile(const std::filesystem::path& pathToZip)
        {
            bool ret = false;
            zipFile z = NULL;
            if (std::filesystem::exists(pathToZip))
            {
                if (std::filesystem::is_regular_file(pathToZip) && pathToZip.extension() == MINIZIP_SUFFIX)
                {
                    z = zipOpen64(pathToZip.string().c_str(), APPEND_STATUS_ADDINZIP);
                    ret = (bool) z;
                    zipClose(z, NULL);
                }
            }
            return ret;
        }

        zipFile ensureZipFile(const std::filesystem::path& pathToZip, bool createIfNotExistent)
        {
            zipFile z = NULL;
            if (!checkZipFile(pathToZip))
            {
                if (createIfNotExistent)
                {
                    z = zipOpen64(pathToZip.string().c_str(), APPEND_STATUS_CREATE);
                }
                else
                {
                    throw error::ArMemError("Could not find zip file: " + pathToZip.string());
                }
            }
            else
            {
                z = zipOpen64(pathToZip.string().c_str(), APPEND_STATUS_ADDINZIP);
            }

            if (!z)
            {
                throw error::ArMemError("Unknown error occured during opening zip file: " + pathToZip.string());
            }
            return z;
        }

        unzFile getUnzFile(const std::filesystem::path& pathToZip)
        {
            unzFile z = NULL;
            if (std::filesystem::exists(pathToZip))
            {
                if (std::filesystem::is_regular_file(pathToZip) && pathToZip.extension() == MINIZIP_SUFFIX)
                {
                    z = unzOpen64(pathToZip.string().c_str());
                }
                else
                {
                    throw error::ArMemError("Existing file is not a zip file: " + pathToZip.string());
                }
            }

            // if z is NULL then the zip file might be empty

            return z;
        }

        bool checkIfElementInZipExists(const std::filesystem::path& pathToZip, const std::filesystem::path& p)
        {
            if (!checkZipFile(pathToZip)) return false;
            auto uzf = getUnzFile(pathToZip);

            bool ret = (unzLocateFile(uzf, p.string().c_str(), MINIZIP_CASE_SENSITIVITY) == UNZ_OK);

            unzClose(uzf);
            return ret;
        }

        void ensureElementInZipExists(const std::filesystem::path& pathToZip, const std::filesystem::path& p, bool createIfNotExistent)
        {
            auto zf = ensureZipFile(pathToZip, createIfNotExistent);
            auto uzf = getUnzFile(pathToZip);

            if (auto r = unzLocateFile(uzf, p.string().c_str(), MINIZIP_CASE_SENSITIVITY); r != UNZ_OK)
            {
                if (createIfNotExistent)
                {
                    zip_fileinfo zfi;
                    zipOpenNewFileInZip64(zf, p.string().c_str(), &zfi, NULL, 0, NULL, 0, NULL, Z_DEFLATED, Z_DEFAULT_COMPRESSION, 1);
                    zipCloseFileInZip(zf);
                }
                else
                {
                    unzClose(uzf);
                    zipClose(zf, NULL);
                    throw error::ArMemError("Could not find element '" + p.string() + "' in zip file: " + pathToZip.string());
                }
            }
            // else folder exists

            unzClose(uzf);
            zipClose(zf, NULL);
        }

        void ensureFolderInZipExists(const std::filesystem::path& pathToZip, const std::filesystem::path& p, bool createIfNotExistent)
        {
            if (!p.filename().empty())
            {
                throw error::ArMemError("The specified path is not a folder (it needs tailing /): " + p.string());
            }

            ensureElementInZipExists(pathToZip, p, createIfNotExistent);
        }

        void ensureFileInZipExists(const std::filesystem::path& pathToZip, const std::filesystem::path& p)
        {
            if (p.filename().empty())
            {
                throw error::ArMemError("The specified path is not a file (it needs a filename): " + p.string());
            }

            ensureElementInZipExists(pathToZip, p, true);
        }

        bool checkIfFolderInZipExists(const std::filesystem::path& pathToZip, const std::filesystem::path& p)
        {
            if (!p.filename().empty())
            {
                throw error::ArMemError("The specified path is not a folder (it needs tailing /): " + p.string());
            }

            return checkIfElementInZipExists(pathToZip, p);
        }

        bool checkIfFileInZipExists(const std::filesystem::path& pathToZip, const std::filesystem::path& p)
        {
            if (p.filename().empty())
            {
                throw error::ArMemError("The specified path is not a file (it needs a filename): " + p.string());
            }

            return checkIfElementInZipExists(pathToZip, p);
        }

        void writeDataInFileInZipFile(const std::filesystem::path& pathToZip, const std::filesystem::path& p, const std::vector<unsigned char>& data)
        {
            auto zf = ensureZipFile(pathToZip);

            zip_fileinfo zfi;
            if(zipOpenNewFileInZip64(zf, p.string().c_str(), &zfi, NULL, 0, NULL, 0, NULL, Z_DEFLATED, Z_DEFAULT_COMPRESSION, 1) == Z_OK)
            {
                if(zipWriteInFileInZip(zf, data.data(), data.size()) != Z_OK)
                {
                    throw error::ArMemError("Could not write a file '"+p.string()+"' to zip '" + pathToZip.string() + "'.");
                }
            }
            else
            {
                throw error::ArMemError("Could not add a new file '"+p.string()+"' to zip '" + pathToZip.string() + "'.");
            }
            zipCloseFileInZip(zf);
            zipClose(zf, NULL);
        }

        std::vector<unsigned char> readDataFromFileInZipFile(const std::filesystem::path& pathToZip, const std::filesystem::path& p)
        {
            auto uzf = getUnzFile(pathToZip);
            if (unzLocateFile(uzf, p.string().c_str(), MINIZIP_CASE_SENSITIVITY) == UNZ_OK) // set current file
            {
                // File located
                unz_file_info uzfi;
                if (unzGetCurrentFileInfo(uzf, &uzfi, NULL, 0, NULL, 0, NULL, 0) == UNZ_OK)
                {
                    std::vector<unsigned char> data(uzfi.uncompressed_size);

                    if (unzOpenCurrentFile(uzf) == UNZ_OK) // open current file
                    {
                        if (unzReadCurrentFile(uzf, data.data(), uzfi.uncompressed_size) == UNZ_OK) // read file in buffer
                        {
                            unzCloseCurrentFile(uzf);
                            unzClose(uzf);
                            return data;
                        }
                    }
                }
            }
            unzClose(uzf);
            throw error::ArMemError("Could not read data from zip file '" + pathToZip.string() + "' and internal path '" + p.string() + "'.");
        }

        std::vector<std::string> getAllDirectoriesInZipFile(const std::filesystem::path& pathToZip, const std::filesystem::path p)
        {
            std::vector<std::string> ret;

            unzFile uzf = getUnzFile(pathToZip);
            if (unzLocateFile(uzf, p.string().c_str(), MINIZIP_CASE_SENSITIVITY) == UNZ_OK) // set current file
            {
                while(unzGoToNextFile(uzf) == UNZ_OK)
                {
                    unz_file_info uzfi;
                    unzGetCurrentFileInfo(uzf, &uzfi, NULL, 0, NULL, 0, NULL, 0); // get file info
                    std::vector<char> filenameVec(uzfi.size_filename);
                    unzGetCurrentFileInfo(uzf, NULL, filenameVec.data(), uzfi.size_filename, NULL, 0, NULL, 0); // get file name
                    std::string filename(filenameVec.begin(), filenameVec.end());

                    auto pString = p.string();
                    if (!simox::alg::starts_with(filename, pString))
                    {
                        // we moved out of the folder. Abort
                        break;
                    }

                    std::string filenameWithoutPrefix = simox::alg::remove_prefix(filename, pString);

                    if (!simox::alg::ends_with(filenameWithoutPrefix, "/"))
                    {
                        // this is not a directory
                        continue;
                    }

                    if (simox::alg::count(filenameWithoutPrefix, "/") != 1)
                    {
                        // we are in a subfolder of the subfolder
                        continue;
                    }

                    ret.push_back(filenameWithoutPrefix);
                }
            }
            return ret;
        }

        std::vector<std::string> getAllFilesInZipFile(const std::filesystem::path& pathToZip, const std::filesystem::path p)
        {
            std::vector<std::string> ret;

            unzFile uzf = getUnzFile(pathToZip);
            if (unzLocateFile(uzf, p.string().c_str(), MINIZIP_CASE_SENSITIVITY) == UNZ_OK) // set current file
            {
                while(unzGoToNextFile(uzf) == UNZ_OK)
                {
                    unz_file_info uzfi;
                    unzGetCurrentFileInfo(uzf, &uzfi, NULL, 0, NULL, 0, NULL, 0); // get file info
                    std::vector<char> filenameVec(uzfi.size_filename);
                    unzGetCurrentFileInfo(uzf, NULL, filenameVec.data(), uzfi.size_filename, NULL, 0, NULL, 0); // get file name
                    std::string filename(filenameVec.begin(), filenameVec.end());

                    auto pString = p.string();
                    if (!simox::alg::starts_with(filename, pString))
                    {
                        // we moved out of the folder. Abort
                        break;
                    }

                    std::string filenameWithoutPrefix = simox::alg::remove_prefix(filename, pString);

                    if (simox::alg::ends_with(filenameWithoutPrefix, "/"))
                    {
                        // this is a directory
                        continue;
                    }

                    if (simox::alg::count(filenameWithoutPrefix, "/") != 1)
                    {
                        // we are in a subfolder of the subfolder
                        continue;
                    }

                    ret.push_back(filenameWithoutPrefix);
                }
            }
            return ret;
        }
    }
} // namespace armarx::armem::server::ltm::disk
