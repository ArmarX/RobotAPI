#include "filesystem_util.h"

#include <RobotAPI/libraries/armem/core/error/ArMemError.h>


namespace armarx::armem::server::ltm::disk
{
    namespace filesystem::util
    {
        size_t getSizeOfDirectory(const std::filesystem::path& p)
        {
            if (!std::filesystem::exists(p) || !std::filesystem::is_directory(p))
            {
                return 0;
            }

            // command to be executed. Taken from https://stackoverflow.com/questions/15495756/how-can-i-find-the-size-of-all-files-located-inside-a-folder/15501719
            std::string cmd = "du -sb " + p.string() + " | cut -f1 2>&1";

            // execute above command and get the output
            FILE *stream = popen(cmd.c_str(), "r");
            if (stream) {
                const int max_size = 256;
                char readbuf[max_size];
                if (fgets(readbuf, max_size, stream) != NULL)
                {
                    return atoll(readbuf);
                }
                pclose(stream);
            }

            // return error val
            return 0;
        }

        bool checkIfFolderInFilesystemExists(const std::filesystem::path& p)
        {
            return std::filesystem::exists(p) and std::filesystem::is_directory(p);
        }

        void ensureFolderInFilesystemExists(const std::filesystem::path& p, bool createIfNotExistent)
        {
            if (!std::filesystem::exists(p))
            {
                if (createIfNotExistent)
                {
                    std::filesystem::create_directories(p);
                }
            }
            if (!std::filesystem::is_directory(p))
            {
                throw error::ArMemError("Could not find folder: " + p.string());
            }
        }

        bool checkIfFileInFilesystemExists(const std::filesystem::path& p)
        {
            return std::filesystem::exists(p) and std::filesystem::is_regular_file(p);
        }

        void ensureFileInFilesystemExists(const std::filesystem::path& p)
        {
            if (!std::filesystem::exists(p) || !std::filesystem::is_regular_file(p))
            {
                // not found
                throw error::ArMemError("Could not find file: " + p.string());
            }
        }

        void writeDataInFilesystemFile(const std::filesystem::path& p, const std::vector<unsigned char>& data)
        {
            std::ofstream dataofs;
            dataofs.open(p);
            if (!dataofs)
            {
                throw error::ArMemError("Could not write data to filesystem file '" + p.string() + "'. Skipping this file.");
            }
            dataofs.write(reinterpret_cast<const char*>(data.data()), data.size());
            dataofs.close();
        }

        std::vector<unsigned char> readDataFromFilesystemFile(const std::filesystem::path path)
        {
            std::ifstream dataifs(path);
            std::vector<unsigned char> datafilecontent((std::istreambuf_iterator<char>(dataifs)), (std::istreambuf_iterator<char>()));
            return datafilecontent;
        }

        std::vector<std::string> getAllFilesystemDirectories(const std::filesystem::path path)
        {
            std::vector<std::string> ret;
            for (const auto& subdir : std::filesystem::directory_iterator(path))
            {
                std::filesystem::path subdirPath = subdir.path();
                if (std::filesystem::is_directory(subdirPath))
                {
                    ret.push_back(subdirPath.filename());
                }
            }
            return ret;
        }

        std::vector<std::string> getAllFilesystemFiles(const std::filesystem::path path)
        {
            std::vector<std::string> ret;
            for (const auto& subdir : std::filesystem::directory_iterator(path))
            {
                std::filesystem::path subdirPath = subdir.path();
                if (std::filesystem::is_regular_file(subdirPath))
                {
                    ret.push_back(subdirPath.filename());
                }
            }
            return ret;
        }
    }
} // namespace armarx::armem::server::ltm::disk
