#pragma once

#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>


namespace armarx::armem::server::ltm::disk
{
    namespace filesystem::util
    {
        size_t getSizeOfDirectory(const std::filesystem::path& p);

        bool checkIfFolderInFilesystemExists(const std::filesystem::path& p);

        void ensureFolderInFilesystemExists(const std::filesystem::path& p, bool createIfNotExistent = true);

        bool checkIfFileInFilesystemExists(const std::filesystem::path& p);

        void ensureFileInFilesystemExists(const std::filesystem::path& p);

        void writeDataInFilesystemFile(const std::filesystem::path& p, const std::vector<unsigned char>& data);

        std::vector<unsigned char> readDataFromFilesystemFile(const std::filesystem::path path);

        std::vector<std::string> getAllFilesystemDirectories(const std::filesystem::path path);

        std::vector<std::string> getAllFilesystemFiles(const std::filesystem::path path);
    }
} // namespace armarx::armem::server::ltm::disk
