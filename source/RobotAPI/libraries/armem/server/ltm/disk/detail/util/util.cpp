#include "util.h"

#include <thread>
#include <chrono>

namespace armarx::armem::server::ltm::disk
{
    namespace util
    {
        // check whether a string is a number (e.g. timestamp folders)
        bool isNumber(const std::string& s)
        {
            for (char const& ch : s)
            {
                if (std::isdigit(ch) == 0)
                {
                    return false;
                }
            }
            return true;
        }

        bool checkIfBasePathExists(const std::filesystem::path& mPath)
        {
            if (mPath.extension() == minizip::util::MINIZIP_SUFFIX)
            {
                return minizip::util::checkZipFile(mPath);
            }
            return filesystem::util::checkIfFolderInFilesystemExists(mPath);
        }

        void ensureBasePathExists(const std::filesystem::path& mPath, bool createIfNotExistent)
        {
            if (mPath.extension() == minizip::util::MINIZIP_SUFFIX)
            {
                auto z = minizip::util::ensureZipFile(mPath, createIfNotExistent);
                zipClose(z, NULL);
                return;
            }
            return filesystem::util::ensureFolderInFilesystemExists(mPath, createIfNotExistent);
        }

        bool checkIfFolderExists(const std::filesystem::path& mPath, const std::filesystem::path& p)
        {
            if (mPath.extension() == minizip::util::MINIZIP_SUFFIX)
            {
                return minizip::util::checkIfFolderInZipExists(mPath, p);
            }

            return filesystem::util::checkIfFolderInFilesystemExists(mPath / p);
        }

        void ensureFolderExists(const std::filesystem::path& mPath, const std::filesystem::path& p, bool createIfNotExistent)
        {
            if (mPath.extension() == minizip::util::MINIZIP_SUFFIX)
            {
                return minizip::util::ensureFolderInZipExists(mPath, p, createIfNotExistent);
            }

            return filesystem::util::ensureFolderInFilesystemExists(mPath / p, createIfNotExistent);
        }

        bool checkIfFileExists(const std::filesystem::path& mPath, const std::filesystem::path& p)
        {
            if (mPath.extension() == minizip::util::MINIZIP_SUFFIX)
            {
                return minizip::util::checkIfFileInZipExists(mPath, p);
            }

            return filesystem::util::checkIfFileInFilesystemExists(mPath / p);
        }

        void ensureFileExists(const std::filesystem::path& mPath, const std::filesystem::path& p)
        {
            if (mPath.extension() == minizip::util::MINIZIP_SUFFIX)
            {
                return minizip::util::ensureFileInZipExists(mPath, p);
            }

            return filesystem::util::ensureFileInFilesystemExists(mPath / p);
        }

        void writeDataToFile(const std::filesystem::path& mPath, const std::filesystem::path& p, const std::vector<unsigned char>& data)
        {
            if (mPath.extension() == minizip::util::MINIZIP_SUFFIX)
            {
                return minizip::util::writeDataInFileInZipFile(mPath, p, data);
            }

            return filesystem::util::writeDataInFilesystemFile(mPath / p, data);
        }

        void writeDataToFileRepeated(const std::filesystem::path& mPath, const std::filesystem::path& p, const std::vector<unsigned char>& data, const unsigned int maxTries, const unsigned int sleepTimeMs)
        {
            for (unsigned int i = 0; i < maxTries; ++i)
            {
                try
                {
                    writeDataToFile(mPath, p, data);
                    return;
                }
                catch (const error::ArMemError&)
                {
                    // wait a bit to give the filesystem enough time to manage the workload
                    std::this_thread::sleep_for(std::chrono::milliseconds(sleepTimeMs));
                }
            }

            // even after all the tries we did not succeeded. This is very bad!
            throw error::ArMemError("ATTENTION! Even after " + std::to_string(maxTries) + " tries, the memory was not able to store the instance at path '" + p.string() + "'. This means this instance will be lost!");
        }

        std::vector<unsigned char> readDataFromFile(const std::filesystem::path& mPath, const std::filesystem::path& p)
        {
            if (mPath.extension() == minizip::util::MINIZIP_SUFFIX)
            {
                return minizip::util::readDataFromFileInZipFile(mPath, p);
            }

            return filesystem::util::readDataFromFilesystemFile(mPath / p);
        }

        std::vector<std::string> getAllDirectories(const std::filesystem::path& mPath, const std::filesystem::path& p)
        {
            if (mPath.extension() == minizip::util::MINIZIP_SUFFIX)
            {
                return {};
            }

            return filesystem::util::getAllFilesystemDirectories(mPath / p);
        }

        std::vector<std::string> getAllFiles(const std::filesystem::path& mPath, const std::filesystem::path& p)
        {
            if (mPath.extension() == minizip::util::MINIZIP_SUFFIX)
            {
                return {};
            }

            return filesystem::util::getAllFilesystemFiles(mPath / p);
        }
    }
} // namespace armarx::armem::server::ltm::disk
