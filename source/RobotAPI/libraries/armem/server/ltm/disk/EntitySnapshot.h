#pragma once

#include <filesystem>

// Base Class
#include "../base/detail/EntitySnapshotBase.h"
#include "detail/DiskStorage.h"

namespace armarx::armem::server::ltm::disk
{

    class EntitySnapshot :
            public EntitySnapshotBase,
            public DiskMemoryItem
    {
    public:
        EntitySnapshot(const std::filesystem::path& parentPath, const MemoryID& id, const std::shared_ptr<Processors>& p, const DiskMemoryItem::MemoryEncodingMode mode, const unsigned long e);

    protected:
        void _loadAllReferences(armem::wm::EntitySnapshot&) const override;
        void _resolve(armem::wm::EntitySnapshot&) const override;
        void _store(const armem::wm::EntitySnapshot&) override;

    private:
        MemoryEncodingMode currentMode;
        unsigned long currentExport;
    };

} // namespace armarx::armem::server::ltm::disk
