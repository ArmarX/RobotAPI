#include "Recording.h"

#include <ArmarXCore/core/ice_conversions.h>
#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>

#include <ArmarXCore/core/time/ice_conversions.h>
#include <RobotAPI/libraries/armem/core/wm/ice_conversions.h>

namespace armarx::armem::server::ltm
{
    using armarx::fromIce;
    using armarx::toIce;

    /* ===========================
     * DirectlyStoreInput
     * =========================== */
    DirectlyStoreInput DirectlyStoreInput::fromIce(const dto::DirectlyStoreInput& ice)
    {
        return armarx::fromIce<DirectlyStoreInput>(ice);
    }
    dto::DirectlyStoreInput DirectlyStoreInput::toIce() const
    {
        return armarx::toIce<dto::DirectlyStoreInput>(*this);
    }
    void toIce(dto::DirectlyStoreInput& ice, const DirectlyStoreInput& result)
    {
        toIce(ice.memory, result.memory);
    }
    void fromIce(const dto::DirectlyStoreInput& ice, DirectlyStoreInput& result)
    {
        fromIce(ice.memory, result.memory);
    }

    /* ===========================
     * DirectlyStoreResult
     * =========================== */
    DirectlyStoreResult DirectlyStoreResult::fromIce(const dto::DirectlyStoreResult& ice)
    {
        return armarx::fromIce<DirectlyStoreResult>(ice);
    }
    dto::DirectlyStoreResult DirectlyStoreResult::toIce() const
    {
        return armarx::toIce<dto::DirectlyStoreResult>(*this);
    }
    void toIce(dto::DirectlyStoreResult& ice, const DirectlyStoreResult& result)
    {
        toIce(ice, dynamic_cast<const detail::SuccessHeader&>(result));
    }
    void fromIce(const dto::DirectlyStoreResult& ice, DirectlyStoreResult& result)
    {
        fromIce(ice, dynamic_cast<detail::SuccessHeader&>(result));
    }

    /* ===========================
     * DirectlyStoreInput
     * =========================== */
    StartRecordInput StartRecordInput::fromIce(const dto::StartRecordInput& ice)
    {
        return armarx::fromIce<StartRecordInput>(ice);
    }
    dto::StartRecordInput StartRecordInput::toIce() const
    {
        return armarx::toIce<dto::StartRecordInput>(*this);
    }
    void toIce(dto::StartRecordInput& ice, const StartRecordInput& result)
    {
        toIce(ice.executionTime, result.executionTime);
        ice.recordingID = result.recordingID;
        ice.configuration.clear();
        for (const auto& [k, v] : result.configuration)
        {
            armem::data::MemoryID id;
            armem::toIce(id, k);

            dto::RecordingModeConfiguration c;
            c.mode = v.mode;
            c.frequency = v.frequency;

            ice.configuration[id] = c;
        }
    }
    void fromIce(const dto::StartRecordInput& ice, StartRecordInput& result)
    {
        fromIce(ice.executionTime, result.executionTime);
        result.recordingID = ice.recordingID;
        result.configuration.clear();
        for (const auto& [k, v] : ice.configuration)
        {
            armem::MemoryID id;
            armem::fromIce(k, id);

            StartRecordInput::RecordingModeConfiguration c;
            c.mode = v.mode;
            c.frequency = v.frequency;

            result.configuration[id] = c;
        }
    }

    /* ===========================
     * StartRecordResult
     * =========================== */
    StartRecordResult StartRecordResult::fromIce(const dto::StartRecordResult& ice)
    {
        return armarx::fromIce<StartRecordResult>(ice);
    }
    dto::StartRecordResult StartRecordResult::toIce() const
    {
        return armarx::toIce<dto::StartRecordResult>(*this);
    }
    void toIce(dto::StartRecordResult& ice, const StartRecordResult& result)
    {
        toIce(ice, dynamic_cast<const detail::SuccessHeader&>(result));
    }
    void fromIce(const dto::StartRecordResult& ice, StartRecordResult& result)
    {
        fromIce(ice, dynamic_cast<detail::SuccessHeader&>(result));
    }

    /* ===========================
     * StopRecordResult
     * =========================== */
    StopRecordResult StopRecordResult::fromIce(const dto::StopRecordResult& ice)
    {
        return armarx::fromIce<StopRecordResult>(ice);
    }
    dto::StopRecordResult StopRecordResult::toIce() const
    {
        return armarx::toIce<dto::StopRecordResult>(*this);
    }
    void toIce(dto::StopRecordResult& ice, const StopRecordResult& result)
    {
        toIce(ice, dynamic_cast<const detail::SuccessHeader&>(result));
    }
    void fromIce(const dto::StopRecordResult& ice, StopRecordResult& result)
    {
        fromIce(ice, dynamic_cast<detail::SuccessHeader&>(result));
    }

    /* ===========================
     * RecordStatusResult
     * =========================== */
    RecordStatusResult RecordStatusResult::fromIce(const dto::RecordStatusResult& ice)
    {
        return armarx::fromIce<RecordStatusResult>(ice);
    }
    dto::RecordStatusResult RecordStatusResult::toIce() const
    {
        return armarx::toIce<dto::RecordStatusResult>(*this);
    }
    void toIce(dto::RecordStatusResult& ice, const RecordStatusResult& result)
    {
        toIce(ice, dynamic_cast<const detail::SuccessHeader&>(result));
    }
    void fromIce(const dto::RecordStatusResult& ice, RecordStatusResult& result)
    {
        fromIce(ice, dynamic_cast<detail::SuccessHeader&>(result));
    }

    /* ===========================
     * GetServerStructureResult
     * ===========================
    GetServerStructureResult GetServerStructureResult::fromIce(const dto::GetServerStructureResult& ice)
    {
        return armarx::fromIce<GetServerStructureResult>(ice);
    }
    dto::GetServerStructureResult GetServerStructureResult::toIce() const
    {
        return armarx::toIce<dto::GetServerStructureResult>(*this);
    }
    void toIce(dto::GetServerStructureResult& ice, const GetServerStructureResult& result)
    {
        toIce(ice, dynamic_cast<const detail::SuccessHeader&>(result));
        toIce(ice.serverStructure, result.serverStructure);
    }
    void fromIce(const dto::GetServerStructureResult& ice, GetServerStructureResult& result)
    {
        fromIce(ice, dynamic_cast<detail::SuccessHeader&>(result));
        fromIce(ice.serverStructure, result.serverStructure);
    }*/
}
