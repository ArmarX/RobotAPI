#pragma once

#include <RobotAPI/interface/armem/server/RecordingMemoryInterface.h>
#include <RobotAPI/libraries/armem/core/SuccessHeader.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>


namespace armarx::armem::server::ltm
{

    /* ===========================
     * DirectlyStoreInput
     * =========================== */
    struct DirectlyStoreInput
    {
        armem::wm::Memory memory;

        static DirectlyStoreInput fromIce(const dto::DirectlyStoreInput& ice);
        dto::DirectlyStoreInput toIce() const;
    };
    void toIce(dto::DirectlyStoreInput& ice, const DirectlyStoreInput& input);
    void fromIce(const dto::DirectlyStoreInput& ice, DirectlyStoreInput& input);



    /* ===========================
     * DirectlyStoreResult
     * =========================== */
    struct DirectlyStoreResult : public detail::SuccessHeader
    {
        // TODO: Move to armarx::core::time
        long timeStartedMicroSeconds;
        long timeFinishedMicroSeconds;

        static DirectlyStoreResult fromIce(const dto::DirectlyStoreResult& ice);
        dto::DirectlyStoreResult toIce() const;
    };
    void toIce(dto::DirectlyStoreResult& ice, const DirectlyStoreResult& input);
    void fromIce(const dto::DirectlyStoreResult& ice, DirectlyStoreResult& input);



    /* ===========================
     * StartRecordInput
     * =========================== */
    struct StartRecordInput
    {
        struct RecordingModeConfiguration
        {
            dto::RecordingMode::RecordingModeEnum mode;
            float frequency;
        };

        armarx::core::time::DateTime executionTime;
        std::string recordingID;
        std::map<armem::MemoryID, RecordingModeConfiguration> configuration;

        static StartRecordInput fromIce(const dto::StartRecordInput& ice);
        dto::StartRecordInput toIce() const;
    };
    void toIce(dto::StartRecordInput& ice, const StartRecordInput& input);
    void fromIce(const dto::StartRecordInput& ice, StartRecordInput& input);



    /* ===========================
     * StartRecordResult
     * =========================== */
    struct StartRecordResult : public detail::SuccessHeader
    {
        static StartRecordResult fromIce(const dto::StartRecordResult& ice);
        dto::StartRecordResult toIce() const;
    };
    void toIce(dto::StartRecordResult& ice, const StartRecordResult& input);
    void fromIce(const dto::StartRecordResult& ice, StartRecordResult& input);



    /* ===========================
     * StopRecordResult
     * =========================== */
    struct StopRecordResult : public detail::SuccessHeader
    {
        static StopRecordResult fromIce(const dto::StopRecordResult& ice);
        dto::StopRecordResult toIce() const;
    };
    void toIce(dto::StopRecordResult& ice, const StopRecordResult& input);
    void fromIce(const dto::StopRecordResult& ice, StopRecordResult& input);



    /* ===========================
     * RecordStatusResult
     * =========================== */
    struct RecordStatusResult : public detail::SuccessHeader
    {
        struct RecordStatus
        {
            int totalSnapshots;
            int savedSnapshots;
        } status;

        static RecordStatusResult fromIce(const dto::RecordStatusResult& ice);
        dto::RecordStatusResult toIce() const;
    };
    void toIce(dto::RecordStatusResult& ice, const RecordStatusResult& input);
    void fromIce(const dto::RecordStatusResult& ice, RecordStatusResult& input);
}
