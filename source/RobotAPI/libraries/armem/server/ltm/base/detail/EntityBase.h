#pragma once

#include <functional>

// BaseClass
#include "MemoryItem.h"

#include "EntitySnapshotBase.h"

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/wm/aron_conversions.h>

namespace armarx::armem::server::ltm
{
    /// @brief Interface functions for the longterm memory classes
    template <class _EntitySnapshotT>
    class EntityBase : public MemoryItem
    {
    public:
        struct Statistics
        {
            long recordedSnapshots = 0;

        };

    public:
        using EntitySnapshotT = _EntitySnapshotT;

        using MemoryItem::MemoryItem;

        /// return the full sub-ltm as a wm::Entity with only references
        /// the ltm may be huge, use with caution
        void loadAllReferences(armem::wm::Entity& e)
        {
            _loadAllReferences(e);
        }

        /// convert the references of the input into a wm::Memory
        void resolve(armem::wm::Entity& e)
        {
            _resolve(e);
        }

        /// encode the content of a wm::Memory and store
        void store(const armem::wm::Entity& e)
        {
            _store(e);
        }

        /// statistics
        void resetStatistics()
        {
            statistics.recordedSnapshots = 0;
        }
        Statistics getStatistics() const
        {
            return statistics;
        }

        /// iterate over all entity snapshots of this ltm
        virtual bool forEachSnapshot(std::function<void(EntitySnapshotT&)>&& func) const = 0;
        virtual bool forEachSnapshotInIndexRange(long first, long last, std::function<void(EntitySnapshotT&)>&& func) const = 0;
        virtual bool forEachSnapshotInTimeRange(const Time& min, const Time& max, std::function<void(EntitySnapshotT&)>&& func) const = 0;
        virtual bool forEachSnapshotBeforeOrAt(const Time& time, std::function<void(EntitySnapshotT&)>&& func) const = 0;
        virtual bool forEachSnapshotBefore(const Time& time, std::function<void(EntitySnapshotT&)>&& func) const = 0;

        /// find entity snapshot segment
        virtual std::shared_ptr<EntitySnapshotT> findSnapshot(const Time&) const = 0;
        virtual std::shared_ptr<EntitySnapshotT> findLatestSnapshot() const = 0;
        virtual std::shared_ptr<EntitySnapshotT> findLatestSnapshotBefore(const Time& time) const = 0;
        virtual std::shared_ptr<EntitySnapshotT> findLatestSnapshotBeforeOrAt(const Time& time) const = 0;
        virtual std::shared_ptr<EntitySnapshotT> findFirstSnapshotAfter(const Time& time) const = 0;
        virtual std::shared_ptr<EntitySnapshotT> findFirstSnapshotAfterOrAt(const Time& time) const = 0;

        static std::string getLevelName()
        {
            return "LT-Entity";
        }

    protected:
        virtual void _loadAllReferences(armem::wm::Entity&) = 0;
        virtual void _resolve(armem::wm::Entity&) = 0;
        virtual void _store(const armem::wm::Entity&) = 0;

    protected:
        mutable std::recursive_mutex ltm_mutex;

        Statistics statistics;
    };
} // namespace armarx::armem::server::ltm
