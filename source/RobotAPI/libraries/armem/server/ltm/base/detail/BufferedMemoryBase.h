#pragma once

#include "MemoryBase.h"

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

namespace armarx::armem::server::ltm
{
    template <class _CoreSegmentT>
    class BufferedMemoryBase : public MemoryBase<_CoreSegmentT>
    {
        using Base = MemoryBase<_CoreSegmentT>;

    public:
        BufferedMemoryBase(const MemoryID& id) :
            Base(id),
            buffer(std::make_shared<armem::wm::Memory>(id)),
            to_store(std::make_shared<armem::wm::Memory>(id))
        {
        }

        virtual ~BufferedMemoryBase() = default;

        void setMemoryID(const MemoryID& id) override
        {
            ARMARX_CHECK_NOT_EMPTY(id.memoryName) << " The full id was: " << id.str();

            Base::setMemoryID(id.getMemoryID());

            buffer->id() = id.getMemoryID();
            to_store->id() = id.getMemoryID();
        }

        armem::wm::Memory getBuffer() const
        {
            std::lock_guard l(bufferMutex);
            return *buffer;
        }

        void directlyStore(const armem::wm::Memory& memory)
        {
            TIMING_START(LTM_Memory_DirectlyStore);
            for (auto& f : this->processors->memFilters)
            {
                if (!f->accept(memory))
                {
                    ARMARX_WARNING << deactivateSpam() << "Ignoring to commit a Memory into the LTM because the full commit got filtered.";
                    return;
                }
            }
            _directlyStore(memory);
            TIMING_END_STREAM(LTM_Memory_DirectlyStore, ARMARX_DEBUG);
        }

        void storeBuffer()
        {
            std::lock_guard l(storeMutex);
            {
                std::lock_guard l(bufferMutex);
                to_store = buffer;
                buffer = std::make_shared<armem::wm::Memory>(this->id());
            }

            if (to_store->empty())
            {
                ARMARX_INFO << deactivateSpam() << "Cannot store an empty buffer. Ignoring.";
                return;
            }

            this->directlyStore(*to_store);
        }

        /// configuration
        void configure(const nlohmann::json& json) override
        {
            Base::configure(json);
            if (json.find("BufferedMemoryBase.storeFrequency") != json.end())
            {
                storeFrequency = json.at("BufferedMemoryBase.storeFrequency");
            }
        }

    protected:
        virtual void _directlyStore(const armem::wm::Memory& memory) = 0;

        void _store(const armem::wm::Memory& memory) override
        {
            std::lock_guard l(bufferMutex);
            buffer->append(memory);

            // create task if not already exists
            if (!task)
            {
                int waitingTimeMs = 1000.f / storeFrequency;
                task = new armarx::PeriodicTask<BufferedMemoryBase>(this, &BufferedMemoryBase::storeBuffer, waitingTimeMs);
                task->start();
            }
        }

    protected:
        /// Internal memory for data consolidated from wm to ltm (double-buffer)
        /// The to-put-to-ltm buffer (contains data in plain text)
        /// This buffer may still be filtered (e.g. snapshot filters).
        /// This means that it is not guaranteed that all data in the buffer will be stored in the ltm
        std::shared_ptr<armem::wm::Memory> buffer;
        std::shared_ptr<armem::wm::Memory> to_store;

        /// The periodic'task to store the content of the buffer to the ltm
        typename armarx::PeriodicTask<BufferedMemoryBase>::pointer_type task = nullptr;

        /// The frequency (Hz) to store data to the ltm
        float storeFrequency = 10;

        /// a mutex to access the buffer object
        mutable std::mutex bufferMutex;
        mutable std::mutex storeMutex;

    };
} // namespace armarx::armem::server::ltm
