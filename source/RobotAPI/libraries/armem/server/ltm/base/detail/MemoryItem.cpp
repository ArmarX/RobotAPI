#include "MemoryItem.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

namespace armarx::armem::server::ltm
{
    MemoryItem::MemoryItem(const MemoryID& id) :
        processors(std::make_shared<Processors>()),
        _id(id)
    {
    }

    MemoryItem::MemoryItem(const MemoryID& id, const std::shared_ptr<Processors>& p) :
        processors(p),
        _id(id)
    {
    }

    void MemoryItem::setMemoryID(const MemoryID& id)
    {
        _id = id;
    }

    MemoryID MemoryItem::id() const
    {
        return _id;
    }

    std::string MemoryItem::name() const
    {
        return _id.getLeafItem();
    }
} // namespace armarx::armem::server::ltm
