#pragma once

#include <map>
#include <mutex>
#include <optional>
#include <string>

#include "Processors.h"

namespace armarx::armem::server::ltm
{
    /// @brief Interface functions for the longterm memory classes
    class MemoryItem
    {
    public:
        MemoryItem(const MemoryID&); // only used by memory
        MemoryItem(const MemoryID&, const std::shared_ptr<Processors>&); // used by all other segments
        virtual ~MemoryItem() = default;

        MemoryID id() const;
        std::string name() const;

        virtual void setMemoryID(const MemoryID&);

    protected:
        std::shared_ptr<Processors> processors;

    private:
        MemoryID _id;
    };
} // namespace armarx::armem::server::ltm
