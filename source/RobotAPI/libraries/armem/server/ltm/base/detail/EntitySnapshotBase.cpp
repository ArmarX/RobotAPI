#include "EntitySnapshotBase.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time/TimeUtil.h>

#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

namespace armarx::armem::server::ltm
{
    std::string EntitySnapshotBase::getLevelName()
    {
        return "LT-EntitySnapshot";
    }
} // namespace armarx::armem::server::ltm
