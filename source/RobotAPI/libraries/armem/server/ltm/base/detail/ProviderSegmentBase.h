#pragma once

#include <functional>

// BaseClass
#include "MemoryItem.h"

#include "EntityBase.h"

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/wm/aron_conversions.h>

namespace armarx::armem::server::ltm
{
    /// @brief Interface functions for the longterm memory classes
    template <class _EntityT>
    class ProviderSegmentBase : public MemoryItem
    {
    public:
        struct Statistics
        {
            long recordedEntities = 0;

        };

    public:
        using EntityT = _EntityT;

        using MemoryItem::MemoryItem;

        /// return the full sub-ltm as a wm::ProviderSegment with only references
        /// the ltm may be huge, use with caution
        void loadAllReferences(armem::wm::ProviderSegment& provSeg)
        {
            _loadAllReferences(provSeg);
        }

        /// convert the references of the input into a wm::Memory
        void resolve(armem::wm::ProviderSegment& provSeg)
        {
            _resolve(provSeg);
        }

        /// encode the content of a wm::Memory and store
        void store(const armem::wm::ProviderSegment& provSeg)
        {
            _store(provSeg);
        }

        /// store the type of the segment
        void storeType(const armem::wm::ProviderSegment& coreSeg)
        {
            _storeType(coreSeg);
        }

        /// statistics
        void resetStatistics()
        {
            statistics.recordedEntities = 0;
        }
        Statistics getStatistics() const
        {
            return statistics;
        }

        /// iterate over all core segments of this ltm
        virtual bool forEachEntity(std::function<void(EntityT&)>&& func) const = 0;

        /// find entity segment
        virtual std::shared_ptr<EntityT> findEntity(const std::string&) const = 0;

        aron::type::ObjectPtr aronType() const
        {
            return nullptr;
        }

        static std::string getLevelName()
        {
            return "LT-ProviderSegment";
        }

    protected:
        virtual void _loadAllReferences(armem::wm::ProviderSegment&) = 0;
        virtual void _resolve(armem::wm::ProviderSegment&) = 0;
        virtual void _store(const armem::wm::ProviderSegment&) = 0;
        virtual void _storeType(const armem::wm::ProviderSegment&) = 0;

    protected:
        mutable std::recursive_mutex ltm_mutex;

        Statistics statistics;
    };
} // namespace armarx::armem::server::ltm
