#pragma once

#include <functional>

// BaseClass
#include "MemoryItem.h"

#include "ProviderSegmentBase.h"

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/wm/aron_conversions.h>

namespace armarx::armem::server::ltm
{
    /// @brief Interface functions for the longterm memory classes
    template <class _ProviderSegmentT>
    class CoreSegmentBase : public MemoryItem
    {
    public:
        struct Statistics
        {
            long recordedProviderSegments = 0;

        };

    public:
        using ProviderSegmentT = _ProviderSegmentT;

        using MemoryItem::MemoryItem;

        /// return the full sub-ltm as a wm::CoreSegment with only references
        /// the ltm may be huge, use with caution
        void loadAllReferences(armem::wm::CoreSegment& coreSeg)
        {
            _loadAllReferences(coreSeg);
        }

        /// convert the references of the input into a wm::Memory
        void resolve(armem::wm::CoreSegment& coreSeg)
        {
            _resolve(coreSeg);
        }

        /// encode the content of a wm::Memory and store
        void store(const armem::wm::CoreSegment& coreSeg)
        {
            _store(coreSeg);
        }

        /// store the type of the core segment
        void storeType(const armem::wm::CoreSegment& coreSeg)
        {
            _storeType(coreSeg);
        }

        /// statistics
        void resetStatistics()
        {
            statistics.recordedProviderSegments = 0;
        }
        Statistics getStatistics() const
        {
            return statistics;
        }

        /// iterate over all provider segments of this ltm
        virtual bool forEachProviderSegment(std::function<void(ProviderSegmentT&)>&& func) const = 0;

        /// find provider segment
        virtual std::shared_ptr<ProviderSegmentT> findProviderSegment(const std::string&) const = 0;

        /// get aron type
        aron::type::ObjectPtr aronType() const
        {
            return nullptr;
        }

        /// get level name
        static std::string getLevelName()
        {
            return "LT-CoreSegment";
        }

    protected:
        virtual void _loadAllReferences(armem::wm::CoreSegment&) = 0;
        virtual void _resolve(armem::wm::CoreSegment&) = 0;
        virtual void _store(const armem::wm::CoreSegment&) = 0;
        virtual void _storeType(const armem::wm::CoreSegment&) = 0;

    protected:
        mutable std::recursive_mutex ltm_mutex;

        Statistics statistics;
    };
} // namespace armarx::armem::server::ltm
