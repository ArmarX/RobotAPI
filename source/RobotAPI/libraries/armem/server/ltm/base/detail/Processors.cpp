#include "Processors.h"

namespace armarx::armem::server::ltm
{
    Processors::Processors()
    {
        // setup containers
        memFilters.push_back(&memFreqFilter);
        snapFilters.push_back(&snapFreqFilter);
        snapFilters.push_back(&snapEqFilter);
        extractors.push_back(&imageExtractor);
        extractors.push_back(&depthImageExtractor);
        converters.insert({pngConverter.identifier, &pngConverter});
        converters.insert({exrConverter.identifier, &exrConverter});
    }

    void Processors::configure(const nlohmann::json& config)
    {

    }
}
