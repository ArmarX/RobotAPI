#pragma once

#include "MemoryBase.h"

namespace armarx::armem::server::ltm
{
    /*// TODO refactor to mixin (see buffered)
    template <class _CoreSegmentT>
    class CachedMemoryBase : virtual public MemoryBase<_CoreSegmentT>
    {
    public:
        using MemoryBase<_CoreSegmentT>::MemoryBase;

        armem::wm::Memory getCache() const
        {
            std::lock_guard l(this->ltm_mutex);
            return cache;
        }

        void setMemoryID(const MemoryID& id) override
        {
            MemoryBase<_CoreSegmentT>::setMemoryID(id);
            cache.name() = this->name();
        }

    protected:
        static bool EntitySnapshotHasData(const armem::wm::EntitySnapshot& e)
        {
            // check whether all data is nullptr
            bool allDataIsNull = e.size() > 0;
            e.forEachInstance([&allDataIsNull](armem::wm::EntityInstance & e)
            {
                if (e.data())
                {
                    allDataIsNull = false;
                    return false; // means break
                }
                return true;
            });
            return !allDataIsNull;
        }

    protected:

        armem::wm::Memory cache;

    };*/
} // namespace armarx::armem::server::ltm
