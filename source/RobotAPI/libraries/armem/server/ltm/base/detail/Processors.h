#pragma once

#include <map>
#include <mutex>
#include <optional>
#include <string>

#include <ArmarXCore/core/application/properties/Properties.h>

#include "../filter/frequencyFilter/FrequencyFilter.h"
#include "../filter/equalityFilter/EqualityFilter.h"
#include "../extractor/imageExtractor/ImageExtractor.h"
#include "../extractor/imageExtractor/DepthImageExtractor.h"
#include "../converter/object/json/JsonConverter.h"
#include "../converter/image/png/PngConverter.h"
#include "../converter/image/exr/ExrConverter.h"
#include "../typeConverter/json/JsonConverter.h"

#include <RobotAPI/libraries/armem/core/MemoryID.h>

namespace armarx::armem::server::ltm
{
    /// all necessary classes to filter and convert an entry of the ltm to some other format(s)
    class Processors
    {
    public:
        Processors();
        void configure(const nlohmann::json& config);

    public:
        // Unique Memory Filters
        std::vector<MemoryFilter*> memFilters;
        filter::MemoryFrequencyFilter memFreqFilter;

        // Unique Snapshot filters
        std::vector<SnapshotFilter*> snapFilters;
        filter::SnapshotFrequencyFilter snapFreqFilter;
        filter::SnapshotEqualityFilter snapEqFilter;

        // Special Extractors
        std::vector<Extractor*> extractors;
        extractor::ImageExtractor imageExtractor;
        extractor::DepthImageExtractor depthImageExtractor;

        // Special Converters
        std::map<std::string, Converter*> converters;
        converter::image::PngConverter pngConverter;
        converter::image::ExrConverter exrConverter;

        // Default converter
        converter::object::JsonConverter defaultObjectConverter;
        converter::type::JsonConverter defaultTypeConverter;
    };
}
