#pragma once

#include <functional>

// BaseClass
#include "MemoryItem.h"

// ChildType
#include "CoreSegmentBase.h"

// ArmarX
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/logging/LoggingUtil.h>

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/wm/aron_conversions.h>
#include <RobotAPI/libraries/armem/core/operations.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>


namespace armarx::armem::server::ltm
{
    /// @brief Interface functions for the longterm memory classes
    template <class _CoreSegmentT>
    class MemoryBase : public MemoryItem
    {
    public:
        struct Statistics
        {
            armarx::core::time::DateTime lastEnabled = armarx::core::time::DateTime::Invalid();
            long recordedCoreSegments = 0;
        };

    public:
        using CoreSegmentT = _CoreSegmentT;

        MemoryBase(const MemoryID& id) :
            MemoryItem(id)
        {
        }

        /// initialize config
        virtual void init()
        {
            enabled = enabled_on_startup;
            ARMARX_INFO << VAROUT(configuration_on_startup);

            try
            {
                const auto j = nlohmann::json::parse(configuration_on_startup);
                this->configure(j);
            }
            catch(...)
            {
                ARMARX_WARNING << "Failed to parse `" << configuration_on_startup << "`";
                enabled = false;
            }
        }

        /// return the full ltm as a wm::Memory with only references
        /// the ltm may be huge, use with caution
        armem::wm::Memory loadAllReferences()
        {
            armem::wm::Memory ret;
            loadAllReferences(ret);
            return ret;
        }
        void loadAllReferences(armem::wm::Memory& memory)
        {
            TIMING_START(LTM_Memory_LoadAll);
            _loadAllReferences(memory);
            TIMING_END_STREAM(LTM_Memory_LoadAll, ARMARX_DEBUG);
        }

        /// return the full ltm as a wm::Memory and resolves the references
        /// the ltm may be huge, use with caution
        armem::wm::Memory loadAllAndResolve()
        {
            armem::wm::Memory ret;
            loadAllAndResolve(ret);
            return ret;
        }

        void loadAllAndResolve(armem::wm::Memory& memory)
        {
            TIMING_START(LTM_Memory_LoadAllAndResolve);
            _loadAllReferences(memory);
            _resolve(memory);
            TIMING_END_STREAM(LTM_Memory_LoadAllAndResolve, ARMARX_DEBUG);
        }

        /// convert the references of the input into a wm::Memory
        void resolve(armem::wm::Memory& memory)
        {
            TIMING_START(LTM_Memory_Load);
            _resolve(memory);
            TIMING_END_STREAM(LTM_Memory_Load, ARMARX_DEBUG);
        }

        /// append a wm::Memory instance to the ltm
        void store(const armem::wm::Memory& memory)
        {
            TIMING_START(LTM_Memory_Append);
            for (auto& f : processors->memFilters)
            {
                if (f->enabled && !f->accept(memory))
                {
                    ARMARX_INFO << deactivateSpam() << "Ignoring to put a Memory into the LTM because it got filtered.";
                    return;
                }
            }
            _store(memory);
            TIMING_END_STREAM(LTM_Memory_Append, ARMARX_DEBUG);
        }

        /// append a wm::Memory instance to the ltm
        void store(const armem::server::wm::Memory& serverMemory)
        {
            wm::Memory memory;
            memory.update(armem::toCommit(serverMemory));
            this->store(memory);
        }

        /// iterate over all core segments of this ltm
        virtual bool forEachCoreSegment(std::function<void(CoreSegmentT&)>&& func) const = 0;

        /// find core segment
        virtual std::shared_ptr<CoreSegmentT> findCoreSegment(const std::string&) const = 0;

        /// default parameters. Implementation should use the configuration to configure
        virtual void createPropertyDefinitions(PropertyDefinitionsPtr& defs, const std::string& prefix)
        {
            defs->optional(enabled_on_startup, prefix + "enabled");
            defs->optional(configuration_on_startup, prefix + "configuration");
            //processors->createPropertyDefinitions(defs, prefix);
        }

        /// configuration
        virtual void configure(const nlohmann::json& config)
        {
            // Processors are shared. So we only need to configure the root
            processors->configure(config);
        }

        /// enable/disable
        void startRecording()
        {
            statistics.lastEnabled = armarx::core::time::DateTime::Now();
            enabled = true;
        }
        void stopRecording()
        {
            enabled = false;
        }
        bool isRecording() const
        {
            return enabled;
        }

        /// statistics
        virtual void resetStatistics()
        {
            // enabled stays the same
            statistics.lastEnabled = armarx::core::time::DateTime::Invalid();
            statistics.recordedCoreSegments = 0;
        }
        Statistics getStatistics() const
        {
            return statistics;
        }

        /// get level name
        static std::string getLevelName()
        {
            return "LT-Memory";
        }

    protected:
        virtual void _loadAllReferences(armem::wm::Memory& memory) = 0;
        virtual void _resolve(armem::wm::Memory& memory) = 0;
        virtual void _store(const armem::wm::Memory& memory) = 0;

    public:
        // stuff for scenario parameters
        bool enabled_on_startup = false;
        std::string configuration_on_startup = "{}";

    protected:
        mutable std::recursive_mutex ltm_mutex;

        Statistics statistics;

    private:
        std::atomic_bool enabled = false;

    };
} // namespace armarx::armem::server::ltm
