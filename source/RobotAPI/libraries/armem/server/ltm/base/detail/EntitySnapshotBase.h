#pragma once

#include <functional>

// BaseClass
#include "MemoryItem.h"

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/wm/aron_conversions.h>

namespace armarx::armem::server::ltm
{
    /// @brief Interface functions for the longterm memory classes
    class EntitySnapshotBase : public MemoryItem
    {
    public:
        struct Statistics
        {
            long recordedInstances = 0;

        };

    public:

        using MemoryItem::MemoryItem;

        /// return the full sub-ltm as a wm::EntitySnapshot with only references
        /// the ltm may be huge, use with caution
        void loadAllReferences(armem::wm::EntitySnapshot& e) const
        {
            _loadAllReferences(e);
        }

        /// convert the references of the input into a wm::Memory
        void resolve(armem::wm::EntitySnapshot& e) const
        {
            _resolve(e);
        }

        /// encode the content of a wm::Memory and store
        void store(const armem::wm::EntitySnapshot& e)
        {
            _store(e);
        }

        /// statistics
        void resetStatistics()
        {
            statistics.recordedInstances = 0;
        }
        Statistics getStatistics() const
        {
            return statistics;
        }

        static std::string getLevelName();

    protected:
        virtual void _loadAllReferences(armem::wm::EntitySnapshot&) const = 0;
        virtual void _resolve(armem::wm::EntitySnapshot&) const = 0;
        virtual void _store(const armem::wm::EntitySnapshot&) = 0;

    protected:
        mutable std::recursive_mutex ltm_mutex;

        Statistics statistics;
    };
} // namespace armarx::armem::server::ltm
