#include "Converter.h"

namespace armarx::armem::server::ltm
{

    std::pair<std::vector<unsigned char>, std::string> ImageConverter::convert(const aron::data::VariantPtr& data)
    {
        auto d = aron::data::NDArray::DynamicCastAndCheck(data);
        return _convert(d);
    }

    aron::data::VariantPtr ImageConverter::convert(const std::vector<unsigned char>& data, const std::string& m)
    {
        auto d = _convert(data, m);
        return d;
    }

}
