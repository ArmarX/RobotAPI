#pragma once

// Base Class
#include "../Converter.h"

namespace armarx::armem::server::ltm::converter::image
{
    class ExrConverter : public ImageConverter
    {
    public:
        ExrConverter() :
            ImageConverter(ConverterType::Binary, "depthimage", ".exr")
        {
            enabled = true; // enabled by default
        }

    protected:
        std::pair<std::vector<unsigned char>, std::string> _convert(const aron::data::NDArrayPtr& data) final;
        aron::data::NDArrayPtr _convert(const std::vector<unsigned char>& data, const std::string&) final;
    };
}
