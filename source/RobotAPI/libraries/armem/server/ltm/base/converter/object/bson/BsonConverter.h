#pragma once

// Base Class
#include "../Converter.h"

// ArmarX
#include "../json/JsonConverter.h"

namespace armarx::armem::server::ltm::converter::object
{
    class BsonConverter;
    using BsonConverterPtr = std::shared_ptr<BsonConverter>;

    class BsonConverter : public ObjectConverter
    {
    public:
        BsonConverter() :
            ObjectConverter(ConverterType::Binary, "dict", ".bson")
        {}

    protected:
        std::pair<std::vector<unsigned char>, std::string> _convert(const aron::data::DictPtr& data) final;
        aron::data::DictPtr _convert(const std::vector<unsigned char>& data, const std::string&) final;

    private:
        JsonConverter jsonConverter;
    };
}
