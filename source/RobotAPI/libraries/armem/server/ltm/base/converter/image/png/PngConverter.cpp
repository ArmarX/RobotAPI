#include "PngConverter.h"

// ArmarX
#include <RobotAPI/libraries/aron/converter/opencv/OpenCVConverter.h>

#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>


namespace armarx::armem::server::ltm::converter::image
{
    std::pair<std::vector<unsigned char>, std::string> PngConverter::_convert(const aron::data::NDArrayPtr& data)
    {
        ARMARX_CHECK_NOT_NULL(data);

        auto img = aron::converter::AronOpenCVConverter::ConvertToMat(data);
        std::vector<unsigned char> buffer;


        auto shape = data->getShape(); // we know from the extraction that the shape has 3 elements
        ARMARX_CHECK_EQUAL(shape.size(), 3);

        if (shape[2] == 3) // its probably a rgb image
        {
            cv::cvtColor(img, img, CV_RGB2BGR);
            cv::imencode(suffix, img, buffer);
            return std::make_pair(buffer, ".rgb");
        }

        if (shape[2] == 1) // its probably a grayscale image
        {
            cv::imencode(suffix, img, buffer);
            return std::make_pair(buffer, ".gs");
        }

        // try to export without conversion
        cv::imencode(suffix, img, buffer);
        return std::make_pair(buffer, "");
    }

    aron::data::NDArrayPtr PngConverter::_convert(const std::vector<unsigned char>& data, const std::string& m)
    {
        if (m == ".rgb")
        {
            cv::Mat img = cv::imdecode(data, cv::IMREAD_COLOR);
            cv::cvtColor(img, img, CV_BGR2RGB);
            return aron::converter::AronOpenCVConverter::ConvertFromMat(img);
        }

        if (m == ".gs")
        {
            cv::Mat img = cv::imdecode(data, cv::IMREAD_GRAYSCALE);
            return aron::converter::AronOpenCVConverter::ConvertFromMat(img);
        }

        // try to load without conversion
        cv::Mat img = cv::imdecode(data, cv::IMREAD_ANYCOLOR);
        return aron::converter::AronOpenCVConverter::ConvertFromMat(img);
    }
}
