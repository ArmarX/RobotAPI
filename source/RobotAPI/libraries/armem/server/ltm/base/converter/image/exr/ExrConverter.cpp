#include "ExrConverter.h"

// ArmarX
#include <RobotAPI/libraries/aron/converter/opencv/OpenCVConverter.h>

#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>


namespace armarx::armem::server::ltm::converter::image
{
    std::pair<std::vector<unsigned char>, std::string> ExrConverter::_convert(const aron::data::NDArrayPtr& data)
    {
        ARMARX_CHECK_NOT_NULL(data);

        auto img = aron::converter::AronOpenCVConverter::ConvertToMat(data);
        std::vector<unsigned char> buffer;

        auto shape = data->getShape(); // we know from the extraction that the shape has 3 elements
        ARMARX_CHECK_EQUAL(shape.size(), 3);
        ARMARX_CHECK_EQUAL(shape[2], 4);

        cv::imencode(".exr", img, buffer);
        return std::make_pair(buffer, "");
    }

    aron::data::NDArrayPtr ExrConverter::_convert(const std::vector<unsigned char>& data, const std::string& m)
    {
        cv::Mat img = cv::imdecode(data, cv::IMREAD_ANYDEPTH);
        return aron::converter::AronOpenCVConverter::ConvertFromMat(img);
    }
}
