#pragma once

// Base Class
#include "../Converter.h"

// Simox
#include <SimoxUtility/json.h>

namespace armarx::armem::server::ltm::converter::object
{
    class JsonConverter : public ObjectConverter
    {
    public:
        JsonConverter() :
            ObjectConverter(ConverterType::Str, "dict", ".json")
        {
            enabled = true; // always true!
        }

    protected:
        std::pair<std::vector<unsigned char>, std::string> _convert(const aron::data::DictPtr& data) final;
        aron::data::DictPtr _convert(const std::vector<unsigned char>& data, const std::string&) final;
    };
}
