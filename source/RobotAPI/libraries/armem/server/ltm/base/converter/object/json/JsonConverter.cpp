#include "JsonConverter.h"

#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

namespace armarx::armem::server::ltm::converter::object
{
    std::pair<std::vector<unsigned char>, std::string> JsonConverter::_convert(const aron::data::DictPtr& data)
    {
        nlohmann::json j = aron::converter::AronNlohmannJSONConverter::ConvertToNlohmannJSON(data);
        auto str = j.dump(2);
        return std::make_pair(std::vector<unsigned char>(str.begin(), str.end()), "");
    }

    aron::data::DictPtr JsonConverter::_convert(const std::vector<unsigned char>& data, const std::string&)
    {
        std::string str(data.begin(), data.end());
        nlohmann::json j = nlohmann::json::parse(str);
        return aron::converter::AronNlohmannJSONConverter::ConvertFromNlohmannJSONObject(j);
    }
}
