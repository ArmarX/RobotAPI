#include "BsonConverter.h"

#include <bsoncxx/json.hpp>
#include <bsoncxx/builder/stream/helpers.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/stream/array.hpp>

namespace armarx::armem::server::ltm::converter::object
{
    namespace bsoncxxbuilder = bsoncxx::builder::stream;
    namespace bsoncxxdoc = bsoncxx::document;

    std::pair<std::vector<unsigned char>, std::string> BsonConverter::_convert(const aron::data::DictPtr& data)
    {
        auto [jsonVec, str] = jsonConverter.convert(data);
        std::string json(jsonVec.begin(), jsonVec.end());
        auto view = bsoncxx::from_json(json).view();

        std::vector<unsigned char> bson(view.length());
        if (view.length() > 0)
        {
            std::memcpy(bson.data(), view.data(), view.length());
        }
        return std::make_pair(bson, str);
    }

    aron::data::DictPtr BsonConverter::_convert(const std::vector<unsigned char>& data, const std::string& m)
    {
        bsoncxx::document::view view(data.data(), data.size());
        nlohmann::json json = bsoncxx::to_json(view);
        std::string str = json.dump(2);
        std::vector<unsigned char> jsonVec(str.begin(), str.end());
        auto v = jsonConverter.convert(jsonVec, m);
        return aron::data::Dict::DynamicCast(v);
    }
}
