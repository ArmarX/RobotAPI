#pragma once

// STD/STL
#include <memory>

// BaseClass
#include "../Converter.h"

// ArmarX
#include <RobotAPI/libraries/aron/core/data/variant/complex/NDArray.h>

namespace armarx::armem::server::ltm
{
    class ImageConverter : public Converter
    {
    public:
        ImageConverter(const ConverterType t, const std::string& id, const std::string& s):
            Converter(t, id, s, aron::type::Descriptor::IMAGE)
        {}

        virtual ~ImageConverter() = default;

        std::pair<std::vector<unsigned char>, std::string> convert(const aron::data::VariantPtr& data) final;
        aron::data::VariantPtr convert(const std::vector<unsigned char>& data, const std::string&) final;

    protected:
        virtual std::pair<std::vector<unsigned char>, std::string> _convert(const aron::data::NDArrayPtr& data) = 0;
        virtual aron::data::NDArrayPtr _convert(const std::vector<unsigned char>& data, const std::string&) = 0;
    };
}
