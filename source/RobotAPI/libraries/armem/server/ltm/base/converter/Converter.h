#pragma once

// STD/STL
#include <memory>

// ArmarX
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>

namespace armarx::armem::server::ltm
{
    class Converter
    {
    public:
        enum class ConverterType
        {
            Str,
            Binary
        };

        Converter(const ConverterType t, const std::string& id, const std::string& s, const aron::type::Descriptor c):
            type(t),
            identifier(id),
            suffix(s),
            convertsType(c)
        {}
        virtual ~Converter() = default;

        virtual std::pair<std::vector<unsigned char>, std::string> convert(const aron::data::VariantPtr& data) = 0;
        virtual aron::data::VariantPtr convert(const std::vector<unsigned char>& data, const std::string&) = 0;

    public:
        const ConverterType type;
        const std::string identifier;
        const std::string suffix;
        const aron::type::Descriptor convertsType;
        bool enabled = false;
    };
}
