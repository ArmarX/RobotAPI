#pragma once

// STD/STL
#include <memory>

// ArmarX
#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>

namespace armarx::armem::server::ltm
{
    class TypeConverter
    {
    public:
        TypeConverter(const std::string& id, const std::string& s):
            identifier(id),
            suffix(s)
        {}
        virtual ~TypeConverter() = default;

        virtual std::pair<std::vector<unsigned char>, std::string> convert(const aron::type::ObjectPtr& data) = 0;
        virtual aron::type::ObjectPtr convert(const std::vector<unsigned char>& data, const std::string&) = 0;

    public:
        const std::string identifier;
        const std::string suffix;
        bool enabled = false;
    };
}
