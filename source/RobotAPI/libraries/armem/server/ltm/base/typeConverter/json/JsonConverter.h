#pragma once

// Base Class
#include "../Converter.h"

// Simox
#include <SimoxUtility/json.h>

namespace armarx::armem::server::ltm::converter::type
{
    class JsonConverter : public TypeConverter
    {
    public:
        JsonConverter() :
            TypeConverter("dict", ".json")
        {
            enabled = true; // always true!
        }

        std::pair<std::vector<unsigned char>, std::string> convert(const aron::type::ObjectPtr& data) final;
        aron::type::ObjectPtr convert(const std::vector<unsigned char>& data, const std::string&) final;
    };
}
