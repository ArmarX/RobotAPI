#pragma once

// Base Class
#include "../Extractor.h"

#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>

namespace armarx::armem::server::ltm::extractor
{
    class DepthImageExtractorVisitor : public aron::data::RecursiveVariantVisitor
    {
    public:
        std::map<std::string, aron::data::VariantPtr> depthImages;

        void visitDictOnEnter(Input& data);
        void visitUnknown(Input& data);
    };

    class DepthImageExtractor : public Extractor
    {
    public:
        DepthImageExtractor() :
            Extractor(aron::type::Descriptor::IMAGE, "depthimage")
        {
            enabled = true;
        };

        virtual Extraction extract(aron::data::DictPtr& data) override;
        virtual aron::data::DictPtr merge(Extraction& encoding) override;
    };
}
