#pragma once

// STD/STL
#include <memory>

// ArmarX
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/data/variant/complex/NDArray.h>

namespace armarx::armem::server::ltm
{
    class Extractor
    {
    public:
        struct Extraction
        {
            aron::data::DictPtr dataWithoutExtraction;
            std::map<std::string, aron::data::VariantPtr> extraction;
        };

        Extractor(const aron::type::Descriptor t, const std::string& id) : extractsType(t), identifier(id) {};
        virtual ~Extractor() = default;

        virtual Extraction extract(aron::data::DictPtr& data) = 0;
        virtual aron::data::DictPtr merge(Extraction& encoding) = 0;

        const aron::type::Descriptor extractsType;
        const std::string identifier;
        bool enabled = false;
    };
}
