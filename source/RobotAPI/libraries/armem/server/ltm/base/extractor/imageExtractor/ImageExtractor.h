#pragma once

// Base Class
#include "../Extractor.h"

#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>

namespace armarx::armem::server::ltm::extractor
{
    class ImageExtractorVisitor : public aron::data::RecursiveVariantVisitor
    {
    public:
        std::map<std::string, aron::data::VariantPtr> images;

        void visitDictOnEnter(Input& data);
        void visitUnknown(Input& data);
    };

    class ImageExtractor : public Extractor
    {
    public:
        ImageExtractor() :
            Extractor(aron::type::Descriptor::IMAGE, "image")
        {
            enabled = true;
        };

        virtual Extraction extract(aron::data::DictPtr& data) override;
        virtual aron::data::DictPtr merge(Extraction& encoding) override;
    };
}
