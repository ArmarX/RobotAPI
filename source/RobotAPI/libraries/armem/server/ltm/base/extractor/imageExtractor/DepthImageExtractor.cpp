#include "DepthImageExtractor.h"


namespace armarx::armem::server::ltm::extractor
{
    void DepthImageExtractorVisitor::visitDictOnEnter(Input& data)
    {
        ARMARX_CHECK_NOT_NULL(data);

        auto dict = aron::data::Dict::DynamicCastAndCheck(data);
        for (const auto& [key, child] : dict->getElements())
        {
            if (child && child->getDescriptor() == aron::data::Descriptor::NDARRAY)
            {
                auto ndarray = aron::data::NDArray::DynamicCastAndCheck(child);
                auto shape = ndarray->getShape();
                if (shape.size() == 3 && shape[2] == 4 && std::accumulate(std::begin(shape), std::end(shape), 1, std::multiplies<int>()) > 200) // must be big enough to assume an image (instead of 4x4x4 poses)
                {
                    depthImages[key] = ndarray;
                    dict->setElement(key, nullptr);
                }
            }
        }
    }

    void DepthImageExtractorVisitor::visitUnknown(Input&)
    {
        // A member is null. Simply ignore...
    }

    Extractor::Extraction DepthImageExtractor::extract(aron::data::DictPtr& data)
    {
        DepthImageExtractorVisitor visitor;
        aron::data::VariantPtr var = std::static_pointer_cast<aron::data::Variant>(data);
        aron::data::VariantPtr p;
        aron::data::visitRecursive(visitor, var);

        Extraction encoding;
        encoding.dataWithoutExtraction = data;
        encoding.extraction = visitor.depthImages;
        return encoding;
    }

    aron::data::DictPtr DepthImageExtractor::merge(Extraction& encoding)
    {
        return encoding.dataWithoutExtraction;
    }
}
