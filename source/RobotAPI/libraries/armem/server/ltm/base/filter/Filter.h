#pragma once

// STD/STL
#include <memory>

// ArmarX
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>

namespace armarx::armem::server::ltm
{
    class MemoryFilter
    {
    public:
        MemoryFilter() = default;
        virtual ~MemoryFilter() = default;

        virtual bool accept(const armem::wm::Memory& e) = 0;

        bool enabled = false;
    };

    class SnapshotFilter
    {
    public:
        SnapshotFilter() = default;
        virtual ~SnapshotFilter() = default;

        virtual bool accept(const armem::wm::EntitySnapshot& e) = 0;

        bool enabled = false;
    };
}
