#include "EqualityFilter.h"

#include <IceUtil/Time.h>

namespace armarx::armem::server::ltm::filter
{
    bool SnapshotEqualityFilter::accept(const armem::wm::EntitySnapshot& e)
    {
        auto entityID = e.id().getEntityID();
        auto genMs = e.time().toMilliSecondsSinceEpoch();

        long lastMs = 0;
        std::vector<aron::data::DictPtr> lastData;
        if (timestampLastCommitInMs.count(entityID) > 0)
        {
            lastData = dataLastCommit.at(entityID);
            lastMs = timestampLastCommitInMs.at(entityID);
        }

        auto timePassedSinceLastStored = genMs - lastMs;
        if (maxWaitingTimeInMs < 0 || timePassedSinceLastStored > maxWaitingTimeInMs)
        {
            bool accept = false;
            std::vector<aron::data::DictPtr> genData;
            for (unsigned int i = 0; i != e.size(); ++i)
            {
                const auto& d = e.getInstance(i).data();
                genData.push_back(d);

                if (lastMs == 0 || e.size() != lastData.size()) // nothing stored yet or we cannot compare
                {
                    accept = true;
                    break;
                }

                const auto& el = lastData.at(i);
                if ((!d and el) || (d and !el) || (d && el && !(*d == *el))) // data unequal?
                {
                    accept = true;
                    break;
                }
            }

            if (!accept) return false;

            dataLastCommit[entityID] = genData;
            timestampLastCommitInMs[entityID] = genMs;
            return true;
        }
        return false;
    }
}
