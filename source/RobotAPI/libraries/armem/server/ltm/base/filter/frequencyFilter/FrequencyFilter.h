#pragma once

#include <map>

// Base Class
#include "../Filter.h"

namespace armarx::armem::server::ltm::filter
{
    class MemoryFrequencyFilter :
            public MemoryFilter
    {
    public:
        MemoryFrequencyFilter() = default;

        virtual bool accept(const armem::wm::Memory& e) override;

    public:
        int waitingTimeInMs = -1;

    private:
        long timestampLastCommitInMs = 0;
    };

    class SnapshotFrequencyFilter :
            public SnapshotFilter
    {
    public:
        SnapshotFrequencyFilter() = default;

        virtual bool accept(const armem::wm::EntitySnapshot& e) override;

    public:
        int waitingTimeInMs = -1;

    private:
        std::map<MemoryID, long> timestampLastCommitInMs;
    };
}
