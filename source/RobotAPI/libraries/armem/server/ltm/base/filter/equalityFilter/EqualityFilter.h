#pragma once

#include <vector>
#include <map>

// Base Class
#include "../Filter.h"

// Aron
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>

namespace armarx::armem::server::ltm::filter
{
    class SnapshotEqualityFilter :
            public SnapshotFilter
    {
    public:
        SnapshotEqualityFilter() = default;

        virtual bool accept(const armem::wm::EntitySnapshot& e) override;

    public:
        int maxWaitingTimeInMs = -1;

    private:
        std::map<MemoryID, std::vector<aron::data::DictPtr>> dataLastCommit;
        std::map<MemoryID, long> timestampLastCommitInMs;
    };
}
