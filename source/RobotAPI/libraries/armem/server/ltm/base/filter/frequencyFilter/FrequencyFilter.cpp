#include "FrequencyFilter.h"

#include <IceUtil/Time.h>

namespace armarx::armem::server::ltm::filter
{
    bool MemoryFrequencyFilter::accept(const armem::wm::Memory& e)
    {
        auto now = armem::Time::Now().toMilliSecondsSinceEpoch();
        if (waitingTimeInMs < 0 || (now - timestampLastCommitInMs) > waitingTimeInMs)
        {
            timestampLastCommitInMs = now;
            return true;
        }
        return false;
    }

    bool SnapshotFrequencyFilter::accept(const armem::wm::EntitySnapshot& e)
    {
        auto entityID = e.id().getEntityID();
        auto genMs = e.time().toMilliSecondsSinceEpoch();

        long lastMs = 0;
        if (timestampLastCommitInMs.count(entityID) > 0)
        {
            lastMs = timestampLastCommitInMs.at(entityID);
        }

        if (waitingTimeInMs < 0 || (genMs - lastMs) > waitingTimeInMs)
        {
            /*std::cout << "diff: " << (dataGeneratedInMs - timestampLastCommitInMs) << std::endl;
            std::cout << "gen: " << (dataGeneratedInMs) << std::endl;
            std::cout << "last: " << (timestampLastCommitInMs) << std::endl;*/
            timestampLastCommitInMs[entityID] = genMs;
            return true;
        }
        return false;
    }
}
