#include "ConnectionManager.h"

#include <iostream>
#include <fstream>
#include <algorithm>

namespace armarx::armem::server::ltm::mongodb
{
    std::mutex ConnectionManager::initializationMutex;
    bool ConnectionManager::initialized = false;
    std::map<std::string, std::unique_ptr<mongocxx::pool>> ConnectionManager::Connections = {};

    ConnectionManager::MongoDBSettings::MongoDBSettings()
    {
        std::string armarx_home = std::string(getenv("HOME")) + "/.armarx";
        if (getenv("ARMARX_DEFAULTS_DIR"))
        {
            armarx_home = getenv("ARMARX_DEFAULTS_DIR");
        }
        std::ifstream cFile (armarx_home + "/default.cfg");
        if (cFile.is_open())
        {
            std::string line;
            while(getline(cFile, line))
            {
                line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
                if(line[0] == '#' || line.empty())
                {
                    continue;
                }
                auto delimiterPos = line.find("=");
                const auto name = line.substr(0, delimiterPos);
                const auto value = line.substr(delimiterPos + 1);
                if (name == "ArmarX.MongoHost")
                {
                    host = value;
                }
                if (name == "ArmarX.MongoPort")
                {
                    port = (unsigned int) std::stoi(value);
                }
                if (name == "ArmarX.MongoUser")
                {
                    user = value;
                }
                if (name == "ArmarX.MongoPassword")
                {
                    password = value;
                }
            }
        }
    }

    bool ConnectionManager::MongoDBSettings::isSet() const
    {
        // we always need a host and a port
        return !host.empty() and port != 0;
    }

    std::string ConnectionManager::MongoDBSettings::baseUri() const
    {
        std::stringstream ss;
        ss << "mongodb://";

        if (!user.empty())
        {
            ss << user;
            if (!password.empty())
            {
                ss << ":" << password;
            }
            ss << "@";
        }
        ss << host;
        return ss.str();
    }

    std::string ConnectionManager::MongoDBSettings::key() const
    {
        // TODO: What happens if a connection exists and you would like to open another one with a different user (e.g. that sees different things)?
        return "mongodb://" + host + ":" + std::to_string(port);
    }

    std::string ConnectionManager::MongoDBSettings::uri() const
    {
        return baseUri() + ":" + std::to_string(port) + "/?minPoolSize=" + std::to_string(minPoolSize) + "&maxPoolSize=" + std::to_string(maxPoolSize);
    }

    std::string ConnectionManager::MongoDBSettings::toString() const
    {
        return uri() + "&database=" + database;
    }

    void ConnectionManager::initialize_if()
    {
        std::lock_guard l(initializationMutex); // all others have to wait until the initialization is complete
        if (!initialized)
        {
            initialized = true;
            mongocxx::instance instance{}; // This should be done only once.
        }
    }

    mongocxx::pool& ConnectionManager::Connect(const MongoDBSettings& settings)
    {
        initialize_if();

        const auto uri_str = settings.uri();
        auto it = Connections.find(uri_str);
        if (it == Connections.end())
        {
            mongocxx::uri uri(uri_str);
            auto pool = std::make_unique<mongocxx::pool>(uri);
            auto con = Connections.emplace(settings.key(), std::move(pool));
            return *con.first->second;
        }
        else
        {
            // A connection already exists. We do not need to open another one.
            return *it->second;
        }
    }

    bool ConnectionManager::ConnectionIsValid(const MongoDBSettings& settings, bool forceNewConnection)
    {
        initialize_if();

        try
        {
            if (!forceNewConnection)
            {
                auto it = Connections.find(settings.key());
                if (it != Connections.end())
                {
                    auto client = it->second->acquire();
                    auto admin = client->database("admin");
                    auto result = admin.run_command(bsoncxx::builder::basic::make_document(bsoncxx::builder::basic::kvp("isMaster", 1)));
                    return true;
                }
            }

            mongocxx::uri uri(settings.uri());
            auto client = mongocxx::client(uri);
            auto admin = client["admin"];
            auto result = admin.run_command(bsoncxx::builder::basic::make_document(bsoncxx::builder::basic::kvp("isMaster", 1)));
            return true;
        }
        catch (const std::exception& xcp)
        {
            return false;
        }
    }

    bool ConnectionManager::ConnectionExists(const MongoDBSettings& settings)
    {
        initialize_if();

        auto it = Connections.find(settings.key());
        return it != Connections.end();
    }
}
