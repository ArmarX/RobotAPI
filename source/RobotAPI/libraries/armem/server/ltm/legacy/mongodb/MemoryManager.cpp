// Header
#include "MemoryManager.h"

// Simox
#include <SimoxUtility/json.h>

// ArmarX
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/armem/core/wm/aron_conversions.h>
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

#include "operations.h"

namespace armarx::armem::server::ltm::mongodb
{
    namespace bsoncxxbuilder = bsoncxx::builder::stream;
    namespace bsoncxxdoc = bsoncxx::document;

    PoolClientPtr Memory::checkConnection() const
    {
        // Check connection:
        if (!ConnectionManager::ConnectionIsValid(dbsettings))
        {
            ARMARX_WARNING << deactivateSpam("LTM_ConnectionError_" + cache.name())
                           << "The connection to mongocxx for ltm '" << cache.name() << "' is not valid. Settings are: " << dbsettings.toString()
                           << "\nTo start it, run e.g.: \n"
                           << "armarx memory start"
                           << "\n\n";
            return nullptr;
        }

        auto& pool = ConnectionManager::Connect(dbsettings);
        auto client = pool.acquire();

        return client;
    }

    void Memory::reload()
    {
        TIMING_START(LTM_Reload);
        ARMARX_DEBUG << "(Re)Establishing connection to: " << dbsettings.toString();

        auto client = checkConnection();
        if (!client)
        {
            return;
        }

        std::lock_guard l(ltm_mutex);
        auto databases = client->list_databases();
        std::stringstream ss;
        ss << "Found Memory-Collection in MongoDB for '" + cache.name() + "': \n";
        for (const auto& doc : databases)
        {
            auto el = doc["name"];
            ss << "\t - " << el.get_utf8().value << "\n";
        }
        ARMARX_DEBUG << ss.str();

        armem::wm::Memory temp(lut.id());
        mongocxx::database db = client->database(dbsettings.database);
        util::load(db, temp);

        lut.append(temp);

        TIMING_END_STREAM(LTM_Reload, ARMARX_DEBUG);
    }

    void Memory::convert(armem::wm::Memory& m)
    {
        TIMING_START(LTM_Convert);

        auto client = checkConnection();
        if (!client)
        {
            return;
        }

        std::lock_guard l(ltm_mutex);
        mongocxx::database db = client->database(dbsettings.database);

        util::convert(db, m);

        TIMING_END_STREAM(LTM_Convert, ARMARX_DEBUG);
    }

    void Memory::encodeAndStore()
    {
        TIMING_START(LTM_Encode);

        auto client = checkConnection();
        if (!client)
        {
            return;
        }

        std::lock_guard l(ltm_mutex);
        mongocxx::database db = client->database(dbsettings.database);
        util::store(db, cache);

        // what to do with clear text data after encoding?
        // TODO!

        // Finaly clear cache and put reference to lut
        moveCacheToLUTAndClearCache();

        TIMING_END_STREAM(LTM_Encode, ARMARX_DEBUG);
    }
}
