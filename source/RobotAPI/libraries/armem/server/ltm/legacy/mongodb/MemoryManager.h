#pragma once

// STD / STL
#include <mutex>
#include <optional>

// Base Class
#include "../MemoryBase.h"

// Data
# include "ConnectionManager.h"

namespace armarx::armem::server::ltm::mongodb
{
    /// @brief A memory storing data in mongodb (needs 'armarx memory start' to start the mongod instance)
    class Memory :
        public MemoryBase
    {
        using Base = MemoryBase;

    public:
        using Base::MemoryBase;
        using Base::convert;

        Memory() = default;

        void reload() override;
        void convert(armem::wm::Memory&) override;
        void encodeAndStore() override;

    private:
        PoolClientPtr checkConnection() const; // return nullptr if not possible

    public:
        ConnectionManager::MongoDBSettings dbsettings;

    private:

    };
}
