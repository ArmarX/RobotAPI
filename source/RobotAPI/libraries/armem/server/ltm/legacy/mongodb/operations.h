#pragma once

#include <RobotAPI/libraries/armem/core/forward_declarations.h>
#include <RobotAPI/libraries/armem/server/forward_declarations.h>

// Data
# include "ConnectionManager.h"

namespace armarx::armem::server::ltm::mongodb
{

    namespace constantes
    {
        const std::string FOREIGN_KEY = "foreign_key";
        const std::string ID = "id";
        const std::string TIMESTAMP = "timestamp";
        const std::string INSTANCES = "instances";
    }

    namespace util
    {
        void load(const mongocxx::database& db, armem::wm::Memory& memory);
        void load(const mongocxx::database& db, armem::wm::CoreSegment& memory);
        void load(const mongocxx::database& db, armem::wm::ProviderSegment& memory);
        void load(const mongocxx::database& db, armem::wm::Entity& memory);

        void convert(const mongocxx::database& db, armem::wm::Memory& memory);
        void convert(const mongocxx::database& db, armem::wm::CoreSegment& memory);
        void convert(const mongocxx::database& db, armem::wm::ProviderSegment& memory);
        void convert(const mongocxx::database& db, armem::wm::Entity& memory);

        void store(const mongocxx::database& db, const armem::wm::Memory& memory);
        void store(const mongocxx::database& db, const armem::wm::CoreSegment& memory);
        void store(const mongocxx::database& db, const armem::wm::ProviderSegment& memory);
        void store(const mongocxx::database& db, const armem::wm::Entity& memory);
    }

} // namespace armarx::armem::server::ltm::mongodb
