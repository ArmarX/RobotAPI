#pragma once

#include <string>
#include <mutex>
#include <map>
#include <memory>
#include <sstream>

#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/pool.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/instance.hpp>
#include <bsoncxx/builder/stream/helpers.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/stream/array.hpp>


namespace armarx::armem::server::ltm::mongodb
{

    using PoolClientPtr = mongocxx::pool::entry;

    /**
     * @brief A manager of multiple mongodb connection
     */
    class ConnectionManager
    {
    public:
        ConnectionManager() = delete;

        struct MongoDBSettings
        {
            std::string host = "";
            unsigned int port = 0;
            std::string user = "";
            std::string password = "";
            std::string database = "Test";
            int minPoolSize = 5;
            int maxPoolSize = 100;

            MongoDBSettings();

            bool isSet() const;

            std::string baseUri() const;

            std::string key() const;

            std::string uri() const;

            std::string toString() const;
        };

        static mongocxx::pool& Connect(const MongoDBSettings& settings);
        static bool ConnectionIsValid(const MongoDBSettings& settings, bool forceNewConnection = false);
        static bool ConnectionExists(const MongoDBSettings& settings);

    private:
        static void initialize_if();


    private:
        static std::mutex initializationMutex;
        static bool initialized;
        static std::map<std::string, std::unique_ptr<mongocxx::pool>> Connections;

    };
}
