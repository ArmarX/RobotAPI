#include "RemoteGuiAronDataVisitor.h"

#include <SimoxUtility/algorithm/string.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::armem::server
{

    RemoteGuiAronDataVisitor::Group::Group(const std::string& label)
    {
        groupBox.setLabel(label);
        groupBox.addChild(layout);
    }

    bool RemoteGuiAronDataVisitor::visitEnter(const std::string& key, const std::string& type, size_t size)
    {
        std::stringstream label;
        label << key << " (" << type << " of size " << size << ")";
        Group& group = groups.emplace(label.str());
        (void) group;
        return true;
    }

    bool RemoteGuiAronDataVisitor::visitExit()
    {
        Group group = groups.top();
        groups.pop();
        if (groups.size() > 0)
        {
            groups.top().groupBox.addChild(group.groupBox);
        }
        else
        {
            result = group.groupBox;
        }
        return true;
    }

    void RemoteGuiAronDataVisitor::streamValueText(aron::data::String& n, std::stringstream& ss)
    {
        ss << "'" << n.getValue() << "'";
    }

    void RemoteGuiAronDataVisitor::streamValueText(aron::data::NDArray& n, std::stringstream& ss)
    {
        ss << "shape (" << simox::alg::join(simox::alg::multi_to_string(n.getShape()), ", ") << ")";
    }

    void RemoteGuiAronDataVisitor::checkGroupsNotEmpty() const
    {
        ARMARX_CHECK_POSITIVE(groups.size()) << "Groups must not be empty.";
    }





}
