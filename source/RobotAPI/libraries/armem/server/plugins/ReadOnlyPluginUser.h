#pragma once

#include <RobotAPI/libraries/armem/server/forward_declarations.h>

#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>
#include <RobotAPI/interface/armem/server/MemoryInterface.h>

#include <ArmarXCore/core/ManagedIceObject.h>


namespace armarx::armem::server::plugins
{

    class Plugin;


    /**
     * @brief Base class of memory server components.
     *
     * Implements the server ice interfaces using the ice adapter of the plugin.
     */
    class ReadOnlyPluginUser :
        virtual public ManagedIceObject
        , virtual public ReadingMemoryInterface
        , virtual public client::plugins::PluginUser
    {
    public:

        ReadOnlyPluginUser();
        virtual ~ReadOnlyPluginUser() override;


        void setMemoryName(const std::string& memoryName);


        // ReadingInterface interface
        virtual armem::query::data::Result query(
                const armem::query::data::Input& input,
                const Ice::Current& = Ice::emptyCurrent) override;


        virtual armem::structure::data::GetServerStructureResult
        getServerStructure(const Ice::Current& = Ice::emptyCurrent) override;


    public:

        Plugin& memoryServerPlugin();

        server::wm::Memory& workingMemory();
        MemoryToIceAdapter& iceAdapter();


    private:

        plugins::Plugin* plugin = nullptr;

    };

}

namespace armarx::armem::server
{
    using plugins::ReadOnlyPluginUser;
}
