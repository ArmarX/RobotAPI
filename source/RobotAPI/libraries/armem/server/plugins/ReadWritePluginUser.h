#pragma once

#include <RobotAPI/libraries/armem/server/forward_declarations.h>

#include <RobotAPI/libraries/armem/client/plugins/ListeningPluginUser.h>
#include <RobotAPI/libraries/armem/core/actions.h>
#include <RobotAPI/interface/armem/server/MemoryInterface.h>

#include <ArmarXCore/core/ManagedIceObject.h>


namespace armarx::armem::server::plugins
{

    class Plugin;


    /**
     * @brief Base class of memory server components.
     *
     * Implements the server ice interfaces using the ice adapter of the plugin.
     */
    class ReadWritePluginUser :
        virtual public ManagedIceObject
        , virtual public MemoryInterface
        , virtual public client::plugins::ListeningPluginUser
    {
    public:

        ReadWritePluginUser();
        virtual ~ReadWritePluginUser() override;


        void setMemoryName(const std::string& memoryName);


        // WritingInterface interface
        virtual data::AddSegmentsResult addSegments(const data::AddSegmentsInput& input, const Ice::Current& = Ice::emptyCurrent) override;
        data::AddSegmentsResult addSegments(const data::AddSegmentsInput& input, bool addCoreSegments);

        virtual data::CommitResult commit(const data::Commit& commit, const Ice::Current& = Ice::emptyCurrent) override;


        // ReadingInterface interface
        virtual armem::query::data::Result query(const armem::query::data::Input& input, const Ice::Current& = Ice::emptyCurrent) override;
        virtual armem::structure::data::GetServerStructureResult getServerStructure(const Ice::Current& = Ice::emptyCurrent) override;


        // StoringInterface interface
        virtual dto::DirectlyStoreResult directlyStore(const dto::DirectlyStoreInput&, const Ice::Current& = Ice::emptyCurrent) override;
        virtual dto::StartRecordResult startRecord(const dto::StartRecordInput& startRecordInput, const Ice::Current& = Ice::emptyCurrent) override;
        virtual dto::StopRecordResult stopRecord(const Ice::Current& = Ice::emptyCurrent) override;
        virtual dto::RecordStatusResult getRecordStatus(const Ice::Current& = Ice::emptyCurrent) override;

        // ActionsInterface interface
        virtual armem::actions::GetActionsOutputSeq getActions(const armem::actions::GetActionsInputSeq& inputs);
        virtual armem::actions::ExecuteActionOutputSeq executeActions(const armem::actions::ExecuteActionInputSeq& inputs);

        virtual armem::actions::GetActionsOutputSeq getActions(const armem::actions::GetActionsInputSeq& inputs, const ::Ice::Current&) override;
        virtual armem::actions::ExecuteActionOutputSeq executeActions(const armem::actions::ExecuteActionInputSeq& inputs, const ::Ice::Current&) override;

        // PredictingInterface interface
        virtual armem::prediction::data::PredictionResultSeq predict(const armem::prediction::data::PredictionRequestSeq& requests);

        // Unless you need very unusual behavior from this method for your memory server,
        // it is better to set the available prediction engines in the memory itself
        // and let it handle the requests than to override this.
        virtual armem::prediction::data::EngineSupportMap getAvailableEngines();

        virtual armem::prediction::data::PredictionResultSeq predict(const armem::prediction::data::PredictionRequestSeq& requests, const ::Ice::Current&) override;
        virtual armem::prediction::data::EngineSupportMap getAvailableEngines(const ::Ice::Current&) override;

    public:

        Plugin& memoryServerPlugin();

        server::wm::Memory& workingMemory();
        MemoryToIceAdapter& iceAdapter();

        server::ltm::disk::Memory& longtermMemory();


    private:

        plugins::Plugin* plugin = nullptr;

    };

}

namespace armarx::armem::server
{
    using plugins::ReadWritePluginUser;
}

