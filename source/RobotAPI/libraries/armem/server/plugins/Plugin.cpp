#include "Plugin.h"
#include "ArmarXCore/util/CPPUtility/trace.h"

#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/client/util/MemoryListener.h>
#include <RobotAPI/libraries/armem/client/plugins/Plugin.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::armem::server::plugins
{

    Plugin::~Plugin() = default;


    Plugin::Plugin(ManagedIceObject& parent, std::string prefix) :
        armarx::ComponentPlugin(parent, prefix)
    {
        ARMARX_TRACE;
        addPlugin(clientPlugin);
        ARMARX_CHECK_NOT_NULL(clientPlugin);
        addPluginDependency(clientPlugin);
    }


    void Plugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {
        const std::string prefix = "mem.";

        // set Memory name if not already set
        if (workingMemory.name().empty())
        {
            Component& parent = this->parent<Component>();
            workingMemory.name() = parent.getName();
        }

        // also add scenario param to overwrite the chosen name
        if (not properties->hasDefinition(prefix + "MemoryName"))
        {
            properties->optional(workingMemory.name(), prefix + "MemoryName", "Name of this memory server.");
        }

        // stuff for ltm
        longtermMemory.createPropertyDefinitions(properties, prefix + "ltm.");
    }


    void Plugin::preOnInitComponent()
    {
        ARMARX_TRACE;
        memoryTopicName = client::util::MemoryListener::MakeMemoryTopicName(MemoryID(workingMemory.name()));
        parent().offeringTopic(memoryTopicName);
    }


    void Plugin::postOnInitComponent()
    {
        Component& parent = this->parent<Component>();

        // activate LTM
        ARMARX_TRACE;
        longtermMemory.setMemoryID(workingMemory.id(), parent.getName());
        longtermMemory.init();

        initialized = true;
    }


    void Plugin::postOnConnectComponent()
    {
        Component& parent = this->parent<Component>();

        // register to MNS
        if (clientPlugin->isMemoryNameSystemEnabled() and clientPlugin->getMemoryNameSystemClient())
        {
            registerServer(parent);
        }
        parent.getTopic(memoryTopic, memoryTopicName);
        iceAdapter.setMemoryListener(memoryTopic);

        connected = true;
    }


    void Plugin::preOnDisconnectComponent()
    {
        if (clientPlugin->isMemoryNameSystemEnabled() and clientPlugin->getMemoryNameSystemClient())
        {
            removeServer();
        }
    }


    void Plugin::setMemoryName(const std::string& memoryName)
    {
        if (initialized)
        {
            ARMARX_WARNING << "Please set the memory name before initializing the component. Otherwise the WM and LTM may have different names";
        }

        workingMemory.name() = memoryName;
    }


    mns::dto::RegisterServerResult Plugin::registerServer(armarx::Component& parent)
    {
        ARMARX_TRACE;

        MemoryID id = MemoryID().withMemoryName(workingMemory.name());
        mns::dto::MemoryServerInterfaces server;
        server.reading = ReadingMemoryInterfacePrx::uncheckedCast(parent.getProxy());
        server.writing = WritingMemoryInterfacePrx::uncheckedCast(parent.getProxy());
        server.prediction = PredictingMemoryInterfacePrx::uncheckedCast(parent.getProxy());
        server.actions = actions::ActionsInterfacePrx::uncheckedCast(parent.getProxy());

        mns::dto::RegisterServerResult result;
        try
        {
            clientPlugin->getMemoryNameSystemClient().registerServer(id, server);
            result.success = true;
            ARMARX_DEBUG << "Registered memory server for " << id << " in the Memory Name System (MNS).";
        }
        catch (const armem::error::ServerRegistrationOrRemovalFailed& e)
        {
            result.success = false;
            result.errorMessage = e.what();
            ARMARX_WARNING << e.what();
        }
        return result;
    }


    mns::dto::RemoveServerResult Plugin::removeServer()
    {
        MemoryID id = MemoryID().withMemoryName(workingMemory.name());

        mns::dto::RemoveServerResult result;
        try
        {
            clientPlugin->getMemoryNameSystemClient().removeServer(id);
            result.success = true;
            ARMARX_DEBUG << "Removed memory server for " << id << " from the Memory Name System (MNS).";
        }
        catch (const armem::error::ServerRegistrationOrRemovalFailed& e)
        {
            result.success = false;
            result.errorMessage = e.what();
            ARMARX_WARNING << e.what();
        }
        catch (const Ice::NotRegisteredException&)
        {
            // It's ok, the MNS is gone.
            result.success = false;
            result.errorMessage = "Memory Name System is gone.";
        }
        return result;
    }

}
