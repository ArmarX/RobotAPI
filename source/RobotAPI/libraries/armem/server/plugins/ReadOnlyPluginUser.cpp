#include "ReadOnlyPluginUser.h"
#include "Plugin.h"

#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>



namespace armarx::armem::server::plugins
{

    ReadOnlyPluginUser::ReadOnlyPluginUser()
    {
        addPlugin(plugin);
    }


    ReadOnlyPluginUser::~ReadOnlyPluginUser()
    {
    }


    void ReadOnlyPluginUser::setMemoryName(const std::string& memoryName)
    {
        plugin->setMemoryName(memoryName);
    }


    armem::query::data::Result ReadOnlyPluginUser::query(const armem::query::data::Input& input, const Ice::Current&)
    {
        ARMARX_TRACE;
        return iceAdapter().query(input);
    }

    structure::data::GetServerStructureResult ReadOnlyPluginUser::getServerStructure(const Ice::Current&)
    {
        ARMARX_TRACE;
        return iceAdapter().getServerStructure();
    }


    Plugin& ReadOnlyPluginUser::memoryServerPlugin()
    {
        return *plugin;
    }


    wm::Memory& ReadOnlyPluginUser::workingMemory()
    {
        return plugin->workingMemory;
    }


    MemoryToIceAdapter& ReadOnlyPluginUser::iceAdapter()
    {
        return plugin->iceAdapter;
    }

}
