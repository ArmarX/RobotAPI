#include "ReadWritePluginUser.h"
#include "Plugin.h"

#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/Prediction.h>
#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>



namespace armarx::armem::server::plugins
{

    ReadWritePluginUser::ReadWritePluginUser()
    {
        addPlugin(plugin);
    }


    ReadWritePluginUser::~ReadWritePluginUser()
    {
    }


    void ReadWritePluginUser::setMemoryName(const std::string& memoryName)
    {
        plugin->setMemoryName(memoryName);
    }


    // WRITING
    data::AddSegmentsResult ReadWritePluginUser::addSegments(const data::AddSegmentsInput& input, const Ice::Current&)
    {
        ARMARX_TRACE;
        bool addCoreSegmentOnUsage = false;
        return addSegments(input, addCoreSegmentOnUsage);
    }

    data::AddSegmentsResult ReadWritePluginUser::addSegments(const data::AddSegmentsInput& input, bool addCoreSegments)
    {
        ARMARX_TRACE;
        data::AddSegmentsResult result = iceAdapter().addSegments(input, addCoreSegments);
        return result;
    }


    data::CommitResult ReadWritePluginUser::commit(const data::Commit& commitIce, const Ice::Current&)
    {
        ARMARX_TRACE;
        return iceAdapter().commit(commitIce);
    }


    // READING
    armem::query::data::Result ReadWritePluginUser::query(const armem::query::data::Input& input, const Ice::Current&)
    {
        ARMARX_TRACE;
        return iceAdapter().query(input);
    }

    structure::data::GetServerStructureResult ReadWritePluginUser::getServerStructure(const Ice::Current&)
    {
        ARMARX_TRACE;
        return iceAdapter().getServerStructure();
    }


    // LTM STORING AND RECORDING
    dto::DirectlyStoreResult ReadWritePluginUser::directlyStore(const dto::DirectlyStoreInput& input, const Ice::Current&)
    {
        ARMARX_TRACE;
        return iceAdapter().directlyStore(input);
    }

    dto::StartRecordResult ReadWritePluginUser::startRecord(const dto::StartRecordInput& startRecordInput, const Ice::Current&)
    {
        ARMARX_TRACE;
        return iceAdapter().startRecord(startRecordInput);
    }

    dto::StopRecordResult ReadWritePluginUser::stopRecord(const Ice::Current&)
    {
        ARMARX_TRACE;
        return iceAdapter().stopRecord();
    }

    dto::RecordStatusResult ReadWritePluginUser::getRecordStatus(const Ice::Current&)
    {
        ARMARX_TRACE;
        return iceAdapter().getRecordStatus();
    }


    Plugin& ReadWritePluginUser::memoryServerPlugin()
    {
        return *plugin;
    }


    wm::Memory& ReadWritePluginUser::workingMemory()
    {
        return plugin->workingMemory;
    }


    MemoryToIceAdapter& ReadWritePluginUser::iceAdapter()
    {
        return plugin->iceAdapter;
    }


    ltm::disk::Memory& ReadWritePluginUser::longtermMemory()
    {
        return plugin->longtermMemory;
    }

    // ACTIONS
    armem::actions::GetActionsOutputSeq ReadWritePluginUser::getActions(
            const armem::actions::GetActionsInputSeq& inputs, const ::Ice::Current&  /*unused*/)
    {
        return getActions(inputs);
    }

    armem::actions::GetActionsOutputSeq ReadWritePluginUser::getActions(
            const armem::actions::GetActionsInputSeq& inputs)
    {
        (void) inputs;
        return {};
    }

    armem::actions::ExecuteActionOutputSeq ReadWritePluginUser::executeActions(
            const armem::actions::ExecuteActionInputSeq& inputs, const ::Ice::Current& /*unused*/)
    {
        return executeActions(inputs);
    }

    armem::actions::ExecuteActionOutputSeq ReadWritePluginUser::executeActions(
            const armem::actions::ExecuteActionInputSeq& inputs)
    {
        return {};
    }

    // PREDICTIONS
    armem::prediction::data::PredictionResultSeq
    ReadWritePluginUser::predict(const armem::prediction::data::PredictionRequestSeq& requests)
    {
        return iceAdapter().predict(requests);
    }

    armem::prediction::data::EngineSupportMap
    ReadWritePluginUser::getAvailableEngines()
    {
        return iceAdapter().getAvailableEngines();
    }

    armem::prediction::data::PredictionResultSeq
    ReadWritePluginUser::predict(const armem::prediction::data::PredictionRequestSeq& requests,
                                 const ::Ice::Current& /*unused*/)
    {
        return predict(requests);
    }

    armem::prediction::data::EngineSupportMap
    ReadWritePluginUser::getAvailableEngines(const ::Ice::Current& /*unused*/)
    {
        return getAvailableEngines();
    }

} // namespace armarx::armem::server::plugins
