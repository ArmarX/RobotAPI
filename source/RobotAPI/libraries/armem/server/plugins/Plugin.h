#pragma once

#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>

#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

#include <RobotAPI/interface/armem/client/MemoryListenerInterface.h>
#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>

#include <ArmarXCore/core/ComponentPlugin.h>


namespace armarx
{
    class Component;
}
namespace armarx::armem::client::plugins
{
    class Plugin;
}

namespace armarx::armem::server::plugins
{

    class Plugin :
        public armarx::ComponentPlugin
    {
    public:

        Plugin(ManagedIceObject& parent, std::string prefix);
        virtual ~Plugin() override;


        virtual void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

        virtual void preOnInitComponent() override;
        virtual void postOnInitComponent() override;
        virtual void postOnConnectComponent() override;
        virtual void preOnDisconnectComponent() override;


    public:

        /// Set the name of the wm and the ltm (if enabled).
        void setMemoryName(const std::string& memoryName);


    protected:

        /**
         * @brief Register the parent component in the MNS.
         *
         * Called before onConnect() if MNS is enabled.
         */
        mns::dto::RegisterServerResult registerServer(armarx::Component& parent);

        /**
         * @brief Remove the parent component from the MNS.
         *
         * Called before onDisconnect() if MNS is enabled.
         */
        mns::dto::RemoveServerResult removeServer();



    public:

        // Working Memory

        /// The actual memory.
        server::wm::Memory workingMemory;

        /// Helps connecting `memory` to ice. Used to handle Ice callbacks.
        MemoryToIceAdapter iceAdapter { &workingMemory, &longtermMemory};


        // Working Memory Updates (publishing)

        /// Available at onInit().
        std::string memoryTopicName;
        /// Available after onConnect().
        client::MemoryListenerInterfacePrx memoryTopic;


        // Long-Term Memory

        /// A manager class for the ltm. It internally holds a normal wm instance as a cache.
        server::ltm::disk::Memory longtermMemory;


    private:

        client::plugins::Plugin* clientPlugin = nullptr;

        std::atomic_bool initialized = false;
        std::atomic_bool connected = false;

    };
}

namespace armarx::armem::server
{
    using plugins::Plugin;
}
