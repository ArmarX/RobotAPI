#pragma once

#include <RobotAPI/interface/armem/server/MemoryInterface.h>
#include <RobotAPI/interface/armem/client/MemoryListenerInterface.h>

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/ltm/disk/Memory.h>
#include <RobotAPI/libraries/armem/client/Query.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>


namespace armarx::armem::server
{

    /**
     * @brief Helps connecting a Memory server to the Ice interface.
     *
     * This involves conversion of ice types to C++ types as well as
     * catchin exceptions and converting them to error messages
     */
    class MemoryToIceAdapter
    {
    public:

        /// Construct an MemoryToIceAdapter from an existing Memory.
        MemoryToIceAdapter(server::wm::Memory* workingMemory = nullptr,
                           server::ltm::disk::Memory* longtermMemory = nullptr);

        void setMemoryListener(client::MemoryListenerInterfacePrx memoryListenerTopic);


        // WRITING
        data::AddSegmentResult addSegment(
            const data::AddSegmentInput& input, bool addCoreSegments = false);

        data::AddSegmentsResult addSegments(
            const data::AddSegmentsInput& input, bool addCoreSegments = false);


        data::CommitResult commit(const data::Commit& commitIce, Time timeArrived);
        data::CommitResult commit(const data::Commit& commitIce);
        armem::CommitResult commit(const armem::Commit& commit);
        armem::CommitResult commitLocking(const armem::Commit& commit);


        // READING
        query::data::Result query(const armem::query::data::Input& input);
        client::QueryResult query(const client::QueryInput& input);
        armem::structure::data::GetServerStructureResult getServerStructure();

        // LTM LOADING AND REPLAYING

        // LTM STORING AND RECORDING
        dto::DirectlyStoreResult directlyStore(const dto::DirectlyStoreInput& directlStoreInput);
        dto::StartRecordResult startRecord(const dto::StartRecordInput& startRecordInput);
        dto::StopRecordResult stopRecord();
        dto::RecordStatusResult getRecordStatus();

        // PREDICTION
        prediction::data::PredictionResultSeq
        predict(prediction::data::PredictionRequestSeq requests);

        prediction::data::EngineSupportMap getAvailableEngines();

    public:

        server::wm::Memory* workingMemory;
        server::ltm::disk::Memory* longtermMemory;

        client::MemoryListenerInterfacePrx memoryListenerTopic;


    private:

        armem::CommitResult _commit(const armem::Commit& commit, bool locking);

    };


}
