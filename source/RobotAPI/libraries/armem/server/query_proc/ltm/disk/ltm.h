#pragma once

#include <RobotAPI/libraries/armem/server/ltm/disk/Memory.h>
#include <RobotAPI/libraries/armem/server/query_proc/base.h>

#include "../detail/MemoryQueryProcessorBase.h"
#include "../detail/CoreSegmentQueryProcessorBase.h"
#include "../detail/ProviderSegmentQueryProcessorBase.h"
#include "../detail/EntityQueryProcessorBase.h"

namespace armarx::armem::server::query_proc::ltm_server::disk
{
    class EntityQueryProcessor :
        public ltm::detail::EntityQueryProcessorBase<armem::server::ltm::disk::Entity, armem::wm::Entity>
    {
    protected:

        using Base = ltm::detail::EntityQueryProcessorBase<armem::server::ltm::disk::Entity, armem::wm::Entity>;


    public:

        using Base::process;

    };

    class ProviderSegmentQueryProcessor :
        public ltm::detail::ProviderSegmentQueryProcessorBase<armem::server::ltm::disk::ProviderSegment, armem::wm::ProviderSegment, EntityQueryProcessor>
    {
    protected:

        using Base = ltm::detail::ProviderSegmentQueryProcessorBase<armem::server::ltm::disk::ProviderSegment, armem::wm::ProviderSegment, EntityQueryProcessor>;


    public:
        using Base::process;
    };

    class CoreSegmentQueryProcessor :
        public ltm::detail::CoreSegmentQueryProcessorBase<armem::server::ltm::disk::CoreSegment, armem::wm::CoreSegment, ProviderSegmentQueryProcessor>
    {
    protected:

        using Base = ltm::detail::CoreSegmentQueryProcessorBase<armem::server::ltm::disk::CoreSegment, armem::wm::CoreSegment, ProviderSegmentQueryProcessor>;


    public:
        using Base::process;
    };

    class MemoryQueryProcessor :
        public ltm::detail::MemoryQueryProcessorBase<armem::server::ltm::disk::Memory, armem::wm::Memory, CoreSegmentQueryProcessor>
    {
    protected:

        using Base = ltm::detail::MemoryQueryProcessorBase<armem::server::ltm::disk::Memory, armem::wm::Memory, CoreSegmentQueryProcessor>;

    public:
        using Base::process;
    };

}
