#pragma once

#include "../../base/ProviderSegmentQueryProcessorBase.h"


namespace armarx::armem::server::query_proc::ltm::detail
{

    template <class _ProviderSegmentT, class _ResultProviderSegmentT, class _ChildProcessorT>
    class ProviderSegmentQueryProcessorBase :
        public base::ProviderSegmentQueryProcessorBase<_ProviderSegmentT, _ResultProviderSegmentT, _ChildProcessorT>
    {
    protected:

        using Base = base::ProviderSegmentQueryProcessorBase<_ProviderSegmentT, _ResultProviderSegmentT, _ChildProcessorT>;

    public:

        using ProviderSegmentT = typename Base::ProviderSegmentT;
        using EntityT = typename Base::EntityT;
        using ResultProviderSegmentT = typename Base::ResultProviderSegmentT;
        using ResultEntityT = typename Base::EntityT;
        using ChildProcessorT = typename Base::ChildProcessorT;

    public:
        using Base::ProviderSegmentQueryProcessorBase;
        virtual ~ProviderSegmentQueryProcessorBase() = default;

        using Base::process;


    };
}
