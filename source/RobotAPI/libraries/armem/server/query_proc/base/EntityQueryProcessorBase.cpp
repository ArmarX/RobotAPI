#include "EntityQueryProcessorBase.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::armem::server::query_proc::base
{
    void detail::checkReferenceTimestampNonNegative(const Time& timestamp)
    {
        ARMARX_CHECK_NONNEGATIVE(timestamp.toMicroSecondsSinceEpoch()) << "Reference timestamp must be non-negative.";
    }
}
