#pragma once

#include <RobotAPI/interface/armem/query.h>
#include <RobotAPI/libraries/armem/core/query/QueryTarget.h>

#include <Ice/Handle.h>

#include <set>
#include <vector>

namespace armarx::armem::server::query_proc::base
{

    using QueryTarget = armem::query::data::QueryTarget::QueryTargetEnum;


    /**
     * @brief Base class for memory query processors.
     */
    template <class DataT, class ResultT, class QueryT>
    class BaseQueryProcessorBase
    {
    public:

        using QueryPtrT = ::IceInternal::Handle<QueryT>;
        using QuerySeqT = std::vector<QueryPtrT>;


    public:

        virtual ~BaseQueryProcessorBase() = default;

        ResultT process(const QueryT& query, const DataT& data) const
        {
            ResultT result { data.id() };
            this->process(result, query, data);
            return result;
        }

        ResultT process(const QueryPtrT& query, const DataT& data) const
        {
            return this->process(*query, *data);
        }

        ResultT process(const QuerySeqT& queries, const DataT& data) const
        {
            ResultT result { data.id() };
            this->process(result, queries, data);
            return result;
        }

        void process(ResultT& result, const QuerySeqT& queries, const DataT& data) const
        {
            if (queries.empty())
            {
                return;
            }

            for (const auto& query : queries)
            {
                this->process(result, *query, data);
            }
        }


        /**
         * @brief Process the query and populate `result`.
         *
         * @param result The result container.
         * @param query The query.
         * @param data The source container.
         */
        virtual void process(ResultT& result, const QueryT& query, const DataT& data) const = 0;

    };


}
