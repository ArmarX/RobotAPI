#pragma once

#include "BaseQueryProcessorBase.h"

#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/interface/armem/query.h>

#include <regex>


namespace armarx::armem::server::query_proc::base
{

    template <class _MemoryT, class _ResultMemoryT, class _ChildProcessorT>
    class MemoryQueryProcessorBase :
        public BaseQueryProcessorBase<_MemoryT, _ResultMemoryT, armem::query::data::MemoryQuery>
    {
    protected:

        using Base = BaseQueryProcessorBase<_MemoryT, _ResultMemoryT, armem::query::data::MemoryQuery>;

    public:

        using MemoryT = _MemoryT;
        using CoreSegmentT = typename MemoryT::CoreSegmentT;

        using ResultMemoryT = _ResultMemoryT;
        using ResultCoreSegmentT = typename ResultMemoryT::CoreSegmentT;

        using ChildProcessorT = _ChildProcessorT;


    public:

        MemoryQueryProcessorBase()
        {
        }
        MemoryQueryProcessorBase(ChildProcessorT&& childProcessor) :
            childProcessor(childProcessor)
        {
        }


        using Base::process;
        ResultMemoryT process(const armem::query::data::Input& input, const MemoryT& memory) const
        {
            return this->process(input.memoryQueries, memory);
        }

        virtual void process(ResultMemoryT& result,
                             const armem::query::data::MemoryQuery& query,
                             const MemoryT& memory) const override
        {
            if (auto q = dynamic_cast<const armem::query::data::memory::All*>(&query))
            {
                process(result, *q, memory);
            }
            else if (auto q = dynamic_cast<const armem::query::data::memory::Single*>(&query))
            {
                process(result, *q, memory);
            }
            else if (auto q = dynamic_cast<const armem::query::data::memory::Regex*>(&query))
            {
                process(result, *q, memory);
            }
            else
            {
                throw armem::error::UnknownQueryType(MemoryT::getLevelName(), query);
            }
        }

        virtual void process(ResultMemoryT& result,
                     const armem::query::data::memory::All& query,
                     const MemoryT& memory) const
        {
            memory.forEachCoreSegment([this, &result, &query](const CoreSegmentT& coreSegment)
            {
                this->_processResult(result, coreSegment, query);
            });
        }

        virtual void process(ResultMemoryT& result,
                     const armem::query::data::memory::Single& query,
                     const MemoryT& memory) const
        {
            if (auto coreSegment = memory.findCoreSegment(query.coreSegmentName))
            {
                this->_processResult(result, *coreSegment, query);
            }
        }

        virtual void process(ResultMemoryT& result,
                     const armem::query::data::memory::Regex& query,
                     const MemoryT& memory) const
        {
            const std::regex regex(query.coreSegmentNameRegex);
            memory.forEachCoreSegment([this, &result, &query, &regex](const CoreSegmentT& coreSegment)
            {
                if (std::regex_search(coreSegment.name(), regex))
                {
                    this->_processResult(result, coreSegment, query);
                }
            });
        }


    protected:

        virtual bool _processAllowed(const armem::query::data::MemoryQuery& query) const
        {
            // always execute query. Override if you want to execute the quey only if a special condition is fulfilled (e.g. querytargets)
            return true;
        }

        void _processResult(ResultMemoryT& result,
                            const CoreSegmentT& coreSegment,
                            const armem::query::data::MemoryQuery& query) const
        {
            ResultCoreSegmentT* child = result.findCoreSegment(coreSegment.name());
            if (child == nullptr)
            {
                child = &result.addCoreSegment(coreSegment.name(), coreSegment.aronType());
            }
            childProcessor.process(*child, query.coreSegmentQueries, coreSegment);
        }


    protected:

        ChildProcessorT childProcessor;

    };
}
