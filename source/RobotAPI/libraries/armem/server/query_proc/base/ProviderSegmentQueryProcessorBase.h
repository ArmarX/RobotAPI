#pragma once

#include "BaseQueryProcessorBase.h"

#include <RobotAPI/libraries/armem/core/error.h>

#include <RobotAPI/interface/armem/query.h>

#include <regex>


namespace armarx::armem::server::query_proc::base
{

    template <class _ProviderSegmentT, class _ResultProviderSegmentT, class _ChildProcessorT>
    class ProviderSegmentQueryProcessorBase :
        public BaseQueryProcessorBase<_ProviderSegmentT, _ResultProviderSegmentT, armem::query::data::ProviderSegmentQuery>
    {
    protected:

        using Base = BaseQueryProcessorBase<_ProviderSegmentT, _ResultProviderSegmentT, armem::query::data::ProviderSegmentQuery>;


    public:

        using ProviderSegmentT = _ProviderSegmentT;
        using EntityT = typename ProviderSegmentT::EntityT;

        using ResultProviderSegmentT = _ResultProviderSegmentT;
        using ResultEntityT = typename ResultProviderSegmentT::EntityT;

        using ChildProcessorT = _ChildProcessorT;


    public:

        ProviderSegmentQueryProcessorBase()
        {
        }
        ProviderSegmentQueryProcessorBase(ChildProcessorT&& childProcessor) :
            childProcessor(childProcessor)
        {
        }

        using Base::process;
        virtual void process(ResultProviderSegmentT& result,
                             const armem::query::data::ProviderSegmentQuery& query,
                             const ProviderSegmentT& providerSegment) const override
        {
            if (auto q = dynamic_cast<const armem::query::data::provider::All*>(&query))
            {
                process(result, *q, providerSegment);
            }
            else if (auto q = dynamic_cast<const armem::query::data::provider::Single*>(&query))
            {
                process(result, *q, providerSegment);
            }
            else if (auto q = dynamic_cast<const armem::query::data::provider::Regex*>(&query))
            {
                process(result, *q, providerSegment);
            }
            else
            {
                throw armem::error::UnknownQueryType(ProviderSegmentT::getLevelName(), query);
            }
        }

        virtual void process(ResultProviderSegmentT& result,
                     const armem::query::data::provider::All& query,
                     const ProviderSegmentT& providerSegment) const
        {
            providerSegment.forEachEntity([this, &result, &query](const EntityT& entity)
            {
                this->_processResult(result, entity, query);
            });
        }

        virtual void process(ResultProviderSegmentT& result,
                     const armem::query::data::provider::Single& query,
                     const ProviderSegmentT& providerSegment) const
        {
            if (auto entity = providerSegment.findEntity(query.entityName))
            {
                this->_processResult(result, *entity, query);
            }
        }

        virtual void process(ResultProviderSegmentT& result,
                     const armem::query::data::provider::Regex& query,
                     const ProviderSegmentT& providerSegment) const
        {
            const std::regex regex(query.entityNameRegex);
            providerSegment.forEachEntity([this, &result, &query, &regex](const EntityT& entity)
            {
                if (std::regex_search(entity.name(), regex))
                {
                    this->_processResult(result, entity, query);
                }
                return true;
            });
        }


    protected:

        void _processResult(ResultProviderSegmentT& result,
                            const EntityT& entity,
                            const armem::query::data::ProviderSegmentQuery& query) const
        {
            ResultEntityT* child = result.findEntity(entity.name());
            if (child == nullptr)
            {
                child = &result.addEntity(entity.name());
            }
            childProcessor.process(*child, query.entityQueries, entity);
        }


    protected:

        ChildProcessorT childProcessor;

    };
}
