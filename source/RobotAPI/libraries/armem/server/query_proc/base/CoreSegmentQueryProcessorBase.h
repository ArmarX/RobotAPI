#pragma once

#include "BaseQueryProcessorBase.h"

#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/interface/armem/query.h>

#include <regex>


namespace armarx::armem::server::query_proc::base
{

    /**
     * @brief Handles memory queries.
     */
    template <class _CoreSegmentT, class _ResultCoreSegmentT, class _ChildProcessorT>
    class CoreSegmentQueryProcessorBase :
        public BaseQueryProcessorBase<_CoreSegmentT, _ResultCoreSegmentT, armem::query::data::CoreSegmentQuery>
    {
    protected:

        using Base = BaseQueryProcessorBase<_CoreSegmentT, _ResultCoreSegmentT, armem::query::data::CoreSegmentQuery>;

    public:

        using CoreSegmentT = _CoreSegmentT;
        using ProviderSegmentT = typename CoreSegmentT::ProviderSegmentT;

        using ResultCoreSegmentT = _ResultCoreSegmentT;
        using ResultProviderSegmentT = typename ResultCoreSegmentT::ProviderSegmentT;

        using ChildProcessorT = _ChildProcessorT;


    public:

        CoreSegmentQueryProcessorBase()
        {
        }
        CoreSegmentQueryProcessorBase(ChildProcessorT&& childProcessor) :
            childProcessor(childProcessor)
        {
        }


        using Base::process;
        virtual void process(ResultCoreSegmentT& result,
                             const armem::query::data::CoreSegmentQuery& query,
                             const CoreSegmentT& coreSegment) const override
        {
            if (auto q = dynamic_cast<const armem::query::data::core::All*>(&query))
            {
                process(result, *q, coreSegment);
            }
            else if (auto q = dynamic_cast<const armem::query::data::core::Single*>(&query))
            {
                process(result, *q, coreSegment);
            }
            else if (auto q = dynamic_cast<const armem::query::data::core::Regex*>(&query))
            {
                process(result, *q, coreSegment);
            }
            else
            {
                throw armem::error::UnknownQueryType("core segment", query);
            }
        }

        virtual void process(ResultCoreSegmentT& result,
                     const armem::query::data::core::All& query,
                     const CoreSegmentT& coreSegment) const
        {
            coreSegment.forEachProviderSegment([this, &query, &result](const ProviderSegmentT & providerSegment)
            {
                this->_processResult(result, providerSegment, query);
            });
        }

        virtual void process(ResultCoreSegmentT& result,
                     const armem::query::data::core::Single& query,
                     const CoreSegmentT& coreSegment) const
        {
            if (auto providerSegment = coreSegment.findProviderSegment(query.providerSegmentName))
            {
                this->_processResult(result, *providerSegment, query);
            }
        }

        virtual void process(ResultCoreSegmentT& result,
                     const armem::query::data::core::Regex& query,
                     const CoreSegmentT& coreSegment) const
        {
            const std::regex regex(query.providerSegmentNameRegex);
            coreSegment.forEachProviderSegment(
                [this, &result, &query, &regex](const ProviderSegmentT & providerSegment)
            {
                if (std::regex_search(providerSegment.name(), regex))
                {
                    this->_processResult(result, providerSegment, query);
                }
            });
        }


    protected:

        void _processResult(ResultCoreSegmentT& result,
                            const ProviderSegmentT& providerSegment,
                            const armem::query::data::CoreSegmentQuery& query) const
        {
            ResultProviderSegmentT* child = result.findProviderSegment(providerSegment.name());
            if (child == nullptr)
            {
                child = &result.addProviderSegment(providerSegment.name(), providerSegment.aronType());
            }
            childProcessor.process(*child, query.providerSegmentQueries, providerSegment);
        }


    protected:

        ChildProcessorT childProcessor;

    };
}
