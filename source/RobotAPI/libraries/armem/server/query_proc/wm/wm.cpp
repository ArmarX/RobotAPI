#include "wm.h"

#include <ArmarXCore/core/time/ice_conversions.h>


namespace armarx::armem::server::query_proc::wm::detail
{

    HasDataMode::HasDataMode(armem::query::DataMode dataMode) : dataMode(dataMode)
    {
    }

}


namespace armarx::armem::server::query_proc::wm
{

    ProviderSegmentQueryProcessor::ProviderSegmentQueryProcessor(armem::query::DataMode dataMode) :
        detail::ProviderSegmentQueryProcessorBase<armem::wm::ProviderSegment, armem::wm::ProviderSegment, EntityQueryProcessor>(dataMode),
        HasDataMode(dataMode)
    {
    }


    CoreSegmentQueryProcessor::CoreSegmentQueryProcessor(armem::query::DataMode dataMode) :
        CoreSegmentQueryProcessorBase(dataMode), HasDataMode(dataMode)
    {
    }


    MemoryQueryProcessor::MemoryQueryProcessor(armem::query::DataMode dataMode) :
        MemoryQueryProcessorBase(dataMode), HasDataMode(dataMode)
    {
    }

}


namespace armarx::armem::server::query_proc::wm_server
{
    ProviderSegmentQueryProcessor::ProviderSegmentQueryProcessor(armem::query::DataMode dataMode) :
        ProviderSegmentQueryProcessorBase(dataMode), HasDataMode(dataMode)
    {
    }


    CoreSegmentQueryProcessor::CoreSegmentQueryProcessor(armem::query::DataMode dataMode) :
        CoreSegmentQueryProcessorBase(dataMode),
        HasDataMode(dataMode)
    {
    }


    void CoreSegmentQueryProcessor::process(
        armem::wm::CoreSegment& result,
        const armem::query::data::CoreSegmentQuery& query,
        const CoreSegment& coreSegment) const
    {
        coreSegment.doLocked([&]()
        {
            CoreSegmentQueryProcessorBase::process(result, query, coreSegment);
        });
    }


    MemoryQueryProcessor::MemoryQueryProcessor(armem::query::DataMode dataMode) :
        MemoryQueryProcessorBase(dataMode),
        HasDataMode(dataMode)
    {
    }


}
