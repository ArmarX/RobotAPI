#pragma once

#include <RobotAPI/libraries/armem/core/query/DataMode.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>

#include "detail/MemoryQueryProcessorBase.h"
#include "detail/CoreSegmentQueryProcessorBase.h"
#include "detail/ProviderSegmentQueryProcessorBase.h"
#include "detail/EntityQueryProcessorBase.h"

namespace armarx::armem::server::query_proc::wm::detail
{

    class HasDataMode
    {
    public:

        HasDataMode(armem::query::DataMode dataMode);


    protected:

        armem::query::DataMode dataMode;

    };



    template <class SourceEntityT>
    class EntityQueryProcessor :
        public EntityQueryProcessorBase<SourceEntityT, armem::wm::Entity>,
        public HasDataMode
    {
    protected:

        using Base = EntityQueryProcessorBase<SourceEntityT, armem::wm::Entity>;
        using Entity = armem::wm::Entity;


    public:

        EntityQueryProcessor(armem::query::DataMode dataMode = armem::query::DataMode::WithData) :
            HasDataMode(dataMode)
        {}

        using Base::process;

    protected:

        void addResultSnapshot(armem::wm::Entity& result, const typename SourceEntityT::EntitySnapshotT& snapshot) const
        {
            bool withData = (dataMode == armem::query::DataMode::WithData);
            if (withData)
            {
                result.addSnapshot(server::wm::EntitySnapshot{ snapshot });
            }
            else
            {
                server::wm::EntitySnapshot copy = snapshot;
                copy.forEachInstance([](server::wm::EntityInstance & i)
                {
                    i.data() = nullptr;
                    return true;
                });
                result.addSnapshot(std::move(copy));
            }
        }

    };
}


namespace armarx::armem::server::query_proc::wm
{

    using EntityQueryProcessor = detail::EntityQueryProcessor<armem::wm::Entity>;

    class ProviderSegmentQueryProcessor :
        public detail::ProviderSegmentQueryProcessorBase<armem::wm::ProviderSegment, armem::wm::ProviderSegment, EntityQueryProcessor>,
        public detail::HasDataMode
    {
    protected:

        using Base = detail::ProviderSegmentQueryProcessorBase<armem::wm::ProviderSegment, armem::wm::ProviderSegment, EntityQueryProcessor>;
        using ProviderSegment = armem::wm::ProviderSegment;
        using Entity = armem::wm::Entity;


    public:

        ProviderSegmentQueryProcessor(armem::query::DataMode dataMode = armem::query::DataMode::WithData);

        using Base::process;
    };


    class CoreSegmentQueryProcessor :
        public detail::CoreSegmentQueryProcessorBase <armem::wm::CoreSegment, armem::wm::CoreSegment, ProviderSegmentQueryProcessor>,
        public detail::HasDataMode
    {
    protected:

        using Base = wm::detail::CoreSegmentQueryProcessorBase<armem::wm::CoreSegment, armem::wm::CoreSegment, ProviderSegmentQueryProcessor>;
        using CoreSegment = armem::wm::CoreSegment;
        using ProviderSegment = armem::wm::ProviderSegment;


    public:

        CoreSegmentQueryProcessor(armem::query::DataMode dataMode = armem::query::DataMode::WithData);

        using Base::process;

    };


    class MemoryQueryProcessor :
        public detail::MemoryQueryProcessorBase<armem::wm::Memory, armem::wm::Memory, CoreSegmentQueryProcessor>,
        public detail::HasDataMode
    {
    protected:

        using Base = detail::MemoryQueryProcessorBase<armem::wm::Memory, armem::wm::Memory, CoreSegmentQueryProcessor>;
        using Memory = armem::wm::Memory;
        using CoreSegment = armem::wm::CoreSegment;


    public:

        MemoryQueryProcessor(armem::query::DataMode dataMode = armem::query::DataMode::WithData);

        using Base::process;

    };

}


namespace armarx::armem::server::query_proc::wm_server
{

    using EntityQueryProcessor = wm::detail::EntityQueryProcessor<server::wm::Entity>;


    class ProviderSegmentQueryProcessor :
        public wm::detail::ProviderSegmentQueryProcessorBase<server::wm::ProviderSegment, armem::wm::ProviderSegment, EntityQueryProcessor>,
        public wm::detail::HasDataMode
    {
    protected:

        using Base = wm::detail::ProviderSegmentQueryProcessorBase<server::wm::ProviderSegment, armem::wm::ProviderSegment, EntityQueryProcessor>;
        using ProviderSegment = server::wm::ProviderSegment;
        using Entity = server::wm::Entity;

    public:

        ProviderSegmentQueryProcessor(armem::query::DataMode dataMode = armem::query::DataMode::WithData);

    };


    class CoreSegmentQueryProcessor :
        public wm::detail::CoreSegmentQueryProcessorBase <server::wm::CoreSegment, armem::wm::CoreSegment, ProviderSegmentQueryProcessor>,
        public wm::detail::HasDataMode
    {
    protected:

        using Base = wm::detail::CoreSegmentQueryProcessorBase <server::wm::CoreSegment, armem::wm::CoreSegment, ProviderSegmentQueryProcessor>;
        using CoreSegment = server::wm::CoreSegment;
        using ProviderSegment = server::wm::ProviderSegment;


    public:

        CoreSegmentQueryProcessor(armem::query::DataMode dataMode = armem::query::DataMode::WithData);

        using Base::process;

        /// Locks the core segment, then delegates back to `CoreSegmentQueryProcessorBase`.
        void process(
            armem::wm::CoreSegment& result,
            const armem::query::data::CoreSegmentQuery& query,
            const CoreSegment& coreSegment) const override;

    };


    class MemoryQueryProcessor :
        public wm::detail::MemoryQueryProcessorBase<server::wm::Memory, armem::wm::Memory, CoreSegmentQueryProcessor>,
        public wm::detail::HasDataMode
    {
    protected:

        using Base = wm::detail::MemoryQueryProcessorBase<server::wm::Memory, armem::wm::Memory, CoreSegmentQueryProcessor>;
        using Memory = server::wm::Memory;
        using CoreSegment = server::wm::CoreSegment;

    public:

        MemoryQueryProcessor(armem::query::DataMode dataMode = armem::query::DataMode::WithData);

        using Base::process;

    };

}
