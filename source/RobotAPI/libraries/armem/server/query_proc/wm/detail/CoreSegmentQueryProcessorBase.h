#pragma once

#include "../../base/CoreSegmentQueryProcessorBase.h"


namespace armarx::armem::server::query_proc::wm::detail
{

    /**
     * @brief Handles memory queries.
     */
    template <class _CoreSegmentT, class _ResultCoreSegmentT, class _ChildProcessorT>
    class CoreSegmentQueryProcessorBase :
        public base::CoreSegmentQueryProcessorBase<_CoreSegmentT, _ResultCoreSegmentT, _ChildProcessorT>
    {
    protected:

        using Base = base::CoreSegmentQueryProcessorBase<_CoreSegmentT, _ResultCoreSegmentT, _ChildProcessorT>;


    public:

        using CoreSegmentT = typename Base::CoreSegmentT;
        using ProviderSegmentT = typename Base::ProviderSegmentT;
        using ResultCoreSegmentT = typename Base::ResultCoreSegmentT;
        using ResultProviderSegmentT = typename Base::ProviderSegmentT;
        using ChildProcessorT = typename Base::ChildProcessorT;


    public:

        using Base::CoreSegmentQueryProcessorBase;
        virtual ~CoreSegmentQueryProcessorBase() = default;

        using Base::process;


    };
}
