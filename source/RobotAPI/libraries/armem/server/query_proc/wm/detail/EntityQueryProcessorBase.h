#pragma once

#include "../../base/EntityQueryProcessorBase.h"


namespace armarx::armem::server::query_proc::wm::detail
{

    template <class _EntityT, class _ResultEntityT>
    class EntityQueryProcessorBase :
        public base::EntityQueryProcessorBase<_EntityT, _ResultEntityT>
    {
    protected:

        using Base = base::EntityQueryProcessorBase<_EntityT, _ResultEntityT>;


    public:

        using EntityT = typename Base::EntityT;
        using EntitySnapshotT = typename Base::EntitySnapshotT;
        using ResultEntityT = typename Base::ResultEntityT;
        using ResultSnapshotT = typename Base::EntitySnapshotT;


    public:
        virtual ~EntityQueryProcessorBase() = default;

        using Base::process;


    protected:
        void addResultSnapshot(ResultEntityT& result, const EntitySnapshotT& snapshot) const override
        {
            result.addSnapshot(snapshot);
        }
    };
}
