#pragma once

#include "../../base/MemoryQueryProcessorBase.h"


namespace armarx::armem::server::query_proc::wm::detail
{

    template <class _MemoryT, class _ResultMemoryT, class _ChildProcessorT>
    class MemoryQueryProcessorBase :
        public base::MemoryQueryProcessorBase<_MemoryT, _ResultMemoryT, _ChildProcessorT>
    {
    protected:

        using Base = base::MemoryQueryProcessorBase<_MemoryT, _ResultMemoryT, _ChildProcessorT>;


    public:

        using MemoryT = typename Base::MemoryT;
        using CoreSegmentT = typename Base::CoreSegmentT;
        using ResultMemoryT = typename Base::ResultMemoryT;
        using ResultCoreSegmentT = typename Base::CoreSegmentT;
        using ChildProcessorT = typename Base::ChildProcessorT;


    public:
        using Base::MemoryQueryProcessorBase;
        virtual ~MemoryQueryProcessorBase() = default;

        using Base::process;


    };
}
