#pragma once

#include <sstream>
#include <stack>

#include <ArmarXCore/core/logging/Logging.h>

#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>


namespace armarx::armem::server
{

    struct RemoteGuiAronDataVisitor :
            public aron::data::RecursiveConstVariantVisitor
    {
        using GroupBox = armarx::RemoteGui::Client::GroupBox;
        using GridLayout = armarx::RemoteGui::Client::GridLayout;
        using Label = armarx::RemoteGui::Client::Label;


        struct Group
        {
            Group(const std::string& label = {});

            GroupBox groupBox;
            GridLayout layout;
            int nextRow = 0;
        };

        std::stack<Group> groups;
        GroupBox result;


        virtual ~RemoteGuiAronDataVisitor() = default;

        void visitDictOnEnter(const aron::data::VariantPtr& n) override
        {
            ARMARX_CHECK_NOT_NULL(n);
            const std::string key = n->getPath().size() > 0 ? n->getPath().getLastElement() : ""; // check if root of object
            visitEnter(key, "dict", n->childrenSize());
        }
        void visitDictOnExit(const aron::data::VariantPtr&) override
        {
            visitExit();
        }

        void visitListOnEnter(const aron::data::VariantPtr& n) override
        {
            ARMARX_CHECK_NOT_NULL(n);
            const std::string key = n->getPath().getLastElement();
            visitEnter(key, "list", n->childrenSize());
        }
        void visitListOnExit(const aron::data::VariantPtr&) override
        {
            visitExit();
        }

        // Same for Dict and List
        bool visitEnter(const std::string& key, const std::string& type, size_t size);
        bool visitExit();

        void visitBool(const aron::data::VariantPtr& b) override
        {
            ARMARX_CHECK_NOT_NULL(b);
            const std::string key = b->getPath().getLastElement();
            this->addValueLabel(key, *aron::data::Bool::DynamicCastAndCheck(b), "bool");
        }
        void visitDouble(const aron::data::VariantPtr& d) override
        {
            ARMARX_CHECK_NOT_NULL(d);
            const std::string key = d->getPath().getLastElement();
            this->addValueLabel(key, *aron::data::Double::DynamicCastAndCheck(d), "double");
        }
        void visitFloat(const aron::data::VariantPtr& f) override
        {
            ARMARX_CHECK_NOT_NULL(f);
            const std::string key = f->getPath().getLastElement();
            this->addValueLabel(key, *aron::data::Float::DynamicCastAndCheck(f), "float");
        }
        void visitInt(const aron::data::VariantPtr& i) override
        {
            ARMARX_CHECK_NOT_NULL(i);
            const std::string key = i->getPath().getLastElement();
            this->addValueLabel(key, *aron::data::Int::DynamicCastAndCheck(i), "int");
        }
        void visitLong(const aron::data::VariantPtr& l) override
        {
            ARMARX_CHECK_NOT_NULL(l);
            const std::string key = l->getPath().getLastElement();
            this->addValueLabel(key, *aron::data::Long::DynamicCastAndCheck(l), "long");
        }
        void visitString(const aron::data::VariantPtr& string) override
        {
            ARMARX_CHECK_NOT_NULL(string);
            auto s = aron::data::String::DynamicCastAndCheck(string);
            const std::string key = string->getPath().getLastElement();
            this->addValueLabel(key, *s, "string");
        }

        void visitNDArray(const aron::data::VariantPtr& array) override
        {
            ARMARX_CHECK_NOT_NULL(array);
            const std::string key = array->getPath().getLastElement();
            this->addValueLabel(key, *aron::data::NDArray::DynamicCastAndCheck(array), "ND Array");
        }

        void visitUnknown(const aron::data::VariantPtr& unknown) override
        {
            ARMARX_IMPORTANT << "Unknown data encountered: "
                             << (unknown ? unknown->getFullName() : "nullptr");
        }


    private:
        template <class DataT>
        void addValueLabel(const std::string& key, DataT& n, const std::string& typeName)
        {
            checkGroupsNotEmpty();
            Group& g = groups.top();
            g.layout
            .add(Label(key), {g.nextRow, 0})
            .add(Label("(" + typeName + ")"), {g.nextRow, 1})
            .add(Label("= " + getValueText(n)), {g.nextRow, 2})
            ;
            ++g.nextRow;
        }

        template <class DataT>
        std::string getValueText(DataT& n)
        {
            std::stringstream ss;
            streamValueText(n, ss);
            return ss.str();
        }

        template <class DataT>
        void streamValueText(DataT& n, std::stringstream& ss)
        {
            ss << n.getValue();
        }
        void streamValueText(aron::data::String& n, std::stringstream& ss);
        void streamValueText(aron::data::NDArray& n, std::stringstream& ss);

        void checkGroupsNotEmpty() const;

    };


}
