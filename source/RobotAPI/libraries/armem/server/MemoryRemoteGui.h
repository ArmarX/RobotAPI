#pragma once

#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>


namespace armarx::armem::server
{

    /**
     * @brief Utility for memory Remote Guis.
     */
    class MemoryRemoteGui
    {
    public:
        using GroupBox = armarx::RemoteGui::Client::GroupBox;
        using Label = armarx::RemoteGui::Client::Label;


        GroupBox makeGroupBox(const armem::wm::Memory& memory) const;
        GroupBox makeGroupBox(const armem::wm::CoreSegment& coreSegment) const;
        GroupBox makeGroupBox(const armem::wm::ProviderSegment& providerSegment) const;
        GroupBox makeGroupBox(const armem::wm::Entity& entity) const;
        GroupBox makeGroupBox(const armem::wm::EntitySnapshot& entitySnapshot) const;
        GroupBox makeGroupBox(const armem::wm::EntityInstance& instance) const;

        GroupBox makeGroupBox(const armem::server::wm::Memory& memory) const;
        GroupBox makeGroupBox(const armem::server::wm::CoreSegment& coreSegment) const;
        GroupBox makeGroupBox(const armem::server::wm::ProviderSegment& providerSegment) const;
        GroupBox makeGroupBox(const armem::server::wm::Entity& entity) const;


        std::string makeGroupLabel(const std::string& term, const std::string& name, size_t size,
                                   const std::string& namePrefix = ": '", const std::string& nameSuffix = "'") const;
        std::string makeNoItemsMessage(const std::string& term) const;



        int maxHistorySize = 10;

    private:

        template <class ...Args>
        MemoryRemoteGui::GroupBox _makeGroupBox(const armem::base::MemoryBase<Args...>& memory) const;
        template <class ...Args>
        MemoryRemoteGui::GroupBox _makeGroupBox(const armem::base::CoreSegmentBase<Args...>& coreSegment) const;
        template <class ...Args>
        MemoryRemoteGui::GroupBox _makeGroupBox(const armem::base::ProviderSegmentBase<Args...>& providerSegment) const;
        template <class ...Args>
        MemoryRemoteGui::GroupBox _makeGroupBox(const armem::base::EntityBase<Args...>& entity) const;

    };


}
