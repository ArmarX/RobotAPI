#pragma once

#include "plugins/Plugin.h"
#include "plugins/ReadOnlyPluginUser.h"
#include "plugins/ReadWritePluginUser.h"


namespace armarx::armem::server
{
    using plugins::Plugin;
    using plugins::ReadOnlyPluginUser;
    using plugins::ReadWritePluginUser;
}

namespace armarx::armem
{
    using ServerPlugin = server::Plugin;
    using ReadOnlyServerPluginUser = server::ReadOnlyPluginUser;
    using ReadWriteServerPluginUser = server::ReadWritePluginUser;
}
