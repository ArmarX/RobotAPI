#pragma once

#include "server/ComponentPlugin.h"
#include "server/MemoryRemoteGui.h"


namespace armarx::armem
{
    using server::MemoryRemoteGui;
    using ServerComponentPluginUser = server::ComponentPluginUser;
}
