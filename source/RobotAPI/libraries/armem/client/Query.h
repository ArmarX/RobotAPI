#pragma once

// RobotAPI
#include <RobotAPI/interface/armem/query.h>

#include <RobotAPI/libraries/armem/core/wm/ice_conversions.h>
#include <RobotAPI/libraries/armem/core/SuccessHeader.h>
#include <RobotAPI/libraries/armem/core/query/DataMode.h>
#include <RobotAPI/libraries/armem/core/query/QueryTarget.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>


namespace armarx::armem::client::query
{
    // #include <RobotAPI/libraries/armem/client/query/Builder.h>
    class Builder;
}

namespace armarx::armem::client
{
    using QueryBuilder = query::Builder;


    /**
     * @brief An update of an entity for a specific point in time.
     */
    struct QueryInput
    {
        armem::query::data::MemoryQuerySeq memoryQueries;
        armem::query::DataMode dataMode;

        static QueryInput fromIce(const armem::query::data::Input& ice);
        armem::query::data::Input toIce() const;
    };


    /**
     * @brief Result of an `EntityUpdate`.
     */
    struct QueryResult : public detail::SuccessHeader
    {
        wm::Memory memory;


        static QueryResult fromIce(const armem::query::data::Result& ice);
        armem::query::data::Result toIce() const;

        friend std::ostream& operator<<(std::ostream& os, const QueryResult& rhs);
    };


    void toIce(armem::query::data::Input& ice, const QueryInput& input);
    void fromIce(const armem::query::data::Input& ice, QueryInput& input);

    void toIce(armem::query::data::Result& ice, const QueryResult& result);
    void fromIce(const armem::query::data::Result& ice, QueryResult& result);

}
