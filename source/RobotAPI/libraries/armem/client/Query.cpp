#include "Query.h"

#include <ArmarXCore/core/ice_conversions.h>
#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>

#include <RobotAPI/libraries/armem/core/wm/ice_conversions.h>


namespace armarx::armem::client
{
    using armarx::fromIce;
    using armarx::toIce;


    QueryInput QueryInput::fromIce(const armem::query::data::Input& ice)
    {
        return armarx::fromIce<QueryInput>(ice);
    }

    armem::query::data::Input QueryInput::toIce() const
    {
        return armarx::toIce<armem::query::data::Input>(*this);
    }

    QueryResult QueryResult::fromIce(const armem::query::data::Result& ice)
    {
        return armarx::fromIce<QueryResult>(ice);
    }

    armem::query::data::Result QueryResult::toIce() const
    {
        return armarx::toIce<armem::query::data::Result>(*this);
    }

    std::ostream& operator<<(std::ostream& os, const QueryResult& rhs)
    {
        return os << "Query result: "
               << "\n- success:       \t" << rhs.success
               << "\n- error message: \t" << rhs.errorMessage
               ;
    }
}


namespace armarx::armem
{
    void client::toIce(armem::query::data::Input& ice, const QueryInput& input)
    {
        ice.memoryQueries = input.memoryQueries;
        toIce(ice.withData, (input.dataMode == armem::query::DataMode::WithData));
    }

    void client::fromIce(const armem::query::data::Input& ice, QueryInput& input)
    {
        input.memoryQueries = ice.memoryQueries;
        input.dataMode = (ice.withData ? armem::query::DataMode::WithData : armem::query::DataMode::NoData);
    }

    void client::toIce(armem::query::data::Result& ice, const QueryResult& result)
    {
        toIce(ice, dynamic_cast<const detail::SuccessHeader&>(result));
        toIce(ice.memory, result.memory);
    }

    void client::fromIce(const armem::query::data::Result& ice, QueryResult& result)
    {
        fromIce(ice, dynamic_cast<detail::SuccessHeader&>(result));
        fromIce(ice.memory, result.memory);
    }

}
