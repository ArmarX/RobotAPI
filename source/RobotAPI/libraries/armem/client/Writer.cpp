#include "Writer.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/ice_conversions.h>

#include "../error.h"


namespace armarx::armem::client
{

    Writer::Writer(server::WritingMemoryInterfacePrx memory) : memory(memory)
    {
    }

    data::AddSegmentResult Writer::addSegment(const std::string& coreSegmentName, const std::string& providerSegmentName, bool clearWhenExists)
    {
        data::AddSegmentInput input;
        input.coreSegmentName = coreSegmentName;
        input.providerSegmentName = providerSegmentName;
        input.clearWhenExists = clearWhenExists;
        return addSegment(input);
    }

    data::AddSegmentResult Writer::addSegment(const MemoryID& providerSegmentID, bool clearWhenExists)
    {
        return addSegment(providerSegmentID.coreSegmentName, providerSegmentID.providerSegmentName, clearWhenExists);
    }

    data::AddSegmentResult Writer::addSegment(const std::pair<std::string, std::string>& names, bool clearWhenExists)
    {
        return addSegment(names.first, names.second, clearWhenExists);
    }

    data::AddSegmentResult Writer::addSegment(const data::AddSegmentInput& input)
    {
        data::AddSegmentsResult results = addSegments({input});
        ARMARX_CHECK_EQUAL(results.size(), 1);
        return results.at(0);
    }

    data::AddSegmentsResult Writer::addSegments(const data::AddSegmentsInput& inputs)
    {
        ARMARX_CHECK_NOT_NULL(memory);
        data::AddSegmentsResult results = memory->addSegments(inputs);
        ARMARX_CHECK_EQUAL(results.size(), inputs.size());
        return results;
    }


    CommitResult Writer::commit(const Commit& commit)
    {
        ARMARX_CHECK_NOT_NULL(memory);

        data::Commit commitIce;
        toIce(commitIce, commit);

        data::CommitResult resultIce = this->_commit(commitIce);

        armem::CommitResult result;
        fromIce(resultIce, result);

        return result;
    }


    data::CommitResult Writer::commit(const data::Commit& _commit)
    {
        data::Commit commit = _commit;
        return this->_commit(commit);
    }


    EntityUpdateResult Writer::commit(const EntityUpdate& update)
    {
        armem::Commit commit;
        commit.updates.push_back(update);

        armem::CommitResult result = this->commit(commit);
        ARMARX_CHECK_EQUAL(result.results.size(), 1);
        return result.results.at(0);
    }

    EntityUpdateResult Writer::commit(
        const MemoryID& entityID,
        const std::vector<aron::data::DictPtr>& instancesData,
        Time timeCreated)
    {
        EntityUpdate update;
        update.entityID = entityID;
        update.instancesData = instancesData;
        update.timeCreated = timeCreated;
        return commit(update);
    }

    void
    Writer::setWritingMemory(server::WritingMemoryInterfacePrx memory)
    {
        this->memory = memory;
    }

    data::CommitResult Writer::_commit(data::Commit& commit)
    {
        ARMARX_CHECK_NOT_NULL(memory);

        /*
         * This function sets the `timeSent` of each `EntityUpdate` before
         * sending the data. To allow that, `commit` needs to bo non-const.
         * Otherwise, the function would need to make a copy of `commit`,
         * which is not necessary in all cases; e.g. when called by
         * `commit(Const Commit&)`, which converts the commit to ice types
         * anyway.
         */

        const Time timeSent = armem::Time::Now();
        for (data::EntityUpdate& update : commit.updates)
        {
            toIce(update.timeSent, timeSent);
        }

        data::CommitResult result;
        auto handleError = [&commit, &result](const std::string & what)
        {
            for (const auto& _ : commit.updates)
            {
                (void) _;
                data::EntityUpdateResult& r = result.results.emplace_back();
                r.success = false;
                r.errorMessage = "Memory component not registered.\n" + std::string(what);
            }
        };

        try
        {
            result = memory->commit(commit);
        }
        catch (const Ice::ConnectionRefusedException& e)
        {
            handleError(e.what());
        }
        catch (const Ice::ConnectionLostException& e)
        {
            handleError(e.what());
        }
        catch (const Ice::NotRegisteredException& e)
        {
            handleError(e.what());
        }

        return result;
    }
}


std::ostream& armarx::armem::data::operator<<(std::ostream& os, const AddSegmentInput& rhs)
{
    return os << "AddSegmentInput: "
           << "\n- core segment:     \t'" << rhs.coreSegmentName << "'"
           << "\n- provider segment: \t'" << rhs.providerSegmentName << "'"
           << "\n- clear when exists:\t"  << rhs.clearWhenExists << ""
           << "\n";
}
std::ostream& armarx::armem::data::operator<<(std::ostream& os, const AddSegmentsInput& rhs)
{
    for (size_t i = 0; i < rhs.size(); ++i)
    {
        os << "[" << i << "] " << rhs[i];
    }
    return os;
}
std::ostream& armarx::armem::data::operator<<(std::ostream& os, const AddSegmentResult& rhs)
{
    return os << "AddSegmentResult:"
           << "\n- success:       \t" << rhs.success
           << "\n- segment ID:    \t'" << rhs.segmentID << "'"
           << "\n- error message: \t"  << rhs.errorMessage
           << "\n";
}
std::ostream& armarx::armem::data::operator<<(std::ostream& os, const AddSegmentsResult& rhs)
{
    for (size_t i = 0; i < rhs.size(); ++i)
    {
        os << "[" << i << "] " << rhs[i];
    }
    return os;
}
