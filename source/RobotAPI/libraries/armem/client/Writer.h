#pragma once

#include <RobotAPI/interface/armem/server/WritingMemoryInterface.h>

#include <RobotAPI/libraries/armem/core/wm/ice_conversions.h>


namespace armarx::armem::client
{

    /**
     * @brief Helps a memory client sending data to a memory.
     *
     * You can check whether the writer is ready using the bool operator:
     * @code
     * client::Writer writer(writingMemoryProxy);
     * ...
     * if (writer)
     * {
     *     writer.commit(...);
     * }
     * @endcode
     */
    class Writer
    {
    public:

        /**
         * @brief Construct a memory writer.
         * @param memory The memory proxy.
         */
        Writer(server::WritingMemoryInterfacePrx memory = nullptr);


        data::AddSegmentResult addSegment(const std::string& coreSegmentName, const std::string& providerSegmentName, bool clearWhenExists = false);
        data::AddSegmentResult addSegment(const MemoryID& providerSegmentID, bool clearWhenExists = false);
        data::AddSegmentResult addSegment(const std::pair<std::string, std::string>& names, bool clearWhenExists = false);
        data::AddSegmentResult addSegment(const data::AddSegmentInput& input);
        data::AddSegmentsResult addSegments(const data::AddSegmentsInput& input);


        /**
         * @brief Writes a `Commit` to the memory.
         */
        CommitResult commit(const Commit& commit);
        /// Commit a single entity update.
        EntityUpdateResult commit(const EntityUpdate& update);
        /// Commit a single entity update.
        EntityUpdateResult commit(
            const MemoryID& entityID,
            const std::vector<aron::data::DictPtr>& instancesData,
            Time timeCreated);

        // with bare-ice types
        data::CommitResult commit(const data::Commit& commit);


        void setWritingMemory(server::WritingMemoryInterfacePrx memory);

        operator bool() const
        {
            return bool(memory);
        }

    private:

        /// Sets `timeSent` on all entity updates and performs the commit,
        data::CommitResult _commit(data::Commit& commit);


    public:

        server::WritingMemoryInterfacePrx memory;

    };

}

namespace armarx::armem::data
{
    std::ostream& operator<<(std::ostream& os, const AddSegmentInput& rhs);
    std::ostream& operator<<(std::ostream& os, const AddSegmentsInput& rhs);
    std::ostream& operator<<(std::ostream& os, const AddSegmentResult& rhs);
    std::ostream& operator<<(std::ostream& os, const AddSegmentsResult& rhs);
}
