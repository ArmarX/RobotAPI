#pragma once

// #include <RobotAPI/libraries/armem/core/forward_declarations.h>



namespace armarx::armem::client::query
{
    class Builder;
}

namespace armarx::armem::client
{
    class MemoryNameSystem;

    class Reader;
    class Writer;

    using QueryBuilder = query::Builder;
    struct QueryInput;
    struct QueryResult;
}

namespace armarx::armem::client::plugins
{
    class Plugin;
    class PluginUser;
    class ListeningPluginUser;
}

namespace armarx::armem
{
    using ClientPlugin = client::plugins::Plugin;
    using ClientPluginUser = client::plugins::PluginUser;
    using ListeningClientPluginUser = client::plugins::ListeningPluginUser;
}


