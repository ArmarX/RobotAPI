/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <string>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/ComponentPlugin.h>

#include <RobotAPI/libraries/armem/client/plugins/Plugin.h>
#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>


namespace armarx::armem::client::plugins
{

    /**
     * @brief A component plugin offering client-side access to a reader or writer
     * and manages the lifecycle, e.g. connecting to the memory.
     * Access to the working memory system is provided by a Memory Name System (MNS) client.
     *
     * @see MemoryNameSystem
     */
    template <typename T>
    class ReaderWriterPlugin : public armarx::ComponentPlugin
    {
    public:
        ReaderWriterPlugin(ManagedIceObject& parent, const std::string pre) :
            ComponentPlugin(parent, pre), readerWriter(memoryNameSystem())
        {
            if (not armemPlugin)
            {
                addPlugin(armemPlugin);
            }
            addPluginDependency(armemPlugin);
        }

        ~ReaderWriterPlugin() override = default;

        void
        postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override
        {
            readerWriter.registerPropertyDefinitions(properties);
        }

        void
        preOnConnectComponent() override
        {
            ARMARX_CHECK_NOT_NULL(armemPlugin);
            if (not armemPlugin->isMemoryNameSystemEnabled())
            {
                ARMARX_INFO << "The memory name system is disabled by user choice via the property."
                            << "Reader and writer will not be able to connect to the memory.";
                return;
            }

            readerWriter.connect();
        }

        bool
        isAvailable() const
        {
            ARMARX_CHECK_NOT_NULL(armemPlugin);
            return armemPlugin->isMemoryNameSystemEnabled();
        }

        T&
        get()
        {
            ARMARX_CHECK_NOT_NULL(armemPlugin);
            ARMARX_CHECK(armemPlugin->isMemoryNameSystemEnabled());
            return readerWriter;
        }

        const T&
        get() const
        {
            ARMARX_CHECK_NOT_NULL(armemPlugin);
            ARMARX_CHECK(armemPlugin->isMemoryNameSystemEnabled());
            return readerWriter;
        }

    private:
        MemoryNameSystem&
        memoryNameSystem()
        {
            if (armemPlugin == nullptr)
            {
                addPlugin(armemPlugin);
            }

            ARMARX_CHECK_NOT_NULL(armemPlugin);
            return armemPlugin->getMemoryNameSystemClient();
        }

        armarx::armem::client::plugins::Plugin* armemPlugin = nullptr;
        
        T readerWriter;

    };

} // namespace armarx::armem::client::plugins
