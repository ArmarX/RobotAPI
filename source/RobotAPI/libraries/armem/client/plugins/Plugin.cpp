#include "Plugin.h"

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/ice_conversions.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>


namespace armarx::armem::client::plugins
{

    Plugin::Plugin(
        ManagedIceObject& parent, std::string pre) :
        ComponentPlugin(parent, pre)
    {
    }


    Plugin::~Plugin()
    {}


    void Plugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
    {
        if (not properties->hasDefinition(makePropertyName(PROPERTY_MNS_NAME_NAME)))
        {
            properties->defineOptionalProperty<std::string>(
                makePropertyName(PROPERTY_MNS_NAME_NAME),
                memoryNameSystemName,
                "Name of the Memory Name System (MNS) component.");
        }
        if (not properties->hasDefinition(makePropertyName(PROPERTY_MNS_ENABLED_NAME)))
        {
            properties->defineOptionalProperty<bool>(
                makePropertyName(PROPERTY_MNS_ENABLED_NAME),
                memoryNameSystemEnabled,
                "Whether to use (and depend on) the Memory Name System (MNS)."
                "\nSet to false to use this memory as a stand-alone.");
        }
    }


    void Plugin::preOnInitComponent()
    {
        parent<Component>().getProperty(memoryNameSystemName, makePropertyName(PROPERTY_MNS_NAME_NAME));
        parent<Component>().getProperty(memoryNameSystemEnabled, makePropertyName(PROPERTY_MNS_ENABLED_NAME));

        if (isMemoryNameSystemEnabled())
        {
            parent().usingProxy(getMemoryNameSystemName());
        }
    }


    void Plugin::preOnConnectComponent()
    {
        if (isMemoryNameSystemEnabled())
        {
            ARMARX_DEBUG << "Creating MemoryNameSystem client with owning component '" << parent().getName() << "'.";
            memoryNameSystem.initialize(getMemoryNameSystemProxy(), &parent());
        }
    }


    bool
    Plugin::isMemoryNameSystemEnabled()
    {
        return memoryNameSystemEnabled;
    }


    std::string
    Plugin::getMemoryNameSystemName()
    {
        return memoryNameSystemName;
    }


    mns::MemoryNameSystemInterfacePrx
    Plugin::getMemoryNameSystemProxy()
    {
        return isMemoryNameSystemEnabled() and parentDerives<ManagedIceObject>()
               ? parent<ManagedIceObject>().getProxy<mns::MemoryNameSystemInterfacePrx>(getMemoryNameSystemName())
               : nullptr;
    }


    MemoryNameSystem&
    Plugin::getMemoryNameSystemClient()
    {
        return memoryNameSystem;
    }


    const MemoryNameSystem&
    Plugin::getMemoryNameSystemClient() const
    {
        return memoryNameSystem;
    }

}
