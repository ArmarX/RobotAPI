#include "ListeningPlugin.h"

#include <RobotAPI/interface/armem/client/MemoryListenerInterface.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>


namespace armarx::armem::client::plugins
{

    ListeningPlugin::~ListeningPlugin()
    {}


    void ListeningPlugin::postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties)
    {
        // This is now done by the client::util::MemoryListener class on subscription of a memory ID.
        // Subscribe topics by single servers, use this as a prefix.
        // properties->topic<MemoryListenerInterface>("MemoryUpdates");
    }

}
