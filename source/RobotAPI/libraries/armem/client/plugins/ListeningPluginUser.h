#pragma once

#include "PluginUser.h"

#include <RobotAPI/interface/armem/client/MemoryListenerInterface.h>

#include <vector>


namespace armarx::armem::client::plugins
{
    class ListeningPlugin;


    /**
     * @brief A memory name system client which listens to the memory updates
     * topic (`MemoryListenerInterface`).
     *
     * Derive from this plugin user class to receive memory update events.
     * If your class already inherits from an ice interface, your ice interface might need to inherit from the
     * MemoryListenerInterface to avoid errors.
     */
    class ListeningPluginUser :
        virtual public PluginUser,
        virtual public MemoryListenerInterface
    {
    protected:

        ListeningPluginUser();
        virtual ~ListeningPluginUser() override;


        // MemoryListenerInterface
        virtual void
        memoryUpdated(const std::vector<data::MemoryID>& updatedSnapshotIDs,
                      const Ice::Current&) override;


    private:

        ListeningPlugin* listeningPlugin = nullptr;

    };

}
namespace armarx::armem::client
{
    using ListeningPluginUser = plugins::ListeningPluginUser;
}
namespace armarx::armem
{
    using ListeningClientPluginUser = client::ListeningPluginUser;
}
