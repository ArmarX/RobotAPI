#include "ListeningPluginUser.h"

#include "ListeningPlugin.h"

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>


namespace armarx::armem::client::plugins
{

    ListeningPluginUser::ListeningPluginUser()
    {
        addPlugin(listeningPlugin);
    }


    ListeningPluginUser::~ListeningPluginUser()
    {
    }


    void ListeningPluginUser::memoryUpdated(
        const std::vector<data::MemoryID>& updatedSnapshotIDs, const Ice::Current&)
    {
        memoryNameSystem().updated(updatedSnapshotIDs);
    }

}
