#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>


namespace armarx::armem::client::plugins
{

    /**
     * @brief Subscribes the memory updates topic.
     *
     * When using this plugin, the component needs to implement the
     * `MemoryListenerInterface`.
     *
     * @see MemoryListenerInterface
     */
    class ListeningPlugin :
        public armarx::ComponentPlugin
    {
    public:

        using ComponentPlugin::ComponentPlugin;
        virtual ~ListeningPlugin() override;

        virtual void postCreatePropertyDefinitions(armarx::PropertyDefinitionsPtr& properties) override;

    };

}
