#pragma once

#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>

#include <ArmarXCore/core/ComponentPlugin.h>

#include <string>


namespace armarx::armem::client::plugins
{

    /**
     * @brief A component plugin offering client-side access to to the
     * working memory system by providing a Memory Name System (MNS) client.
     *
     * @see MemoryNameSystem
     */
    class Plugin :
        public armarx::ComponentPlugin
    {
    public:

        Plugin(ManagedIceObject& parent, std::string pre);
        virtual ~Plugin() override;


        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

        void preOnInitComponent() override;
        void preOnConnectComponent() override;


    public:

        /**
         * @brief Get the MNS client.
         *
         * Only valid when enabled.
         */
        MemoryNameSystem& getMemoryNameSystemClient();
        const MemoryNameSystem& getMemoryNameSystemClient() const;


        /**
         * @brief Indicate whether the Memory Name System (MNS) is enabled.
         */
        bool isMemoryNameSystemEnabled();

        /**
         * @brief Get the name of the MNS component.
         */
        std::string getMemoryNameSystemName();

        /**
         * @brief Get the MNS proxy.
         * @return The MNS proxy when MNS is enabled, nullptr when MNS is disabled.
         */
        mns::MemoryNameSystemInterfacePrx getMemoryNameSystemProxy();


    private:

        /// The MNS client.
        MemoryNameSystem memoryNameSystem;

        bool memoryNameSystemEnabled = true;
        std::string memoryNameSystemName = "MemoryNameSystem";


        static constexpr const char* PROPERTY_MNS_ENABLED_NAME = "mns.MemoryNameSystemEnabled";
        static constexpr const char* PROPERTY_MNS_NAME_NAME = "mns.MemoryNameSystemName";

    };

}
