#include "PluginUser.h"
#include "Plugin.h"

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>



namespace armarx::armem::client::plugins
{

    PluginUser::PluginUser()
    {
        addPlugin(plugin);
    }


    PluginUser::~PluginUser()
    {
    }


    MemoryNameSystem& PluginUser::memoryNameSystem()
    {
        return plugin->getMemoryNameSystemClient();
    }


    const MemoryNameSystem& PluginUser::memoryNameSystem() const
    {
        return plugin->getMemoryNameSystemClient();
    }

}
