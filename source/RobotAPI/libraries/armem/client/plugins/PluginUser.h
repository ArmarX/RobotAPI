#pragma once

#include <RobotAPI/libraries/armem/client/forward_declarations.h>

#include <ArmarXCore/core/ManagedIceObject.h>


// Use this one include in your .cpp:
// #include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>


namespace armarx::armem::client::plugins
{
    class Plugin;


    /**
     * @brief Adds the Memory Name System client component plugin.
     *
     * This plugin class does not implement the `MemoryListenerInterface`.
     * Therefore, it will not receive and process memory udpate events via the
     * memory updates topic. If you want to receive these updates, use the
     * `ListeningMemoryNameSystemPluginUser`.
     *
     *
     * @see MemoryNameSystemPlugin
     * @see ListeningMemoryNameSystemPluginUser
     */
    class PluginUser :
        virtual public armarx::ManagedIceObject
    {
    protected:

        PluginUser();
        virtual ~PluginUser() override;


    public:

        MemoryNameSystem& memoryNameSystem();
        const MemoryNameSystem& memoryNameSystem() const;


    private:

        plugins::Plugin* plugin = nullptr;

    };

}
namespace armarx::armem::client
{
    using PluginUser = plugins::PluginUser;
}
namespace armarx::armem
{
    using ClientPluginUser = client::PluginUser;
}
