#pragma once

#include "selectors.h"


namespace armarx::armem::client::query_fns
{

    inline auto
    all()
    {
        return [ ](auto & selector)
        {
            selector.all();
        };
    }


    inline auto
    withID(const MemoryID& id)
    {
        return [ & ](auto & selector)
        {
            selector.withID(id);
        };
    }


    // NAME-BASED QUERIES

    inline
    auto withName(const std::string& name)
    {
        return [ &name ](auto & selector)
        {
            selector.withName(name);
        };
    }

    inline auto withNamesMatching(const std::string& regex)
    {
        return [ &regex ](auto & selector)
        {
            selector.withNamesMatching(regex);
        };
    }

    inline auto withNames(const std::vector<std::string>& names)
    {
        return [ &names ](auto & selector)
        {
            selector.withNames(names);
        };
    }
    template <class StringContainerT>
    auto withNames(const StringContainerT& names)
    {
        return [ &names ](auto & selector)
        {
            selector.withNames(names);
        };
    }
    template <class IteratorT>
    auto withNames(IteratorT begin, IteratorT end)
    {
        return [ begin, end ](auto & selector)
        {
            selector.withNames(begin, end);
        };
    }

    inline auto withNamesStartingWith(const std::string& prefix)
    {
        return [ &prefix ](auto & selector)
        {
            selector.withNamesStartingWith(prefix);
        };
    }
    inline auto withNamesEndingWith(const std::string& suffix)
    {
        return [ &suffix ](auto & selector)
        {
            selector.withNamesEndingWith(suffix);
        };
    }
    inline auto withNamesContaining(const std::string& substring)
    {
        return [ &substring ](auto & selector)
        {
            selector.withNamesContaining(substring);
        };
    }


    // SNAPSHOT QUERIES

    inline
    std::function<void(query::SnapshotSelector&)>
    atTime(Time time)
    {
        return [ = ](query::SnapshotSelector & selector)
        {
            selector.atTime(time);
        };
    }

    inline
    std::function<void(query::SnapshotSelector&)>
    latest()
    {
        return [ = ](query::SnapshotSelector & selector)
        {
            selector.latest();
        };
    }

    inline
    std::function<void(query::SnapshotSelector&)>
    indexRange(long first, long last)
    {
        return [ = ](query::SnapshotSelector & selector)
        {
            selector.indexRange(first, last);
        };
    }

    inline
    std::function<void(query::SnapshotSelector&)>
    timeRange(Time min, Time max)
    {
        return [ = ](query::SnapshotSelector & selector)
        {
            selector.timeRange(min, max);
        };
    }

    inline
    std::function<void(query::SnapshotSelector&)>
    atTimeApprox(Time time, Duration eps)
    {
        return [ = ](query::SnapshotSelector & selector)
        {
            selector.atTimeApprox(time, eps);
        };
    }

    inline
    std::function<void(query::SnapshotSelector&)>
    beforeOrAtTime(Time time)
    {
        return [ = ](query::SnapshotSelector & selector)
        {
            selector.beforeOrAtTime(time);
        };
    }

    inline
    std::function<void(query::SnapshotSelector&)>
    beforeTime(Time time, long nElements = 1)
    {
        return [ = ](query::SnapshotSelector & selector)
        {
            selector.beforeTime(time, nElements);
        };
    }

}  // namespace armarx::armem::client::query_fns
