#pragma once

#include <string>
#include <vector>


namespace armarx::armem::client::query::detail
{

    template <class DerivedT>
    class NameSelectorOps
    {
    public:

        NameSelectorOps() = default;
        virtual ~NameSelectorOps() = default;


        virtual DerivedT& withName(const std::string& name) = 0;
        virtual DerivedT& withNamesMatching(const std::string& regex) = 0;


        virtual DerivedT& withNames(const std::vector<std::string>& names)
        {
            return withNames<std::vector<std::string>>(names);
        }

        template <class StringContainerT>
        DerivedT& withNames(const StringContainerT& names)
        {
            return withNames(names.begin(), names.end());
        }
        template <class IteratorT>
        DerivedT& withNames(IteratorT begin, IteratorT end)
        {
            for (auto it = begin; it != end; ++it)
            {
                this->withName(*it);
            }
            return dynamic_cast<DerivedT&>(*this);
        }


        virtual DerivedT& withNamesStartingWith(const std::string& prefix)
        {
            return withNamesMatching(prefix + ".*");
        }
        virtual DerivedT& withNamesEndingWith(const std::string& suffix)
        {
            return withNamesMatching(".*" + suffix);
        }
        virtual DerivedT& withNamesContaining(const std::string& substring)
        {
            return withNamesMatching(".*" + substring + ".*");
        }


    };

}
