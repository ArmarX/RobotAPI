#pragma once

#include <vector>

#include <Ice/Handle.h>

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/interface/armem/query.h>


namespace armarx::armem::client::query::detail
{

    template <class _DerivedT, class _QueryT>
    class ChildSelectorOps
    {
    public:
        using DerivedT = _DerivedT;
        using QueryT = _QueryT;

        ChildSelectorOps() = default;
        virtual ~ChildSelectorOps() = default;


        virtual DerivedT& all() = 0;
        virtual DerivedT& withID(const MemoryID& id) = 0;


        void addQuery(const IceInternal::Handle<QueryT>& query)
        {
            _queries.push_back(query);
        }
        void addQueries(const std::vector<IceInternal::Handle<QueryT>>& queries)
        {
            for (auto q : queries)
            {
                addQuery(q);
            }
        }


    protected:

        template <class DerivedQueryT>
        DerivedQueryT& _addQuery()
        {
            IceInternal::Handle<DerivedQueryT> query(new DerivedQueryT);
            _queries.push_back(query);
            return *query;
        }

        std::vector<QueryT> _queries;


        // Only friends have access to these.

        template <class ParentDerivedT, class ParentChildT>
        friend class ParentSelectorOps;

        template <class T>
        inline void _apply(T arg)
        {
            arg(dynamic_cast<DerivedT&>(*this));
        }
        template <class T, class ...Ts>
        inline void _apply(T arg, Ts... args)
        {
            _apply(arg);
            _apply(args...);
        }



    };



    template <class _DerivedT, class _ChildT>
    class ParentSelectorOps
    {
    public:
        using DerivedT = _DerivedT;
        using ChildT = _ChildT;


    public:
        virtual ~ParentSelectorOps() = default;

    protected:

        ChildT& _addChild()
        {
            return _children.emplace_back();
        }
        ChildT& _addChild(const ChildT& child)
        {
            return _children.emplace_back(child);
        }

        template <class ...Ts>
        ChildT& _addChild(Ts... args)
        {
            ChildT& sel = _addChild();
            sel._apply(args...);
            return sel;
        }


        std::vector<ChildT> _children;
    };



    template <class DerivedT, class QueryT, class ChildT>
    class InnerSelectorOps :
        public ParentSelectorOps<DerivedT, ChildT>,
        public ChildSelectorOps<DerivedT, QueryT>
    {
    public:

        InnerSelectorOps() = default;

        virtual std::vector<QueryT> buildQueries() const
        {
            std::vector<typename ChildT::QueryT> childQueries;
            for (const auto& child : this->_children)
            {
                for (auto& query : child.buildQueries())
                {
                    childQueries.push_back(query);
                }
            }

            std::vector<QueryT> queries = this->_queries;
            for (auto& query : queries)
            {
                this->_setChildQueries(query, childQueries);
            }
            return queries;
        }


    protected:

        virtual void _setChildQueries(QueryT& query, const std::vector<typename ChildT::QueryT>& childQueries) const = 0;

    };

}
