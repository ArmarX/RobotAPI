#include "Builder.h"


namespace armarx::armem::client::query
{

    Builder::Builder(armem::query::DataMode dataMode, armem::query::QueryTarget target) : dataMode(dataMode), queryTarget(target)
    {
    }

    QueryInput Builder::buildQueryInput() const
    {
        QueryInput input;
        input.memoryQueries = buildMemoryQueries();
        input.dataMode = dataMode;
        return input;
    }
    armem::query::data::Input Builder::buildQueryInputIce() const
    {
        return buildQueryInput().toIce();
    }

    armem::query::data::MemoryQuerySeq Builder::buildMemoryQueries() const
    {
        armem::query::data::MemoryQuerySeq memoryQueries;
        for (const CoreSegmentSelector& child : _children)
        {
            for (armem::query::data::MemoryQueryPtr& query : child.buildQueries())
            {
                toIce(query->target, queryTarget);
                memoryQueries.push_back(query);
            }
        }
        return memoryQueries;
    }


    CoreSegmentSelector& Builder::coreSegments()
    {
        return _addChild();
    }

    CoreSegmentSelector& Builder::coreSegments(const CoreSegmentSelector& selector)
    {
        return _addChild(selector);
    }


    void Builder::all()
    {
        coreSegments().all()
        .providerSegments().all()
        .entities().all()
        .snapshots().all();
    }


    void Builder::allLatest()
    {
        coreSegments().all()
        .providerSegments().all()
        .entities().all()
        .snapshots().latest();
    }


    void Builder::allInCoreSegment(const MemoryID& coreSegmentID)
    {
        coreSegments().withName(coreSegmentID.coreSegmentName)
        .providerSegments().all()
        .entities().all()
        .snapshots().all();
    }

    void Builder::allLatestInCoreSegment(const MemoryID& coreSegmentID)
    {
        coreSegments().withName(coreSegmentID.coreSegmentName)
        .providerSegments().all()
        .entities().all()
        .snapshots().latest();
    }

    void Builder::allInProviderSegment(const MemoryID& providerSegmentID)
    {
        coreSegments().withName(providerSegmentID.coreSegmentName)
        .providerSegments().withName(providerSegmentID.providerSegmentName)
        .entities().all()
        .snapshots().all();
    }

    void Builder::allLatestInProviderSegment(const MemoryID& providerSegmentID)
    {
        coreSegments().withName(providerSegmentID.coreSegmentName)
        .providerSegments().withName(providerSegmentID.providerSegmentName)
        .entities().all()
        .snapshots().latest();
    }

    void Builder::allEntitySnapshots(const MemoryID& entityID)
    {
        coreSegments().withName(entityID.coreSegmentName)
        .providerSegments().withName(entityID.providerSegmentName)
        .entities().withName(entityID.entityName)
        .snapshots().all();
    }

    void Builder::latestEntitySnapshot(const MemoryID& entityID)
    {
        coreSegments().withName(entityID.coreSegmentName)
        .providerSegments().withName(entityID.providerSegmentName)
        .entities().withName(entityID.entityName)
        .snapshots().latest();
    }

    void Builder::singleEntitySnapshot(const MemoryID& snapshotID)
    {
        coreSegments().withName(snapshotID.coreSegmentName)
        .providerSegments().withName(snapshotID.providerSegmentName)
        .entities().withName(snapshotID.entityName)
        .snapshots().atTime(snapshotID.timestamp);
    }

    void Builder::multipleEntitySnapshots(const std::vector<MemoryID>& snapshotIDs)
    {
        for (const MemoryID& snapshotID : snapshotIDs)
        {
            singleEntitySnapshot(snapshotID);
        }
    }

}
