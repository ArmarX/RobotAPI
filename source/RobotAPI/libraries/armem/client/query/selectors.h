#pragma once

#include <RobotAPI/interface/armem/query.h>

#include <RobotAPI/libraries/armem/core/Time.h>

#include "detail/NameSelectorOps.h"
#include "detail/SelectorOps.h"


namespace armarx::armem::client::query
{
    class SnapshotSelector :
        public detail::ChildSelectorOps<SnapshotSelector, armem::query::data::EntityQueryPtr>
    {
    public:

        SnapshotSelector() = default;

        armem::query::data::EntityQuerySeq buildQueries() const;


        SnapshotSelector& all() override;
        SnapshotSelector& withID(const MemoryID& id) override
        {
            return atTime(id.timestamp);
        }

        SnapshotSelector& latest();
        SnapshotSelector& atTime(Time timestamp);
        SnapshotSelector& atTimeApprox(Time timestamp, Duration eps);

        SnapshotSelector& beforeTime(Time timestamp, long maxEntries = 1);
        SnapshotSelector& beforeOrAtTime(Time timestamp);

        SnapshotSelector& timeRange(Time min, Time max);
        SnapshotSelector& indexRange(long first, long last);

    };


    class EntitySelector :
        public detail::InnerSelectorOps<EntitySelector, armem::query::data::ProviderSegmentQueryPtr, SnapshotSelector>
        , public detail::NameSelectorOps<EntitySelector>

    {
    public:

        EntitySelector() = default;

        /// Start specifying entity snapshots.
        SnapshotSelector& snapshots();
        SnapshotSelector& snapshots(const SnapshotSelector& selector);

        template <class ...Ts>
        SnapshotSelector& snapshots(Ts... args)
        {
            return _addChild(args...);
        }


        EntitySelector& all() override;
        EntitySelector& withID(const MemoryID& id) override
        {
            return withName(id.entityName);
        }

        EntitySelector& withName(const std::string& name) override;
        EntitySelector& withNamesMatching(const std::string& regex) override;
        using NameSelectorOps::withNames;


    protected:
        void _setChildQueries(armem::query::data::ProviderSegmentQueryPtr& query, const armem::query::data::EntityQuerySeq& childQueries) const override;

    };



    class ProviderSegmentSelector :
        public detail::InnerSelectorOps<ProviderSegmentSelector, armem::query::data::CoreSegmentQueryPtr, EntitySelector>
        , public detail::NameSelectorOps<ProviderSegmentSelector>
    {
    public:

        ProviderSegmentSelector() = default;

        /// Start specifying entities.
        EntitySelector& entities();
        EntitySelector& entities(const EntitySelector& selector);

        template <class ...Ts>
        EntitySelector& entities(Ts... args)
        {
            return _addChild(args...);
        }


        ProviderSegmentSelector& all() override;
        ProviderSegmentSelector& withID(const MemoryID& id) override
        {
            return withName(id.providerSegmentName);
        }

        ProviderSegmentSelector& withName(const std::string& name) override;
        ProviderSegmentSelector& withNamesMatching(const std::string& regex) override;
        using NameSelectorOps::withNames;


    protected:
        void _setChildQueries(armem::query::data::CoreSegmentQueryPtr& query, const armem::query::data::ProviderSegmentQuerySeq& childQueries) const override;

    };



    class CoreSegmentSelector :
        public detail::InnerSelectorOps<CoreSegmentSelector, armem::query::data::MemoryQueryPtr, ProviderSegmentSelector>
        , public detail::NameSelectorOps<CoreSegmentSelector>
    {
    public:

        CoreSegmentSelector() = default;

        /// Start specifying provider segments.
        ProviderSegmentSelector& providerSegments();
        ProviderSegmentSelector& providerSegments(const ProviderSegmentSelector& selector);

        template <class ...Ts>
        ProviderSegmentSelector& providerSegments(Ts... args)
        {
            return _addChild(args...);
        }


        CoreSegmentSelector& all() override;
        CoreSegmentSelector& withID(const MemoryID& id) override
        {
            return withName(id.coreSegmentName);
        }

        CoreSegmentSelector& withName(const std::string& name) override;
        CoreSegmentSelector& withNamesMatching(const std::string& regex) override;
        using NameSelectorOps::withNames;


    protected:
        void _setChildQueries(armem::query::data::MemoryQueryPtr& query, const armem::query::data::CoreSegmentQuerySeq& childQueries) const override;

    };

}
