#include "selectors.h"

#include <ArmarXCore/core/ice_conversions.h>
#include <ArmarXCore/core/time/ice_conversions.h>
#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>

#include <RobotAPI/libraries/armem/core/ice_conversions.h>
#include <RobotAPI/libraries/armem/core/wm/ice_conversions.h>


namespace dq = ::armarx::armem::query::data;


namespace armarx::armem::client::query
{
    dq::EntityQuerySeq SnapshotSelector::buildQueries() const
    {
        return _queries;
    }

    SnapshotSelector& SnapshotSelector::all()
    {
        auto& q = _addQuery<dq::entity::All>();
        (void) q;
        return *this;
    }

    SnapshotSelector& SnapshotSelector::latest()
    {
        auto& q = _addQuery<dq::entity::Single>();
        toIce(q.timestamp, Time::Invalid());
        return *this;
    }

    SnapshotSelector& SnapshotSelector::atTime(Time timestamp)
    {
        auto& q = _addQuery<dq::entity::Single>();
        toIce(q.timestamp, timestamp);
        return *this;
    }

    SnapshotSelector& SnapshotSelector::timeRange(Time min, Time max)
    {
        auto& q = _addQuery<dq::entity::TimeRange>();
        toIce(q.minTimestamp, min);
        toIce(q.maxTimestamp, max);
        return *this;
    }

    SnapshotSelector& SnapshotSelector::atTimeApprox(Time timestamp, Duration eps)
    {
        auto& q = _addQuery<dq::entity::TimeApprox>();
        toIce(q.timestamp, timestamp);
        toIce(q.eps, eps);
        return *this;
    }


    SnapshotSelector& SnapshotSelector::indexRange(long first, long last)
    {
        auto& q = _addQuery<dq::entity::IndexRange>();
        q.first = first;
        q.last = last;
        return *this;
    }


    SnapshotSelector& SnapshotSelector::beforeOrAtTime(Time timestamp)
    {
        auto& q = _addQuery<dq::entity::BeforeOrAtTime>();
        toIce(q.timestamp, timestamp);
        return *this;
    }


    SnapshotSelector& SnapshotSelector::beforeTime(Time timestamp, long maxEntries)
    {
        using armarx::toIce;

        auto& q = _addQuery<dq::entity::BeforeTime>();
        toIce(q.timestamp, timestamp);
        toIce(q.maxEntries, maxEntries);
        return *this;
    }

    SnapshotSelector& EntitySelector::snapshots()
    {
        return _addChild();
    }

    SnapshotSelector& EntitySelector::snapshots(const ChildT& selector)
    {
        return _addChild(selector);
    }

    EntitySelector& EntitySelector::all()
    {
        auto& q = _addQuery<dq::provider::All>();
        (void) q;
        return *this;
    }

    EntitySelector& EntitySelector::withName(const std::string& name)
    {
        auto& q = _addQuery<dq::provider::Single>();
        q.entityName = name;
        return *this;
    }

    EntitySelector& EntitySelector::withNamesMatching(const std::string& regex)
    {
        auto& q = _addQuery<dq::provider::Regex>();
        q.entityNameRegex = regex;
        return *this;
    }

    void EntitySelector::_setChildQueries(dq::ProviderSegmentQueryPtr& query, const dq::EntityQuerySeq& childQueries) const
    {
        query->entityQueries = childQueries;
    }


    EntitySelector& ProviderSegmentSelector::entities()
    {
        return _addChild();
    }

    EntitySelector& ProviderSegmentSelector::entities(const EntitySelector& selector)
    {
        return _addChild(selector);
    }

    ProviderSegmentSelector& ProviderSegmentSelector::all()
    {
        auto& q = _addQuery<dq::core::All>();
        (void) q;
        return *this;
    }

    ProviderSegmentSelector& ProviderSegmentSelector::withName(const std::string& name)
    {
        auto& q = _addQuery<dq::core::Single>();
        q.providerSegmentName = name;
        return *this;
    }

    ProviderSegmentSelector& ProviderSegmentSelector::withNamesMatching(const std::string& regex)
    {
        auto& q = _addQuery<dq::core::Regex>();
        q.providerSegmentNameRegex = regex;
        return *this;
    }

    void ProviderSegmentSelector::_setChildQueries(dq::CoreSegmentQueryPtr& query, const dq::ProviderSegmentQuerySeq& childQueries) const
    {
        query->providerSegmentQueries = childQueries;
    }


    ProviderSegmentSelector& CoreSegmentSelector::providerSegments()
    {
        return _addChild();
    }

    ProviderSegmentSelector& CoreSegmentSelector::providerSegments(const ProviderSegmentSelector& selector)
    {
        return _addChild(selector);
    }

    CoreSegmentSelector& CoreSegmentSelector::all()
    {
        auto& q = _addQuery<dq::memory::All>();
        (void) q;
        return *this;
    }

    CoreSegmentSelector& CoreSegmentSelector::withName(const std::string& name)
    {
        auto& q = _addQuery<dq::memory::Single>();
        q.coreSegmentName = name;
        return *this;
    }
    CoreSegmentSelector& CoreSegmentSelector::withNamesMatching(const std::string& regex)
    {
        auto& q = _addQuery<dq::memory::Regex>();
        q.coreSegmentNameRegex = regex;
        return *this;
    }

    void CoreSegmentSelector::_setChildQueries(dq::MemoryQueryPtr& query, const dq::CoreSegmentQuerySeq& childQueries) const
    {
        query->coreSegmentQueries = childQueries;
    }

}
