/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::libraries::armem
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <optional>

#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>
#include <RobotAPI/interface/armem/server/MemoryInterface.h>

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/client/util/MemoryListener.h>
#include <RobotAPI/libraries/armem/mns/Registry.h>


namespace armarx
{
    class ManagedIceObject;
}
namespace armarx::armem::client
{
    class Reader;
    class Writer;
}

namespace armarx::armem::wm
{
    class EntityInstance;
}

namespace armarx::armem::client
{

    /**
     * @brief The memory name system (MNS) client.
     *
     * This client class serves provides the MNS interface and is a local cache
     * of the MNS registry. It can be used to resolve memory servers by their
     * memory ID and to construct `client::Readers` and `client::Writers`.
     *
     * During server resolution, it first consults the locally cached registry.
     * If the memory server is not known locally, the local registry is
     * updated to that of the remote MNS before trying again.
     *
     * In addition, the MNS client can be used to send queries over multiple
     * memory servers, as well as retrieving the data for arbitrary entity or
     * entity snapshot IDs.
     */
    class MemoryNameSystem : public util::MemoryListener
    {
    public:

        MemoryNameSystem();

        /**
         * @brief Construct an MNS client.
         *
         * @param mns The MNS proxy.
         * @param component The owning component. When `using` a memory server,
         *  dependencies will be added to this component.
         */
        MemoryNameSystem(mns::MemoryNameSystemInterfacePrx mns,
                         ManagedIceObject* component = nullptr);

        void initialize(mns::MemoryNameSystemInterfacePrx mns, ManagedIceObject* component = nullptr);


        mns::MemoryNameSystemInterfacePrx getMemoryNameSystem() const;
        void getMemoryNameSystem(mns::MemoryNameSystemInterfacePrx mns);

        void setComponent(ManagedIceObject* component);


        // Name Resolution

        /**
         * @brief Update the internal registry to the data in the MNS.
         *
         * @throw `error::MemoryNameSystemQueryFailed` If the call to the MNS failed.
         */
        void update();


        // Reader/Writer construction

        /**
         * @brief Get a reader to the given memory name.
         *
         * @param memoryID The memory ID.
         * @return The reader.
         *
         * @throw `error::CouldNotResolveMemoryServer` If the memory name could not be resolved.
         */
        Reader getReader(const MemoryID& memoryID);

        /// Use a memory server and get a reader for it.
        Reader useReader(const MemoryID& memoryID);
        Reader useReader(const MemoryID& memoryID, ManagedIceObject& component);
        Reader useReader(const std::string& memoryName);
        Reader useReader(const std::string& memoryName, ManagedIceObject& component);

        /**
         * @brief Get Readers for all registered servers.
         *
         * @param update If true, perform an update first.
         * @return The readers.
         */
        std::map<std::string, Reader> getAllReaders(bool update = true);
        /**
         * @brief Get Readers for all registered servers (without updating).
         *
         * @return The readers.
         */
        std::map<std::string, Reader> getAllReaders() const;


        /**
         * @brief Get a writer to the given memory name.
         *
         * @param memoryID The memory ID.
         * @return The writer.
         *
         * @throw `error::CouldNotResolveMemoryServer` If the memory name could not be resolved.
         */
        Writer getWriter(const MemoryID& memoryID);

        /// Use a memory server and get a writer for it.
        Writer useWriter(const MemoryID& memoryID);
        Writer useWriter(const MemoryID& memoryID, ManagedIceObject& component);
        Writer useWriter(const std::string& memoryName);
        Writer useWriter(const std::string& memoryName, ManagedIceObject& component);

        /**
         * @brief Get Writers for all registered servers.
         *
         * @param update If true, perform an update first.
         * @return The readers.
         */
        std::map<std::string, Writer> getAllWriters(bool update = true);
        /**
         * @brief Get Writers for all registered servers (without updating).
         *
         * @return The readers.
         */
        std::map<std::string, Writer> getAllWriters() const;


        // ToDo: commit() and query()

        /**
         * @brief Resolve a memory ID to an EntityInstance.
         *
         * The ID can refer to an entity, an entity snapshot, or an entity
         * instance. When not referring to an entity instance, the latest
         * snapshot and first instance will be queried.
         *
         * @param id The entity, snapshot or instance ID.
         * @return
         */
        std::optional<wm::EntityInstance> resolveEntityInstance(const MemoryID& id);

        std::map<MemoryID, wm::EntityInstance> resolveEntityInstances(const std::vector<MemoryID>& ids);

        /**
         * @brief Resolve the given memory server for the given memory ID.
         *
         * @param memoryID The memory ID.
         * @return The memory server proxy.
         *
         * @throw `error::CouldNotResolveMemoryServer` If the memory name could not be resolved.
         */
        mns::dto::MemoryServerInterfaces resolveServer(const MemoryID& memoryID);

        // Registration - only for memory servers

        /**
         * @brief Register a memory server in the MNS.
         *
         * @param memoryID The memoryID.
         * @param server The memory server proxy.
         *
         * @throw `error::ServerRegistrationOrRemovalFailed` If the registration failed.
         */
        void registerServer(const MemoryID& memoryID, mns::dto::MemoryServerInterfaces server);

        /**
         * @brief Remove a memory server from the MNS.
         *
         * @param memoryID The memoryID.
         *
         * @throw `error::ServerRegistrationOrRemovalFailed` If the removal failed.
         */
        void removeServer(const MemoryID& memoryID);



        // Operators

        /// Indicate whether the proxy is set.
        inline operator bool() const
        {
            return bool(mns);
        }


    private:

        /**
         * @brief Wait for the given memory server.
         *
         * @param memoryID The memory ID.
         * @return The memory server proxy.
         *
         * @throw `error::CouldNotResolveMemoryServer` If the memory name could not be resolved.
         */
        mns::dto::MemoryServerInterfaces waitForServer(const MemoryID& memoryID);

        /**
        * @brief Wait for the given memory server and add a dependency to the memory server.
        *
        * @param memoryID The memory ID.
        * @param component The component that should depend on the memory server.
        * @return The memory server proxy.
        *
        * @throw `error::CouldNotResolveMemoryServer` If the memory name could not be resolved.
        */
        mns::dto::MemoryServerInterfaces useServer(const MemoryID& memoryID);
        mns::dto::MemoryServerInterfaces useServer(const MemoryID& memoryID, ManagedIceObject& component);
        mns::dto::MemoryServerInterfaces useServer(const std::string& memoryName);
        mns::dto::MemoryServerInterfaces useServer(const std::string& memoryName, ManagedIceObject& component);


    private:

        template <class ClientT>
        using ClientFactory = std::function<std::optional<ClientT>(const mns::dto::MemoryServerInterfaces& server)>;

        template <class ClientT>
        std::map<std::string, ClientT> _getAllClients(ClientFactory<ClientT>&& factory) const;


        /// The MNS proxy.
        mns::MemoryNameSystemInterfacePrx mns = nullptr;

        /// The component to which dependencies will be added.
        ManagedIceObject* component = nullptr;

        /// The registered memory servers.
        std::map<std::string, mns::dto::MemoryServerInterfaces> servers;

    };


}
