#pragma once

#include <RobotAPI/libraries/armem/client/forward_declarations.h>

#include <RobotAPI/libraries/armem/client/plugins/ListeningPluginUser.h>
#include <RobotAPI/libraries/armem/client/plugins/Plugin.h>
#include <RobotAPI/libraries/armem/client/plugins/PluginUser.h>


namespace armarx::armem::client
{
    using ComponentPluginUser = plugins::ListeningPluginUser;
}
