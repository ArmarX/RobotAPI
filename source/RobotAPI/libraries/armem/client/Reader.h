#pragma once


// STD/STL
#include <optional>
#include <vector>
#include <mutex>

// RobotAPI
#include <RobotAPI/interface/armem/server/ReadingMemoryInterface.h>
#include <RobotAPI/interface/armem/server/RecordingMemoryInterface.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/core/Prediction.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>

#include "Query.h"


namespace armarx::armem::client
{

    /**
     * @brief Reads data from a memory server.
     */
    class Reader
    {

    public:

        /**
         * @brief Construct a memory reader.
         * @param memory The memory proxy.
         */
        Reader(server::ReadingMemoryInterfacePrx readingMemory = nullptr,
               server::PredictingMemoryInterfacePrx predictingMemory = nullptr);

        void setReadingMemory(server::ReadingMemoryInterfacePrx readingMemory);
        void setPredictingMemory(server::PredictingMemoryInterfacePrx predictingMemory);

        /// Perform a query.
        QueryResult query(const QueryInput& input) const;
        armem::query::data::Result query(const armem::query::data::Input& input) const;

        QueryResult query(armem::query::data::MemoryQueryPtr query, armem::query::DataMode dataMode = armem::query::DataMode::WithData) const;
        QueryResult query(const armem::query::data::MemoryQuerySeq& queries, armem::query::DataMode dataMode = armem::query::DataMode::WithData) const;

        QueryResult query(const QueryBuilder& queryBuilder) const;

        /** Perform a query with recursive memory link resolution.
         *
         * Resolves the links in the query result using the given MemoryNameSystem
         * and inserts the data into the MemoryLink objects' data fields.
         * If the inserted data also contains links, those will be resolved as well
         * up to and including the given recursion depth. Link cycles are detected;
         * the first repeated memory ID will not be inserted into the data.
         * Setting the recursion depth to `-1` is equivalent to an infinite
         * recursion depth.
         *
         * Standalone `MemoryID` subobjects are ignored; only MemoryIDs inside of
         * MemoryLink subobjects are resolved. Empty or unresolvable MemoryIDs
         * are ignored - this includes MemoryIDs whose Entity fields are not set.
         * If the data associated with a MemoryID could not be retrieved or
         * its type does not match the template type of the MemoryLink,
         * it is ignored.
         *
         * @param input the query to perform
         * @param mns the MemoryNameSystem to use when resolving MemoryLinks
         * @param recursionDepth how many layers of MemoryLinks to resolve
         */
        QueryResult query(const QueryInput& input,
                          armem::client::MemoryNameSystem& mns,
                          int recursionDepth = -1) const;
        /**
         * @sa Reader::query(const QueryInput&, armem::client::MemoryNameSystem, int)
         */
        armem::query::data::Result query(const armem::query::data::Input& input,
                                         armem::client::MemoryNameSystem& mns,
                                         int recursionDepth = -1) const;

        /**
         * @sa Reader::query(const QueryInput&, armem::client::MemoryNameSystem, int)
         */
        QueryResult query(armem::query::data::MemoryQueryPtr query,
                          armem::client::MemoryNameSystem& mns,
                          int recursionDepth = -1) const;
        /**
         * @sa Reader::query(const QueryInput&, armem::client::MemoryNameSystem, int)
         */
        QueryResult query(const armem::query::data::MemoryQuerySeq& queries,
                          armem::client::MemoryNameSystem& mns,
                          int recursionDepth = -1) const;

        /**
         * @sa Reader::query(const QueryInput&, armem::client::MemoryNameSystem, int)
         */
        QueryResult query(const QueryBuilder& queryBuilder,
                          armem::client::MemoryNameSystem& mns,
                          int recursionDepth = -1) const;


        /**
         * @brief Query a specific set of memory IDs.
         *
         * Each ID can refer to an entity, a snapshot or an instance. When not
         * referring to an entity instance, the latest snapshot and first
         * instance will be queried, respectively.
         *
         * All memory IDs must refer to the memory this reader is reading from.
         * If an ID refers to another memory, the query will not find it and it
         * will not be part of the result.
         *
         * @param ids The entity, snapshot or instance IDs.
         * @param dataMode Whether to include instance data or just meta data.
         * @return The query result.
         */
        QueryResult
        queryMemoryIDs(const std::vector<MemoryID>& ids, armem::query::DataMode dataMode = armem::query::DataMode::WithData) const;


        /**
         * @brief Query the given snapshot and return the latest existing snapshot.
         * @param snapshotIDs The snapshot (or entity) IDs.
         * @return The latest snapshot contained in the query result, if any.
         */
        std::optional<wm::EntitySnapshot>
        getLatestSnapshotOf(const std::vector<MemoryID>& snapshotIDs) const;


        /**
         * @brief Get the latest snapshots under the given memory ID.
         * @param id A memory, core segment, provider segment or entity ID.
         * @param dataMode With or without data.
         * @return The query result.
         */
        QueryResult
        getLatestSnapshotsIn(const MemoryID& id, armem::query::DataMode dataMode = armem::query::DataMode::WithData) const;

        /**
         * @brief Get the latest snapshot under the given memory ID.
         * @param id A memory, core segment, provider segment or entity ID.
         * @param dataMode With or without data.
         * @return The latest contained snapshot, if any.
         */
        std::optional<wm::EntitySnapshot>
        getLatestSnapshotIn(const MemoryID& id, armem::query::DataMode dataMode = armem::query::DataMode::WithData) const;


        /**
         * @brief Get all latest snapshots in the memory.
         * @param dataMode With or without data.
         * @return The query result.
         */
        QueryResult
        getAllLatestSnapshots(armem::query::DataMode dataMode = armem::query::DataMode::WithData) const;


        /**
         * @brief Get the whole memory content.
         * @param dataMode With or without data.
         * @return The query result.
         */
        QueryResult
        getAll(armem::query::DataMode dataMode = armem::query::DataMode::WithData) const;


        server::dto::DirectlyStoreResult directlyStore(const server::dto::DirectlyStoreInput& input) const;
        void startRecording() const;
        void stopRecording() const;

        /**
         * @brief Get a prediction for the state of multiple entity instances in the future.
         *
         * The timestamp to predict the entities' state at is given via the snapshot
         * given in the MemoryID.
         *
         * The `predictionPrx` must not be null when calling this function.
         *
         * @param requests a list of requests for entity instance predictions
         * @return the vector of prediction results
         */
        std::vector<PredictionResult> predict(const std::vector<PredictionRequest>& requests) const;

        /**
         * @brief Get the list of prediction engines supported by the memory.
         *
         * The `predictionPrx` must not be null when calling this function.
         *
         * @return a map from memory containers to their supported engines
         */
        std::map<MemoryID, std::vector<PredictionEngine>> getAvailablePredictionEngines() const;

        inline operator bool() const
        {
            return bool(readingPrx);
        }

    private:
        static void resolveMemoryLinks(armem::wm::EntityInstance& instance,
                                       armem::wm::Memory& input,
                                       armem::client::MemoryNameSystem& mns,
                                       int recursionDepth);

    public:

        server::ReadingMemoryInterfacePrx readingPrx;
        server::PredictingMemoryInterfacePrx predictionPrx;
    };

} // namespace armarx::armem::client
