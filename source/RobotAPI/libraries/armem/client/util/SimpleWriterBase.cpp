#include "SimpleWriterBase.h"

#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>


namespace armarx::armem::client::util
{
    SimpleWriterBase::SimpleWriterBase(MemoryNameSystem& memoryNameSystem) :
        memoryNameSystem(memoryNameSystem)
    {
    }


    void
    SimpleWriterBase::registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def)
    {
        ARMARX_DEBUG << "Writer: registerPropertyDefinitions";

        const std::string prefix = propertyPrefix();

        props = defaultProperties();

        def->optional(props.memoryName, prefix + "Memory");
        def->optional(props.coreSegmentName, prefix + "CoreSegment");

        // TODO(fabian.reister): this might also be part of the subclass
        //  if the provider name has to be derived from e.g. the component name
        def->optional(props.providerName,
                      prefix + "Provider",
                      "Name of this provider");
    }


    void SimpleWriterBase::connect()
    {
        // Wait for the memory to become available and add it as dependency.
        ARMARX_IMPORTANT << "SimpleWriterBase: Waiting for memory '"
                         << props.memoryName << "' ...";
        try
        {
            memoryWriterClient = memoryNameSystem.useWriter(MemoryID().withMemoryName(props.memoryName));
            ARMARX_IMPORTANT << "SimpleWriterBase: Connected to memory '" << props.memoryName << "'";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }
    }


    std::mutex& SimpleWriterBase::memoryWriterMutex()
    {
        return memoryMutex;
    }


    armem::client::Writer& SimpleWriterBase::memoryWriter()
    {
        return memoryWriterClient;
    }


    const SimpleWriterBase::Properties& SimpleWriterBase::properties() const
    {
        return props;
    }

} // namespace armarx::armem::client::util
