#pragma once


// STD/STL
#include <functional>
#include <unordered_map>
#include <vector>

// RobotAPI
#include <RobotAPI/interface/armem/client/MemoryListenerInterface.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>



namespace armarx
{
    class ManagedIceObject;
}

namespace armarx::armem::client::util
{

    /**
     * @brief Handles update signals from the memory system and distributes it
     * to its subsribers.
     */
    class MemoryListener
    {
    public:

        using Callback = std::function<void(const MemoryID& subscriptionID, const std::vector<MemoryID>& updatedSnapshotIDs)>;
        using CallbackUpdatedOnly = std::function<void(const std::vector<MemoryID>& updatedSnapshotIDs)>;

        template <class CalleeT>
        using MemberCallback = void(CalleeT::*)(const MemoryID& subscriptionID, const std::vector<MemoryID>& updatedSnapshotIDs);
        template <class CalleeT>
        using MemberCallbackUpdatedOnly = void(CalleeT::*)(const std::vector<MemoryID>& updatedSnapshotIDs);

        static std::string MakeMemoryTopicName(const MemoryID& memoryID);


    public:

        MemoryListener(ManagedIceObject* component = nullptr);

        void setComponent(ManagedIceObject* component);

        void subscribe(const MemoryID& subscriptionID, Callback Callback);
        void subscribe(const MemoryID& subscriptionID, CallbackUpdatedOnly Callback);

        /**
         * Subscribe with a class member function:
         * @code
         * listener.subscribe(entityID, this, &This::myCallback);
         * @endcode
         */
        template <class CalleeT>
        void subscribe(const MemoryID& subscriptionID, CalleeT* callee, MemberCallback<CalleeT> callback)
        {
            auto cb = [callee, callback](const MemoryID & subscriptionID, const std::vector<MemoryID>& updatedSnapshotIDs)
            {
                (callee->*callback)(subscriptionID, updatedSnapshotIDs);
            };
            subscribe(subscriptionID, cb);
        }
        template <class CalleeT>
        void subscribe(const MemoryID& subscriptionID, CalleeT* callee, MemberCallbackUpdatedOnly<CalleeT> callback)
        {
            auto cb = [callee, callback](const MemoryID&, const std::vector<MemoryID>& updatedSnapshotIDs)
            {
                (callee->*callback)(updatedSnapshotIDs);
            };
            subscribe(subscriptionID, cb);
        }


        /// Function handling updates from the MemoryListener ice topic.
        void updated(const std::vector<MemoryID>& updatedIDs) const;
        void updated(const std::vector<data::MemoryID>& updatedIDs) const;


    protected:

        std::unordered_map<MemoryID, std::vector<Callback>> callbacks;

    private:

        armarx::ManagedIceObject* component;

    };

}
