#include "MemoryListener.h"

#include <sstream>

#include <ArmarXCore/core/exceptions/LocalException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>
#include <ArmarXCore/core/ManagedIceObject.h>

#include <RobotAPI/libraries/armem/core/ice_conversions.h>
#include <RobotAPI/libraries/armem/core/error.h>


namespace armarx::armem::client::util
{

    std::string MemoryListener::MakeMemoryTopicName(const MemoryID& memoryID)
    {
        return "MemoryUpdates." + memoryID.memoryName;
    }


    MemoryListener::MemoryListener(ManagedIceObject* component) :
        component(component)
    {
    }


    void MemoryListener::setComponent(ManagedIceObject* component)
    {
        this->component = component;
    }


    void
    MemoryListener::updated(const std::vector<data::MemoryID>& updatedSnapshotIDs) const
    {
        std::vector<MemoryID> bos;
        armarx::fromIce(updatedSnapshotIDs, bos);
        updated(bos);
    }


    void
    MemoryListener::updated(const std::vector<MemoryID>& updatedSnapshotIDs) const
    {
        std::stringstream error;

        for (const auto& [subscription, subCallbacks] : this->callbacks)
        {
            std::vector<MemoryID> matchingSnapshotIDs;

            for (const MemoryID& updatedSnapshotID : updatedSnapshotIDs)
            {
                try
                {
                    if (contains(subscription, updatedSnapshotID))
                    {
                        // ARMARX_IMPORTANT << VAROUT(subscription) << " matches " << VAROUT(updatedSnapshotID);
                        matchingSnapshotIDs.push_back(updatedSnapshotID);
                    }
                    else
                    {
                        // ARMARX_IMPORTANT << VAROUT(subscription) << " does not match " << VAROUT(updatedSnapshotID);
                    }
                }
                catch (const armem::error::InvalidMemoryID& e)
                {
                    // Log to debug, but ignore otherwise
                    error << "Error when comparing subscribed ID " << subscription
                          << " with updated ID " << updatedSnapshotID << ":\n"
                          << e.what() << "\n\n";
                }
            }

            if (not matchingSnapshotIDs.empty())
            {
                ARMARX_DEBUG << "Calling " << subCallbacks.size() << " callbacks"
                             << " subscribing " << subscription
                             << " with " << matchingSnapshotIDs.size() << " snapshot IDs ...";
                for (auto& callback : subCallbacks)
                {
                    try
                    {
                        callback(subscription, matchingSnapshotIDs);
                    }
                    catch (const armarx::LocalException& e)
                    {
                        error << "Calling callback subscribing " << subscription << " failed."
                              << "\nCaught armarx::LocalException:"
                              << "\n" << e.getReason()
                              << "\n Stacktrace: \n" << e.generateBacktrace()
                              << "\n"
                                 ;
                    }
                    catch (const std::exception& e)
                    {
                        error << "Calling callback subscribing " << subscription << " failed."
                              << "\nCaught armarx::Exception:"
                              << "\n" << e.what()
                              << "\n"
                                 ;
                    }
                    catch (...)
                    {
                        error << "Calling callback subscribing " << subscription << " failed."
                              << "\nCaught unknown exception."
                              << "\n"
                                 ;
                    }
                }
            }
        }
        if (error.str().size() > 0)
        {
            ARMARX_WARNING << "The following issues were encountered during MemoryListener::" << __FUNCTION__ << "(): \n\n"
                           << error.str();
        }
    }


    void
    MemoryListener::subscribe(const MemoryID& id, Callback callback)
    {
        callbacks[id].push_back(callback);
        if (component)
        {
            component->usingTopic(MakeMemoryTopicName(id));
        }
    }


    void
    MemoryListener::subscribe(const MemoryID& subscriptionID, CallbackUpdatedOnly callback)
    {
        subscribe(subscriptionID, [callback](const MemoryID&, const std::vector<MemoryID>& updatedSnapshotIDs)
        {
            callback(updatedSnapshotIDs);
        });
    }


}
