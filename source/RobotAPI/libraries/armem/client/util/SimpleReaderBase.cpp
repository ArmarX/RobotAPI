#include "SimpleReaderBase.h"

#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>


namespace armarx::armem::client::util
{

    SimpleReaderBase::SimpleReaderBase(MemoryNameSystem& memoryNameSystem) :
        memoryNameSystem(memoryNameSystem)
    {
    }


    void SimpleReaderBase::registerPropertyDefinitions(
        armarx::PropertyDefinitionsPtr& def)
    {
        ARMARX_DEBUG << "Writer: registerPropertyDefinitions";

        const std::string prefix = propertyPrefix();

        props = defaultProperties();

        def->optional(props.memoryName, prefix + "Memory");
        def->optional(props.coreSegmentName, prefix + "CoreSegment");
    }


    void SimpleReaderBase::connect()
    {
        // Wait for the memory to become available and add it as dependency.
        ARMARX_IMPORTANT << "SimpleReaderBase: Waiting for memory '" << props.memoryName
                         << "' ...";
        try
        {
            memoryReaderClient = memoryNameSystem.useReader(MemoryID().withMemoryName(props.memoryName));
            ARMARX_IMPORTANT << "SimpleReaderBase: Connected to memory '" << props.memoryName << "'";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }
    }


    std::mutex& SimpleReaderBase::memoryReaderMutex()
    {
        return memoryMutex;
    }


    const armem::client::Reader& SimpleReaderBase::memoryReader() const
    {
        return memoryReaderClient;
    }


    const SimpleReaderBase::Properties& SimpleReaderBase::properties() const
    {
        return props;
    }

} // namespace armarx::armem::client::util
