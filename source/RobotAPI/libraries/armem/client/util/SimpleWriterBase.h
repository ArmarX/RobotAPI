/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <RobotAPI/libraries/armem/client/Writer.h>


namespace armarx::armem::client
{
    class MemoryNameSystem;
}

namespace armarx::armem::client::util
{

    class SimpleWriterBase
    {
    public:

        SimpleWriterBase(MemoryNameSystem& memoryNameSystem);
        virtual ~SimpleWriterBase() = default;

        void registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def);
        void connect();


    protected:

        struct Properties
        {
            std::string memoryName      = "";
            std::string coreSegmentName = "";
            std::string providerName    = ""; // required property
        };

        const Properties& properties() const;

        virtual std::string propertyPrefix() const   = 0;
        virtual Properties defaultProperties() const = 0;

        std::mutex& memoryWriterMutex();
        armem::client::Writer& memoryWriter();


    private:

        Properties props;

        armem::client::Writer memoryWriterClient;
        std::mutex memoryMutex;

        MemoryNameSystem& memoryNameSystem;

    };

} // namespace armarx::armem::client::util
