/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <RobotAPI/libraries/armem/client/Reader.h>


namespace armarx::armem::client
{
    class MemoryNameSystem;
}

namespace armarx::armem::client::util
{

    class SimpleReaderBase
    {
    public:

        SimpleReaderBase(MemoryNameSystem& memoryNameSystem);
        virtual ~SimpleReaderBase() = default;

        void registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def);
        virtual void connect();


    protected:

        // Properties
        struct Properties
        {
            std::string memoryName      = "";
            std::string coreSegmentName = "";
        };

        const Properties& properties() const;

        virtual std::string propertyPrefix() const   = 0;
        virtual Properties defaultProperties() const = 0;

        std::mutex& memoryReaderMutex();
        const armem::client::Reader& memoryReader() const;


        MemoryNameSystem& memoryNameSystem;

    private:

        Properties props;

        armem::client::Reader memoryReaderClient;
        std::mutex memoryMutex;

    };

} // namespace armarx::armem::client::util
