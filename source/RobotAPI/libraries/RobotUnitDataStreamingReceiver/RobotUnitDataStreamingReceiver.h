/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotUnitDataStreamingReceiver
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <type_traits>

#include <ArmarXCore/core/ManagedIceObject.h>

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

namespace armarx::detail::RobotUnitDataStreamingReceiver
{
    TYPEDEF_PTRS_HANDLE(Receiver);
}
namespace armarx
{
    TYPEDEF_PTRS_SHARED(RobotUnitDataStreamingReceiver);
    /**
    * @defgroup Library-RobotUnitDataStreamingReceiver RobotUnitDataStreamingReceiver
    * @ingroup RobotAPI
    * A description of the library RobotUnitDataStreamingReceiver.
    *
    * @class RobotUnitDataStreamingReceiver
    * @ingroup Library-RobotUnitDataStreamingReceiver
    * @brief Brief description of class RobotUnitDataStreamingReceiver.
    *
    * Detailed description of class RobotUnitDataStreamingReceiver.
    */
    class RobotUnitDataStreamingReceiver
    {
    public:
        using clock_t    = std::chrono::high_resolution_clock;
        using timestep_t = RobotUnitDataStreaming::TimeStep;
        using entry_t    = RobotUnitDataStreaming::DataEntry;

        template<class T>
        struct DataEntryReader
        {
            DataEntryReader()                                  = default;
            DataEntryReader(DataEntryReader&&)                 = default;
            DataEntryReader(const DataEntryReader&)            = default;
            DataEntryReader& operator=(DataEntryReader&&)      = default;
            DataEntryReader& operator=(const DataEntryReader&) = default;

            DataEntryReader(const RobotUnitDataStreaming::DataEntry& k);

            DataEntryReader& operator=(const RobotUnitDataStreaming::DataEntry& k);

            void set(const RobotUnitDataStreaming::DataEntry& k);
            T get(const RobotUnitDataStreaming::TimeStep& t) const;
            T operator()(const RobotUnitDataStreaming::TimeStep& t) const;
        private:
            RobotUnitDataStreaming::DataEntry _key;
        };

        RobotUnitDataStreamingReceiver(
            const ManagedIceObjectPtr& obj,
            const RobotUnitInterfacePrx& ru,
            const RobotUnitDataStreaming::Config& cfg);
        ~RobotUnitDataStreamingReceiver();

        std::deque<timestep_t>& getDataBuffer();
        const RobotUnitDataStreaming::DataStreamingDescription& getDataDescription() const;
        std::string getDataDescriptionString() const;

        template<class T>
        DataEntryReader<T> getDataEntryReader(const std::string& name) const;

        template<class T>
        void getDataEntryReader(DataEntryReader<T>& e, const std::string& name) const;
        //statics
        static void VisitEntry(auto&& f, const timestep_t& st, const entry_t& e);
        template<class T>
        static T GetAs(const timestep_t& st, const entry_t& e);

        template<class T>
        static RobotUnitDataStreaming::DataEntryType ExpectedDataEntryType();
        static void VisitEntries(auto&& f, const timestep_t& st, const auto& cont);
    private:
        ManagedIceObjectPtr                                           _obj;
        RobotUnitInterfacePrx                                         _ru;
        detail::RobotUnitDataStreamingReceiver::ReceiverPtr           _receiver;
        RobotUnitDataStreaming::ReceiverPrx                           _proxy;
        RobotUnitDataStreaming::DataStreamingDescription              _description;
        std::map<std::uint64_t, RobotUnitDataStreaming::TimeStepSeq>  _tmp_data_buffer;
        std::uint64_t                                                 _tmp_data_buffer_seq_id = 0;
        std::deque<RobotUnitDataStreaming::TimeStep>                  _data_buffer;
        long                                                          _last_iteration_id = -1;

    };
}
//impl
namespace armarx
{
    template<class T> inline
    RobotUnitDataStreamingReceiver::DataEntryReader<T>::DataEntryReader(const RobotUnitDataStreaming::DataEntry& k)
    {
        set(k);
    }

    template<class T> inline
    void
    RobotUnitDataStreamingReceiver::DataEntryReader<T>::set(const RobotUnitDataStreaming::DataEntry& k)
    {
        ARMARX_CHECK_EQUAL(RobotUnitDataStreamingReceiver::ExpectedDataEntryType<T>(),
                           k.type) << "the key references a value of the wrong type!";
        _key = k;
    }

    template<class T> inline
    RobotUnitDataStreamingReceiver::DataEntryReader<T>&
    RobotUnitDataStreamingReceiver::DataEntryReader<T>::operator=(const RobotUnitDataStreaming::DataEntry& k)
    {
        set(k);
        return *this;
    }

    template<class T> inline
    T
    RobotUnitDataStreamingReceiver::DataEntryReader<T>::get(const RobotUnitDataStreaming::TimeStep& t) const
    {
        return RobotUnitDataStreamingReceiver::GetAs<T>(t, _key);
    }

    template<class T> inline
    T
    RobotUnitDataStreamingReceiver::DataEntryReader<T>::operator()(const RobotUnitDataStreaming::TimeStep& t) const
    {
        return get(t);
    }

    template<class T> inline
    void
    RobotUnitDataStreamingReceiver::getDataEntryReader(DataEntryReader<T>& e, const std::string& name) const
    {
        e = _description.entries.at(name);
    }
    template<class T> inline
    RobotUnitDataStreamingReceiver::DataEntryReader<T>
    RobotUnitDataStreamingReceiver::getDataEntryReader(const std::string& name) const
    {
        DataEntryReader<T> r;
        getDataEntryReader(r);
        return r;
    }

    inline void
    RobotUnitDataStreamingReceiver::VisitEntry(auto&& f, const timestep_t& st, const entry_t& e)
    {
        using enum_t = RobotUnitDataStreaming::DataEntryType;
    // *INDENT-OFF*
    switch (e.type)
    {
        case enum_t::NodeTypeBool  : f(st.bools  .at(e.index)); break;
        case enum_t::NodeTypeByte  : f(st.bytes  .at(e.index)); break;
        case enum_t::NodeTypeShort : f(st.shorts .at(e.index)); break;
        case enum_t::NodeTypeInt   : f(st.ints   .at(e.index)); break;
        case enum_t::NodeTypeLong  : f(st.longs  .at(e.index)); break;
        case enum_t::NodeTypeFloat : f(st.floats .at(e.index)); break;
        case enum_t::NodeTypeDouble: f(st.doubles.at(e.index)); break;
    };
    // *INDENT-ON*
    }

    template<class T> inline
    T
    RobotUnitDataStreamingReceiver::GetAs(const timestep_t& st, const entry_t& e)
    {
        using enum_t = RobotUnitDataStreaming::DataEntryType;
        if constexpr(std::is_same_v<bool, T>)
        {
            ARMARX_CHECK_EQUAL(e.type, enum_t::NodeTypeBool);
            return st.bools  .at(e.index);
        }
        else if constexpr(std::is_same_v<Ice::Byte, T>)
        {
            ARMARX_CHECK_EQUAL(e.type, enum_t::NodeTypeByte);
            return st.bytes  .at(e.index);
        }
        else if constexpr(std::is_same_v<Ice::Short, T>)
        {
            ARMARX_CHECK_EQUAL(e.type, enum_t::NodeTypeShort);
            return st.shorts .at(e.index);
        }
        else if constexpr(std::is_same_v<Ice::Int, T>)
        {
            ARMARX_CHECK_EQUAL(e.type, enum_t::NodeTypeInt);
            return st.ints   .at(e.index);
        }
        else if constexpr(std::is_same_v<Ice::Long, T>)
        {
            ARMARX_CHECK_EQUAL(e.type, enum_t::NodeTypeLong);
            return st.longs  .at(e.index);
        }
        else if constexpr(std::is_same_v<Ice::Float, T>)
        {
            ARMARX_CHECK_EQUAL(e.type, enum_t::NodeTypeFloat);
            return st.floats .at(e.index);
        }
        else if constexpr(std::is_same_v<Ice::Double, T>)
        {
            ARMARX_CHECK_EQUAL(e.type, enum_t::NodeTypeDouble);
            return st.doubles.at(e.index);
        }
        else
        {
            static_assert(!std::is_same_v<T, T>, "Type not supported!");
        }
    }

    template<class T> inline
    RobotUnitDataStreaming::DataEntryType
    RobotUnitDataStreamingReceiver::ExpectedDataEntryType()
    {
        using enum_t = RobotUnitDataStreaming::DataEntryType;
        if constexpr(std::is_same_v<bool, T>)
        {
            return enum_t::NodeTypeBool;
        }
        else if constexpr(std::is_same_v<Ice::Byte, T>)
        {
            return enum_t::NodeTypeByte;
        }
        else if constexpr(std::is_same_v<Ice::Short, T>)
        {
            return enum_t::NodeTypeShort;
        }
        else if constexpr(std::is_same_v<Ice::Int, T>)
        {
            return enum_t::NodeTypeInt;
        }
        else if constexpr(std::is_same_v<Ice::Long, T>)
        {
            return enum_t::NodeTypeLong;
        }
        else if constexpr(std::is_same_v<Ice::Float, T>)
        {
            return enum_t::NodeTypeFloat;
        }
        else if constexpr(std::is_same_v<Ice::Double, T>)
        {
            return enum_t::NodeTypeDouble;
        }
        else
        {
            static_assert(!std::is_same_v<T, T>, "Type not supported!");
        }
    }

    inline void
    RobotUnitDataStreamingReceiver::VisitEntries(auto&& f, const timestep_t& st, const auto& cont)
    {
        for (const entry_t& e : cont)
        {
            VisitEntry(f, st, e);
        }
    }
}
