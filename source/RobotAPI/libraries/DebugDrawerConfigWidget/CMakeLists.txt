set(LIB_NAME       DebugDrawerConfigWidget)

armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")

armarx_find_qt(QtCore QtGui QtDesigner)

armarx_component_set_name("${LIB_NAME}")
armarx_set_target("Library: ${LIB_NAME}")

set(LIBS
	ArmarXCoreInterfaces 
	ArmarXCore
        DebugDrawer
        ${QT_LIBRARIES}
)

set(SOURCES
./DebugDrawerConfigWidget.cpp
)
set(HEADERS
./DebugDrawerConfigWidget.h
)
set(GUI_MOC_HDRS
./DebugDrawerConfigWidget.h
)
set(GUI_UIS
./DebugDrawerConfigWidget.ui
)

if(ArmarXGui_FOUND)
        armarx_gui_library("${LIB_NAME}" "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${LIBS}")

        # add unit tests
        add_subdirectory(test)
endif()
