
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore DebugDrawerConfigWidget)
 
armarx_add_test(DebugDrawerConfigWidgetTest DebugDrawerConfigWidgetTest.cpp "${LIBS}")