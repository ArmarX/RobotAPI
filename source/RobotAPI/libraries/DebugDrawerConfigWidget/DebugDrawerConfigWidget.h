/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::DebugDrawerConfigWidget
 * @author     Adrian Knobloch ( adrian dot knobloch at student dot kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>

#include <RobotAPI/libraries/DebugDrawerConfigWidget/ui_DebugDrawerConfigWidget.h>

#include <QWidget>
#include <QSettings>


namespace armarx
{
    /**
    * @defgroup Library-DebugDrawerConfigWidget DebugDrawerConfigWidget
    * @ingroup RobotAPI
    * A description of the library DebugDrawerConfigWidget.
    *
    * @class DebugDrawerConfigWidget
    * @ingroup Library-DebugDrawerConfigWidget
    * @brief Brief description of class DebugDrawerConfigWidget.
    *
    * Detailed description of class DebugDrawerConfigWidget.
    */
    class DebugDrawerConfigWidget : public QWidget
    {
    public:
        DebugDrawerConfigWidget(const DebugDrawerComponentPtr& debugDrawer, QWidget* parent = nullptr);

        void loadSettings(QSettings* settings);
        void saveSettings(QSettings* settings);

        void setDebugDrawer(const DebugDrawerComponentPtr& debugDrawer);
        DebugDrawerComponentPtr getDebugDrawer() const;

    public slots:
        void refresh();

        void onVisibilityChanged(QListWidgetItem* item);

        void onDefaultLayerVisibilityChanged(int checkState);
        void onDefaultLayerVisibilityPerLayerChanged(QListWidgetItem* item);

        void onAddDefaultLayerVisibility();
        void onRemoveDefaultLayerVisibility();

    protected:
        DebugDrawerComponentPtr debugDrawer;
        Ui::DebugDrawerConfigWidget ui;
    };

}
