/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Geometry>

#include <RobotAPI/libraries/armem/core/Time.h>


namespace armarx::armem::robot_state
{
    struct TransformHeader
    {
        std::string parentFrame;
        std::string frame;

        std::string agent;

        armem::Time timestamp;
    };

    struct Transform
    {
        TransformHeader header;

        Eigen::Affine3f transform = Eigen::Affine3f::Identity();

        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    };

    struct JointState
    {
        std::string name;
        float position;
    };

}  // namespace armarx::armem::robot_state
