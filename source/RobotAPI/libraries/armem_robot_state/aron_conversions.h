/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem_robot/types.h>

namespace armarx::armem
{
    namespace robot_state
    {
        struct Transform;
        struct TransformHeader;

        struct JointState;

    } // namespace robot_state

    namespace prop::arondto
    {
        struct Platform;
        struct ForceTorque;
    }

    namespace arondto
    {
        struct Transform;
        struct TransformHeader;

        struct JointState;

    } // namespace arondto


    void fromAron(const arondto::Transform& dto, robot_state::Transform& bo);
    void toAron(arondto::Transform& dto, const robot_state::Transform& bo);

    void fromAron(const arondto::TransformHeader& dto, robot_state::TransformHeader& bo);
    void toAron(arondto::TransformHeader& dto, const robot_state::TransformHeader& bo);

    void fromAron(const arondto::JointState& dto, robot_state::JointState& bo);
    void toAron(arondto::JointState& dto, const robot_state::JointState& bo);

    void fromAron(const armarx::armem::prop::arondto::Platform& dto, robot::PlatformState& bo);
    void toAron(armarx::armem::prop::arondto::Platform& dto, const robot::PlatformState& bo);

    void fromAron(const armarx::armem::prop::arondto::ForceTorque& dto, robot::ForceTorque& bo);
    void toAron(armarx::armem::prop::arondto::ForceTorque& dto, const robot::ForceTorque& bo);


}  // namespace armarx::armem
