#include "RobotReader.h"

#include <chrono>
#include <mutex>
#include <optional>
#include <thread>

#include <ArmarXCore/core/PackagePath.h>
#include <ArmarXCore/core/exceptions/LocalException.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time/Clock.h>

#include "RobotAPI/libraries/armem_robot_state/client/common/constants.h"
#include "RobotAPI/libraries/armem_robot_state/common/localization/types.h"
#include "RobotAPI/libraries/core/FramedPose.h"
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem_robot/aron/Robot.aron.generated.h>
#include <RobotAPI/libraries/armem_robot/aron_conversions.h>
#include <RobotAPI/libraries/armem_robot/robot_conversions.h>
#include <RobotAPI/libraries/armem_robot/types.h>
#include <RobotAPI/libraries/armem_robot_state/aron/JointState.aron.generated.h>
#include <RobotAPI/libraries/armem_robot_state/aron/Proprioception.aron.generated.h>
#include <RobotAPI/libraries/armem_robot_state/aron_conversions.h>


namespace fs = ::std::filesystem;

namespace armarx::armem::robot_state
{


    RobotReader::RobotReader(armem::client::MemoryNameSystem& memoryNameSystem) :
        memoryNameSystem(memoryNameSystem), transformReader(memoryNameSystem)
    {
    }

    void
    RobotReader::registerPropertyDefinitions(::armarx::PropertyDefinitionsPtr& def)
    {
        transformReader.registerPropertyDefinitions(def);
    }

    void
    RobotReader::connect()
    {
        transformReader.connect();

        // Wait for the memory to become available and add it as dependency.
        ARMARX_IMPORTANT << "RobotReader: Waiting for memory '" << constants::memoryName << "' ...";
        try
        {
            memoryReader = memoryNameSystem.useReader(constants::memoryName);
            ARMARX_IMPORTANT << "RobotReader: Connected to memory '" << constants::memoryName
                             << "'";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }
    }

    std::optional<robot::Robot>
    RobotReader::get(const std::string& name, const armem::Time& timestamp)
    {
        const auto description = queryDescription(name, timestamp);

        if (not description)
        {
            ARMARX_ERROR << "Unknown object " << name;
            return std::nullopt;
        }

        return get(*description, timestamp);
    }

    robot::Robot
    RobotReader::get(const robot::RobotDescription& description, const armem::Time& timestamp)
    {
        robot::Robot robot{.description = description,
                           .instance = "", // TODO(fabian.reister):
                           .config = {}, // will be populated by synchronize
                           .timestamp = timestamp};

        synchronize(robot, timestamp);

        return robot;
    }

    void
    RobotReader::setSyncTimeout(const armem::Duration& duration)
    {
        syncTimeout = duration;
    }

    void
    RobotReader::setSleepAfterSyncFailure(const armem::Duration& duration)
    {
        ARMARX_CHECK_NONNEGATIVE(duration.toMicroSeconds());
        sleepAfterFailure = duration;
    }

    bool
    RobotReader::synchronize(robot::Robot& obj, const armem::Time& timestamp)
    {
        const auto tsStartFunctionInvokation = armem::Time::Now();

        while (true)
        {
            auto state = queryState(obj.description, timestamp);

            if (not state) /* c++20 [[unlikely]] */
            {
                ARMARX_VERBOSE << "Could not synchronize object " << obj.description.name;

                // if the syncTime is non-positive there will be no retry
                const auto elapsedTime = armem::Time::Now() - tsStartFunctionInvokation;
                if (elapsedTime > syncTimeout)
                {
                    ARMARX_WARNING << "Could not synchronize object " << obj.description.name;
                    return false;
                }

                ARMARX_INFO << "Retrying to query robot state after failure";
                Clock::WaitFor(sleepAfterFailure);
            }

            obj.config = std::move(*state);
            return true;
        }
    }

    std::optional<robot::RobotDescription>
    RobotReader::queryDescription(
            const std::string& name,
            const armem::Time& timestamp,
            bool warnings
            )
    {

        const auto sanitizedTimestamp = timestamp.isValid() ? timestamp : Clock::Now();

        // Query all entities from provider.
        armem::client::query::Builder qb;

        // clang-format off
        qb
        .coreSegments().withName(constants::descriptionCoreSegment)
        .providerSegments().withName(name)
        .entities().all()
        .snapshots().beforeOrAtTime(sanitizedTimestamp);
        // clang-format on

        ARMARX_DEBUG << "Lookup query in reader";

        if (not memoryReader)
        {
            if (warnings)
            {
                ARMARX_WARNING << "Memory reader is null. Did you forget to call "
                                  "RobotReader::connect() in onConnectComponent()?";
            }
            return std::nullopt;
        }

        try
        {
            const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

            ARMARX_DEBUG << "Lookup result in reader: " << qResult;

            if (not qResult.success) /* c++20 [[unlikely]] */
            {
                return std::nullopt;
            }

            return getRobotDescription(qResult.memory, name);
        }
        catch (...)
        {
            if (warnings)
            {
                ARMARX_WARNING << "query description failure" << GetHandledExceptionString();
            }
        }

        return std::nullopt;
    }

    std::optional<robot::RobotState>
    RobotReader::queryState(const robot::RobotDescription& description,
                            const armem::Time& timestamp)
    {
        const auto jointMap = queryJointState(description, timestamp);
        if (not jointMap)
        {
            ARMARX_WARNING << "Failed to query joint state for robot '" << description.name << "'.";
            return std::nullopt;
        }

        const auto globalPose = queryGlobalPose(description, timestamp);
        if (not globalPose)
        {
            ARMARX_WARNING << "Failed to query global pose for robot " << description.name;
            return std::nullopt;
        }

        return robot::RobotState{
            .timestamp = timestamp, .globalPose = *globalPose, .jointMap = *jointMap};
    }

    std::optional<::armarx::armem::robot_state::Transform>
    RobotReader::queryOdometryPose(const robot::RobotDescription& description,
                                   const armem::Time& timestamp) const
    {

        common::robot_state::localization::TransformQuery query{
            .header = {.parentFrame = OdometryFrame,
                       .frame = constants::robotRootNodeName,
                       .agent = description.name,
                       .timestamp = timestamp}};

        try
        {
            const auto result = transformReader.lookupTransform(query);
            if (not result)
            {
                return std::nullopt;
            }
            return result.transform;
        }
        catch (...)
        {
            ARMARX_WARNING << GetHandledExceptionString();
            return std::nullopt;
        }
    }


    std::optional<robot::RobotState::JointMap>
    RobotReader::queryJointState(const robot::RobotDescription& description,
                                 const armem::Time& timestamp) const // Why timestamp?!?!
    {
        // TODO(fabian.reister): how to deal with multiple providers?

        // Query all entities from provider.
        armem::client::query::Builder qb;

        ARMARX_DEBUG << "Querying robot description for robot: " << description;

        // clang-format off
        qb
        .coreSegments().withName(constants::proprioceptionCoreSegment)
        .providerSegments().withName(description.name) // agent
        .entities().all() // TODO
        .snapshots().beforeOrAtTime(timestamp);
        // clang-format on

        try
        {
            const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

            ARMARX_DEBUG << "Lookup result in reader: " << qResult;

            if (not qResult.success) /* c++20 [[unlikely]] */
            {
                ARMARX_WARNING << qResult.errorMessage;
                return std::nullopt;
            }

            return getRobotJointState(qResult.memory, description.name);
        }
        catch (...)
        {
            ARMARX_WARNING << deactivateSpam(1) << "Failed to query joint state. Reason: "
                           << GetHandledExceptionString();
            return std::nullopt;
        }
    }

    RobotReader::JointTrajectory
    RobotReader::queryJointStates(const robot::RobotDescription& description,
                                  const armem::Time& begin,
                                  const armem::Time& end) const
    {
        armem::client::query::Builder qb;

        ARMARX_DEBUG << "Querying robot joint states for robot: `" << description
                     << "` on time interval [" << begin << "," << end << "]";

        // clang-format off
        qb
        .coreSegments().withName(constants::proprioceptionCoreSegment)
        .providerSegments().withName(description.name) // agent
        .entities().all() // TODO
        .snapshots().timeRange(begin, end);
        // clang-format on

        const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success) /* c++20 [[unlikely]] */
        {
            ARMARX_WARNING << qResult.errorMessage;
            return {};
        }

        return getRobotJointStates(qResult.memory, description.name);
    }


    std::optional<robot::PlatformState>
    RobotReader::queryPlatformState(const robot::RobotDescription& description,
                                    const armem::Time& timestamp) const
    {
        // TODO(fabian.reister): how to deal with multiple providers?

        // Query all entities from provider.
        armem::client::query::Builder qb;

        ARMARX_DEBUG << "Querying robot description for robot: " << description;

        // clang-format off
        qb
        .coreSegments().withName(constants::proprioceptionCoreSegment)
        .providerSegments().withName(description.name) // agent
        .entities().all() // TODO
        .snapshots().beforeOrAtTime(timestamp);
        // clang-format on

        const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success) /* c++20 [[unlikely]] */
        {
            ARMARX_WARNING << qResult.errorMessage;
            return std::nullopt;
        }

        return getRobotPlatformState(qResult.memory, description.name);
    }

    std::optional<robot::RobotState::Pose>
    RobotReader::queryGlobalPose(const robot::RobotDescription& description,
                                 const armem::Time& timestamp) const
    {
        try
        {
            const auto result = transformReader.getGlobalPose(description.name, constants::robotRootNodeName, timestamp);
            if (not result)
            {
                return std::nullopt;
            }

            return result.transform.transform;
        }
        catch (...)
        {
            ARMARX_WARNING << deactivateSpam(1) << "Failed to query global pose. Reason: "
                           << GetHandledExceptionString();
            return std::nullopt;
        }
    }

    std::optional<robot::RobotState>
    RobotReader::getRobotState(const armarx::armem::wm::Memory& memory,
                               const std::string& name) const
    {
        // clang-format off
        const armem::wm::ProviderSegment& providerSegment = memory
                .getCoreSegment(constants::proprioceptionCoreSegment)
                .getProviderSegment(name);

        // TODO entitiesToRobotState()

        if (providerSegment.empty())
        {
            ARMARX_WARNING << "No entity found";
            return std::nullopt;
        }

        // clang-format on

        const armem::wm::EntityInstance* instance = nullptr;
        providerSegment.forEachInstance(
            [&instance](const wm::EntityInstance& i)
            {
                instance = &i;
                return false; // break
            });
        if (!instance)
        {
            ARMARX_WARNING << "No entity snapshots found";
            return std::nullopt;
        }

        // Here, we access the RobotUnit streaming data stored in the proprioception segment.
        return robot::convertRobotState(*instance);
    }

    // FIXME remove this, use armem/util/util.h
    template <typename AronClass>
    std::optional<AronClass>
    tryCast(const wm::EntityInstance& item)
    {
        static_assert(std::is_base_of<armarx::aron::codegenerator::cpp::AronGeneratedClass,
                                      AronClass>::value);

        try
        {
            return AronClass::FromAron(item.data());
        }
        catch (const armarx::aron::error::AronException&)
        {
            return std::nullopt;
        }
    }

    std::optional<robot::RobotState::JointMap>
    RobotReader::getRobotJointState(const armarx::armem::wm::Memory& memory,
                                    const std::string& name) const
    {
        // clang-format off
        const armem::wm::CoreSegment& coreSegment = memory
                .getCoreSegment(constants::proprioceptionCoreSegment);
        // clang-format on

        std::optional<robot::RobotState::JointMap> jointMap;

        coreSegment.forEachEntity(
            [&jointMap](const wm::Entity& entity)
            {
                const auto& entityInstance = entity.getLatestSnapshot().getInstance(0);

                const auto proprioception =
                    tryCast<::armarx::armem::arondto::Proprioception>(entityInstance);
                ARMARX_CHECK(proprioception.has_value());

                const armarx::armem::prop::arondto::Joints& joints = proprioception->joints;

                // const auto jointState = tryCast<::armarx::armem::arondto::JointState>(entityInstance);
                // if (not jointState)
                // {
                //     ARMARX_WARNING << "Could not convert entity instance to 'JointState'";
                //     return;
                // }

                jointMap = joints.position;

                // jointMap.emplace(jointState->name, jointState->position);
            });

        return jointMap;
    }

    RobotReader::JointTrajectory
    RobotReader::getRobotJointStates(const armarx::armem::wm::Memory& memory,
                                     const std::string& name) const
    {

        RobotReader::JointTrajectory jointTrajectory;

        // clang-format off
        const armem::wm::CoreSegment& coreSegment = memory
                .getCoreSegment(constants::proprioceptionCoreSegment);
        // clang-format on

        coreSegment.forEachEntity(
            [&jointTrajectory](const wm::Entity& entity)
            {
                entity.forEachSnapshot(
                    [&](const auto& snapshot)
                    {
                        if (not snapshot.hasInstance(0))
                        {
                            return;
                        }

                        const auto& entityInstance = snapshot.getInstance(0);

                        const auto proprioception =
                            tryCast<::armarx::armem::arondto::Proprioception>(entityInstance);
                        ARMARX_CHECK(proprioception.has_value());

                        const armarx::armem::prop::arondto::Joints& joints = proprioception->joints;

                        jointTrajectory.emplace(entityInstance.id().timestamp, joints.position);
                    });
            });

        ARMARX_INFO << "Joint trajectory with " << jointTrajectory.size() << " elements";

        return jointTrajectory;
    }


    // force torque for left and right
    std::optional<std::map<RobotReader::Hand, robot::ForceTorque>>
    RobotReader::queryForceTorque(const robot::RobotDescription& description,
                                  const armem::Time& timestamp) const
    {

        // Query all entities from provider.
        armem::client::query::Builder qb;

        ARMARX_DEBUG << "Querying force torques description for robot: " << description;

        // clang-format off
        qb
        .coreSegments().withName(constants::proprioceptionCoreSegment)
        .providerSegments().withName(description.name) // agent
        .entities().all() // TODO
        .snapshots().beforeOrAtTime(timestamp);
        // clang-format on

        const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success) /* c++20 [[unlikely]] */
        {
            ARMARX_WARNING << qResult.errorMessage;
            return std::nullopt;
        }

        return getForceTorque(qResult.memory, description.name);
    }

    // force torque for left and right
    std::optional<std::map<RobotReader::Hand, std::map<armem::Time, robot::ForceTorque>>>
    RobotReader::queryForceTorques(const robot::RobotDescription& description,
                                   const armem::Time& start,
                                   const armem::Time& end) const
    {

        // Query all entities from provider.
        armem::client::query::Builder qb;

        ARMARX_DEBUG << "Querying force torques description for robot: " << description;

        // clang-format off
        qb
        .coreSegments().withName(constants::proprioceptionCoreSegment)
        .providerSegments().withName(description.name) // agent
        .entities().all() // TODO
        .snapshots().timeRange(start, end);
        // clang-format on

        const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success) /* c++20 [[unlikely]] */
        {
            ARMARX_WARNING << qResult.errorMessage;
            return std::nullopt;
        }

        return getForceTorques(qResult.memory, description.name);
    }


    std::optional<robot::PlatformState>
    RobotReader::getRobotPlatformState(const armarx::armem::wm::Memory& memory,
                                       const std::string& name) const
    {
        std::optional<robot::PlatformState> platformState;

        // clang-format off
        const armem::wm::CoreSegment& coreSegment = memory
                .getCoreSegment(constants::proprioceptionCoreSegment);
        // clang-format on

        coreSegment.forEachEntity(
            [&platformState](const wm::Entity& entity)
            {
                const auto& entityInstance = entity.getLatestSnapshot().getInstance(0);

                const auto proprioception =
                    tryCast<::armarx::armem::arondto::Proprioception>(entityInstance);
                ARMARX_CHECK(proprioception.has_value());

                platformState = robot::PlatformState(); // initialize optional
                fromAron(proprioception->platform, platformState.value());
            });

        return platformState;
    }

    inline RobotReader::Hand
    fromHandName(const std::string& name)
    {
        if (name == "Left")
        {
            return RobotReader::Hand::Left;
        }

        if (name == "Right")
        {
            return RobotReader::Hand::Right;
        }

        throw LocalException("Unknown hand name `" + name + "`!");
    }

    std::map<RobotReader::Hand, robot::ForceTorque>
    RobotReader::getForceTorque(const armarx::armem::wm::Memory& memory,
                                const std::string& name) const
    {
        std::map<RobotReader::Hand, robot::ForceTorque> forceTorques;

        // clang-format off
        const armem::wm::CoreSegment& coreSegment = memory
                .getCoreSegment(constants::proprioceptionCoreSegment);
        // clang-format on

        coreSegment.forEachEntity(
            [&forceTorques](const wm::Entity& entity)
            {
                const auto& entityInstance = entity.getLatestSnapshot().getInstance(0);

                const auto proprioception =
                    tryCast<::armarx::armem::arondto::Proprioception>(entityInstance);
                ARMARX_CHECK(proprioception.has_value());


                for (const auto& [handName, dtoFt] : proprioception->forceTorque)
                {
                    robot::ForceTorque forceTorque;
                    fromAron(dtoFt, forceTorque);

                    const auto hand = fromHandName(handName);
                    forceTorques.emplace(hand, forceTorque);
                }
            });

        return forceTorques;
    }

    std::map<RobotReader::Hand, std::map<armem::Time, robot::ForceTorque>>
    RobotReader::getForceTorques(const armarx::armem::wm::Memory& memory,
                                 const std::string& name) const
    {
        std::map<RobotReader::Hand, std::map<armem::Time, robot::ForceTorque>> forceTorques;

        // clang-format off
        const armem::wm::CoreSegment& coreSegment = memory
                .getCoreSegment(constants::proprioceptionCoreSegment);
        // clang-format on

        coreSegment.forEachInstance(
            [&forceTorques](const wm::EntityInstance& entityInstance)
            {
                const auto proprioception =
                    tryCast<::armarx::armem::arondto::Proprioception>(entityInstance);
                ARMARX_CHECK(proprioception.has_value());


                for (const auto& [handName, dtoFt] : proprioception->forceTorque)
                {
                    robot::ForceTorque forceTorque;
                    fromAron(dtoFt, forceTorque);

                    const auto hand = fromHandName(handName);
                    forceTorques[hand].emplace(entityInstance.id().timestamp, forceTorque);
                }
            });

        return forceTorques;
    }


    std::optional<robot::RobotDescription>
    RobotReader::getRobotDescription(const armarx::armem::wm::Memory& memory,
                                     const std::string& name) const
    {
        // clang-format off
        const armem::wm::ProviderSegment& providerSegment = memory
                .getCoreSegment(constants::descriptionCoreSegment)
                .getProviderSegment(name);
        // clang-format on

        const armem::wm::EntityInstance* instance = nullptr;
        providerSegment.forEachInstance([&instance](const wm::EntityInstance& i)
                                        { instance = &i; });
        if (instance == nullptr)
        {
            ARMARX_WARNING << "No entity snapshots found in provider segment `" << name << "`";
            return std::nullopt;
        }

        return robot::convertRobotDescription(*instance);
    }

} // namespace armarx::armem::robot_state
