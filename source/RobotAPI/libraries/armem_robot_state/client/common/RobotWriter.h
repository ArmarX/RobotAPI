/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <optional>

#include "RobotAPI/libraries/armem/client/Writer.h"
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem_robot/client/interfaces.h>
#include <RobotAPI/libraries/armem_robot/types.h>
#include <RobotAPI/libraries/armem_robot_state/client/localization/TransformReader.h>


namespace armarx::armem::robot_state
{
    /**
     * @brief The RobotReader class.
     *
     * The purpose of this class is to synchronize the armem data structure armem::Robot
     * with the memory.
     */
    class RobotWriter : virtual public robot::WriterInterface
    {
    public:
        RobotWriter(armem::client::MemoryNameSystem& memoryNameSystem);
        ~RobotWriter() override;

        void connect();
        void registerPropertyDefinitions(::armarx::PropertyDefinitionsPtr& def);


        [[nodiscard]] bool
        storeDescription(const robot::RobotDescription& description,
                         const armem::Time& timestamp = armem::Time::Invalid()) override;


        [[nodiscard]] bool storeState(const robot::RobotState& state,
                                      const std::string& robotTypeName,
                                      const std::string& robotName,
                                      const std::string& robotRootNodeName) override;


        [[nodiscard]] bool storeLocalization(const Eigen::Matrix4f& globalRootPose,
                                             const std::string& robotName,
                                             const std::string& robotRootNodeName,
                                             const armem::Time& timestamp);

        [[nodiscard]] bool storeProprioception(const std::map<std::string, float>& jointMap,
                                               const std::string& robotTypeName,
                                               const std::string& robotName,
                                               const armem::Time& timestamp);


        struct Properties
        {

        } properties;

        const std::string propertyPrefix = "mem.robot_state.";

        armem::client::MemoryNameSystem& memoryNameSystem;

        std::mutex memoryWriterMutex;
        armem::client::Writer memoryWriter;
    };

} // namespace armarx::armem::robot_state
