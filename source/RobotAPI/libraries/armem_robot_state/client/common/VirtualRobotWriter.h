/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/XML/RobotIO.h>

#include "RobotAPI/libraries/armem/core/forward_declarations.h"

#include "RobotWriter.h"


namespace armarx::armem::robot_state
{
    /**
     * @brief The VirtualRobotReader class.
     *
     * The aim of this class is to obtain a virtual robot instance and synchronize it
     * with the data (joint positions, global pose, ...) stored in the working memory.
     *
     * This is only a lightweight wrapper of @see RobotReader for Simox's VirtualRobot class.
     */
    class VirtualRobotWriter : virtual public RobotWriter
    {
    public:
        VirtualRobotWriter(armem::client::MemoryNameSystem& memoryNameSystem);
        ~VirtualRobotWriter() override;

        void connect();
        void registerPropertyDefinitions(::armarx::PropertyDefinitionsPtr& def);

        [[nodiscard]] bool storeDescription(const VirtualRobot::Robot& robot,
                                            const armem::Time& timestamp = armem::Time::Invalid());


        [[nodiscard]] bool storeState(const VirtualRobot::Robot& robot,
                                      const armem::Time& timestamp);
    };

} // namespace armarx::armem::robot_state
