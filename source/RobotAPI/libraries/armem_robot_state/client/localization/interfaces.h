/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Geometry>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <RobotAPI/libraries/armem_robot_state/common/localization/types.h>
#include <RobotAPI/libraries/armem_robot_state/types.h>

namespace armarx::armem::client::robot_state::localization
{

    using armarx::armem::common::robot_state::localization::TransformQuery;
    using armarx::armem::common::robot_state::localization::TransformResult;

    class TransformInterface
    {
    public:
        virtual ~TransformInterface() = default;

        virtual void registerPropertyDefinitions(PropertyDefinitionsPtr& def) = 0;
        virtual void connect()                                                = 0;
    };

    class TransformReaderInterface : virtual public TransformInterface
    {
    public:
        virtual ~TransformReaderInterface() = default;

        virtual TransformResult getGlobalPose(const std::string& agentName,
                                              const std::string& robotRootFrame,
                                              const armem::Time& timestamp) const = 0;

        virtual TransformResult lookupTransform(const TransformQuery& query) const = 0;
        // waitForTransform()
    };

    class TransformWriterInterface : virtual public TransformInterface
    {
    public:
        ~TransformWriterInterface() override = default;

        virtual bool commitTransform(const ::armarx::armem::robot_state::Transform& transform) = 0;
    };

} // namespace armarx::armem::client::robot_state::localization