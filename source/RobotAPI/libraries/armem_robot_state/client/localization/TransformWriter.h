/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Writer.h>

#include "interfaces.h"


namespace armarx::armem::client::robot_state::localization
{

    /**
    * @defgroup Component-ExampleClient ExampleClient
    * @ingroup RobotAPI-Components
    * A description of the component ExampleClient.
    *
    * @class ExampleClient
    * @ingroup Component-ExampleClient
    * @brief Brief description of class ExampleClient.
    *
    * Detailed description of class ExampleClient.
    */
    class TransformWriter :
        virtual public TransformWriterInterface
    {
    public:

        TransformWriter(armem::client::MemoryNameSystem& memoryNameSystem);
        ~TransformWriter() override;

        // TransformWriterInterface
        /// to be called in Component::onConnectComponent
        void connect() override;

        /// to be called in Component::addPropertyDefinitions
        void registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def) override;

        bool commitTransform(const ::armarx::armem::robot_state::Transform& transform) override;


    private:

        armem::client::MemoryNameSystem& memoryNameSystem;
        armem::client::Writer memoryWriter;
        std::mutex memoryWriterMutex;

        // Properties
        struct Properties
        {
            MemoryID coreSegmentID { "RobotState", "Localization" };
        } properties;

        const std::string propertyPrefix = "mem.robot_state.";
    };

}  // namespace armarx::armem::client::robot_state::localization
