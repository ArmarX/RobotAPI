/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TransformWriter.h"

#include <optional>
#include <algorithm>
#include <iterator>
#include <numeric>

#include <Eigen/Geometry>

#include <IceUtil/Time.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/CycleUtil.h>

#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/client/query/query_fns.h>
#include <RobotAPI/libraries/armem/core/wm/ice_conversions.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/MemoryRemoteGui.h>
#include <RobotAPI/libraries/aron/core/type/variant/Factory.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <RobotAPI/libraries/armem_robot_state/aron/Transform.aron.generated.h>
#include <RobotAPI/libraries/armem_robot_state/aron_conversions.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/error.h>



namespace armarx::armem::client::robot_state::localization
{

    TransformWriter::TransformWriter(armem::client::MemoryNameSystem& memoryNameSystem) :
        memoryNameSystem(memoryNameSystem) {}

    TransformWriter::~TransformWriter() = default;

    void TransformWriter::registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def)
    {
        ARMARX_DEBUG << "TransformWriter: registerPropertyDefinitions";

        def->optional(properties.coreSegmentID.memoryName, propertyPrefix + "MemoryName");
        def->optional(properties.coreSegmentID.coreSegmentName,
                      propertyPrefix + "LocalizationSegmentName",
                      "Name of the localization memory core segment to use.");
    }

    void TransformWriter::connect()
    {
        // Wait for the memory to become available and add it as dependency.
        ARMARX_IMPORTANT << "TransformWriter: Waiting for memory '" << properties.coreSegmentID.memoryName
                         << "' ...";
        try
        {
            memoryWriter = memoryNameSystem.useWriter(properties.coreSegmentID);
            ARMARX_IMPORTANT << "TransformWriter: Connected to memory for '" << properties.coreSegmentID << "'";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }
    }

    bool TransformWriter::commitTransform(const ::armarx::armem::robot_state::Transform& transform)
    {
        std::lock_guard g{memoryWriterMutex};

        const MemoryID providerId = properties.coreSegmentID.withProviderSegmentName(transform.header.agent);
        // const auto& timestamp = transform.header.timestamp;
        const MemoryID entityID = providerId.withEntityName(
                                      transform.header.parentFrame + "," + transform.header.frame);
        const Time timestamp = Time::Now();  // FIXME remove

        armem::EntityUpdate update;
        update.entityID = entityID;
        update.timeCreated = timestamp;

        arondto::Transform aronTransform;
        toAron(aronTransform, transform);
        update.instancesData = {aronTransform.toAron()};

        ARMARX_DEBUG << "Committing " << update << " at time " << transform.header.timestamp;
        armem::EntityUpdateResult updateResult = memoryWriter.commit(update);

        ARMARX_DEBUG << updateResult;

        if (not updateResult.success)
        {
            ARMARX_ERROR << updateResult.errorMessage;
        }

        return updateResult.success;
    }

}  // namespace armarx::armem::client::robot_state::localization
