/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Geometry>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>


#include <RobotAPI/libraries/armem_robot_state/types.h>

namespace armarx::armem::common::robot_state::localization
{
    struct TransformResult
    {
        ::armarx::armem::robot_state::Transform transform;

        enum class Status
        {
            Success,
            Error,
            ErrorLookupIntoFuture,
            ErrorFrameNotAvailable
        } status;

        explicit operator bool() const
        {
            return status == Status::Success;
        }

        std::string errorMessage = "";
    };

    struct TransformChainResult
    {
        ::armarx::armem::robot_state::TransformHeader header;
        std::vector<Eigen::Affine3f> transforms;

        enum class Status
        {
            Success,
            Error,
            ErrorLookupIntoFuture,
            ErrorFrameNotAvailable
        } status;

        explicit operator bool() const
        {
            return status == Status::Success;
        }

        std::string errorMessage = "";
    };

    struct TransformQuery
    {
        ::armarx::armem::robot_state::TransformHeader header;

        // bool exact;
    };

}  // namespace armarx::armem::common::robot_state::localization
