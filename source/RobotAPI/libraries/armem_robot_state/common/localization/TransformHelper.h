/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <RobotAPI/libraries/armem/core/forward_declarations.h>
#include <RobotAPI/libraries/armem_robot_state/common/localization/types.h>


namespace armarx::armem::common::robot_state::localization
{
    using armarx::armem::common::robot_state::localization::TransformQuery;
    using armarx::armem::common::robot_state::localization::TransformResult;



    class TransformHelper
    {
    public:

        static
        TransformResult
        lookupTransform(
            const armem::wm::CoreSegment& localizationCoreSegment,
            const TransformQuery& query);

        static
        TransformResult
        lookupTransform(
            const armem::server::wm::CoreSegment& localizationCoreSegment,
            const TransformQuery& query);


        static
        TransformChainResult
        lookupTransformChain(
            const armem::wm::CoreSegment& localizationCoreSegment,
            const TransformQuery& query);

        static
        TransformChainResult
        lookupTransformChain(
            const armem::server::wm::CoreSegment& localizationCoreSegment,
            const TransformQuery& query);


    private:

        template <class ...Args>
        static
        TransformResult
        _lookupTransform(
            const armem::base::CoreSegmentBase<Args...>& localizationCoreSegment,
            const TransformQuery& query);


        template <class ...Args>
        static
        TransformChainResult
        _lookupTransformChain(
            const armem::base::CoreSegmentBase<Args...>& localizationCoreSegment,
            const TransformQuery& query);


        template <class ...Args>
        static
        std::vector<std::string>
        _buildTransformChain(
            const armem::base::CoreSegmentBase<Args...>& localizationCoreSegment,
            const TransformQuery& query);


        template <class ...Args>
        static
        std::vector<Eigen::Affine3f>
        _obtainTransforms(
            const armem::base::CoreSegmentBase<Args...>& localizationCoreSegment,
            const std::vector<std::string>& tfChain,
            const std::string& agent,
            const armem::Time& timestamp);


        template <class ...Args>
        static
        Eigen::Affine3f
        _obtainTransform(
            const std::string& entityName,
            const armem::base::ProviderSegmentBase<Args...>& agentProviderSegment,
            const armem::Time& timestamp);

        template <class ...Args>
        static
        std::optional<armarx::core::time::DateTime>
        _obtainTimestamp(const armem::base::CoreSegmentBase<Args...>& localizationCoreSegment, const armem::Time& timestamp);

        static
        Eigen::Affine3f
        _interpolateTransform(
            const std::vector<::armarx::armem::robot_state::Transform>& queue,
            armem::Time timestamp);

        static
        ::armarx::armem::robot_state::Transform
        _convertEntityToTransform(
            const armem::wm::EntityInstance& item);

    };
} // namespace armarx::armem::common::robot_state::localization
