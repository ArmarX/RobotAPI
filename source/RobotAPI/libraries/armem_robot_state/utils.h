#pragma once

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem_robot/types.h>

namespace armarx::armem::robot_state
{
    armarx::armem::MemoryID makeMemoryID(const robot::RobotDescription& desc);
}
