/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD / STL
#include <map>
#include <optional>
#include <string>

// ArmarX
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>

// RobotAPI
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/Prediction.h>
#include <RobotAPI/libraries/armem/server/segment/SpecializedCoreSegment.h>
#include <RobotAPI/libraries/armem/server/segment/SpecializedProviderSegment.h>
#include <RobotAPI/libraries/armem_robot_state/server/forward_declarations.h>


namespace armarx
{
    class DebugObserverHelper;
}
namespace armarx::armem::server::robot_state::proprioception
{
    class Segment : public segment::SpecializedCoreSegment
    {
        using Base = segment::SpecializedCoreSegment;

    public:

        Segment(server::MemoryToIceAdapter& iceMemory);
        virtual ~Segment() override;

        void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "") override;

        void init() override;

        void onConnect(RobotUnitInterfacePrx robotUnitPrx);


        RobotJointPositionMap getRobotJointPositions(
            const armem::Time& timestamp, DebugObserverHelper* debugObserver = nullptr) const;
        RobotJointPositionMap getRobotJointPositionsLocking(
            const armem::Time& timestamp, DebugObserverHelper* debugObserver = nullptr) const;

        const armem::MemoryID& getRobotUnitProviderID() const;

        armem::PredictionResult predictLinear(const armem::PredictionRequest& request) const;


    private:

        static
        std::map<std::string, float>
        readJointPositions(const wm::EntityInstanceData& data);


    private:

        RobotUnitInterfacePrx robotUnit;
        armem::MemoryID robotUnitProviderID;

        struct Properties
        {
            double predictionTimeWindow = 2;
        };
        Properties properties;

        // Debug Observer prefix
        const std::string dp = "Proprioception::getRobotJointPositions() | ";

    };

}  // namespace armarx::armem::server::robot_state::proprioception
