#pragma once

#include <queue>
#include <map>
#include <memory>
#include <optional>
#include <string>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/libraries/DebugObserverHelper/DebugObserverHelper.h>

#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>

#include "RobotUnitData.h"
#include "converters/ConverterRegistry.h"
#include "converters/ConverterInterface.h"


namespace armarx::plugins
{
    class RobotUnitComponentPlugin;
    class DebugObserverComponentPlugin;
}
namespace armarx
{
    using RobotUnitDataStreamingReceiverPtr = std::shared_ptr<class RobotUnitDataStreamingReceiver>;
}

namespace armarx::armem::server::robot_state::proprioception
{
    class RobotUnitReader : public armarx::Logging
    {
    public:

        RobotUnitReader();


        void connect(
            armarx::plugins::RobotUnitComponentPlugin& robotUnitPlugin,
            armarx::plugins::DebugObserverComponentPlugin& debugObserverPlugin,
            const std::string& robotTypeName);



        /// Reads data from `handler` and fills `dataQueue`.
        void run(float pollFrequency,
                 std::queue<RobotUnitData>& dataQueue,
                 std::mutex& dataMutex);

        std::optional<RobotUnitData> fetchAndConvertLatestRobotUnitData();


    private:

        /// Fetch the latest timestep and clear the robot unit buffer.
        std::optional<RobotUnitDataStreaming::TimeStep> fetchLatestData();


    public:

        struct Properties
        {
            std::string sensorPrefix = "sens.*";
        };
        Properties properties;

        std::optional<DebugObserverHelper> debugObserver;


        RobotUnitDataStreaming::Config config;
        RobotUnitDataStreamingReceiverPtr receiver;
        RobotUnitDataStreaming::DataStreamingDescription description;

        ConverterRegistry converterRegistry;
        ConverterInterface* converter = nullptr;

        armarx::SimpleRunningTask<>::pointer_type task = nullptr;

    };

}

