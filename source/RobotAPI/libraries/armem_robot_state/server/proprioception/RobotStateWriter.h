/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotSensorMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <optional>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/libraries/DebugObserverHelper/DebugObserverHelper.h>

#include <RobotAPI/libraries/armem/core/Commit.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem_robot_state/types.h>

#include "RobotUnitData.h"


namespace armarx::plugins
{
    class DebugObserverComponentPlugin;
}
namespace armarx::armem::server
{
    class MemoryToIceAdapter;
}
namespace armarx::armem::server::robot_state::localization
{
    class Segment;
}
namespace armarx::armem::server::robot_state::proprioception
{
    class RobotStateWriter : public armarx::Logging
    {
    public:

        void connect(armarx::plugins::DebugObserverComponentPlugin& debugObserver);

        /// Reads data from `dataQueue` and commits to the memory.
        void run(float pollFrequency,
                 std::queue<RobotUnitData>& dataQueue, std::mutex& dataMutex,
                 MemoryToIceAdapter& memory,
                 localization::Segment& localizationSegment
                );


        struct Update
        {
            armem::Commit proprioception;
            std::vector<armem::robot_state::Transform> localization;
        };
        Update buildUpdate(std::queue<RobotUnitData>& dataQueue);


    private:

        armem::robot_state::Transform
        getTransform(
            const aron::data::DictPtr& platformData,
            const Time& timestamp) const;


    public:

        struct Properties
        {
            armem::MemoryID robotUnitProviderID;
        };
        Properties properties;

        std::optional<DebugObserverHelper> debugObserver;


        armarx::SimpleRunningTask<>::pointer_type task = nullptr;


    private:

        bool noOdometryDataLogged = false;

    };

}
