#include "ConverterRegistry.h"

#include "Armar6Converter.h"

#include <SimoxUtility/algorithm/get_map_keys_values.h>


namespace armarx::armem::server::robot_state::proprioception
{

    ConverterRegistry::ConverterRegistry()
    {
        add<Armar6Converter>("Armar6");
        add<Armar6Converter>("ArmarDE");
    }


    ConverterInterface*
    ConverterRegistry::get(const std::string& key) const
    {
        auto it = converters.find(key);
        return it != converters.end() ? it->second.get() : nullptr;
    }


    std::vector<std::string> ConverterRegistry::getKeys() const
    {
        return simox::alg::get_keys(converters);
    }

}
