#pragma once

#include <memory>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>

namespace armarx::RobotUnitDataStreaming
{
    struct TimeStep;
    struct DataStreamingDescription;
    struct DataEntry;
}
namespace armarx::armem::arondto
{
    class Proprioception;
}

namespace armarx::armem::server::robot_state::proprioception
{
    class ConverterInterface
    {
    public:

        virtual ~ConverterInterface();

        virtual
        aron::data::DictPtr convert(
            const RobotUnitDataStreaming::TimeStep& data,
            const RobotUnitDataStreaming::DataStreamingDescription& description) = 0;

    };
}

