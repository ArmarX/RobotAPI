#pragma once

#include <map>
#include <string>

#include <Eigen/Core>

#include <RobotAPI/libraries/armem_robot_state/aron/Proprioception.aron.generated.h>

#include "ConverterInterface.h"



namespace armarx::armem::server::robot_state::proprioception
{
    struct ConverterValue;
    class ConverterTools;


    class Armar6Converter : public ConverterInterface
    {
    public:

        Armar6Converter();
        virtual ~Armar6Converter() override;


        aron::data::DictPtr
        convert(
            const RobotUnitDataStreaming::TimeStep& data,
            const RobotUnitDataStreaming::DataStreamingDescription& description) override;


    protected:

        void process(arondto::Proprioception& dto, const std::string& entryName, const ConverterValue& value);



    private:

        void processPlatformEntry(
            prop::arondto::Platform& dto,
            const std::string& fieldName,
            const ConverterValue& value);

        void processForceTorqueEntry(
            std::map<std::string, prop::arondto::ForceTorque>& fts,
            const std::vector<std::string>& split,
            const ConverterValue& value);

        void processForceTorqueEntry(
            prop::arondto::ForceTorque& ft,
            const std::vector<std::string>& split,
            const ConverterValue& value);

        bool processJointEntry(
            prop::arondto::Joints& dto,
            const std::vector<std::string>& split,
            const ConverterValue& value);


    private:

        std::unique_ptr<ConverterTools> tools;

    };
}

