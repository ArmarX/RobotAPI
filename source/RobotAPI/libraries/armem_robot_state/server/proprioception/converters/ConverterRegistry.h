#pragma once

#include <map>
#include <memory>
#include <string>
#include <vector>

#include "ConverterInterface.h"


namespace armarx::armem::server::robot_state::proprioception
{
    class ConverterRegistry
    {
    public:

        ConverterRegistry();


        template <class ConverterT, class ...Args>
        void add(const std::string& key, Args... args)
        {
            converters[key].reset(new ConverterT(args...));
        }


        ConverterInterface* get(const std::string& key) const;
        std::vector<std::string> getKeys() const;


    private:

        std::map<std::string, std::unique_ptr<ConverterInterface>> converters;

    };
}

