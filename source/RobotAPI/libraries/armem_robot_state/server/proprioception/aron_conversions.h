#pragma once

#include <memory>

#include <SimoxUtility/meta/enum/EnumNames.hpp>

#include <RobotAPI/libraries/aron/core/data/variant/Variant.h>
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>


namespace armarx::RobotUnitDataStreaming
{
    aron::data::VariantPtr toAron(
        const TimeStep& timestep,
        const DataEntry& dataEntry);

    extern const simox::meta::EnumNames<DataEntryType> DataEntryNames;

}
