/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::RobotSensorMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotStateWriter.h"

// STL
#include <chrono>

// Simox
#include <SimoxUtility/math/convert/rpy_to_mat3f.h>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

#include "RobotAPI/libraries/armem_robot_state/client/common/constants.h"
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem/core/ice_conversions.h>
#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>
#include <RobotAPI/libraries/armem_robot_state/aron/Transform.aron.generated.h>
#include <RobotAPI/libraries/armem_robot_state/aron/Proprioception.aron.generated.h>
#include <RobotAPI/libraries/armem_robot_state/server/localization/Segment.h>


namespace armarx::armem::server::robot_state::proprioception
{

    void RobotStateWriter::connect(armarx::plugins::DebugObserverComponentPlugin& debugObserver)
    {
        // Thread-local copy of debug observer helper.
        this->debugObserver =
            DebugObserverHelper(Logging::tag.tagName, debugObserver.getDebugObserver(), true);
    }


    static float toDurationMs(
        std::chrono::high_resolution_clock::time_point start,
        std::chrono::high_resolution_clock::time_point end)
    {
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        return duration.count() / 1000.f;
    }


    void RobotStateWriter::run(
        float pollFrequency,
        std::queue<RobotUnitData>& dataQueue,
        std::mutex& dataMutex,
        MemoryToIceAdapter& memory,
        localization::Segment& localizationSegment)
    {
        CycleUtil cycle(static_cast<int>(1000.f / pollFrequency));
        while (task and not task->isStopped())
        {
            size_t queueSize = 0;
            std::queue<RobotUnitData> batch;
            {
                std::lock_guard lock{dataMutex};
                queueSize = dataQueue.size();
                if (!dataQueue.empty())
                {
                    std::swap(batch, dataQueue);
                }
            }
            if (debugObserver)
            {
                debugObserver->setDebugObserverDatafield("RobotStateWriter | Queue Size", queueSize);
            }
            if (not batch.empty())
            {
                auto start = std::chrono::high_resolution_clock::now();
                auto endBuildUpdate = start, endProprioception = start, endLocalization = start;

                Update update = buildUpdate(batch);
                endBuildUpdate = std::chrono::high_resolution_clock::now();

                // Commits lock the core segments.

                // Proprioception
                armem::CommitResult result = memory.commitLocking(update.proprioception);
                endProprioception = std::chrono::high_resolution_clock::now();

                // Localization
                for (const armem::robot_state::Transform& transform : update.localization)
                {
                    localizationSegment.doLocked([&localizationSegment, &transform]()
                    {
                        localizationSegment.commitTransform(transform);
                    });
                }
                endLocalization = std::chrono::high_resolution_clock::now();

                if (not result.allSuccess())
                {
                    ARMARX_WARNING << "Could not commit data to memory. Error message: " << result.allErrorMessages();
                }
                if (debugObserver)
                {
                    auto end = std::chrono::high_resolution_clock::now();

                    debugObserver->setDebugObserverDatafield("RobotStateWriter | t Commit (ms)", toDurationMs(start, end));
                    debugObserver->setDebugObserverDatafield("RobotStateWriter | t Commit 1. Build Update (ms)", toDurationMs(start, endBuildUpdate));
                    debugObserver->setDebugObserverDatafield("RobotStateWriter | t Commit 2. Proprioception (ms)", toDurationMs(endBuildUpdate, endProprioception));
                    debugObserver->setDebugObserverDatafield("RobotStateWriter | t Commit 3. Localization (ms)", toDurationMs(endProprioception, endLocalization));
                }
            }

            if (debugObserver)
            {
                debugObserver->sendDebugObserverBatch();
            }
            cycle.waitForCycleDuration();
        }
    }


    RobotStateWriter::Update RobotStateWriter::buildUpdate(std::queue<RobotUnitData>& dataQueue)
    {
        ARMARX_CHECK_GREATER(dataQueue.size(), 0);
        ARMARX_DEBUG << "RobotStateWriter: Commit batch of " << dataQueue.size() << " timesteps to memory...";

        // Send batch to memory
        Update update;

        while (dataQueue.size() > 0)
        {
            const RobotUnitData& data = dataQueue.front();

            {
                armem::EntityUpdate& up = update.proprioception.add();
                up.entityID = properties.robotUnitProviderID.withEntityName(properties.robotUnitProviderID.providerSegmentName);
                up.timeCreated = data.timestamp;
                up.instancesData = { data.proprioception };
            }

            // Extract odometry data.
            const std::string platformKey = "platform";
            if (data.proprioception->hasElement(platformKey))
            {
                ARMARX_DEBUG << "Found odometry data.";
                auto platformData = aron::data::Dict::DynamicCastAndCheck(data.proprioception->getElement(platformKey));
                update.localization.emplace_back(getTransform(platformData, data.timestamp));
            }
            else
            {
                ARMARX_INFO << "No odometry data! "
                            << "(No element '" << platformKey << "' in proprioception data.)"
                            << "\nIf you are using a mobile platform this should not have happened."
                            << "\nThis error is only logged once."
                            << "\nThese keys exist: " << data.proprioception->getAllKeys()
                            ;
                noOdometryDataLogged = true;
            }

            dataQueue.pop();
        }

        return update;
    }


    armem::robot_state::Transform
    RobotStateWriter::getTransform(
        const aron::data::DictPtr& platformData,
        const Time& timestamp) const
    {
        prop::arondto::Platform platform;
        platform.fromAron(platformData);

        const Eigen::Vector3f& relPose = platform.relativePosition;

        Eigen::Affine3f odometryPose = Eigen::Affine3f::Identity();
        odometryPose.translation() << relPose.x(), relPose.y(), 0;  // TODO set height
        odometryPose.linear() = simox::math::rpy_to_mat3f(0.f, 0.f, relPose.z());

        armem::robot_state::Transform transform;
        transform.header.parentFrame = armarx::OdometryFrame;
        transform.header.frame = armarx::armem::robot_state::constants::robotRootNodeName;
        transform.header.agent = properties.robotUnitProviderID.providerSegmentName;
        transform.header.timestamp = timestamp;
        transform.transform = odometryPose;

        return transform;
    }

}
