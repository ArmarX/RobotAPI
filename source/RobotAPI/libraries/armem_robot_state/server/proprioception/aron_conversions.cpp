#include "aron_conversions.h"

#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/RobotUnitDataStreamingReceiver/RobotUnitDataStreamingReceiver.h>

#include <ArmarXCore/core/exceptions/local/UnexpectedEnumValueException.h>


namespace armarx
{
    aron::data::VariantPtr RobotUnitDataStreaming::toAron(
        const TimeStep& timestep,
        const DataEntry& dataEntry)
    {
        switch (dataEntry.type)
        {
            case RobotUnitDataStreaming::NodeTypeFloat:
            {
                float value = RobotUnitDataStreamingReceiver::GetAs<Ice::Float>(timestep, dataEntry);
                return std::make_shared<aron::data::Float>(value);
            }
            case RobotUnitDataStreaming::NodeTypeBool:
            {
                bool value = RobotUnitDataStreamingReceiver::GetAs<bool>(timestep, dataEntry);
                return std::make_shared<aron::data::Bool>(value);
            }
            case RobotUnitDataStreaming::NodeTypeByte:
            {
                int value = RobotUnitDataStreamingReceiver::GetAs<Ice::Byte>(timestep, dataEntry);
                return std::make_shared<aron::data::Int>(value);
            }
            case RobotUnitDataStreaming::NodeTypeShort:
            {
                int value = RobotUnitDataStreamingReceiver::GetAs<Ice::Short>(timestep, dataEntry);
                return std::make_shared<aron::data::Int>(value);
            }
            case RobotUnitDataStreaming::NodeTypeInt:
            {
                int value = RobotUnitDataStreamingReceiver::GetAs<Ice::Int>(timestep, dataEntry);
                return std::make_shared<aron::data::Int>(value);
            }
            case RobotUnitDataStreaming::NodeTypeLong:
            {
                long value = RobotUnitDataStreamingReceiver::GetAs<Ice::Long>(timestep, dataEntry);
                return std::make_shared<aron::data::Long>(value);
            }
            case RobotUnitDataStreaming::NodeTypeDouble:
            {
                double value = RobotUnitDataStreamingReceiver::GetAs<Ice::Double>(timestep, dataEntry);
                return std::make_shared<aron::data::Double>(value);
            }
            default:
                ARMARX_UNEXPECTED_ENUM_VALUE(RobotUnitDataStreaming::NodeType, dataEntry.type);
        }
    }


    const simox::meta::EnumNames<RobotUnitDataStreaming::DataEntryType> RobotUnitDataStreaming::DataEntryNames =
    {
        { NodeTypeBool, "Bool" },
        { NodeTypeByte, "Byte" },
        { NodeTypeShort, "Short" },
        { NodeTypeInt, "Int" },
        { NodeTypeLong, "Long" },
        { NodeTypeFloat, "Float" },
        { NodeTypeDouble, "Double" },
    };

}
