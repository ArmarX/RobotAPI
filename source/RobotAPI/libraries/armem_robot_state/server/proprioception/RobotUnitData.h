#pragma once

#include <memory>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/armem/core/Time.h>

namespace armarx::armem::server::robot_state::proprioception
{

    struct RobotUnitData
    {
        Time timestamp;
        aron::data::DictPtr proprioception;
    };
}

