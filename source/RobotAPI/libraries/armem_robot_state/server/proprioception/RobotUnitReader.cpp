#include "RobotUnitReader.h"

#include <RobotAPI/libraries/armem_robot_state/server/proprioception/converters/Armar6Converter.h>
#include <RobotAPI/libraries/armem_robot_state/server/proprioception/aron_conversions.h>
#include <RobotAPI/libraries/armem_robot_state/aron/Proprioception.aron.generated.h>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/data/variant/primitive/Long.h>
#include <RobotAPI/libraries/aron/core/data/variant/primitive/String.h>

#include <RobotAPI/libraries/RobotUnitDataStreamingReceiver/RobotUnitDataStreamingReceiver.h>

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

#include <SimoxUtility/algorithm/string/string_tools.h>

#include <istream>
#include <filesystem>
#include <fstream>


namespace armarx::armem::server::robot_state::proprioception
{

    RobotUnitReader::RobotUnitReader()
    {
    }


    void RobotUnitReader::connect(
        armarx::plugins::RobotUnitComponentPlugin& robotUnitPlugin,
        armarx::plugins::DebugObserverComponentPlugin& debugObserverPlugin,
        const std::string& robotTypeName)
    {
        {
            converter = converterRegistry.get(robotTypeName);
            ARMARX_CHECK_NOT_NULL(converter)
                    << "No converter for robot type '" << robotTypeName << "' available. \n"
                    << "Known are: " << converterRegistry.getKeys();

            config.loggingNames.push_back(properties.sensorPrefix);
            receiver = robotUnitPlugin.startDataSatreming(config);
            description = receiver->getDataDescription();
        }
        {
            // Thread-local copy of debug observer helper.
            debugObserver =
                DebugObserverHelper(Logging::tag.tagName, debugObserverPlugin.getDebugObserver(), true);
        }
    }


    void RobotUnitReader::run(
        float pollFrequency,
        std::queue<RobotUnitData>& dataQueue,
        std::mutex& dataMutex)
    {
        CycleUtil cycle(static_cast<int>(1000.f / pollFrequency));
        while (task and not task->isStopped())
        {
            if (std::optional<RobotUnitData> commit = fetchAndConvertLatestRobotUnitData())
            {
                std::lock_guard g{dataMutex};
                dataQueue.push(std::move(commit.value()));
            }

            if (debugObserver)
            {
                debugObserver->sendDebugObserverBatch();
            }
            cycle.waitForCycleDuration();
        }
    }


    std::optional<RobotUnitData> RobotUnitReader::fetchAndConvertLatestRobotUnitData()
    {
        ARMARX_CHECK_NOT_NULL(converter);

        const std::optional<RobotUnitDataStreaming::TimeStep> data = fetchLatestData();
        if (not data.has_value())
        {
            return std::nullopt;
        }

        ARMARX_DEBUG << "RobotUnitReader: Converting data current timestep to commit";
        auto start = std::chrono::high_resolution_clock::now();

        RobotUnitData result;
        result.proprioception = converter->convert(data.value(), description);
        result.timestamp = Time(Duration::MicroSeconds(data->timestampUSec));

        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        ARMARX_DEBUG << "RobotUnitReader: The total time needed to convert the data to commit: " << duration;

        if (debugObserver)
        {
            debugObserver->setDebugObserverDatafield("RobotUnitReader | t Read+Group [ms]", duration.count() / 1000.f);
        }

        return result;
    }


    std::optional<RobotUnitDataStreaming::TimeStep> RobotUnitReader::fetchLatestData()
    {
        std::deque<RobotUnitDataStreaming::TimeStep>& data = receiver->getDataBuffer();
        if (debugObserver)
        {
            debugObserver->setDebugObserverDatafield("RobotUnitReader | Buffer Size", data.size());
        }
        if (data.empty())
        {
            return std::nullopt;
        }
        else
        {
            RobotUnitDataStreaming::TimeStep currentTimestep = data.back();
            data.clear();
            return currentTimestep;
        }
    }


}

