/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD / STL
#include <string>
#include <optional>
#include <unordered_map>

// Eigen
#include <Eigen/Geometry>

// ArmarX
#include <RobotAPI/libraries/armem/core/forward_declarations.h>
#include <RobotAPI/libraries/armem/server/segment/SpecializedCoreSegment.h>
#include <RobotAPI/libraries/armem/server/segment/SpecializedProviderSegment.h>

#include <RobotAPI/libraries/armem_robot_state/aron/Transform.aron.generated.h>
#include <RobotAPI/libraries/armem_robot_state/server/forward_declarations.h>


namespace armarx::armem::server::robot_state::localization
{
    class Segment : public segment::SpecializedCoreSegment
    {
        using Base = segment::SpecializedCoreSegment;

    public:

        Segment(server::MemoryToIceAdapter& iceMemory);
        virtual ~Segment() override;

        void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "") override;

        void init() override;

        void onConnect();


        RobotPoseMap getRobotGlobalPoses(const armem::Time& timestamp) const;
        RobotPoseMap getRobotGlobalPosesLocking(const armem::Time& timestamp) const;

        RobotFramePoseMap getRobotFramePoses(const armem::Time& timestamp) const;
        RobotFramePoseMap getRobotFramePosesLocking(const armem::Time& timestamp) const;

        bool commitTransform(const armem::robot_state::Transform& transform);
        bool commitTransformLocking(const armem::robot_state::Transform& transform);


    private:

        EntityUpdate makeUpdate(const armem::robot_state::Transform& transform) const;

        PredictionResult predictLinear(const PredictionRequest& request);

        struct Properties
        {
            double predictionTimeWindow = 2;
        };
        Properties properties;

    };

}  // namespace armarx::armem::server::robot_state::localization
