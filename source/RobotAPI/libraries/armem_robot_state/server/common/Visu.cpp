#include "Visu.h"

#include <algorithm>
#include <exception>
#include <string>

#include <Eigen/Geometry>

#include <SimoxUtility/algorithm/get_map_keys_values.h>
#include <SimoxUtility/math/pose.h>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/interface/core/PackagePath.h>

#include <RobotAPI/libraries/armem/core/Time.h>

#include <RobotAPI/components/ArViz/Client/Elements.h>

#include <RobotAPI/libraries/armem_robot_state/server/description/Segment.h>
#include <RobotAPI/libraries/armem_robot_state/server/localization/Segment.h>
#include <RobotAPI/libraries/armem_robot_state/server/proprioception/Segment.h>

#include "combine.h"


namespace armarx::armem::server::robot_state
{

    Visu::Visu(
        const description::Segment& descriptionSegment,
        const proprioception::Segment& proprioceptionSegment,
        const localization::Segment& localizationSegment)
        : descriptionSegment(descriptionSegment),
          proprioceptionSegment(proprioceptionSegment),
          localizationSegment(localizationSegment)
    {
        Logging::setTag("Visu");
    }


    void Visu::defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix)
    {
        defs->optional(
            p.enabled, prefix + "enabled", "Enable or disable visualization of objects.");
        defs->optional(p.frequencyHz, prefix + "frequenzyHz", "Frequency of visualization.");
    }


    void Visu::init()
    {
    }


    void Visu::connect(const viz::Client& arviz, DebugObserverInterfacePrx debugObserver)
    {
        this->arviz = arviz;
        if (debugObserver)
        {
            bool batchMode = true;
            this->debugObserver = DebugObserverHelper("RobotStateMemory", debugObserver, batchMode);
        }

        if (updateTask)
        {
            updateTask->stop();
            updateTask->join();
            updateTask = nullptr;
        }
        updateTask = new SimpleRunningTask<>([this]()
        {
            this->visualizeRun();
        });
        updateTask->start();
    }


    void Visu::visualizeRobots(viz::Layer& layer, const robot::Robots& robots)
    {
        for (const robot::Robot& robot : robots)
        {
            const armarx::data::PackagePath xmlPath = robot.description.xml.serialize();

            // clang-format off
            viz::Robot robotVisu = viz::Robot(robot.description.name)
                                   .file(xmlPath.package, xmlPath.path)
                                   .joints(robot.config.jointMap)
                                   .pose(robot.config.globalPose);

            robotVisu.useFullModel();
            // clang-format on

            layer.add(robotVisu);
        }
    }


    void Visu::visualizeFrames(
        viz::Layer& layerFrames,
        const std::unordered_map<std::string, std::vector<Eigen::Affine3f>>& frames)
    {
        for (const auto& [robotName, robotFrames] : frames)
        {
            Eigen::Affine3f pose = Eigen::Affine3f::Identity();

            int i = 0;
            for (const auto& frame : robotFrames)
            {
                Eigen::Affine3f from = pose;
                Eigen::Affine3f to = pose * frame;

                layerFrames.add(viz::Arrow(robotName + std::to_string(++i)).fromTo(from.translation(), to.translation()));
            }
        }
    }


    void Visu::visualizeRun()
    {
        CycleUtil cycle(static_cast<int>(1000 / p.frequencyHz));
        while (updateTask and not updateTask->isStopped())
        {
            if (p.enabled)
            {
                const Time timestamp = Time::Now();
                ARMARX_DEBUG << "Visu task at " << armem::toStringMilliSeconds(timestamp);

                try
                {
                    visualizeOnce(timestamp);
                }
                catch (const std::exception& e)
                {
                    ARMARX_WARNING << "Caught exception while visualizing robots: \n" << e.what();
                }
                catch (...)
                {
                    ARMARX_WARNING << "Caught unknown exception while visualizing robots.";
                }

                if (debugObserver.has_value())
                {
                    debugObserver->sendDebugObserverBatch();
                }
            }
            cycle.waitForCycleDuration();
        }
    }


    void Visu::visualizeOnce(const Time& timestamp)
    {
        TIMING_START(tVisuTotal);

        // TODO(fabian.reister): use timestamp

        // Get data.
        TIMING_START(tVisuGetData);

        TIMING_START(tRobotDescriptions);
        const description::RobotDescriptionMap robotDescriptions =
            descriptionSegment.getRobotDescriptionsLocking(timestamp);
        TIMING_END_STREAM(tRobotDescriptions, ARMARX_DEBUG);

        TIMING_START(tGlobalPoses);
        const auto globalPoses = localizationSegment.getRobotGlobalPosesLocking(timestamp);
        TIMING_END_STREAM(tGlobalPoses, ARMARX_DEBUG);

        TIMING_START(tRobotFramePoses);
        const auto frames = localizationSegment.getRobotFramePosesLocking(timestamp);
        TIMING_END_STREAM(tRobotFramePoses, ARMARX_DEBUG);

        TIMING_START(tJointPositions);
        const auto jointPositions =
            proprioceptionSegment.getRobotJointPositionsLocking(
                timestamp, debugObserver ? &*debugObserver : nullptr);
        TIMING_END_STREAM(tJointPositions, ARMARX_DEBUG);

        TIMING_END_STREAM(tVisuGetData, ARMARX_DEBUG);


        // Build layers.
        TIMING_START(tVisuBuildLayers);

        // We need all 3 information:
        // - robot description
        // - global pose
        // - joint positions
        // => this is nothing but an armem::Robot
        ARMARX_DEBUG << "Combining robot ..."
                     << "\n- " << robotDescriptions.size() << " descriptions"
                     << "\n- " << globalPoses.size() << " global poses"
                     << "\n- " << jointPositions.size() << " joint positions";

        const robot::Robots robots =
            combine(robotDescriptions, globalPoses, jointPositions, timestamp);

        ARMARX_DEBUG << "Visualize " << robots.size() << " robots ...";
        viz::Layer layer = arviz.layer("Robots");
        visualizeRobots(layer, robots);

        ARMARX_DEBUG << "Visualize frames ...";
        viz::Layer layerFrames = arviz.layer("Frames");
        visualizeFrames(layerFrames, frames);

        TIMING_END_STREAM(tVisuBuildLayers, ARMARX_DEBUG);


        // Commit layers.

        ARMARX_DEBUG << "Commit visualization ...";
        TIMING_START(tVisuCommit);
        arviz.commit({layer, layerFrames});
        TIMING_END_STREAM(tVisuCommit, ARMARX_DEBUG);

        TIMING_END_STREAM(tVisuTotal, ARMARX_DEBUG);

        if (debugObserver.has_value())
        {
            const std::string p = "Visu | ";
            debugObserver->setDebugObserverDatafield(p + "t Total (ms)", tVisuTotal.toMilliSecondsDouble());
            debugObserver->setDebugObserverDatafield(p + "t 1 Get Data (ms)", tVisuGetData.toMilliSecondsDouble());
            debugObserver->setDebugObserverDatafield(p + "t 1.1 Descriptions (ms)", tRobotDescriptions.toMilliSecondsDouble());
            debugObserver->setDebugObserverDatafield(p + "t 1.2 Global Poses (ms)", tGlobalPoses.toMilliSecondsDouble());
            debugObserver->setDebugObserverDatafield(p + "t 1.3 Frames (ms)", tRobotFramePoses.toMilliSecondsDouble());
            debugObserver->setDebugObserverDatafield(p + "t 1.4 Joint Positions (ms)", tJointPositions.toMilliSecondsDouble());
            debugObserver->setDebugObserverDatafield(p + "t 2 Build Layers (ms)", tVisuBuildLayers.toMilliSecondsDouble());
            debugObserver->setDebugObserverDatafield(p + "t 3 Commit (ms)", tVisuCommit.toMilliSecondsDouble());
        }
    }

} // namespace armarx::armem::server::robot_state
