/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <optional>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/libraries/DebugObserverHelper/DebugObserverHelper.h>

#include <RobotAPI/components/ArViz/Client/Client.h>

#include <RobotAPI/libraries/armem_objects/types.h>

#include <RobotAPI/libraries/armem_robot_state/server/forward_declarations.h>


namespace armarx::armem::server::robot_state
{

    /**
     * @brief Models decay of object localizations by decreasing the confidence
     * the longer the object was not localized.
     */
    class Visu : public armarx::Logging
    {
    public:

        Visu(const description::Segment& descriptionSegment,
             const proprioception::Segment& proprioceptionSegment,
             const localization::Segment& localizationSegment);


        void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "visu.");
        void init();
        void connect(const viz::Client& arviz, DebugObserverInterfacePrx debugObserver = nullptr);


    private:

        void visualizeRun();
        void visualizeOnce(const Time& timestamp);


        static
        void visualizeRobots(
            viz::Layer& layer,
            const robot::Robots& robots);

        static
        void visualizeFrames(
            viz::Layer& layerFrames,
            const std::unordered_map<std::string, std::vector<Eigen::Affine3f>>& frames);


    private:

        viz::Client arviz;
        std::optional<DebugObserverHelper> debugObserver;

        const description::Segment& descriptionSegment;
        const proprioception::Segment& proprioceptionSegment;
        const localization::Segment& localizationSegment;

        struct Properties
        {
            bool enabled = true;
            float frequencyHz = 25;
        } p;


        SimpleRunningTask<>::pointer_type updateTask;

    };

}  // namespace armarx::armem::server::robot_state
