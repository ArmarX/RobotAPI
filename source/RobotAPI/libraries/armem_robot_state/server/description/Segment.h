/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD / STL
#include <string>
#include <optional>
#include <mutex>
#include <unordered_map>

// ArmarX
#include <RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h>
#include <RobotAPI/libraries/armem/server/segment/SpecializedCoreSegment.h>
#include <RobotAPI/libraries/armem/server/segment/SpecializedProviderSegment.h>
#include <RobotAPI/libraries/armem_robot_state/server/forward_declarations.h>

// Aron
#include <RobotAPI/libraries/armem_robot/aron/RobotDescription.aron.generated.h>


namespace armarx::armem::server::robot_state::description
{

    class Segment : public segment::SpecializedCoreSegment
    {
        using Base = segment::SpecializedCoreSegment;

    public:

        Segment(server::MemoryToIceAdapter& iceMemory);
        virtual ~Segment() override;


        void onConnect(const RobotUnitInterfacePrx& robotUnitPrx);


        RobotDescriptionMap getRobotDescriptions(const armem::Time& timestamp) const;
        RobotDescriptionMap getRobotDescriptionsLocking(const armem::Time& timestamp) const;


    private:

        void commitRobotDescription(const robot::RobotDescription& robotDescription);
        void updateRobotDescription();

        RobotUnitInterfacePrx robotUnit;

    };

}  // namespace armarx::armem::server::robot_state::description
