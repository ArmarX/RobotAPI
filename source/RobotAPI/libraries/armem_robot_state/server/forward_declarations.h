/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <map>
#include <string>
#include <unordered_map>
#include <vector>

#include <Eigen/Geometry>



namespace armarx::armem::arondto
{
    struct Transform;
    struct TransformHeader;

    struct JointState;

    class RobotDescription;
}

namespace armarx::armem::robot
{
    struct RobotDescription;
    struct RobotState;
    struct Robot;

    using Robots = std::vector<Robot>;
    using RobotDescriptions = std::vector<RobotDescription>;
    using RobotStates = std::vector<RobotState>;
}

namespace armarx::armem::robot_state
{
    struct JointState;

    struct Transform;
    struct TransformHeader;
}

namespace armarx::armem::server::robot_state::description
{
    using RobotDescriptionMap = std::unordered_map<std::string, robot::RobotDescription>;
    class Segment;
}

namespace armarx::armem::server::robot_state::localization
{
    using RobotPoseMap = std::unordered_map<std::string, Eigen::Affine3f>;
    using RobotFramePoseMap = std::unordered_map<std::string, std::vector<Eigen::Affine3f>>;
    class Segment;
}

namespace armarx::armem::server::robot_state::proprioception
{
    using RobotJointPositionMap = std::unordered_map<std::string, std::map<std::string, float>>;
    class Segment;
}

