#include "utils.h"

namespace armarx::armem::robot_state
{
    armarx::armem::MemoryID makeMemoryID(const robot::RobotDescription& desc)
    {
        return MemoryID("RobotState/Description")
               .withProviderSegmentName(desc.name)
               .withEntityName("description");
    }
}