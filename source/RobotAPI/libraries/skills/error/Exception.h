/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Peller-Konrad (fabian dot peller-konrad at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

// STD/STL
#include <string>
#include <vector>
#include <map>

// ArmarX
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/exceptions/Exception.h>


namespace armarx::skills::error
{
    /**
     * @brief A base class for aron exceptions. All aron exceptions inherit from this class
     */
    class SkillException :
        public armarx::LocalException
    {
    public:
        SkillException() = delete;
        SkillException(const std::string& prettymethod, const std::string& reason) :
            LocalException(prettymethod + ": " + reason + ".")
        {
        }
    };

    /**
     * @brief The NotImplementedYetException class
     */
    class NotImplementedYetException :
        public SkillException
    {
    public:
        NotImplementedYetException() = delete;
        NotImplementedYetException(const std::string& prettymethod) :
            SkillException(prettymethod, "This method is not yet implemented!")
        {
        }
    };
}
