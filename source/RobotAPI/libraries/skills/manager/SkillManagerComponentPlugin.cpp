#include "SkillManagerComponentPlugin.h"

#include <ArmarXCore/core/Component.h>
#include "../error/Exception.h"

namespace armarx::plugins
{
    void SkillManagerComponentPlugin::preOnInitComponent()
    {}

    void SkillManagerComponentPlugin::preOnConnectComponent()
    {}

    void SkillManagerComponentPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {}
}


namespace armarx
{
    SkillManagerComponentPluginUser::SkillManagerComponentPluginUser()
    {
        addPlugin(plugin);
    }

    void SkillManagerComponentPluginUser::addProvider(const skills::manager::dto::ProviderInfo& info, const Ice::Current&)
    {
        std::lock_guard l(skillProviderMapMutex);
        if (skillProviderMap.find(info.providerName) == skillProviderMap.end())
        {
            ARMARX_INFO << "Adding a provider with name '" << info.providerName << "'.";
            skillProviderMap.insert({info.providerName, info.provider});
        }
        else
        {
            ARMARX_INFO << "Trying to add a provider with name '" << info.providerName << "' but the provider already exists.";
        }
    }

    void SkillManagerComponentPluginUser::removeProvider(const std::string& providerName, const Ice::Current&)
    {
        std::lock_guard l(skillProviderMapMutex);
        if (auto it = skillProviderMap.find(providerName); it != skillProviderMap.end())
        {
            ARMARX_INFO << "Removing a provider with name '" << providerName << "'.";
            skillProviderMap.erase(it);
        }
        else
        {
            ARMARX_INFO << "Trying to remove a provider with name '" << providerName << "' but it couldn't found.";
        }
    }

    std::string SkillManagerComponentPluginUser::getFirstProviderNameThatHasSkill(const std::string& skillName)
    {
        for (const auto& [providerName, providerPrx] : skillProviderMap)
        {
            auto allSkills = providerPrx->getSkillDescriptions();
            for (const auto& [currentSkillName, skillDesc] : allSkills)
            {
                if (currentSkillName == skillName)
                {
                    return providerName;
                }
            }
        }
        return "INVALID PROVIDER NAME";
    }

    skills::provider::dto::SkillStatusUpdate SkillManagerComponentPluginUser::executeSkill(const skills::manager::dto::SkillExecutionRequest& info, const Ice::Current&)
    {
        std::string providerName = "INVALID PROVIDER NAME";
        if (info.skillId.providerName == "*")
        {
            providerName = getFirstProviderNameThatHasSkill(info.skillId.skillName);
        }
        else if(not(info.skillId.providerName.empty()))
        {
            providerName = info.skillId.providerName;
        }

        if (auto it = skillProviderMap.find(providerName); it != skillProviderMap.end())
        {
            skills::callback::dti::SkillProviderCallbackInterfacePrx myPrx;
            getProxy(myPrx, -1);

            skills::provider::dto::SkillExecutionRequest exInfo;
            exInfo.skillName = info.skillId.skillName;
            exInfo.executorName = info.executorName;
            exInfo.callbackInterface = myPrx;
            exInfo.params = info.params;

            return it->second->executeSkill(exInfo);
        }
        else
        {
            ARMARX_ERROR << "Could not execute a skill of provider '" + providerName + "' because the provider does not exist.";
            throw skills::error::SkillException(__PRETTY_FUNCTION__, "Skill execution failed. Could not execute a skill of provider '" + providerName + "' because the provider does not exist.");
        }
    }

    void SkillManagerComponentPluginUser::abortSkill(const std::string& providerName, const std::string& skillName, const Ice::Current &current)
    {
        if (auto it = skillProviderMap.find(providerName); it != skillProviderMap.end())
        {
            it->second->abortSkill(skillName);
        }
    }

    void SkillManagerComponentPluginUser::updateStatusForSkill(const skills::provider::dto::SkillStatusUpdate& statusUpdate, const Ice::Current&)
    {
        (void) statusUpdate;
        // If you want to use the status, implement this method!
    }

    skills::manager::dto::SkillDescriptionMapMap SkillManagerComponentPluginUser::getSkillDescriptions(const Ice::Current &current)
    {
        skills::manager::dto::SkillDescriptionMapMap ret;
        for (const auto& [n, s] : skillProviderMap)
        {
            skills::provider::dto::SkillDescriptionMap m = s->getSkillDescriptions();
            ret.insert({n, m});
        }
        return ret;
    }

    skills::manager::dto::SkillStatusUpdateMapMap SkillManagerComponentPluginUser::getSkillExecutionStatuses(const Ice::Current &current)
    {
        skills::manager::dto::SkillStatusUpdateMapMap ret;
        for (const auto& [n, s] : skillProviderMap)
        {
            skills::provider::dto::SkillStatusUpdateMap m = s->getSkillExecutionStatuses();
            ret.insert({n, m});
        }
        return ret;
    }
}
