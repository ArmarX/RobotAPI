#pragma once

#include <mutex>

#include <ArmarXCore/core/ComponentPlugin.h>
#include <ArmarXCore/core/ManagedIceObject.h>

#include <RobotAPI/interface/skills/SkillManagerInterface.h>

namespace armarx::plugins
{
    class SkillManagerComponentPlugin : public ComponentPlugin
    {
    public:
        using ComponentPlugin::ComponentPlugin;

        void preOnInitComponent() override;

        void preOnConnectComponent() override;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;
    };
}

namespace armarx
{
    class SkillManagerComponentPluginUser :
            virtual public ManagedIceObject,
            virtual public skills::manager::dti::SkillManagerInterface
    {
    public:
        SkillManagerComponentPluginUser();

        void addProvider(const skills::manager::dto::ProviderInfo& providerInfo, const Ice::Current &current) override;
        void removeProvider(const std::string&, const Ice::Current &current) override;

        skills::provider::dto::SkillStatusUpdate executeSkill(const skills::manager::dto::SkillExecutionRequest& info, const Ice::Current &current) override;
        void updateStatusForSkill(const skills::provider::dto::SkillStatusUpdate& update, const Ice::Current &current) override;
        void abortSkill(const std::string& providerName, const std::string& skillName, const Ice::Current &current) override;

        skills::manager::dto::SkillDescriptionMapMap getSkillDescriptions(const Ice::Current &current) override;
        skills::manager::dto::SkillStatusUpdateMapMap getSkillExecutionStatuses(const Ice::Current &current) override;

    protected:
        std::string getFirstProviderNameThatHasSkill(const std::string& skillName);

    private:
        armarx::plugins::SkillManagerComponentPlugin* plugin = nullptr;

    protected:
        std::mutex skillProviderMapMutex;
        std::map<std::string, skills::provider::dti::SkillProviderInterfacePrx> skillProviderMap;
    };
}
