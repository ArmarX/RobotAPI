/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/time/Frequency.h>
#include <ArmarXCore/core/time/Metronome.h>

#include "Skill.h"
#include "PeriodicSkill.h"
#include "SpecializedSkill.h"

namespace armarx::skills
{
    template <class AronT>
    class PeriodicSpecializedSkill : public SpecializedSkill<AronT>
    {
    public:
        using Base = SpecializedSkill<AronT>;

        using Skill::description;
        using Skill::getSkillId;

        using StepResult = PeriodicSkill::StepResult;

        PeriodicSpecializedSkill() = delete;
        PeriodicSpecializedSkill(const SkillDescription& skillDescription, const armarx::Frequency& frequency) :
            Base(skillDescription), frequency(frequency)
        {
        }


        StepResult stepOfSkill(const typename Base::SpecializedMainInput& in)
        {
            return this->step(in);
        }

    private:
        using Skill::stopped;
        using Skill::timeoutReached;

        /// Do not use anymore
        Skill::MainResult main(const typename Base::SpecializedMainInput& in) final
        {
            core::time::Metronome metronome(frequency);

            while (not Skill::checkWhetherSkillShouldStopASAP())
            {
                const auto statusUpdate = stepOfSkill(in);
                switch (statusUpdate.status)
                {
                    case ActiveOrTerminatedSkillStatus::Running:
                        // nothing to do here
                        break;
                    case ActiveOrTerminatedSkillStatus::Aborted:
                        return {TerminatedSkillStatus::Aborted, statusUpdate.data};
                    case ActiveOrTerminatedSkillStatus::Succeeded:
                        return {TerminatedSkillStatus::Succeeded, statusUpdate.data};
                    case ActiveOrTerminatedSkillStatus::Failed:
                        return {TerminatedSkillStatus::Failed, statusUpdate.data};
                }

                const auto sleepDuration = metronome.waitForNextTick();
                if (not sleepDuration.isPositive())
                {
                    ARMARX_INFO << deactivateSpam() << "PeriodicSkill: execution took too long (" << -sleepDuration << " too long. Expected " << frequency.toCycleDuration() << ")";
                }
            }

            if (stopped)
            {
                return {TerminatedSkillStatus::Aborted, nullptr};
            }

            if (timeoutReached)
            {
                ARMARX_WARNING << "The skill " << getSkillId().toString() << " reached timeout!";
                return {TerminatedSkillStatus::Failed, nullptr};
            }

            throw skills::error::SkillException(__PRETTY_FUNCTION__, "Should not happen!");
        }

        /// Override this method with your own step function
        virtual StepResult step(const typename Base::SpecializedMainInput& in)
        {
            ARMARX_IMPORTANT << "Dummy executing once skill '" << this->description.skillName << "'. Please overwrite this method!";
            return {ActiveOrTerminatedSkillStatus::Succeeded, nullptr};
        }

    private:
        const armarx::Frequency frequency;
    };

} // namespace armarx::skills
