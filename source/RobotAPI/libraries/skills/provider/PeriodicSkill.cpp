/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PeriodicSkill.h"

#include "ArmarXCore/core/exceptions/LocalException.h"
#include "ArmarXCore/core/logging/Logging.h"
#include "ArmarXCore/core/time/Frequency.h"
#include <ArmarXCore/core/time/Metronome.h>

#include "RobotAPI/libraries/skills/provider/Skill.h"

namespace armarx::skills
{
    PeriodicSkill::PeriodicSkill(const SkillDescription& skillDescription, const armarx::Frequency& frequency) :
        Skill(skillDescription), frequency(frequency)
    {
    }

    PeriodicSkill::StepResult PeriodicSkill::stepOfSkill(const MainInput& in)
    {
        return this->step(in);
    }

    Skill::MainResult PeriodicSkill::main(const MainInput& in)
    {
        core::time::Metronome metronome(frequency);

        while (not Skill::checkWhetherSkillShouldStopASAP())
        {
            const auto res = stepOfSkill(in);
            switch (res.status)
            {
                case ActiveOrTerminatedSkillStatus::Running:
                    // nothing to do here
                    break;
                case ActiveOrTerminatedSkillStatus::Aborted:
                    return {TerminatedSkillStatus::Aborted, res.data};
                case ActiveOrTerminatedSkillStatus::Succeeded:
                    return {TerminatedSkillStatus::Succeeded, res.data};
                case ActiveOrTerminatedSkillStatus::Failed:
                    return {TerminatedSkillStatus::Failed, res.data};
            }

            const auto sleepDuration = metronome.waitForNextTick();
            if (not sleepDuration.isPositive())
            {
                ARMARX_INFO << deactivateSpam() << "PeriodicSkill: execution took too long (" << -sleepDuration << " too long. Expected " << frequency.toCycleDuration() << ")";
            }
        }

        if (stopped)
        {
            return {TerminatedSkillStatus::Aborted, nullptr};
        }

        if (timeoutReached)
        {
            ARMARX_WARNING << "The skill " << getSkillId().toString() << " reached timeout!";
            return {TerminatedSkillStatus::Failed, nullptr};
        }

        throw skills::error::SkillException(__PRETTY_FUNCTION__, "Should not happen!");
    }


    PeriodicSkill::StepResult PeriodicSkill::step(const MainInput& in)
    {
        ARMARX_IMPORTANT << "Dummy executing once skill '" << description.skillName << "'. Please overwrite this method!";
        return {ActiveOrTerminatedSkillStatus::Succeeded, nullptr};
    }

} // namespace armarx::skills
