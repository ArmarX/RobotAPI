#include "SkillProxy.h"

namespace armarx
{
    namespace skills
    {
        SkillProxy::SkillProxy(const manager::dti::SkillManagerInterfacePrx& manager, const SkillID& skillId) :
            manager(manager),
            skillId(skillId)
        {
        }

        SkillProxy::SkillProxy(const manager::dti::SkillManagerInterfacePrx& manager, const std::string& skillProviderName, const std::string& skillName) :
            manager(manager),
            skillId(skillProviderName, skillName)
        {
        }

        SkillProxy::SkillProxy(const manager::dti::SkillManagerInterfacePrx& manager, const std::string& skillProviderName, const SkillDescription& skillDesc) :
            manager(manager),
            skillId(skillProviderName, skillDesc.skillName)
        {
        }

        TerminatedSkillStatusUpdate SkillProxy::executeFullSkill(const std::string& executorName, const aron::data::DictPtr& params)
        {
            skills::manager::dto::SkillExecutionRequest req;
            req.executorName = executorName;
            req.params = params->toAronDictDTO();
            req.skillId = skillId.toIce();

            auto terminatingUpdate = manager->executeSkill(req);
            return TerminatedSkillStatusUpdate::FromIce(terminatingUpdate);
        }

        IceInternal::Handle<Ice::AsyncResult> SkillProxy::begin_executeFullSkill(const std::string& executorName, const aron::data::DictPtr& params)
        {
            skills::manager::dto::SkillExecutionRequest req;
            req.executorName = executorName;
            req.params = params->toAronDictDTO();
            req.skillId = skillId.toIce();

            auto future = manager->begin_executeSkill(req);
            return future;
        }
    }
}
