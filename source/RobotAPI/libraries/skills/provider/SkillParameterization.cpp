#include "SkillParameterization.h"

namespace armarx
{
    namespace skills
    {
        aron::data::dto::DictPtr SkillParameterization::toIce() const
        {
            return aron::data::Dict::ToAronDictDTO(usedInputParams);
        }
    }
}
