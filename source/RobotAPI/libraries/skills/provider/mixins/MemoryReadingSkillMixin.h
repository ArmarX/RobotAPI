#pragma once


// Others
#include <RobotAPI/libraries/armem/client/Reader.h>

namespace armarx::skills::mixin
{
    struct MemoryReadingSkillMixin
    {
        armem::client::Reader memoryReader;

        //MemoryReadingSkillMixin(armem::client::MemoryNameSystem& mns) : memoryReader(mns)
        //{}
    };
}
