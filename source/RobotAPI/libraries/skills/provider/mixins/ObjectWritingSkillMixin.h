#pragma once


// Others
#include <RobotAPI/libraries/armem_objects/client/instance/ObjectWriter.h>

namespace armarx::skills::mixin
{
    struct ObjectWritingSkillMixin
    {
        armem::obj::instance::Writer objectWriter;

        ObjectWritingSkillMixin(armem::client::MemoryNameSystem& mns) : objectWriter(mns)
        {}
    };
}
