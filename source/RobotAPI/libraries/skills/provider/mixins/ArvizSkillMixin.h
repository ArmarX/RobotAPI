#pragma once


// Others
#include <RobotAPI/libraries/RobotAPIComponentPlugins/ArVizComponentPlugin.h>

namespace armarx::skills::mixin
{
    struct ArvizSkillMixin
    {
        armarx::viz::Client arviz;
        std::string layerName;

        ArvizSkillMixin(const armarx::viz::Client& a, const std::string& ln) :
            arviz(a),
            layerName(ln)
        {
        }

        void clearLayer()
        {
            auto l = arviz.layer(layerName);
            arviz.commit(l);
        }
    };
}
