#pragma once


// Others
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>

namespace armarx::skills::mixin
{
    struct MNSSkillMixin
    {
        armem::client::MemoryNameSystem mns;

        MNSSkillMixin(const armem::client::MemoryNameSystem& m) : mns(m)
        {}
    };
}
