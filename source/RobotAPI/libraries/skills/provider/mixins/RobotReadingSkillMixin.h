#pragma once


// Others
#include <RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h>

namespace armarx::skills::mixin
{
    struct RobotReadingSkillMixin
    {
        armem::robot_state::VirtualRobotReader robotReader;

        RobotReadingSkillMixin(armem::client::MemoryNameSystem& mns) :
            robotReader(mns)
        {}
    };

    struct SpecificRobotReadingSkillMixin
    {
        std::string robotName;
        armem::robot_state::VirtualRobotReader robotReader;

        SpecificRobotReadingSkillMixin(const std::string& rn, armem::client::MemoryNameSystem& mns) :
            robotName(rn),
            robotReader(mns)
        {}
    };
}
