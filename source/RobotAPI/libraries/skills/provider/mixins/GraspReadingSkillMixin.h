#pragma once


// Others
#include <RobotAPI/libraries/armem_grasping/client/KnownGraspCandidateReader.h>

namespace armarx::skills::mixin
{
    struct GraspReadingSkillMixin
    {
        armem::grasping::known_grasps::Reader graspReader;

        GraspReadingSkillMixin(armem::client::MemoryNameSystem& mns) : graspReader(mns)
        {}
    };

    struct SpecificGraspReadingSkillMixin
    {
        std::string objectEntityId;
        armem::grasping::known_grasps::Reader graspReader;

        SpecificGraspReadingSkillMixin(const std::string& n, armem::client::MemoryNameSystem& mns) : objectEntityId(n), graspReader(mns)
        {}
    };
}
