#pragma once


// Others
#include <RobotAPI/libraries/armem_objects/client/instance/ObjectReader.h>

namespace armarx::skills::mixin
{
    struct ObjectReadingSkillMixin
    {
        armem::obj::instance::Reader objectReader;

        ObjectReadingSkillMixin(armem::client::MemoryNameSystem& mns, const objpose::ObjectPoseProviderPrx& o) : objectReader(mns, o)
        {}
    };

    struct SpecificObjectReadingSkillMixin
    {
        std::string objectEntityId;
        armem::obj::instance::Reader objectReader;

        SpecificObjectReadingSkillMixin(const std::string& n, armem::client::MemoryNameSystem& mns, const objpose::ObjectPoseProviderPrx& o) : objectEntityId(n), objectReader(mns, o)
        {}
    };
}
