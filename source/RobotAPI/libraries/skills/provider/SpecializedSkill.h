#pragma once

#include "Skill.h"

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>

// Debug
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

namespace armarx
{
    namespace skills
    {
        template <class AronT>
        class SpecializedSkill : public Skill
        {
        public:
            using ParamType = AronT;

            struct SpecializedInitInput
            {
                std::string executorName;
                AronT params;
            };

            struct SpecializedMainInput
            {
                std::string executorName;
                AronT params;
                CallbackT callback;
            };

            struct SpecializedExitInput
            {
                std::string executorName;
                AronT params;
            };

            using Skill::Skill;
            virtual ~SpecializedSkill() = default;

            /// returns the accepted type of the skill
            static armarx::aron::type::ObjectPtr GetAcceptedType()
            {
                return AronT::ToAronType();
            }

            bool isSkillAvailable(const SpecializedInitInput& in) const
            {
                return this->isAvailable();
            }

            Skill::InitResult initSkill(const SpecializedInitInput& in)
            {
                Skill::_init();
                return this->init(in);
            }
            Skill::MainResult mainOfSkill(const SpecializedMainInput& in)
            {
                Skill::_main();
                return this->main(in);
            }
            Skill::ExitResult exitSkill(const SpecializedExitInput& in)
            {
                Skill::_exit();
                return this->exit(in);
            }

            Skill::MainResult executeFullSkill(const SpecializedMainInput& in)
            {
                this->resetSkill();
                this->initSkill(SpecializedInitInput({.executorName = in.executorName, .params = in.params}));
                auto ret = this->mainOfSkill(in);
                this->exit(SpecializedExitInput({.executorName = in.executorName, .params = in.params}));
                return ret;
            }

        private:
            virtual bool isAvailable(const SpecializedInitInput&) const
            {
                return true;
            }

            /// Override this method with the actual implementation. The callback is for status updates to the calling instance
            virtual Skill::InitResult init(const SpecializedInitInput&)
            {
                return InitResult{.status = TerminatedSkillStatus::Succeeded};
            }

            /// Override this method with the actual implementation. The callback is for status updates to the calling instance
            virtual Skill::MainResult main(const SpecializedMainInput& in)
            {
                ARMARX_IMPORTANT << "Dummy executing skill '" << description.skillName << "'. Please overwrite this method.";
                return Skill::MainResult{.status = TerminatedSkillStatus::Succeeded, .data = nullptr};
            }

            /// Override this method with the actual implementation. The callback is for status updates to the calling instance
            virtual Skill::ExitResult exit(const SpecializedExitInput&)
            {
                return ExitResult{.status = TerminatedSkillStatus::Succeeded};
            }

            /// Do not use anymore
            bool isAvailable(const InitInput& in) const final
            {
                AronT p;
                p.fromAron(in.params);

                return isAvailable(SpecializedInitInput({.executorName = in.executorName, .params = p}));
            }

            /// Do not use anymore
            Skill::InitResult init(const InitInput& in) final
            {
                AronT p;
                p.fromAron(in.params);

                return init(SpecializedInitInput({.executorName = in.executorName, .params = p}));
            }

            /// Do not use anymore
            Skill::MainResult main(const MainInput& in) final
            {
                AronT p;
                p.fromAron(in.params);

                return main(SpecializedMainInput({.executorName = in.executorName, .params = p, .callback = in.callback}));
            }

            /// Do not use anymore
            Skill::ExitResult exit(const ExitInput& in) final
            {
                AronT p;
                p.fromAron(in.params);

                return exit(SpecializedExitInput({.executorName = in.executorName, .params = p}));
            }
        };
    }
}
