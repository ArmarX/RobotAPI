#include "SkillProviderComponentPlugin.h"

#include <ArmarXCore/core/Component.h>

#include <RobotAPI/libraries/aron/core/data/variant/primitive/All.h>

namespace armarx::plugins
{
    void SkillProviderComponentPlugin::preOnInitComponent()
    {

    }

    void SkillProviderComponentPlugin::preOnConnectComponent()
    {
        auto& p = parent<SkillProviderComponentPluginUser>();
        p.getProxy(myPrx, -1);
    }

    void SkillProviderComponentPlugin::postOnConnectComponent()
    {
        auto& p = parent<SkillProviderComponentPluginUser>();
        const std::string providerName = p.getName();

        // update skill ownership
        for (auto& [skillName, impl] : p.skillImplementations)
        {
            impl.skill->manager = manager;
            impl.skill->providerName = providerName;

            impl.statusUpdate.skillId = {providerName, skillName};
        }

        // register self to manager
        skills::manager::dto::ProviderInfo i;
        i.provider = myPrx;
        i.providerName = providerName;
        i.providedSkills = p.getSkillDescriptions();
        manager->addProvider(i);

        p.connected = true;
    }

    void SkillProviderComponentPlugin::preOnDisconnectComponent()
    {
        auto& p = parent<SkillProviderComponentPluginUser>();
        std::string providerName = p.getName();

        manager->removeProvider(providerName);
    }

    void SkillProviderComponentPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {
        std::string prefix = "skill.";
        properties->component(manager, "SkillMemory", prefix + "SkillManager", "The name of the SkillManager (or SkillMemory) proxy this provider belongs to.");
    }
}


namespace armarx
{
    SkillProviderComponentPluginUser::SkillProviderComponentPluginUser()
    {
        addPlugin(plugin);
    }

    void SkillProviderComponentPluginUser::addSkill(std::unique_ptr<skills::Skill>&& skill)
    {
        if (!skill)
        {
            return;
        }

        std::string skillName = skill->description.skillName;
        if (connected) // TODO: fix so that skills can be added anytime!!!
        {
            ARMARX_WARNING << "The SkillProvider already registered to a manager. The skill '" + skillName + "' therefore cannot be added anymore. Please only add skills in the onInit method.";
            return;
        }

        // lock skills map
        std::unique_lock l(skillsMutex);
        if (skillImplementations.find(skillName) != skillImplementations.end())
        {
            ARMARX_WARNING << "Try to add a skill '" + skillName + "' which already exists in list. Ignoring this skill.";
            return;
        }

        ARMARX_INFO << "Adding skill and set owning provider name" << skillName;
        auto s = skillImplementations.emplace(skillName, std::move(skill));
        s.first->second.statusUpdate.skillId = skills::SkillID(getName(), skillName);
    }

    void SkillProviderComponentPluginUser::addSkill(const skills::LambdaSkill::FunT& f, const skills::SkillDescription& desc)
    {
        auto lambda = std::make_unique<skills::LambdaSkill>(f, desc);
        addSkill(std::move(lambda));
    }

    skills::provider::dto::SkillDescription SkillProviderComponentPluginUser::getSkillDescription(const std::string& name, const Ice::Current &)
    {
        std::shared_lock l(skillsMutex);
        const auto& skillWrapper = skillImplementations.at(name);
        return skillWrapper.skill->description.toIce();
    }

    skills::provider::dto::SkillDescriptionMap SkillProviderComponentPluginUser::getSkillDescriptions(const Ice::Current &)
    {
        std::shared_lock l(skillsMutex);
        skills::provider::dto::SkillDescriptionMap skillDesciptions;
        for (const auto& [key, skillWrapper] : skillImplementations)
        {
            skillDesciptions.insert({key, skillWrapper.skill->description.toIce()});
        }
        return skillDesciptions;
    }

    skills::provider::dto::SkillStatusUpdate SkillProviderComponentPluginUser::getSkillExecutionStatus(const std::string& skill, const Ice::Current &)
    {
        std::shared_lock l(skillsMutex);
        auto& skillWrapper = skillImplementations.at(skill);

        std::shared_lock l2(skillWrapper.skillStatusMutex);
        return skillWrapper.statusUpdate.toIce();
    }

    skills::provider::dto::SkillStatusUpdateMap SkillProviderComponentPluginUser::getSkillExecutionStatuses(const Ice::Current &)
    {
        std::shared_lock l(skillsMutex);
        skills::provider::dto::SkillStatusUpdateMap skillUpdates;
        for (const auto& [key, skillWrapper] : skillImplementations)
        {
            std::shared_lock l2(skillWrapper.skillStatusMutex);
            skillUpdates.insert({key, skillWrapper.statusUpdate.toIce()});
        }
        return skillUpdates;
    }

    // Please not that this method waits until the skill can be scheduled!
    skills::provider::dto::SkillStatusUpdate SkillProviderComponentPluginUser::executeSkill(const skills::provider::dto::SkillExecutionRequest& info, const Ice::Current &)
    {
        // The skill will be executed in a different thread
        std::thread execution;

        // setup input args for skill execution
        skills::SkillParameterization usedParameterization;
        usedParameterization.usedCallbackInterface = info.callbackInterface;
        usedParameterization.usedInputParams = aron::data::Dict::FromAronDictDTO(info.params);

        skills::provider::dto::SkillStatusUpdate ret;
        {
            std::shared_lock l(skillsMutex);
            std::string skillName = info.skillName;
            ARMARX_CHECK_EXPRESSION(skillImplementations.count(skillName) > 0) << "\nThe names are: " << VAROUT(skillName) << ", " << VAROUT(getName());

            // get reference of the wrapper
            auto& wrapper = skillImplementations.at(skillName);

            // async start execution. But we wait for the execution to finish at the end of this method
            execution = std::thread([&ret, &wrapper, &info, &usedParameterization](){
                // execute waits until the previous execution finishes.
                auto x = wrapper.setupAndExecuteSkill(info.executorName, usedParameterization);
                ret = x.toIce();
            });
        } // release lock. We don't know how long the skill needs to finish and we have to release the lock for being able to abort the execution

        if (execution.joinable())
        {
            execution.join();
        }
        return ret;
    }

    void SkillProviderComponentPluginUser::abortSkill(const std::string& skillName, const Ice::Current &)
    {
        std::shared_lock l(skillsMutex);
        ARMARX_CHECK_EXPRESSION(skillImplementations.count(skillName) > 0);

        auto& wrapper = skillImplementations.at(skillName);
        wrapper.skill->notifySkillToStopASAP();
    }
}
