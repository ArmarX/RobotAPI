#include "SkillDescription.h"

namespace armarx
{
    namespace skills
    {
        provider::dto::SkillDescription SkillDescription::toIce() const
        {
            provider::dto::SkillDescription ret;
            ret.acceptedType = aron::type::Object::ToAronObjectDTO(acceptedType);
            ret.description = description;
            ret.skillName = skillName;
            ret.robots = robots;
            ret.timeoutMs = timeout.toMilliSeconds();
            ret.defaultParams = aron::data::Dict::ToAronDictDTO(defaultParams);
            return ret;
        }
    }
}
