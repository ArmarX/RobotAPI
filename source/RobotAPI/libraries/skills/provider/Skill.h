#pragma once

// std/stl
#include <mutex>
#include <queue>
#include <thread>
#include <functional>

// base class
#include <ArmarXCore/core/logging/Logging.h>

// ArmarX
#include <ArmarXCore/core/time/DateTime.h>
#include <ArmarXCore/core/time/Metronome.h>

#include <RobotAPI/interface/skills/SkillManagerInterface.h>
#include <RobotAPI/libraries/aron/core/data/variant/All.h>

#include "../error/Exception.h"

#include "SkillID.h"
#include "SkillStatusUpdate.h"
#include "SkillDescription.h"

namespace armarx
{
    namespace skills
    {
        class Skill : public armarx::Logging
        {
        public:
            using CallbackT = std::function<void(const aron::data::DictPtr&)>;

            struct InitResult
            {
                TerminatedSkillStatus status;
            };

            struct InitInput
            {
                std::string executorName;
                aron::data::DictPtr params;
            };

            struct MainInput
            {
                std::string executorName;
                aron::data::DictPtr params;
                CallbackT callback;
            };

            struct MainResult
            {
                TerminatedSkillStatus status;
                aron::data::DictPtr data = nullptr;
            };

            struct ExitResult
            {
                TerminatedSkillStatus status;
            };

            struct ExitInput
            {
                std::string executorName;
                aron::data::DictPtr params;
            };

            Skill() = delete;
            Skill(const SkillDescription&);
            virtual ~Skill() = default;

            /// The id of the skill (combination of provider and name must be unique).
            SkillID getSkillId() const
            {
                return {providerName, description.skillName};
            }

            // Non virtual base methods. They internally call the virtual methods
            // Lifecycle of a skill:
            // 1. check if it is available
            bool isSkillAvailable(const InitInput& in) const;

            // 2. reset skill
            void resetSkill();

            // 3. Set skill scheduled. Wait for dependencies
            void waitForDependenciesOfSkill();

            // 4. All dependencies resolved. Init skill
            InitResult initSkill(const InitInput& in);

            // 5. Execute main function of skill
            MainResult mainOfSkill(const MainInput& in);

            // 6. Exit skill. This method is called in any case once the skill is scheduled.
            ExitResult exitSkill(const ExitInput& in);

            // Condition listeners
            // used to notify the skill from extern to stop
            void notifySkillToStopASAP();

            // returns whether the skill should terminate as soon as possible
            bool checkWhetherSkillShouldStopASAP() const;

            /// Do init, main, exit together
            MainResult executeFullSkill(const MainInput& in);

        protected:
            // fires if the skill reaches timeout
            void notifyTimeoutReached();

            // helper methods to do all the static initialization stuff
            void _init();
            void _main();
            void _exit();

        private:
            /// Override if your skill can be unavailable. It receives the same input as the init method
            virtual bool isAvailable(const InitInput& in) const;

            /// Override if you have special members that needs to be resetted. It is called before the skill ititializes
            virtual void reset();

            /// Override if you have special dependencies you have to wait for
            virtual void waitForDependencies();

            /// Override this method with the actual implementation.
            virtual InitResult init(const InitInput& in);

            /// Override this method with the actual implementation. The callback is for status updates to the calling instance
            virtual MainResult main(const MainInput& in);

            /// Override this method with the actual implementation.
            virtual ExitResult exit(const ExitInput& in);

            /// Override these methods if you want to do something special when notification comes
            virtual void onTimeoutReached();
            virtual void onStopRequested();

        protected:
            /// install a condition which is frequently checked from the conditionCheckingThread
            void installConditionWithCallback(std::function<bool()>&& f, std::function<void()>&& cb);

        public:
            /// The descripion of the skill, which will be available via the provider/manager
            const SkillDescription description;

            /// running params
            armarx::core::time::DateTime started = armarx::core::time::DateTime::Invalid();
            armarx::core::time::DateTime exited = armarx::core::time::DateTime::Invalid();

            /// proxy that called the skills. Will be set from provider and is const afterwards
            manager::dti::SkillManagerInterfacePrx manager = nullptr;

            /// the provider that owns this skill. Will be set from provider and is const afterwards
            std::string providerName = "INVALID PROVIDER NAME";

        protected:
            /// active conditions. First is condition (bool return func)
            std::vector<std::pair<std::function<bool()>, std::function<void()>>> callbacks;
            mutable std::mutex callbacksMutex;

            /// Use conditions this way
            std::atomic_bool running = false;
            std::atomic_bool stopped = false;
            std::atomic_bool timeoutReached = false;

        private:
            std::thread conditionCheckingThread; // A therad that checks the conditions frequently
            armarx::Frequency conditionCheckingThreadFrequency = armarx::Frequency::Hertz(20);
        };
    }
}
