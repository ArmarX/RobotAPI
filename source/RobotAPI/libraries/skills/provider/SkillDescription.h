#pragma once

#include <string>
#include <vector>

#include <ArmarXCore/core/time/Duration.h>

#include <RobotAPI/interface/skills/SkillProviderInterface.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>

namespace armarx
{
    namespace skills
    {
        struct SkillDescription
        {
            std::string                        skillName = "NOT INITIALIZED YET";
            std::string                        description = "NOT INITIALIZED YET";
            std::vector<std::string>           robots = {};
            armarx::core::time::Duration       timeout = armarx::core::time::Duration::MilliSeconds(-1);
            aron::type::ObjectPtr              acceptedType = nullptr;
            aron::data::DictPtr                defaultParams = nullptr;

            provider::dto::SkillDescription toIce() const;
        };
    }
}
