#pragma once

#include "Skill.h"

namespace armarx
{
    namespace skills
    {
        class LambdaSkill : public Skill
        {
        public:
            using FunT = std::function<TerminatedSkillStatus(const std::string clientId, const aron::data::DictPtr&)>;

            LambdaSkill() = delete;
            LambdaSkill(const FunT& f, const SkillDescription& desc) :
                Skill(desc),
                fun(f)
            {};

        private:
            MainResult main(const MainInput& in) override;

        private:
            FunT fun;
        };
    }
}
