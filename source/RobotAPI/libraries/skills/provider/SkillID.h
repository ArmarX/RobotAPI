#pragma once

#include <string>
#include <vector>

#include <SimoxUtility/algorithm/string.h>

#include "../error/Exception.h"

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/interface/skills/SkillProviderInterface.h>

namespace armarx
{
    namespace skills
    {
        class SkillID
        {
        public:
            static const constexpr char* PREFIX_SEPARATOR = "->";
            static const constexpr char* NAME_SEPARATOR = "/";

            std::string providerName;
            std::string skillName;

            SkillID() = delete;
            SkillID(const std::string& providerName, const std::string& skillName);

            bool operator==(const SkillID& other) const;
            bool operator<(const SkillID& other) const;

            provider::dto::SkillID toIce() const;
            std::string toString(const std::string& prefix = "") const;
        };
    }
}
