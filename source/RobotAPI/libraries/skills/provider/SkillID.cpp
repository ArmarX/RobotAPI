#include "SkillID.h"

namespace armarx
{
    namespace skills
    {
        SkillID::SkillID(const std::string& providerName, const std::string& skillName) : providerName(providerName), skillName(skillName)
        {
            if (simox::alg::contains(providerName, NAME_SEPARATOR) || simox::alg::contains(skillName, NAME_SEPARATOR))
            {
                throw error::SkillException(__PRETTY_FUNCTION__, std::string("A skill provider or a skill contains the blacklisted token '") + NAME_SEPARATOR + "'.");
            }

            if (simox::alg::contains(providerName, PREFIX_SEPARATOR) || simox::alg::contains(skillName, PREFIX_SEPARATOR))
            {
                throw error::SkillException(__PRETTY_FUNCTION__, std::string("A skill provider or a skill contains the blacklisted token '") + PREFIX_SEPARATOR + "'.");
            }
        }

        bool SkillID::operator==(const SkillID& other) const
        {
            return toString() == other.toString();
        }

        bool SkillID::operator<(const SkillID& other) const
        {
            return toString() < other.toString();
        }

        provider::dto::SkillID SkillID::toIce() const
        {
            return {providerName, skillName};
        }

        std::string SkillID::toString(const std::string& prefix) const
        {
            return (prefix.empty() ? std::string("") : (prefix + PREFIX_SEPARATOR)) + providerName + NAME_SEPARATOR + skillName;
        }
    }
}
