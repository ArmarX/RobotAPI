#pragma once

#include <string>
#include <vector>

#include <RobotAPI/interface/skills/SkillProviderInterface.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>

namespace armarx
{
    namespace skills
    {
        struct SkillParameterization
        {
            aron::data::DictPtr                                 usedInputParams = nullptr;
            callback::dti::SkillProviderCallbackInterfacePrx    usedCallbackInterface = nullptr;

            aron::data::dto::DictPtr toIce() const;
        };
    }
}
