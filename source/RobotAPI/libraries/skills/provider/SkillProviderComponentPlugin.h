#pragma once

#include <shared_mutex>
#include <queue>
#include <thread>
#include <functional>

#include <ArmarXCore/core/ComponentPlugin.h>
#include <ArmarXCore/core/ManagedIceObject.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <RobotAPI/interface/skills/SkillManagerInterface.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>

// Include all types of skills
#include "Skill.h"
#include "SpecializedSkill.h"
#include "LambdaSkill.h"
#include "PeriodicSkill.h"
#include "PeriodicSpecializedSkill.h"

// Helper wrapper for execution
#include "detail/SkillImplementationWrapper.h"

namespace armarx::plugins
{
    class SkillProviderComponentPlugin : public ComponentPlugin
    {
    public:
        using ComponentPlugin::ComponentPlugin;

        void preOnInitComponent() override;

        void preOnConnectComponent() override;
        void postOnConnectComponent() override;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

        void preOnDisconnectComponent() override;

    private:
        skills::manager::dti::SkillManagerInterfacePrx manager;
        skills::provider::dti::SkillProviderInterfacePrx myPrx;
    };
}

namespace armarx
{
    class SkillProviderComponentPluginUser :
            virtual public ManagedIceObject,
            virtual public skills::provider::dti::SkillProviderInterface
    {
    public:
        SkillProviderComponentPluginUser();

        skills::provider::dto::SkillDescription getSkillDescription(const std::string&, const Ice::Current &current = Ice::Current()) override;
        skills::provider::dto::SkillDescriptionMap getSkillDescriptions(const Ice::Current &current = Ice::Current()) override;
        skills::provider::dto::SkillStatusUpdate getSkillExecutionStatus(const std::string& skill, const Ice::Current &current = Ice::Current()) override;
        skills::provider::dto::SkillStatusUpdateMap getSkillExecutionStatuses(const Ice::Current &current = Ice::Current()) override;

        skills::provider::dto::SkillStatusUpdate executeSkill(const skills::provider::dto::SkillExecutionRequest& executionInfo, const Ice::Current &current = Ice::Current()) override;
        void abortSkill(const std::string& name, const Ice::Current &current = Ice::Current()) override;

    protected:
        void addSkill(const skills::LambdaSkill::FunT&, const skills::SkillDescription&);
        void addSkill(std::unique_ptr<skills::Skill>&&);

    private:
        armarx::plugins::SkillProviderComponentPlugin* plugin = nullptr;

    protected:
        mutable std::shared_mutex skillsMutex;

    public:
        bool connected = false;
        std::map<std::string, skills::detail::SkillImplementationWrapper> skillImplementations;
    };
}
