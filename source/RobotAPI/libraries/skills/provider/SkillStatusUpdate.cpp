#include "SkillStatusUpdate.h"

namespace armarx
{
    namespace skills
    {
        SkillStatus toSkillStatus(const ActiveOrTerminatedSkillStatus& d)
        {
            switch (d)
            {
                case ActiveOrTerminatedSkillStatus::Running:
                    return SkillStatus::Running;
                case ActiveOrTerminatedSkillStatus::Failed:
                    return SkillStatus::Failed;
                case ActiveOrTerminatedSkillStatus::Succeeded:
                    return SkillStatus::Succeeded;
                case ActiveOrTerminatedSkillStatus::Aborted:
                    return SkillStatus::Aborted;
            }
            throw error::SkillException(__PRETTY_FUNCTION__, "Should not happen!");
        }

        SkillStatus toSkillStatus(const TerminatedSkillStatus& d)
        {
            switch (d)
            {
                case TerminatedSkillStatus::Failed:
                    return SkillStatus::Failed;
                case TerminatedSkillStatus::Succeeded:
                    return SkillStatus::Succeeded;
                case TerminatedSkillStatus::Aborted:
                    return SkillStatus::Aborted;
            }
            throw error::SkillException(__PRETTY_FUNCTION__, "Should not happen!");
        }

        void toIce(provider::dto::Execution::Status& ret, const SkillStatus& status)
        {
            switch(status)
            {
                case SkillStatus::Idle:
                    ret =  provider::dto::Execution::Status::Idle;
                    return;
                case SkillStatus::Scheduled:
                    ret =  provider::dto::Execution::Status::Scheduled;
                    return;
                case SkillStatus::Running:
                    ret =  provider::dto::Execution::Status::Running;
                    return;
                case SkillStatus::Failed:
                    ret =  provider::dto::Execution::Status::Failed;
                    return;
                case SkillStatus::Succeeded:
                    ret =  provider::dto::Execution::Status::Succeeded;
                    return;
                case SkillStatus::Aborted:
                    ret =  provider::dto::Execution::Status::Aborted;
                    return;
            }
            throw error::SkillException(__PRETTY_FUNCTION__, "Should not happen!");
        }

        void toIce(provider::dto::Execution::Status& ret, const ActiveOrTerminatedSkillStatus& status)
        {
            switch(status)
            {
                case ActiveOrTerminatedSkillStatus::Running:
                    ret =  provider::dto::Execution::Status::Running;
                    return;
                case ActiveOrTerminatedSkillStatus::Failed:
                    ret =  provider::dto::Execution::Status::Failed;
                    return;
                case ActiveOrTerminatedSkillStatus::Succeeded:
                    ret =  provider::dto::Execution::Status::Succeeded;
                    return;
                case ActiveOrTerminatedSkillStatus::Aborted:
                    ret =  provider::dto::Execution::Status::Aborted;
                    return;
            }
            throw error::SkillException(__PRETTY_FUNCTION__, "Should not happen!");
        }

        void toIce(provider::dto::Execution::Status& ret, const TerminatedSkillStatus& status)
        {
            switch(status)
            {
                case TerminatedSkillStatus::Failed:
                    ret =  provider::dto::Execution::Status::Failed;
                    return;
                case TerminatedSkillStatus::Succeeded:
                    ret =  provider::dto::Execution::Status::Succeeded;
                    return;
                case TerminatedSkillStatus::Aborted:
                    ret =  provider::dto::Execution::Status::Aborted;
                    return;
            }
            throw error::SkillException(__PRETTY_FUNCTION__, "Should not happen!");
        }

        void fromIce(const provider::dto::Execution::Status& status, TerminatedSkillStatus& ret)
        {
            switch(status)
            {
                case provider::dto::Execution::Status::Idle:
                    [[fallthrough]];
                case provider::dto::Execution::Status::Scheduled:
                    [[fallthrough]];
                case provider::dto::Execution::Status::Running:
                    break;
                case provider::dto::Execution::Status::Failed:
                    ret =  TerminatedSkillStatus::Failed;
                    return;
                case provider::dto::Execution::Status::Succeeded:
                    ret =  TerminatedSkillStatus::Succeeded;
                    return;
                case provider::dto::Execution::Status::Aborted:
                    ret =  TerminatedSkillStatus::Aborted;
                    return;
            }
            throw error::SkillException(__PRETTY_FUNCTION__, "You entered an invalid execution status type to convert to a terminating status.");
        }

        void fromIce(const provider::dto::Execution::Status& status, ActiveOrTerminatedSkillStatus& ret)
        {
            switch(status)
            {
                case provider::dto::Execution::Status::Idle:
                    [[fallthrough]];
                case provider::dto::Execution::Status::Scheduled:
                    break;
                case provider::dto::Execution::Status::Running:
                    ret =  ActiveOrTerminatedSkillStatus::Running;
                    return;
                case provider::dto::Execution::Status::Failed:
                    ret =  ActiveOrTerminatedSkillStatus::Failed;
                    return;
                case provider::dto::Execution::Status::Succeeded:
                    ret =  ActiveOrTerminatedSkillStatus::Succeeded;
                    return;
                case provider::dto::Execution::Status::Aborted:
                    ret =  ActiveOrTerminatedSkillStatus::Aborted;
                    return;
            }
            throw error::SkillException(__PRETTY_FUNCTION__, "You entered an invalid execution status type to convert to a terminating status.");
        }

        void fromIce(const provider::dto::Execution::Status& status, SkillStatus& ret)
        {
            switch(status)
            {
                case provider::dto::Execution::Status::Idle:
                    ret =  SkillStatus::Idle;
                    return;
                case provider::dto::Execution::Status::Scheduled:
                    ret =  SkillStatus::Scheduled;
                    return;
                case provider::dto::Execution::Status::Running:
                    ret =  SkillStatus::Running;
                    return;
                case provider::dto::Execution::Status::Failed:
                    ret =  SkillStatus::Failed;
                    return;
                case provider::dto::Execution::Status::Succeeded:
                    ret =  SkillStatus::Succeeded;
                    return;
                case provider::dto::Execution::Status::Aborted:
                    ret =  SkillStatus::Aborted;
                    return;
            }
            throw error::SkillException(__PRETTY_FUNCTION__, "Should not happen!");
        }




        provider::dto::SkillStatusUpdate TerminatedSkillStatusUpdate::toIce() const
        {
            provider::dto::SkillStatusUpdate ret;
            ret.header.skillId = skillId.toIce();
            ret.header.executorName = executorName;
            ret.data = aron::data::Dict::ToAronDictDTO(data);
            skills::toIce(ret.header.status, status);
            ret.header.usedCallbackInterface = usedParameterization.usedCallbackInterface;
            ret.header.usedParams = usedParameterization.toIce();
            return ret;
        }

        provider::dto::SkillStatusUpdate SkillStatusUpdate::toIce() const
        {
            provider::dto::SkillStatusUpdate ret;
            ret.header.skillId = skillId.toIce();
            ret.header.executorName = executorName;
            ret.data = aron::data::Dict::ToAronDictDTO(data);
            skills::toIce(ret.header.status, status);
            ret.header.usedCallbackInterface = usedParameterization.usedCallbackInterface;
            ret.header.usedParams = usedParameterization.toIce();
            return ret;
        }

        provider::dto::SkillStatusUpdate ActiveOrTerminatedSkillStatusUpdate::toIce() const
        {
            provider::dto::SkillStatusUpdate ret;
            ret.header.skillId = skillId.toIce();
            ret.header.executorName = executorName;
            ret.data = aron::data::Dict::ToAronDictDTO(data);
            skills::toIce(ret.header.status, status);
            ret.header.usedCallbackInterface = usedParameterization.usedCallbackInterface;
            ret.header.usedParams = usedParameterization.toIce();
            return ret;
        }

        TerminatedSkillStatusUpdate TerminatedSkillStatusUpdate::FromIce(const provider::dto::SkillStatusUpdate& update)
        {
            TerminatedSkillStatusUpdate ret;
            ret.skillId = {update.header.skillId.providerName, update.header.skillId.skillName};
            ret.executorName = update.header.executorName;
            skills::fromIce(update.header.status, ret.status);
            ret.usedParameterization = {aron::data::Dict::FromAronDictDTO(update.header.usedParams), update.header.usedCallbackInterface};
            ret.data = aron::data::Dict::FromAronDictDTO(update.data);
            return ret;
        }

        SkillStatusUpdate SkillStatusUpdate::FromIce(const provider::dto::SkillStatusUpdate& update)
        {
            SkillStatusUpdate ret;
            ret.skillId = {update.header.skillId.providerName, update.header.skillId.skillName};
            ret.executorName = update.header.executorName;
            skills::fromIce(update.header.status, ret.status);
            ret.usedParameterization = {aron::data::Dict::FromAronDictDTO(update.header.usedParams), update.header.usedCallbackInterface};
            ret.data = aron::data::Dict::FromAronDictDTO(update.data);
            return ret;
        }

        ActiveOrTerminatedSkillStatusUpdate ActiveOrTerminatedSkillStatusUpdate::FromIce(const provider::dto::SkillStatusUpdate& update)
        {
            ActiveOrTerminatedSkillStatusUpdate ret;
            ret.skillId = {update.header.skillId.providerName, update.header.skillId.skillName};
            ret.executorName = update.header.executorName;
            skills::fromIce(update.header.status, ret.status);
            ret.usedParameterization = {aron::data::Dict::FromAronDictDTO(update.header.usedParams), update.header.usedCallbackInterface};
            ret.data = aron::data::Dict::FromAronDictDTO(update.data);
            return ret;
        }
    }
}
