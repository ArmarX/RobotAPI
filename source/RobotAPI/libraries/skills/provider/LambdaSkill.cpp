#include "LambdaSkill.h"

namespace armarx
{
    namespace skills
    {

        Skill::MainResult LambdaSkill::main(const MainInput& in)
        {
            TerminatedSkillStatus res = fun(in.executorName, in.params);

            return {.status = res, .data = nullptr};
        }
    }
}
