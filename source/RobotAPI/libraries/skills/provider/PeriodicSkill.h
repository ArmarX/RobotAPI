/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/time/Frequency.h>

#include "Skill.h"
#include "SpecializedSkill.h"

namespace armarx::skills
{

    class PeriodicSkill : public Skill
    {
    public:
        struct StepResult
        {
            ActiveOrTerminatedSkillStatus status;
            aron::data::DictPtr data;
        };

        PeriodicSkill(const SkillDescription& skillDescription, const armarx::Frequency& frequency);

        StepResult stepOfSkill(const MainInput& in);

    private:
        /// Do not use anymore
        Skill::MainResult main(const MainInput& in) final;

        /// Override this method with your own step function
        virtual StepResult step(const MainInput& in);

    private:
        const armarx::Frequency frequency;
    };


  
} // namespace armarx::skills
