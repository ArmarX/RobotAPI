#pragma once

#include "Skill.h"

namespace armarx
{
    namespace skills
    {
        /* Manages the remote execution of a skill and converts the ice types */
        class SkillProxy : public armarx::Logging
        {
        public:
            SkillProxy(const manager::dti::SkillManagerInterfacePrx& manager, const SkillID& skillId);
            SkillProxy(const manager::dti::SkillManagerInterfacePrx& manager, const std::string& skillProviderName, const std::string& skillName);
            SkillProxy(const manager::dti::SkillManagerInterfacePrx& manager, const std::string& skillProviderName, const SkillDescription& skillDesc);

            TerminatedSkillStatusUpdate executeFullSkill(const std::string& executorName, const aron::data::DictPtr& params = nullptr);
            IceInternal::Handle<Ice::AsyncResult> begin_executeFullSkill(const std::string& executorName, const aron::data::DictPtr& params = nullptr);

        private:
            const manager::dti::SkillManagerInterfacePrx& manager;

            const SkillID skillId;
        };
    }
}
