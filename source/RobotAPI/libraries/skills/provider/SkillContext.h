#pragma once

// ArmarX
#include <ArmarXCore/core/Component.h>

namespace armarx
{
    namespace skills
    {
        /* A base class for skill contexts. It is not required to use a context for skills but it eases the management of dependencies and properties for providers */
        class SkillContext
        {
        public:
            SkillContext() = default;

            virtual void defineProperties(const armarx::PropertyDefinitionsPtr& defs, const std::string& prefix) {};

            virtual void onInit(armarx::Component& parent) {};
            virtual void onConnected(armarx::Component& parent) {};
            virtual void onDisconnected(armarx::Component& parent) {};
            virtual void onStopped(armarx::Component& parent) {};

        private:
        };
    }
}
