#pragma once

#include <shared_mutex>

#include "../SkillDescription.h"
#include "../SkillStatusUpdate.h"
#include "../Skill.h"

#include <RobotAPI/interface/skills/SkillManagerInterface.h>

namespace armarx
{
    namespace skills
    {
        namespace detail
        {
            class SkillImplementationWrapper
            {
            public:
                // fixed values. Do not change after skill instantiation
                const std::unique_ptr<Skill> skill;

                // Current execution status. Changes during execution
                // The status also holds the used parameterization
                // skillName and providerName are const after registering the skill in a provider
                mutable std::shared_mutex skillStatusMutex;
                SkillStatusUpdate statusUpdate;

                // Task information
                mutable std::shared_mutex executingMutex;

                // ctor
                SkillImplementationWrapper(std::unique_ptr<skills::Skill>&& skill);

                // execute a skill. The parameterization is copied. T
                // the return type additionally contains the input configuration (similar to the status updates used in callbacks)
                TerminatedSkillStatusUpdate setupAndExecuteSkill(const std::string& executorName, const skills::SkillParameterization);
            };
        }
    }
}
