#pragma once

#include <string>
#include <vector>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/interface/skills/SkillProviderInterface.h>

#include "SkillID.h"
#include "SkillParameterization.h"

namespace armarx
{
    namespace skills
    {
        enum class SkillStatus
        {
            Idle = 0,
            Scheduled = 1,
            Running = 2,
            Failed = 4,
            Succeeded = 8,
            Aborted = 16
        };

        enum class ActiveOrTerminatedSkillStatus
        {
            Running = 2,
            Failed = 4,
            Succeeded = 8,
            Aborted = 16
        };

        enum class TerminatedSkillStatus
        {
            Failed = 4,
            Succeeded = 8,
            Aborted = 16
        };

        SkillStatus toSkillStatus(const ActiveOrTerminatedSkillStatus&);
        SkillStatus toSkillStatus(const TerminatedSkillStatus&);

        void toIce(provider::dto::Execution::Status& ret, const SkillStatus& status);
        void toIce(provider::dto::Execution::Status& ret, const ActiveOrTerminatedSkillStatus& status);
        void toIce(provider::dto::Execution::Status& ret, const TerminatedSkillStatus& status);

        void fromIce(const provider::dto::Execution::Status& status, TerminatedSkillStatus& ret);
        void fromIce(const provider::dto::Execution::Status& status, ActiveOrTerminatedSkillStatus& ret);
        void fromIce(const provider::dto::Execution::Status& status, SkillStatus& ret);

        struct SkillStatusUpdateBase
        {
            // header
            SkillID                                             skillId = {"NOT INITIALIZED YET", "NOT INITIALIZED YET"};
            std::string                                         executorName = "";
            SkillParameterization                               usedParameterization;

            // data
            aron::data::DictPtr                                 data = nullptr;
        };

        // Will be returned after the execution of a skill
        struct TerminatedSkillStatusUpdate : public SkillStatusUpdateBase
        {
            TerminatedSkillStatus                               status = TerminatedSkillStatus::Failed;

            provider::dto::SkillStatusUpdate toIce() const;
            static TerminatedSkillStatusUpdate FromIce(const provider::dto::SkillStatusUpdate& update);
        };

        // Will be returned from periodic skills which can still run
        struct ActiveOrTerminatedSkillStatusUpdate : public SkillStatusUpdateBase
        {
            ActiveOrTerminatedSkillStatus                       status = ActiveOrTerminatedSkillStatus::Failed;

            provider::dto::SkillStatusUpdate toIce() const;
            static ActiveOrTerminatedSkillStatusUpdate FromIce(const provider::dto::SkillStatusUpdate& update);
        };

        // Will be used as status updates from skills to the callback interface
        struct SkillStatusUpdate : public SkillStatusUpdateBase
        {
            SkillStatus                                         status = SkillStatus::Idle;

            provider::dto::SkillStatusUpdate toIce() const;
            static SkillStatusUpdate FromIce(const provider::dto::SkillStatusUpdate& update);
        };
    }
}
