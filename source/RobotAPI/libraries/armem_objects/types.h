/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Geometry>

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem_robot/types.h>


namespace armarx::armem::attachment
{
    using AgentID = armem::MemoryID;
    using ObjectID = armem::MemoryID;

    struct AgentDescription
    {
        /**
         * @brief id either pointing to a arondto::Robot or arondto::ArticulatedObject
         *
         */
        AgentID id;

        std::string frame;
    };

    /**
     * @brief ObjectAttachment describes a fixed transformation between an agent and an object.
     *
     *  The transformation is defined as follows:
     *
     *      agent.frame -> object root frame
     *
     */
    struct ObjectAttachment
    {
        AgentDescription agent;

        Eigen::Affine3f transformation;

        ObjectID object;

        armem::Time timestamp;

        bool active;
    };

    /**
     * @brief ArticulatedObjectAttachment describes a fixed transformation between an agent and an articulated object.
     *
     *  The transformation is defined as follows:
     *
     *      agent.frame -> object.frame
     *
     */
    struct ArticulatedObjectAttachment
    {
        AgentDescription agent;

        Eigen::Affine3f transformation;

        AgentDescription object;

        armem::Time timestamp;

        bool active;

    };

}  // namespace armarx::armem::attachment

namespace armarx::armem::articulated_object
{
    using ArticulatedObjectDescription = armarx::armem::robot::RobotDescription;

    using ArticulatedObject  = armarx::armem::robot::Robot;
    using ArticulatedObjects = armarx::armem::robot::Robots;
} // namespace armarx::armem::articulated_object
