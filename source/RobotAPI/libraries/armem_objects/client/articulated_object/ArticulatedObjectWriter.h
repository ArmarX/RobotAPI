

#pragma once

#include <VirtualRobot/VirtualRobot.h>

#include "Writer.h"

namespace armarx::armem::articulated_object
{

    class ArticulatedObjectWriter : virtual public Writer
    {
    public:
        using Writer::Writer;

        bool
        storeArticulatedObject(const VirtualRobot::RobotPtr& articulatedObject,
                               const armem::Time& timestamp);
    };
} // namespace armarx::armem::articulated_object
