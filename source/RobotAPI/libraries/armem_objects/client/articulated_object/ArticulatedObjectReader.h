#pragma once

#include "Reader.h"

namespace armarx::armem::articulated_object
{

    class ArticulatedObjectReader : virtual public Reader
    {
    public:
        using Reader::Reader;

        VirtualRobot::RobotPtr getArticulatedObject(
                const std::string& name,
                const armem::Time& timestamp);
    };
} // namespace armarx::armem::articulated_object
