#include "Reader.h"

#include <mutex>
#include <optional>

#include <Eigen/Geometry>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectInfo.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>
#include <RobotAPI/libraries/ArmarXObjects/aron_conversions/objpose.h>
#include <RobotAPI/libraries/armem_objects/aron_conversions.h>
#include <ArmarXCore/core/PackagePath.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/libraries/ArmarXObjects/aron/ObjectPose.aron.generated.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem_robot/aron/Robot.aron.generated.h>
#include <RobotAPI/libraries/armem_robot/aron_conversions.h>
#include <RobotAPI/libraries/armem_robot/robot_conversions.h>

#include "utils.h"

namespace fs = ::std::filesystem;

namespace armarx::armem::articulated_object
{

    Reader::Reader(armem::client::MemoryNameSystem& memoryNameSystem) :
        memoryNameSystem(memoryNameSystem)
    {
    }

    void
    Reader::registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def)
    {
        ARMARX_DEBUG << "Reader: registerPropertyDefinitions";

        const std::string prefix = propertyPrefix;

        def->optional(properties.memoryName, prefix + "MemoryName");

        def->optional(properties.coreInstanceSegmentName,
                      prefix + "CoreSegment",
                      "Name of the memory core segment to use for object instances.");
        def->optional(properties.coreClassSegmentName,
                      prefix + "CoreSegment",
                      "Name of the memory core segment to use for object classes.");
        def->optional(properties.providerName, prefix + "read.ProviderName");
    }

    void
    Reader::connect()
    {
        // Wait for the memory to become available and add it as dependency.
        ARMARX_IMPORTANT << "Reader: Waiting for memory '" << properties.memoryName << "' ...";
        try
        {
            memoryReader = memoryNameSystem.useReader(properties.memoryName);
            ARMARX_IMPORTANT << "Reader: Connected to memory '" << properties.memoryName << "'";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }

        armem::MemoryID id = armem::MemoryID();
        id.memoryName = properties.memoryName;
        id.coreSegmentName = properties.coreClassSegmentName;
        // listen to all provider segments!

        memoryNameSystem.subscribe(id, this, &Reader::updateKnownObjects);
    }

    void
    Reader::updateKnownObject(const armem::MemoryID& snapshotId)
    {
        // const std::string& nameWithDataset = snapshotId.providerSegmentName;

        // arondto::RobotDescription aronArticulatedObjectDescription;
        // aronArticulatedObjectDescription.fromAron(snapshotId.);

        // TODO(fabian.reister): implement
    }

    void
    Reader::updateKnownObjects(const armem::MemoryID& subscriptionID,
                               const std::vector<armem::MemoryID>& snapshotIDs)
    {
        ARMARX_INFO << "New objects available!";

        // // Query all entities from provider.
        // armem::client::query::Builder qb;

        // // clang-format off
        // qb
        // .coreSegments().withName(properties.coreClassSegmentName)
        // .providerSegments().all() // TODO(fabian.reister): think about this: which authority is trustworthy?
        // .entities().withName(name)
        // .snapshots().atTime(timestamp);
        // // clang-format on

        // const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

        // std::for_each(snapshotIDs.begin(), snapshotIDs.end(), [&](const auto & snapshotID)
        // {
        //     updateKnownObject(snapshotID);
        // });
    }

    std::optional<ArticulatedObject>
    Reader::get(const std::string& name, const armem::Time& timestamp)
    {
        const auto splits = simox::alg::split(name, "/");
        ARMARX_CHECK_EQUAL(splits.size(), 3) << "`name` must be of form `DATASET/NAME/INSTANCE`";

        const std::string className = splits.at(0) + "/" + splits.at(1); // `DATASET/NAME`
        const std::string instanceName = splits.at(2);

        const auto description = queryDescription(name, timestamp);

        if (not description)
        {
            ARMARX_WARNING << "Unknown object " << name;
            return std::nullopt;
        }

        return get(*description, timestamp, instanceName);
    }

    ArticulatedObject
    Reader::get(const ArticulatedObjectDescription& description,
                const armem::Time& timestamp,
                const std::string& instanceName)
    {
        ArticulatedObject obj{.description = description,
                              .instance = instanceName,
                              .config = {}, // will be populated by `synchronize()`
                              .timestamp = timestamp};

        synchronize(obj, timestamp);

        return obj;
    }

    bool
    Reader::synchronize(ArticulatedObject& obj, const armem::Time& timestamp)
    {
        ARMARX_CHECK_NOT_EMPTY(obj.instance) << "An instance name must be provided!";

        auto state = queryState(obj.name(), timestamp);

        if (not state) /* c++20 [[unlikely]] */
        {
            ARMARX_WARNING << "Could not synchronize object " << obj.instance;
            return false;
        }

        obj.config = std::move(*state);
        return true;
    }

    std::vector<robot::RobotDescription>
    Reader::queryDescriptions(const armem::Time& timestamp)
    {
        // Query all entities from provider.
        armem::client::query::Builder qb;

        // clang-format off
        qb
        .coreSegments().withName(properties.coreClassSegmentName)
        .providerSegments().all()
        .entities().all()
        .snapshots().latest(); // TODO beforeTime(timestamp);
        // clang-format on

        const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success) /* c++20 [[unlikely]] */
        {
            return {};
        }

        return getRobotDescriptions(qResult.memory);
    }

    std::string
    Reader::getProviderName() const
    {
        return properties.providerName;
    }

    void
    Reader::setProviderName(const std::string& providerName)
    {
        this->properties.providerName = providerName;
    }

    std::optional<robot::RobotDescription>
    Reader::queryDescription(const std::string& name, const armem::Time& timestamp)
    {
        // FIXME: why is `name` unused?

        // Query all entities from provider.
        armem::client::query::Builder qb;

        // clang-format off
        qb
        .coreSegments().withName(properties.coreClassSegmentName)
        .providerSegments().withName(properties.providerName)
        .entities().all() // withName(name)
        .snapshots().latest();
        // clang-format on

        const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success) /* c++20 [[unlikely]] */
        {
            ARMARX_WARNING << qResult.errorMessage;
            return std::nullopt;
        }

        return getRobotDescription(qResult.memory);
    }

    std::optional<robot::RobotState>
    Reader::queryState(const std::string& instanceName, const armem::Time& timestamp)
    {
        // TODO(fabian.reister): how to deal with multiple providers?

        // Query all entities from provider.
        armem::client::query::Builder qb;

        // clang-format off
        qb
        .coreSegments().withName(properties.coreInstanceSegmentName)
        .providerSegments().all() // withName(properties.providerName) // agent
        .entities().withName(instanceName)
        .snapshots().latest();
        // clang-format on

        const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success) /* c++20 [[unlikely]] */
        {
            return std::nullopt;
        }

        return getArticulatedObjectState(qResult.memory);
    }


    std::optional<robot::RobotState>
    convertToRobotState(const armem::wm::EntityInstance& instance)
    {
        armarx::armem::arondto::ObjectInstance aronObjectInstance;
        try
        {
            aronObjectInstance.fromAron(instance.data());
        }
        catch (...)
        {
            ARMARX_WARNING << "Conversion to ObjectInstance failed!";
            return std::nullopt;
        }

        objpose::ObjectPose objectPose;
        objpose::fromAron(aronObjectInstance.pose, objectPose);

        robot::RobotState robotState{.timestamp = objectPose.timestamp,
                                     .globalPose = Eigen::Affine3f(objectPose.objectPoseGlobal),
                                     .jointMap = objectPose.objectJointValues};

        return robotState;
    }

    std::optional<robot::RobotState>
    Reader::getArticulatedObjectState(const armarx::armem::wm::Memory& memory) const
    {
        // clang-format off
        const armem::wm::CoreSegment& coreSegment = memory
                .getCoreSegment(properties.coreInstanceSegmentName);
        // clang-format on

        std::optional<wm::EntityInstance> instance;
        coreSegment.forEachInstance([&instance](const wm::EntityInstance& i) { instance = i; });

        if (instance.has_value())
        {
            return convertToRobotState(instance.value());
            // return robot::convertRobotState(instance.value());
        }

        ARMARX_FATAL << "Failed to obtain robot state";
        return std::nullopt;
    }


    std::optional<robot::RobotDescription>
    Reader::getRobotDescription(const armarx::armem::wm::Memory& memory) const
    {
        // clang-format off
        const armem::wm::CoreSegment& coreSegment = memory.getCoreSegment(properties.coreClassSegmentName);
        // clang-format on

        std::optional<wm::EntityInstance> instance;
        coreSegment.forEachInstance([&instance](const wm::EntityInstance& i) { instance = i; });

        if (instance.has_value())
        {
            return convertRobotDescription(instance.value());
        }

        ARMARX_DEBUG << "No robot description";
        return std::nullopt;
    }


    std::vector<robot::RobotDescription>
    Reader::getRobotDescriptions(const armarx::armem::wm::Memory& memory) const
    {
        const armem::wm::CoreSegment& coreSegment =
            memory.getCoreSegment(properties.coreClassSegmentName);

        std::vector<robot::RobotDescription> descriptions;
        coreSegment.forEachEntity(
            [&descriptions](const wm::Entity& entity)
            {
                if (not entity.empty())
                {
                    const auto robotDescription =
                        convertRobotDescription(entity.getFirstSnapshot().getInstance(0));
                    if (robotDescription)
                    {
                        descriptions.push_back(*robotDescription);
                    }
                }
            });

        return descriptions;
    }

} // namespace armarx::armem::articulated_object
