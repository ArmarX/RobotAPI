/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <optional>

#include <ArmarXCore/core/application/properties/forward_declarations.h>

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client/Writer.h>

#include "interfaces.h"


namespace armarx::armem::articulated_object
{

    class Writer:
        virtual public WriterInterface
    {
    public:
        Writer(armem::client::MemoryNameSystem& memoryNameSystemopti);
        virtual ~Writer() = default;

        void registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def);
        void connect();


        bool store(const ArticulatedObject& obj) override;

        bool storeInstance(const ArticulatedObject& obj);
        std::optional<armem::MemoryID> storeClass(const ArticulatedObject& obj);

        // const std::string& getPropertyPrefix() const override;

        std::string getProviderName() const;
        void setProviderName(const std::string& providerName);


    private:

        std::optional<armem::MemoryID> storeOrGetClass(const ArticulatedObject& obj);

        void updateKnownObjects(const armem::MemoryID& subscriptionID, const std::vector<armem::MemoryID>& snapshotIDs);
        void updateKnownObjects();
        void updateKnownObject(const armem::MemoryID& snapshotId);

        // TODO duplicate
        std::unordered_map<std::string, armem::MemoryID> queryDescriptions(const armem::Time& timestamp);
        std::optional<robot::RobotDescription> getRobotDescription(const armarx::armem::wm::Memory& memory) const;
        std::unordered_map<std::string, armem::MemoryID> getRobotDescriptions(const armarx::armem::wm::Memory& memory) const;


        struct Properties
        {
            std::string memoryName              = "Object";
            std::string coreInstanceSegmentName = "Instance";
            std::string coreClassSegmentName    = "Class";
            std::string providerName            = "";

            bool allowClassCreation             = false;
        } properties;

        const std::string propertyPrefix = "mem.obj.articulated.";

        armem::client::MemoryNameSystem& memoryNameSystem;

        armem::client::Writer memoryWriter;
        std::mutex memoryWriterMutex;

        armem::client::Reader memoryReader;
        std::mutex memoryReaderMutex;

        // key: name of object: RobotDescription::name
        std::unordered_map<std::string, MemoryID> knownObjects;
    };


}  // namespace armarx::armem::articulated_object
