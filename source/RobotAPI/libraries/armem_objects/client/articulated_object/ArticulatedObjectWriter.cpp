#include "ArticulatedObjectWriter.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <VirtualRobot/Robot.h>


namespace armarx::armem::articulated_object
{
    armem::articulated_object::ArticulatedObject convert(
            const VirtualRobot::Robot& obj,
            const armem::Time& timestamp)
    {
        ARMARX_DEBUG << "Filename is " << obj.getFilename();

        // TODO(fabian.reister): remove "PriorKnowledgeData" below

        return armem::articulated_object::ArticulatedObject
        {
            .description = {
                .name = obj.getType(),
                .xml  = PackagePath(armarx::ArmarXDataPath::getProject(
                {"PriorKnowledgeData"}, obj.getFilename()),
                obj.getFilename())
            },
            .instance    = obj.getName(),
            .config      = {
                .timestamp  = timestamp,
                .globalPose = Eigen::Affine3f(obj.getRootNode()->getGlobalPose()),
                .jointMap   = obj.getJointValues()
            },
            .timestamp   = timestamp};
    }

    bool
    ArticulatedObjectWriter::storeArticulatedObject(
            const VirtualRobot::RobotPtr& articulatedObject,
            const armem::Time& timestamp)
    {

        ARMARX_CHECK_NOT_NULL(articulatedObject);

        armarx::armem::articulated_object::ArticulatedObject armemArticulatedObject =
            convert(*articulatedObject, Time::Now());

        return store(armemArticulatedObject);
    }
} // namespace armarx::armem::articulated_object
