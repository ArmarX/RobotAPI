#include "utils.h"

#include <RobotAPI/libraries/armem_robot/aron_conversions.h>

#include <ArmarXCore/core/logging/Logging.h>

namespace armarx::armem::articulated_object
{
    std::optional<robot::RobotDescription>
    convertRobotDescription(const armem::wm::EntityInstance& instance)
    {

        arondto::ObjectClass aronObjectInfo;
        try
        {
            aronObjectInfo.fromAron(instance.data());
        }
        catch (...)
        {
            ARMARX_WARNING << "Conversion to ObjectPose failed!";
            return std::nullopt;
        }

        robot::RobotDescription robotDescription;
        fromAron(aronObjectInfo, robotDescription);

        // check if robot description is valid
        const auto xml = robotDescription.xml.serialize();
        if (robotDescription.name.empty() or xml.package.empty() or xml.path.empty())
        {
            return std::nullopt;
        }

        return robotDescription;
    }
    
} // namespace armarx::armem::articulated_object
