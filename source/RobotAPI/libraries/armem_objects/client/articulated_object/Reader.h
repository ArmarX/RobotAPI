/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <optional>

#include <VirtualRobot/VirtualRobot.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Reader.h>

#include "interfaces.h"

namespace armarx::armem::articulated_object
{

    class Reader : virtual public ReaderInterface
    {
    public:
        Reader(armem::client::MemoryNameSystem& memoryNameSystem);
        virtual ~Reader() = default;

        void registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def);
        void connect();

        bool synchronize(ArticulatedObject& obj, const armem::Time& timestamp) override;

        std::optional<ArticulatedObject> get(const std::string& name,
                                             const armem::Time& timestamp) override;
        ArticulatedObject get(const ArticulatedObjectDescription& description,
                              const armem::Time& timestamp, const std::string& instanceName) override;

        std::optional<robot::RobotState> queryState(const std::string &instanceName,
                const armem::Time& timestamp);
        std::optional<robot::RobotDescription> queryDescription(const std::string& name,
                const armem::Time& timestamp);

        std::vector<robot::RobotDescription> queryDescriptions(const armem::Time& timestamp);

        std::string getProviderName() const;
        void setProviderName(const std::string& providerName);

        // TODO(fabian.reister): register property defs

    protected:
        std::optional<robot::RobotState>
        getArticulatedObjectState(const armarx::armem::wm::Memory& memory) const;
        std::optional<robot::RobotDescription>
        getRobotDescription(const armarx::armem::wm::Memory& memory) const;
        std::vector<robot::RobotDescription>
        getRobotDescriptions(const armarx::armem::wm::Memory& memory) const;

    private:
        void updateKnownObjects(const armem::MemoryID& subscriptionID,
                                const std::vector<armem::MemoryID>& snapshotIDs);
        void updateKnownObject(const armem::MemoryID& snapshotId);

        struct Properties
        {
            std::string memoryName              = "Object";
            std::string coreInstanceSegmentName = "Instance";
            std::string coreClassSegmentName    = "Class";
            std::string providerName            = "PriorKnowledgeData";
        } properties;

        const std::string propertyPrefix = "mem.obj.articulated.";

        armem::client::MemoryNameSystem& memoryNameSystem;

        armem::client::Reader memoryReader;
        std::mutex memoryWriterMutex;
    };


} // namespace armarx::armem::articulated_object
