#include "Writer.h"

#include <SimoxUtility/algorithm/get_map_keys_values.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectInfo.h>
#include <RobotAPI/libraries/ArmarXObjects/aron/ObjectID.aron.generated.h>
#include <RobotAPI/libraries/armem/client/query.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/aron_conversions.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/operations.h>
#include <RobotAPI/libraries/armem_objects/aron/ObjectClass.aron.generated.h>
#include <RobotAPI/libraries/armem_objects/aron/ObjectInstance.aron.generated.h>
#include <RobotAPI/libraries/armem_robot/aron/Robot.aron.generated.h>
#include <RobotAPI/libraries/armem_robot/aron/RobotDescription.aron.generated.h>
#include <RobotAPI/libraries/armem_robot/aron_conversions.h>
#include <RobotAPI/libraries/armem_robot/robot_conversions.h>

#include "utils.h"


namespace armarx::armem::articulated_object
{
    Writer::Writer(armem::client::MemoryNameSystem& memoryNameSystem) :
        memoryNameSystem(memoryNameSystem)
    {
    }

    void Writer::registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def)
    {
        ARMARX_DEBUG << "Writer: registerPropertyDefinitions";

        const std::string prefix = propertyPrefix;

        def->optional(properties.memoryName, prefix + "MemoryName");

        def->optional(properties.coreInstanceSegmentName,
                      prefix + "CoreSegment",
                      "Name of the memory core segment to use for object instances.");
        def->optional(properties.coreClassSegmentName,
                      prefix + "CoreSegment",
                      "Name of the memory core segment to use for object classes.");

        ARMARX_IMPORTANT << "Writer: add property '" << prefix << "ProviderName'";
        def->required(properties.providerName, prefix + "write.ProviderName", "Name of this provider");
    }

    void Writer::connect()
    {
        // Wait for the memory to become available and add it as dependency.
        ARMARX_IMPORTANT << "Writer: Waiting for memory '" << properties.memoryName << "' ...";
        try
        {
            memoryWriter = memoryNameSystem.useWriter(properties.memoryName);
            memoryReader = memoryNameSystem.useReader(properties.memoryName);
            ARMARX_IMPORTANT << "Writer: Connected to memory '" << properties.memoryName << "'";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }

        const auto resultCoreClassSegment =
            memoryWriter.addSegment(properties.coreClassSegmentName, properties.providerName);

        const auto resultCoreInstanceSegmentName =
            memoryWriter.addSegment(properties.coreInstanceSegmentName, properties.providerName);

        armem::MemoryID refId = armem::MemoryID(resultCoreClassSegment.segmentID);

        armem::MemoryID id;
        id.setCoreSegmentID(refId); // listen to all provider segments!

        updateKnownObjects();
        memoryNameSystem.subscribe(id, this, &Writer::updateKnownObjects);
    }

    void Writer::updateKnownObject(const armem::MemoryID& snapshotId)
    {
        arondto::RobotDescription aronArticulatedObjectDescription;
        // aronArticulatedObjectDescription.fromAron(snapshotId.ent);

        // TODO(fabian.reister): implement
    }

    void Writer::updateKnownObjects(const armem::MemoryID& subscriptionID,
                                    const std::vector<armem::MemoryID>& snapshotIDs)
    {
        ARMARX_INFO << "New objects available!";
        updateKnownObjects();
    }

    void Writer::updateKnownObjects()
    {
        knownObjects = queryDescriptions(Time::Now());

        ARMARX_INFO << "Known articulated objects " << simox::alg::get_keys(knownObjects);
    }

    std::optional<armem::MemoryID> Writer::storeOrGetClass(const ArticulatedObject& obj)
    {
        ARMARX_TRACE;

        const auto objectId = knownObjects.find(obj.description.name);

        // check if exists
        if (objectId != knownObjects.end())
        {
            return objectId->second;
        }

        throw LocalException("articulated object class " + obj.description.name + " not found");

        // otherwise create
        if (properties.allowClassCreation)
        {
            return storeClass(obj);
        }

        return std::nullopt;
    }

    std::optional<armem::MemoryID> Writer::storeClass(const ArticulatedObject& obj)
    {
        std::lock_guard g{memoryWriterMutex};

        ARMARX_DEBUG << "Trying to create core segment + provider segment";

        // TODO(fabian.reister): variable provider segment
        const auto result =
            memoryWriter.addSegment(properties.coreClassSegmentName, properties.providerName);

        if (not result.success)
        {
            ARMARX_ERROR << "Creating core segment failed. Reason: " << result.errorMessage;
            return std::nullopt;
        }

        const auto& timestamp = obj.timestamp;

        const auto providerId = armem::MemoryID(result.segmentID);
        const auto entityID =
            providerId.withEntityName(obj.description.name).withTimestamp(timestamp);

        armem::EntityUpdate update;
        update.entityID = entityID;

        arondto::RobotDescription aronArticulatedObjectDescription;
        toAron(aronArticulatedObjectDescription, obj.description);

        update.instancesData = {aronArticulatedObjectDescription.toAron()};
        update.timeCreated   = timestamp;

        ARMARX_DEBUG << "Committing " << update << " at time " << timestamp;
        armem::EntityUpdateResult updateResult = memoryWriter.commit(update);

        ARMARX_DEBUG << updateResult;

        if (not updateResult.success)
        {
            ARMARX_ERROR << updateResult.errorMessage;
            return std::nullopt;
        }

        // update cache (TODO: likely remove this)
        knownObjects[obj.description.name] = updateResult.snapshotID;

        return updateResult.snapshotID;
    }

    std::string Writer::getProviderName() const
    {
        return properties.providerName;
    }

    void Writer::setProviderName(const std::string& providerName)
    {
        this->properties.providerName = providerName;
    }

    bool Writer::storeInstance(const ArticulatedObject& obj)
    {
        std::lock_guard g{memoryWriterMutex};

        const auto& timestamp = obj.timestamp;

        ARMARX_CHECK(not obj.instance.empty()) << "An object instance name must be provided!";
        const std::string entityName = obj.description.name + "/" + obj.instance;

        ARMARX_DEBUG << "Storing articulated object instance '" << entityName << "' (provider '" << properties.providerName << "')";

        const auto providerId = armem::MemoryID()
                                .withMemoryName(properties.memoryName)
                                .withCoreSegmentName(properties.coreInstanceSegmentName)
                                .withProviderSegmentName(properties.providerName);

        armem::EntityUpdate update;
        update.entityID = providerId.withEntityName(entityName);
        // .withTimestamp(timestamp); // You only need to specify the entity ID, not the snapshot ID

        // arondto::Robot aronArticulatedObject;
        // robot::toAron(aronArticulatedObject, obj);

        arondto::ObjectInstance objectInstance;
        toAron(objectInstance, obj.config);

        const auto classId = storeOrGetClass(obj);

        if (not classId)
        {
            ARMARX_WARNING << "Could not get class for object " << obj.description.name;
            return false;
        }

        // install memory link
        toAron(objectInstance.classID, *classId);

        armem::MemoryID id;
        id.setEntityID(classId->getEntityID());

        armarx::ObjectID objectId(id.entityName);

        armarx::arondto::ObjectID cs;
        cs.className = objectId.className();
        cs.instanceName = objectId.instanceName();
        cs.dataset = objectId.dataset();

        objectInstance.pose.objectID = cs;

        update.instancesData = {objectInstance.toAron()};
        update.timeCreated   = timestamp;

        ARMARX_DEBUG << "Committing " << update << " at time " << timestamp;
        armem::EntityUpdateResult updateResult = memoryWriter.commit(update);

        ARMARX_DEBUG << updateResult;

        if (not updateResult.success)
        {
            ARMARX_WARNING << updateResult.errorMessage;
        }

        return updateResult.success;
    }

    bool Writer::store(const ArticulatedObject& obj)
    {
        const std::optional<armem::MemoryID> classId = storeOrGetClass(obj);

        if (not classId)
        {
            ARMARX_WARNING << "Could not get class id for object " << obj.description.name << "! "
                           << "Known classes are " << simox::alg::get_keys(knownObjects);
            return false;
        }

        return storeInstance(obj);
    }

    // TODO this is a duplicate
    std::optional<robot::RobotDescription>
    Writer::getRobotDescription(const armarx::armem::wm::Memory& memory) const
    {
        // clang-format off
        const armem::wm::ProviderSegment& providerSegment = memory
                .getCoreSegment(properties.coreClassSegmentName)
                .getProviderSegment(properties.providerName); // TODO(fabian.reister): all
        // clang-format on

        if (const armem::wm::EntityInstance* instance = findFirstInstance(providerSegment))
        {
            return convertRobotDescription(*instance);
        }
        else
        {
            ARMARX_WARNING << "No entity snapshots found";
            return std::nullopt;
        }
    }

    std::unordered_map<std::string, armem::MemoryID>
    Writer::getRobotDescriptions(const armarx::armem::wm::Memory& memory) const
    {
        const armem::wm::CoreSegment& coreSegment =
            memory.getCoreSegment(properties.coreClassSegmentName);

        std::unordered_map<std::string, armem::MemoryID> descriptions;
        coreSegment.forEachEntity([&descriptions](const wm::Entity & entity)
        {
            if (entity.empty())
            {
                ARMARX_WARNING << "No entity found";
                return true;
            }

            const armem::wm::EntitySnapshot& sn = entity.getFirstSnapshot();
            if (const auto robotDescription = convertRobotDescription(sn.getInstance(0)))
            {
                const armem::MemoryID snapshotID(sn.id());
                descriptions.insert({robotDescription->name, snapshotID});
            }
            return true;
        });

        return descriptions;
    }

    std::unordered_map<std::string, armem::MemoryID>
    Writer::queryDescriptions(const armem::Time& timestamp)
    {
        // Query all entities from provider.
        armem::client::query::Builder qb;

        // clang-format off
        qb
        .coreSegments().withName(properties.coreClassSegmentName)
        .providerSegments().all()
        .entities().all()
        .snapshots().latest(); // TODO beforeTime(timestamp);
        // clang-format on

        const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success) /* c++20 [[unlikely]] */
        {
            return {};
        }

        return getRobotDescriptions(qResult.memory);
    }

} // namespace armarx::armem::articulated_object
