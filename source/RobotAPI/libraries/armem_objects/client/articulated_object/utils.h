#pragma once

#include <optional>

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem_robot/types.h>
#include <RobotAPI/libraries/armem_objects/aron/ObjectClass.aron.generated.h>


namespace armarx::armem::articulated_object
{

    std::optional<robot::RobotDescription>
    convertRobotDescription(const armem::wm::EntityInstance& instance);

} // namespace armarx::armem::articulated_object
