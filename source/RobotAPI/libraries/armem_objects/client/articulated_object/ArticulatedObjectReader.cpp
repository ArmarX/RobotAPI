#include "ArticulatedObjectReader.h"

#include <mutex>
#include <optional>

#include <VirtualRobot/XML/RobotIO.h>

#include <ArmarXCore/core/PackagePath.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectInfo.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>
#include <RobotAPI/libraries/armem_objects/aron_conversions.h>
#include <RobotAPI/libraries/ArmarXObjects/aron/ObjectPose.aron.generated.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem_robot/aron/Robot.aron.generated.h>
#include <RobotAPI/libraries/armem_robot/aron_conversions.h>
#include <RobotAPI/libraries/armem_robot/robot_conversions.h>

namespace armarx::armem::articulated_object
{

    VirtualRobot::RobotPtr ArticulatedObjectReader::getArticulatedObject(
            const std::string& name,
            const armem::Time& timestamp)
    {
        const auto descriptions = queryDescriptions(armem::Time::Now());

        ARMARX_INFO << "Found " << descriptions.size() << " articulated object descriptions";

        const auto it = std::find_if(
                            descriptions.begin(),
                            descriptions.end(),
                            [&](const armem::articulated_object::ArticulatedObjectDescription & desc) -> bool
        { return desc.name == name; });

        if (it == descriptions.end())
        {
            ARMARX_WARNING << "Articulated object " << name << " not (yet) available";
            return nullptr;
        }

        ARMARX_DEBUG << "Object " << name << " available";

        auto obj = VirtualRobot::RobotIO::loadRobot(
                    ArmarXDataPath::resolvePath(it->xml.serialize().path),
                    VirtualRobot::RobotIO::eStructure);

        if (not obj)
        {
            ARMARX_WARNING << "Failed to load object: " << name;
            return nullptr;
        }

        obj->setName("");
        obj->setType(it->name);

        return obj;
    }

} // namespace armarx::armem::articulated_object
