#pragma once


#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem_objects/types.h>

namespace armarx::armem::articulated_object
{
    class ReaderInterface
    {
    public:
        virtual ~ReaderInterface() = default;

        virtual bool synchronize(ArticulatedObject& obj, const armem::Time& timestamp) = 0;

        virtual ArticulatedObject get(const ArticulatedObjectDescription& description, const armem::Time& timestamp, const std::string& instanceName) = 0;
        virtual std::optional<ArticulatedObject> get(const std::string& name, const armem::Time& timestamp) = 0;
    };


    class WriterInterface
    {
    public:
        virtual ~WriterInterface() = default;

        virtual bool store(const ArticulatedObject& obj) = 0;
    };

}  // namespace armarx::armem::articulated_object
