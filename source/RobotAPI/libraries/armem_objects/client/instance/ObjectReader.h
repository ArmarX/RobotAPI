/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>
#include <optional>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>

#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem_objects/types.h>

#include <RobotAPI/libraries/armem_objects/aron/ObjectInstance.aron.generated.h>

// The object pose provider. As long as the provider is not connected to armem we need the second proxy
#include <RobotAPI/interface/objectpose/ObjectPoseProvider.h>

namespace armarx::armem::obj::instance
{
    class Reader
    {
    public:
        Reader(armem::client::MemoryNameSystem& memoryNameSystem, const objpose::ObjectPoseProviderPrx& objPoseProvider);
        virtual ~Reader() = default;

        void registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def);
        void connect();

        // localization stuff
        bool requestLocalization(const std::string& entityName, const armarx::core::time::Duration& until);

        // query existing instances from the memory
        std::optional<armem::arondto::ObjectInstance> queryObject(const armem::wm::Memory& memory, const armem::Time&);
        std::optional<armem::arondto::ObjectInstance> queryObjectByEntityID(const std::string& entityName, const armem::Time&);
        std::optional<armem::arondto::ObjectInstance> queryObjectByObjectID(const std::string& objectId, const armem::Time&);

        // return the dataset/class, e.g. Kitchen/greencup in Kitchen/greencup/0
        static std::string GetObjectId(const std::string& s)
        {
            auto split = simox::alg::split(s, "/");
            if (simox::alg::starts_with(s, "/"))
            {
                split.insert(split.begin(), ""); // sanitize
            }

            for (auto& e : split)
            {
                e = simox::alg::replace_all(e, "/", "");
            }

            if (IsEntityId(s)) return (split[0] + "/" + split[1]);
            if (IsObjectId(s)) return s;

            ARMARX_ERROR << "Unknown structure for object id '" << s << "'.";
            return "";
        }

        // return all parts of the entity name
        static std::tuple<std::string, std::string, std::string> GetEntityNameParts(const std::string& s)
        {
            std::string dataset = "";
            std::string clazz = "";
            std::string instance = "";

            auto split = simox::alg::split(s, "/");
            if (simox::alg::starts_with(s, "/"))
            {
                split.insert(split.begin(), ""); // sanitize
            }

            for (auto& e : split)
            {
                e = simox::alg::replace_all(e, "/", "");
            }

            if (split.size() > 0)
            {
                dataset = split[0];
            }
            if (split.size() > 1)
            {
                clazz = split[1];
            }
            if (split.size() > 2)
            {
                instance = split[2];
            }
            return {dataset, clazz, instance};
        }

        // return the class, e.g. greencup in Kitchen/greencup/0
        static std::string GetObjectClassName(const std::string& s)
        {
            auto split = simox::alg::split(s, "/");
            if (simox::alg::starts_with(s, "/"))
            {
                split.insert(split.begin(), ""); // sanitize
            }

            for (auto& e : split)
            {
                e = simox::alg::replace_all(e, "/", "");
            }

            if (IsEntityId(s)) return split[1];
            if (IsObjectId(s)) return split[1];

            ARMARX_ERROR << "Unknown structure for object id '" << s << "'.";
            return "";
        }

        // check if s matches ??/??/??
        static bool IsEntityId(const std::string& s)
        {
            auto split = simox::alg::split(s, "/");
            if (simox::alg::starts_with(s, "/"))
            {
                split.insert(split.begin(), ""); // sanitize
            }

            if (split.size() != 3)
            {
                return false;
            }
            return true;
        }

        // check if s matches ??/??
        static bool IsObjectId(const std::string& s)
        {
            auto split = simox::alg::split(s, "/");
            if (simox::alg::starts_with(s, "/"))
            {
                split.insert(split.begin(), ""); // sanitize
            }

            if (split.size() != 2)
            {
                return false;
            }
            return true;
        }

    private:

        struct Properties
        {
            std::string memoryName                  = "Object";
            std::string coreSegmentName             = "Instance";
        } properties;

        const std::string propertyPrefix = "mem.obj.instance.";

        armem::client::MemoryNameSystem& memoryNameSystem;
        objpose::ObjectPoseProviderPrx objPoseProvider;

        armem::client::Reader memoryReader;
        mutable std::mutex memoryWriterMutex;
    };


}  // namespace armarx::armem::attachment
