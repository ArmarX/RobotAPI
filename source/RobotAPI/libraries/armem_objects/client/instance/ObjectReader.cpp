#include "ObjectReader.h"

#include <mutex>
#include <optional>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/PackagePath.h>

#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem/core/error.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/util/util.h>
#include <RobotAPI/libraries/armem_robot/robot_conversions.h>
#include <RobotAPI/libraries/armem_robot/aron_conversions.h>
#include <RobotAPI/libraries/armem_robot/aron/Robot.aron.generated.h>
#include <RobotAPI/libraries/armem_objects/aron/Attachment.aron.generated.h>
#include <RobotAPI/libraries/armem_objects/aron_conversions.h>
#include <RobotAPI/libraries/aron/common/aron_conversions.h>


namespace armarx::armem::obj::instance
{
    Reader::Reader(armem::client::MemoryNameSystem& memoryNameSystem, const objpose::ObjectPoseProviderPrx& objPoseProvider) :
        memoryNameSystem(memoryNameSystem), objPoseProvider(objPoseProvider)
    {}

    void Reader::registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def)
    {
        ARMARX_DEBUG << "Reader: registerPropertyDefinitions";

        const std::string prefix = propertyPrefix;

        def->optional(properties.memoryName, prefix + "MemoryName");

        def->optional(properties.coreSegmentName,
                      prefix + "CoreSegment",
                      "Name of the memory core segment to use for object instances.");
    }


    void Reader::connect()
    {
        // Wait for the memory to become available and add it as dependency.
        ARMARX_IMPORTANT << "Reader: Waiting for memory '" << properties.memoryName << "' ...";
        try
        {
            memoryReader = memoryNameSystem.useReader(properties.memoryName);
            ARMARX_IMPORTANT << "Reader: Connected to memory '" << properties.memoryName << "'";
        }
        catch (const armem::error::CouldNotResolveMemoryServer& e)
        {
            ARMARX_ERROR << e.what();
            return;
        }
    }

    bool Reader::requestLocalization(const std::string& entityName, const armarx::core::time::Duration& until)
    {
        auto entityNameParts = GetEntityNameParts(entityName);

        armarx::data::ObjectID requestObject({std::get<0>(entityNameParts), std::get<1>(entityNameParts), std::get<2>(entityNameParts)});
        armarx::objpose::provider::RequestObjectsOutput requestResult = objPoseProvider->requestObjects({{requestObject}, until.toMilliSeconds()});

        return requestResult.results.at(requestObject).success;
    }

    /// get the latest object from an memory and cast it to an ObjectInstance
    std::optional<armarx::armem::arondto::ObjectInstance> Reader::queryObject(const armem::wm::Memory& memory, const armem::Time& timestamp)
    {
        // clang-format off
        const armem::wm::CoreSegment& s = memory
                .getCoreSegment(properties.coreSegmentName);
        // clang-format on

        // What to do with timestamp?
        const armem::wm::EntityInstance* instance = nullptr;
        s.forEachInstance([&instance](const wm::EntityInstance& i)
                                        { instance = &i; });
        if (instance == nullptr)
        {
            return std::nullopt;
        }
        return armem::arondto::ObjectInstance::FromAron(instance->data());
    }

    /// Query an object with full name entityName (e.g. Kitchen/green-cup/0)
    std::optional<armarx::armem::arondto::ObjectInstance> Reader::queryObjectByEntityID(const std::string& entityName, const armem::Time& timestamp)
    {
        // Query all entities from all provider.
        armem::client::query::Builder qb;

        // clang-format off
        qb
        .coreSegments().withName(properties.coreSegmentName)
        .providerSegments().all()
        .entities().withName(entityName)
        .snapshots().beforeOrAtTime(timestamp);
        // clang-format on

        const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success) /* c++20 [[unlikely]] */
        {
            return std::nullopt;
        }

        return queryObject(qResult.memory, timestamp);
    }

    /// Query an object by objectId (e.g. Kitchen/green-cup in Entity Kitchen/green-cup/0)
    std::optional<armarx::armem::arondto::ObjectInstance> Reader::queryObjectByObjectID(const std::string& objectId, const armem::Time& timestamp)
    {
        // Query all entities from all provider.
        armem::client::query::Builder qb;

        // clang-format off
        qb
        .coreSegments().withName(properties.coreSegmentName)
        .providerSegments().all()
        .entities().all()
        .snapshots().beforeOrAtTime(timestamp);
        // clang-format on

        const armem::client::QueryResult qResult = memoryReader.query(qb.buildQueryInput());

        ARMARX_DEBUG << "Lookup result in reader: " << qResult;

        if (not qResult.success) /* c++20 [[unlikely]] */
        {
            return std::nullopt;
        }

        // clang-format off
        const armem::wm::CoreSegment& s = qResult.memory
                .getCoreSegment(properties.coreSegmentName);
        // clang-format on

        const armem::wm::EntityInstance* instance = nullptr;
        s.forEachInstance([&instance, &objectId](const wm::EntityInstance& i)
                                        { if (simox::alg::starts_with(i.id().entityName, objectId)) instance = &i; });
        if (instance == nullptr)
        {
            return std::nullopt;
        }
        return armem::arondto::ObjectInstance::FromAron(instance->data());
    }

}  // namespace armarx::armem::attachment
