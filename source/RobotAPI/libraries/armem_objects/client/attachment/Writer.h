/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>

#include <RobotAPI/libraries/armem_robot_state/client/common/RobotReader.h>

#include <RobotAPI/libraries/armem_objects/types.h>


namespace armarx::armem::attachment
{

    class Writer
    {
    public:
        Writer(armem::client::MemoryNameSystem& memoryNameSystem);
        virtual ~Writer() = default;


        void registerPropertyDefinitions(armarx::PropertyDefinitionsPtr& def);
        void connect();

        std::optional<armem::MemoryID> commit(const ObjectAttachment& attachment);
        std::optional<armem::MemoryID> commit(const ArticulatedObjectAttachment&);

    private:

        struct Properties
        {
            std::string memoryName                  = "Object";
            std::string coreAttachmentsSegmentName  = "Attachments";
            std::string providerName                = "AttachmentProvider";

            bool allowClassCreation                 = false;
        } properties;

        const std::string propertyPrefix = "mem.obj.articulated.";

        armem::client::MemoryNameSystem& memoryNameSystem;

        armem::client::Writer memoryWriter;
        std::mutex memoryWriterMutex;

        armem::client::Reader memoryReader;
        std::mutex memoryReaderMutex;
    };


}  // namespace armarx::armem::attachment
