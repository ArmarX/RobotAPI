/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem/core/forward_declarations.h>
#include <RobotAPI/libraries/armem/server/forward_declarations.h>
#include <RobotAPI/libraries/armem_objects/types.h>

#include <ArmarXCore/core/application/properties/forward_declarations.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <string>


namespace armarx::armem::server::obj::attachments
{

    class Segment : public armarx::Logging
    {
    public:

        Segment(server::MemoryToIceAdapter& iceMemory);
        virtual ~Segment();

        void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "");

        void init();
        void connect();

        std::vector<armarx::armem::attachment::ObjectAttachment> getAttachments(const armem::Time& timestamp) const;


    private:

        server::MemoryToIceAdapter& iceMemory;
        wm::CoreSegment* coreSegment = nullptr;

        struct Properties
        {
            std::string coreSegmentName = "Attachments";
            int64_t maxHistorySize = -1;
        };
        Properties p;

    };

}  // namespace armarx::armem::server::obj::attachments
