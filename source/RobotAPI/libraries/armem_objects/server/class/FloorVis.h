#pragma once

#include <string>

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/libraries/armem_objects/aron_forward_declarations.h>


namespace armarx
{
    using PropertyDefinitionsPtr = IceUtil::Handle<class PropertyDefinitionContainer>;
}
namespace armarx::armem::server::wm
{
    class CoreSegment;
    class Entity;
}

namespace armarx::armem::server::obj::clazz
{
    class FloorVis
    {
    public:

        FloorVis();

        void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "");
        void setArViz(armarx::viz::Client arviz);

        /// Draw a the floor as a simox object.
        /// @see `makeFloorObject()`
        void updateFloorObject(const wm::CoreSegment& classCoreSegment);

        armarx::viz::Object makeFloorObject(const wm::Entity& classEntity);
        armarx::viz::Object makeFloorObject(const std::string& name, const arondto::ObjectClass& objectClass);


    public:

        struct Properties
        {
            bool show = true;

            std::string entityName = "Building/floor-20x20";
            std::string layerName = "Floor";
            float height = -1;

            void define(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "");
        };

    private:

        Properties properties;

        armarx::viz::Client arviz;

    };
}
