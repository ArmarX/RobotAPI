#pragma once

#include <mutex>
#include <string>

#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/segment/SpecializedSegment.h>
#include <RobotAPI/libraries/armem_objects/server/class/FloorVis.h>


namespace armarx::armem::arondto
{
    class ObjectClass;
}
namespace armarx::armem::server::obj::clazz
{

    class Segment : public segment::SpecializedCoreSegment
    {
    public:

        Segment(armem::server::MemoryToIceAdapter& iceMemory);
        virtual ~Segment() override;


        void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "") override;
        void init() override;
        void connect(viz::Client arviz);


        void loadByObjectFinder(const std::string& objectsPackage);
        void loadByObjectFinder(const ObjectFinder& finder);
        void loadByObjectFinder();

        void visualizeClass(const MemoryID& entityID, bool showAABB = true, bool showOOBB = true);


        static arondto::ObjectClass objectClassFromInfo(const ObjectInfo& info);


    private:

        ObjectFinder objectFinder;

        viz::Client arviz;
        FloorVis floorVis;


        struct Properties
        {
            std::string objectsPackage = ObjectFinder::DefaultObjectsPackageName;
            bool loadFromObjectsPackage = true;
        };
        Properties p;


    public:

        struct RemoteGui
        {
            armarx::RemoteGui::Client::GroupBox group;

            struct Data
            {
                armarx::RemoteGui::Client::GroupBox group;

                armarx::RemoteGui::Client::Button reloadButton;
                armarx::RemoteGui::Client::IntSpinBox maxHistorySize;
                armarx::RemoteGui::Client::CheckBox infiniteHistory;

                void setup(const Segment& segment);
                void update(Segment& segment);

                bool rebuild = false;
            };
            Data data;

            struct Visu
            {
                armarx::RemoteGui::Client::GroupBox group;

                std::vector<MemoryID> showOptionsIndex;
                armarx::RemoteGui::Client::ComboBox showComboBox;
                armarx::RemoteGui::Client::Button showButton;

                void setup(const Segment& segment);
                void update(Segment& segment);
            };
            Visu visu;

            void setup(const Segment& segment);
            void update(Segment& segment);

        };

    };

}
