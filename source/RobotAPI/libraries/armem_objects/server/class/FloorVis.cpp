#include "FloorVis.h"

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem/server/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem_objects/aron/ObjectClass.aron.generated.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>


namespace armarx::armem::server::obj::clazz
{

    FloorVis::FloorVis()
    {
    }


    void FloorVis::defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix)
    {
        properties.define(defs, prefix);
    }


    void FloorVis::setArViz(armarx::viz::Client arviz)
    {
        this->arviz = arviz;
    }


    void FloorVis::updateFloorObject(const wm::CoreSegment& classCoreSegment)
    {
        viz::Layer layer = arviz.layer(properties.layerName);
        if (properties.show)
        {
            const wm::Entity* entity = classCoreSegment.findEntity(properties.entityName);
            if (entity)
            {
                ARMARX_INFO << "Drawing floor class '" << properties.entityName << "'.";
                layer.add(makeFloorObject(*entity));
            }
            else
            {
                ARMARX_INFO << "Did not find floor class '" << properties.entityName << "'.";
            }
        }
        arviz.commit({layer});
    }


    armarx::viz::Object FloorVis::makeFloorObject(const wm::Entity& classEntity)
    {
        const wm::EntityInstance& instance = classEntity.getLatestSnapshot().getInstance(0);
        arondto::ObjectClass data;
        data.fromAron(instance.data());
        return makeFloorObject(classEntity.name(), data);
    }


    armarx::viz::Object FloorVis::makeFloorObject(
        const std::string& name,
        const arondto::ObjectClass& objectClass)
    {
        ARMARX_TRACE;
        return armarx::viz::Object(name)
               .file(objectClass.simoxXmlPath.package, objectClass.simoxXmlPath.path)
               .position(Eigen::Vector3f(0, 0, properties.height));
    }


    void FloorVis::Properties::define(armarx::PropertyDefinitionsPtr defs, const std::string& prefix)
    {
        defs->optional(show, prefix + "Show", "Whether to show the floor.");
        defs->optional(entityName, prefix +  "EntityName", "Object class entity of the floor.");
        defs->optional(layerName, prefix +  "LayerName", "Layer to draw the floor on.");
        defs->optional(height, prefix +  "Height",
                       "Height (z) of the floor plane. \n"
                       "Set slightly below 0 to avoid z-fighting when drawing planes on the ground.");
    }

}


