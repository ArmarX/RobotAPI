#pragma once

#include <string>
#include <vector>
#include <optional>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time/DateTime.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include <RobotAPI/interface/observers/KinematicUnitObserverInterface.h>
#include <RobotAPI/interface/objectpose/ObjectPoseStorageInterface.h>


namespace armarx
{
    class PropertyDefinitionContainer;
    using PropertyDefinitionsPtr = IceUtil::Handle<PropertyDefinitionContainer>;
}

namespace armarx::armem::server::obj::instance
{
    class RobotHeadMovement : public armarx::Logging
    {
    public:

        void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "head.");

        void fetchDatafields();
        bool isMoving() const;

        void movementStarts(long discardIntervalMs);
        void movementStarts(const Duration& discardInterval);
        void movementStops(long discardIntervalMs);
        void movementStops(const Duration& discardInterval);

        objpose::SignalHeadMovementOutput signalHeadMovement(const objpose::SignalHeadMovementInput& input);


        struct Discard
        {
            std::optional<DateTime> updatesUntil;
            bool all = false;
        };
        Discard getDiscard();


    public:

        bool checkHeadVelocity = true;

        std::string jointVelocitiesChannelName = "jointvelocities";
        std::vector<std::string> jointNames = {"Neck_1_Yaw", "Neck_2_Pitch"};
        float maxJointVelocity = 0.05f;
        int discardIntervalAfterMoveMS = 100;

        KinematicUnitObserverInterfacePrx kinematicUnitObserver;
        std::vector<DatafieldRefPtr> jointVelocitiesDatafields;
        DateTime discardUpdatesUntil = DateTime::Invalid();

        DebugObserverInterfacePrx debugObserver;


        struct RemoteGui
        {
            armarx::RemoteGui::Client::GroupBox group;

            armarx::RemoteGui::Client::CheckBox checkHeadVelocity;
            armarx::RemoteGui::Client::FloatSpinBox maxJointVelocity;
            armarx::RemoteGui::Client::IntSpinBox discardIntervalAfterMoveMS;

            void setup(const RobotHeadMovement& rhm);
            void update(RobotHeadMovement& rhm);
        };

    };

}
