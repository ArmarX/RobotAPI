#pragma once

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/core/time/forward_declarations.h>

#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>


namespace armarx::armem::server::obj::instance
{

    /**
     * @brief Models decay of object localizations by decreasing the confidence
     * the longer the object was not localized.
     */
    class Decay : public armarx::Logging
    {
    public:

        void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "decay.");

        void updateConfidence(objpose::ObjectPose& pose, const DateTime& now) const;
        void updateConfidences(objpose::ObjectPoseSeq& objectPoses, const DateTime& now) const;

    private:

        float calculateConfidence(const DateTime& localization, const DateTime& now) const;


    public:

        bool enabled = false;

        /// Duration after latest localization before decay starts.
        float delaySeconds = 5.0;
        /// How long to reach minConfidence.
        float durationSeconds = 20.0;

        float maxConfidence = 1.0;
        float minConfidence = 0.0f;

        float removeObjectsBelowConfidence = 0.1f;


        struct RemoteGui
        {
            armarx::RemoteGui::Client::GroupBox group;

            armarx::RemoteGui::Client::CheckBox enabled;

            armarx::RemoteGui::Client::FloatSpinBox delaySeconds;
            armarx::RemoteGui::Client::FloatSpinBox durationSeconds;
            armarx::RemoteGui::Client::FloatSlider maxConfidence;
            armarx::RemoteGui::Client::FloatSlider minConfidence;

            armarx::RemoteGui::Client::FloatSlider removeObjectsBelowConfidence;

            void setup(const Decay& decay);
            void update(Decay& decay);
        };

    };

}
