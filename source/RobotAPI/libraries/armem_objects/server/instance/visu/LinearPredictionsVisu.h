#pragma once


#include <ArmarXCore/core/application/properties/forward_declarations.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time/DateTime.h>

#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include <RobotAPI/components/ArViz/Client/Layer.h>
#include <RobotAPI/libraries/ArmarXObjects/forward_declarations.h>


namespace armarx::armem::server::obj::instance::visu
{

    /// Visualization control for linear predictions for objects.
    struct LinearPredictions : public armarx::Logging
    {
        bool showGhost = false;
        float ghostAlpha = 0.7;

        bool showFrame = false;
        bool showArrow = false;

        float timeOffsetSeconds = 1;
        float timeWindowSeconds = 2;

        void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix);

        bool showAny() const;
        void draw(viz::Layer& layer,
                  std::function<viz::Object(const std::string& key)> makeObjectFn,
                  const objpose::ObjectPose& objectPose,
                  const std::map<DateTime, objpose::ObjectPose>& poseHistory,
                  bool inGlobalFrame) const;


        struct RemoteGui
        {
            armarx::RemoteGui::Client::CheckBox showGhost;
            armarx::RemoteGui::Client::FloatSpinBox ghostAlpha;

            armarx::RemoteGui::Client::CheckBox showFrame;
            armarx::RemoteGui::Client::CheckBox showArrow;

            armarx::RemoteGui::Client::FloatSpinBox timeOffsetSeconds;
            armarx::RemoteGui::Client::FloatSpinBox timeWindowSeconds;

            armarx::RemoteGui::Client::GroupBox group;

            void setup(const LinearPredictions& data);
            void update(LinearPredictions& data);
        };

    };


}
