/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

// #include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/libraries/armem_objects/types.h>

namespace armarx
{
    class ObjectFinder;
}

namespace armarx::armem::server::obj::instance
{
    class Segment;

    /**
     * @brief Visualizes articulated objects
     */
    class ArticulatedObjectVisu : public armarx::Logging
    {
    public:
        ArticulatedObjectVisu(const viz::Client& arviz, Segment& segment) :
            arviz(arviz), segment(segment)
        {
        }

        void defineProperties(armarx::PropertyDefinitionsPtr defs,
                              const std::string& prefix = "visu.");

        void init();

    protected:
        viz::Layer visualizeProvider(
            const std::string& providerName,
            const armarx::armem::articulated_object::ArticulatedObjects& objects) const;

        void visualizeObjects(
            viz::Layer& layer,
            const armarx::armem::articulated_object::ArticulatedObjects& objects) const;

    private:
        viz::Client arviz;
        Segment& segment;

        struct Properties
        {
            bool enabled      = true;
            float frequencyHz = 25;
        } p;

        PeriodicTask<ArticulatedObjectVisu>::pointer_type updateTask;
        void visualizeRun();

        // struct RemoteGui
        // {
        //     armarx::RemoteGui::Client::GroupBox group;

        //     armarx::RemoteGui::Client::CheckBox enabled;

        //     armarx::RemoteGui::Client::CheckBox inGlobalFrame;
        //     armarx::RemoteGui::Client::FloatSlider alpha;
        //     armarx::RemoteGui::Client::CheckBox alphaByConfidence;
        //     armarx::RemoteGui::Client::CheckBox oobbs;
        //     armarx::RemoteGui::Client::CheckBox objectFrames;
        //     armarx::RemoteGui::Client::FloatSpinBox objectFramesScale;

        //     // void setup(const Visu& visu);
        //     // void update(Visu& visu);
        // };
    };

} // namespace armarx::armem::server::obj::instance
