#pragma once


#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>

#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include <RobotAPI/components/ArViz/Client/Client.h>
#include <RobotAPI/interface/objectpose/ObjectPoseStorageInterface.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>
#include <RobotAPI/libraries/armem_objects/server/instance/visu/LinearPredictionsVisu.h>


namespace armarx
{
    class ObjectFinder;
}
namespace armarx::armem::server::obj::instance
{

    /**
     * @brief Models decay of object localizations by decreasing the confidence
     * the longer the object was not localized.
     */
    class Visu : public armarx::Logging
    {
    public:

        void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "visu.");

        std::vector<viz::Layer> visualizeCommit(
            const std::map<std::string, objpose::ObjectPoseSeq>& objectPoses,
            const std::map<std::string, std::vector<std::map<DateTime, objpose::ObjectPose>>>&
                poseHistories,
            const ObjectFinder& objectFinder) const;

        /// Visualize the given object poses, with one layer per provider.
        std::vector<viz::Layer> visualizeCommit(
            const objpose::ObjectPoseSeq& objectPoses,
            const std::vector<std::map<DateTime, objpose::ObjectPose>>& poseHistories,
            const ObjectFinder& objectFinder
        ) const;
        std::vector<viz::Layer> visualizeCommit(
            const objpose::ObjectPoseMap& objectPoses,
            const std::map<ObjectID, std::map<DateTime, objpose::ObjectPose>>& poseHistories,
            const ObjectFinder& objectFinder
        ) const;

        viz::Layer visualizeProvider(
            const std::string& providerName,
            const objpose::ObjectPoseSeq& objectPoses,
            const std::vector<std::map<DateTime, objpose::ObjectPose>>& poseHistories,
            const ObjectFinder& objectFinder
        ) const;


        void visualizeObjectPose(
            viz::Layer& layer,
            const objpose::ObjectPose& objectPose,
            const std::map<DateTime, objpose::ObjectPose>& poseHistory,
            const ObjectFinder& objectFinder
        ) const;


    private:

        viz::Layer& getLayer(const std::string& providerName, std::map<std::string, viz::Layer>& stage) const;


    public:

        viz::Client arviz;

        bool enabled = true;
        float frequencyHz = 25;

        bool inGlobalFrame = true;
        float minConfidence = -1;
        float alpha = 1.0;
        bool alphaByConfidence = false;
        bool oobbs = false;

        bool objectFrames = false;
        float objectFramesScale = 1.0;

        struct Gaussians
        {
            bool position = true;
            float positionScale = 1.0;
            bool orientation = false;
            float orientationScale = 100;
            bool orientationDisplaced = false;

            void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix);

            bool showAny() const;
            void draw(viz::Layer& layer,
                      const objpose::ObjectPose& objectPose,
                      bool inGlobalFrame) const;
        };
        Gaussians gaussians;

        /// Prefer articulated models if available.
        bool useArticulatedModels = true;

        /// Linear predictions for objects.
        visu::LinearPredictions linearPredictions;

        SimpleRunningTask<>::pointer_type updateTask;

        struct RemoteGui
        {
            armarx::RemoteGui::Client::GroupBox group;

            armarx::RemoteGui::Client::CheckBox enabled;

            armarx::RemoteGui::Client::CheckBox inGlobalFrame;
            armarx::RemoteGui::Client::FloatSlider alpha;
            armarx::RemoteGui::Client::CheckBox alphaByConfidence;
            armarx::RemoteGui::Client::CheckBox oobbs;

            armarx::RemoteGui::Client::CheckBox objectFrames;
            armarx::RemoteGui::Client::FloatSpinBox objectFramesScale;

            struct Gaussians
            {
                armarx::RemoteGui::Client::CheckBox position;
                armarx::RemoteGui::Client::FloatSpinBox positionScale;

                armarx::RemoteGui::Client::CheckBox orientation;
                armarx::RemoteGui::Client::FloatSpinBox orientationScale;
                armarx::RemoteGui::Client::CheckBox orientationDisplaced;

                armarx::RemoteGui::Client::GroupBox group;

                void setup(const Visu& data);
                void update(Visu::Gaussians& data);
            };
            Gaussians gaussians;

            armarx::RemoteGui::Client::CheckBox useArticulatedModels;

            visu::LinearPredictions::RemoteGui linearPredictions;

            void setup(const Visu& visu);
            void update(Visu& visu);
        };

    };

}
