/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::armem_objects::Adapter
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <mutex>

#include <VirtualRobot/VirtualRobot.h>

#include <ArmarXGui/libraries/RemoteGui/Client/Widgets.h>

#include "RobotAPI/libraries/armem_robot_state/client/common/VirtualRobotReader.h"
#include <RobotAPI/interface/armem/server/ObjectMemoryInterface.h>
#include <RobotAPI/interface/core/RobotState.h>

#include <RobotAPI/components/ArViz/Client/Client.h>

#include <RobotAPI/libraries/armem/server/MemoryToIceAdapter.h>
#include <RobotAPI/libraries/armem_objects/server/instance/Segment.h>
#include <RobotAPI/libraries/armem_objects/server/instance/Decay.h>
#include <RobotAPI/libraries/armem_objects/server/instance/Visu.h>
#include <RobotAPI/libraries/armem_objects/server/instance/RobotHeadMovement.h>


#define ICE_CURRENT_ARG const Ice::Current& = Ice::emptyCurrent


namespace armarx::armem::server::obj::instance
{

    /**
     * @brief Helps implementing the `armarx::armem::server::ObjectInstanceSegmentInterface`.
     */
    class SegmentAdapter :
        virtual public armarx::Logging
        , virtual public armarx::armem::server::ObjectInstanceSegmentInterface
    {
    public:

        SegmentAdapter(MemoryToIceAdapter& iceMemory);

        std::string getName() const;
        void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "");

        void init();
        void connect(
            robot_state::VirtualRobotReader* virtualRobotReader,
            KinematicUnitObserverInterfacePrx kinematicUnitObserver,
            viz::Client arviz,
            DebugObserverInterfacePrx debugObserver
        );


        // ObjectPoseTopic interface
    public:
        virtual void reportProviderAvailable(const std::string& providerName, const objpose::ProviderInfo& info, ICE_CURRENT_ARG) override;
        virtual void reportObjectPoses(const std::string& providerName, const objpose::data::ProvidedObjectPoseSeq& objectPoses, ICE_CURRENT_ARG) override;

        // ObjectInstanceSegmentInterface interface
    public:

        // OBJECT POSES

        virtual objpose::data::ObjectPoseSeq getObjectPoses(ICE_CURRENT_ARG) override;
        virtual objpose::data::ObjectPoseSeq getObjectPosesByProvider(const std::string& providerName, ICE_CURRENT_ARG) override;

        // PROVIDER INFORMATION

        virtual bool hasProvider(const std::string& providerName, ICE_CURRENT_ARG) override;
        virtual objpose::ProviderInfo getProviderInfo(const std::string& providerName, ICE_CURRENT_ARG) override;
        virtual Ice::StringSeq getAvailableProviderNames(ICE_CURRENT_ARG) override;
        virtual objpose::ProviderInfoMap getAvailableProvidersInfo(ICE_CURRENT_ARG) override;


        // REQUESTING

        virtual objpose::observer::RequestObjectsOutput requestObjects(const objpose::observer::RequestObjectsInput& input, ICE_CURRENT_ARG) override;

        // ATTACHING

        virtual objpose::AttachObjectToRobotNodeOutput attachObjectToRobotNode(const objpose::AttachObjectToRobotNodeInput& input, ICE_CURRENT_ARG) override;
        virtual objpose::DetachObjectFromRobotNodeOutput detachObjectFromRobotNode(const objpose::DetachObjectFromRobotNodeInput& input, ICE_CURRENT_ARG) override;
        virtual objpose::DetachAllObjectsFromRobotNodesOutput detachAllObjectsFromRobotNodes(const objpose::DetachAllObjectsFromRobotNodesInput& input, ICE_CURRENT_ARG) override;

        virtual objpose::AgentFramesSeq getAttachableFrames(ICE_CURRENT_ARG) override;

        // HEAD MOVEMENT SIGNALS

        virtual objpose::SignalHeadMovementOutput signalHeadMovement(const objpose::SignalHeadMovementInput& input, ICE_CURRENT_ARG) override;

        // PREDICTING

        virtual objpose::ObjectPosePredictionResultSeq
        predictObjectPoses(const objpose::ObjectPosePredictionRequestSeq& requests,
                           ICE_CURRENT_ARG) override;

        virtual armem::prediction::data::PredictionEngineSeq
            getAvailableObjectPoseEngines(ICE_CURRENT_ARG) override;


    private:

        void updateProviderInfo(const std::string& providerName, const objpose::ProviderInfo& info);

        void updateObjectPoses(const std::string& providerName, const objpose::data::ProvidedObjectPoseSeq& providedPoses);
        void handleProviderUpdate(const std::string& providerName);


        // Visualization

        void visualizeRun();


    public:

        static const std::string linearPredictionEngineID;
        static const std::vector<PredictionEngine> predictionEngines;


    private:

        viz::Client arviz;
        DebugObserverInterfacePrx debugObserver;

        instance::Segment segment;

        instance::RobotHeadMovement robotHead;
        std::mutex robotHeadMutex;

        instance::Visu visu;
        std::mutex visuMutex;


        struct Calibration
        {
            std::string robotName = "";
            std::string robotNode = "Neck_2_Pitch";
            float offset = 0.0f;

            void defineProperties(armarx::PropertyDefinitionsPtr defs, const std::string& prefix = "calibration.");
        };
        Calibration calibration;


    public:

        struct RemoteGui
        {
            armarx::RemoteGui::Client::GroupBox group;
            armarx::RemoteGui::Client::VBoxLayout layout;

            instance::Visu::RemoteGui visu;
            instance::Segment::RemoteGui segment;
            instance::Decay::RemoteGui decay;
            instance::RobotHeadMovement::RemoteGui robotHead;

            void setup(const SegmentAdapter& adapter);
            void update(SegmentAdapter& adapter);
        };

    };

}

#undef ICE_CURRENT_ARG
