#pragma once

#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>

#include <RobotAPI/libraries/armem_objects/aron/ObjectInstance.aron.generated.h>
#include <RobotAPI/libraries/armem_objects/aron/Attachment.aron.generated.h>

#include <RobotAPI/libraries/armem_objects/types.h>

namespace armarx::armem
{
    void fromAron(const arondto::ObjectInstance& dto, objpose::arondto::ObjectPose& bo);
    void toAron(arondto::ObjectInstance& dto, const objpose::arondto::ObjectPose& bo);

    void fromAron(const arondto::ObjectInstance& dto, objpose::ObjectPose& bo);
    void toAron(arondto::ObjectInstance& dto, const objpose::ObjectPose& bo);


    /* Attachments */
    void fromAron(const arondto::attachment::AgentDescription& dto, attachment::AgentDescription& bo);
    void toAron(arondto::attachment::AgentDescription& dto, const attachment::AgentDescription& bo);

    void fromAron(const arondto::attachment::ObjectAttachment& dto, attachment::ObjectAttachment& bo);
    void toAron(arondto::attachment::ObjectAttachment& dto, const attachment::ObjectAttachment& bo);

    void fromAron(const arondto::attachment::ArticulatedObjectAttachment& dto, attachment::ArticulatedObjectAttachment& bo);
    void toAron(arondto::attachment::ArticulatedObjectAttachment& dto, const attachment::ArticulatedObjectAttachment& bo);

}  // namespace armarx::armem


#include <RobotAPI/libraries/armem/core/MemoryID.h>

namespace armarx::armem::obj
{
    /// Make a Memory ID for the object instance snapshot representing this pose.
    MemoryID makeObjectInstanceMemoryID(const objpose::ObjectPose& objectPose);
}
