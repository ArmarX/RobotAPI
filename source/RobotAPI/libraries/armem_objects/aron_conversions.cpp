#include "aron_conversions.h"

#include <RobotAPI/libraries/ArmarXObjects/aron_conversions.h>

#include <RobotAPI/libraries/aron/common/aron_conversions.h>
#include <RobotAPI/libraries/armem/core/aron_conversions.h>

namespace armarx::armem
{

    void fromAron(const arondto::ObjectInstance& dto,
                  objpose::arondto::ObjectPose& bo)
    {
        bo = dto.pose;
    }

    void toAron(arondto::ObjectInstance& dto, const objpose::arondto::ObjectPose& bo)
    {
        dto.pose = bo;
    }

    void fromAron(const arondto::ObjectInstance& dto, objpose::ObjectPose& bo)
    {
        objpose::fromAron(dto.pose, bo);
    }

    void toAron(arondto::ObjectInstance& dto, const objpose::ObjectPose& bo)
    {
        objpose::toAron(dto.pose, bo);
    }


    /* Attachments */
    void fromAron(const arondto::attachment::AgentDescription& dto, attachment::AgentDescription& bo)
    {
        fromAron(dto.id, bo.id);
        aron::fromAron(dto.frame, bo.frame);
    }

    void toAron(arondto::attachment::AgentDescription& dto, const attachment::AgentDescription& bo)
    {
        toAron(dto.id, bo.id);
        aron::toAron(dto.frame, bo.frame);
    }


    void fromAron(const arondto::attachment::ObjectAttachment& dto, attachment::ObjectAttachment& bo)
    {
        fromAron(dto.agent, bo.agent);
        aron::fromAron(dto.transformation, bo.transformation);
        fromAron(dto.object, bo.object);
        aron::fromAron(dto.active, bo.active);
        // TODO aron::fromAron(dto.timestamp, bo.timestamp);
    }

    void toAron(arondto::attachment::ObjectAttachment& dto, const attachment::ObjectAttachment& bo)
    {
        toAron(dto.agent, bo.agent);
        aron::toAron(dto.transformation, bo.transformation);
        toAron(dto.object, bo.object);
        aron::toAron(dto.active, bo.active);
        // TODO aron::toAron(dto.timestamp, bo.timestamp);
    }


    void fromAron(const arondto::attachment::ArticulatedObjectAttachment& dto, attachment::ArticulatedObjectAttachment& bo)
    {
        fromAron(dto.agent, bo.agent);
        aron::fromAron(dto.transformation, bo.transformation);
        fromAron(dto.object, bo.object);
        aron::fromAron(dto.active, bo.active);
        // TODO aron::fromAron(dto.timestamp, bo.timestamp);
    }

    void toAron(arondto::attachment::ArticulatedObjectAttachment& dto, const attachment::ArticulatedObjectAttachment& bo)
    {
        toAron(dto.agent, bo.agent);
        aron::toAron(dto.transformation, bo.transformation);
        toAron(dto.object, bo.object);
        aron::toAron(dto.active, bo.active);
        // TODO aron::toAron(dto.timestamp, bo.timestamp);
    }




}  // namespace armarx::armem

armarx::armem::MemoryID
armarx::armem::obj::makeObjectInstanceMemoryID(const objpose::ObjectPose& objectPose)
{
    return MemoryID("Object/Instance")
           .withProviderSegmentName(objectPose.providerName)
           .withEntityName(objectPose.objectID.str())
           .withTimestamp(objectPose.timestamp);
}
