#include "gui_utils.h"

#include <QLayout>
#include <QLayoutItem>
#include <QSplitter>
#include <QWidget>
#include <QTreeWidgetItem>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


void armarx::gui::clearLayout(QLayout* layout)
{
    // Source: https://stackoverflow.com/a/4857631

    ARMARX_CHECK(layout);

    QLayoutItem* item;
    while ((item = layout->takeAt(0)))
    {
        if (item->layout())
        {
            clearLayout(item->layout());
            delete item->layout();
        }
        if (item->widget())
        {
            delete item->widget();
        }
        delete item;
    }
}

void armarx::gui::clearItem(QTreeWidgetItem* item)
{
    while (item->childCount() > 0)
    {
        delete item->takeChild(0);
    }
}


QSplitter* armarx::gui::useSplitter(QLayout* layout)
{
    ARMARX_CHECK(layout);

    // Check all items
    for (int i = 0; i < layout->count(); ++i)
    {
        ARMARX_CHECK_NOT_NULL(layout->itemAt(i)->widget())
                << "QSplitter only supports widgets, but layout item #" << i << " is not a widget.";
    }

    QSplitter* splitter;
    if (dynamic_cast<QHBoxLayout*>(layout))
    {
        splitter = new QSplitter(Qt::Orientation::Horizontal);
    }
    else if (dynamic_cast<QVBoxLayout*>(layout))
    {
        splitter = new QSplitter(Qt::Orientation::Vertical);
    }
    else
    {
        splitter = new QSplitter();
    }

    while (layout->count() > 0)
    {
        const int index = 0;
        if (layout->itemAt(index))
        {
            QLayoutItem* item = layout->takeAt(index);

            ARMARX_CHECK(item->widget());
            splitter->addWidget(item->widget());

            delete item;
        }
    }
    ARMARX_CHECK_EQUAL(layout->count(), 0);

    layout->addWidget(splitter);
    ARMARX_CHECK_EQUAL(layout->count(), 1);
    ARMARX_CHECK_EQUAL(layout->itemAt(0)->widget(), splitter);

    return splitter;
}

armarx::gui::LeadingZeroSpinBox::LeadingZeroSpinBox(int numDigits, int base) :
    numDigits(numDigits), base(base)
{
}

QString
armarx::gui::LeadingZeroSpinBox::textFromValue(int value) const
{
    return QString("%1").arg(value, numDigits, base, QChar('0'));
}
