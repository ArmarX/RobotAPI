#pragma once

#include <mutex>

#include <RobotAPI/libraries/armem/core/query/DataMode.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>

#include "SnapshotSelectorWidget.h"

class QWidget;
class QTabWidget;
class QPushButton;
class QGroupBox;

namespace armarx::armem::gui
{

    class QueryWidget : public QWidget
    {
        Q_OBJECT
        using This = QueryWidget;

    public:

        QueryWidget();

        armem::query::DataMode dataMode() const;

        bool dropRemovedMemories() const;
        bool dropDisabledMemories() const;

        int queryLinkRecursionDepth() const;

        std::vector<std::string> enabledMemories() const;

        void update(const std::vector<std::string>& memoryNames);


    public slots:

    signals:
        void storeInLTM();
        void startRecording();
        void stopRecording();

        // ToDo:
        // void queryChanged(armem::query::data::Input query);


    private slots:
        void setRecursionDepthSpinnerEnabled(int state);

    signals:



    private:

        QCheckBox* _dataCheckBox;

        QCheckBox* _dropRemovedCheckBox;
        QCheckBox* _dropDisabledCheckBox;

        QLabel* _recursionDepthLabel;
        QSpinBox* _recursionDepthSpinner;

        QPushButton* _storeInLTMButton;
        QPushButton* _startLTMRecordingButton;
        QPushButton* _stopLTMRecordingButton;

        QGroupBox* _additionalSettingsGroupBox;
        QGroupBox* _availableMemoriesGroupBox;

        mutable std::mutex enabledMemoriesMutex;
    };

}
