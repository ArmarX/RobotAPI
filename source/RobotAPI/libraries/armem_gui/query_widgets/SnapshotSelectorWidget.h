#pragma once

#include <map>

#include <QWidget>

#include <RobotAPI/libraries/armem/core/query/DataMode.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>

#include "SnapshotForm.h"


class QCheckBox;
class QComboBox;
class QVBoxLayout;
class QPushButton;


namespace armarx::armem::gui
{

    /**
     * @brief Widget where queries for snapshots can be configured.
     *
     * Manages multiple forms which are hidden if not selected
     * (in order to keep their settings).
     */
    class SnapshotSelectorWidget : public QWidget
    {
        Q_OBJECT
        using This = SnapshotSelectorWidget;

    public:

        SnapshotSelectorWidget();


        armem::query::DataMode dataMode() const;
        client::query::SnapshotSelector selector();


    public slots:

    signals:
        void queryChanged();

    private slots:

        //void updateSelector();

        void hideAllForms();
        void showSelectedFormForQuery(QString selected);

    signals:
        void queryOutdated();


    private:

        void addForm(const QString& key, SnapshotForm* form);


    public:
        QVBoxLayout* _pageLayout;
        QComboBox* _queryComboBox;
        /// The forms for the different query types. Hidden when not selected.
        std::map<QString, SnapshotForm*> _queryForms;

    };

}
