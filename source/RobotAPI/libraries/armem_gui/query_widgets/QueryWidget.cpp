#include "QueryWidget.h"

#include <limits>
#include <mutex>

#include <QCheckBox>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QWidget>
#include <QScrollArea>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::armem::gui
{

    QueryWidget::QueryWidget()
    {
        auto* vlayout = new QVBoxLayout();
        auto* hlayout = new QHBoxLayout();

        {
            _availableMemoriesGroupBox = new QGroupBox("Available memories");
            auto vboxlayout = new QVBoxLayout();
            vboxlayout->setMargin(1);
            vboxlayout->setSizeConstraint(QLayout::SizeConstraint::SetMinAndMaxSize);
            vboxlayout->setAlignment(Qt::AlignTop);
            _availableMemoriesGroupBox->setLayout(vboxlayout);
            hlayout->addWidget(_availableMemoriesGroupBox);
        }

        QFrame* vFrame = new QFrame;
        vFrame->setFrameShape(QFrame::VLine);
        hlayout->addWidget(vFrame);

        {
            _additionalSettingsGroupBox = new QGroupBox("Additional query settings");
            auto vboxlayout = new QVBoxLayout();
            vboxlayout->setMargin(1);
            vboxlayout->setSizeConstraint(QLayout::SizeConstraint::SetMinAndMaxSize);
            vboxlayout->setAlignment(Qt::AlignTop);
            _additionalSettingsGroupBox->setLayout(vboxlayout);

            _dataCheckBox = new QCheckBox("Get Data");
            _dataCheckBox->setChecked(true);
            vboxlayout->addWidget(_dataCheckBox);

            _dropRemovedCheckBox = new QCheckBox("Drop disconnected memories");
            _dropRemovedCheckBox->setChecked(true);
            vboxlayout->addWidget(_dropRemovedCheckBox);

            _dropDisabledCheckBox = new QCheckBox("Drop disabled memories");
            _dropDisabledCheckBox->setChecked(false);
            vboxlayout->addWidget(_dropDisabledCheckBox);

            auto* recDepthHLayout = new QHBoxLayout();
            _recursionDepthSpinner = new QSpinBox(); // NOLINT
            _recursionDepthSpinner->setRange(-1, std::numeric_limits<int>::max());
            _recursionDepthSpinner->setValue(0);
            _recursionDepthSpinner->setEnabled(_dataCheckBox->isChecked());
            _recursionDepthSpinner->setSpecialValueText(QString("Unlimited"));
            recDepthHLayout->addWidget(_recursionDepthSpinner);

            _recursionDepthLabel = new QLabel("Link resolution depth");
            recDepthHLayout->addWidget(_recursionDepthLabel);
            recDepthHLayout->addStretch();
            vboxlayout->addLayout(recDepthHLayout);

            auto ltmButtonsLayout = new QVBoxLayout();
            _storeInLTMButton = new QPushButton("Query and store in LTM");
            ltmButtonsLayout->addWidget(_storeInLTMButton);

            auto* ltmRecHLayout = new QHBoxLayout();
            _startLTMRecordingButton = new QPushButton("Start LTM Recording");
            _stopLTMRecordingButton = new QPushButton("Stop LTM Recording");

            ltmRecHLayout->addWidget(_startLTMRecordingButton);
            ltmRecHLayout->addWidget(_stopLTMRecordingButton);
            ltmButtonsLayout->addLayout(ltmRecHLayout);

            vboxlayout->addLayout(ltmButtonsLayout);

            hlayout->addWidget(_additionalSettingsGroupBox);
        }
        vlayout->addLayout(hlayout);

        // Public connections.
        connect(_storeInLTMButton, &QPushButton::pressed, this, &This::storeInLTM);
        connect(_startLTMRecordingButton, &QPushButton::pressed, this, &This::startRecording);
        connect(_stopLTMRecordingButton, &QPushButton::pressed, this, &This::stopRecording);

        // Private connections.
        connect(_dataCheckBox, &QCheckBox::stateChanged, this, &This::setRecursionDepthSpinnerEnabled);

        setLayout(vlayout);
    }

    void QueryWidget::update(const std::vector<std::string>& activeMemoryNames)
    {
        std::scoped_lock l(enabledMemoriesMutex);
        std::vector<std::string> alreadyPresentMemoryCheckboxes;

        // get information about memories already present and activate inactive ones if necessary
        int maxIndex = _availableMemoriesGroupBox->layout()->count();
        for (int i = 0; i < maxIndex; ++i)
        {
            auto w = _availableMemoriesGroupBox->layout()->itemAt(i)->widget();
            QCheckBox* box = static_cast<QCheckBox*>(w);
            std::string memoryName = box->text().toStdString();
            if (box->isEnabled() && std::find(activeMemoryNames.begin(), activeMemoryNames.end(), memoryName) == activeMemoryNames.end())
            {
                // checkbox is enabled but memory is not available anymore
                box->setVisible(false);
                box->setChecked(false);
                box->setEnabled(false);
            }
            if (not(box->isEnabled()) && std::find(activeMemoryNames.begin(), activeMemoryNames.end(), memoryName) != activeMemoryNames.end())
            {
                // checkbox is disabled but memory is available again
                box->setVisible(true);
                box->setChecked(true);
                box->setEnabled(true);
            }
            alreadyPresentMemoryCheckboxes.push_back(memoryName);
        }

        // Add checkboxes for new memories
        for (const auto& memoryName : activeMemoryNames)
        {
            if (std::find(alreadyPresentMemoryCheckboxes.begin(), alreadyPresentMemoryCheckboxes.end(), memoryName) == alreadyPresentMemoryCheckboxes.end())
            {
                // new memory available
                auto box = new QCheckBox(QString::fromStdString(memoryName));
                box->setChecked(true);
                box->setVisible(true);
                box->setEnabled(true);
                _availableMemoriesGroupBox->layout()->addWidget(box);
            }
        }
    }

    armem::query::DataMode QueryWidget::dataMode() const
    {
        return _dataCheckBox->isChecked()
               ? armem::query::DataMode::WithData
               : armem::query::DataMode::NoData;
    }

    bool QueryWidget::dropRemovedMemories() const
    {
        return _dropRemovedCheckBox->isChecked();
    }

    bool QueryWidget::dropDisabledMemories() const
    {
        return _dropDisabledCheckBox->isChecked();
    }


    std::vector<std::string> QueryWidget::enabledMemories() const
    {
        std::scoped_lock l(enabledMemoriesMutex);

        std::vector<std::string> enabledMemoryCheckboxes;
        int maxIndex = _availableMemoriesGroupBox->layout()->count();
        for (int i = 0; i < maxIndex; ++i)
        {
            auto w = _availableMemoriesGroupBox->layout()->itemAt(i)->widget();
            QCheckBox* box = static_cast<QCheckBox*>(w);
            std::string memoryName = box->text().toStdString();
            if (box->isEnabled() && box->isChecked())
            {
                // Invisible ones are always unchecked if set to invisible from update method
                enabledMemoryCheckboxes.push_back(memoryName);
            }
        }
        return enabledMemoryCheckboxes;
    }

    int QueryWidget::queryLinkRecursionDepth() const
    {
        return _recursionDepthSpinner->value();
    }

    void
    QueryWidget::setRecursionDepthSpinnerEnabled(int state)
    {
        switch (state)
        {
            case Qt::Checked:
                _recursionDepthSpinner->setEnabled(true);
                break;
            case Qt::Unchecked:
            default:
                _recursionDepthSpinner->setEnabled(false);
                break;
        }
    }
}
