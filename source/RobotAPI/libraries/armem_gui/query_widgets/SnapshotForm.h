#pragma once

#include <QWidget>

#include <RobotAPI/libraries/armem/client/query/Builder.h>


class QCheckBox;
class QDateTimeEdit;
class QLabel;
class QSpinBox;


namespace armarx::armem::gui
{

    class SnapshotForm : public QWidget
    {
        Q_OBJECT

    public:
        virtual client::query::SnapshotSelector makeEntitySelector();


    signals:
        void queryChanged();


    protected:
        virtual void fillEntitySelector(client::query::SnapshotSelector& selector) = 0;
        void setDateTimeDisplayFormat(QDateTimeEdit* dt);

    };



    class SnapshotFormAll : public SnapshotForm
    {
    public:
        SnapshotFormAll();

    protected:
        void fillEntitySelector(client::query::SnapshotSelector& selector) override;
    };


    class SnapshotFormSingle : public SnapshotForm
    {
    public:
        SnapshotFormSingle();

    protected:
        void fillEntitySelector(client::query::SnapshotSelector& selector) override;

    private:
        QLabel* label;
        QDateTimeEdit* dateTime;
        QSpinBox* microseconds;
        QCheckBox* latest;
    };


    class SnapshotFormTimeRange : public SnapshotForm
    {
    public:
        SnapshotFormTimeRange();

        void fillEntitySelector(client::query::SnapshotSelector& selector) override;


        QLabel* fromLabel;
        QDateTimeEdit* fromDateTime;
        QCheckBox* fromBegin;

        QLabel* toLabel;
        QDateTimeEdit* toDateTime;
        QCheckBox* toEnd;
    };


    class SnapshotFormIndexRange : public SnapshotForm
    {
    public:
        SnapshotFormIndexRange();

        void fillEntitySelector(client::query::SnapshotSelector& selector) override;


        QLabel* firstLabel;
        QSpinBox* firstSpinBox;
        QCheckBox* firstBegin;

        QLabel* lastLabel;
        QSpinBox* lastSpinBox;
        QCheckBox* lastEnd;
    };

}
