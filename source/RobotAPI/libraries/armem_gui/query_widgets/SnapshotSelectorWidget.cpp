#include "SnapshotSelectorWidget.h"

#include <QCheckBox>
#include <QComboBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::armem::gui
{
    client::query::SnapshotSelector SnapshotSelectorWidget::selector()
    {
        return _queryForms.at(_queryComboBox->currentText())->makeEntitySelector();
    }

    SnapshotSelectorWidget::SnapshotSelectorWidget()
    {
        _pageLayout = new QVBoxLayout();
        setLayout(_pageLayout);

        {
            QHBoxLayout* typeLayout = new QHBoxLayout();

            // snapshot select box
            _queryComboBox = new QComboBox();
            _queryComboBox->addItems({"All", "Single", "Time Range", "Index Range"});
            _queryComboBox->setCurrentIndex(3);

            typeLayout->addWidget(_queryComboBox);

            connect(_queryComboBox, &QComboBox::currentTextChanged, this, &This::showSelectedFormForQuery);
            connect(_queryComboBox, &QComboBox::currentTextChanged, this, &This::queryChanged);

            // query type select box
            auto queryTargetLayout = new QHBoxLayout();

            typeLayout->addLayout(queryTargetLayout);
            _pageLayout->addLayout(typeLayout);
        }

        // Build and add the forms.
        _queryForms.clear();
        addForm("All", new SnapshotFormAll());
        addForm("Single", new SnapshotFormSingle());
        addForm("Time Range", new SnapshotFormTimeRange());
        addForm("Index Range", new SnapshotFormIndexRange());

        showSelectedFormForQuery(_queryComboBox->currentText());
    }

    void SnapshotSelectorWidget::showSelectedFormForQuery(QString selected)
    {
        for (auto& [name, form] : _queryForms)
        {
            form->setVisible(name == selected);
        }
    }

    void SnapshotSelectorWidget::addForm(const QString& key, SnapshotForm* form)
    {
        auto r = _queryForms.emplace(key, form);
        _pageLayout->addWidget(r.first->second);
        //connect(r.first->second, &SnapshotForm::queryChanged, this, &This::updateSelector);
    }

    void SnapshotSelectorWidget::hideAllForms()
    {
        for (auto& [_, form] : _queryForms)
        {
            form->setVisible(false);
        }
    }


}
