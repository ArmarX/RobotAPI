#include "SnapshotForm.h"

#include <QCheckBox>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QFormLayout>
#include <QGridLayout>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSpinBox>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/armem/core/ice_conversions.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>
#include <RobotAPI/libraries/armem_gui/gui_utils.h>


namespace armarx::armem::gui
{

    client::query::SnapshotSelector SnapshotForm::makeEntitySelector()
    {
        client::query::SnapshotSelector s;
        fillEntitySelector(s);
        return s;
    }

    void SnapshotForm::setDateTimeDisplayFormat(QDateTimeEdit* dt)
    {
        dt->setDisplayFormat("yyyy-MM-dd HH:mm:ss");
    }


    SnapshotFormAll::SnapshotFormAll()
    {
    }
    void SnapshotFormAll::fillEntitySelector(client::query::SnapshotSelector& selector)
    {
        selector.all();
    }



    SnapshotFormSingle::SnapshotFormSingle()
    {
        const QDateTime now = QDateTime::currentDateTime();

        label = new QLabel("Timestamp:");

        dateTime = new QDateTimeEdit(now);
        dateTime->setDisabled(true);
        setDateTimeDisplayFormat(dateTime);

        microseconds = new armarx::gui::LeadingZeroSpinBox(6, 10);
        microseconds->setDisabled(true);
        microseconds->setMinimum(0);
        microseconds->setMaximum(1000 * 1000 - 1);
        microseconds->setSingleStep(1);
        microseconds->setValue(static_cast<int>(now.toMSecsSinceEpoch() % 1000) * 1000);

        QHBoxLayout* timestampLayout = new QHBoxLayout();
        timestampLayout->addWidget(dateTime);
        timestampLayout->addWidget(microseconds);

        latest = new QCheckBox("Latest");
        latest->setChecked(true);

        QGridLayout* layout = new QGridLayout(this);
        layout->addWidget(label, 0, 0);
        layout->addLayout(timestampLayout, 0, 1);
        layout->addWidget(latest, 0, 2);

        connect(latest, &QCheckBox::toggled, dateTime, &QDateTimeEdit::setDisabled);
        connect(latest, &QCheckBox::toggled, microseconds, &QSpinBox::setDisabled);

        connect(dateTime, &QDateTimeEdit::dateTimeChanged, this, &SnapshotForm::queryChanged);
        connect(microseconds, QOverload<int>::of(&QSpinBox::valueChanged), this, &SnapshotForm::queryChanged);
        connect(latest, &QCheckBox::toggled, this, &SnapshotForm::queryChanged);
    }


    void SnapshotFormSingle::fillEntitySelector(client::query::SnapshotSelector& selector)
    {
        const Time time = latest->isChecked()
                          ? Time::Invalid()
                          : (Time(Duration::Seconds(dateTime->dateTime().toSecsSinceEpoch()))
                             + Duration::MicroSeconds(microseconds->value()));
        selector.atTime(time);
    }


    SnapshotFormTimeRange::SnapshotFormTimeRange()
    {
        const QDateTime now = QDateTime::currentDateTime();

        fromLabel = new QLabel("From:");

        fromDateTime = new QDateTimeEdit(now.addSecs(-60));
        fromDateTime->setDisabled(true);
        setDateTimeDisplayFormat(fromDateTime);

        fromBegin = new QCheckBox("Begin");
        fromBegin->setChecked(true);

        toLabel = new QLabel("To:");

        toDateTime = new QDateTimeEdit(now.addSecs(60));
        toDateTime->setDisabled(true);
        setDateTimeDisplayFormat(toDateTime);

        toEnd = new QCheckBox("End");
        toEnd->setChecked(true);

        QGridLayout* layout = new QGridLayout(this);
        int row = 0;

        layout->addWidget(fromLabel, row, 0);
        layout->addWidget(fromDateTime, row, 1);
        layout->addWidget(fromBegin, row, 2);
        ++row;

        layout->addWidget(toLabel, row, 0);
        layout->addWidget(toDateTime, row, 1);
        layout->addWidget(toEnd, row, 2);
        ++row;

        connect(fromBegin, &QCheckBox::toggled, fromDateTime, &QDateTimeEdit::setDisabled);
        connect(toEnd, &QCheckBox::toggled, toDateTime, &QDateTimeEdit::setDisabled);

        connect(fromBegin, &QCheckBox::toggled, this, &SnapshotForm::queryChanged);
        connect(fromDateTime, &QDateTimeEdit::dateTimeChanged, this, &SnapshotForm::queryChanged);
        connect(toEnd, &QCheckBox::toggled, this, &SnapshotForm::queryChanged);
        connect(toDateTime, &QDateTimeEdit::dateTimeChanged, this, &SnapshotForm::queryChanged);
    }

    void SnapshotFormTimeRange::fillEntitySelector(client::query::SnapshotSelector& selector)
    {
        Time defaults = Time::Invalid();
        Time min = defaults, max = defaults;

        if (!fromBegin->isChecked())
        {
            min = Time(Duration::MilliSeconds(fromDateTime->dateTime().toMSecsSinceEpoch()));
        }
        if (!toEnd->isChecked())
        {
            max = Time(Duration::MilliSeconds(toDateTime->dateTime().toMSecsSinceEpoch()))
                  + Duration::MicroSeconds(int(1e6) - 1);
        }

        selector.timeRange(min, max);
    }


    SnapshotFormIndexRange::SnapshotFormIndexRange()
    {
        firstLabel = new QLabel("First:");
        firstSpinBox = new QSpinBox();
        firstBegin = new QCheckBox("Begin");

        firstBegin->setChecked(false);
        firstSpinBox->setDisabled(firstBegin->isChecked());

        lastLabel = new QLabel("To:");
        lastSpinBox = new QSpinBox();
        lastEnd = new QCheckBox("End");

        lastEnd->setChecked(true);
        lastSpinBox->setDisabled(lastEnd->isChecked());

        std::initializer_list<QSpinBox*> sbs { firstSpinBox, lastSpinBox };
        for (QSpinBox* sb : sbs)
        {
            sb->setMinimum(-1000);
            sb->setMaximum(1000);
        }
        firstSpinBox->setValue(-2);
        lastSpinBox->setValue(-1);

        QString tooltip = "Python index semantics: Negative indices count from the end (-1 is the last entry).";
        firstSpinBox->setToolTip(tooltip);
        lastSpinBox->setToolTip(tooltip);


        QGridLayout* layout = new QGridLayout(this);
        int row = 0;

        layout->addWidget(firstLabel, row, 0);
        layout->addWidget(firstSpinBox, row, 1);
        layout->addWidget(firstBegin, row, 2);
        ++row;

        layout->addWidget(lastLabel, row, 0);
        layout->addWidget(lastSpinBox, row, 1);
        layout->addWidget(lastEnd, row, 2);
        ++row;

        connect(firstBegin, &QCheckBox::toggled, firstSpinBox, &QSpinBox::setDisabled);
        connect(lastEnd, &QCheckBox::toggled, lastSpinBox, &QSpinBox::setDisabled);

        connect(firstBegin, &QCheckBox::toggled, this, &SnapshotForm::queryChanged);
        connect(firstSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &SnapshotForm::queryChanged);
        connect(lastEnd, &QCheckBox::toggled, this, &SnapshotForm::queryChanged);
        connect(lastSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &SnapshotForm::queryChanged);
    }

    void SnapshotFormIndexRange::fillEntitySelector(client::query::SnapshotSelector& selector)
    {
        long first = 0, last = -1;

        if (!firstBegin->isChecked())
        {
            first = firstSpinBox->value();
        }
        if (!lastEnd->isChecked())
        {
            last = lastSpinBox->value();
        }

        selector.indexRange(first, last);
    }

}
