/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SpeechX::ArmarXObjects::gui
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "lifecycle.h"

#include <ArmarXCore/core/ManagedIceObject.h>


namespace armarx::gui
{
    LifecycleClient::~LifecycleClient()
    {
    }

    void LifecycleClient::onInit(ManagedIceObject* component)
    {
        if (component)
        {
            onInit(*component);
        }
    }

    void LifecycleClient::onConnect(ManagedIceObject* component)
    {
        if (component)
        {
            onConnect(*component);
        }
    }

    void LifecycleClient::onDisconnect(ManagedIceObject* component)
    {
        if (component)
        {
            onDisconnect(*component);
        }
    }

    void LifecycleClient::onInit(ManagedIceObject& component)
    {
        (void) component;
        onInit();
    }

    void LifecycleClient::onConnect(ManagedIceObject& component)
    {
        (void) component;
        onConnect();
    }

    void LifecycleClient::onDisconnect(ManagedIceObject& component)
    {
        (void) component;
        onDisconnect();
    }

    void LifecycleClient::onInit()
    {
    }

    void LifecycleClient::onConnect()
    {
    }

    void LifecycleClient::onDisconnect()
    {
    }

}

void armarx::gui::connectLifecycle(LifecycleServer* server, LifecycleClient* client)
{
    if (server && client)
    {
        server->connect(server, &LifecycleServer::initialized, [client](ManagedIceObject * o)
        {
            client->onInit(o);
        });
        server->connect(server, &LifecycleServer::connected, [client](ManagedIceObject * o)
        {
            client->onConnect(o);
        });
        server->connect(server, &LifecycleServer::disconnected, [client](ManagedIceObject * o)
        {
            client->onDisconnect(o);
        });
    }
}
