#pragma once

#include <map>

#include <QTreeWidget>

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>

#include <RobotAPI/libraries/armem_gui/TreeWidgetBuilder.h>


namespace armarx::armem::gui::memory
{

    class TreeWidget : public QTreeWidget
    {
        Q_OBJECT
        using This = TreeWidget;

    public:

        TreeWidget();

        void update(const armem::wm::Memory& memory);
        void update(const std::map<std::string, const armem::wm::Memory*>& memories);

        std::optional<MemoryID> selectedID() const;


    signals:

        /// Tree contents have changed.
        void updated();

        /// The selected item or its contents have changed.
        void selectedItemChanged(const MemoryID& id);

        void itemSelected(const MemoryID& id);

        void memorySelected(const MemoryID& id);
        void coreSegmentSelected(const MemoryID& id);
        void providerSegmentSelected(const MemoryID& id);
        void entitySelected(const MemoryID& id);
        void snapshotSelected(const MemoryID& id);
        void instanceSelected(const MemoryID& id);

        void actionsMenuRequested(const MemoryID& memoryID, QWidget* parent,
                const QPoint& pos, QMenu* menu);


    private slots:

        void handleSelection();


    private:

        void initWidget();
        void initBuilders();

        void updateChildren(const armem::wm::Memory& memory, QTreeWidget* tree);
        void updateChildren(const std::map<std::string, const armem::wm::Memory*>& memories, QTreeWidget* tree);

        void updateChildren(const armem::wm::Memory& memory, QTreeWidgetItem* memoryItem);
        void updateChildren(const armem::wm::CoreSegment& coreSeg, QTreeWidgetItem* coreSegItem);
        void updateChildren(const armem::wm::ProviderSegment& provSeg, QTreeWidgetItem* provSegItem);
        void updateChildren(const armem::wm::Entity& entity, QTreeWidgetItem* entityItem);
        void updateChildren(const armem::wm::EntitySnapshot& snapshot, QTreeWidgetItem* snapshotItem);
        void updateChildren(const armem::wm::EntityInstance& data, QTreeWidgetItem* parent);

        void prepareTreeContextMenu(const QPoint& pos);

        template <class MemoryItemT>
        QTreeWidgetItem* makeItem(const std::string& key, const MemoryItemT& memoryItem);
        QTreeWidgetItem* makeItem(const std::string& key, const std::string& levelName, const MemoryID& id);

        void updateItemItem(const armem::base::detail::MemoryItem& level, QTreeWidgetItem* item);
        template <class ContainerT>
        void updateContainerItem(const ContainerT& container, QTreeWidgetItem* item);


    private:

        MapTreeWidgetBuilder<std::string, const wm::Memory*> memoryBuilder;
        TreeWidgetBuilder<wm::CoreSegment> coreSegmentBuilder;
        TreeWidgetBuilder<wm::ProviderSegment> provSegmentBuilder;
        TreeWidgetBuilder<wm::Entity> entityBuilder;
        TreeWidgetBuilder<wm::EntitySnapshot> snapshotBuilder;
        TreeWidgetBuilder<wm::EntityInstance> instanceBuilder;

        std::optional<MemoryID> _selectedID;
        /// While this is false, do not handle selection updates.
        bool handleSelections = true;

        enum class Columns
        {
            KEY = 0,
            SIZE = 1,
            TYPE = 2,
            LEVEL = 3,
            ID = 4,
            NUM = 5
        };
    };

}

namespace armarx::armem::gui
{
    using MemoryTreeWidget = memory::TreeWidget;
}
