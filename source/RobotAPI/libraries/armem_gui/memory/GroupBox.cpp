#include "GroupBox.h"

#include <QCheckBox>
#include <QGuiApplication>
#include <QHBoxLayout>
#include <QScreen>
#include <QSplitter>
#include <QVBoxLayout>


namespace armarx::armem::gui::memory
{

    GroupBox::GroupBox(PredictionWidget::GetEntityInfoFn&& entityInfoRetriever)
    {
        QVBoxLayout* layout = new QVBoxLayout();
        this->setLayout(layout);

        QSplitter* splitter = new QSplitter(Qt::Orientation::Vertical);
        layout->addWidget(splitter);

        this->setTitle("Memory");
        int margin = 3;
        this->layout()->setContentsMargins(margin, margin, margin, margin);


        _tree = new armem::gui::MemoryTreeWidget();


        _memoryTabWidget = new QTabWidget();
        _memoryTabGroup = new QGroupBox("Queries, Predictions and Commits");
        _memoryTabGroup->setLayout(new QVBoxLayout());

        margin = 0;
        _memoryTabGroup->setContentsMargins(margin, margin, margin, margin);
        _memoryTabGroup->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Maximum);

        {
            _queryWidget = new armem::gui::QueryWidget();
            _queryWidget->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Maximum);

            _memoryTabWidget->addTab(_queryWidget, QString("Query Settings"));
        }
        {
            _snapshotSelectorWidget = new armem::gui::SnapshotSelectorWidget();
            _snapshotSelectorWidget->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Maximum);

            _memoryTabWidget->addTab(_snapshotSelectorWidget, QString("Snapshot Selection"));
        }
        {
            _predictionWidget = new armem::gui::PredictionWidget(std::move(entityInfoRetriever));
            _predictionWidget->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Maximum);

            _memoryTabWidget->addTab(_predictionWidget, QString("Prediction"));
        }
        {
            _commitWidget = new armem::gui::CommitWidget();
            _commitWidget->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Maximum);

            _memoryTabWidget->addTab(_commitWidget, QString("Commit"));
        }

        _memoryTabGroup->layout()->addWidget(_memoryTabWidget);

        splitter->addWidget(_tree);
        splitter->addWidget(_memoryTabGroup);
        // Prevent QueryGroup from growing.
        const int largeHeight = QGuiApplication::primaryScreen()->size().height();
        splitter->setSizes({largeHeight, 1});
    }

    QueryWidget* GroupBox::queryWidget() const
    {
        return _queryWidget;
    }

    QGroupBox* GroupBox::queryGroup() const
    {
        return _memoryTabGroup;
    }

    SnapshotSelectorWidget* GroupBox::snapshotSelectorWidget() const
    {
        return _snapshotSelectorWidget;
    }

    PredictionWidget* GroupBox::predictionWidget() const
    {
        return _predictionWidget;
    }

    CommitWidget* GroupBox::commitWidget() const
    {
        return _commitWidget;
    }

    MemoryTreeWidget* GroupBox::tree() const
    {
        return _tree;
    }

    armem::client::QueryInput GroupBox::queryInput() const
    {
        armem::client::query::Builder queryBuilder(_queryWidget->dataMode());
        queryBuilder
        .coreSegments().all()
        .providerSegments().all()
        .entities().all()
        .snapshots(_snapshotSelectorWidget->selector());

        return queryBuilder.buildQueryInput();
    }

}  // namespace armarx::armem::gui::memory
