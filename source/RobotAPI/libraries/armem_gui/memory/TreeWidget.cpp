#include "TreeWidget.h"

#include <RobotAPI/libraries/armem_gui/instance/sanitize_typename.h>

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/interface/armem/actions.h>

#include <SimoxUtility/algorithm/string.h>

#include <QHeaderView>
#include <QMenu>


namespace armarx::armem::gui::memory
{

    TreeWidget::TreeWidget()
    {
        initBuilders();
        initWidget();
    }

    void TreeWidget::initWidget()
    {
        clear();
        QStringList columns;
        columns.insert(int(Columns::KEY), "Key");
        columns.insert(int(Columns::SIZE), "#");
        columns.insert(int(Columns::TYPE), "Type");
        columns.insert(int(Columns::LEVEL), "Level");
        columns.insert(int(Columns::ID), "Memory ID");
        setColumnCount(columns.size());
        setHeaderLabels(columns);

        header()->setMinimumSectionSize(25);
        header()->resizeSection(int(Columns::KEY), 250);
        header()->resizeSection(int(Columns::SIZE), 40);
        header()->setTextElideMode(Qt::TextElideMode::ElideRight);


        connect(this, &This::updated, this, &This::handleSelection);
        connect(this, &QTreeWidget::currentItemChanged, this, &This::handleSelection);

        connect(this, &This::memorySelected, this, &This::itemSelected);
        connect(this, &This::coreSegmentSelected, this, &This::itemSelected);
        connect(this, &This::providerSegmentSelected, this, &This::itemSelected);
        connect(this, &This::entitySelected, this, &This::itemSelected);
        connect(this, &This::snapshotSelected, this, &This::itemSelected);
        connect(this, &This::instanceSelected, this, &This::itemSelected);

        setContextMenuPolicy(Qt::CustomContextMenu);
        connect(this, &QTreeWidget::customContextMenuRequested, this, &This::prepareTreeContextMenu);
    }

    void TreeWidget::initBuilders()
    {
        memoryBuilder.setExpand(true);
        memoryBuilder.setMakeItemFn([this](const std::string & name, const wm::Memory * memory)
        {
            return makeItem(name, *memory);
        });
        memoryBuilder.setUpdateItemFn([this](const std::string&, const wm::Memory * memory, QTreeWidgetItem * memoryItem)
        {
            updateContainerItem(*memory, memoryItem);
            if (memoryItem)
            {
                updateChildren(*memory, memoryItem);
            }
            return true;
        });

        auto nameFn = [](const auto & element)
        {
            return element.name();
        };

        coreSegmentBuilder.setExpand(true);
        coreSegmentBuilder.setNameFn(nameFn, int(Columns::KEY));
        coreSegmentBuilder.setMakeItemFn([this](const wm::CoreSegment & coreSeg)
        {
            return makeItem(coreSeg.name(), coreSeg);
        });
        coreSegmentBuilder.setUpdateItemFn([this](const wm::CoreSegment & coreSeg, QTreeWidgetItem * coreSegItem)
        {
            updateContainerItem(coreSeg, coreSegItem);
            updateChildren(coreSeg, coreSegItem);
            return true;
        });

        provSegmentBuilder.setExpand(true);
        provSegmentBuilder.setNameFn(nameFn, int(Columns::KEY));
        provSegmentBuilder.setMakeItemFn([this](const wm::ProviderSegment & provSeg)
        {
            return makeItem(provSeg.name(), provSeg);
        });
        provSegmentBuilder.setUpdateItemFn([this](const wm::ProviderSegment & provSeg, QTreeWidgetItem * provSegItem)
        {
            updateContainerItem(provSeg, provSegItem);
            updateChildren(provSeg, provSegItem);
            return true;
        });

        // entityBuilder.setExpand(true);
        entityBuilder.setNameFn(nameFn, int(Columns::KEY));
        entityBuilder.setMakeItemFn([this](const wm::Entity & entity)
        {
            return makeItem(entity.name(), entity);
        });
        entityBuilder.setUpdateItemFn([this](const wm::Entity & entity, QTreeWidgetItem *  entityItem)
        {
            updateContainerItem(entity, entityItem);
            updateChildren(entity, entityItem);
            return true;
        });

        snapshotBuilder.setMakeItemFn([this](const wm::EntitySnapshot & snapshot)
        {
            QTreeWidgetItem* item = makeItem(toDateTimeMilliSeconds(snapshot.time()), snapshot);
            item->setData(int(Columns::KEY), Qt::ItemDataRole::UserRole, QVariant(static_cast<qlonglong>(snapshot.time().toMicroSecondsSinceEpoch())));
            return item;
        });
        snapshotBuilder.setCompareFn([](const wm::EntitySnapshot & snapshot, QTreeWidgetItem * item)
        {
            return armarx::detail::compare(static_cast<qlonglong>(snapshot.time().toMicroSecondsSinceEpoch()),
                                           item->data(int(Columns::KEY), Qt::ItemDataRole::UserRole).toLongLong());
        });
        snapshotBuilder.setUpdateItemFn([this](const wm::EntitySnapshot & snapshot, QTreeWidgetItem * snapshotItem)
        {
            updateContainerItem(snapshot, snapshotItem);
            updateChildren(snapshot, snapshotItem);
            return true;
        });

        instanceBuilder.setMakeItemFn([this](const wm::EntityInstance & instance)
        {
            QTreeWidgetItem* item = makeItem("", instance);
            return item;
        });
        instanceBuilder.setCompareFn([](const wm::EntityInstance & lhs, QTreeWidgetItem * rhsItem)
        {
            return armarx::detail::compare(lhs.index(), rhsItem->text(0).toInt());
        });
        instanceBuilder.setUpdateItemFn([this](const wm::EntityInstance & instance, QTreeWidgetItem * instanceItem)
        {
            updateItemItem(instance, instanceItem);
            updateChildren(instance, instanceItem);
            return true;
        });

    }


    void TreeWidget::update(const armem::wm::Memory& memory)
    {
        // Removing elements during the update can create unwanted signals triggering selection handling.
        handleSelections = false;
        updateChildren(memory, this);
        handleSelections = true;
        emit updated();
    }

    void TreeWidget::update(const std::map<std::string, const armem::wm::Memory*>& memories)
    {
        handleSelections = false;
        updateChildren(memories, this);
        handleSelections = true;
        emit updated();
    }

    std::optional<MemoryID> TreeWidget::selectedID() const
    {
        return _selectedID;
    }

    void TreeWidget::handleSelection()
    {
        if (!handleSelections)
        {
            return;
        }
        QTreeWidgetItem* item = this->currentItem();
        if (!item)
        {
            return;
        }

        MemoryID id(item->data(int(Columns::ID), Qt::UserRole).toString().toStdString());

        // Has the selection changed due to an update? If yes, signal this.
        if (!_selectedID || id != *_selectedID)
        {
            _selectedID = id;

            const std::string levelName = item->data(int(Columns::LEVEL), Qt::UserRole).toString().toStdString();
            if (levelName == wm::Memory::getLevelName())
            {
                emit memorySelected(*_selectedID);
            }
            else if (levelName == wm::CoreSegment::getLevelName())
            {
                emit coreSegmentSelected(*_selectedID);
            }
            else if (levelName == wm::ProviderSegment::getLevelName())
            {
                emit providerSegmentSelected(*_selectedID);
            }
            else if (levelName == wm::Entity::getLevelName())
            {
                emit entitySelected(*_selectedID);
            }
            else if (levelName == wm::EntitySnapshot::getLevelName())
            {
                emit snapshotSelected(*_selectedID);
            }
            else if (levelName == wm::EntityInstance::getLevelName())
            {
                emit instanceSelected(*_selectedID);
            }
        }

        // Either selection or its contents have changed.
        emit selectedItemChanged(*_selectedID);
    }

    template <class ContainerT>
    static
    auto makeIteratorFn(const ContainerT& container)
    {
        return [&container](auto&& elementFn)
        {
            container.forEachChild(elementFn);
        };
    }


    void TreeWidget::updateChildren(const armem::wm::Memory& memory, QTreeWidget* tree)
    {
        updateChildren(std::map<std::string, const armem::wm::Memory*> {{memory.name(), &memory}}, tree);
    }

    void TreeWidget::updateChildren(const std::map<std::string, const armem::wm::Memory*>& memories, QTreeWidget* tree)
    {
        memoryBuilder.updateTree(tree, memories);
    }


    void TreeWidget::updateChildren(const armem::wm::Memory& memory, QTreeWidgetItem* memoryItem)
    {
        coreSegmentBuilder.updateTreeWithIterator(memoryItem, makeIteratorFn(memory));
    }

    void TreeWidget::updateChildren(const armem::wm::CoreSegment& coreSeg, QTreeWidgetItem* coreSegItem)
    {
        provSegmentBuilder.updateTreeWithIterator(coreSegItem, makeIteratorFn(coreSeg));
    }

    void TreeWidget::updateChildren(const armem::wm::ProviderSegment& provSeg, QTreeWidgetItem* provSegItem)
    {
        entityBuilder.updateTreeWithIterator(provSegItem, makeIteratorFn(provSeg));
    }

    void TreeWidget::updateChildren(const armem::wm::Entity& entity, QTreeWidgetItem* entityItem)
    {
        snapshotBuilder.updateTreeWithIterator(entityItem, makeIteratorFn(entity));
    }

    void TreeWidget::updateChildren(const armem::wm::EntitySnapshot& snapshot, QTreeWidgetItem* snapshotItem)
    {
        instanceBuilder.updateTreeWithIterator(snapshotItem, makeIteratorFn(snapshot));
    }

    void TreeWidget::updateChildren(const armem::wm::EntityInstance& data, QTreeWidgetItem* dataItem)
    {
        (void) data, (void) dataItem;
    }

    void TreeWidget::prepareTreeContextMenu(const QPoint& pos)
    {
        const QTreeWidgetItem* item = this->itemAt(pos);
        if (item == nullptr)
        {
            return;
        }

        MemoryID memoryID(item->data(int(Columns::ID), Qt::UserRole).toString().toStdString());
        emit actionsMenuRequested(memoryID, this, mapToGlobal(pos), nullptr);
    }

    template <class MemoryItemT>
    QTreeWidgetItem* TreeWidget::makeItem(const std::string& key, const MemoryItemT& memoryItem)
    {
        (void) key;
        return makeItem(memoryItem.getKeyString(), MemoryItemT::getLevelName(), memoryItem.id());
    }

    QTreeWidgetItem* TreeWidget::makeItem(const std::string& key, const std::string& levelName, const MemoryID& id)
    {
        QStringList columns;
        columns.insert(int(Columns::KEY), QString::fromStdString(key));
        columns.insert(int(Columns::SIZE), "");
        columns.insert(int(Columns::TYPE), "");
        columns.insert(int(Columns::LEVEL), QString::fromStdString(simox::alg::capitalize_words(levelName)));
        columns.insert(int(Columns::ID), QString::fromStdString(id.str()));

        QTreeWidgetItem* item = new QTreeWidgetItem(columns);
        item->setData(int(Columns::LEVEL), Qt::UserRole, QString::fromStdString(levelName));
        item->setData(int(Columns::ID), Qt::UserRole, QString::fromStdString(id.str()));
        item->setTextAlignment(int(Columns::SIZE), Qt::AlignRight);
        return item;
    }

    void TreeWidget::updateItemItem(const armem::base::detail::MemoryItem& level, QTreeWidgetItem* item)
    {
        (void) level, (void) item;
    }

    template <class ContainerT>
    void TreeWidget::updateContainerItem(const ContainerT& container, QTreeWidgetItem* item)
    {
        updateItemItem(container, item);
        item->setText(int(Columns::SIZE), QString::number(container.size()));

        // Does not work
        if constexpr(std::is_base_of_v<base::detail::AronTyped, ContainerT>)
        {
            const base::detail::AronTyped& cast = static_cast<const base::detail::AronTyped&>(container);
            std::string typeName;
            if (cast.aronType())
            {
                typeName = cast.aronType()->getFullName();
                typeName = instance::sanitizeTypeName(typeName);
            }
            else
            {
                typeName = "(no Aron type)";
            }
            item->setText(int(Columns::TYPE), QString::fromStdString(typeName));
        }
    }

}
