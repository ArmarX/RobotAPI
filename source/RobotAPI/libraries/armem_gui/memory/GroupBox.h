#pragma once

#include <QGroupBox>

#include <RobotAPI/libraries/armem_gui/memory/TreeWidget.h>
#include <RobotAPI/libraries/armem_gui/commit_widget/CommitWidget.h>
#include <RobotAPI/libraries/armem_gui/prediction_widget/PredictionWidget.h>
#include <RobotAPI/libraries/armem_gui/query_widgets/QueryWidget.h>


class QCheckBox;
class QLabel;


namespace armarx::armem::gui::memory
{

    class GroupBox : public QGroupBox
    {
        Q_OBJECT
        using This = GroupBox;

    public:
        GroupBox(PredictionWidget::GetEntityInfoFn&& entityInfoRetriever);

        TreeWidget* tree() const;
        QGroupBox* queryGroup() const;
        QueryWidget* queryWidget() const;
        SnapshotSelectorWidget* snapshotSelectorWidget() const;
        PredictionWidget* predictionWidget() const;
        CommitWidget* commitWidget() const;

        armem::client::QueryInput queryInput() const;


    public slots:

    signals:

    private slots:

    signals:


    private:

        TreeWidget* _tree;

        QTabWidget* _memoryTabWidget;
        QGroupBox* _memoryTabGroup;
        QueryWidget* _queryWidget;
        SnapshotSelectorWidget* _snapshotSelectorWidget;
        PredictionWidget* _predictionWidget;
        CommitWidget* _commitWidget;

    };

}

namespace armarx::armem::gui
{
    using MemoryGroupBox = memory::GroupBox;
}
