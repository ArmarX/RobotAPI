#pragma once

#include <string>


namespace armarx::armem::gui::instance
{

    extern const std::string rawMemoryIDTypeName;
    extern const std::string sanitizedMemoryIDTypeName;

    std::string sanitizeTypeName(const std::string& typeName);

}

