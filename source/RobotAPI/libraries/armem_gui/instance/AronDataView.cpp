#include "AronDataView.h"
namespace armarx::armem::gui::instance
{

    AronDataView::AronDataView()
    {
        Logging::setTag("AronDataView");
    }

    void
    AronDataView::update(aron::data::DictPtr aronData, aron::type::ObjectPtr aronType)
    {
        currentData = aronData;
        currentAronType = aronType;
        update();
    }

    void
    AronDataView::update()
    {
        if (currentData)
        {
            updateData(currentData, currentAronType);
            updateImageView(currentData);

            emit updated();
        }
    }

    aron::data::DictPtr
    AronDataView::getData()
    {
        return currentData;
    }
} // namespace armarx::armem::gui::instance
