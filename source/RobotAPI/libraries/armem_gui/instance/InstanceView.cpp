#include "InstanceView.h"
#include <new>

#include <QAction>
#include <QApplication>
#include <QClipboard>
#include <QGroupBox>
#include <QHeaderView>
#include <QImage>
#include <QLabel>
#include <QLayout>
#include <QMenu>
#include <QSplitter>
#include <QTreeWidget>
#include <QVBoxLayout>

#include <SimoxUtility/algorithm/string.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/aron/converter/json/NLohmannJSONConverter.h>

#include <RobotAPI/libraries/armem/aron/MemoryID.aron.generated.h>
#include <RobotAPI/libraries/armem/core/aron_conversions.h>

#include <RobotAPI/libraries/armem_gui/gui_utils.h>
#include <RobotAPI/libraries/armem_gui/instance/sanitize_typename.h>
#include <RobotAPI/libraries/armem_gui/instance/serialize_path.h>
#include <RobotAPI/libraries/armem_gui/instance/tree_builders/DataTreeBuilder.h>
#include <RobotAPI/libraries/armem_gui/instance/tree_builders/TypedDataTreeBuilder.h>
#include <RobotAPI/libraries/armem_gui/instance/tree_visitors/TreeTypedJSONConverter.h>

#include "MemoryIDTreeWidgetItem.h"
#include "WidgetsWithToolbar.h"


namespace armarx::armem::gui::instance
{

    InstanceView::InstanceView()
    {
        Logging::setTag("InstanceView");

        treeItemInstanceID = new MemoryIDTreeWidgetItem({"Instance ID"});
        treeItemInstanceID->addKeyChildren();

        treeItemMetadata = new QTreeWidgetItem({"Metadata"});
        treeItemMetadata->addChild(new QTreeWidgetItem({"Confidence"}));
        treeItemMetadata->addChild(new QTreeWidgetItem({"Time Created"}));
        treeItemMetadata->addChild(new QTreeWidgetItem({"Time Sent"}));
        treeItemMetadata->addChild(new QTreeWidgetItem({"Time Arrived"}));

        QList<QTreeWidgetItem*> items = {treeItemInstanceID, treeItemMetadata};
        tree->insertTopLevelItems(0, items);

        treeItemInstanceID->setExpanded(true);
        treeItemMetadata->setExpanded(false);
    }

    void InstanceView::update(const MemoryID& id, const wm::Memory& memory)
    {
        aron::type::ObjectPtr aronType = nullptr;
        const armem::wm::EntityInstance* instance = nullptr;
        try
        {
            instance = &memory.getInstance(id);
            if (useTypeInfo)
            {
                aronType = memory.getProviderSegment(id).aronType();
            }
        }
        catch (const armem::error::ArMemError& e)
        {
            showErrorMessage(e.what());
        }
        if (instance)
        {
            update(*instance, aronType);
        }
    }

    void InstanceView::update(const wm::EntityInstance& instance, aron::type::ObjectPtr aronType)
    {
        currentInstance = instance;
        currentAronType = aronType;
        update();
    }

    void InstanceView::update()
    {
        if (currentInstance)
        {
            updateInstanceID(currentInstance->id());
            updateMetaData(currentInstance->metadata());
            updateData(currentInstance->data(), currentAronType);
            updateImageView(currentInstance->data());

            emit updated();
        }
    }

    void InstanceView::updateInstanceID(const MemoryID& id)
    {
        treeItemInstanceID->setInstanceID(id, int(Columns::VALUE));
    }

    void InstanceView::updateMetaData(const wm::EntityInstanceMetadata& metadata)
    {
        std::vector<std::string> items =
        {
            std::to_string(metadata.confidence),
            armem::toDateTimeMilliSeconds(metadata.timeCreated),
            armem::toDateTimeMilliSeconds(metadata.timeSent),
            armem::toDateTimeMilliSeconds(metadata.timeArrived)
        };
        ARMARX_CHECK_EQUAL(static_cast<size_t>(treeItemMetadata->childCount()), items.size());
        int i = 0;
        for (const std::string& item : items)
        {
            treeItemMetadata->child(i++)->setText(int(Columns::VALUE), QString::fromStdString(item));
        }
    }

    QMenu* InstanceView::buildActionsMenu(const QPoint& pos)
    {
        auto* parentMenu = DataView::buildActionsMenu(pos);

        const QTreeWidgetItem* item = tree->itemAt(pos);
        if (item == this->treeItemInstanceID && currentInstance.has_value())
        {
            if (QAction* action = makeActionCopyMemoryID(currentInstance->id()))
            {
                parentMenu->addAction(action);
            }
        }

        return parentMenu;
    }

    void InstanceView::prepareTreeContextMenu(const QPoint& pos)
    {
        if (currentInstance.has_value())
        {
            auto* menu = buildActionsMenu(pos);

            emit actionsMenuRequested(currentInstance->id(), this, tree->mapToGlobal(pos),
                                      menu->actions().isEmpty() ? nullptr : menu);
        }
    }

    aron::data::DictPtr InstanceView::getData()
    {
        if (currentInstance)
        {
            return currentInstance->data();
        }
        return nullptr;
    }

} // namespace armarx::armem::gui::instance

