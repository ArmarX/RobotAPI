#include "InstanceViewList.h"

#include <QAction>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QImage>
#include <QLabel>
#include <QLayout>
#include <QMenu>
#include <QSplitter>
#include <QVBoxLayout>

#include <SimoxUtility/algorithm/string.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronObjectTypeNavigator.h>
#include <RobotAPI/libraries/armem_gui/gui_utils.h>


namespace armarx::armem::gui::instance
{

    InstanceViewList::InstanceViewList()
    {
    }

    void InstanceViewList::setStatusLabel(QLabel* statusLabel)
    {
        this->statusLabel = statusLabel;
    }

    void InstanceViewList::setUseTypeInfo(bool enable)
    {
        this->useTypeInfo = enable;
        update();
    }


}
