#include "DataDisplayVisitor.h"

#include <RobotAPI/libraries/aron/core/data/variant/All.h>

namespace armarx::aron
{

    std::string DataDisplayVisitor::getValue(const data::VariantPtr& n)
    {
        DataDisplayVisitor v;
        data::visit(v, n);
        return v.value.str();
    }

    void DataDisplayVisitor::visitDict(const data::VariantPtr& n)
    {
        auto x = data::Dict::DynamicCastAndCheck(n);
        value << x->childrenSize() << " items";
    }

    void DataDisplayVisitor::visitList(const data::VariantPtr& n)
    {
        auto x = data::List::DynamicCastAndCheck(n);
        value << x->childrenSize() << " items";
    }

    void DataDisplayVisitor::visitBool(const data::VariantPtr& b)
    {
        auto x = data::Bool::DynamicCastAndCheck(b);
        if (x->getValue())
        {
            value << "true";
        }
        else
        {
            value << "false";
        }
    }

    void DataDisplayVisitor::visitDouble(const data::VariantPtr& n)
    {
        auto x = data::Double::DynamicCastAndCheck(n);
        value << x->getValue();
    }

    void DataDisplayVisitor::visitFloat(const data::VariantPtr& n)
    {
        auto x = data::Float::DynamicCastAndCheck(n);
        value << x->getValue();
    }

    void DataDisplayVisitor::visitInt(const data::VariantPtr& n)
    {
        auto x = data::Int::DynamicCastAndCheck(n);
        value << x->getValue();
    }

    void DataDisplayVisitor::visitLong(const data::VariantPtr& n)
    {
        auto x = data::Long::DynamicCastAndCheck(n);
        value << x->getValue();
    }

    void DataDisplayVisitor::visitString(const data::VariantPtr& n)
    {
        auto x = data::String::DynamicCastAndCheck(n);
        value << "'" << x->getValue() << "'";
    }

    void DataDisplayVisitor::visitNDArray(const data::VariantPtr& n)
    {
        auto x = data::NDArray::DynamicCastAndCheck(n);
        value << "shape " << aron::data::NDArray::DimensionsToString(x->getShape()) << ", type '" << x->getType() << "'";
    }


}
