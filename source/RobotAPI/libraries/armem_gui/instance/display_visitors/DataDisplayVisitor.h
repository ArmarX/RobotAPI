#pragma once

#include <sstream>

#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>


namespace armarx::aron
{

    class DataDisplayVisitor : public aron::data::ConstVariantVisitor
    {
    public:
        static std::string getValue(const data::VariantPtr& n);


    public:

        std::stringstream value;

        void visitDict(const data::VariantPtr& n) override;
        void visitList(const data::VariantPtr& n) override;

        void visitBool(const data::VariantPtr& b) override;
        void visitDouble(const data::VariantPtr& n) override;
        void visitFloat(const data::VariantPtr& n) override;
        void visitInt(const data::VariantPtr& n) override;
        void visitLong(const data::VariantPtr& n) override;
        void visitString(const data::VariantPtr& n) override;

        void visitNDArray(const data::VariantPtr& n) override;

    };

}
