#pragma once

#include <sstream>
#include <string>

#include <Eigen/Core>

#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>


namespace armarx::aron
{

    class TypedDataDisplayVisitor : public aron::data::ConstTypedVariantVisitor
    {
    public:

        static std::string getValue(const type::VariantPtr& type, const data::VariantPtr& data);


    public:

        TypedDataDisplayVisitor();


        std::stringstream value;


        void visitDict(const data::VariantPtr& data, const type::VariantPtr& type) override;
        void visitObject(const data::VariantPtr& data, const type::VariantPtr& type) override;

        void visitList(const data::VariantPtr& data, const type::VariantPtr& type) override;
        void visitTuple(const data::VariantPtr& data, const type::VariantPtr& type) override;
        // What about Pair? currently defaulted

        void visitBool(const data::VariantPtr& data, const type::VariantPtr& type) override;
        void visitDouble(const data::VariantPtr& data, const type::VariantPtr& type) override;
        void visitFloat(const data::VariantPtr& data, const type::VariantPtr& type) override;
        void visitInt(const data::VariantPtr& data, const type::VariantPtr& type) override;
        void visitLong(const data::VariantPtr& data, const type::VariantPtr& type) override;
        void visitString(const data::VariantPtr& data, const type::VariantPtr& type) override;


        void visitMatrix(const data::VariantPtr& data, const type::VariantPtr& type) override;
        void visitQuaternion(const data::VariantPtr& data, const type::VariantPtr& type) override;
        void visitImage(const data::VariantPtr& data, const type::VariantPtr& type) override;
        void visitPointCloud(const data::VariantPtr& data, const type::VariantPtr& type) override;
        // What about NDArray? currently defaulted


    protected:

        std::string coeffSep;
        Eigen::IOFormat eigenIof;


    private:

        template <typename ScalarT>
        void processMatrix(const type::Matrix&, const data::NDArray& data);
        void processQuaternion(const data::NDArray& data);

        void processPose(const type::Matrix&, const data::NDArray& data);
        void processPosition(const type::Matrix&, const data::NDArray& data);
        void processOrientation(const type::Quaternion&, const data::NDArray& data);

        void setStreamPrecision();
        void setStreamPrecision(std::ostream& os);

    };

}
