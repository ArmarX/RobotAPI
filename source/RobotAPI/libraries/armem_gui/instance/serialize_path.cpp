#include "serialize_path.h"

#include <RobotAPI/libraries/aron/core/Path.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <SimoxUtility/algorithm/string.h>

#include <QString>
#include <QStringList>


QStringList armarx::armem::gui::instance::serializePath(const aron::Path& path)
{
    QStringList qpath;
    qpath.append(QString::fromStdString(path.getRootIdentifier()));
    qpath.append(QString::fromStdString(path.getDelimeter()));
    for (const std::string& item : path.getPath())
    {
        qpath.append(QString::fromStdString(item));
    }
    return qpath;
}

armarx::aron::Path armarx::armem::gui::instance::deserializePath(const QStringList& qpath)
{
    ARMARX_CHECK_GREATER_EQUAL(qpath.size(), 2);
    std::vector<std::string> pathItems;
    for (int i = 2; i < qpath.size(); ++i)
    {
        pathItems.push_back(qpath.at(i).toStdString());
    }
    aron::Path path(pathItems);
    path.setRootIdentifier(qpath.at(0).toStdString());
    path.setDelimeter(qpath.at(1).toStdString());
    return path;
}
