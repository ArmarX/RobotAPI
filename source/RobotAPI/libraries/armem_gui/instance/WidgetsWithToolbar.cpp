/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu)
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "WidgetsWithToolbar.h"

#include <QAction>
#include <QHBoxLayout>
#include <QPushButton>
#include <QToolBar>
#include <QVBoxLayout>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "InstanceView.h"


namespace armarx::armem::gui::instance
{

    WidgetsWithToolbar::WidgetsWithToolbar(QWidget* parent) : QWidget(parent)
    {
        const int margin = 0;
        this->setContentsMargins(margin, margin, margin, margin);

        _layout = new QHBoxLayout();
        this->setLayout(_layout);
        _layout->setContentsMargins(margin, margin, margin, margin);
        _layout->setSpacing(0);


        toolbar = new QToolBar();
        toolbar->setOrientation(Qt::Orientation::Vertical);
        toolbar->setContentsMargins(margin, margin, margin, margin);

        QAction* action = toolbar->addAction(QIcon(":/icons/dialog-close.ico"), "Close", [this]()
        {
            this->close();
        });
        action->setToolTip("Remove this instance view");

        _layout->addWidget(toolbar);
    }


    void WidgetsWithToolbar::addWidget(QWidget* widget)
    {
        ARMARX_CHECK_GREATER_EQUAL(_layout->count(), 1);
        _layout->insertWidget(_layout->count() - 1, widget);
    }

    void WidgetsWithToolbar::close()
    {
        // ARMARX_IMPORTANT << "Closing instance view ...";
        emit closing();

        this->hide();
        this->deleteLater();
    }

}

