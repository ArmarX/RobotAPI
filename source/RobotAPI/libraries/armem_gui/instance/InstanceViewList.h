#pragma once

#include <optional>

#include <QWidget>

#include <RobotAPI/libraries/aron/aroncore/navigators/typenavigator/AronObjectTypeNavigator.h>

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>


class QGroupBox;
class QLabel;
class QSplitter;
class QTreeWidget;
class QTreeWidgetItem;


namespace armarx::armem::gui::instance
{
    namespace instance
    {
        class ImageView;
    }


    class InstanceViewList : public QWidget
    {
        Q_OBJECT
        using This = InstanceViewList;


    public:

        InstanceViewList();

        void setStatusLabel(QLabel* statusLabel);
        void setUseTypeInfo(bool enable);



    signals:

        void updated();


    private slots:


    private:



    private:


        QSplitter* splitter;

        QLabel* statusLabel = nullptr;
        bool useTypeInfo = true;

    };

}

namespace armarx::armem::gui
{
    using InstanceViewList = instance::InstanceViewList;
}
