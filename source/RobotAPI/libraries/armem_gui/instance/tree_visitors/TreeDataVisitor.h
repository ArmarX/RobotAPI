#pragma once

#include <sstream>
#include <stack>

#include <QTreeWidget>
#include <QLabel>

#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/armem_gui/instance/tree_visitors/TreeDataVisitorBase.h>


namespace armarx::armem::gui::instance
{

    class TreeDataVisitor :
        public aron::data::RecursiveConstVariantVisitor,
        public TreeDataVisitorBase
    {
    public:

        using TreeDataVisitorBase::TreeDataVisitorBase;


        void visitDictOnEnter(const aron::data::VariantPtr& n) override
        {
            const std::string key = n->getPath().getLastElement();
            _visitEnter(key, "Dict", n->childrenSize());
        }
        void visitDictOnExit(const aron::data::VariantPtr&) override
        {
            _visitExit();
        }

        void visitListOnEnter(const aron::data::VariantPtr& n) override
        {
            const std::string key = n->getPath().getLastElement();
            _visitEnter(key, "List", n->childrenSize());
        }
        void visitListOnExit(const aron::data::VariantPtr&) override
        {
            _visitExit();
        }


        void visitBool(const aron::data::VariantPtr& b) override
        {
            const auto x = *aron::data::Bool::DynamicCastAndCheck(b);
            const std::string key = b->getPath().getLastElement();
            this->addValueRow(key, x, "Bool");
        }
        void visitDouble(const aron::data::VariantPtr& d) override
        {
            const auto x = *aron::data::Double::DynamicCastAndCheck(d);
            const std::string key = d->getPath().getLastElement();
            this->addValueRow(key, x, "Double");
        }
        void visitFloat(const aron::data::VariantPtr& f) override
        {
            const auto x = *aron::data::Float::DynamicCastAndCheck(f);
            const std::string key = f->getPath().getLastElement();
            this->addValueRow(key, x, "Float");
        }
        void visitInt(const aron::data::VariantPtr& i) override
        {
            const auto x = *aron::data::Int::DynamicCastAndCheck(i);
            const std::string key = i->getPath().getLastElement();
            this->addValueRow(key, x, "Int");
        }
        void visitLong(const aron::data::VariantPtr& l) override
        {
            const auto x = *aron::data::Long::DynamicCastAndCheck(l);
            const std::string key = l->getPath().getLastElement();
            this->addValueRow(key, x, "Long");
        }
        void visitString(const aron::data::VariantPtr& string) override
        {
            const auto x = *aron::data::String::DynamicCastAndCheck(string);
            const std::string key = string->getPath().getLastElement();
            this->addValueRow(key, x, "String");
        }

        void visitNDArray(const aron::data::VariantPtr& array) override
        {
            const auto x = *aron::data::NDArray::DynamicCastAndCheck(array);
            const std::string key = array->getPath().getLastElement();
            this->addValueRow(key, x, "ND Array");
        }

    };


}
