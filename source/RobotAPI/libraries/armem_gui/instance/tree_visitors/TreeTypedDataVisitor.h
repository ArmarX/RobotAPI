#pragma once

#include <sstream>
#include <stack>

#include <QTreeWidget>
#include <QLabel>

#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>

#include <RobotAPI/libraries/armem_gui/instance/sanitize_typename.h>
#include <RobotAPI/libraries/armem_gui/instance/serialize_path.h>

#include <RobotAPI/libraries/armem_gui/instance/tree_visitors/TreeDataVisitorBase.h>

#include <SimoxUtility/algorithm/string/string_tools.h>


namespace armarx::armem::gui::instance
{

    class TreeTypedDataVisitor :
        public aron::data::RecursiveConstTypedVariantVisitor,
        public TreeDataVisitorBase
    {
    public:

        using TreeDataVisitorBase::TreeDataVisitorBase;


        void visitDictOnEnter(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();
            _visitEnter(key, sanitizeTypeName(type->getFullName()), data->childrenSize());
        }
        void visitDictOnExit(const aron::data::VariantPtr&, const aron::type::VariantPtr&) override
        {
            _visitExit();
        }

        void visitObjectOnEnter(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();
            _visitEnter(key, sanitizeTypeName(type->getFullName()), data->childrenSize());
        }
        void visitObjectOnExit(const aron::data::VariantPtr&, const aron::type::VariantPtr&) override
        {
            _visitExit();
        }

        void visitListOnEnter(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();
            _visitEnter(key, sanitizeTypeName(type->getFullName()), data->childrenSize());
        }
        void visitListOnExit(const aron::data::VariantPtr&, const aron::type::VariantPtr&) override
        {
            _visitExit();
        }

        void visitTupleOnEnter(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();
            _visitEnter(key, sanitizeTypeName(type->getFullName()), data->childrenSize());
        }
        void visitTupleOnExit(const aron::data::VariantPtr&, const aron::type::VariantPtr&) override
        {
            _visitExit();
        }
        // What about Pair??


        void visitBool(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();

            auto d = *aron::data::Bool::DynamicCastAndCheck(data);
            auto t = *aron::type::Bool::DynamicCastAndCheck(type);
            this->addValueRow(key, d, t);
        }
        void visitDouble(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();

            auto d = *aron::data::Double::DynamicCastAndCheck(data);
            auto t = *aron::type::Double::DynamicCastAndCheck(type);
            this->addValueRow(key, d, t);
        }
        void visitFloat(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();

            auto d = *aron::data::Float::DynamicCastAndCheck(data);
            auto t = *aron::type::Float::DynamicCastAndCheck(type);
            this->addValueRow(key, d, t);
        }
        void visitInt(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();

            auto d = *aron::data::Int::DynamicCastAndCheck(data);
            auto t = *aron::type::Int::DynamicCastAndCheck(type);
            this->addValueRow(key, d, t);
        }
        void visitLong(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();

            auto d = *aron::data::Long::DynamicCastAndCheck(data);
            auto t = *aron::type::Long::DynamicCastAndCheck(type);
            this->addValueRow(key, d, t);
        }
        void visitString(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();

            auto d = *aron::data::String::DynamicCastAndCheck(data);
            auto t = *aron::type::String::DynamicCastAndCheck(type);
            this->addValueRow(key, d, t);
        }
        /*void visitDateTime(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();

            auto d = *aron::data::Long::DynamicCastAndCheck(data);
            auto t = *aron::type::DateTime::DynamicCastAndCheck(type);
            this->addValueRow(key, d, t);
        }*/


        void visitMatrix(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();

            auto d = *aron::data::NDArray::DynamicCastAndCheck(data);
            auto t = *aron::type::Matrix::DynamicCastAndCheck(type);
            this->addValueRow(key, d, t);
        }
        void visitQuaternion(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();

            auto d = *aron::data::NDArray::DynamicCastAndCheck(data);
            auto t = *aron::type::Quaternion::DynamicCastAndCheck(type);
            this->addValueRow(key, d, t);
        }
        void visitPointCloud(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();

            auto d = *aron::data::NDArray::DynamicCastAndCheck(data);
            auto t = *aron::type::PointCloud::DynamicCastAndCheck(type);
            this->addValueRow(key, d, t);
        }
        void visitImage(const aron::data::VariantPtr& data, const aron::type::VariantPtr& type) override
        {
            ARMARX_CHECK_NOT_NULL(data);
            ARMARX_CHECK_NOT_NULL(type);
            const std::string key = data->getPath().getLastElement();

            auto d = *aron::data::NDArray::DynamicCastAndCheck(data);
            auto t = *aron::type::Image::DynamicCastAndCheck(type);
            this->addValueRow(key, d, t);
        }
        // What aboud NDArray


    protected:

        template <class DataNavigatorT, class TypeNavigatorT>
        bool addValueRow(const std::string& key, const DataNavigatorT& data, const TypeNavigatorT& type)
        {
            if (items.size() > 0)
            {
                QTreeWidgetItem* item = makeItem(key, data, type);
                items.top()->addChild(item);

                item->setData(columnKey, Qt::UserRole, serializePath(data.getPath()));
                item->setData(columnType, Qt::UserRole, int(type.getDescriptor()));

                if (false) // remove?
                {
                    QFont font;
                    font.setFamily("Consolas");
                    font.setStyleHint(QFont::Monospace);
                    font.setFixedPitch(true);
                    font.setPointSize(10);
                    item->setFont(columnValue, font);
                }
            }
            return true;
        }

        template <class DataNavigatorT, class TypeNavigatorT>
        QTreeWidgetItem* makeItem(const std::string& key, const DataNavigatorT& data, const TypeNavigatorT& type) const
        {
            std::stringstream ss;
            try
            {
                this->streamValueText(data, type, ss);
            }
            catch (const aron::error::AronException& e)
            {
                ss << "x ";
                TreeDataVisitorBase::streamValueText(data, ss);
                std::stringstream es;
                es << e.what();
                ss << simox::alg::replace_all(es.str(), "\n", " | ");
            }
            return new QTreeWidgetItem(this->makeValueRowStrings(key, ss.str(), sanitizeTypeName(type.getFullName())));
        }


        template <class DataNavigatorT, class TypeNavigatorT>
        void streamValueText(const DataNavigatorT& data, const TypeNavigatorT& type, std::stringstream& ss) const
        {
            // Fallback to type-agnostic (but data-aware).
            (void) type;
            TreeDataVisitorBase::streamValueText(data, ss);
        }

        using TreeDataVisitorBase::streamValueText;
        //void streamValueText(const aron::data::Long& data, const aron::type::DateTime& type, std::stringstream& ss) const;
        void streamValueText(const aron::data::NDArray& data, const aron::type::Matrix& type, std::stringstream& ss) const;

        void streamPoseText(const aron::data::NDArray& data, const aron::type::Matrix& type, std::stringstream& ss) const;
        void streamPositionText(const aron::data::NDArray& data, const aron::type::Matrix& type, std::stringstream& ss) const;
        void streamOrientationText(const aron::data::NDArray& data, const aron::type::Matrix& type, std::stringstream& ss) const;


        std::string coeffSep = "  ";
    };

}

