#include "TreeDataVisitorBase.h"

#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::armem::gui::instance
{

    TreeDataVisitorBase::TreeDataVisitorBase()
    {
    }

    TreeDataVisitorBase::TreeDataVisitorBase(QTreeWidgetItem* root)
    {
        rootItems.push(root);
    }

    TreeDataVisitorBase::~TreeDataVisitorBase()
    {}

    void TreeDataVisitorBase::setColumns(int key, int value, int type)
    {
        this->columnKey = key;
        this->columnType = type;
        this->columnValue = value;
    }

    bool TreeDataVisitorBase::_visitEnter(const std::string& key, const std::string& type, size_t numChildren)
    {
        QTreeWidgetItem* item = nullptr;
        if (rootItems.size() > 0)
        {
            item = rootItems.top();
            rootItems.pop();
        }
        else
        {
            QStringList cols;
            cols.insert(columnKey, QString::fromStdString(key));
            cols.insert(columnValue, QString::number(numChildren) + " items");
            cols.insert(columnType, QString::fromStdString(type));
            item = new QTreeWidgetItem(cols);
        }
        items.push(item);
        return true;
    }

    bool TreeDataVisitorBase::_visitExit()
    {
        ARMARX_CHECK_POSITIVE(items.size());
        QTreeWidgetItem* item = items.top();
        items.pop();
        item->setExpanded(true);
        if (items.size() > 0)
        {
            items.top()->addChild(item);
        }
        else
        {
            rootItems.push(item);
        }
        return true;
    }

    void TreeDataVisitorBase::streamValueText(const aron::data::Bool& n, std::stringstream& ss) const
    {
        if (n.getValue())
        {
            ss << "true";
        }
        else
        {
            ss << "false";
        }
    }

    void TreeDataVisitorBase::streamValueText(const aron::data::String& n, std::stringstream& ss) const
    {
        ss << "'" << n.getValue() << "'";
    }

    void TreeDataVisitorBase::streamValueText(const aron::data::NDArray& n, std::stringstream& ss) const
    {
        ss << "shape " << aron::data::NDArray::DimensionsToString(n.getShape());
    }

}
