#include "TreeTypedJSONConverter.h"

#include <SimoxUtility/json/eigen_conversion.h>

#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/armem/core/Time.h>
#include <RobotAPI/libraries/aron/converter/eigen/EigenConverter.h>
#include <RobotAPI/libraries/aron/core/Exception.h>
#include <RobotAPI/libraries/aron/core/data/variant/All.h>

namespace armarx::armem::gui::instance
{
    TreeTypedJSONConverter::TreeTypedJSONConverter()
    {
        jsonStack.push({"", nlohmann::json()});
    }

    const nlohmann::json&
    TreeTypedJSONConverter::getJSON()
    {
        nlohmann::json& obj = jsonStack.top().second;
        if (obj.front().is_object())
        {
            return obj.front();
        }
        return obj;
    }

    /* We override this method because we need to handle untyped members in the hierarchy.
     * The other get*Elements() methods will either not be called with a null type or can handle it.
     */
    TreeTypedJSONConverter::MapElements
    TreeTypedJSONConverter::getObjectElements(DataInput& elementData, TypeInput& elementType)
    {
        return GetObjectElementsWithNullType(elementData, elementType);
    }

    void
    TreeTypedJSONConverter::visitObjectOnEnter(DataInput& elementData, TypeInput& elementType)
    {
        std::string key = "";
        aron::Path path;
        if (elementData)
        {
            path = elementData->getPath();
        }
        else if (elementType)
        {
            path = elementType->getPath();
        }
        else
        {
            return;
        }

        try
        {
            key = path.getLastElement();
        }
        catch (const aron::error::AronException& e)
        {
            // This happens when we start at the top-level object.
        }
        jsonStack.push({key, nlohmann::json(nlohmann::json::value_t::object)});
    }

    void
    TreeTypedJSONConverter::visitObjectOnExit(DataInput& /*elementData*/,
                                              TypeInput& /*elementType*/)
    {
        auto obj = jsonStack.top();
        jsonStack.pop();
        insertIntoJSON(obj.first, obj.second);
    }

    void
    TreeTypedJSONConverter::visitDictOnEnter(DataInput& elementData, TypeInput& elementType)
    {
        this->visitObjectOnEnter(elementData, elementType);
    }

    void
    TreeTypedJSONConverter::visitDictOnExit(DataInput& elementData, TypeInput& elementType)
    {
        this->visitObjectOnExit(elementData, elementType);
    }

    void
    TreeTypedJSONConverter::visitPairOnEnter(DataInput& elementData, TypeInput& elementType)
    {
        this->visitListOnEnter(elementData, elementType);
    }

    void
    TreeTypedJSONConverter::visitPairOnExit(DataInput& elementData, TypeInput& elementType)
    {
        this->visitListOnExit(elementData, elementType);
    }

    void
    TreeTypedJSONConverter::visitTupleOnEnter(DataInput& elementData, TypeInput& elementType)
    {
        this->visitListOnEnter(elementData, elementType);
    }

    void
    TreeTypedJSONConverter::visitTupleOnExit(DataInput& elementData, TypeInput& elementType)
    {
        this->visitListOnExit(elementData, elementType);
    }

    void
    TreeTypedJSONConverter::visitListOnEnter(DataInput& elementData, TypeInput& /*elementType*/)
    {
        const std::string key = elementData->getPath().getLastElement();
        jsonStack.push({key, nlohmann::json(nlohmann::json::value_t::array)});
    }

    void
    TreeTypedJSONConverter::visitListOnExit(DataInput& /*elementData*/, TypeInput& /*elementType*/)
    {
        auto list = jsonStack.top();
        jsonStack.pop();
        insertIntoJSON(list.first, list.second);
    }

    void
    TreeTypedJSONConverter::visitMatrix(DataInput& elementData, TypeInput& elementType)
    {
        const std::string key = elementData->getPath().getLastElement();
        auto nd = *aron::data::NDArray::DynamicCastAndCheck(elementData);
        auto t = *aron::type::Matrix::DynamicCastAndCheck(elementType);
        nlohmann::json obj;
        if (nd.getType() == "float")
        {
            Eigen::Map<Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic>> m(
                reinterpret_cast<float*>(nd.getData()), t.getRows(), t.getCols());
            Eigen::to_json(obj, m);
        }
        else if (nd.getType() == "double")
        {
            Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>> m(
                reinterpret_cast<double*>(nd.getData()), t.getRows(), t.getCols());
            Eigen::to_json(obj, m);
        }
        else
        {
            obj = handleGenericNDArray(nd);
        }
        insertIntoJSON(key, obj);
    }

    void
    TreeTypedJSONConverter::visitNDArray(DataInput& elementData, TypeInput& /*elementType*/)
    {
        const std::string key = elementData->getPath().getLastElement();
        auto nd = *aron::data::NDArray::DynamicCastAndCheck(elementData);
        insertIntoJSON(key, handleGenericNDArray(nd));
    }

    void
    TreeTypedJSONConverter::visitQuaternion(DataInput& elementData, TypeInput& /*elementType*/)
    {
        const std::string key = elementData->getPath().getLastElement();
        auto nd = *aron::data::NDArray::DynamicCastAndCheck(elementData);
        nlohmann::json obj;
        Eigen::to_json(obj, aron::converter::AronEigenConverter::ConvertToQuaternionf(nd));
        insertIntoJSON(key, obj);
    }

    void
    TreeTypedJSONConverter::visitImage(DataInput& elementData, TypeInput& /*elementType*/)
    {
        const std::string key = elementData->getPath().getLastElement();
        auto nd = *aron::data::NDArray::DynamicCastAndCheck(elementData);
        insertIntoJSON(key, handleGenericNDArray(nd));
    }

    void
    TreeTypedJSONConverter::visitPointCloud(DataInput& elementData, TypeInput& /*elementType*/)
    {
        const std::string key = elementData->getPath().getLastElement();
        auto nd = *aron::data::NDArray::DynamicCastAndCheck(elementData);
        insertIntoJSON(key, handleGenericNDArray(nd));
    }

    void
    TreeTypedJSONConverter::visitIntEnum(DataInput& elementData, TypeInput& elementType)
    {
        /* Not handled by the TreeTypedDataVisitor either */
    }

    void
    TreeTypedJSONConverter::visitInt(DataInput& elementData, TypeInput& /*elementType*/)
    {
        const std::string key = elementData->getPath().getLastElement();
        int i = *aron::data::Int::DynamicCastAndCheck(elementData);
        insertIntoJSON(key, i);
    }

    void
    TreeTypedJSONConverter::visitLong(DataInput& elementData, TypeInput& /*elementType*/)
    {
        const std::string key = elementData->getPath().getLastElement();
        int64_t l = *aron::data::Long::DynamicCastAndCheck(elementData);
        insertIntoJSON(key, l);
    }

    void
    TreeTypedJSONConverter::visitFloat(DataInput& elementData, TypeInput& /*elementType*/)
    {
        const std::string key = elementData->getPath().getLastElement();
        float f = *aron::data::Float::DynamicCastAndCheck(elementData);
        insertIntoJSON(key, f);
    }

    void
    TreeTypedJSONConverter::visitDouble(DataInput& elementData, TypeInput& /*elementType*/)
    {
        const std::string key = elementData->getPath().getLastElement();
        double d = *aron::data::Double::DynamicCastAndCheck(elementData);
        insertIntoJSON(key, d);
    }

    void
    TreeTypedJSONConverter::visitBool(DataInput& elementData, TypeInput& /*elementType*/)
    {
        const std::string key = elementData->getPath().getLastElement();
        bool b = *aron::data::Bool::DynamicCastAndCheck(elementData);
        insertIntoJSON(key, b);
    }

    void
    TreeTypedJSONConverter::visitString(DataInput& elementData, TypeInput& /*elementType*/)
    {
        const std::string key = elementData->getPath().getLastElement();
        std::string s = *aron::data::String::DynamicCastAndCheck(elementData);
        insertIntoJSON(key, s);
    }

    //void
    //TreeTypedJSONConverter::visitDateTime(DataInput& elementData, TypeInput& /*elementType*/)
    /*{
        const std::string key = elementData->getPath().getLastElement();
        int64_t l = *aron::data::Long::DynamicCastAndCheck(elementData);
        armem::Time time { armem::Duration::MicroSeconds(l) };
        insertIntoJSON(key, l);
        if (!jsonStack.top().second.is_array())
        {
            insertIntoJSON(key + "_hr", armem::toDateTimeMilliSeconds(time));
        }
    }*/

    template <typename ElementType>
    void
    TreeTypedJSONConverter::insertIntoJSON(const std::string& key, const ElementType& data)
    {
        if (jsonStack.top().second.is_object())
        {
            jsonStack.top().second[key] = nlohmann::json(data);
        }
        else
        {
            jsonStack.top().second.emplace_back(data);
        }
    }

    nlohmann::json
    TreeTypedJSONConverter::handleGenericNDArray(const aron::data::NDArray& nd)
    {
        nlohmann::json ndobj;
        std::vector<int> shape = nd.getShape();
        ndobj["dimensions"] = shape;
        ndobj["type"] = nd.getType();

        int elements =
            shape.empty()
                ? 0
                : std::accumulate(std::begin(shape), std::end(shape), 1, std::multiplies<>());
        std::vector<unsigned char> d = std::vector<unsigned char>(elements);
        memcpy(d.data(), nd.getData(), elements);
        ndobj["data"] = d;
        return ndobj;
    }
} // namespace armarx::armem::gui::instance
