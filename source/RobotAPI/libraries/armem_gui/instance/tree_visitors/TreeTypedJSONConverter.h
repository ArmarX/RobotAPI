#pragma once

#include <stack>
#include <utility>

#include <SimoxUtility/json/json.hpp>

#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>

namespace armarx::armem::gui::instance
{
    class TreeTypedJSONConverter : public aron::data::RecursiveConstTypedVariantVisitor
    {
    public:
        TreeTypedJSONConverter();
        ~TreeTypedJSONConverter() override = default;

        const nlohmann::json& getJSON();

        MapElements getObjectElements(DataInput& elementData, TypeInput& elementType) override;

        void visitObjectOnEnter(DataInput& elementData, TypeInput& elementType) override;
        void visitObjectOnExit(DataInput& elementData, TypeInput& elementType) override;
        void visitDictOnEnter(DataInput& elementData, TypeInput& elementType) override;
        void visitDictOnExit(DataInput& elementData, TypeInput& elementType) override;
        void visitPairOnEnter(DataInput& elementData, TypeInput& elementType) override;
        void visitPairOnExit(DataInput& elementData, TypeInput& elementType) override;
        void visitTupleOnEnter(DataInput& elementData, TypeInput& elementType) override;
        void visitTupleOnExit(DataInput& elementData, TypeInput& elementType) override;
        void visitListOnEnter(DataInput& elementData, TypeInput& elementType) override;
        void visitListOnExit(DataInput& elementData, TypeInput& elementType) override;

        void visitMatrix(DataInput& elementData, TypeInput& elementType) override;
        void visitNDArray(DataInput& elementData, TypeInput& elementType) override;
        void visitQuaternion(DataInput& elementData, TypeInput& elementType) override;
        void visitImage(DataInput& elementData, TypeInput& elementType) override;
        void visitPointCloud(DataInput& elementData, TypeInput& elementType) override;
        void visitIntEnum(DataInput& elementData, TypeInput& elementType) override;
        void visitInt(DataInput& elementData, TypeInput& elementType) override;
        void visitLong(DataInput& elementData, TypeInput& elementType) override;
        void visitFloat(DataInput& elementData, TypeInput& elementType) override;
        void visitDouble(DataInput& elementData, TypeInput& elementType) override;
        void visitBool(DataInput& elementData, TypeInput& elementType) override;
        void visitString(DataInput& elementData, TypeInput& elementType) override;

    private:
        std::stack<std::pair<std::string, nlohmann::json>> jsonStack;

        template <typename ElementType>
        void insertIntoJSON(const std::string& key, const ElementType& data);

        static nlohmann::json handleGenericNDArray(const aron::data::NDArray& nd);
    };
} // namespace armarx::armem::gui::instance
