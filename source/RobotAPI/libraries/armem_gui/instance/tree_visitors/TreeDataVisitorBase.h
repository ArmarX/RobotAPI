#pragma once

#include <sstream>
#include <stack>

#include <QTreeWidget>
#include <QLabel>

#include <RobotAPI/libraries/aron/core/data/visitor/variant/VariantVisitor.h>


namespace armarx::armem::gui::instance
{

    class TreeDataVisitorBase
    {
    public:

        TreeDataVisitorBase();
        TreeDataVisitorBase(QTreeWidgetItem* root);
        virtual ~TreeDataVisitorBase();

        void setColumns(int key, int value, int type);


    protected:

        // Same for Dict and List
        bool _visitEnter(const std::string& key, const std::string& type, size_t numChildren);
        bool _visitExit();


        template <class Navigator>
        bool addValueRow(const std::string& key, Navigator& n, const std::string& typeName)
        {
            if (items.size() > 0)
            {
                items.top()->addChild(new QTreeWidgetItem(this->makeValueRowStrings(key, n, typeName)));
            }
            return true;
        }

        QStringList makeValueRowStrings(const std::string& key, const std::string& value, const std::string& typeName) const
        {
            QStringList cols;
            cols.insert(columnKey, QString::fromStdString(key));
            cols.insert(columnValue, QString::fromStdString(value));
            cols.insert(columnType, QString::fromStdString(typeName));
            return cols;
        }

        template <class Navigator>
        QStringList makeValueRowStrings(const std::string& key, Navigator& n, const std::string& typeName) const
        {
            std::stringstream value;
            streamValueText(n, value);
            return makeValueRowStrings(key, value.str(), typeName);
        }

        template <class Navigator>
        void streamValueText(Navigator& n, std::stringstream& ss) const
        {
            ss << n.getValue();
        }
        void streamValueText(const aron::data::Bool& n, std::stringstream& ss) const;
        void streamValueText(const aron::data::String& n, std::stringstream& ss) const;
        void streamValueText(const aron::data::NDArray& n, std::stringstream& ss) const;


    public:

        std::stack<QTreeWidgetItem*> rootItems;
        std::stack<QTreeWidgetItem*> items;

        int columnKey = 0;
        int columnValue = 1;
        int columnType = 2;

    };

}
