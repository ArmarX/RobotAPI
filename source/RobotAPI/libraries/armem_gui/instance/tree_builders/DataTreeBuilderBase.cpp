#include "DataTreeBuilderBase.h"

#include <QTreeWidgetItem>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
// #include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/armem_gui/TreeWidgetBuilder.h>
#include <RobotAPI/libraries/armem_gui/instance/sanitize_typename.h>
#include <RobotAPI/libraries/armem_gui/instance/display_visitors/DataDisplayVisitor.h>


namespace armarx::armem::gui::instance
{

    const int keyIndexRole = Qt::UserRole + 10;


    DataTreeBuilderBase::DataTreeBuilderBase()
    {
    }

    DataTreeBuilderBase::~DataTreeBuilderBase()
    {}

    void DataTreeBuilderBase::setColumns(int key, int value, int type)
    {
        this->columnKey = key;
        this->columnType = type;
        this->columnValue = value;
    }


    QTreeWidgetItem* DataTreeBuilderBase::makeItem(const std::string& key) const
    {
        return new QTreeWidgetItem(QStringList{QString::fromStdString(key)});
    }

    QTreeWidgetItem* DataTreeBuilderBase::makeItem(size_t key) const
    {
        QTreeWidgetItem* item = new QTreeWidgetItem();
        item->setData(0, keyIndexRole, static_cast<int>(key));
        return item;
    }

    void DataTreeBuilderBase::setRowTexts(QTreeWidgetItem* item, const std::string& key, const std::string& value, const std::string& typeName) const
    {
        item->setText(columnKey, QString::fromStdString(key));
        item->setText(columnValue, QString::fromStdString(value));
        item->setText(columnType, QString::fromStdString(typeName));
    }


    void DataTreeBuilderBase::setRowTexts(
        QTreeWidgetItem* item, const std::string& key, const aron::data::VariantPtr& data)
    {
        if (!data)
        {
            return;
        }
        const std::string value = armarx::aron::DataDisplayVisitor::getValue(data);
        setRowTexts(item, key, value, sanitizeTypeName(data->getFullName()));
    }

    DataTreeBuilderBase::DictBuilder DataTreeBuilderBase::getDictBuilder() const
    {
        DictBuilder builder;
        builder.setCompareFn([](const std::string & key, QTreeWidgetItem * item)
        {
            return armarx::detail::compare(key, item->text(0).toStdString());
        });
        builder.setMakeItemFn([this](const std::string & key)
        {
            return this->makeItem(key);
        });
        return builder;
    }


    DataTreeBuilderBase::ListBuilder DataTreeBuilderBase::getListBuilder() const
    {
        ListBuilder builder;
        builder.setCompareFn([](size_t key, QTreeWidgetItem * item)
        {
            QVariant itemKey = item->data(0, keyIndexRole);
            ARMARX_CHECK_EQUAL(itemKey.type(), QVariant::Type::Int);
            // << VAROUT(key) << " | " << VAROUT(item->text(0).toStdString()) << itemKey.typeName();

            return armarx::detail::compare(static_cast<int>(key), itemKey.toInt());
        });
        builder.setMakeItemFn([this](size_t key)
        {
            return this->makeItem(key);
        });
        return builder;
    }


    std::vector<size_t> DataTreeBuilderBase::getIndex(size_t size) const
    {
        std::vector<size_t> index;
        index.reserve(size);
        for (size_t i = 0; i < size; ++i)
        {
            index.push_back(i);
        }
        return index;
    }

}
