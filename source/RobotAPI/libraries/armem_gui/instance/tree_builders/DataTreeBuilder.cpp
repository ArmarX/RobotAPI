#include "DataTreeBuilder.h"

#include <QTreeWidgetItem>

#include <RobotAPI/libraries/armem_gui/TreeWidgetBuilder.h>


namespace armarx::armem::gui::instance
{

    DataTreeBuilder::DataTreeBuilder()
    {
    }

    void DataTreeBuilder::updateTree(QTreeWidgetItem* parent, const aron::data::DictPtr& data)
    {
        DictBuilder builder = getDictBuilder();
        builder.setUpdateItemFn([this, &data](const std::string & key, QTreeWidgetItem * item)
        {
            auto child = data->getElement(key);
            this->update(item, key, child, data->getPath());
            return true;
        });

        builder.updateTreeWithContainer(parent, data->getAllKeys());
    }

    void DataTreeBuilder::updateTree(QTreeWidgetItem* parent, const aron::data::ListPtr& data)
    {

        ListBuilder builder = getListBuilder();
        builder.setUpdateItemFn([this, &data](size_t key, QTreeWidgetItem * item)
        {
            auto child = data->getElement(key);
            this->update(item, std::to_string(key), child, data->getPath());
            return true;
        });

        builder.updateTreeWithContainer(parent, getIndex(data->childrenSize()));
    }


    void
    DataTreeBuilder::update(QTreeWidgetItem* item,
                            const std::string& key,
                            const aron::data::VariantPtr& data,
                            const aron::Path& parentPath)
    {
        if (data)
        {
            this->setRowTexts(item, key, data);

            if (auto cast = aron::data::Dict::DynamicCast(data))
            {
                updateTree(item, cast);
            }
            else if (auto cast = aron::data::List::DynamicCast(data))
            {
                updateTree(item, cast);
            }
        }
        else
        {
            // Optional data?
            this->setRowTexts(item, key, "(none)");
            auto empty = std::make_shared<aron::data::Dict>(parentPath.withElement(key));
            updateTree(item, empty);
        }

    }
}
