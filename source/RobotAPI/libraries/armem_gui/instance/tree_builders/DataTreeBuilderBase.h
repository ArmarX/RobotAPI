#pragma once

#include <string>

#include <RobotAPI/libraries/aron/core/data/variant/Variant.h>


namespace armarx
{
    template <class ContainerT> struct TreeWidgetBuilder;
}

class QTreeWidgetItem;


namespace armarx::armem::gui::instance
{

    class DataTreeBuilderBase
    {
    public:

        DataTreeBuilderBase();
        virtual ~DataTreeBuilderBase();

        void setColumns(int key, int value, int type);


    protected:

        using DictBuilder = armarx::TreeWidgetBuilder<std::string>;
        using ListBuilder = armarx::TreeWidgetBuilder<size_t>;

        DictBuilder getDictBuilder() const;
        ListBuilder getListBuilder() const;
        std::vector<size_t> getIndex(size_t size) const;


        QTreeWidgetItem* makeItem(const std::string& key) const;
        QTreeWidgetItem* makeItem(size_t key) const;

        void setRowTexts(QTreeWidgetItem* item, const std::string& key, const std::string& value, const std::string& typeName = "") const;
        void setRowTexts(QTreeWidgetItem* item, const std::string& key, const aron::data::VariantPtr& data);


    public:

        int columnKey = 0;
        int columnValue = 1;
        int columnType = 2;

    };

}
