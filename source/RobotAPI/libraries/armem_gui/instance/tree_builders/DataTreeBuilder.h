#pragma once

#include <string>

#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/List.h>

#include "DataTreeBuilderBase.h"


namespace armarx::armem::gui::instance
{

    class DataTreeBuilder : public DataTreeBuilderBase
    {
    public:

        DataTreeBuilder();

        void updateTree(QTreeWidgetItem* parent, const aron::data::DictPtr& data);
        void updateTree(QTreeWidgetItem* parent, const aron::data::ListPtr& data);


    protected:
        void update(QTreeWidgetItem* item,
                    const std::string& key,
                    const aron::data::VariantPtr& data,
                    const aron::Path& parentPath);
    };
}
