#pragma once

#include <string>

#include <RobotAPI/libraries/aron/core/data/variant/Variant.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/data/variant/container/List.h>

#include <RobotAPI/libraries/aron/core/type/variant/Variant.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/Dict.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/List.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/Object.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/Pair.h>
#include <RobotAPI/libraries/aron/core/type/variant/container/Tuple.h>

#include "DataTreeBuilderBase.h"


class QStringList;


namespace armarx::armem::gui::instance
{

    class TypedDataTreeBuilder : public DataTreeBuilderBase
    {
    public:

        TypedDataTreeBuilder();


        void updateTree(QTreeWidgetItem* parent,
                        const aron::type::Dict& type,
                        const aron::data::Dict& data);
        void updateTree(QTreeWidgetItem* parent,
                        const aron::type::AnyObject& type,
                        const aron::data::Dict& data);
        void updateTree(QTreeWidgetItem* parent,
                        const aron::type::Object& type,
                        const aron::data::Dict& data);

        void updateTree(QTreeWidgetItem* parent,
                        const aron::type::List& type,
                        const aron::data::List& data);
        void updateTree(QTreeWidgetItem* parent,
                        const aron::type::Pair& type,
                        const aron::data::List& data);
        void updateTree(QTreeWidgetItem* parent,
                        const aron::type::Tuple& type,
                        const aron::data::List& data);


    protected:

        void updateDispatch(QTreeWidgetItem* item,
                    const std::string& key,
                    const aron::type::VariantPtr& type,
                    const aron::data::VariantPtr& data);

        void update(QTreeWidgetItem* item,
                    const std::string& key,
                    const aron::type::VariantPtr& type,
                    const aron::data::VariantPtr& data);

        void update(QTreeWidgetItem* item,
                    const std::string& key,
                    const aron::data::VariantPtr& data);

        template <class DataT, class TypeT>
        void _updateTree(QTreeWidgetItem* item, TypeT& type, DataT& data);

    };


}
