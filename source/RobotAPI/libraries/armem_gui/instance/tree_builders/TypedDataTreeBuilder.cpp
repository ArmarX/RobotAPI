#include "TypedDataTreeBuilder.h"

#include <QTreeWidgetItem>

#include <RobotAPI/libraries/aron/common/aron_conversions.h>
#include <RobotAPI/libraries/armem/core/aron_conversions.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/aron/MemoryID.aron.generated.h>

#include <RobotAPI/libraries/armem_gui/TreeWidgetBuilder.h>
#include <RobotAPI/libraries/armem_gui/instance/serialize_path.h>
#include <RobotAPI/libraries/armem_gui/instance/sanitize_typename.h>
#include <RobotAPI/libraries/armem_gui/instance/display_visitors/TypedDataDisplayVisitor.h>
#include <RobotAPI/libraries/armem_gui/instance/MemoryIDTreeWidgetItem.h>
#include <RobotAPI/libraries/armem_gui/instance/tree_builders/DataTreeBuilder.h>


namespace armarx::armem::gui::instance
{

    TypedDataTreeBuilder::TypedDataTreeBuilder()
    {
    }


    void TypedDataTreeBuilder::updateTree(
        QTreeWidgetItem* parent,
        const aron::type::Dict& type,
        const aron::data::Dict& data)
    {
        auto childType = type.getAcceptedType();
        DictBuilder builder = getDictBuilder();
        builder.setUpdateItemFn([this, &childType, &data](const std::string & key, QTreeWidgetItem * item)
        {
            auto childData = data.getElement(key);
            if (childData)
            {
                this->updateDispatch(item, key, childType, childData);
            }
            return true;
        });

        builder.updateTreeWithContainer(parent, data.getAllKeys());
    }

    void TypedDataTreeBuilder::updateTree(
        QTreeWidgetItem* parent,
        const aron::type::AnyObject& type,
        const aron::data::Dict& data)
    {
        DictBuilder builder = getDictBuilder();
        builder.setUpdateItemFn([this, &data](const std::string & key, QTreeWidgetItem * item)
        {
            auto childData = data.getElement(key);
            if (childData)
            {
                this->updateDispatch(item, key, nullptr, childData);
            }
            return true;
        });

        builder.updateTreeWithContainer(parent, data.getAllKeys());
    }


    void TypedDataTreeBuilder::updateTree(
        QTreeWidgetItem* parent,
        const aron::type::Object& type,
        const aron::data::Dict& data)
    {
        DictBuilder builder = getDictBuilder();
        builder.setMakeItemFn([this, &type](const std::string & key) -> QTreeWidgetItem*
        {
            if (type.hasMemberType(key)
               && type.getMemberType(key)->getFullName() == instance::rawMemoryIDTypeName)
            {
                MemoryIDTreeWidgetItem* item = new MemoryIDTreeWidgetItem({QString::fromStdString(key)});
                item->addKeyChildren();
                return item;
            }
            else
            {
                return this->makeItem(key);
            }
        });
        builder.setUpdateItemFn([this, &type, &data](const std::string & key, QTreeWidgetItem * item)
        {
            auto childData = data.getElement(key);

            // We need this check here because getMemberType(key) throws
            // instead of returning nullptr if the type doesn't have the key.
            if (type.hasMemberType(key))
            {
                this->updateDispatch(item, key, type.getMemberType(key), childData);
            }
            else
            {
                this->updateDispatch(item, key, nullptr, childData);
            }
            return true;
        });

        builder.updateTreeWithContainer(parent, data.getAllKeys());
    }


    void TypedDataTreeBuilder::updateTree(QTreeWidgetItem* parent,
                                          const aron::type::List& type,
                                          const aron::data::List& data)
    {
        auto childType = type.getAcceptedType();
        auto children = data.getChildren();

        ListBuilder builder = getListBuilder();
        builder.setUpdateItemFn([this, &children, &childType](size_t key, QTreeWidgetItem * item)
        {
            if (auto childData = children.at(key))
            {
                this->updateDispatch(item, std::to_string(key), childType, childData);
            }
            return true;
        });

        builder.updateTreeWithContainer(parent, getIndex(children.size()));
    }


    void TypedDataTreeBuilder::updateTree(
        QTreeWidgetItem* parent,
        const aron::type::Pair& type,
        const aron::data::List& data)
    {
        ARMARX_CHECK_EQUAL(data.childrenSize(), 2);
        auto childTypes = type.getAcceptedTypes();

        ListBuilder builder = getListBuilder();
        builder.setUpdateItemFn([this, &data, &childTypes](size_t i, QTreeWidgetItem * item)
        {
            auto childType = i == 0 ? childTypes.first : childTypes.second;
            auto childData = data.getElement(static_cast<unsigned int>(i));

            this->updateDispatch(item, std::to_string(i), childType, childData);
            return true;
        });

        builder.updateTreeWithContainer(parent, getIndex(data.childrenSize()));
    }


    void TypedDataTreeBuilder::updateTree(
        QTreeWidgetItem* parent,
        const aron::type::Tuple& type,
        const aron::data::List& data)
    {
        // Allows tuples where the data list is longer than the type tuple -
        // is that desired behavior?

        auto childTypes = type.getAcceptedTypes();

        ListBuilder builder = getListBuilder();
        builder.setUpdateItemFn([this, &data, &childTypes](size_t i, QTreeWidgetItem * item)
        {
            auto childType = (i < childTypes.size()) ? childTypes.at(i) : nullptr;
            auto childData = data.getElement(static_cast<unsigned int>(i));

            this->updateDispatch(item, std::to_string(i), childType, childData);
            return true;
        });

        builder.updateTreeWithContainer(parent, getIndex(data.childrenSize()));
    }


    /*! Used so that elements in the data that don't appear in the type
     *  can still be shown in the GUI if type information is enabled
     *  (otherwise, they would be hidden).
     */
    void TypedDataTreeBuilder::updateDispatch(
        QTreeWidgetItem* item,
        const std::string& key,
        const aron::type::VariantPtr& type,
        const aron::data::VariantPtr& data)
    {
        if (type)
        {
            this->update(item, key, type, data);
        }
        else
        {
            this->update(item, key, data);
        }
    }


    void TypedDataTreeBuilder::update(
        QTreeWidgetItem* item,
        const std::string& key,
        const aron::type::VariantPtr& type,
        const aron::data::VariantPtr& data)
    {
        using namespace aron;

        const std::string value = data ? aron::TypedDataDisplayVisitor::getValue(type, data) : "(none)";
        std::string typeName = instance::sanitizeTypeName(type->getFullName());
        switch (type->getMaybe())
        {
            case aron::type::Maybe::OPTIONAL:
                typeName = "Optional[" + typeName + "]";
                break;
            default:
                break;
        }

        setRowTexts(item, key, value, typeName);

        item->setData(columnKey, Qt::UserRole, data ? instance::serializePath(data->getPath()) : QStringList());
        item->setData(columnType, Qt::UserRole, static_cast<int>(type->getDescriptor()));

        if (typeName == sanitizedMemoryIDTypeName)
        {
            MemoryIDTreeWidgetItem* memoryIDItem = dynamic_cast<MemoryIDTreeWidgetItem*>(item);
            const auto dictData = aron::data::Dict::DynamicCastAndCheck(data);
            if (memoryIDItem && dictData)
            {
                arondto::MemoryID dto;
                // Because fromAron does not take refs -.-
                dto.fromAron(dictData);

                MemoryID id = aron::fromAron<MemoryID>(dto);
                memoryIDItem->setInstanceID(id);
                return;  // Done, no recursion.
            }
        }

        // We pass empty containers if data is null so that subitems of the data are deleted.
        auto emptyDict = aron::data::Dict(type->getPath());
        auto emptyList = aron::data::List(type->getPath());
        if (const auto d = aron::data::Dict::DynamicCast(data); const auto t = type::Object::DynamicCast(type))
        {
            _updateTree(item, *t, d ? *d : emptyDict);
        }
        else if (const auto d = aron::data::Dict::DynamicCast(data); const auto t = type::Dict::DynamicCast(type))
        {
            _updateTree(item, *t, d ? *d : emptyDict);
        }
        else if (const auto d = aron::data::List::DynamicCast(data); const auto t = type::List::DynamicCast(type))
        {
            _updateTree(item, *t, d ? *d : emptyList);
        }
        else if (const auto d = aron::data::List::DynamicCast(data); const auto t = type::Pair::DynamicCast(type))
        {
            _updateTree(item, *t, d ? *d : emptyList);
        }
        else if (const auto d = aron::data::List::DynamicCast(data); const auto t = type::Tuple::DynamicCast(type))
        {
            _updateTree(item, *t, d ? *d : emptyList);
        }
        else if (const auto d = aron::data::Dict::DynamicCast(data); const auto t = type::AnyObject::DynamicCast(type))
        {
            _updateTree(item, *t, d ? *d : emptyDict);
        }
    }

    
    void TypedDataTreeBuilder::update(QTreeWidgetItem* item,
        const std::string& key,
        const aron::data::VariantPtr& data)
    {
        if (data)
        {
            this->setRowTexts(item, key, data);

            item->setData(columnKey, Qt::UserRole,
                    data ? instance::serializePath(data->getPath()) : QStringList());

            if (auto cast = aron::data::Dict::DynamicCast(data))
            {
                DataTreeBuilder builder;
                builder.updateTree(item, cast);
            }
            else if (auto cast = aron::data::List::DynamicCast(data))
            {
                DataTreeBuilder builder;
                builder.updateTree(item, cast);
            }
        }
        else
        {
            this->setRowTexts(item, key, "(none)");
        }
    }


    template <class DataT, class TypeT>
    void TypedDataTreeBuilder::_updateTree(QTreeWidgetItem* item, TypeT& type, DataT& data)
    {
        updateTree(item, type, data);
    }

}
