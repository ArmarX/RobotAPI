#include "sanitize_typename.h"

#include <sstream>

#include <SimoxUtility/algorithm/string.h>

#include <RobotAPI/libraries/armem/aron/MemoryID.aron.generated.h>


const std::string armarx::armem::gui::instance::rawMemoryIDTypeName = armarx::armem::arondto::MemoryID::ToAronType()->getFullName();
const std::string armarx::armem::gui::instance::sanitizedMemoryIDTypeName = "MemoryID";


namespace
{
    std::string remove_wrap(const std::string& string, const std::string& prefix, const std::string& suffix)
    {
        if (simox::alg::starts_with(string, prefix) && simox::alg::ends_with(string, suffix))
        {
            return string.substr(prefix.size(), string.size() - prefix.size() - suffix.size());
        }
        else
        {
            return string;
        }
    }
}

std::string armarx::armem::gui::instance::sanitizeTypeName(const std::string& typeName)
{
    if (typeName == rawMemoryIDTypeName)
    {
        return sanitizedMemoryIDTypeName;
    }

    namespace s = simox::alg;
    std::string n = typeName;
    n = s::replace_all(n, "armarx::aron::type::", "");
    n = s::replace_all(n, "armarx::aron::data::", "");

    if (s::starts_with(n, "Object<"))
    {
        n = remove_wrap(n, "Object<", ">");
        const std::string del = "::"; // remove namespace
        size_t find = n.rfind(del);
        if (find != n.npos)
        {
            find += del.size();  // include del
            std::stringstream ss;
            ss << n.substr(find) << "    (" << n.substr(0, find - del.size()) << ")";
            n = ss.str();
        }
    }
    else
    {
        if (s::contains(n, "<")) // another containertype
        {
            std::string container = n.substr(0, n.find("<"));
            std::string subtype = remove_wrap(n, (container + "<"), ">");
            subtype = sanitizeTypeName(subtype);
            n = n.substr(0, n.find("<")+1) +  subtype + ">";
        }
    }

    return n;

}
