#include "GroupBox.h"

#include <QCheckBox>
#include <QHBoxLayout>
#include <QVBoxLayout>


namespace armarx::armem::gui::instance
{

    GroupBox::GroupBox()
    {
        QVBoxLayout* layout = new QVBoxLayout();
        this->setLayout(layout);

        view = new armem::gui::InstanceView();

        useTypeInfoCheckBox = new QCheckBox("Use Type Information", this);
        useTypeInfoCheckBox->setChecked(true);

        QHBoxLayout* checkBoxLayout = new QHBoxLayout();
        checkBoxLayout->setDirection(QBoxLayout::RightToLeft);
        checkBoxLayout->addWidget(useTypeInfoCheckBox);

        layout->addWidget(view);
        layout->addLayout(checkBoxLayout);

        this->setTitle("Instance View (select an entity instance on the left)");
        const int margin = 3;
        this->layout()->setContentsMargins(margin, margin, margin, margin);


        connect(view, &InstanceView::updated, this, &This::viewUpdated);
        connect(useTypeInfoCheckBox, &QCheckBox::toggled, view, &InstanceView::setUseTypeInfo);
    }

    void GroupBox::setStatusLabel(QLabel* statusLabel)
    {
        view->setStatusLabel(statusLabel);
    }

}
