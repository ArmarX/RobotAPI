/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu)
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QWidget>
#include <QImage>


namespace armarx::armem::gui::instance
{

    /**
     * @brief A widget drawing an image in itself.
     */
    class ImageView : public QWidget
    {
        Q_OBJECT
        using This = ImageView;

    public:
        ImageView(QWidget* parent = nullptr);

        void setImage(const QImage& image);


    public slots:

    signals:


    protected slots:
        void updateImage();

    signals:
        void sourceImageChanged();


    protected:

        void paintEvent(QPaintEvent* pPaintEvent) override;


    private:

        QImage sourceImage;
        QImage scaledImage;
    };
}


