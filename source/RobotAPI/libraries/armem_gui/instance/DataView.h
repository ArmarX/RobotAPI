#pragma once

#include <deque>
#include <optional>
#include <variant>

#include <QGroupBox>
#include <QMenu>
#include <QWidget>

#include <SimoxUtility/color/ColorMap.h>

#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/aron/core/Path.h>
#include <RobotAPI/libraries/aron/core/type/variant/forward_declarations.h>


class QGroupBox;
class QLabel;
class QSplitter;
class QTreeWidget;
class QTreeWidgetItem;


namespace armarx::armem::gui::instance
{
    class ImageView;
    class MemoryIDTreeWidgetItem;
    class WidgetsWithToolbar;


    class DataView : public QWidget, public armarx::Logging
    {
        Q_OBJECT

    public:
        DataView();

        virtual ~DataView() = default;

        void setStatusLabel(QLabel* statusLabel);
        void setUseTypeInfo(bool enable);

        virtual void update() = 0;

        void addDataView(DataView* dataView);


    signals:

        void updated();
        void useTypeInfoChanged(bool enable);
        void memoryIdResolutionRequested(const MemoryID& id);
        void actionsMenuRequested(const MemoryID& memoryID, QWidget* parent,
                const QPoint& pos, QMenu* menu);

    protected slots:

        virtual void prepareTreeContextMenu(const QPoint& pos);

        void showImageView(const aron::Path& elementPath);
        void removeImageView();


    private:
        QAction* makeActionResolveMemoryID(const MemoryID& id);
        std::vector<QAction*> makeActionsCopyDataToClipboard();
        std::vector<QAction*> makeActionsCopyDataToClipboard(const aron::Path& path);
        std::vector<QAction*> makeCopyActions(const aron::data::VariantPtr& element,
                                              const aron::type::VariantPtr& elementType);


    protected:
        virtual aron::data::DictPtr getData() = 0;
        virtual void updateData(const aron::data::DictPtr& data,
                                aron::type::ObjectPtr aronType = nullptr);
        virtual QMenu* buildActionsMenu(const QPoint& pos);
        QAction* makeActionCopyMemoryID(const MemoryID& id);
        void updateImageView(const aron::data::DictPtr& data);

        void showErrorMessage(const std::string& message);

        static std::optional<aron::Path> getElementPath(const QTreeWidgetItem* item);
        std::optional<MemoryID> getElementMemoryID(const aron::Path& elementPath);

    protected:
        enum class Columns
        {
            KEY = 0,
            VALUE = 1,
            TYPE = 2,
        };

        aron::type::ObjectPtr currentAronType = nullptr;
        bool useTypeInfo = true;

        QSplitter* splitter;

        QTreeWidget* tree;
        QTreeWidgetItem* treeItemData;


        class ImageView : public QGroupBox
        {
        public:
            ImageView();

            QImage convertDepth32ToRGB32(const aron::data::NDArray& aron);

            instance::ImageView* view;
            aron::Path elementPath;

            WidgetsWithToolbar* toolbar;


            struct Limits
            {
                float min = std::numeric_limits<float>::max();
                float max = -std::numeric_limits<float>::max();
            };

            /// Color map to visualize depth images.
            simox::ColorMap cmap;
            /// History over first n extremal depth values used to calibrate the colormap.
            std::deque<Limits> limitsHistory;
            /// In this context, n.
            const size_t limitsHistoryMaxSize;
        };
        ImageView* imageView = nullptr;

        QLabel* statusLabel = nullptr;
    };

} // namespace armarx::armem::gui::instance
