/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu)
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ImageView.h"

#include <QImage>
#include <QPainter>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::armem::gui::instance
{

    ImageView::ImageView(QWidget* parent) : QWidget(parent)
    {
        setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        connect(this, &This::sourceImageChanged, this, &This::updateImage);
    }

    void ImageView::setImage(const QImage& image)
    {
        sourceImage = image.copy();
        emit sourceImageChanged();
    }

    void ImageView::paintEvent(QPaintEvent* event)
    {
        (void) event;

        QPainter painter(this);
        //scaledImage = image.scaled (width(), height(), Qt::KeepAspectRatio, Qt::FastTransformation);
        scaledImage = sourceImage.scaled(width(), height(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
        painter.drawImage(0, 0, scaledImage);
    }

    void ImageView::updateImage()
    {
        update(0, 0, width(), height());
    }

}

