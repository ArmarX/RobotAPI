#include "MemoryIDTreeWidgetItem.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <RobotAPI/libraries/armem/core/MemoryID.h>


namespace armarx::armem::gui::instance
{

    void MemoryIDTreeWidgetItem::addKeyChildren()
    {
        addChild(new QTreeWidgetItem({"Memory Name"}));
        addChild(new QTreeWidgetItem({"Core Segment Name"}));
        addChild(new QTreeWidgetItem({"Provider Segment Name"}));
        addChild(new QTreeWidgetItem({"Entity Name"}));
        addChild(new QTreeWidgetItem({"Timestamp"}));
        addChild(new QTreeWidgetItem({"Instance Index"}));
    }


    void MemoryIDTreeWidgetItem::setInstanceID(const MemoryID& id, int valueColumn)
    {
        setText(valueColumn, QString::fromStdString(id.str()));

        const std::vector<std::string> items = id.getAllItems();
        ARMARX_CHECK_EQUAL(childCount(), static_cast<int>(items.size()));
        int i = 0;
        for (const std::string& item : items)
        {
            child(i++)->setText(valueColumn, QString::fromStdString(item));
        }
        // Timestamp in human-readable format
        if (id.hasTimestamp())
        {
            static const char* mu = "\u03BC";
            std::stringstream ss;
            ss << toDateTimeMilliSeconds(id.timestamp)
               << " (" << id.timestamp.toMicroSecondsSinceEpoch() << " " << mu << "s)";
            child(4)->setText(valueColumn, QString::fromStdString(ss.str()));
        }
    }

}

