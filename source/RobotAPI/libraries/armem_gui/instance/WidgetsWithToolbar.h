/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu)
* @date       2021
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <QWidget>

class QHBoxLayout;
class QVBoxLayout;
class QToolBar;


namespace armarx::armem::gui::instance
{
    class WidgetsWithToolbar : public QWidget
    {
        Q_OBJECT
        using This = WidgetsWithToolbar;

    public:

        WidgetsWithToolbar(QWidget* parent = nullptr);

        void addWidget(QWidget* widget);


    public slots:

        void close();


    signals:

        void closing();


    protected slots:

    signals:


    protected:


    public:

        QToolBar* toolbar;


    private:

        QHBoxLayout* _layout;

    };
}


