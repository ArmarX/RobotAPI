#pragma once

#include <deque>
#include <optional>

#include <QMenu>
#include <QWidget>

#include <RobotAPI/libraries/armem/core/wm/memory_definitions.h>
#include <RobotAPI/libraries/armem_gui/instance/DataView.h>


namespace armarx::armem::gui::instance
{
    class MemoryIDTreeWidgetItem;

    class InstanceView : public DataView
    {
        Q_OBJECT
        using This = InstanceView;


    public:

        InstanceView();

        void update(const MemoryID& id, const wm::Memory& memory);
        void update(const wm::EntityInstance& instance, aron::type::ObjectPtr aronType = nullptr);
        void update() override;


    signals:

        void instanceSelected(const MemoryID& id);

    private slots:

        void prepareTreeContextMenu(const QPoint& pos) override;

    private:
        aron::data::DictPtr getData() override;
        QMenu* buildActionsMenu(const QPoint& pos) override;

        void updateInstanceID(const MemoryID& id);
        void updateMetaData(const wm::EntityInstanceMetadata& metadata);

    private:

        std::optional<wm::EntityInstance> currentInstance;

        MemoryIDTreeWidgetItem* treeItemInstanceID;
        QTreeWidgetItem* treeItemMetadata;

    };

}

namespace armarx::armem::gui
{
    using InstanceView = instance::InstanceView;
}
