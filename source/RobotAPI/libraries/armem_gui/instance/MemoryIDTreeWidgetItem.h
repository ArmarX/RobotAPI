#pragma once

#include <QTreeWidgetItem>


namespace armarx::armem
{
    class MemoryID;
}

namespace armarx::armem::gui::instance
{

    class MemoryIDTreeWidgetItem : public QTreeWidgetItem
    {
    public:

        using QTreeWidgetItem::QTreeWidgetItem;

        void addKeyChildren();
        void setInstanceID(const MemoryID& id, int valueColumn = 1);


    };

}

