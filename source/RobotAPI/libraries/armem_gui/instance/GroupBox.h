#pragma once

#include <QGroupBox>

#include "InstanceView.h"


class QCheckBox;
class QLabel;


namespace armarx::armem::gui::instance
{

    class GroupBox : public QGroupBox
    {
        Q_OBJECT
        using This = GroupBox;

    public:

        GroupBox();

        void setStatusLabel(QLabel* statusLabel);


    public slots:

    signals:

        void viewUpdated();


    private slots:

    signals:


    public:

        InstanceView* view;
        QCheckBox* useTypeInfoCheckBox;

    };

}

namespace armarx::armem::gui
{
    using InstanceGroupBox = instance::GroupBox;
}
