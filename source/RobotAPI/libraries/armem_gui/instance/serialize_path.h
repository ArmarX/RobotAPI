#pragma once


namespace armarx::aron
{
    class Path;
}
class QStringList;


namespace armarx::armem::gui::instance
{

    QStringList serializePath(const aron::Path& path);
    aron::Path deserializePath(const QStringList& qpath);

}

