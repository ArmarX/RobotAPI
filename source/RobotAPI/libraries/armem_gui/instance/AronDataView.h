#pragma once

#include <QMenu>
#include <QWidget>

#include <RobotAPI/libraries/armem_gui/instance/DataView.h>

namespace armarx::armem::gui::instance
{

    class AronDataView : public DataView
    {
        Q_OBJECT
        using This = AronDataView;

    public:
        AronDataView();

        void update(aron::data::DictPtr aronData, aron::type::ObjectPtr aronType = nullptr);
        void update() override;

    private:
        aron::data::DictPtr getData() override;

    private:
        enum class Columns
        {
            KEY = 0,
            VALUE = 1,
            TYPE = 2,
        };

        aron::data::DictPtr currentData = nullptr;
    };

} // namespace armarx::armem::gui::instance

namespace armarx::armem::gui
{
    using AronDataView = instance::AronDataView;
}
