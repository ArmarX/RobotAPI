/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::armem_gui
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::armem_gui

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
// #include "../armem_gui.h"

#include <iostream>

#include <RobotAPI/libraries/armem_gui/instance/sanitize_typename.h>
#include <RobotAPI/libraries/aron/core/data/variant/All.h>
#include <RobotAPI/libraries/aron/core/type/variant/All.h>

namespace dn = armarx::aron::data;
namespace tn = armarx::aron::type;


namespace ArMemGuiTest
{
    struct Fixture
    {
    };

    void test_sanitize(const std::string& in, const std::string& expected)
    {
        const std::string out = armarx::armem::gui::instance::sanitizeTypeName(in);
        BOOST_TEST_CONTEXT("in = '" << in << "'")
        {
            BOOST_CHECK_EQUAL(out, expected);
        }
    }
}



BOOST_FIXTURE_TEST_SUITE(ArMemGuiTest, ArMemGuiTest::Fixture)


BOOST_AUTO_TEST_CASE(test_sanitizeTypeName_int)
{
    test_sanitize(tn::Int().getFullName(), "Int");
    test_sanitize(dn::Int().getFullName(), "Int");
}

BOOST_AUTO_TEST_CASE(test_sanitizeTypeName_float)
{
    test_sanitize(tn::Float().getFullName(), "Float");
    test_sanitize(dn::Float().getFullName(), "Float");
}

BOOST_AUTO_TEST_CASE(test_sanitizeTypeName_dict)
{
    tn::Dict dict(std::make_shared<tn::Float>());
    test_sanitize(dict.getFullName(), "Dict<Float>");

    test_sanitize(dn::Dict().getFullName(), "Dict");
}

BOOST_AUTO_TEST_CASE(test_sanitizeTypeName_list)
{
    tn::List list(std::make_shared<tn::Long>());
    test_sanitize(list.getFullName(), "List<Long>");

    test_sanitize(dn::List().getFullName(), "List");
}

BOOST_AUTO_TEST_CASE(test_sanitizeTypeName_object)
{
    tn::Object obj("namespace::MyObjectName");
    test_sanitize(obj.getFullName(), "MyObjectName    (namespace)");
}

BOOST_AUTO_TEST_CASE(test_sanitizeTypeName_tuple)
{
    std::vector<tn::VariantPtr> ac = {std::make_shared<tn::Int>(), std::make_shared<tn::Float>()};
    tn::Tuple type(ac);
    test_sanitize(type.getFullName(), "Tuple<Int, Float>");
}

BOOST_AUTO_TEST_CASE(test_sanitizeTypeName_pair)
{
    tn::Pair type(std::make_shared<tn::Int>(), std::make_shared<tn::Float>());
    test_sanitize(type.getFullName(), "Pair<Int, Float>");
}

BOOST_AUTO_TEST_CASE(test_sanitizeTypeName_ndarry)
{
    {
        tn::NDArray type;
        type.setNumberDimensions(3); //old: { 3, 2, 1});
        type.setElementType(armarx::aron::type::ndarray::FLOAT32);
        test_sanitize(type.getFullName(), "NDArray");
    }
    {
        dn::NDArray data;
        data.setShape({ 3, 2, 1});
        test_sanitize(data.getFullName(), "NDArray<3, 2, 1, >");
    }
}


BOOST_AUTO_TEST_SUITE_END()
