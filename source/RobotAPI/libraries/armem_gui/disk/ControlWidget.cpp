#include "ControlWidget.h"

#include <RobotAPI/libraries/armem/server/ltm/disk/Memory.h>
#include <RobotAPI/libraries/armem/server/query_proc/ltm/disk/ltm.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <QHBoxLayout>
#include <QSpacerItem>
#include <QFileDialog>
#include <QPushButton>

#include <filesystem>

#include <cmath>


namespace armarx::armem::gui::disk
{

    ControlWidget::ControlWidget()
    {
        _latestDirectory = QString::fromStdString("/tmp/MemoryExport");

        _loadFromDiskButton = new QPushButton(" Load Query from Disk into WM", this);
        _loadFromDiskButton->setIcon(QIcon(":/icons/document-open.svg"));
        _storeOnDiskButton = new QPushButton(" Store shown Data on Disk", this);
        _storeOnDiskButton->setIcon(QIcon(":/icons/document-save.svg"));

        // Allow horizontal shrinking of buttons
        std::vector<QPushButton*> buttons { _storeOnDiskButton, _loadFromDiskButton };
        for (QPushButton* button : buttons)
        {
            button->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        }

        this->setSizePolicy(QSizePolicy::Policy::Preferred, QSizePolicy::Policy::Fixed);

        QHBoxLayout* layout = new QHBoxLayout();
        this->setLayout(layout);

        const int margin = 0;
        layout->setContentsMargins(margin, margin, margin, margin);

        layout->addWidget(_loadFromDiskButton);
        layout->addWidget(_storeOnDiskButton);


        // Connections

        connect(_loadFromDiskButton, &QPushButton::pressed, [this]()
        {
            QString directory = chooseDirectoryDialog();
            if (directory.size() > 0)
            {
                emit requestedLoadFromDisk(directory);
            }
        });
        connect(_storeOnDiskButton, &QPushButton::pressed, [this]()
        {
            QString directory = chooseDirectoryDialog();
            if (directory.size() > 0)
            {
                emit requestedStoreOnDisk(directory);
            }
        });
    }


    static
    const std::string&
    handleSingular(int num, const std::string& singular, const std::string& plural)
    {
        return num == 1 ? singular : plural;
    }


    void
    ControlWidget::storeOnDisk(
        QString directory,
        const std::map<std::string, wm::Memory> memoryData,
        std::string* outStatus)
    {
        std::filesystem::path path(directory.toUtf8().constData());
        ARMARX_CHECK_POSITIVE(path.string().size());  // An empty path indicates an error.

        std::stringstream status;
        if (std::filesystem::is_regular_file(path))
        {
            status << "Could not export memories contents to " << path << ": Cannot overwrite existing file.";
        }
        else
        {
            int numStored = 0;
            for (const auto& [name, data] : memoryData)
            {
                if (std::filesystem::is_regular_file(path / name))
                {
                    status << "Could not export memory '" << name << "' to " << path << ": Cannot overwrite existing file.\n";
                }
                else
                {
                    armem::server::ltm::disk::Memory memory(path, name);
                    memory.directlyStore(data);

                    numStored++;
                }
            }
            status << "Exported " << numStored << " " << handleSingular(numStored, "memory", "memories") << " to " << path << ".";
        }

        if (outStatus)
        {
            *outStatus = status.str();
        }
    }


    std::map<std::string, wm::Memory>
    ControlWidget::loadFromDisk(
        QString directory,
        const armem::client::QueryInput& _queryInput,
        std::string* outStatus)
    {
        std::filesystem::path path(directory.toUtf8().constData());

        std::map<std::string, wm::Memory> memoryData;

        auto setStatus = [&](const std::string& s){
            if (outStatus)
            {
                *outStatus = s;
            }
        };

        if (not std::filesystem::is_directory(path))
        {
            setStatus("Could not import a memory from " + path.string() + ". It is not a directory. Skipping import.");
            return memoryData;
        }

        // Find out whether this is a single memory or a collection of memories by searching
        // for a data.aron.* or metadata.aron.* file at depth 5 (if 6 then it is collection of memories)
        bool isSingleMemory = false;
        for (auto i = std::filesystem::recursive_directory_iterator(path); i != std::filesystem::recursive_directory_iterator(); ++i)
        {
            if (i.depth() > armem::server::ltm::disk::Memory::DEPTH_TO_DATA_FILES + 2)
            {
                // After some depth we stop searching to not freeze GUI too long
                setStatus("Could not import a memory from " + path.string() + ". Data files were not found until max-depth 7. Skipping import.");
                return memoryData;
            }

            auto& dir = *i;

            // if one matches it is enough to check
            if (std::filesystem::is_regular_file(dir.path()) && simox::alg::starts_with(dir.path().filename(), "data.aron"))
            {
                isSingleMemory = (i.depth() == armem::server::ltm::disk::Memory::DEPTH_TO_DATA_FILES);
                break;
            }
        }

        // TODO: Only add data that matchs query?
        // We use LTM as query target for the disk
        // armem::client::QueryInput queryInput = _queryInput;
        // queryInput.addQueryTargetToAll(armem::query::data::QueryTarget::LTM);
        // const query::data::Input queryIce = queryInput.toIce();

        int numLoaded = 0;
        auto loadMemory = [&](const std::filesystem::path& p) {
            if (std::filesystem::is_directory(p))
            {
                armem::server::ltm::disk::Memory ltm(p.parent_path(), p.filename());
                armem::wm::Memory memory = ltm.loadAllAndResolve(); // load list of references
                memoryData[memory.name()] = std::move(memory);

                numLoaded++;
            }
        };

        if (isSingleMemory)
        {
            loadMemory(path);
        }
        else
        {
            // we have to load multiple memories (each subfolder)
            for (const auto& dir : std::filesystem::directory_iterator(path))
            {
                loadMemory(dir.path());
            }
        }

        setStatus("Loaded " + std::to_string(numLoaded) + " " + handleSingular(numLoaded, "memory", "memories") + " from " + path.string() + ".");
        return memoryData;
    }


    QString ControlWidget::chooseDirectoryDialog()
    {
        _latestDirectory = QFileDialog::getExistingDirectory(this, "Open query result",
                                                             _latestDirectory,
                                                             QFileDialog::ShowDirsOnly
                                                             | QFileDialog::DontResolveSymlinks);

        return _latestDirectory;
    }

}

