#pragma once

#include <RobotAPI/libraries/armem/core/forward_declarations.h>
#include <RobotAPI/libraries/armem/client/Query.h>

#include <QWidget>
#include <QString>


class QPushButton;


namespace armarx::armem::gui::disk
{

    class ControlWidget : public QWidget
    {
        Q_OBJECT
        using This = ControlWidget;

    public:

        ControlWidget();


        void
        storeOnDisk(
            QString directory,
            const std::map<std::string, wm::Memory> memoryData,
            std::string* outStatus = nullptr);

        std::map<std::string, wm::Memory>
        loadFromDisk(
            QString directory,
            const armem::client::QueryInput& queryInput,
            std::string* outStatus = nullptr);


    signals:

        void requestedLoadFromDisk(QString directory);
        void requestedStoreOnDisk(QString directory);


    private slots:

        QString chooseDirectoryDialog();


    signals:


    private:

        QPushButton* _loadFromDiskButton;
        QPushButton* _storeOnDiskButton;

        QString _latestDirectory;

    };

}
