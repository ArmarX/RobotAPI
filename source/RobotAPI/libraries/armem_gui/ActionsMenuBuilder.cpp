#include "ActionsMenuBuilder.h"

#include <RobotAPI/libraries/armem/core/wm/ice_conversions.h>


namespace armarx::armem::gui
{
    ActionsMenuBuilder::ActionsMenuBuilder(
        MemoryID memoryID,
        QWidget* parent,
        std::function<void(const MemoryID&, const actions::ActionPath&)> func) :
        memoryID(std::move(memoryID)), parent(parent), func(std::move(func))
    {
    }

    QMenu*
    ActionsMenuBuilder::buildActionsMenu(actions::data::GetActionsOutput& actionsOutput)
    {
        auto* menu = new QMenu("Actions", parent);

        for (const auto& entry : actions::Menu::fromIce(actionsOutput.menu).entries)
        {
            addMenuEntry(menu, {}, entry);
        }

        return menu;
    }

    void
    ActionsMenuBuilder::addMenuEntry( // NOLINT (clangd complains about the recursion here)
        QMenu* menu,
        actions::ActionPath path,
        const actions::MenuEntry& entry)
    {
        path.push_back(entry.id);
        if (entry.entries.empty())
        {
            menu->addAction(QString::fromStdString(entry.text),
                            parent,
                            [this, path]() { func(memoryID, path); });
        }
        else
        {
            QMenu* qSubmenu = menu->addMenu(QString::fromStdString(entry.text));
            for (const auto& subEntry : entry.entries)
            {
                addMenuEntry(qSubmenu, path, subEntry);
            }
        }
    }

} // namespace armarx::armem::gui
