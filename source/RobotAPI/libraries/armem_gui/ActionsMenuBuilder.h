#pragma once

#include <QMenu>

#include <RobotAPI/interface/armem/actions.h>
#include <RobotAPI/libraries/armem/core/actions.h>
#include <RobotAPI/libraries/armem/core/MemoryID.h>

namespace armarx::armem::gui
{
    class ActionsMenuBuilder
    {
    public:
        ActionsMenuBuilder(MemoryID memoryID,
                           QWidget* parent,
                           std::function<void(const MemoryID&, const actions::ActionPath&)> func);

        QMenu* buildActionsMenu(actions::data::GetActionsOutput& actionsOutput);

    private:
        void
        addMenuEntry(QMenu* menu, actions::ActionPath path, const actions::MenuEntry& entry);

        MemoryID memoryID;
        QWidget* parent;
        std::function<void(const MemoryID&, const actions::ActionPath&)> func;
    };
} // namespace armarx::armem::gui
