#include "CommitWidget.h"

#include <QWidget>
#include <QLineEdit>
#include <QTabWidget>
#include <QPushButton>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTextEdit>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx::armem::gui
{

    CommitWidget::CommitWidget()
    {
        QHBoxLayout* hlayout1 = new QHBoxLayout();
        QHBoxLayout* hlayout2 = new QHBoxLayout();
        QVBoxLayout* vlayout = new QVBoxLayout();

        _sendCommit = new QPushButton("Commit");
        _memoryID = new QLineEdit("memory/coreSegment/providerSegment/entity");
        _aronJSONInput = new QTextEdit("");

        QFont font;
        font.setFamily("Courier");
        font.setStyleHint(QFont::Monospace);
        font.setFixedPitch(true);
        font.setPointSize(10);
        _aronJSONInput->setFont(font);

        const int tabStop = 4;
        QFontMetrics metrics(font);
        _aronJSONInput->setTabStopWidth(tabStop * metrics.width(' '));

        hlayout1->addWidget(_memoryID);
        hlayout1->addWidget(_sendCommit);

        hlayout2->addWidget(_aronJSONInput);

        const int margin = 0;
        vlayout->setContentsMargins(margin, margin, margin, margin);

        vlayout->addLayout(hlayout1);
        vlayout->addLayout(hlayout2);

        // Public connections.
        connect(_sendCommit, &QPushButton::pressed, this, &This::commit);

        setLayout(vlayout);
    }

    std::string CommitWidget::getAronJSON() const
    {
        return _aronJSONInput->toPlainText().toStdString();
    }

    std::string CommitWidget::getMemoryID() const
    {
        return _memoryID->text().toStdString();
    }
}
