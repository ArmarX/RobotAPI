#pragma once

#include <RobotAPI/libraries/armem/core/query/DataMode.h>
#include <RobotAPI/libraries/armem/client/query/Builder.h>

#include <QWidget>

class QLineEdit;
class QPushButton;
class QTextEdit;

namespace armarx::armem::gui
{

    class CommitWidget : public QWidget
    {
        Q_OBJECT
        using This = CommitWidget;

    public:

        CommitWidget();

        std::string getAronJSON() const;
        std::string getMemoryID() const;

    public slots:

    signals:
        void commit();


    private slots:

    signals:



    private:

        QPushButton* _sendCommit;
        QLineEdit* _memoryID;
        QTextEdit* _aronJSONInput;
    };

}
