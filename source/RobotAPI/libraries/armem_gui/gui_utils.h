#pragma once

#include <QLayout>
#include <QLayoutItem>
#include <QSpinBox>
#include <QWidget>

class QLayout;
class QSplitter;
class QTreeWidgetItem;


namespace armarx::gui
{
    /**
     * @brief Clear a layout.
     *
     * Recursively take and delete all items in `layout`.
     *
     * @param layout The layout.
     */
    void clearLayout(QLayout* layout);

    /**
     * @brief Clear a tree widget item
     *
     * Take and delete all children of `item`.
     *
     * @param item The tree widget item.
     */
    void clearItem(QTreeWidgetItem* item);



    template <class WidgetT>
    void replaceWidget(WidgetT*& old, QWidget* neu, QLayout* parentLayout)
    {
        QLayoutItem* oldItem = parentLayout->replaceWidget(old, neu);
        if (oldItem)
        {
            delete oldItem;
            delete old;
            old = nullptr;
        }
    }

    /**
     * @brief Let items in `layout` be children of a splitter.
     *
     * Items in `layout` are moved to a new `QSplitter`, which will
     * be the only child of `layout`.
     * If `layout` is a Q{H,V}BoxLayout, the respective orientation
     * will be adopted.
     *
     * @param The parent layout.
     * @return The splitter item.
     */
    QSplitter* useSplitter(QLayout* layout);

    // Source: https://stackoverflow.com/a/26538572
    class LeadingZeroSpinBox : public QSpinBox
    {
        using QSpinBox::QSpinBox;

    public:
        LeadingZeroSpinBox(int numDigits, int base);

        QString textFromValue(int value) const override;

    private:
        int numDigits;
        int base;
    };
}
