/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SpeechX::ArmarXObjects::gui
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QObject>


namespace armarx
{
    class ManagedIceObject;
}
namespace armarx::gui
{

    class LifecycleServer : public QObject
    {
        Q_OBJECT
    public:

        using QObject::QObject;


    public slots:

    signals:

        void initialized(ManagedIceObject* component);
        void connected(ManagedIceObject* component);
        void disconnected(ManagedIceObject* component);

    };


    class LifecycleClient
    {
    public:

        virtual ~LifecycleClient();


    public:

        // Used by QtSignals.
        virtual void onInit(ManagedIceObject* component);
        virtual void onConnect(ManagedIceObject* component);
        virtual void onDisconnect(ManagedIceObject* component);

        // To be overriden by deriving classes.
        virtual void onInit(ManagedIceObject& component);
        virtual void onConnect(ManagedIceObject& component);
        virtual void onDisconnect(ManagedIceObject& component);

        // Alternative override
        virtual void onInit();
        virtual void onConnect();
        virtual void onDisconnect();

    };


    void connectLifecycle(LifecycleServer* server, LifecycleClient* client);

}
