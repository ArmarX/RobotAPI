/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::armem::gui
 * @author     phesch ( ulila at student dot kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PredictionWidget.h"

#include <QComboBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSpinBox>
#include <QVBoxLayout>

#include "TimestampInput.h"


namespace armarx::armem::gui
{
    PredictionWidget::PredictionWidget(GetEntityInfoFn&& entityInfoRetriever) :
        memoryEntity(new QLineEdit()),
        timestampInputSelector(new QComboBox()),
        timestampLayout(new QHBoxLayout()),
        instanceSpinner(new QSpinBox()),
        predictionEngineSelector(new QComboBox()),
        predictButton(new QPushButton("Predict")),
        entityInfoRetriever(entityInfoRetriever)
    {
        auto* vlayout = new QVBoxLayout();
        auto* hlayout = new QHBoxLayout();
        hlayout->addWidget(new QLabel("Entity ID"));
        hlayout->addWidget(memoryEntity);
        vlayout->addLayout(hlayout);

        timestampInputSelector->addItems({"Absolute", "Relative to now"});
        timestampLayout->addWidget(new QLabel("Time"));
        timestampLayout->addWidget(timestampInputSelector);
        vlayout->addLayout(timestampLayout);

        hlayout = new QHBoxLayout();
        predictionEngineSelector->setSizeAdjustPolicy(QComboBox::AdjustToContents);
        hlayout->addWidget(new QLabel("Engine"));
        hlayout->addWidget(predictionEngineSelector);
        hlayout->addStretch();
        vlayout->addLayout(hlayout);

        vlayout->addWidget(predictButton);

        addTimestampInputMethod("Absolute", new AbsoluteTimestampInput());
        addTimestampInputMethod("Relative to now", new RelativeTimestampInput());
        timestampLayout->addStretch();

        setLayout(vlayout);

        showTimestampInputMethod("Absolute");

        connect(timestampInputSelector,
                &QComboBox::currentTextChanged,
                this,
                &PredictionWidget::showTimestampInputMethod);

        connect(memoryEntity,
                &QLineEdit::editingFinished,
                this,
                &PredictionWidget::updateCurrentEntity);

        connect(predictButton, &QPushButton::clicked, this, &PredictionWidget::startPrediction);
    }

    void
    PredictionWidget::addTimestampInputMethod(const QString& key, TimestampInput* input)
    {
        timestampInputs.emplace(key, input);
        timestampLayout->addWidget(input);
    }

    void
    PredictionWidget::showTimestampInputMethod(const QString& key) // NOLINT
    {
        for (const auto& [inputKey, input] : timestampInputs)
        {
            input->setVisible(key == inputKey);
        }
    }

    void
    PredictionWidget::updateCurrentEntity()
    {
        MemoryID entityID = MemoryID::fromString(memoryEntity->text().toStdString());
        predictionEngineSelector->clear();
        if (!entityID.hasGap() && entityID.hasEntityName())
        {
            auto info = entityInfoRetriever(entityID);
            currentType = info.type;
            currentEngines = info.engines;
            for (const auto& engine : info.engines)
            {
                predictionEngineSelector->addItem(QString::fromStdString(engine.engineID));
            }
        }
        else
        {
            currentType.reset();
            currentEngines.clear();
        }
    }

    void
    PredictionWidget::startPrediction()
    {
        MemoryID entityID = MemoryID::fromString(memoryEntity->text().toStdString());
        armarx::DateTime timestamp;
        for (const auto& [inputKey, input] : timestampInputs)
        {
            if (input->isVisible())
            {
                timestamp = input->retrieveTimeStamp();
            }
        }
        std::string engineID = predictionEngineSelector->currentText().toStdString();
        emit makePrediction(entityID, currentType, timestamp, engineID);
    }
} // namespace armarx::armem::gui
