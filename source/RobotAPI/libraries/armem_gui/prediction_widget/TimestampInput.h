/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::armem::gui
 * @author     phesch ( ulila at student dot kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <QWidget>

#include <ArmarXCore/core/time/DateTime.h>

class QDateTimeEdit;
class QDoubleSpinBox;
namespace armarx::gui
{
    class LeadingZeroSpinBox;
}

namespace armarx::armem::gui
{

    class TimestampInput : public QWidget
    {
        Q_OBJECT // NOLINT

    public:
        virtual armarx::DateTime retrieveTimeStamp() = 0;
    };


    class AbsoluteTimestampInput : public TimestampInput
    {
        Q_OBJECT // NOLINT

    public:
        AbsoluteTimestampInput();

        armarx::DateTime retrieveTimeStamp() override;

    private:
        QDateTimeEdit* dateTime;
        armarx::gui::LeadingZeroSpinBox* microseconds;
    };


    class RelativeTimestampInput : public TimestampInput
    {
        Q_OBJECT // NOLINT

    public:
        RelativeTimestampInput();

        armarx::DateTime retrieveTimeStamp() override;

    private:
        QDoubleSpinBox* seconds; // NOLINT
    };

} // namespace armarx::armem::gui
