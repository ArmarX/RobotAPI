/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::armem::gui
 * @author     phesch ( ulila at student dot kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TimestampInput.h"

#include <QDateTimeEdit>
#include <QHBoxLayout>
#include <QLabel>

#include <RobotAPI/libraries/armem_gui/gui_utils.h>

namespace armarx::armem::gui
{
    AbsoluteTimestampInput::AbsoluteTimestampInput() :
        dateTime(new QDateTimeEdit(QDateTime::currentDateTime())),
        microseconds(new armarx::gui::LeadingZeroSpinBox(6, 10))
    {
        dateTime->setDisplayFormat("yyyy-MM-dd HH:mm:ss");
        microseconds->setRange(0, 1000 * 1000 - 1);
        microseconds->setSingleStep(1);
        // This cast is safe because 999 * 1000 < MAX_INT.
        microseconds->setValue(
            static_cast<int>((QDateTime::currentMSecsSinceEpoch() % 1000) * 1000));
        microseconds->setSuffix(" µs");

        auto* hlayout = new QHBoxLayout();
        // hlayout->addWidget(new QLabel("Time"));
        hlayout->addWidget(dateTime);
        hlayout->addWidget(new QLabel("."));
        hlayout->addWidget(microseconds);
        setLayout(hlayout);
    }

    armarx::DateTime
    AbsoluteTimestampInput::retrieveTimeStamp()
    {
        return {Duration::MilliSeconds(dateTime->dateTime().toMSecsSinceEpoch()) +
                Duration::MicroSeconds(microseconds->value())};
    }

    RelativeTimestampInput::RelativeTimestampInput() : seconds(new QDoubleSpinBox())
    {
        seconds->setDecimals(6);
        seconds->setSingleStep(0.1);
        seconds->setRange(-1e6, 1e6);
        seconds->setSuffix(" s");
        seconds->setValue(0);

        auto* hlayout = new QHBoxLayout();
        // hlayout->addWidget(new QLabel("Seconds"));
        hlayout->addWidget(seconds);
        setLayout(hlayout);
    }

    armarx::DateTime
    RelativeTimestampInput::retrieveTimeStamp()
    {
        return DateTime::Now() + Duration::SecondsDouble(seconds->value());
    }
} // namespace armarx::armem::gui
