/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::armem::gui
 * @author     phesch ( ulila at student dot kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <functional>
#include <map>

#include <QWidget>

#include <RobotAPI/libraries/armem/core/MemoryID.h>
#include <RobotAPI/libraries/armem/core/Prediction.h>
#include <RobotAPI/libraries/aron/core/type/variant/forward_declarations.h>


class QComboBox;
class QHBoxLayout;
class QLineEdit;
class QPushButton;
class QSpinBox;


namespace armarx::armem::gui
{
    class TimestampInput;


    class PredictionWidget : public QWidget
    {
        Q_OBJECT // NOLINT

    public:
        struct EntityInfo
        {
            aron::type::ObjectPtr type = nullptr;
            std::vector<PredictionEngine> engines;
        };
        using GetEntityInfoFn = std::function<EntityInfo(const MemoryID&)>;

    public:
        PredictionWidget(GetEntityInfoFn&& entityInfoRetriever);

    signals:
        void makePrediction(const MemoryID& entityID,
                            const aron::type::ObjectPtr& entityType,
                            const armarx::DateTime& timestamp,
                            const std::string& engineID);

    private:
        QLineEdit* memoryEntity;

        QComboBox* timestampInputSelector;
        QHBoxLayout* timestampLayout;
        QSpinBox* instanceSpinner;
        QComboBox* predictionEngineSelector;
        QPushButton* predictButton;

        std::map<QString, TimestampInput*> timestampInputs;

        GetEntityInfoFn entityInfoRetriever;

        // Type of currently entered entity and engine availability for it
        aron::type::ObjectPtr currentType;
        std::vector<PredictionEngine> currentEngines;

        void addTimestampInputMethod(const QString& key, TimestampInput* input);

    private slots: // NOLINT
        void showTimestampInputMethod(const QString& key);

        void updateCurrentEntity();

        void startPrediction();

    };
} // namespace armarx::armem::gui
