#pragma once

#include <QWidget>


class QCheckBox;
class QDoubleSpinBox;
class QPushButton;
class QTimer;

namespace armarx::armem::gui
{

    class PeriodicUpdateWidget : public QWidget
    {
        Q_OBJECT
        using This = PeriodicUpdateWidget;

    public:

        PeriodicUpdateWidget(double frequency = 2.0, double maxFrequency = 60);


        QTimer* timer();

        QCheckBox* autoCheckBox();
        QDoubleSpinBox* frequencySpinBox();
        QPushButton* updateButton();

        bool isAutoEnabled() const;
        double getUpdateFrequency() const;
        int getUpdateIntervalMs() const;

        void startTimerIfEnabled();
        void stopTimer();


    public slots:

    signals:

        void update();

        void updateSingle();
        void updatePeriodic();


    private slots:

        void _updateTimerFrequency();
        void _toggleAutoUpdates(bool enabled);

    signals:

        void startTimerSignal();
        void stopTimerSignal();

    private:

        QPushButton* _updateButton;
        QCheckBox* _autoCheckBox;
        QDoubleSpinBox* _frequencySpinBox;

        QTimer* _timer;

    };

}
