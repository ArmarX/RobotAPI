#pragma once

#include <functional>
#include <map>
#include <sstream>

#include <QTreeWidget>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace armarx
{

    /**
     * A class to efficiently build and maintain sorted items of `QTreeWidget`
     * or `QTreeWidgetItem` based on a sorted container matching the intended structure.
     */
    template <class _ElementT>
    struct TreeWidgetBuilder
    {
        using ElementT = _ElementT;

        /// Return < 0 if `element < item`, 0 if `element == item`, and > 0 if `element > item`.
        using CompareFn = std::function<int(const ElementT& element, QTreeWidgetItem* item)>;
        using NameFn = std::function<std::string(const ElementT& element)>;
        using MakeItemFn = std::function<QTreeWidgetItem*(const ElementT& element)>;
        using UpdateItemFn = std::function<bool(const ElementT& element, QTreeWidgetItem* item)>;


        TreeWidgetBuilder() = default;
        /// Constructor to automatically derive the template argument.
        TreeWidgetBuilder(const ElementT&)
        {}

        TreeWidgetBuilder(CompareFn compareFn, MakeItemFn makeItemFn, UpdateItemFn updateItemFn = NoUpdate) :
            compareFn(compareFn), makeItemFn(makeItemFn), updateItemFn(updateItemFn)
        {}
        TreeWidgetBuilder(NameFn nameFn, MakeItemFn makeItemFn, UpdateItemFn updateItemFn = NoUpdate,
                          int nameColumn = 0) :
            compareFn(MakeCompareNameFn(nameFn, nameColumn)), makeItemFn(makeItemFn), updateItemFn(updateItemFn)
        {}


        void setCompareFn(CompareFn compareFn)
        {
            this->compareFn = compareFn;
        }
        void setNameFn(NameFn nameFn, int nameColumn = 0)
        {
            compareFn = MakeCompareNameFn(nameFn, nameColumn);
        }
        void setMakeItemFn(MakeItemFn makeItemFn)
        {
            this->makeItemFn = makeItemFn;
        }
        void setUpdateItemFn(UpdateItemFn updateItemFn)
        {
            this->updateItemFn = updateItemFn;
        }

        void setExpand(bool expand)
        {
            this->expand = expand;
        }


        /**
         * @brief Update the tree according to the elements iterated over by `iteratorFn`.
         *
         * @param parent
         *  The parent, i.e. the tree widget or a tree widget item.
         * @param iteratorFn
         *  Function taking a function `bool fn(const ElementT& element)`
         *  and calling it for each element in the underlying container, i.e. like:
         *  void iteratorFn(bool (*elementFn)(const ElementT& element));
         */
        template <class ParentT, class IteratorFn>
        void updateTreeWithIterator(ParentT* parent, IteratorFn&& iteratorFn);

        /**
         * @brief Update the tree with the iterable container.
         *
         * @param parent
         *  The parent, i.e. the tree widget or a tree widget item.
         * @param iteratorFn
         *  Function taking a function `bool fn(const ElementT& element)`
         *  and calling it for each element in the underlying container, i.e. like:
         *  void iteratorFn(bool (*elementFn)(const ElementT& element));
         */
        template <class ParentT, class ContainerT>
        void updateTreeWithContainer(ParentT* parent, const ContainerT& elements);


        /// No update function (default).
        static bool NoUpdate(const ElementT& element, QTreeWidgetItem* item)
        {
            (void) element, (void) item;
            return true;
        }

        /// Uses the name for comparison.
        static CompareFn MakeCompareNameFn(NameFn nameFn, int nameColumn);


    private:

        CompareFn compareFn;
        MakeItemFn makeItemFn;
        UpdateItemFn updateItemFn;

        /// Whether to expand items on first creation.
        bool expand = false;

    };



    /**
     * A class to efficiently build and maintain sorted items of `QTreeWidget`
     * or `QTreeWidgetItem` based on a map matching the intended structure.
     */
    template <class KeyT, class ValueT>
    struct MapTreeWidgetBuilder
    {
        using MapT = std::map<KeyT, ValueT>;
        using Base = TreeWidgetBuilder<typename MapT::value_type>;
        using ElementT = typename Base::ElementT;

        using CompareFn = std::function<int(const ElementT& element, QTreeWidgetItem* item)>;
        using NameFn = std::function<std::string(const KeyT& key, const ValueT& value)>;

        using MakeItemFn = std::function<QTreeWidgetItem*(const KeyT& key, const ValueT& value)>;
        using UpdateItemFn = std::function<bool(const KeyT& key, const ValueT& value, QTreeWidgetItem* item)>;


        MapTreeWidgetBuilder()
        {
            setNameFn(KeyAsName);
        }
        /// Allows declaring instance from container without explicit template arguments.
        MapTreeWidgetBuilder(const MapT&) : MapTreeWidgetBuilder()
        {}

        MapTreeWidgetBuilder(MakeItemFn makeItemFn, UpdateItemFn updateItemFn = NoUpdate)  : MapTreeWidgetBuilder()
        {
            setMakeItemFn(makeItemFn);
            setUpdateItemFn(updateItemFn);
        }
        MapTreeWidgetBuilder(NameFn nameFn, MakeItemFn makeItemFn, UpdateItemFn updateItemFn = NoUpdate)
        {
            setNameFn(nameFn);
            setMakeItemFn(makeItemFn);
            setUpdateItemFn(updateItemFn);
        }

        void setNameFn(NameFn nameFn)
        {
            builder.setNameFn([nameFn](const ElementT & element)
            {
                const auto& [key, value] = element;
                return nameFn(key, value);
            });
        }

        void setCompareFn(CompareFn compareFn)
        {
            builder.setCompareFn(compareFn);
        }

        void setMakeItemFn(MakeItemFn makeItemFn)
        {
            builder.setMakeItemFn([makeItemFn](const ElementT & element)
            {
                const auto& [key, value] = element;
                return makeItemFn(key, value);
            });
        }
        void setUpdateItemFn(UpdateItemFn updateItemFn)
        {
            builder.setUpdateItemFn([updateItemFn](const ElementT & element, QTreeWidgetItem * item)
            {
                const auto& [key, value] = element;
                return updateItemFn(key, value, item);
            });
        }

        void setExpand(bool expand)
        {
            builder.setExpand(expand);
        }


        template <class ParentT>
        void updateTree(ParentT* tree, const MapT& elements)
        {
            builder.updateTreeWithContainer(tree, elements);
        }


        /// A name function using the key as name.
        static std::string KeyAsName(const KeyT& key, const ValueT& value)
        {
            (void) value;
            if constexpr(std::is_same<KeyT, std::string>())
            {
                return key;
            }
            else
            {
                std::stringstream ss;
                ss << key;
                return ss.str();
            }
        }

        /// No update function (default).
        static bool NoUpdate(const KeyT& key, const ValueT& value, QTreeWidgetItem* item)
        {
            (void) key, (void) value, (void) item;
            return true;
        }


    private:

        TreeWidgetBuilder<typename MapT::value_type> builder;

    };



    namespace detail
    {
        template <class ParentT> struct ParentAPI;

        template <> struct ParentAPI<QTreeWidget>
        {
            static int getItemCount(QTreeWidget* tree)
            {
                return tree->topLevelItemCount();
            }
            static QTreeWidgetItem* getItem(QTreeWidget* tree, int index)
            {
                return tree->topLevelItem(index);
            }
            static void insertItem(QTreeWidget* tree, int index, QTreeWidgetItem* item)
            {
                tree->insertTopLevelItem(index, item);
            }
            static QTreeWidgetItem* takeItem(QTreeWidget* tree, int index)
            {
                return tree->takeTopLevelItem(index);
            }
        };

        template <> struct ParentAPI<QTreeWidgetItem>
        {
            static int getItemCount(QTreeWidgetItem* parent)
            {
                return parent->childCount();
            }
            static QTreeWidgetItem* getItem(QTreeWidgetItem* parent, int index)
            {
                return parent->child(index);
            }
            static QTreeWidgetItem* takeItem(QTreeWidgetItem* parent, int index)
            {
                return parent->takeChild(index);
            }
            static void insertItem(QTreeWidgetItem* parent, int index, QTreeWidgetItem* item)
            {
                parent->insertChild(index, item);
            }
        };

        template <typename T>
        int compare(const T& lhs, const T& rhs)
        {
            if (lhs < rhs)
            {
                return -1;
            }
            else if (lhs == rhs)
            {
                return 0;
            }
            else
            {
                return 1;
            }
        }
        inline int compare(const std::string& lhs, const std::string& rhs)
        {
            return lhs.compare(rhs);
        }
    }


    template <class ElementT>
    auto TreeWidgetBuilder<ElementT>::MakeCompareNameFn(NameFn nameFn, int nameColumn) -> CompareFn
    {
        return [nameFn, nameColumn](const ElementT & element, QTreeWidgetItem * item)
        {
            return detail::compare(nameFn(element), item->text(nameColumn).toStdString());
        };
    }


    template <class ElementT>
    template <class ParentT, class ContainerT>
    void TreeWidgetBuilder<ElementT>::updateTreeWithContainer(ParentT* parent, const ContainerT& elements)
    {
        this->updateTreeWithIterator(parent, [&elements](auto&& elementFn)
        {
            for (const auto& element : elements)
            {
                if (not elementFn(element))
                {
                    break;
                }
            }
        });
    }


    template <class ElementT>
    template <class ParentT, class IteratorFn>
    void TreeWidgetBuilder<ElementT>::updateTreeWithIterator(ParentT* parent, IteratorFn&& iteratorFn)
    {
        using api = detail::ParentAPI<ParentT>;

        ARMARX_CHECK_NOT_NULL(makeItemFn) << "makeItemFn must be set";
        ARMARX_CHECK_NOT_NULL(updateItemFn) << "updateItemFn must be set";
        ARMARX_CHECK_NOT_NULL(compareFn) << "compareFn must be set";

        int currentIndex = 0;
        iteratorFn([this, &parent, &currentIndex](const auto & element)
        {
            bool inserted = false;
            QTreeWidgetItem* item = nullptr;
            if (currentIndex >= api::getItemCount(parent))
            {
                // Add elements to the end of the list.
                ARMARX_CHECK_NOT_NULL(makeItemFn);
                item = makeItemFn(element);
                api::insertItem(parent, api::getItemCount(parent), item);
                ++currentIndex;
                inserted = true;
            }
            else
            {
                QTreeWidgetItem* currentItem = api::getItem(parent, currentIndex);
                while (currentItem != nullptr && compareFn(element, currentItem) > 0)
                {
                    delete api::takeItem(parent, currentIndex);
                    currentItem = api::getItem(parent, currentIndex);
                }
                if (currentItem == nullptr || compareFn(element, currentItem) < 0)
                {
                    // Insert new item before child.
                    item = makeItemFn(element);
                    api::insertItem(parent, currentIndex, item);
                    ++currentIndex;
                    inserted = true;
                }
                else if (currentItem != nullptr && compareFn(element, currentItem) == 0)
                {
                    // Already existing.
                    item = currentItem;
                    ++currentIndex;
                }
            }
            ARMARX_CHECK_NOT_NULL(item);
            bool cont = updateItemFn(element, item);
            if (inserted && expand)
            {
                item->setExpanded(true);
            }
            return cont;
        });
        // Remove superfluous items. (currentIndex must point behind the last item)
        while (api::getItemCount(parent) > currentIndex)
        {
            delete api::takeItem(parent, api::getItemCount(parent) - 1);
        }
        ARMARX_CHECK_EQUAL(currentIndex, api::getItemCount(parent));
    }

}
