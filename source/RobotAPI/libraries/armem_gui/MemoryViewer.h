#pragma once

#include <future>
#include <string>

#include <QObject>

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/interface/armem/actions.h>
#include <RobotAPI/interface/armem/mns/MemoryNameSystemInterface.h>
#include <RobotAPI/libraries/armem/client/MemoryNameSystem.h>
#include <RobotAPI/libraries/armem/client/Reader.h>
#include <RobotAPI/libraries/armem/client/Writer.h>
#include <RobotAPI/libraries/armem_gui/PeriodicUpdateWidget.h>
#include <RobotAPI/libraries/armem_gui/lifecycle.h>
#include <RobotAPI/libraries/armem_gui/instance/GroupBox.h>
#include <RobotAPI/libraries/armem_gui/memory/GroupBox.h>
#include <RobotAPI/libraries/armem_gui/disk/ControlWidget.h>


class QBoxLayout;
class QDialog;
class QGroupBox;
class QLabel;
class QLayout;
class QSettings;
class QTimer;


namespace armarx
{
    class ManagedIceObject;
    class SimpleConfigDialog;
}

namespace armarx::armem::gui
{

    class MemoryViewer :
        public QObject,
        public armarx::Logging,
        public armarx::gui::LifecycleClient
    {
        Q_OBJECT
        using This = MemoryViewer;


    public:

        MemoryViewer(
            QBoxLayout* updateWidgetLayout,
            QGroupBox* memoryGroupBox, QLayout* memoryGroupBoxParentLayout,
            QGroupBox* instanceGroupBox, QLayout* instanceGroupBoxParentLayout,
            QBoxLayout* diskControlWidgetLayout,
            QLabel* statusLabel
        );

        void setLogTag(const std::string& tag);


        void loadSettings(QSettings* settings);
        void saveSettings(QSettings* settings);

        void writeConfigDialog(SimpleConfigDialog* dialog);
        void readConfigDialog(SimpleConfigDialog* dialog);


    public slots:

        void updateInstanceTree(const armem::MemoryID& selectedID);

        void resolveMemoryID(const MemoryID& id);

        void showActionsMenu(const MemoryID& memoryID, QWidget* parent,
                const QPoint& pos, QMenu* menu);

        void makePrediction(const MemoryID& entityID,
                            const aron::type::ObjectPtr& entityType,
                            const armarx::DateTime& timestamp,
                            const std::string& engineID);

        // Disk Control
        void storeOnDisk(QString directory);
        void loadFromDisk(QString directory);

        void queryAndStoreInLTM();
        void startLTMRecording();
        void stopLTMRecording();

        void commit();



    signals:

        void initialized();
        void connected();
        void disconnected();

        void memoryTreeUpdated();
        void instanceTreeUpdated();


    private slots:

        void startQueries();
        void startPeriodicUpdateTimer();
        void processQueryResults();

        void updateMemoryTree();
        void updateAvailableMemories();


    signals:

        void memoryDataChanged();


    private:

        void onInit(ManagedIceObject& component);
        void onConnect(ManagedIceObject& component);
        void onDisconnect(ManagedIceObject& component);

        const armem::wm::Memory* getSingleMemoryData(const std::string& memoryName);
        void updateStatusLabel(int errorCount);



        /// Start a query for each entry in `memoryReaders` which is not in `runningQueries`.
        void startDueQueries();

        std::map<std::string, client::QueryResult>
        collectQueryResults();

        void applyQueryResults(
            const std::map<std::string, client::QueryResult>& results,
            int* outErrorCount = nullptr);


    public:

        std::string mnsName;
        armem::client::MemoryNameSystem mns;

        std::map<std::string, armem::client::Reader> memoryReaders;
        std::map<std::string, armem::client::Writer> memoryWriters;

        std::map<std::string, armem::wm::Memory> memoryData;

        std::map<std::string, std::future<armem::query::data::Result>> runningQueries;

        /// Periodically triggers query result collection and updates the available memories
        QTimer* periodicUpdateTimer;


        QLayout* updateWidgetLayout = nullptr;
        armem::gui::PeriodicUpdateWidget* updateWidget = nullptr;

        QLayout* diskControlLayout = nullptr;
        armem::gui::disk::ControlWidget* diskControl = nullptr;

        armem::gui::MemoryGroupBox* memoryGroup = nullptr;

        armem::gui::InstanceGroupBox* instanceGroup = nullptr;

        QLabel* statusLabel = nullptr;


        std::string debugObserverName;
        DebugObserverInterfacePrx debugObserver;

        // Queries.
        armem::query::data::EntityQueryPtr entityQuery;

        // others
        std::atomic_bool is_initialized = false;
        std::atomic_bool is_connected = false;

    };

}
