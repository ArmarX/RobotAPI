/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "DiffIKProvider.h"
#include "GraspTrajectory.h"

#include <memory>

namespace armarx
{
    typedef std::shared_ptr<class RobotPlacement> RobotPlacementPtr;

    class RobotPlacement
    {
    public:
        struct Result
        {
            DiffIKResult ikResult;
        };
        struct PlacementParams
        {
            std::vector<Eigen::Matrix4f> prePoses;
            GraspTrajectoryPtr graspTrajectory;
        };
    public:
        RobotPlacement(const DiffIKProviderPtr& ikProvider);

        static std::vector<Eigen::Matrix4f> CreateGrid(float dx, int minx, int maxx, float dy, int miny, int maxy, float da, int mina, int maxa);

        std::vector<Result> evaluatePlacements(const std::vector<Eigen::Matrix4f>& robotPlacements, const PlacementParams& params);


    private:
        DiffIKProviderPtr ikProvider;
        bool returnOnlyReachable = true;
    };
}
