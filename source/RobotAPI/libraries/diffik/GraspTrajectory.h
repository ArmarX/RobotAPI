/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once



#include <RobotAPI/libraries/SimpleJsonLogger/SimpleJsonLogger.h>
#include <RobotAPI/libraries/diffik/SimpleDiffIK.h>
#include <RobotAPI/interface/units/GraspCandidateProviderInterface.h>

#include <ArmarXGui/libraries/StructuralJson/JPathNavigator.h>
#include <ArmarXGui/libraries/StructuralJson/StructuralJsonParser.h>

#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlWriter.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/interface/serialization/Eigen.h>

#include <VirtualRobot/math/AbstractFunctionR1R6.h>
#include <VirtualRobot/math/Helpers.h>

#include <Eigen/Core>

#include <vector>
#include <map>

namespace armarx
{
    typedef std::shared_ptr<class GraspTrajectory> GraspTrajectoryPtr;

    class GraspTrajectory
    {
    public:
        class Keypoint;
        typedef std::shared_ptr<Keypoint> KeypointPtr;

        class Keypoint
        {
        public:
            Eigen::Matrix4f tcpTarget;
            Eigen::VectorXf handJointsTarget;
            float dt;
            Eigen::Vector3f feedForwardPosVelocity;
            Eigen::Vector3f feedForwardOriVelocity;
            Eigen::VectorXf feedForwardHandJointsVelocity;

            Keypoint(const Eigen::Matrix4f& tcpTarget, const Eigen::VectorXf& handJointsTarget);
            Keypoint(const Eigen::Matrix4f& tcpTarget, const Eigen::VectorXf& handJointsTarget, float dt,
                     const Eigen::Vector3f& feedForwardPosVelocity, const Eigen::Vector3f& feedForwardOriVelocity,
                     const Eigen::VectorXf& feedForwardHandJointsVelocity);
            Eigen::Vector3f getTargetPosition() const;
            Eigen::Matrix3f getTargetOrientation() const;
            Eigen::Matrix4f getTargetPose() const;
            void updateVelocities(const KeypointPtr& prev, float dt);
        };

        struct Length
        {
            float pos = 0;
            float ori = 0;
        };


    public:
        GraspTrajectory(const Eigen::Matrix4f& tcpStart, const Eigen::VectorXf& handJointsStart);

        void addKeypoint(const Eigen::Matrix4f& tcpTarget, const Eigen::VectorXf& handJointsTarget, float dt);

        size_t getKeypointCount() const;

        void insertKeypoint(size_t index, const Eigen::Matrix4f& tcpTarget, const Eigen::VectorXf& handJointsTarget, float dt);

        void removeKeypoint(size_t index);

        void replaceKeypoint(size_t index, const Eigen::Matrix4f& tcpTarget, const Eigen::VectorXf& handJointsTarget, float dt);

        void setKeypointDt(size_t index, float dt);

        KeypointPtr& lastKeypoint();
        KeypointPtr& getKeypoint(int i);
        Eigen::Matrix4f getStartPose();

        void getIndex(float t, int& i1, int& i2, float& f);

        Eigen::Vector3f GetPosition(float t);

        Eigen::Matrix3f GetOrientation(float t);

        Eigen::Matrix4f GetPose(float t);

        std::vector<Eigen::Matrix4f> getAllKeypointPoses();
        std::vector<Eigen::Vector3f> getAllKeypointPositions();
        std::vector<Eigen::Matrix3f> getAllKeypointOrientations();

        Eigen::VectorXf GetHandValues(float t);

        Eigen::Vector3f GetPositionDerivative(float t);

        Eigen::Vector3f GetOrientationDerivative(float t);

        Eigen::Vector6f GetTcpDerivative(float t);

        Eigen::VectorXf GetHandJointsDerivative(float t);

        float getDuration() const;

        Length calculateLength() const;
        int GetHandJointCount() const;


        GraspTrajectoryPtr getTranslatedAndRotated(const Eigen::Vector3f& translation, const Eigen::Matrix3f& rotation);
        GraspTrajectoryPtr getTransformed(const Eigen::Matrix4f& transform);

        GraspTrajectoryPtr getClone();

        GraspTrajectoryPtr getTransformedToGraspPose(const Eigen::Matrix4f& target, const Eigen::Vector3f& handForward = Eigen::Vector3f::UnitZ());

        SimpleDiffIK::Reachability calculateReachability(VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodePtr tcp = VirtualRobot::RobotNodePtr(), SimpleDiffIK::Parameters params = SimpleDiffIK::Parameters());

        void writeToFile(const std::string& filename);

        static GraspTrajectoryPtr ReadFromFile(const grasping::GraspCandidatePtr& cnd);

        static GraspTrajectoryPtr ReadFromReader(const RapidXmlReaderPtr& reader);
        static GraspTrajectoryPtr ReadFromFile(const std::string& filename);
        static GraspTrajectoryPtr ReadFromString(const std::string& xml);

    private:

        void updateKeypointMap();

    private:
        std::vector<KeypointPtr> keypoints;
        std::map<float, size_t> keypointMap;
    };
}
