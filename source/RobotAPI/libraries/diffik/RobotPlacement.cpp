/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RobotPlacement.h"

#include <VirtualRobot/math/Helpers.h>

using namespace armarx;

RobotPlacement::RobotPlacement(const DiffIKProviderPtr& ikProvider)
{
}

std::vector<Eigen::Matrix4f> RobotPlacement::CreateGrid(float dx, int minx, int maxx, float dy, int miny, int maxy, float da, int mina, int maxa)
{
    std::vector<Eigen::Matrix4f> r;
    for (int x = minx; x <= maxx; x++)
        for (int y = miny; y <= maxy; y++)
            for (int a = mina; a <= maxa; a++)
            {
                r.emplace_back(math::Helpers::CreatePose(Eigen::Vector3f(x * dx, y * dy, 0), Eigen::AngleAxisf(a * da, Eigen::Vector3f::UnitZ()).toRotationMatrix()));
            }
    return r;
}

std::vector<RobotPlacement::Result> RobotPlacement::evaluatePlacements(const std::vector<Eigen::Matrix4f>& robotPlacements, const RobotPlacement::PlacementParams& params)
{
    std::vector<RobotPlacement::Result> r;
    std::vector<Eigen::Matrix4f> tcpTargets;
    for (const Eigen::Matrix4f& pp : params.prePoses)
    {
        tcpTargets.emplace_back(pp);
    }
    std::vector<Eigen::Matrix4f> grasPoses = params.graspTrajectory->getAllKeypointPoses();

    for (const Eigen::Matrix4f& placement : robotPlacements)
    {
        Eigen::Matrix4f invPlacement = placement.inverse();
        std::vector<Eigen::Matrix4f> localPoses;
        for (const Eigen::Matrix4f& tcpPose : tcpTargets)
        {
            localPoses.emplace_back(invPlacement * tcpPose);
        }
        DiffIKResult ikResult = ikProvider->SolveAbsolute(localPoses.at(0));
        throw LocalException("not implemented");
    }
    return {};
}
