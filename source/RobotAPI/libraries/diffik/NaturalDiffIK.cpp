/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     SecondHands Demo (shdemo at armar6)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "NaturalDiffIK.h"

#include <RobotAPI/libraries/core/CartesianPositionController.h>
#include <RobotAPI/libraries/core/CartesianVelocityController.h>

#include <cfloat>

namespace armarx
{
    Eigen::VectorXf NaturalDiffIK::LimitInfNormTo(Eigen::VectorXf vec, float maxValue)
    {
        float infNorm = vec.lpNorm<Eigen::Infinity>();
        if (infNorm > maxValue)
        {
            vec = vec / infNorm * maxValue;
        }
        return vec;
    }

    NaturalDiffIK::Result NaturalDiffIK::CalculateDiffIK(const Eigen::Matrix4f& targetPose, const Eigen::Vector3f& elbowTarget, VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodePtr tcp, VirtualRobot::RobotNodePtr elbow, Mode setOri, Parameters params)
    {
        VirtualRobot::IKSolver::CartesianSelection mode = ModeToCartesianSelection(setOri);

        CartesianVelocityController vcTcp(rns);
        CartesianPositionController pcTcp(tcp);
        CartesianVelocityController vcElb(rns, elbow);
        CartesianPositionController pcElb(elbow);

        if (params.resetRnsValues)
        {
            for (VirtualRobot::RobotNodePtr rn : rns->getAllRobotNodes())
            {
                if (rn->isLimitless())
                {
                    rn->setJointValue(0);
                }
                else
                {
                    rn->setJointValue((rn->getJointLimitHi() + rn->getJointLimitLo()) / 2);
                }
            }
        }


        //std::stringstream ss;

        //ARMARX_IMPORTANT << "start";

        int posMet = 0;
        int oriMet = 0;

        std::vector<IKStep> ikSteps;
        Eigen::VectorXf currentJointValues = rns->getJointValuesEigen();
        for (size_t i = 0; i <= params.stepsInitial + params.stepsFineTune; i++)
        {
            //ss << pdTcp.norm() << " ## " << odTcp.norm()  << " ## " << pdElb.norm() << std::endl;

            int posLen = mode & VirtualRobot::IKSolver::Position ? 3 : 0;
            int oriLen = mode & VirtualRobot::IKSolver::Orientation ? 3 : 0;
            Eigen::Vector3f pdTcp = posLen ? pcTcp.getPositionDiff(targetPose) : Eigen::Vector3f::Zero();
            Eigen::Vector3f odTcp = oriLen ? pcTcp.getOrientationDiff(targetPose) : Eigen::Vector3f::Zero();
            Eigen::VectorXf cartesianVel(posLen + oriLen);
            if (posLen)
            {
                cartesianVel.block<3, 1>(0, 0) = pdTcp;
            }
            if (oriLen)
            {
                cartesianVel.block<3, 1>(posLen, 0) = odTcp;
            }

            Eigen::Vector3f pdElb = pcElb.getPositionDiffVec3(elbowTarget);
            Eigen::VectorXf cartesianVelElb(3);
            cartesianVelElb.block<3, 1>(0, 0) = pdElb;
            Eigen::VectorXf jvElb = params.elbowKp * vcElb.calculate(cartesianVelElb, VirtualRobot::IKSolver::Position);
            Eigen::VectorXf jvLA = params.jointLimitAvoidanceKp * vcTcp.calculateJointLimitAvoidance();
            Eigen::VectorXf jv = vcTcp.calculate(cartesianVel, jvElb + jvLA, mode);


            float stepLength = i < params.stepsInitial ? params.ikStepLengthInitial : params.ikStepLengthFineTune;
            Eigen::VectorXf jvClamped = jv * stepLength;
            jvClamped = LimitInfNormTo(jvClamped, params.maxJointAngleStep);

            if (params.returnIKSteps)
            {
                IKStep s;
                s.pdTcp = pdTcp;
                s.odTcp = odTcp;
                s.pdElb = pdElb;
                s.tcpPose = tcp->getPoseInRootFrame();
                s.elbPose = elbow->getPoseInRootFrame();
                s.cartesianVel = cartesianVel;
                s.cartesianVelElb = cartesianVelElb;
                s.jvElb = jvElb;
                s.jvLA = jvLA;
                s.jv = jv;
                s.jvClamped = jvClamped;
                ikSteps.emplace_back(s);
            }


            Eigen::VectorXf newJointValues = currentJointValues + jvClamped;
            rns->setJointValues(newJointValues);
            currentJointValues = newJointValues;

            if (pdTcp.norm() > params.maxPosError)
            {
                posMet = 0;
            }
            else
            {
                posMet++;
            }
            if (odTcp.norm() > params.maxOriError)
            {
                oriMet = 0;
            }
            else
            {
                oriMet++;
            }

            // terminate early, if reached for at least 3 iterations
            if (posMet > 2 && oriMet > 2)
            {
                break;
            }
        }

        //ARMARX_IMPORTANT << ss.str();

        Result result;
        result.ikSteps = ikSteps;
        result.jointValues = rns->getJointValuesEigen();
        result.posDiff = pcTcp.getPositionDiff(targetPose);
        result.oriDiff = pcTcp.getOrientationDiff(targetPose);
        result.posError = pcTcp.getPositionError(targetPose);
        result.oriError = pcTcp.getOrientationError(targetPose);
        result.reached = result.posError < params.maxPosError && (setOri == Mode::Position || result.oriError < params.maxOriError);
        result.posDiffElbow = pcElb.getPositionDiffVec3(elbowTarget);
        result.posErrorElbow = result.posDiffElbow.norm();

        result.jointLimitMargins = Eigen::VectorXf::Zero(rns->getSize());
        result.minimumJointLimitMargin = FLT_MAX;
        for (size_t i = 0; i < rns->getSize(); i++)
        {
            VirtualRobot::RobotNodePtr rn = rns->getNode(i);
            if (rn->isLimitless())
            {
                result.jointLimitMargins(i) = M_PI;
            }
            else
            {
                result.jointLimitMargins(i) = std::min(rn->getJointValue() - rn->getJointLimitLo(), rn->getJointLimitHi() - rn->getJointValue());
                result.minimumJointLimitMargin = std::min(result.minimumJointLimitMargin, result.jointLimitMargins(i));
            }
        }

        return result;
    }

    VirtualRobot::IKSolver::CartesianSelection NaturalDiffIK::ModeToCartesianSelection(NaturalDiffIK::Mode mode)
    {
        return mode == Mode::All ? VirtualRobot::IKSolver::All : VirtualRobot::IKSolver::Position;
    }



}
