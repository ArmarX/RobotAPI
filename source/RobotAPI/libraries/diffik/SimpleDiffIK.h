/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "DiffIKProvider.h"

#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotNodeSet.h>

#include <memory>

namespace armarx
{
    using SimpleDiffIKPtr = std::shared_ptr<class SimpleDiffIK>;

    class SimpleDiffIK
    {
    public:
        struct Parameters
        {
            Parameters() {}
            // IK params
            float ikStepLengthInitial = 0.2f;
            float ikStepLengthFineTune = 0.5f;
            size_t stepsInitial = 25;
            size_t stepsFineTune = 10;
            float maxPosError = 10.f;
            float maxOriError = 0.05f;
            float jointLimitAvoidanceKp = 2.0f;
            float maxJointAngleStep = 0.1f;
            bool returnIKSteps = false;
            bool resetRnsValues = true;
        };
        struct IKStep
        {
            Eigen::VectorXf jointValues;
            Eigen::Vector3f posDiff;
            Eigen::Vector3f oriDiff;
            Eigen::VectorXf cartesianVel;
            Eigen::VectorXf jnv;
            Eigen::VectorXf jv;
            float infNorm;
            Eigen::VectorXf jvClamped;
        };

        struct Result
        {
            Eigen::VectorXf jointValues;
            Eigen::Vector3f posDiff;
            Eigen::Vector3f oriDiff;
            float posError;
            float oriError;
            bool reached;
            Eigen::VectorXf jointLimitMargins;
            float minimumJointLimitMargin;
            std::vector<IKStep> ikSteps;
        };


        struct Reachability
        {

            bool reachable = true;
            float minimumJointLimitMargin = M_PI;
            Eigen::VectorXf jointLimitMargins;
            float maxPosError = 0;
            float maxOriError = 0;
            std::vector<Result> ikResults;

            void aggregate(const Result& result)
            {
                ikResults.emplace_back(result);
                reachable = reachable && result.reached;
                minimumJointLimitMargin = std::min(minimumJointLimitMargin, result.minimumJointLimitMargin);
                if (jointLimitMargins.rows() == 0)
                {
                    jointLimitMargins = result.jointLimitMargins;
                }
                else
                {
                    jointLimitMargins = jointLimitMargins.cwiseMin(result.jointLimitMargins);
                }
                maxPosError = std::max(maxPosError, result.posError);
                maxOriError = std::max(maxOriError, result.oriError);
            }
        };

        static Result CalculateDiffIK(const Eigen::Matrix4f targetPose, VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodePtr tcp = VirtualRobot::RobotNodePtr(), Parameters params = Parameters());

        ///@brief Use this to check a trajectory of waypoints
        static Reachability CalculateReachability(const std::vector<Eigen::Matrix4f> targets, const Eigen::VectorXf& initialJV, VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodePtr tcp = VirtualRobot::RobotNodePtr(), Parameters params = Parameters());
    };

    class SimpleDiffIKProvider :
        public DiffIKProvider
    {
    public:
        SimpleDiffIKProvider(VirtualRobot::RobotNodeSetPtr rns, VirtualRobot::RobotNodePtr tcp = VirtualRobot::RobotNodePtr(), SimpleDiffIK::Parameters params = SimpleDiffIK::Parameters());
        DiffIKResult SolveAbsolute(const Eigen::Matrix4f& targetPose);
        DiffIKResult SolveRelative(const Eigen::Matrix4f& targetPose, const Eigen::VectorXf& startJointValues);

    private:
        VirtualRobot::RobotNodeSetPtr rns;
        VirtualRobot::RobotNodePtr tcp;
        SimpleDiffIK::Parameters params;
    };
}
