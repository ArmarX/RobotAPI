#pragma once

#include <SimoxUtility/json/json.hpp>
#include <QAbstractItemModel>

namespace armarx
{

    class JSONTreeModel : public QAbstractItemModel
    {
    public:
        void setRoot(nlohmann::json const& root);

        // QAbstractItemModel interface
        QModelIndex index(int row, int column, const QModelIndex& parent) const override;
        QModelIndex parent(const QModelIndex& index) const override;
        int rowCount(const QModelIndex& parent) const override;
        int columnCount(const QModelIndex& parent) const override;
        QVariant data(const QModelIndex& index, int role) const override;
        QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    private:
        nlohmann::json* refFrom(QModelIndex const& index) const;

    private:
        mutable std::map<nlohmann::json*, int> rows;
        mutable std::map<nlohmann::json*, nlohmann::json*> parents;
        mutable std::map<nlohmann::json*, std::string> names;
        mutable nlohmann::json root;
    };

}
