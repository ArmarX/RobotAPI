#include "JSONTreeModel.h"

#include <ArmarXCore/core/logging/Logging.h>

namespace armarx
{

    void JSONTreeModel::setRoot(nlohmann::json const& rootParam)
    {
        root = rootParam;
        rows.clear();
        rows[&root] = 0;
        parents.clear();
        parents[&root] = nullptr;
        names.clear();
        names[&root] = "";
    }

    QModelIndex JSONTreeModel::index(int row, int column, const QModelIndex& parentIndex) const
    {
        nlohmann::json* parent = refFrom(parentIndex);
        if (row >= int(parent->size()))
        {
            return QModelIndex();
        }

        nlohmann::json* child = nullptr;
        if (parent->is_array())
        {
            child = &parent->at(row);
            names[child] = "[" + std::to_string(row) + "]";
        }
        else if (parent->is_object())
        {
            auto it = parent->begin();
            std::advance(it, row);
            child = &it.value();
            names[child] = it.key();
        }

        if (child)
        {
            parents[child] = parent;
            rows[child] = row;

            return createIndex(row, column, child);
        }
        else
        {
            return QModelIndex();
        }

    }

    QModelIndex JSONTreeModel::parent(const QModelIndex& index) const
    {
        if (!index.isValid())
        {
            return QModelIndex();
        }

        nlohmann::json* child = refFrom(index);
        if (!parents.count(child))
        {
            //ARMARX_WARNING << "Map does not contain parent for child: " <<
            //               (child ? child->dump(2) : "(null)");
            return QModelIndex();
        }
        nlohmann::json* parent = parents.at(child);
        if (parent == nullptr)
        {
            return QModelIndex();
        }
        int row = rows.at(parent);

        return createIndex(row, 0, parent);
    }

    int JSONTreeModel::rowCount(const QModelIndex& parentIndex) const
    {
        if (parentIndex.column() > 1)
        {
            return 0;
        }

        nlohmann::json* parent = refFrom(parentIndex);
        if (parent->is_array() || parent->is_object())
        {
            return parent->size();
        }
        else
        {
            return 0;
        }
    }

    int JSONTreeModel::columnCount(const QModelIndex& parent) const
    {
        return 2;
    }

    QVariant JSONTreeModel::data(const QModelIndex& index, int role) const
    {
        if (!index.isValid())
        {
            return QVariant();
        }

        if (role != Qt::DisplayRole)
        {
            return QVariant();
        }

        nlohmann::json* ref = refFrom(index);

        if (index.column() == 0)
        {
            if (!names.count(ref))
            {
                ARMARX_WARNING << "Map does not contain name for ref: " <<
                               (ref ? ref->dump(2) : "(null)");
                return QVariant();
            }
            std::string const& name = names.at(ref);
            return QVariant(name.c_str());
        }

        switch (ref->type())
        {
            case nlohmann::json::value_t::null:
                return QVariant("null");
            case nlohmann::json::value_t::object:
                return QVariant("object");
            case nlohmann::json::value_t::array:
                return QVariant("array");
            case nlohmann::json::value_t::string:
                return QVariant(ref->get<std::string>().c_str());
            case nlohmann::json::value_t::boolean:
                return QVariant(ref->get<bool>());
            case nlohmann::json::value_t::number_integer:
                return QVariant(ref->get<int>());
            case nlohmann::json::value_t::number_unsigned:
                return QVariant(ref->get<unsigned int>());
            case nlohmann::json::value_t::number_float:
                return QVariant(ref->get<float>());

            default:
                return QVariant("n/a");
        }
    }

    QVariant JSONTreeModel::headerData(int section, Qt::Orientation orientation, int role) const
    {
        if (role != Qt::DisplayRole)
        {
            return QVariant();
        }

        switch (section)
        {
            case 0:
                return QVariant("Name");
            case 1:
                return QVariant("Value");
            default:
                return QVariant();
        }
    }

    nlohmann::json* JSONTreeModel::refFrom(const QModelIndex& index) const
    {
        if (index.isValid())
        {
            return static_cast<nlohmann::json*>(index.internalPointer());
        }
        else
        {
            return &root;
        }
    }

}
