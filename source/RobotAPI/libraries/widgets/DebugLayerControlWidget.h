/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    MemoryX::WorkingMemoryGui
* @author     Dominik Muth
* @copyright  2015 KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License

*/

#pragma once

/* ArmarX headers */

#include <ArmarXCore/core/Component.h>
#include <RobotAPI/components/DebugDrawer/DebugDrawerComponent.h>

/* QT headers */
#include <QWidget>
#include <QCheckBox>
#include <QSignalMapper>
#include <QPushButton>

/* STD headers */
#include <unordered_map>

/* Inventor headers */
#include <Inventor/sensors/SoTimerSensor.h>
#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>

/* System headers */
#include <string>


namespace Ui
{
    class DebugLayerControlWidget;
}

class DebugLayerControlWidget : public QWidget
{
    Q_OBJECT

public:
    DebugLayerControlWidget(QWidget* parent = 0);
    ~DebugLayerControlWidget() override;
    void setEntityDrawer(armarx::DebugDrawerComponentPtr entityDrawer);

public slots:
    void updateLayers();

    void layerToggleVisibility(QString layerName);

protected:
    armarx::DebugDrawerComponentPtr entityDrawer;
    SoTimerSensor* timerSensor;
    /**
     * @brief Maps events to toggle a layer's visibility from checkboxes contained in the table to layerToggleVisibility.
     */
    QSignalMapper layerSignalMapperVisible;
    /**
     * @brief Maps events to remove a layer.
     */
    QSignalMapper layerSignalMapperRemove;
    /**
     * @brief Stores whether a layer is currently visible.
     */
    std::unordered_map<std::string, bool> layerVisibility;

    static void onTimer(void* data, SoSensor* sensor);

protected slots:
    void layerRemove(QString layerName);
private:
    Ui::DebugLayerControlWidget* ui;
};

