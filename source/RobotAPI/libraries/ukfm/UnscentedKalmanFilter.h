/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ROBDEKON
 * @author     Christoph Pohl ( christoph dot pohl at kit dot edu )
 * @date       16.03.21
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 *
 * Code adapted to C++ from: https://github.com/CAOR-MINES-ParisTech/ukfm
 * See also:
 *      - https://arxiv.org/pdf/2002.00878.pdf
 *      - https://ieeexplore.ieee.org/document/8206066/
 */

#ifndef ROBDEKON_UNSCENTEDKALMANFILTER_H
#define ROBDEKON_UNSCENTEDKALMANFILTER_H

#include <Eigen/Dense>

#include "SystemModel.h"


template <typename SystemModelT>
class UnscentedKalmanFilter
{
public:
    using FloatT = typename SystemModelT::FloatT;
    static constexpr float eps = 10 * std::numeric_limits<float>::epsilon();
    // variable dimensions:
    using dim = typename SystemModelT::dim;
    using StateT = typename SystemModelT::StateT;
    using ControlT = typename SystemModelT::ControlT;
    using ControlNoiseT =
        typename SystemModelT::ControlNoiseT; // todo: correct name? Is it the same as ControlT?
    using SigmaPointsT = typename SystemModelT::SigmaPointsT; // todo: rename

    using StateCovT = Eigen::Matrix<FloatT, dim::state, dim::state>;
    using PropCovT = Eigen::Matrix<FloatT, dim::control, dim::control>;
    using ObsT = Eigen::Matrix<FloatT, dim::obs, 1>;
    using ObsCovT = Eigen::Matrix<FloatT, dim::obs, dim::obs>;
    using AlphaT = Eigen::Matrix<FloatT, 3, 1>;


    UnscentedKalmanFilter(const PropCovT& Q,
                          ObsCovT R,
                          const AlphaT& alpha,
                          StateT state0,
                          StateCovT P0);
    UnscentedKalmanFilter() = delete;

private:
    PropCovT Q_;
    PropCovT chol_Q_;
    ObsCovT R_;
    AlphaT alpha_;
    StateT state_;
    StateCovT P_;

    struct Weights
    {
        struct W
        {
            W(size_t l, FloatT alpha);
            float sqrt_d_lambda, wj, wm, w0;
        };

        explicit Weights(AlphaT alpha);
        W state, control, update;
    };

    Weights weights_;
    StateCovT calculateClosestPosSemidefMatrix(const StateCovT& cov);

public:
    void propagation(const ControlT& omega, FloatT dt);
    void update(const ObsT& y);
    const StateT& getCurrentState() const;
    const StateCovT& getCurrentStateCovariance() const;
};

extern template class UnscentedKalmanFilter<SystemModelSE3<float>>;
extern template class UnscentedKalmanFilter<SystemModelSE3<double>>;
extern template class UnscentedKalmanFilter<SystemModelSO3xR3<float>>;
extern template class UnscentedKalmanFilter<SystemModelSO3xR3<double>>;

#endif //ROBDEKON_UNSCENTEDKALMANFILTER_H
