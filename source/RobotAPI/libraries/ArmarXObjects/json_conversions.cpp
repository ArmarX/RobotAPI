#include "json_conversions.h"

#include <SimoxUtility/json/eigen_conversion.h>
#include <SimoxUtility/shapes/json_conversions.h>

#include "ObjectID.h"
#include "ObjectPose.h"
#include "Scene.h"
#include "ice_conversions.h"


void armarx::to_json(simox::json::json& j, const ObjectID& id)
{
    j["dataset"] = id.dataset();
    j["className"] = id.className();
    j["instanceName"] = id.instanceName();
    j["str"] = id.str();
}

void armarx::from_json(const simox::json::json& j, ObjectID& id)
{
    id =
    {
        j.at("dataset").get<std::string>(),
        j.at("className").get<std::string>(),
        j.at("instanceName").get<std::string>()
    };
}

void armarx::objpose::to_json(simox::json::json& j, const ObjectPose& op)
{
    j["providerName"] = op.providerName;
    j["objectType"] = ObjectTypeNames.to_name(op.objectType);

    j["objectID"] = op.objectID;

    j["objectPoseRobot"] = op.objectPoseRobot;
    j["objectPoseGlobal"] = op.objectPoseGlobal;
    j["objectPoseOriginal"] = op.objectPoseOriginal;
    j["objectPoseOriginalFrame"] = op.objectPoseOriginalFrame;

    j["robotConfig"] = op.robotConfig;
    j["robotPose"] = op.robotPose;

    j["confidence"] = op.confidence;
    j["timestampMicroSeconds"] = op.timestamp.toMicroSecondsSinceEpoch();

    if (op.localOOBB)
    {
        j["localOOBB"] = *op.localOOBB;
    }
}


void armarx::objpose::from_json(const simox::json::json& j, ObjectPose& op)
{
    op.providerName = j.at("providerName");
    op.objectType = ObjectTypeNames.from_name(j.at("objectType"));

    op.objectID = j.at("objectID");

    op.objectPoseRobot = j.at("objectPoseRobot");
    op.objectPoseGlobal = j.at("objectPoseGlobal");
    op.objectPoseOriginal = j.at("objectPoseOriginal");
    op.objectPoseOriginalFrame = j.at("objectPoseOriginalFrame");

    op.robotConfig = j.at("robotConfig").get<std::map<std::string, float>>();
    op.robotPose = j.at("robotPose");

    op.confidence = j.at("confidence");
    op.timestamp = DateTime(Duration::MicroSeconds(j.at("timestampMicroSeconds")));

    if (j.count("localOOBB"))
    {
        op.localOOBB = j.at("localOOBB").get<simox::OrientedBoxf>();
    }
}


void armarx::objects::to_json(simox::json::json& j, const SceneObject& rhs)
{
    j["class"] = rhs.className;
    j["instanceName"] = rhs.instanceName;
    j["collection"] = rhs.collection;
    j["position"] = rhs.position;
    j["orientation"] = rhs.orientation;
    if (rhs.isStatic.has_value())
    {
        j["isStatic"] = rhs.isStatic.value();
    }
    j["jointValues"] = rhs.jointValues;
}


void armarx::objects::from_json(const simox::json::json& j, SceneObject& rhs)
{
    j.at("class").get_to(rhs.className);
    if (j.count("instanceName"))
    {
        j["instanceName"].get_to(rhs.instanceName);
    }
    else
    {
        rhs.instanceName.clear();
    }
    j.at("collection").get_to(rhs.collection);
    j.at("position").get_to(rhs.position);
    j.at("orientation").get_to(rhs.orientation);

    if (j.count("isStatic"))
    {
        rhs.isStatic = j.at("isStatic").get<bool>();
    }
    else
    {
        rhs.isStatic = std::nullopt;
    }

    if (j.count("jointValues"))
    {
        j.at("jointValues").get_to(rhs.jointValues);
    }
    else
    {
        rhs.jointValues.clear();
    }
}



void armarx::objects::to_json(simox::json::json& j, const Scene& rhs)
{
    j["objects"] = rhs.objects;
}


void armarx::objects::from_json(const simox::json::json& j, Scene& rhs)
{
    j.at("objects").get_to(rhs.objects);
}
