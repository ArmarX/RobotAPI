#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>

#include <RobotAPI/interface/objectpose/ObjectPoseStorageInterface.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPoseClient.h>


namespace armarx::plugins
{
    class ObjectPoseClientPlugin : public ComponentPlugin
    {
    public:

        using ComponentPlugin::ComponentPlugin;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;
        objpose::ObjectPoseStorageInterfacePrx createObjectPoseStorage();

        template<class...Ts>
        std::optional<ObjectInfo> findObject(Ts&& ...ts) const
        {
            return _finder.findObject(std::forward<Ts>(ts)...);
        }

        template<class...Ts>
        VirtualRobot::ManipulationObjectPtr
        findAndLoadObject(Ts&& ...ts) const
        {
            return findAndLoadObject(findObject(std::forward<Ts>(ts)...));
        }
        VirtualRobot::ManipulationObjectPtr
        findAndLoadObject(const std::optional<ObjectInfo>& ts) const
        {
            return _finder.loadManipulationObject(ts);
        }

        const ObjectFinder& setObjectFinderPath(const std::string& path);
        const ObjectFinder& getObjectFinder() const;


    private:

        void preOnInitComponent() override;
        void preOnConnectComponent() override;

        static constexpr const char* PROPERTY_NAME = "ObjectMemoryName";

        ObjectFinder _finder;

    };
}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{
    /**
     * @brief Provides an `objpose::ObjectPoseTopicPrx objectPoseTopic` as member variable.
     */
    class ObjectPoseClientPluginUser :
        virtual public ManagedIceObject
    {
    public:

        /// Allow usage like: ObjectPoseClient::getObjects()
        using ObjectPoseClient = ObjectPoseClientPluginUser;

        ObjectPoseClientPluginUser();

        objpose::ObjectPoseStorageInterfacePrx createObjectPoseStorage();
        objpose::ObjectPoseStorageInterfacePrx objectPoseStorage;


        objpose::ObjectPoseClient getClient() const;


        objpose::ObjectPoseSeq getObjectPoses();
        objpose::ObjectPoseSeq getObjectPosesByProvider(const std::string& providerName);


        plugins::ObjectPoseClientPlugin& getObjectPoseClientPlugin();
        const plugins::ObjectPoseClientPlugin& getObjectPoseClientPlugin() const;

        const ObjectFinder& getObjectFinder() const;


    private:

        armarx::plugins::ObjectPoseClientPlugin* plugin = nullptr;

    };
}
