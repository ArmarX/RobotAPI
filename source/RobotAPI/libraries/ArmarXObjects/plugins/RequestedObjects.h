#pragma once

#include <set>
#include <deque>
#include <vector>


#include <RobotAPI/interface/objectpose/object_pose_types.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>



namespace armarx::objpose
{
    /**
     * @brief Handles requests for object pose estimation from the ObjectPoseProviderInterface.
     */
    class RequestedObjects
    {

    public:
        struct Request
        {
            std::vector<armarx::ObjectID> objectIDs;
            bool infinite = false;
        };
        struct Update
        {
            std::vector<armarx::ObjectID> added;
            std::vector<armarx::ObjectID> removed;
            std::vector<armarx::ObjectID> current;
        };


    public:

        RequestedObjects();

        void requestObjects(const std::vector<armarx::data::ObjectID>& objectIDs, long relativeTimeOutMS);
        void requestObjects(const std::vector<armarx::ObjectID>& objectIDs, long relativeTimeOutMS);
        void requestObjects(const std::vector<armarx::ObjectID>& objectIDs, IceUtil::Time relativeTimeout);


        Update updateRequestedObjects();
        Update updateRequestedObjects(IceUtil::Time now);


    public:

        /// Map from (absolute timeout) to (request)
        std::map<IceUtil::Time, std::vector<Request>> currentRequests;
        std::vector<armarx::ObjectID> infiniteRequests;


    private:
        std::vector<armarx::ObjectID> lastCurrent;

    };
}
