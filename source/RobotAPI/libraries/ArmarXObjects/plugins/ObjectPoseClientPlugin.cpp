#include "ObjectPoseClientPlugin.h"

#include <ArmarXCore/core/Component.h>


namespace armarx::plugins
{
    void ObjectPoseClientPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {
        if (!properties->hasDefinition(makePropertyName(PROPERTY_NAME)))
        {
            properties->defineOptionalProperty<std::string>(
                makePropertyName(PROPERTY_NAME),
                "ObjectMemory",
                "Name of the object memory.");
        }
    }

    void ObjectPoseClientPlugin::preOnInitComponent()
    {
        parent<Component>().usingProxyFromProperty(makePropertyName(PROPERTY_NAME));
    }

    void ObjectPoseClientPlugin::preOnConnectComponent()
    {
        parent<ObjectPoseClientPluginUser>().objectPoseStorage = createObjectPoseStorage();
    }

    objpose::ObjectPoseStorageInterfacePrx ObjectPoseClientPlugin::createObjectPoseStorage()
    {
        return parent<Component>().getProxyFromProperty<objpose::ObjectPoseStorageInterfacePrx>(makePropertyName(PROPERTY_NAME));
    }

    const ObjectFinder& ObjectPoseClientPlugin::setObjectFinderPath(const std::string& path)
    {
        _finder.setPath(path);
        return _finder;
    }

    const ObjectFinder& ObjectPoseClientPlugin::getObjectFinder() const
    {
        return _finder;
    }
}

namespace armarx
{
    ObjectPoseClientPluginUser::ObjectPoseClientPluginUser()
    {
        addPlugin(plugin);
    }

    objpose::ObjectPoseStorageInterfacePrx ObjectPoseClientPluginUser::createObjectPoseStorage()
    {
        return plugin->createObjectPoseStorage();
    }


    objpose::ObjectPoseClient ObjectPoseClientPluginUser::getClient() const
    {
        return objpose::ObjectPoseClient(objectPoseStorage, getObjectFinder());
    }


    objpose::ObjectPoseSeq ObjectPoseClientPluginUser::getObjectPoses()
    {
        if (!objectPoseStorage)
        {
            ARMARX_WARNING << "No object pose observer.";
            return {};
        }
        return objpose::fromIce(objectPoseStorage->getObjectPoses());
    }

    objpose::ObjectPoseSeq ObjectPoseClientPluginUser::getObjectPosesByProvider(const std::string& providerName)
    {
        return objpose::ObjectPoseClient(objectPoseStorage, getObjectFinder()).fetchObjectPosesFromProvider(providerName);
    }


    plugins::ObjectPoseClientPlugin& ObjectPoseClientPluginUser::getObjectPoseClientPlugin()
    {
        return *plugin;
    }
    const plugins::ObjectPoseClientPlugin& ObjectPoseClientPluginUser::getObjectPoseClientPlugin() const
    {
        return *plugin;
    }

    const ObjectFinder& ObjectPoseClientPluginUser::getObjectFinder() const
    {
        return plugin->getObjectFinder();
    }
}
