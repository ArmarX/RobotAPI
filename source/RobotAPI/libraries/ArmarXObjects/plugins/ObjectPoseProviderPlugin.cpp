#include "ObjectPoseProviderPlugin.h"

#include <ArmarXCore/core/Component.h>


namespace armarx::plugins
{

    void ObjectPoseProviderPlugin::postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties)
    {
        if (!properties->hasDefinition(makePropertyName(PROPERTY_NAME)))
        {
            properties->defineOptionalProperty<std::string>(
                makePropertyName(PROPERTY_NAME),
                "ObjectPoseTopic",
                "Name of the object pose topic.");
        }
    }

    void ObjectPoseProviderPlugin::preOnInitComponent()
    {
        parent<Component>().offeringTopicFromProperty(makePropertyName(PROPERTY_NAME));
    }

    void ObjectPoseProviderPlugin::preOnConnectComponent()
    {
        parent<ObjectPoseProviderPluginUser>().objectPoseTopic = createObjectPoseTopic();
    }

    void ObjectPoseProviderPlugin::postOnConnectComponent()
    {
        ObjectPoseProviderPluginUser& parent = this->parent<ObjectPoseProviderPluginUser>();
        objpose::ObjectPoseTopicPrx& topic = parent.objectPoseTopic;
        if (topic)
        {
            objpose::ProviderInfo info = parent.getProviderInfo();
            if (!info.proxy)
            {
                info.proxy = ::armarx::objpose::ObjectPoseProviderPrx::checkedCast(parent.getProxy());
            }
            topic->reportProviderAvailable(parent.getName(), info);
        }
        else
        {
            ARMARX_ERROR << "Object pose topic not ready.";
        }
    }

    objpose::ObjectPoseTopicPrx ObjectPoseProviderPlugin::createObjectPoseTopic()
    {
        return parent<Component>().getTopicFromProperty<objpose::ObjectPoseTopicPrx>(makePropertyName(PROPERTY_NAME));
    }

}


namespace armarx
{

    ObjectPoseProviderPluginUser::ObjectPoseProviderPluginUser()
    {
        addPlugin(plugin);
    }

    objpose::provider::RequestObjectsOutput ObjectPoseProviderPluginUser::requestObjects(const objpose::provider::RequestObjectsInput& input, const Ice::Current&)
    {
        // ARMARX_INFO << "Requested " << input.objectIDs.size() << " objects for " << input.relativeTimeoutMS << " ms.";
        objpose::provider::RequestObjectsOutput output;
        for (const auto& id : input.objectIDs)
        {
            output.results[id].success = false;
        }
        return output;
    }

    objpose::ObjectPoseTopicPrx ObjectPoseProviderPluginUser::createObjectPoseTopic()
    {
        return plugin->createObjectPoseTopic();
    }

}
