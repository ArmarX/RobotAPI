#pragma once

#include <ArmarXCore/core/ComponentPlugin.h>

#include <RobotAPI/interface/objectpose/ObjectPoseProvider.h>


namespace armarx::plugins
{

    class ObjectPoseProviderPlugin : public ComponentPlugin
    {
    public:

        using ComponentPlugin::ComponentPlugin;

        void postCreatePropertyDefinitions(PropertyDefinitionsPtr& properties) override;

        void preOnInitComponent() override;
        void preOnConnectComponent() override;
        void postOnConnectComponent() override;

        objpose::ObjectPoseTopicPrx createObjectPoseTopic();


    private:

        static constexpr const char* PROPERTY_NAME = "ObjectPoseTopicName";

    };

}


#include <ArmarXCore/core/ManagedIceObject.h>

namespace armarx
{

    /**
     * @brief Provides an `objpose::ObjectPoseTopicPrx objectPoseTopic` as member variable.
     */
    class ObjectPoseProviderPluginUser :
        virtual public ManagedIceObject
        , virtual public objpose::ObjectPoseProvider
    {
    public:

        ObjectPoseProviderPluginUser();

        /// Implement to process object requests (empty default implementation).
        objpose::provider::RequestObjectsOutput requestObjects(const objpose::provider::RequestObjectsInput& input, const Ice::Current&) override;

        objpose::ObjectPoseTopicPrx createObjectPoseTopic();

        objpose::ObjectPoseTopicPrx objectPoseTopic;


    private:

        armarx::plugins::ObjectPoseProviderPlugin* plugin = nullptr;

    };
}
