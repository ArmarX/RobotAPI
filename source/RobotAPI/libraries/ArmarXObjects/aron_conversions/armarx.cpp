#include "armarx.h"

#include <RobotAPI/libraries/aron/common/aron_conversions.h>

void armarx::fromAron(const arondto::ObjectID& dto, ObjectID& bo)
{
    bo = ObjectID(dto.dataset, dto.className, dto.instanceName);
}
void armarx::toAron(arondto::ObjectID& dto, const ObjectID& bo)
{
    aron::toAron(dto.dataset, bo.dataset());
    aron::toAron(dto.className, bo.className());
    aron::toAron(dto.instanceName, bo.instanceName());

}


void armarx::fromAron(const armarx::arondto::PackagePath& dto, armarx::PackageFileLocation& bo)
{
    aron::toAron(bo.package, dto.package);
    aron::toAron(bo.relativePath, dto.path);
    aron::toAron(bo.absolutePath, std::filesystem::path(""));

}

void armarx::toAron(armarx::arondto::PackagePath& dto, const armarx::PackageFileLocation& bo)
{
    aron::toAron(dto.package, bo.package);
    aron::toAron(dto.path, bo.relativePath);

}
