#pragma once

#include <RobotAPI/interface/objectpose/object_pose_types.h>
#include <RobotAPI/libraries/ArmarXObjects/forward_declarations.h>


namespace armarx::objpose
{

    void fromAron(const arondto::ObjectAttachmentInfo& dto, ObjectAttachmentInfo& bo);
    void toAron(arondto::ObjectAttachmentInfo& dto, const ObjectAttachmentInfo& bo);

    void fromAron(const arondto::PoseManifoldGaussian& dto, PoseManifoldGaussian& bo);
    void toAron(arondto::PoseManifoldGaussian& dto, const PoseManifoldGaussian& bo);

    void fromAron(const arondto::ObjectType& dto, ObjectType& bo);
    void toAron(arondto::ObjectType& dto, const ObjectType& bo);

    void fromAron(const arondto::ObjectPose& dto, ObjectPose& bo);
    void toAron(arondto::ObjectPose& dto, const ObjectPose& bo);

}
