#pragma once

#include <RobotAPI/libraries/ArmarXObjects/ObjectInfo.h>  // For PackageFileLocation
#include <RobotAPI/libraries/aron/common/aron/PackagePath.aron.generated.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
#include <RobotAPI/libraries/ArmarXObjects/aron/ObjectID.aron.generated.h>


namespace armarx
{
    void fromAron(const arondto::PackagePath& dto, PackageFileLocation& bo);
    void toAron(arondto::PackagePath& dto, const PackageFileLocation& bo);

    void fromAron(const arondto::ObjectID& dto, ObjectID& bo);
    void toAron(arondto::ObjectID& dto, const ObjectID& bo);
}
