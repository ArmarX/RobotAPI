#pragma once

#include <map>
#include <vector>


namespace armarx
{
    class ObjectID;
    class ObjectInfo;
    class ObjectFinder;
}
namespace armarx::objpose
{
    struct ObjectAttachmentInfo;
    struct PoseManifoldGaussian;

    struct ObjectPose;

    using ObjectPoseSeq = std::vector<ObjectPose>;
    using ObjectPoseMap = std::map<ObjectID, ObjectPose>;

    class ProvidedObjectPose;

    using ProvidedObjectPoseSeq = std::vector<ProvidedObjectPose>;
    using ProvidedObjectPoseMap = std::map<ObjectID, ProvidedObjectPose>;

    class ObjectPoseClient;
}
namespace armarx::objects
{
    struct Scene;
    struct SceneObject;
}


// Ice Types
namespace armarx::objpose::data
{
    class PoseManifoldGaussian;
}
// Aron Types
namespace armarx::objpose::arondto
{
    class ObjectAttachmentInfo;
    class ObjectType;
    class ObjectPose;
    struct PoseManifoldGaussian;
}
