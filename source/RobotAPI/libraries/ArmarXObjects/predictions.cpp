/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects
 * @author     phesch ( ulila at student dot kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "predictions.h"

#include <manif/SE3.h>

#include <SimoxUtility/math/pose/pose.h>
#include <SimoxUtility/math/regression/linear.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/ice_conversions/ice_conversions_templates.h>

namespace armarx::objpose
{

    objpose::ObjectPosePredictionResult
    predictObjectPoseLinear(const std::map<DateTime, ObjectPose>& poses,
                      const DateTime& time,
                      const ObjectPose& latestPose)
    {
        objpose::ObjectPosePredictionResult result;
        result.success = false;

        const DateTime timeOrigin = DateTime::Now();

        static const int tangentDims = 6;
        using Vector6d = Eigen::Matrix<double, tangentDims, 1>;

        std::vector<double> timestampsSec;
        std::vector<Vector6d> tangentPoses;

        // ToDo: How to handle attached objects?
        for (const auto& [timestamp, pose] : poses)
        {
            timestampsSec.push_back((timestamp - timeOrigin).toSecondsDouble());
            manif::SE3f se3(simox::math::position(pose.objectPoseGlobal),
                            Eigen::Quaternionf(simox::math::orientation(pose.objectPoseGlobal)));
            tangentPoses.emplace_back(se3.cast<double>().log().coeffs());
        }

        ARMARX_CHECK_EQUAL(timestampsSec.size(), tangentPoses.size());

        Eigen::Matrix4f prediction;
        // Static objects don't move. Objects that haven't moved for a while probably won't either.
        if (timestampsSec.size() <= 1 || latestPose.isStatic)
        {
            prediction = latestPose.objectPoseGlobal;
        }
        else
        {
            using simox::math::LinearRegression;
            const bool inputOffset = false;

            const LinearRegression<tangentDims> model =
                LinearRegression<tangentDims>::Fit(timestampsSec, tangentPoses, inputOffset);
            const auto predictionTime = armarx::fromIce<DateTime>(time);
            Vector6d linearPred = model.predict((predictionTime - timeOrigin).toSecondsDouble());
            prediction = manif::SE3Tangentd(linearPred).exp().transform().cast<float>();
        }

        // Used as a template for the returned pose prediction.
        ObjectPose latestCopy = latestPose;

        latestCopy.setObjectPoseGlobal(prediction);

        result.success = true;
        result.prediction = latestCopy.toIce();

        return result;
    }
} // namespace armarx::objpose
