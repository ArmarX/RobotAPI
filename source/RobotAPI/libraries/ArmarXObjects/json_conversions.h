#pragma once

#include <SimoxUtility/json/json.h>

#include "forward_declarations.h"


namespace armarx
{
    void to_json(simox::json::json& j, const ObjectID& value);
    void from_json(const simox::json::json& j, ObjectID& value);
}

namespace armarx::objpose
{
    void to_json(simox::json::json& j, const ObjectPose& op);
    void from_json(const simox::json::json& j, ObjectPose& op);
}


namespace armarx::objects
{
    void to_json(simox::json::json& j, const SceneObject& rhs);
    void from_json(const simox::json::json& j, SceneObject& rhs);

    void to_json(simox::json::json& j, const Scene& rhs);
    void from_json(const simox::json::json& j, Scene& rhs);
}
