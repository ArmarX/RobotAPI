#include "ObjectPoseClient.h"
#include <optional>
#include "RobotAPI/libraries/ArmarXObjects/ObjectPose.h"


namespace armarx::objpose
{

    ObjectPoseClient::ObjectPoseClient()
    {
    }


    ObjectPoseClient::ObjectPoseClient(
            const ObjectPoseStorageInterfacePrx& objectPoseStorage,
            const ObjectFinder& finder) :
        objectFinder(finder)
    {
        this->connect(objectPoseStorage);
    }


    void
    ObjectPoseClient::connect(
            const ObjectPoseStorageInterfacePrx& objectPoseStorage)
    {
        this->objectPoseStorage = objectPoseStorage;
    }


    bool
    ObjectPoseClient::isConnected() const
    {
        return bool(objectPoseStorage);
    }


    ObjectPoseSeq
    ObjectPoseClient::fetchObjectPoses() const
    {
        if (not objectPoseStorage)
        {
            ARMARX_WARNING << "No object pose observer.";
            return {};
        }
        return fromIce(objectPoseStorage->getObjectPoses());
    }


    ObjectPoseMap
    ObjectPoseClient::fetchObjectPosesAsMap() const
    {
        ObjectPoseMap map;
        for (auto& pose : fetchObjectPoses())
        {
            map.emplace(pose.objectID, std::move(pose));
        }
        return map;
    }

    
    std::optional<ObjectPose>
    ObjectPoseClient::fetchObjectPose(const ObjectID& objectID) const
    {
        const auto *object = findObjectPoseByID(fetchObjectPoses(), objectID);

        if(object != nullptr)
        {
            return *object;
        }

        return std::nullopt;
    }
    

    ObjectPoseSeq
    ObjectPoseClient::fetchObjectPosesFromProvider(const std::string& providerName) const
    {
        if (!objectPoseStorage)
        {
            ARMARX_WARNING << "No object pose observer.";
            return {};
        }
        return fromIce(objectPoseStorage->getObjectPosesByProvider(providerName));
    }


    const ObjectPoseStorageInterfacePrx&
    ObjectPoseClient::getObjectPoseStorage() const
    {
        return objectPoseStorage;
    }


    const ObjectFinder&
    ObjectPoseClient::getObjectFinder() const
    {
        return objectFinder;
    }

}
