#pragma once

#include <optional>

#include <RobotAPI/interface/objectpose/ObjectPoseStorageInterface.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>


namespace armarx::objpose
{

    /**
     * @brief Provides access to the `armarx::objpose::ObjectPoseStorageInterface`
     * (aka the object memory).
     */
    class ObjectPoseClient
    {
    public:

        /// Construct a disconnected client.
        ObjectPoseClient();
        /// Construct a client and connect it to the object pose storage.
        ObjectPoseClient(
                const ObjectPoseStorageInterfacePrx& objectPoseStorage,
                const ObjectFinder& finder = {}
                );

        /**
         * @brief Connect to the given object pose storage.
         *
         * This function can be used after default-constructing the client.
         *
         * @param objectPoseStorage The object pose storage.
         */
        void
        connect(const ObjectPoseStorageInterfacePrx& objectPoseStorage);

        /**
         * @brief Indicate whether this client is connected to an object pose
         * storage.
         *
         * That is, whether its proxy has been set via the constructor or
         * `connect()`.
         *
         * If false, all `fetch*()` functions will return empty results.
         *
         * @return True if connected
         */
        bool
        isConnected() const;


        /**
         * @brief Fetch all known object poses.
         * @return The known object poses.
         */
        ObjectPoseSeq
        fetchObjectPoses() const;

        /**
         * @brief Fetch all known object poses.
         * @return The known object poses, with object ID as key.
         */
        ObjectPoseMap
        fetchObjectPosesAsMap() const;

        /**
         * @brief Fetch the pose of a single object.
         *
         * This is a network call. If you need multiple object poses, use
         * `fetchObjectPoses()` instead.
         *
         * @param objectID The object's ID.
         * @return The object's pose, if known.
         */
        std::optional<ObjectPose>
        fetchObjectPose(const ObjectID& objectID) const;

        /**
         * @brief Fetch object poses from a specific provider.
         * @param providerName The provider's name.
         * @return The object poses from that provider.
         */
        ObjectPoseSeq
        fetchObjectPosesFromProvider(const std::string& providerName) const;


        /**
         * @brief Get the object pose storage's proxy.
         */
        const ObjectPoseStorageInterfacePrx&
        getObjectPoseStorage() const;

        /**
         * @brief Get the internal object finder.
         */
        const ObjectFinder&
        getObjectFinder() const;


    public:

        ObjectPoseStorageInterfacePrx objectPoseStorage;

        ObjectFinder objectFinder;

    };

}
