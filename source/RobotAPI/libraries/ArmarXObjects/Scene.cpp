#include "Scene.h"

#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>


namespace armarx::objects
{

    ObjectID SceneObject::getClassID() const
    {
        return ObjectID(className);
    }


    ObjectID SceneObject::getClassID(ObjectFinder& finder) const
    {
        ObjectID id = getClassID();
        if (id.dataset().empty())
        {
            if (std::optional<ObjectInfo> info = finder.findObject(id.className()))
            {
                return info->id();
            }
        }
        return id;
    }


    ObjectID SceneObject::getObjectID() const
    {
        return getClassID().withInstanceName(instanceName);
    }


    ObjectID SceneObject::getObjectID(ObjectFinder& finder) const
    {
        return getClassID(finder).withInstanceName(instanceName);
    }

}



