
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore ${LIB_NAME})

armarx_add_test(ArmarXObjectsTest ArmarXObjectsTest.cpp "${LIBS}")
armarx_add_test(ArmarXObjects_ObjectIDTest ObjectIDTest.cpp "${LIBS}")
