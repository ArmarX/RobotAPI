/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArmarXObjects
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::ArmarXObjects

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include "../ArmarXObjects.h"

#include "../ObjectID.h"

#include <iostream>


namespace armarx
{
    struct Fixture
    {
        std::vector<armarx::ObjectID> dcs
        {
            { "Data/Class/0" },
            { "Data/Class/1" },
            { "Data/Class/2" }
        };
        armarx::ObjectID dc0 { "Data/Class/0" };

        std::vector<armarx::ObjectID> ots
        {
            { "Other/Type/0" },
            { "Other/Type/1" },
            { "Other/Type/2" }
        };
        armarx::ObjectID ot0 { "Other/Type/0" };
    };
}

BOOST_FIXTURE_TEST_SUITE(ObjectIDTests, armarx::Fixture)

BOOST_AUTO_TEST_CASE(test_construction_from_string)
{
    for (std::size_t i = 0; i < dcs.size(); ++i)
    {
        BOOST_CHECK_EQUAL(dcs[i].dataset(), "Data");
        BOOST_CHECK_EQUAL(dcs[i].className(), "Class");
        BOOST_CHECK_EQUAL(dcs[i].instanceName(), std::to_string(i));

        BOOST_CHECK_EQUAL(ots[i].dataset(), "Other");
        BOOST_CHECK_EQUAL(ots[i].className(), "Type");
        BOOST_CHECK_EQUAL(ots[i].instanceName(), std::to_string(i));
    }
}

BOOST_AUTO_TEST_CASE(test_equals_operator)
{
    for (std::size_t i = 0; i < dcs.size(); ++i)
    {
        BOOST_TEST_CONTEXT("i=" << i)
        {
            BOOST_CHECK_EQUAL(dcs[i], dcs[i]);
            BOOST_CHECK_NE(dcs[i], ots[i]);
            if (i != 0)
            {
                BOOST_CHECK_NE(dcs[i], dc0);
            }
        }
    }
}

BOOST_AUTO_TEST_CASE(test_less_than_operator)
{
    for (std::size_t i = 0; i < dcs.size(); ++i)
    {
        for (std::size_t j = i; j < dcs.size(); ++j)
        {
            BOOST_CHECK_LE(dcs[i], dcs[j]);
            BOOST_CHECK_GE(dcs[j], dcs[i]);
            if (i != j)
            {
                BOOST_CHECK_LT(dcs[i], dcs[j]);
                BOOST_CHECK_GT(dcs[j], dcs[i]);
            }
        }
    }
}

BOOST_AUTO_TEST_CASE(test_equalClass)
{
    for (std::size_t i = 0; i < dcs.size(); ++i)
    {
        for (std::size_t j = 0; j < dcs.size(); ++j)
        {
            BOOST_CHECK(dcs[i].equalClass(dcs[j]));
            BOOST_CHECK(ots[i].equalClass(ots[j]));

            BOOST_CHECK(!dcs[i].equalClass(ots[j]));
            BOOST_CHECK(!ots[i].equalClass(dcs[j]));
        }
    }
}

BOOST_AUTO_TEST_SUITE_END()
