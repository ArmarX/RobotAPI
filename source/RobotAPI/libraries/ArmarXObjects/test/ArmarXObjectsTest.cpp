/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ArmarXObjects
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE RobotAPI::ArmarXLibraries::ArmarXObjects

#define ARMARX_BOOST_TEST

#include <RobotAPI/Test.h>
#include "../ArmarXObjects.h"

#include <iostream>

#include <RobotAPI/libraries/ArmarXObjects/aron/ObjectPose.aron.generated.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>


namespace fs = std::filesystem;


BOOST_AUTO_TEST_SUITE(arondto_ObjectPose_test)


BOOST_AUTO_TEST_CASE(test_ObjectType_copy_assignment)
{
    BOOST_TEST_MESSAGE("Constructor");
    armarx::objpose::arondto::ObjectType lhs, rhs;

    BOOST_TEST_MESSAGE("Assignment");
    BOOST_CHECK_NO_THROW(lhs = rhs);

    BOOST_TEST_MESSAGE("Done");
}

BOOST_AUTO_TEST_CASE(test_ObjectAttachmentInfo_copy_assignment)
{
    BOOST_TEST_MESSAGE("Constructor");
    armarx::objpose::arondto::ObjectAttachmentInfo lhs, rhs;

    BOOST_TEST_MESSAGE("Assignment");
    BOOST_CHECK_NO_THROW(lhs = rhs);

    BOOST_TEST_MESSAGE("Done");
}

BOOST_AUTO_TEST_CASE(test_ObjectPose_copy_assignment)
{
    BOOST_TEST_MESSAGE("Constructor");
    armarx::objpose::arondto::ObjectPose lhs, rhs;

    BOOST_TEST_MESSAGE("Assignment");
    BOOST_CHECK_NO_THROW(lhs = rhs);

    BOOST_TEST_MESSAGE("Done");
}


BOOST_AUTO_TEST_SUITE_END()



BOOST_AUTO_TEST_SUITE(ObjectFinderTest)


BOOST_AUTO_TEST_CASE(test_find)
{
    using namespace armarx;

    ObjectFinder finder;

    bool checkPaths = false;
    std::vector<ObjectInfo> objects = finder.findAllObjects(checkPaths);
    BOOST_CHECK_GT(objects.size(), 0);

    for (const ObjectInfo& object : objects)
    {
        fs::path simoxXML = object.simoxXML().absolutePath;
        fs::path objectDir = simoxXML.parent_path();
        BOOST_TEST_CONTEXT("Object: " << object.id() << " at " << objectDir)
        {
            BOOST_CHECK(fs::is_directory(objectDir));
#if 0
            BOOST_CHECK(fs::is_regular_file(simoxXML)
                        || fs::is_regular_file(object.articulatedSimoxXML().absolutePath));
#endif
        }
    }
}



BOOST_AUTO_TEST_SUITE_END()
