/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "util.h"

#include <string>

#include <Eigen/Geometry>

#include <VirtualRobot/ManipulationObject.h>
#include <VirtualRobot/SceneObjectSet.h>

#include <RobotAPI/libraries/ArmarXObjects/ObjectFinder.h>

namespace armarx::objpose
{

    objpose::ObjectPoseSeq
    filterObjects(objpose::ObjectPoseSeq objects, const std::vector<std::string>& datasetBlocklist)
    {
        const auto isBlacklisted = [&datasetBlocklist](const objpose::ObjectPose& objectPose)
        {
            const auto dataset = objectPose.objectID.dataset();

            return std::find(datasetBlocklist.begin(), datasetBlocklist.end(), dataset) !=
                   datasetBlocklist.end();
        };

        objects.erase(std::remove_if(objects.begin(), objects.end(), isBlacklisted), objects.end());
        return objects;
    }


    VirtualRobot::ManipulationObjectPtr
    asManipulationObject(const objpose::ObjectPose& objectPose)
    {
        ObjectFinder finder;

        VirtualRobot::SceneObjectSetPtr sceneObjects(new VirtualRobot::SceneObjectSet);
        if (auto obstacle = finder.loadManipulationObject(objectPose))
        {
            obstacle->setGlobalPose(objectPose.objectPoseGlobal);
            return obstacle;
        }

        ARMARX_WARNING << "Failed to load scene object `" << objectPose.objectID << "`";
        return nullptr;
    }


    VirtualRobot::SceneObjectSetPtr
    asSceneObjects(const objpose::ObjectPoseSeq& objectPoses)
    {
        ObjectFinder finder;

        VirtualRobot::SceneObjectSetPtr sceneObjects(new VirtualRobot::SceneObjectSet);
        for (const auto& objectPose : objectPoses)
        {
            if (auto obstacle = finder.loadManipulationObject(objectPose))
            {
                obstacle->setGlobalPose(objectPose.objectPoseGlobal);
                sceneObjects->addSceneObject(obstacle);
            }
        }

        return sceneObjects;
    }


} // namespace armarx::objpose
