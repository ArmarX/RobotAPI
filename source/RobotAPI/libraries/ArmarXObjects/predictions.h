/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects
 * @author     phesch ( ulila at student dot kit dot edu )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/time/DateTime.h>

#include <RobotAPI/interface/objectpose/ObjectPoseStorageInterface.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectPose.h>

namespace armarx::objpose
{
    /**
    * @brief Predict the pose of an object given a history of poses
    * based on a linear regression.
    *
    * If `poses` is empty, `latestPose` is returned.
    *
    * @param poses the history of poses to base the prediction on
    * @param time the timestamp to make the prediction for
    * @param latestPose used for metadata so the result is valid even if poses is empty
    * @param settings the settings to use for the prediction
    * @return the result of the prediction
    */
    objpose::ObjectPosePredictionResult
    predictObjectPoseLinear(
            const std::map<DateTime, ObjectPose>& poses,
            const DateTime& time,
            const ObjectPose& latestPose);

} // namespace armarx::objpose
