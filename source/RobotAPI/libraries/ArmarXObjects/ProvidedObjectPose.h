#pragma once

#include <optional>
#include <map>
#include <vector>

#include <Eigen/Core>

#include <SimoxUtility/shapes/OrientedBox.h>

#include <ArmarXCore/core/time/DateTime.h>

#include <RobotAPI/interface/objectpose/object_pose_types.h>
#include <RobotAPI/libraries/ArmarXObjects/forward_declarations.h>
#include <RobotAPI/libraries/ArmarXObjects/ObjectID.h>
#include <RobotAPI/libraries/ArmarXObjects/PoseManifoldGaussian.h>


namespace armarx::objpose
{

    /**
     * @brief An object pose provided by an ObjectPoseProvider.
     */
    class ProvidedObjectPose
    {
    public:

        ProvidedObjectPose();
        ProvidedObjectPose(const data::ProvidedObjectPose& ice);

        void fromIce(const data::ProvidedObjectPose& ice);

        data::ProvidedObjectPose toIce() const;
        void toIce(data::ProvidedObjectPose& ice) const;

    public:

        /// Name of the providing component.
        std::string providerName;
        /// Known or unknown object.
        ObjectType objectType = AnyObject;
        /// Whether object is static. Static objects don't decay.
        bool isStatic = false;

        /// The object ID, i.e. dataset, class name and instance name.
        armarx::ObjectID objectID;

        Eigen::Matrix4f objectPose = Eigen::Matrix4f::Identity();
        std::string objectPoseFrame;
        std::optional<PoseManifoldGaussian> objectPoseGaussian;

        /// The object's joint values if it is articulated.
        std::map<std::string, float> objectJointValues;


        /// Confidence in [0, 1] (1 = full, 0 = none).
        float confidence = 0;
        /// Source timestamp.
        DateTime timestamp = DateTime::Invalid();

        /// Object bounding box in object's local coordinate frame.
        /// @see oobbRobot(), oobbGlobal()
        std::optional<simox::OrientedBoxf> localOOBB;
    };



    void fromIce(const data::ProvidedObjectPose& ice, ProvidedObjectPose& pose);
    ProvidedObjectPose fromIce(const data::ProvidedObjectPose& ice);

    void fromIce(const data::ProvidedObjectPoseSeq& ice, ProvidedObjectPoseSeq& poses);
    ProvidedObjectPoseSeq fromIce(const data::ProvidedObjectPoseSeq& ice);


    void toIce(data::ProvidedObjectPose& ice, const ProvidedObjectPose& pose);
    data::ProvidedObjectPose toIce(const ProvidedObjectPose& pose);

    void toIce(data::ProvidedObjectPoseSeq& ice, const ProvidedObjectPoseSeq& poses);
    data::ProvidedObjectPoseSeq toIce(const ProvidedObjectPoseSeq& poses);


}
