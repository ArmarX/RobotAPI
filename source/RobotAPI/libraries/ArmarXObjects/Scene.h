/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ObjectMemory
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <map>
#include <string>
#include <vector>
#include <optional>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "forward_declarations.h"


namespace armarx::objects
{

    struct SceneObject
    {
        std::string className;
        std::string instanceName;
        std::string collection;

        Eigen::Vector3f position = Eigen::Vector3f::Zero();
        Eigen::Quaternionf orientation = Eigen::Quaternionf::Identity();

        std::optional<bool> isStatic;
        std::map<std::string, float> jointValues;

        ObjectID getClassID() const;
        ObjectID getClassID(ObjectFinder& finder) const;
        ObjectID getObjectID() const;
        ObjectID getObjectID(ObjectFinder& finder) const;
    };

    struct Scene
    {
        std::vector<SceneObject> objects;
    };

}
