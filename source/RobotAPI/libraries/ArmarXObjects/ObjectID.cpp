#include "ObjectID.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <SimoxUtility/algorithm/string/string_tools.h>


namespace armarx
{
    ObjectID::ObjectID()
    {
    }

    ObjectID::ObjectID(const std::string& dataset, const std::string& className, const std::string& instancName) :
        _dataset(dataset), _className(className), _instanceName(instancName)
    {
    }

    ObjectID::ObjectID(const std::string& nameOrID)
    {
        if (nameOrID.find("/") != nameOrID.npos)
        {
            const std::vector<std::string> split = simox::alg::split(nameOrID, "/", true);
            ARMARX_CHECK(split.size() == 2 || split.size() == 3)
                    << "Expected ID of format 'Dataset/ClassName' or 'Dataset/ClassName/InstanceName'"
                    << ", but got: '" << nameOrID << "' (too many '/').";
            _dataset = split[0];
            _className = split[1];
            if (split.size() == 3)
            {
                _instanceName = split[2];
            }
        }
        else
        {
            // Dataset, instanceName are left empty.
            _className = nameOrID;
        }
    }

    std::string ObjectID::str() const
    {
        std::string _str = _dataset + "/" + _className;
        if (!_instanceName.empty())
        {
            _str += "/" + _instanceName;
        }
        return _str;
    }

    ObjectID ObjectID::getClassID() const
    {
        return ObjectID(_dataset, _className);
    }

    bool ObjectID::equalClass(const ObjectID& rhs) const
    {
        return _className == rhs._className && _dataset == rhs._dataset;
    }

    ObjectID ObjectID::withInstanceName(const std::string& instanceName) const
    {
        return ObjectID(_dataset, _className, instanceName);
    }

    bool ObjectID::operator==(const ObjectID& rhs) const
    {
        return _className == rhs._className
               && _dataset == rhs._dataset
               && _instanceName == rhs._instanceName;
    }

    bool ObjectID::operator<(const ObjectID& rhs) const
    {
        int c = _dataset.compare(rhs._dataset);
        if (c != 0)
        {
            return c < 0;
        }
        // equal dataset
        c = _className.compare(rhs._className);
        if (c != 0)
        {
            return c < 0;
        }
        // equal class name
        return _instanceName < rhs._instanceName;
    }

}


std::ostream& armarx::operator<<(std::ostream& os, const ObjectID& id)
{
    return os << "'" << id.str() << "'";
}
