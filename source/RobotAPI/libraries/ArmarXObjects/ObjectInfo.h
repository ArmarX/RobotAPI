#pragma once

#include <filesystem>
#include <optional>
#include <string>
#include <vector>

#include "ObjectID.h"


namespace simox
{
    // #include <SimoxUtility/shapes/AxisAlignedBoundingBox.h>
    struct AxisAlignedBoundingBox;
    // #include <SimoxUtility/shapes/OrientedBox.h>
    template<class FloatT> class OrientedBox;
}

namespace armarx
{

    struct PackageFileLocation
    {
        /// Name of the ArmarX package.
        std::string package;

        /// Relative to the package's data directory.
        std::string relativePath;
        /// The absolute path (in the host's file system).
        std::filesystem::path absolutePath;
    };


    /**
     * @brief Accessor for the object files.
     */
    class ObjectInfo
    {
    public:
        using path = std::filesystem::path;

    public:

        /**
         * @brief ObjectInfo
         *
         * @param packageName The ArmarX package.
         * @param absPackageDataDir Absolute path to the package's data directory.
         * @param localObjectsPath The path where objects are stored in the data directory.
         * @param id The object class ID (with dataset and class name).
         */
        ObjectInfo(const std::string& packageName, const path& absPackageDataDir,
                   const path& relObjectsPath, const ObjectID& id);
        ObjectInfo(const std::string& packageName, const path& packageDataDir,
                   const path& relObjectsPath,
                   const std::string& dataset, const std::string& className);


        virtual ~ObjectInfo() = default;


        void setLogError(bool enabled);

        std::string package() const;

        std::string dataset() const;
        std::string className() const;
        [[deprecated("This function is deprecated. Use className() instead.")]]
        std::string name() const
        {
            return className();
        }

        /// Return "dataset/name".
        ObjectID id() const;
        std::string idStr() const;


        PackageFileLocation file(const std::string& extension, const std::string& suffix = "") const;


        PackageFileLocation simoxXML() const;

        PackageFileLocation articulatedSimoxXML() const;
        PackageFileLocation articulatedUrdf() const;
        /// Return the articulated Simox XML or URDF, if one exists.
        std::optional<PackageFileLocation> getArticulatedModel() const;


        PackageFileLocation meshWrl() const;
        PackageFileLocation wavefrontObj() const;

        PackageFileLocation boundingBoxJson() const;

        /// File containing recognized and spoken names of objects.
        PackageFileLocation namesJson() const;


        /**
         * @brief Load the AABB (axis-aligned bounding-box) from the bounding box JSON file.
         * @return Return the AABB if successful, `std::nullopt` if file does not exist.
         */
        std::optional<simox::AxisAlignedBoundingBox> loadAABB() const;
        /**
         * @brief Load the OOBB (object-oriented bounding box) from the bounding box JSON file.
         * The OOBB is defined the object's local frame.
         * @return Return the OOBB if successful, `std::nullopt` if file does not exist.
         */
        std::optional<simox::OrientedBox<float>> loadOOBB() const;

        /**
         * @brief Load names to use when matched when recognizing an object by name.
         * @see `namesJson()`
         */
        std::optional<std::vector<std::string>> loadRecognizedNames() const;
        /**
         * @brief Load names to use when verbalizing an object name.
         * @see `namesJson()`
         */
        std::optional<std::vector<std::string>> loadSpokenNames() const;


        /**
         * @brief Checks the existence of expected files.
         * If a file is does not exist, emits a warning returns false.
         * @return True if all existing files are found, false otherwise.
         */
        virtual bool checkPaths() const;



    private:

        path objectDirectory() const;

        std::optional<std::vector<std::string>> loadNames(const std::string& jsonKey) const;


    private:

        std::string _packageName;
        path _absPackageDataDir;
        path _relObjectsPath;

        ObjectID _id;

        bool _logError = true;

    };

    std::ostream& operator<<(std::ostream& os, const ObjectInfo& rhs);


    inline bool operator==(const ObjectInfo& lhs, const ObjectInfo& rhs)
    {
        return lhs.id() == rhs.id();
    }
    inline bool operator!=(const ObjectInfo& lhs, const ObjectInfo& rhs)
    {
        return lhs.id() != rhs.id();
    }
    inline bool operator< (const ObjectInfo& lhs, const ObjectInfo& rhs)
    {
        return lhs.id() < rhs.id();
    }
    inline bool operator> (const ObjectInfo& lhs, const ObjectInfo& rhs)
    {
        return lhs.id() > rhs.id();
    }
    inline bool operator<=(const ObjectInfo& lhs, const ObjectInfo& rhs)
    {
        return lhs.id() <= rhs.id();
    }
    inline bool operator>=(const ObjectInfo& lhs, const ObjectInfo& rhs)
    {
        return lhs.id() >= rhs.id();
    }

}
