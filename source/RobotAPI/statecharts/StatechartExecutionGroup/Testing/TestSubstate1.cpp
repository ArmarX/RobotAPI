/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::StatechartExecutionGroup
 * @author     Stefan Reither ( stefan dot reither at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TestSubstate1.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

namespace armarx::StatechartExecutionGroup
{
    // DO NOT EDIT NEXT LINE
    TestSubstate1::SubClassRegistry TestSubstate1::Registry(TestSubstate1::GetName(), &TestSubstate1::CreateInstance);

    void TestSubstate1::onEnter()
    {
        // put your user code for the enter-point here
        // execution time should be short (<100ms)
        ARMARX_INFO << "Substate onEnter";
    }

    void TestSubstate1::run()
    {

        int i = in.getInputInt();
        ARMARX_INFO << "Substate int: " << i;
        // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
        while (!isRunningTaskStopped() && i < 10) // stop run function if returning true
        {
            ARMARX_INFO << "SubstateIntLoop; value: " << i;
            i++;
            out.setOutputInt(i);
            sleep(1);
        }

        emitSuccess();
    }

    void TestSubstate1::onBreak()
    {
        // put your user code for the breaking point here
        // execution time should be short (<100ms)
        ARMARX_INFO << "Substate broke";
    }

    void TestSubstate1::onExit()
    {
        // put your user code for the exit point here
        // execution time should be short (<100ms)
        ARMARX_INFO << "Substate exited";
    }


    // DO NOT EDIT NEXT FUNCTION
    XMLStateFactoryBasePtr TestSubstate1::CreateInstance(XMLStateConstructorParams stateData)
    {
        return XMLStateFactoryBasePtr(new TestSubstate1(stateData));
    }
}
