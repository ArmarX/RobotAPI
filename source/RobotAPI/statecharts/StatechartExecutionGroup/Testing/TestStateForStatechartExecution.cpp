/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::StatechartExecutionGroup
 * @author     Stefan Reither ( stefan dot reither at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TestStateForStatechartExecution.h"

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <RobotAPI/libraries/core/Pose.h>

using namespace armarx;
using namespace StatechartExecutionGroup;

// DO NOT EDIT NEXT LINE
TestStateForStatechartExecution::SubClassRegistry TestStateForStatechartExecution::Registry(TestStateForStatechartExecution::GetName(), &TestStateForStatechartExecution::CreateInstance);



void TestStateForStatechartExecution::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    i = 0;
    std::cout << "Hallo onEnter()" << std::endl;

    PosePtr p;
    if (in.isblaSet())
    {
        ARMARX_INFO << VAROUT(*in.getbla());
        p = in.getbla();
    }
    if (in.isintTestSet())
    {
        ARMARX_INFO << VAROUT(in.getintTest());
    }

    if (in.isbla2Set())
    {
        ARMARX_INFO << VAROUT(*in.getbla2());
    }

    if (in.isintListTestSet())
    {
        ARMARX_INFO << VAROUT(*in.getintListTest()[0]);
    }
    out.setOutputPose(p);
}

void TestStateForStatechartExecution::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
    ARMARX_INFO << "MainState broke";
}

void TestStateForStatechartExecution::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
    //    out.setOutputInt(500);
    ARMARX_INFO << "MainState exited";
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TestStateForStatechartExecution::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TestStateForStatechartExecution(stateData));
}

