/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::DebugDrawerToArVizGroup::DebugDrawerToArVizGroupRemoteStateOfferer
 * @author     Rainer Kartmann ( rainer dot kartmann at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DebugDrawerToArVizGroupRemoteStateOfferer.h"

namespace armarx::DebugDrawerToArVizGroup
{
    // DO NOT EDIT NEXT LINE
    DebugDrawerToArVizGroupRemoteStateOfferer::SubClassRegistry DebugDrawerToArVizGroupRemoteStateOfferer::Registry(DebugDrawerToArVizGroupRemoteStateOfferer::GetName(), &DebugDrawerToArVizGroupRemoteStateOfferer::CreateInstance);

    DebugDrawerToArVizGroupRemoteStateOfferer::DebugDrawerToArVizGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
        XMLRemoteStateOfferer < DebugDrawerToArVizGroupStatechartContext > (reader)
    {}

    void DebugDrawerToArVizGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer() {}

    void DebugDrawerToArVizGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer() {}

    void DebugDrawerToArVizGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer() {}

    // DO NOT EDIT NEXT FUNCTION
    std::string DebugDrawerToArVizGroupRemoteStateOfferer::GetName()
    {
        return "DebugDrawerToArVizGroupRemoteStateOfferer";
    }

    // DO NOT EDIT NEXT FUNCTION
    XMLStateOffererFactoryBasePtr DebugDrawerToArVizGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
    {
        return XMLStateOffererFactoryBasePtr(new DebugDrawerToArVizGroupRemoteStateOfferer(reader));
    }
}
