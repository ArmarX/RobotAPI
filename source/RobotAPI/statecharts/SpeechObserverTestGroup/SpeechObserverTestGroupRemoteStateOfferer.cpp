/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::SpeechObserverTestGroup::SpeechObserverTestGroupRemoteStateOfferer
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "SpeechObserverTestGroupRemoteStateOfferer.h"

using namespace armarx;
using namespace SpeechObserverTestGroup;

// DO NOT EDIT NEXT LINE
SpeechObserverTestGroupRemoteStateOfferer::SubClassRegistry SpeechObserverTestGroupRemoteStateOfferer::Registry(SpeechObserverTestGroupRemoteStateOfferer::GetName(), &SpeechObserverTestGroupRemoteStateOfferer::CreateInstance);



SpeechObserverTestGroupRemoteStateOfferer::SpeechObserverTestGroupRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < SpeechObserverTestGroupStatechartContext > (reader)
{
}

void SpeechObserverTestGroupRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void SpeechObserverTestGroupRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void SpeechObserverTestGroupRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string SpeechObserverTestGroupRemoteStateOfferer::GetName()
{
    return "SpeechObserverTestGroupRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr SpeechObserverTestGroupRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new SpeechObserverTestGroupRemoteStateOfferer(reader));
}



