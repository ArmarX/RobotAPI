/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::SpeechObserverTestGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TestTextToSpeech.h"

#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace SpeechObserverTestGroup;

// DO NOT EDIT NEXT LINE
TestTextToSpeech::SubClassRegistry TestTextToSpeech::Registry(TestTextToSpeech::GetName(), &TestTextToSpeech::CreateInstance);



void TestTextToSpeech::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void TestTextToSpeech::waitForSpeechFinished()
{
    TimeUtil::SleepMS(20);
    while (true)
    {
        if (getSpeechObserver()->getDatafieldByName("TextToSpeech", "State")->getString() == "FinishedSpeaking")
        {
            break;
        }
        TimeUtil::SleepMS(10);
    }
}

void TestTextToSpeech::run()
{

    getTextToSpeech()->reportText("Hello!");
    waitForSpeechFinished();
    getTextToSpeech()->reportText("Goodbye!");


    // uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        // do your calculations
    }
}

//void TestTextToSpeech::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TestTextToSpeech::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TestTextToSpeech::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TestTextToSpeech(stateData));
}

