/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::OrientedTactileSensorGroup
 * @author     andreeatulbure ( andreea_tulbure at yahoo dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "OrientedTactileSensorTest.h"

using namespace armarx;
using namespace OrientedTactileSensorGroup;

// DO NOT EDIT NEXT LINE
OrientedTactileSensorTest::SubClassRegistry OrientedTactileSensorTest::Registry(OrientedTactileSensorTest::GetName(), &OrientedTactileSensorTest::CreateInstance);



void OrientedTactileSensorTest::onEnter()
{
    //OrientedTactileSensorGroupStatechartContext* context = getContext<OrientedTactileSensorGroupStatechartContext>();
    //HapticUnitObserverInterfacePrx hapticObserver = context->getHapticObserver();
    //ChannelRegistry channels = hapticObserver->getAvailableChannels(false);
    //std::map<std::string, DatafieldRefPtr> tactileDatafields_MaximumValueMap;

    //local.setTactileDatafields_MaximumValue(tactileDatafields_MaximumValueMap);
}

//void OrientedTactileSensorTest::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void OrientedTactileSensorTest::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void OrientedTactileSensorTest::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr OrientedTactileSensorTest::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new OrientedTactileSensorTest(stateData));
}

