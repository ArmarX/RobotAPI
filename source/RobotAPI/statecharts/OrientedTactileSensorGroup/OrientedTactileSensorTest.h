/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::OrientedTactileSensorGroup
 * @author     andreeatulbure ( andreea_tulbure at yahoo dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/statecharts/OrientedTactileSensorGroup/OrientedTactileSensorTest.generated.h>

namespace armarx::OrientedTactileSensorGroup
{
    class OrientedTactileSensorTest :
        public OrientedTactileSensorTestGeneratedBase <OrientedTactileSensorTest>
    {
    public:
        OrientedTactileSensorTest(const XMLStateConstructorParams& stateData):
            XMLStateTemplate <OrientedTactileSensorTest> (stateData), OrientedTactileSensorTestGeneratedBase <OrientedTactileSensorTest> (stateData)
        {
        }

        // inherited from StateBase
        void onEnter() override;
        // void run();
        // void onBreak();
        void onExit() override;

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter
    };
}
