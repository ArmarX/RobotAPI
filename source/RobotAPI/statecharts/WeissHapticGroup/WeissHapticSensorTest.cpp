/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::WeissHapticGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "WeissHapticSensorTest.h"

#include <RobotAPI/libraries/core/observerfilters/MatrixFilters.h>
#include <RobotAPI/libraries/core/observerfilters/OffsetFilter.h>

#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace WeissHapticGroup;

//// DO NOT EDIT NEXT LINE
WeissHapticSensorTest::SubClassRegistry WeissHapticSensorTest::Registry(WeissHapticSensorTest::GetName(), &WeissHapticSensorTest::CreateInstance);



WeissHapticSensorTest::WeissHapticSensorTest(const XMLStateConstructorParams& stateData) :
    XMLStateTemplate<WeissHapticSensorTest>(stateData),  WeissHapticSensorTestGeneratedBase<WeissHapticSensorTest>(stateData)
{
}


void WeissHapticSensorTest::onEnter()
{
    HapticUnitObserverInterfacePrx hapticObserver = getHapticObserver();
    ChannelRegistry channels = hapticObserver->getAvailableChannels(false);
    std::map<std::string, DatafieldRefPtr> tactileDatafields_MaximumValueMap;

    if (channels.size() == 0)
    {
        ARMARX_WARNING << "No tactile pads found";
    }
    else
    {
        ARMARX_INFO << "Creating tactile channels";

        for (std::pair<std::string, ChannelRegistryEntry> pair : channels)
        {
            std::string tactilePad = pair.first;
            DatafieldRefBasePtr matrixDatafield = new DatafieldRef(hapticObserver, tactilePad, "matrix");
            DatafieldRefBasePtr matrixNulled = hapticObserver->createFilteredDatafield(DatafieldFilterBasePtr(new filters::OffsetFilter()), matrixDatafield);
            DatafieldRefBasePtr matrixMax = hapticObserver->createFilteredDatafield(DatafieldFilterBasePtr(new filters::MatrixMaxFilter()), matrixNulled);
            tactileDatafields_MaximumValueMap.insert(std::make_pair(tactilePad, DatafieldRefPtr::dynamicCast(matrixMax)));
        }
    }

    local.setTactileDatafields_MaximumValue(tactileDatafields_MaximumValueMap);


}

void WeissHapticSensorTest::run()
{
    std::map<std::string, DatafieldRefPtr> tactileDatafields_MaximumValueMap = local.getTactileDatafields_MaximumValue();

    while (!isRunningTaskStopped()) // stop run function if returning true
    {
        std::stringstream ss;
        std::stringstream ssNames;
        int max = 0;

        for (std::pair<std::string, DatafieldRefPtr> pair : tactileDatafields_MaximumValueMap)
        {
            std::string tactilePad = pair.first;
            DatafieldRefPtr matrixMax = pair.second;
            int padMax = (int)matrixMax->getDataField()->getFloat();
            ss << padMax << "; ";
            ssNames << tactilePad << "; ";
            max = std::max(max, padMax);
        }

        ARMARX_IMPORTANT << "tactile max value: " << max << ";  \n\n" << ss.str() << "\n" << ssNames.str();

        usleep(10000); // 100ms
    }

}

void WeissHapticSensorTest::onBreak()
{
    // put your user code for the breaking point here
    // execution time should be short (<100ms)
}

void WeissHapticSensorTest::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)

}

// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr WeissHapticSensorTest::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new WeissHapticSensorTest(stateData));
}

