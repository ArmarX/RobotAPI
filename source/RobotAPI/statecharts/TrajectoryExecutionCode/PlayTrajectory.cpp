/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::TrajectoryExecutionCode
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PlayTrajectory.h"

#include <RobotAPI/interface/units/RobotUnit/NJointTrajectoryController.h>

#include <RobotAPI/libraries/core/Trajectory.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace TrajectoryExecutionCode;

// DO NOT EDIT NEXT LINE
PlayTrajectory::SubClassRegistry PlayTrajectory::Registry(PlayTrajectory::GetName(), &PlayTrajectory::CreateInstance);



void PlayTrajectory::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
    NJointTrajectoryControllerConfigPtr cfg(new NJointTrajectoryControllerConfig());
    cfg->jointNames.push_back("Hip Yaw");
    cfg->maxAcceleration = 0.05;
    cfg->maxVelocity = 0.3;
    NJointTrajectoryControllerInterfacePrx prx = NJointTrajectoryControllerInterfacePrx::checkedCast(getRobotUnit()->getNJointController("ActorNJointTrajectoryController"));
    if (!prx)
    {
        prx = NJointTrajectoryControllerInterfacePrx::checkedCast(getRobotUnit()->createNJointController("NJointTrajectoryController", "ActorNJointTrajectoryController", cfg));

    }
    else
    {
        ARMARX_INFO << "deactivating controller";
        prx->deactivateController();
        ARMARX_INFO << "DONE deactivating controller";
    }
    TrajectoryPtr t = new Trajectory;

    Ice::DoubleSeq sr = {0, 1, 1.6, 1, 0};
    t->addDimension(sr, {}, "Hip Yaw");
    prx->setTrajectory(t);
    getRobotUnit()->activateNJointController("ActorNJointTrajectoryController");
}

//void PlayTrajectory::run()
//{
//    // put your user code for the execution-phase here
//    // runs in seperate thread, thus can do complex operations
//    // should check constantly whether isRunningTaskStopped() returns true
//
//// uncomment this if you need a continous run function. Make sure to use sleep or use blocking wait to reduce cpu load.
//    while (!isRunningTaskStopped()) // stop run function if returning true
//    {
//        // do your calculations
//    }
//}

//void PlayTrajectory::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void PlayTrajectory::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr PlayTrajectory::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new PlayTrajectory(stateData));
}

