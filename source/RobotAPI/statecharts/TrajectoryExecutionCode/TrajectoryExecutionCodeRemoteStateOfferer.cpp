/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::TrajectoryExecutionCode::TrajectoryExecutionCodeRemoteStateOfferer
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TrajectoryExecutionCodeRemoteStateOfferer.h"

using namespace armarx;
using namespace TrajectoryExecutionCode;

// DO NOT EDIT NEXT LINE
TrajectoryExecutionCodeRemoteStateOfferer::SubClassRegistry TrajectoryExecutionCodeRemoteStateOfferer::Registry(TrajectoryExecutionCodeRemoteStateOfferer::GetName(), &TrajectoryExecutionCodeRemoteStateOfferer::CreateInstance);



TrajectoryExecutionCodeRemoteStateOfferer::TrajectoryExecutionCodeRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
    XMLRemoteStateOfferer < TrajectoryExecutionCodeStatechartContext > (reader)
{
}

void TrajectoryExecutionCodeRemoteStateOfferer::onInitXMLRemoteStateOfferer()
{

}

void TrajectoryExecutionCodeRemoteStateOfferer::onConnectXMLRemoteStateOfferer()
{

}

void TrajectoryExecutionCodeRemoteStateOfferer::onExitXMLRemoteStateOfferer()
{

}

// DO NOT EDIT NEXT FUNCTION
std::string TrajectoryExecutionCodeRemoteStateOfferer::GetName()
{
    return "TrajectoryExecutionCodeRemoteStateOfferer";
}

// DO NOT EDIT NEXT FUNCTION
XMLStateOffererFactoryBasePtr TrajectoryExecutionCodeRemoteStateOfferer::CreateInstance(StatechartGroupXmlReaderPtr reader)
{
    return XMLStateOffererFactoryBasePtr(new TrajectoryExecutionCodeRemoteStateOfferer(reader));
}



