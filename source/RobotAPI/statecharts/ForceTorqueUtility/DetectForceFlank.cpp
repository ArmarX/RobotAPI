/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ForceTorqueUtility
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <thread>

#include <RobotAPI/libraries/core/FramedPose.h>

#include "DetectForceFlank.h"

using namespace armarx;
using namespace ForceTorqueUtility;

// DO NOT EDIT NEXT LINE
DetectForceFlank::SubClassRegistry DetectForceFlank::Registry(DetectForceFlank::GetName(), &DetectForceFlank::CreateInstance);

void DetectForceFlank::run()
{
    ARMARX_CHECK_EXPRESSION(in.getTriggerOnDecreasingForceVectorLength() || in.getTriggerOnIncreasingForceVectorLength());

    const float forceThreshold = in.getForceVectorLengthThreshold();
    DatafieldRefPtr forceDf = DatafieldRefPtr::dynamicCast(getForceTorqueObserver()->getForceDatafield(in.getFTDatafieldName()));
    const Eigen::Vector3f weights = in.getForceWeights()->toEigen();
    const float initialForce = forceDf->getDataField()->get<FramedDirection>()->toEigen().cwiseProduct(weights).norm();

    while (!isRunningTaskStopped()) // stop run function if returning true
    {

        const float force = forceDf->getDataField()->get<FramedDirection>()->toEigen().cwiseProduct(weights).norm();
        ARMARX_INFO << deactivateSpam(1) << VAROUT(force) << " " << VAROUT(initialForce);
        if (
            (
                in.getTriggerOnDecreasingForceVectorLength() &&
                force < initialForce &&
                initialForce - force >= forceThreshold
            ) ||
            (
                in.getTriggerOnIncreasingForceVectorLength() &&
                force > initialForce &&
                force - initialForce >= forceThreshold
            )
        )
        {
            emitSuccess();
            return;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds{10});
    }
    emitFailure();
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr DetectForceFlank::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new DetectForceFlank(stateData));
}

