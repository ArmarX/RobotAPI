/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::RobotNameHelperTestGroup
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "TestGetNames.h"

#include <RobotAPI/libraries/RobotStatechartHelpers/RobotNameHelper.h>

//#include <ArmarXCore/core/time/TimeUtil.h>
//#include <ArmarXCore/observers/variant/DatafieldRef.h>

using namespace armarx;
using namespace RobotNameHelperTestGroup;

// DO NOT EDIT NEXT LINE
TestGetNames::SubClassRegistry TestGetNames::Registry(TestGetNames::GetName(), &TestGetNames::CreateInstance);



void TestGetNames::onEnter()
{
    // put your user code for the enter-point here
    // execution time should be short (<100ms)
}

void TestGetNames::run()
{
    ARMARX_IMPORTANT << "Profiles: " << getRobotNameHelper()->getProfiles();
    auto side = [&](const auto s)
    {
        const auto arm = getRobotNameHelper()->getArm(s);
        try
        {
            ARMARX_IMPORTANT << "qwerqwer               : " << arm.select("qwerqwer");
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "getSide               : " << arm.getSide();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "getKinematicChain     : " << arm.getKinematicChain();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "getTorsoKinematicChain: " << arm.getTorsoKinematicChain();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "getTCP                : " << arm.getTCP();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "getForceTorqueSensor  : " << arm.getForceTorqueSensor();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "getEndEffector        : " << arm.getEndEffector();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "getMemoryHandName     : " << arm.getMemoryHandName();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "getHandControllerName : " << arm.getHandControllerName();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "getHandModelPackage   : " << arm.getHandModelPackage();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "getHandModelPath      : " << arm.getHandModelPath();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "getHandRootNode       : " << arm.getHandRootNode();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }

        RobotNameHelper::RobotArm robarm = arm.addRobot(getRobot());
        try
        {
            ARMARX_IMPORTANT << s << "Arm: " << robarm.getKinematicChain()->getName();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << s << "Arm TCP: " << robarm.getTCP()->getName();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "HandRootNode: " << robarm.getHandRootNode()->getName();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "TorsoKinematicChain: " << robarm.getTorsoKinematicChain()->getName();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
        try
        {
            ARMARX_IMPORTANT << "Tcp2HandRootTransform: " << robarm.getTcp2HandRootTransform();
        }
        catch (...)
        {
            ARMARX_ERROR << GetHandledExceptionString();
        }
    };
    side("Left");
    side("Right");
}

//void TestGetNames::onBreak()
//{
//    // put your user code for the breaking point here
//    // execution time should be short (<100ms)
//}

void TestGetNames::onExit()
{
    // put your user code for the exit point here
    // execution time should be short (<100ms)
}


// DO NOT EDIT NEXT FUNCTION
XMLStateFactoryBasePtr TestGetNames::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new TestGetNames(stateData));
}

