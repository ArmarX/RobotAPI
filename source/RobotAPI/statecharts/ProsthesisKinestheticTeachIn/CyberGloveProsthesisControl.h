/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ProsthesisKinestheticTeachIn
 * @author     Julia Starke ( julia dot starke at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <RobotAPI/statecharts/ProsthesisKinestheticTeachIn/CyberGloveProsthesisControl.generated.h>
//#include <RobotAPI/libraries/SimpleJsonLogger/SimpleJsonLogger.h>

namespace armarx::ProsthesisKinestheticTeachIn
{
    class CyberGloveProsthesisControl :
        public CyberGloveProsthesisControlGeneratedBase < CyberGloveProsthesisControl >
    {
    public:
        CyberGloveProsthesisControl(const XMLStateConstructorParams& stateData):
            XMLStateTemplate < CyberGloveProsthesisControl > (stateData), CyberGloveProsthesisControlGeneratedBase < CyberGloveProsthesisControl > (stateData)
        {
        }

        enum class PhaseType
        {
            CalibOpen,
            CalibClose,
            StartPose,
            TeachIn
        };

        // inherited from StateBase
        void onEnter() override;
        void run() override;
        // void onBreak() override;
        void onExit() override;

        // static functions for AbstractFactory Method
        static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData);
        static SubClassRegistry Registry;

        // DO NOT INSERT ANY CLASS MEMBERS,
        // use stateparameters instead,
        // if classmember are neccessary nonetheless, reset them in onEnter

    private:
        void shapeHand(float fingers, float thumb);
        //SimpleJsonLoggerEntry getRawJointValuesForCalibration(CyberGloveValues& openValues, CyberGloveValues& closedValues);
        //void addCurrentRawJointValues(CyberGloveValues& currentValues, SimpleJsonLoggerEntry& e);


    };
}



