#!/bin/bash

# By Fabian Peller 2021-06-10;
# Extended by Rainer Kartmann 2021-06-23;


if [ "$#" -ne 1 ]; then
  echo "Usage: $0 target-directory" >&2
  echo -e "    target-directory \t Directory where mongocxx should be placed (e.g. ~/repos)"
  exit 1
fi


# Stolen from mycroft-core/dev_setup.sh  // Rainer Kartmann
function found_exe() {
    hash "$1" 2>/dev/null
}

# If tput is available and can handle multiple colors
if found_exe tput ; then
    if [[ $(tput colors) != "-1" && -z $CI ]]; then
        GREEN=$(tput setaf 2)
        BLUE=$(tput setaf 4)
        CYAN=$(tput setaf 6)
        YELLOW=$(tput setaf 3)
        RED=$(tput setaf 1)
        RESET=$(tput sgr0)
        HIGHLIGHT=$YELLOW
    fi
fi


function get_YN() {
    # Loop until the user hits the Y or the N key
    echo -e -n "Choice [${CYAN}Y${RESET}/${CYAN}N${RESET}]: "
    while true; do
        read -N1 -s key
        case $key in
        [Yy])
            return 0
            ;;
        [Nn])
            return 1
            ;;
        esac
    done
}


extractDir="$1"
sourceDir="$extractDir/mongo-cxx-driver-r3.2.1"
installDir="$extractDir/mongo-cxx-driver-r3.2.1/build/install"
scriptDir="$(readlink -f "$(dirname "$0")")"
pwd="$(pwd)"


# You first have to install libbson-dev and libmongoc-dev (not necessary on the lab pcs)
#sudo apt install libbson-dev libmongoc-dev

# Then you have to update the symlinks (not necessary on the lab pcs)
# Update the symlinks because the default mongoc bson cmake configs are shitty
#sudo mkdir -p /usr/lib/include/
#sudo ln -s /usr/include/libbson-1.0/ /usr/lib/include/libbson-1.0
#sudo ln -s /usr/include/libbson-1.0/ /usr/lib/include/libbson-1.0
#sudo ln -s /usr/include/libmongoc-1.0/ /usr/lib/include/libmongoc-1.0
#sudo ln -s /usr/lib /usr/lib/lib


function install_deps()
{
    APT_PACKAGE_LIST=(libbson-dev libmongoc-dev)
    
    declare -a installed_packages
    declare -a missing_packages
   
    for pkg in "${APT_PACKAGE_LIST[@]}"; do 
        if dpkg -V "$pkg" > /dev/null 2>&1 ; then
            installed_packages+=($pkg)
        else
            missing_packages+=($pkg)
        fi
    done
    
    if [ ${#installed_packages[*]} -ne 0 ]; then
        echo "$GREEN
The following apt packages are already installed: "
        echo "${installed_packages[*]} $RESET"
    fi
    
    if [ ${#missing_packages[*]} -eq 0 ]; then
    
        echo "$GREEN
No missing packages. $RESET"
        echo ""
        
    else
    
        echo "$YELLOW
The following apt packages still need to be installed: "
        echo "${missing_packages[*]} $RESET"
        
        echo "
Do you want to install these packages and update some required 
symlinks in /usr/lib/include/? (requires sudo)
Y)es, install missing packages via apt-get and update symlinks
N)o, don't install or I don't have sudo permissions"

        if get_YN ; then
            echo -e "$HIGHLIGHT Y - installing missing packages $RESET"
            sudo apt-get install ${missing_packages[*]}
            
            function verbose_ln()
            {
                echo "Updating symlink $2 -> $1"
                sudo ln -s "$1" "$2"
            }
                        
            sudo mkdir -p /usr/lib/include/
            verbose_ln /usr/include/libbson-1.0/ /usr/lib/include/libbson-1.0
            verbose_ln /usr/include/libbson-1.0/ /usr/lib/include/libbson-1.0
            verbose_ln /usr/include/libmongoc-1.0/ /usr/lib/include/libmongoc-1.0
            verbose_ln /usr/lib /usr/lib/lib

        else
            echo -e "$HIGHLIGHT N - not installing missing pacakges $RESET"
            echo ""
        fi
        
    fi
}

install_deps


# Then you can build the mongocxx driver
mkdir $extractDir &> /dev/null || true
tar -xvf mongo-cxx-driver-r3.2.1.tar.gz -C $extractDir
cd $sourceDir
mkdir build &> /dev/null || true
cd build
cmake ..
cmake .. -DBSON_LIBRARY="/usr/lib/x86_64-linux-gnu/libbson-1.0.so" -DMONGOC_LIBRARY="/usr/lib/x86_64-linux-gnu/libmongoc-1.0.so" -DCMAKE_INSTALL_PREFIX="$installDir"
# install to $extractDir/build/install
cmake --build . -- -j`nproc`
cmake --build . --target install

# Finally, tell RobotAPI where to find mongocxx
echo "$HIGHLIGHT
You now need to cmake RobotAPI with:
cmake -DCMAKE_PREFIX_PATH=$installDir .. $RESET"

echo "
Should I do that for you?
Y)es, run cmake in RobotAPI
N)o, I'll do it myself"
if get_YN ; then

    echo -e "$HIGHLIGHT Y - running cmake in RobotAPI $RESET"    
    armarx-dev exec --no-deps RobotAPI "cd build && cmake -DCMAKE_PREFIX_PATH=$installDir ."

else
    echo -e "$HIGHLIGHT N - not running cmake in RobotAPI $RESET"
fi
