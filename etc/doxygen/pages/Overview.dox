/**
\page RobotAPI-Overview RobotAPI Overview
The RobotAPI package provides a robot-intependent API that other ArmarX packages can use for accessing central functionality of a robot.
The central elements of RobotAPI are Sensor-Actor Units, i.e. components responsible for the propagation of sensor values and actor targets.
Sensor-Actor Units work as a link between higher level ArmarX components and the actual robot hardware or a robot simulation environment, respectively.
The RobotAPI package contains base classes for Sensor-Actor Units for different purposes, e.g. KinematicUnit for sensors and actors related to robotic joints.
Concrete Sensor-Actor Units inherit from these base classes in order to provide a common interface.
Each type of hardware and each type of simulation environment requires a distinct set of Sensor-Actor Units that handle the hardware/simulator access, e.g. KinematicUnitArmar3 and KinematicUnitArmar4 for accessing the kinematic elements of ARMAR-III and ARMAR-4, respectively:

\image html sensoractorunits2.svg "Inheritance diagram for Sensor-Actor Units"

There are three ways to access Sensor-Actor Units:
- Direct access via the unit's interface, e.g. KinematicUnitInterface. See the \link RobotAPI-SensorActorUnits list of Sensor-Actor Units\endlink.

- Indirect access via a corresponding \link RobotAPI-SensorActorUnits-observers Observer\endlink.
An Observer is a component that corresponds to a Sensor-Actor Unit.
It monitors the continuous sensor value stream propagated by the unit and applies registered conditions to the sensor data.
Each condition evauating to true causes the Observer to fire a dedicated event (\link Observers Read more\endlink).

- Indirect access via the RobotStateComponent.
The RobotStateComponent maintains a Simox representation of the robot synchronized with the available Sensor-Actor Units, particularly with the KinematicUnit (\link RobotAPI-Components Read more\endlink).

\image html sensoractorunits.svg "Inheritance diagram for Sensor-Actor Units"


Further documentation on the various contents of the RobotAPI package:

\par Intelligent Coordinates
RobotAPI provides datatypes for intelligent coordinates that refer to a robot coordinate frame and can be conveniently transformed between different frames.
Datatypes are available for positions, orientations, directions and poses.

\par
\link RobotAPI-FramedPose Read more \endlink

\par Remote Robot State
The ArmarX Robot State is the global pose and joint configuration of a robot and is used for coordinate frame transformations.
These Transformations between coordinate frames are essential everyday operations in a robotics framework.
ArmarX offers several ways to cope with the different requirements of coordinate transformations in a distributed system like ArmarX.

\par
\subpage RobotAPI-RemoteRobot-Overview "Read more"

\par Interfaces
RobotAPI provides robot-independent interface definitions that are implemented and used by components in other ArmarX packages.
Important interfaces in RobotAPI for instance provide access to the kinematic robot state, different sensor modalities, inverse kinematics computations or the robot visualization.

\par
\link slicedocumentation Read more \endlink

\par Components
RobotAPI not only provides interfaces, but also implementations of robot-independent components.
This includes components for management of the robot state or inverse kinematics computations as well as sensor-actor units.

\par
\link RobotAPI-Components Read more \endlink

\par GUI-Plugins
RobotAPI contains several robot-independent GUI-plugins that refer to the above components.
Available GUI-plugins provide access to the kinematic robot state, end effectors, inverse kinematics computations or different sensor modalities.
RobotAPI also provides a GUI-plugin for robot visualization.

\par
\link RobotAPI-GuiPlugins Read more \endlink


\defgroup RobotAPI RobotAPI
\copydoc RobotAPI-Overview

\defgroup RobotAPI-Statecharts Statecharts
\ingroup RobotAPI
*/
